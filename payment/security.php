<?php

define ('HMAC_SHA256', 'sha256');
define ('SECRET_KEY', '58f7eda7b2924112bc02a7dc459580c64d6d37be767a41f0a425cce17048002e15c008888f8b40a89455654b255cba648996de2139794c569f6701fedca12a46a23dbeedd4274532a623ab27b1ee56f080c015b34a794a329fc24a7612d2d3f1e6c3fe231a0140c1bc76df2ff7c8d441654879047f9e43bea73dc23805fc1d9b');

function sign ($params) {
  return signData(buildDataToSign($params), SECRET_KEY);
}

function signData($data, $secretKey) {
    return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
}

function buildDataToSign($params) {
        $signedFieldNames = explode(",",$params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
           $dataToSign[] = $field . "=" . $params[$field];
        }
        return commaSeparate($dataToSign);
}

function commaSeparate ($dataToSign) {
    return implode(",",$dataToSign);
}

?>

<?php
class WidgetModel extends CI_Model {
    
    public function selectAll($params){
       
        $this->db->select('wid.widgetID,wid.dateAdded,widDet.widgetName,wid.status');
        $this->db->from('ec_widget wid');
        $this->db->join('ec_widget_detail widDet','wid.widgetID = widDet.widgetID AND widDet.languageID='.$params['languageID'],'left');
         if($params['widgetName']){
            $this->db->where('widDet.widgetName LIKE', '%'.$params['widgetName'].'%');
        }
        $this->db->where('wid.status!=', 'Deleted');
       
        $this->db->order_by('widgetID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
    
    
    public function selectWidgetDetail($widgetID="",$language=""){
        $this->db->select('wid.widgetID,wid.imageDimension,wid.typeID,wid.sortOrder,wid.startDate,wid.endDate,wid.status,widDet.widgetName,widType.typeName,widType.customProduct,widType.typeKey');
        $this->db->from('ec_widget wid');
        $this->db->join('ec_widget_detail widDet','wid.widgetID = widDet.widgetID AND widDet.languageID='.$language,'left');
        $this->db->join('ec_widget_type widType','widType.typeID = wid.typeID','left');
        $this->db->where('wid.status!=', 'Deleted');
        $this->db->where('wid.widgetID',$widgetID);
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->row_array();
    }
    
    public function widgetProduct($widgetID){
        $this->db->select('widgetID,productID');
        $this->db->from('ec_widget_to_product');
         $this->db->where('widgetID',$widgetID);
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
    
     public function widgetCategory($widgetID){
        $this->db->select('widgetID,categoryID');
        $this->db->from('ec_widget_to_category');
         $this->db->where('widgetID',$widgetID);
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
    
    public function getWidgetType(){
        $this->db->select('typeID,typeName,typeDescription,customProduct');
        $this->db->from('ec_widget_type');
        $query = $this->db->get();
        return $query->result_array();
    }
    
    public function getcustomProduct($typeID){
        $this->db->select('customProduct,typeKey');
        $this->db->from('ec_widget_type');
        $this->db->where('typeID', $typeID);
         $this->db->where('status', 'Active');
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->row_array();
    }
    
    public function widgetHomeShow($language="",$customerID=""){

        $sql    = 'SELECT wid.widgetID,wid.imageDimension,wid.typeID,wid.sortOrder,wid.startDate,wid.endDate,wid.status,
                    widDet.widgetName,widType.typeName,widType.customProduct,widType.typeKey
                    FROM ec_widget wid
                    LEFT JOIN ec_widget_detail widDet ON wid.widgetID = widDet.widgetID AND widDet.languageID='.$language .' 
                    LEFT JOIN ec_widget_type widType ON widType.typeID = wid.typeID
                    WHERE wid.status="Active" AND
                    (CASE
                    WHEN widType.typeKey="new" THEN 1
                    WHEN widType.typeKey="category_product" THEN 1
                    WHEN widType.typeKey!="new" THEN (wid.startDate<=CURDATE()) AND (wid.endDate>=CURDATE())
                    END)
                    ORDER BY wid.sortOrder ASC';
    
        $query = $this->db->query($sql);
        //echo $this->db->last_query();exit;
        $widgetData =    $query->result_array();
        //echo "<pre>"; print_r($widgetData);exit;
        if(!empty($widgetData)){
            foreach($widgetData as $key=>$data){
                
                if($data['typeKey']=='category'){
                        $this->db->select('cat.categoryID,cat.pageKey,catDet.image,catDet.name');
                        $this->db->from('ec_widget_to_category widCat');
                        $this->db->join('ec_category cat','cat.categoryID = widCat.categoryID','left');
                        $this->db->join('ec_category_detail catDet','catDet.categoryID = cat.categoryID AND catDet.languageID='.$language,'left');
                        $this->db->where('widCat.widgetID', $data['widgetID']);
                        $this->db->where('cat.status', 'Active');
                        $this->db->limit(3); // limit 3
                        $query = $this->db->get();
                        $widgetData[$key]['widgetCategory']  =   $query->result_array();
                        //echo "<pre>"; print_r($widgetData[$key]['widgetCategory']);exit;
                }else{

                    if($customerID)
                            $wishList   =   ",IF((SELECT productID FROM ec_customer_wishlist wish WHERE wish.customerID='".$customerID."' AND wish.productID=pro.productID), 'YES', 'NO') AS wishList";
                        else 
                            $wishList = "";
                    if($data['customProduct']=='YES'){
                        if($data['widgetID']){
                            $this->db->select('pro.productID,pro.quantity,pro.weight,pro.pageKey,pro.image,pro.price,proDet.description,proDet.name,pro.dateAdded'.$wishList);
                            $this->db->from('ec_widget_to_product widPro');
                            $this->db->join('ec_product pro','widPro.productID = pro.productID','left');
                            $this->db->join('ec_product_description proDet','proDet.productID = pro.productID AND proDet.languageID='.$language,'left');
                            $this->db->where('widPro.widgetID', $data['widgetID']);
                            $this->db->where('pro.status', 'Active');
                            $query = $this->db->get();
                            //echo $this->db->last_query();exit;
                            $widgetData[$key]['widgetProduct']  =   $query->result_array();
                        }
                    }
                    elseif($data['customProduct']=='NO'){ //Latest 10 product fetching
                        if($data['typeKey']=='category_product'){
                            
                        
//                            $this->db->select('pro.productID,pro.pageKey,pro.image,pro.price,proDet.description,proDet.name,pro.dateAdded'.$wishList);
//                            $this->db->from('ec_widget_to_category widCat');
//                            $this->db->join('ec_product_to_category proCat','proCat.categoryID = widCat.categoryID');
//                            $this->db->join('ec_product pro','pro.productID = proCat.productID');
//                            $this->db->join('ec_product_description proDet','proDet.productID = pro.productID AND proDet.languageID='.$language,'left');
//                            $this->db->where('pro.status', 'Active');
//                            $this->db->order_by('pro.dateAdded','DESC');
//                            $this->db->where('widCat.widgetID', $data['widgetID']);
//                            $this->db->limit(10);
//                            $query = $this->db->get();
//                            $widgetData[$key]['widgetProduct']  =   $query->result_array();
                            $sql    =   "select p.productID,p.quantity,p.weight,p.pageKey,p.quantity,p.image,p.price,pd.description,pd.name,p.dateAdded
                                        FROM ec_category_path cp LEFT JOIN ec_product_to_category p2c ON (cp.categoryID = p2c.categoryID) 
                                        LEFT JOIN ec_product p ON (p2c.productID = p.productID) 
                                        LEFT JOIN ec_product_description pd ON (p.productID = pd.productID) 
                                        LEFT JOIN ec_widget_to_category wc ON (wc.categoryID = cp.pathID)
                                        WHERE pd.languageID = '".$language."' AND p.status = 'Active' AND
                                        p.dateAvailable <= NOW() AND
                                        wc.widgetID = '".$data['widgetID']."' 
                                        GROUP BY p.productID 
                                        ORDER BY p.sortOrder ASC LIMIT 10";
//                            echo $sql;exit;".$data['widgetID']."
                             $widgetData[$key]['widgetProduct'] = $this->db->query($sql)->result_array();
                            
                        
                        
                        }else{
                            $this->db->select('pro.productID,pro.quantity,pro.weight,pro.pageKey,pro.image,pro.price,proDet.description,proDet.name,pro.dateAdded'.$wishList);
                            $this->db->from('ec_product pro');
                            $this->db->join('ec_product_description proDet','proDet.productID = pro.productID AND proDet.languageID='.$language,'left');
                            $this->db->where('pro.status', 'Active');
                            $this->db->order_by('pro.sortOrder','ASC');
                            $this->db->limit(10);
                            $query = $this->db->get();
                            $widgetData[$key]['widgetProduct']  =   $query->result_array();
                        }
                    }
                }
            }
        return $widgetData;
        }
    }
}
<?php

class AccountModel extends CI_Model {

    public function selectWishList($languageID = '', $customerID = '') {

        $this->db->select('pro.productID,pro.quantity,custWish.customerID,pro.image,pro.price,proDet.description,proDet.name,pro.dateAdded,pro.model');
        $this->db->from('ec_customer_wishlist custWish');
        $this->db->join('ec_product pro', 'custWish.productID = pro.productID', 'left');
        $this->db->join('ec_product_description proDet', 'custWish.productID=proDet.productID AND proDet.languageID=' . $languageID, 'left');
        $this->db->where('customerID', $customerID);
        $this->db->where('status', 'Active');
        $this->db->order_by("proDet.name", "ASC");


        $this->db->group_by("pro.productID");
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

    public function selectAddress($customerID = '') {
        $this->db->select('addr.addressID,addr.firstname,addr.lastname,addr.address1,addr.address2,addr.city,addr.phone,addr.postcode,addr.zone,addr.additional_direction');
        $this->db->from('ec_address addr');
        $this->db->where('customerID', $customerID);
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
    
    public function selectMobAddress($customerID = '') {
        $this->db->select('addr.addressID,addr.firstname,addr.lastname,addr.address1,addr.address2,addr.city,addr.phone,addr.postcode,addr.zone');
        $this->db->from('ec_address addr');
        $this->db->where('customerID', $customerID);
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result_array();
    }

    public function selectAddressDefault($customerID = '') {
        $this->db->select('cust.addressID');
        $this->db->from('ec_customer cust');
        $this->db->where('customerID', $customerID);
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->row_array();
    }
    
    public function selectStateList($languageID = '', $customerID = '') {

        $this->db->select('cit.cityID,cit.countryID,cit.sortOrder,citDet.name');
        $this->db->from('ec_cities cit');
        $this->db->join('ec_city_detail citDet', 'cit.cityID = citDet.cityID AND citDet.languageID=' . $languageID, 'left');
        
        $this->db->where('status', 'Active');
        $this->db->order_by("citDet.cityID", "ASC");


        $this->db->group_by("cit.cityID");
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

   public function checkDefault($customerID = '') {

        $this->db->select('add.addressID,cust.customerID');
        $this->db->from('ec_address add');
        $this->db->join('ec_customer cust', 'add.addressID = cust.addressID', 'inner');
        $this->db->where('add.customerID', $customerID);
        $this->db->where('cust.status', 'Active');
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->row_array();
    }

}

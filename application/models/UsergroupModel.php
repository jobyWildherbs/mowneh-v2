<?php
class UsergroupModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('*');
        $this->db->from('ec_user_group');
       // $this->db->join('ec_user_group_detail','ec_user_group.userGroupID = ec_user_group_detail.userGroupID AND ec_user_group_detail.languageID='.$params['languageID'],'left');
         if($params['name']){
            $this->db->where('ec_user_group.name LIKE', '%'.$params['name'].'%');
        }
        $this->db->where('ec_user_group.status!=', 'delete');
       
        $this->db->order_by('userGroupID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
   
}
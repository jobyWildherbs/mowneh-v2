<?php

class PremotionModel extends CI_Model {

    public function selectAll($params) {
        $this->db->select('pro.premotionID,pro.type,pro.priority,pro.applyStatus,pro.dateStart,pro.dateEnd,pro.status,pro.dateAdded,pro.dateAdded,proDet.title');
        $this->db->from('ec_premotion pro');
        $this->db->join('ec_premotion_detail proDet', 'pro.premotionID = proDet.premotionID AND proDet.languageID='.$params['languageID'],'left');
        if($params['name']){
            $this->db->where('proDet.title LIKE', '%'.$params['name'].'%');
        }
        $this->db->where('pro.status!=', 'Deleted');
 
        $this->db->order_by('pro.premotionID', 'DESC');

        if ($params['limit']) {
            $this->db->limit($params['limit'], $params['start']);
        }

        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }

    public function selectFilter($id = '', $languageID = '') {

        $this->db->select('manuFact.manufacturerID, manuFactDet.languageID,manuFactDet.name as manufactureName, manuFact.sort_order sortOrder');
        $this->db->from('ec_manufacturer manuFact');
        $this->db->join('ec_manufacturer_detail manuFactDet', 'manuFact.manufacturerID = manuFactDet.manufacturerID AND manuFactDet.languageID=' . $languageID, 'left');
        $this->db->where('manuFact.status!=', 'Deleted');
        if ($id !== '') {
            $this->db->where('manuFact.manufacturerID', $id);
        }
        $this->db->order_by('manuFact.sort_order', 'DESC');
        //$this->db->limit();
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        if ($id != '') {
            return $resultSet->row_array();
        }
        return $resultSet->result_array();
    }
    
    public function getProductByCategory($categoryID){
        $this->db->select('pro.productID, pro.price');
        $this->db->from('ec_product pro');
        $this->db->join('ec_product_to_category proCat', 'proCat.productID = pro.productID AND manuFactDet.languageID=' . $languageID, 'left');
        $this->db->where('manuFact.status!=', 'Deleted');
        if ($id !== '') {
            $this->db->where('manuFact.manufacturerID', $id);
        }
        $this->db->order_by('manuFact.sort_order', 'DESC');
        //$this->db->limit();
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        if ($id != '') {
            return $resultSet->row_array();
        }
        return $resultSet->result_array();
    }


}

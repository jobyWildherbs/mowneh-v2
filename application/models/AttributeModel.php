<?php

/**
 * Description of AttributeModel
 *
 * @author wildherbs-user
 */
class AttributeModel extends CI_Model {

    public function selectGroup($id = '',$languageID) {
        $this->db->select('aG.attributeGroupID as ID, aG.sortOrder as sortOrder, aGD.languageID as langID, aGD.name as name,aG.status');
        $this->db->from('ec_attribute_group aG');
        $this->db->join('ec_attribute_group_detail aGD', 'aG.attributeGroupID = aGD.attributeGroupID AND aGD.languageID = '.$languageID,'left');
        if ($id != '') {
            $this->db->where('aG.attributeGroupID', $id);
        }
        $this->db->where('aG.status!=', 'Deleted');
        $this->db->order_by('sortOrder', 'ASC');
        //$this->db->limit();
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        if ($id != '') {
            return $resultSet->row_array();
        } else {
            return $resultSet->result_array();
        }
    }

    public function selectAllGroup($params) {
        $this->db->select('aG.attributeGroupID as ID, aG.sortOrder as sortOrder, aGD.languageID as langID, aGD.name as name,aG.status ');
        $this->db->from('ec_attribute_group aG');
        $this->db->join('ec_attribute_group_detail aGD', 'aG.attributeGroupID = aGD.attributeGroupID AND aGD.languageID = '.$params['languageID'],'left');
        if ($params['title']) {
            $this->db->where('aGD.name LIKE', '%' . $params['title'] . '%');
        }
        $this->db->where('aG.status!=', 'Deleted');
        $this->db->order_by('aG.attributeGroupID', 'DESC');
        if($params['limit']){
            $this->db->limit($params['limit'], $params['start']);
        }
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;

        return $resultSet->result_array();
    }

    public function selectActiveGroup($id = '') {
        $this->db->select('aG.attributeGroupID as ID, aG.sortOrder as sortOrder, aGD.languageID as langID, aGD.name as name,aG.status ');
        $this->db->from('ec_attribute_group aG');
        $this->db->join('ec_attribute_group_detail aGD', 'aG.attributeGroupID = aGD.attributeGroupID');
        if ($id != '') {
            $this->db->where('aG.attributeGroupID', $id);
        }
        $this->db->where('aG.status', 'Active');
        $this->db->order_by('sortOrder', 'ASC');
        
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;

        return $resultSet->result_array();
    }

    public function selectAll($params, $language) {
        $this->db->select('a.attributeID as ID, a.attributeGroupID as groupID, a.sortOrder as sortOrder, aD.languageID as langID, aD.name as name, a.status as status');
        $this->db->from('ec_attribute a');
        $this->db->join('ec_attribute_detail aD', 'a.attributeID = aD.attributeID AND aD.languageID =' . $language, 'left');
        if ($params['title']) {
            $this->db->where('aD.name LIKE', '%' . $params['title'] . '%');
        }
        $this->db->where('a.status!=', 'Deleted');
        $this->db->order_by('a.attributeID', 'DESC');
        if ($params['limit']) {
            $this->db->limit($params['limit'], $params['start']);
        }
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

    public function select($id = '', $language) {
        $this->db->select('a.attributeID as ID, a.attributeGroupID as groupID, a.sortOrder as sortOrder, aD.languageID as langID, aD.name as name, a.status');
        $this->db->from('ec_attribute a');
        $this->db->join('ec_attribute_detail aD', 'a.attributeID = aD.attributeID AND aD.languageID =' . $language, 'left');
        if ($id != '') {
            $this->db->where('a.attributeID', $id);
        }
        //$this->db->where('a.status', 'Active'); 
        $this->db->order_by('sortOrder', 'ASC');
        //$this->db->limit();
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        if ($id != '') {
            return $resultSet->row_array();
        } else {
            return $resultSet->result_array();
        }
    }

    public function selectAttribute($id = '', $languageID) {

        $this->db->select('attr.attributeID, attrDet.languageID, attrDet.name, attr.sortOrder, attr.attributeGroupID, attr.status');
        $this->db->from('ec_attribute attr');
        $this->db->join('ec_attribute_detail attrDet', 'attr.attributeID = attrDet.attributeID AND attrDet.languageID=' . $languageID, 'left');
        if ($id !== '') {
            $this->db->where('attr.attributeGroupID', $id);
        }
        $this->db->where('attr.status', 'Active');
        $this->db->order_by('attr.sortOrder', 'ASC');
        //$this->db->limit();
        $resultSet = $this->db->get();
//        print_r($this->db->last_query()); exit;
        
        return $resultSet->result_array();
    }

    
     public function importData($data,$languageID){
        //echo "<pre>"; print_r($data); exit;
        $this->load->library('Import');
        $this->load->model('GeneralModel', 'generalModel');
        $rowCount   =   2;
        foreach($data as $key=>$dataVal){
            $uploadErr  =   0;
            if(@$dataVal['attributeGroupName']){
                $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($dataVal['attributeGroupName'])));
                $attributeGroupKey  =   $importKey;
                
                $attGroupDet['name']         = $dataVal['attributeGroupName'];
                $attGroup['importKey']       = $importKey;
            }
            if(@$dataVal['sortOrder'])
                    $attGroup['sortOrder']       = $dataVal['sortOrder'];
            if(@$dataVal['sortOrder'])
                    $attrVal['sortOrder']       = $dataVal['sortOrder'];
            if(@$dataVal['attributeGroupName_ar'])
                    $attGroupDet_ar['name']       = $dataVal['attributeGroupName_ar'];
            
            if(@$dataVal['attributeName']){
                    $attribute['attributeName']      = $dataVal['attributeName'];
                    $attributeGroupID   =   '';
                    if($attributeGroupKey){
                            $attributeGroupID   =   $this->generalModel->getFieldValue('attributeGroupID', 'ec_attribute_group','importKey="'.$attributeGroupKey.'" AND status!="Deleted"');
                    }
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($attribute['attributeName'])));
                    $attributeID   =   $this->generalModel->getFieldValue('attributeID', 'ec_attribute','importKey="'.$importKey.'" AND attributeGroupID="'.$attributeGroupID.'" AND status="Active"');
                    if($attributeID!=""){
                            $uploadErr  =   1;
                                $errorMsg[]   =   $this->import->setAttributeExistMessage('attribute',$attribute['attributeName'],$rowCount);
                        }
            }
            if(@$dataVal['attributeName_ar'])
                    $attribute_ar['attributeName']      = $dataVal['attributeName_ar'];
          
                if($uploadErr==0){
                    if(!empty($attGroup)){
                        if($attributeGroupKey){
                            $attributeGroupID   =   $this->generalModel->getFieldValue('attributeGroupID', 'ec_attribute_group','importKey="'.$attributeGroupKey.'" AND status!="Deleted"');
                            if($attributeGroupID==''){
                                    $this->db->insert('ec_attribute_group', $attGroup); 
                                    $attributeGroupID =   $this->db->insert_id();

                                    if(!empty($attGroupDet)){
                                        $attGroupDet['attributeGroupID']  =  $attributeGroupID; 
                                        $attGroupDet['languageID']    =   1;
                                        $this->db->insert('ec_attribute_group_detail', $attGroupDet);  
                                    }
                                    if(!empty($attGroupDet_ar)){
                                        $attGroupDet_ar['attributeGroupID']  =  $attributeGroupID; 
                                        $attGroupDet_ar['languageID']    =   2;
                                        $this->db->insert('ec_attribute_group_detail', $attGroupDet_ar);  

                                    }
                                }
                            }
                        }


                    if(!empty($attribute)){
                                $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($attribute['attributeName'])));
                                $attributeID   =   $this->generalModel->getFieldValue('attributeID', 'ec_attribute','importKey="'.$importKey.'" AND status="Active"');
                                $attrVal['attributeGroupID']    =   $attributeGroupID;
                                $attrVal['importKey']           =   $importKey;
                                $this->db->insert('ec_attribute', $attrVal);
                                $attributeID   =     $this->db->insert_id();
                                if($attribute['attributeName']){  
                                    $attr_eng     =   array();
                                    $attr_eng['attributeID']      =   $attributeID;
                                    $attr_eng['name']             =   $attribute['attributeName'];
                                    $attr_eng['languageID']       =   '1';
                                    $this->db->insert('ec_attribute_detail', $attr_eng);
                                }

                                if($attribute_ar['attributeName'] ){
                                    $attr_ar     =   array();
                                    $attr_ar['attributeID']         =   $attributeID;
                                    $attr_ar['name']             =   $attribute_ar['attributeName'];
                                    $attr_ar['languageID']       =   '2';
                                    $this->db->insert('ec_attribute_detail', $attr_ar);
                                }
                    }
                }
                $rowCount++;
        }
        if(!empty($errorMsg)){
            return $errorMsg;
        }
       return true;
    }

}

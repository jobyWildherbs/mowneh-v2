<?php
class AddBannerModel extends CI_Model {
        public function selectAll($anguageID,$params){
            $this->db->select('add.addID,add.addUrl,addDet.image,add.status');
            $this->db->from('ec_add_banner as add');
            $this->db->join('ec_add_banner_detail as addDet','add.addID = addDet.addID AND addDet.languageID='.$anguageID,'left');  
            if ($params['title']) {
            $this->db->where('add.addUrl LIKE', '%' . $params['title'] . '%');
        }
            $this->db->order_by('add.addID', 'DESC'); 

            $query = $this->db->get();
            // print_r($this->db->last_query()); exit;
            return $query->result_array();
        }
        
         public function selectForHomePaage($anguageID){
            $this->db->select('add.addID,add.addUrl,addDet.image,add.status');
            $this->db->from('ec_add_banner as add');
            $this->db->join('ec_add_banner_detail as addDet','add.addID = addDet.addID AND addDet.languageID='.$anguageID,'left');  
            $this->db->where('add.status', 'Active');
            $this->db->order_by('add.addID', 'DESC'); 

            $query = $this->db->get();
            // print_r($this->db->last_query()); exit;
            return $query->row_array();
        }
}
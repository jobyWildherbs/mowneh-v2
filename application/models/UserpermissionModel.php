<?php
class UserpermissionModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('*');
        $this->db->from('ec_user_permission');
       // $this->db->join('ec_user_permission_detail','ec_user_permission.id = ec_user_permission_detail.id AND ec_user_permission_detail.languageID='.$params['languageID'],'left');
         if($params['permissionName']){
            $this->db->where('ec_user_permission.permissionName LIKE', '%'.$params['permissionName'].'%');
        }
        $this->db->where('ec_user_permission.status!=', 'delete');
       
        $this->db->order_by('id', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
   
}
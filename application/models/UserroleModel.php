<?php
class UserroleModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('usp.id,ug.name,up.className,usp.value');
        $this->db->from('ec_user_permission_group usp');
        $this->db->join('ec_user_group ug','usp.id = ug.userGroupID','left');
        $this->db->join('ec_user_permission up','usp.permissionID = up.id','left');
//         if($params['name']){
//            $this->db->where('ec_user_permission_group.name LIKE', '%'.$params['name'].'%');
//        }
        //$this->db->where('ec_user_permission_group.status!=', 'delete');
       
        $this->db->order_by('usp.id', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
   
}
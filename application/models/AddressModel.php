<?php
/**
 * Description of AddressModel
 *
 * @author wildherbs-user
 */
class AddressModel extends CI_Model {
    //put your code here
    public function getAddress($customerID, $addressID = null){
        $this->db->select('a.*,c.name country');
        $this->db->from('ec_address a');
        $this->db->join('ec_country c','c.countryID = a.countryID','left');
        if($addressID!=null){
            $this->db->where('addressID',$addressID);
        }
        $this->db->where('customerID',$customerID);
        $result = $this->db->get();
//        echo $this->db->last_query();
        if($addressID!=null){
            return $result->row_array();
        } else {
            return $result->result_array();
        }
    } 
}

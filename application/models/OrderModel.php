<?php

class OrderModel extends CI_Model {

    public function selectAll($params = null) {
        extract($params);
        $this->db->select('o.orderID, o.invoiceNo, o.invoicePrefix, o.firstname, o.lastname, o.total, osd.name as status, o.dateAdded as added, o.dateModified as modified');
        $this->db->from('ec_order o');
        $this->db->join('ec_order_status os', 'o.orderStatusID = os.orderStatusID', 'left');
        $this->db->join('ec_order_status_detail osd', 'o.orderStatusID = osd.orderStatusID AND osd.languageID = '.$params['languageID'], 'left');
        $this->db->join('ec_order_transaction oTrn', 'o.orderID = oTrn.orderID', 'inner');
            $where = '(oTrn.paymentStatus="success" or oTrn.paymentStatus = "cod")';
            $this->db->where($where);
        if (@$orderID != '' )  {
            $this->db->where('o.orderID', $orderID);
        }
        if (isset($limit)) {
            if (isset($start))
                $this->db->limit($limit, $start);
            else
                $this->db->limit($limit);
        }
        $this->db->order_by('o.orderID', 'DESC');
        $resultSet = $this->db->get();
    //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

    public function selectOrderList($params = null) {

        $this->db->select('o.orderID, o.invoiceNo, o.invoicePrefix, o.firstname, o.paymentMethod, o.lastname, o.total, osd.name as status, o.dateAdded as Added, o.dateModified as Modified,ordDet.quantity,ot.value as total');
        $this->db->from('ec_order o');
        $this->db->join('ec_order_product ordDet', 'o.orderID = ordDet.orderID', 'inner');
        $this->db->join('ec_order_status os', 'o.orderStatusID = os.orderStatusID', 'left');
        $this->db->join('ec_order_status_detail osd', 'o.orderStatusID = osd.orderStatusID AND osd.languageID = '.$params['languageID'], 'left');
        $this->db->join('ec_order_total ot', 'o.orderID = ot.orderID AND ot.code="total"', 'left');
        if(isset($params['myAcc'])=='Yes'){
            $this->db->join('ec_order_transaction oTrn', 'o.orderID = oTrn.orderID', 'inner');
            $where = '(oTrn.paymentStatus="success" or oTrn.paymentStatus = "cod")';
            $this->db->where($where);
//            $this->db->where('oTrn.paymentStatus', 'success');
//            $this->db->orWhere('oTrn.paymentStatus', 'cod');
        }
        //$this->db->distinct();
        if (isset($params['ordDet.orderID'])) {
            $this->db->where('ordDet.orderID', $params['orderID']);
        }
        if (isset($params['customerID'])) {
            $this->db->where('o.customerID', $params['customerID']);
        }
        $this->db->group_by('ordDet.orderID', 'o.orderID');

        if (isset($params['limit'])) {
            if (isset($params['start']))
                $this->db->limit($params['limit'], $params['start']);
            else
                $this->db->limit($params['limit']);
        }
        $this->db->order_by('ordDet.orderID', 'DESC');
        return $this->db->get()->result_array();
    }

    public function selectDetails($customerID = '', $orderID = null) {

        $this->db->select('pro.type,pro.productID,ord.orderID,ord.customerID,ord.invoiceNo,ord.total as totalSum,ordDet.orderProductID,ordDet.total as sumProd,pro.image,ordDet.price,ordDet.quantity,ordDet.name,ordDet.model,ord.paymentFirstname,ord.paymentFirstname,ord.paymentLastname,ord.paymentAddress1,ord.paymentAddress2,ord.paymentZone,ord.paymentCity,ord.paymentPostcode,ord.paymentCountry,ord.paymentCountryID,ord.shippingFirstname,ord.shippingLastname,ord.shippingAddress1,ord.shippingAddress2,ord.shippingZone,ord.shippingCity,ord.shippingPostcode,ord.shippingCountry,ord.shippingCountryID,ord.shipping_additional_direction,ord.languageID,ord.currencyID,ord.currencyCode,ord.currencyValue,ord.dateAdded,ord.dateModified,ord.paymentMethod,ordDet.orderProductID,pro.sku,ts.fromTime,ts.toTime,orToDelivery.deliveryDate,orToDelivery.orderDeliveryType');
        //$this->db->select('*');
        $this->db->from('ec_order ord');
        $this->db->join('ec_order_product ordDet', 'ord.orderID=ordDet.orderID', 'inner');
        $this->db->join('ec_order_to_delivery orToDelivery', 'ord.orderID = orToDelivery.orderID', 'left');
	$this->db->join('ec_delevery_time_slot ts', 'orToDelivery.timeSlotID = ts.slotID', 'left');
        $this->db->join('ec_product pro', 'pro.productID=ordDet.productID', 'left');
        $this->db->where('ord.customerID', $customerID);
        if ($orderID != null) {
            $this->db->where('ord.orderID', $orderID);
        }

//        $this->db->where('status', 'Active');
//        $this->db->order_by("ordDet.name", "ASC");
//
//
//        $this->db->group_by("ord.orderID");
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

    public function getOrderDetails($params) {
        extract($params);
        $this->db->select('o.*,osd.name as orderStatus,c.customerID as custID, c.firstname as fName,c.lastname as lName, c.addressID ,cgd.name as gName,orDriver.driverID,ts.fromTime,ts.toTime,orToDelivery.deliveryDate,orToDelivery.orderDeliveryType');
        $this->db->from('ec_order o');
        $this->db->join('ec_order_status_detail osd', 'o.orderStatusID = osd.orderStatusID AND osd.languageID = '.$languageID, 'left');
        $this->db->join('ec_customer c', 'o.customerID = c.customerID', 'left');
        $this->db->join('ec_customer_group_detail cgd', 'o.customerGroupID = cgd.customerGroupID AND cgd.languageID = '.$languageID, 'left');
        $this->db->join('ec_order_to_driver orDriver', 'o.orderID = orDriver.orderID', 'left');
        $this->db->join('ec_order_to_delivery orToDelivery', 'o.orderID = orToDelivery.orderID', 'left');
        $this->db->join('ec_delevery_time_slot ts', 'orToDelivery.timeSlotID = ts.slotID', 'left');
        $this->db->where('o.orderID', $orderID);
        if (@$limit) {
            if (@$start) {
                $this->db > limit($limit, $start);
            }
            $this->db > limit($limit);
        }
        $this->db->order_by('o.orderID', 'DESC');
        $result = $this->db->get();
//        echo $this->db->last_query();exit;
        return $result->row_array();
    }

    public function orderExport($params) {
        $languageID =   $params['languageID'];
        
        if (strpos($params['fields'], 'order.customerName') !== false) {
            $field = ",concat_ws(' ', order.firstname, order.lastname) AS customerName";
            $params['fields'] = str_replace(',order.customerName', $field, $params['fields']);
        }
        $this->db->select($params['fields']);
        $this->db->from('ec_order order');
        $this->db->join('ec_order_status order_status', 'order.orderStatusID = order_status.orderStatusID AND order_status.status="Active"', 'left');
        $this->db->join('ec_order_status_detail order_status_detail', 'order_status.orderStatusID = order_status_detail.orderStatusID AND order_status_detail.languageID="'.$languageID.'"', 'left');

        if ($params['limit']) {
            $this->db->limit($params['limit'], $params['start']);
        }
        $this->db->order_by('order.orderID', 'ASC');

        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function importData($data,$languageID){
        //echo "<pre>"; print_r($data); exit;
        foreach($data as $key=>$dataVal){
            if(@$dataVal['invoiceNo'])
                $order['invoiceNo']           = $dataVal['invoiceNo'];
            if(@$dataVal['invoicePrefix'])
                $order['invoicePrefix']           = $dataVal['invoicePrefix'];
            if(@$dataVal['storeID'])
                $order['storeID']           = $dataVal['storeID'];
            if(@$dataVal['storeUrl'])
                $order['storeUrl']           = $dataVal['storeUrl'];
            if(@$dataVal['storeName'])
                $order['storeName']           = $dataVal['storeName'];
            if(@$dataVal['customerID'])
                $order['customerID']           = $dataVal['customerID'];
            if(@$dataVal['customerName']){
                $customerName   =   explode(" ",$dataVal['customerName']);
                if($customerName[0])
                    $order['firstname']            = $customerName[0];
                if($customerName[1])
                    $order['lastname']            = $customerName[1];
            }
            
            if(@$dataVal['total'])
                $order['total']              = $dataVal['total'];
            if(@$dataVal['orderStatusID'])
                $order['orderStatusID']              = $dataVal['orderStatusID'];
            if(@$dataVal['customerID'])
                $order['customerID']           = $dataVal['customerID'];
            if(@$dataVal['customerGroupID'])
                $order['customerGroupID']           = $dataVal['customerGroupID'];
            if(@$dataVal['email'])
                $order['email']           = $dataVal['email'];
             if(@$dataVal['telephone'])
                $order['telephone']           = $dataVal['telephone'];
             
             
             if(@$dataVal['paymentFirstname'])
                $order['paymentFirstname']              = $dataVal['paymentFirstname'];
            if(@$dataVal['paymentLastname'])
                $order['paymentLastname']              = $dataVal['paymentLastname'];
            if(@$dataVal['paymentCompany'])
                $order['paymentCompany']           = $dataVal['paymentCompany'];
            if(@$dataVal['paymentAddress1'])
                $order['paymentAddress1']           = $dataVal['paymentAddress1'];
            if(@$dataVal['paymentAddress2'])
                $order['paymentAddress2']           = $dataVal['paymentAddress2'];
             if(@$dataVal['paymentCity'])
                $order['paymentCity']           = $dataVal['paymentCity'];
             if(@$dataVal['paymentPostcode'])
                $order['paymentPostcode']           = $dataVal['paymentPostcode'];
            $order['dateAdded']           = date('Y-m-d H:i');;
            if(!empty($order)){
                $this->db->insert('ec_order', $order);
            }
            
        } 
       return true;
    }

    public function getOrderProducts($params) {
        extract($params);
        $this->db->select('op.*,p.pageKey,p.type, p.image, p.quantity as remaining, p.status ');
        $this->db->from('ec_order_product op');
        $this->db->join('ec_product p', 'op.productID = p.productID', 'left');
        $this->db->where('op.orderID', $orderID);
        if (@$limit) {
            if (@$start) {
                $this->db > limit($limit, $start);
            }
            $this->db > limit($limit);
        }
        $this->db->order_by('op.orderProductID', 'ASC');
        $result = $this->db->get();
        return $result->result_array();
    }
    
    public function getBundleProducts($bundleProductID,$languageID) {
        $this->db->select('pd.name,p.pageKey,p.model,p.type, p.image, p.quantity as remaining, p.status,pb.quantity');
        $this->db->from('ec_product p');
        $this->db->join('ec_product_description pd', 'p.productID = pd.productID AND pd.languageID='.$languageID, 'left');
        $this->db->join('ec_product_bundle pb', 'p.productID = pb.bundleProductID', 'inner');
        $this->db->where('p.productID', $bundleProductID);
        
        $result = $this->db->get();
        return $result->row_array();
    }
    
    public function selectReport($params) {
        //echo "<pre>"; print_r($params); exit;
        $languageID =   $params['languageID'];
        $this->db->select('pro.productID,ord.orderID,ord.customerID,ord.invoiceNo,ord.total as totalSum,ordDet.total as sumProd,ordStatusDet.name as orderStatus,ordDet.price,ordDet.quantity,ordDet.name,ordDet.model,ord.paymentFirstname,ord.paymentFirstname,ord.paymentLastname,ord.paymentAddress1,ord.paymentAddress2,ord.paymentCity,ord.paymentPostcode,ord.paymentCountry,ord.paymentCountryID,ord.shippingFirstname,ord.shippingLastname,ord.shippingAddress1,ord.shippingAddress2,ord.shippingCity,ord.shippingPostcode,ord.shippingCountry,ord.shippingCountryID,ord.languageID,ord.currencyID,ord.currencyCode,ord.currencyValue,ord.dateAdded,ord.dateModified,ordDet.orderProductID,pro.sku');
        //$this->db->select('*');
        $this->db->from('ec_order ord');
        $this->db->join('ec_order_product ordDet', 'ord.orderID=ordDet.orderID', 'left');
        $this->db->join('ec_product pro', 'pro.productID=ordDet.productID', 'left');
        $this->db->join('ec_order_status orderStatus', 'orderStatus.orderStatusID=ord.orderStatusID');
        $this->db->join('ec_order_status_detail ordStatusDet', 'orderStatus.orderStatusID=ordStatusDet.orderStatusID AND ordStatusDet.languageID='.$languageID, 'left');

        if($params['fromDate'])
            $this->db->where('ord.dateAdded >=', $params['fromDate']);
        if($params['toDate'])
            $this->db->where('ord.dateAdded <=', $params['toDate']);
        
        if($params['orderStatusID'])
            $this->db->where('ord.orderStatusID', $params['orderStatusID']);
        
        if ($params['limit']) {
                $this->db->limit($params['limit'], $params['start']);
          
        }
        
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function selectStatusFilter($languageID){
        $this->db->select('orderStatus.orderStatusID,ordStatusDet.name');
        $this->db->from('ec_order_status orderStatus');
        $this->db->join('ec_order_status_detail ordStatusDet', 'orderStatus.orderStatusID=ordStatusDet.orderStatusID AND ordStatusDet.languageID='.$languageID, 'left');
        $this->db->where('orderStatus.status', 'Active');   
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function orderTotal($orderID){
        $this->db->select('code,title,value,sortOrder,languageKey');
        $this->db->from('ec_order_total'); 
        $this->db->where('orderID', $orderID);   
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
     public function orderDetailSuccess($orderID){
        $this->db->select('or.orderID,or.invoiceNo,or.invoicePrefix,orTrn.transaction_uuid,orTrn.reference_number,orTrn.transactionRequestID,orTotal.value as amount,orTrn.paymentStatus,orTrn.message');
        $this->db->from('ec_order or'); 
        $this->db->join('ec_order_transaction orTrn', 'or.orderID=orTrn.orderID', 'inner');
        $this->db->join('ec_order_total orTotal', 'or.orderID=orTotal.orderID AND code="total"', 'inner');
        $this->db->where('or.orderID', $orderID);   
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->row_array();
    }

    public function orderOption($orderID){
        $this->db->select('*');
        $this->db->from('ec_order_option op'); 
        $this->db->where('op.orderID', $orderID);
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    public function orderOptionDetail($orderID,$orderProductID){
        $this->db->select('*');
        $this->db->from('ec_order_option op'); 
        $this->db->join('ec_option_value oD', 'op.productOptionValueID = oD.optionValueID', 'left');
        $this->db->where('op.orderID', $orderID);
        $this->db->where('op.productOrderID', $orderProductID);
        $this->db->order_by('oD.optionID','ASC');
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

}

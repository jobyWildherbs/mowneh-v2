<?php

/**
 * Description of SettingsModel
 *
 * @author wildherbs-user
 */
class SettingsModel extends CI_Model {

    //put your code here
    public function __construct() {
    }

    public function select($sKey, $language = 1, $isAdmin = false) {
        $this->db->select('s.id, s.groupID, s.fieldName, s.fieldKey, s.value, s.isLanguage, s.fieldType, s.fieldOptions, s.validation, s.suggestions, s.fieldPlaceholder, s.relation, s.relatedParent, sg.name, sg.settingsKey, sg.sortOrder, sg.status');
        $this->db->from('ec_settings_group sg');
        $this->db->join('ec_settings s', 's.groupID = sg.id', 'left');
        $this->db->join('ec_user_settings_group_permission usgp', 'usgp.settingsGroupID = sg.id', 'left');
        $this->db->join('ec_user_settings_permission usp', 'usp.settingsID = s.id', 'left');
        $this->db->where('sg.settingsKey', $sKey);
        $this->db->where('relatedParent',0);
        $this->db->where('s.status', 'enable');
        $this->db->where('sg.status', 'enable');
        if(!$isAdmin){
            $this->db->where('usgp.userGroupID',$this->session->userdata('userGroupID'));
            $this->db->where('usp.userGroupID',$this->session->userdata('userGroupID'));
        }
        $resultSet = $this->db->get()->result_array();
//        echo "<pre>";print_r($this->db->last_query());exit;
        $settingArray = array();
        foreach ($resultSet as $result) {
            if ($result['isLanguage'] == 'yes') {
                $this->db->select('value as languageValue');
                $this->db->from('ec_settings_field_details');
                $this->db->where('settingsFieldID', $result['id']);
                $this->db->where('languageID', $language);
                $res = $this->db->get()->row_array();
            }
            if (!empty($res['languageValue'])) {
                $result['languageValue'] = $res['languageValue'];
            }
            $settingArray[] = $result;
        }
        //echo "<pre>";print_r($settingArray); exit;
        return $settingArray;
    }
    public function selectRelated($params){
        $this->db->select('*');
        $this->db->from('ec_settings s');
        $this->db->join('ec_settings_field_details sFD','sFD.settingsFieldID = s.id AND sFD.languageID = '.$params['language'],'left');
        if($params['parentID']){
            $this->db->where('s.relatedParent',$params['parentID']);
        }
        if($params['parentValue']){
            $this->db->where('s.relationValue',$params['parentValue']);
        }
//        if($params['limit']){
//            $this->db->limit($params['limit'],$params['start']);
//        }
        return $this->db->get()->result_array();
    }
    
    public function getSettingValues($key,$language,$isLanguage=false){
        if($isLanguage){
            $this->db->select('sD.value');
        } else {
            $this->db->select('s.value');
        }
        
        $this->db->from('ec_settings s');
        if($isLanguage){
            $this->db->join('ec_settings_field_details sD','s.id = sD.settingsFieldID','left');
        }
        $this->db->where('s.fieldKey',$key);
        $this->db->where('sD.languageID',$language);
        return $this->db->get()->row_array();
    }

}

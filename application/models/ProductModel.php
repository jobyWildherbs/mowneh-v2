<?php

/**
 * Description of FilterModel
 *
 * @author wildherbs-user
 */
class ProductModel extends CI_Model {

    public function __construct() {
        
    }

    public function selectAll($params) {
        $this->db->select('pro.productID,proDesc.name,pro.image,pro.quantity,pro.price,pro.dateAdded,pro.status,(SELECT GROUP_CONCAT(DISTINCT subCatDet.name SEPARATOR ",") FROM ec_product_to_category subProCat INNER JOIN ec_category_detail subCatDet ON (subProCat.categoryID=subCatDet.categoryID AND subCatDet.languageID=' . $params['languageID'] .') WHERE subProCat.productID=pro.productID) AS catName ,'
                . 'manDet.name as manName');
        $this->db->from('ec_product pro');
        $this->db->join('ec_product_description proDesc', 'pro.productID = proDesc.productID AND proDesc.languageID=' . $params['languageID'], 'left');
        $this->db->join('ec_product_to_category proCat', 'pro.productID=proCat.productID', 'left');
        $this->db->join('ec_category_detail catDet', 'proCat.categoryID=catDet.categoryID AND catDet.languageID=' . $params['languageID'], 'left');
        $this->db->join('ec_manufacturer_detail manDet', 'pro.manufacturerID=manDet.manufacturerID AND manDet.languageID=' . $params['languageID'], 'left');

        if (@$params['name']) {
            $this->db->where('proDesc.name LIKE', '%' . $params['name'] . '%');
        }
        if (@$params['type']) {
            $this->db->where('pro.type',  $params['type']);
        }
        if (@$params['manufacturerID']) {
            $this->db->where('pro.manufacturerID', $params['manufacturerID']);
        }
        if (@$params['category']) {
            $this->db->where('proCat.categoryID', $params['category']);
        }
        if (@$params['dateAdded']) {
            $this->db->where('DATE(pro.dateAdded)', $params['dateAdded']);
        }

        if (@$params['fromPrice']) {
            $fromPrice = $params['fromPrice'];
            $this->db->where('pro.price >=', $fromPrice);
        }
        if (@$params['toPrice']) {
            $toPrice = $params['toPrice'];
            $this->db->where('pro.price <=', $toPrice);
        }

        $this->db->where('pro.status!=', 'Deleted');
        $this->db->group_by("proCat.productID");
        $this->db->order_by('pro.dateAdded', 'DESC');
        if (@$params['limit']) {
            $this->db->limit($params['limit'], $params['start']);
        }
        $resultSet = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

    public function selectProduct($productID = '', $languageID = '') {

        $this->db->select('pro.productID, proDesc.languageID as langID ,proDesc.name as productName, pro.sortOrder sortOrder');
        $this->db->from('ec_product pro');
        $this->db->join('ec_product_description proDesc', 'pro.productID = proDesc.productID AND proDesc.languageID=' . $languageID, 'left');
        if ($productID !== '') {
            $this->db->where('proDesc.productID', $productID);
        }
        $this->db->where('pro.status', 'Active');
        $this->db->order_by('pro.sortOrder', 'ASC');
        //$this->db->limit();

        $resultSet = $this->db->get();
       //print_r($this->db->last_query()); exit;

        if ($productID != '') {
            return $resultSet->row_array();
        }
        return $resultSet->result_array();
    }
    
    public function selectProductForBundle($productID = '', $languageID = '') {

        $this->db->select('pro.productID, proDesc.languageID as langID ,proDesc.name as productName, pro.sortOrder sortOrder');
        $this->db->from('ec_product pro');
        $this->db->join('ec_product_description proDesc', 'pro.productID = proDesc.productID AND proDesc.languageID=' . $languageID, 'left');
        if ($productID !== '') {
            $this->db->where('proDesc.productID', $productID);
        }
        $this->db->where('pro.status', 'Active');
        $this->db->order_by('pro.sortOrder', 'ASC');
        //$this->db->limit();
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;

        if ($productID != '') {
            return $resultSet->row_array();
        }
        return $resultSet->result_array();
    }

    public function selectProductDetail($productID = '', $languageID = '', $mode = "front", $customerID='') {
        if ($customerID)
            $wishList = ",IF((SELECT productID FROM ec_customer_wishlist wish WHERE wish.customerID='" . $customerID . "' AND wish.productID=pro.productID), 'YES', 'NO') AS wishList";
        else
            $wishList = "";

        $this->db->select('pro.productID,pro.pageKey,pro.type, pro.model,pro.sku,pro.quantity,pro.image,pro.bannerImage,pro.manufacturerID,pro.shipping,pro.price,pro.weight,pro.length,pro.width,pro.height,pro.minimum
                ,pro.minimum,pro.sortOrder,pro.status,proDet.name,proDet.description,proDet.tag,proDet.highlights,proDet.metaTitle,proDet.metaDescription,proDet.metaKeyword'.$wishList);
        $this->db->from('ec_product pro');
        $this->db->join('ec_product_description proDet', 'proDet.productID = pro.productID AND proDet.languageID="' . $languageID . '"', 'left');
        $this->db->where('pro.productID', $productID);
        if ($mode == "front") {
            $this->db->where('pro.status', 'Active');
        } else {
            $this->db->where('pro.status!=', 'Deleted');
        }
        $this->db->order_by('pro.sortOrder', 'ASC');
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->row_array();
    }

    public function selectProductAttributes($productID = '', $languageID = '') {
        $this->db->select('attr.attributeID,attrDet.name,proAtt.text');
        $this->db->from('ec_product_attribute proAtt');
        $this->db->join('ec_attribute attr', 'attr.attributeID = proAtt.attributeID', 'left');
        $this->db->join('ec_attribute_detail attrDet', 'attrDet.attributeID = attr.attributeID AND attrDet.languageID=' . $languageID, 'left');
        $this->db->where('proAtt.productID', $productID);
        $this->db->where('proAtt.languageID', $languageID);

        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

    public function selectProductDiscountList($productID = '', $languageID = '') {
        $this->db->select('productDiscountID,productID,customer_group_id,quantity,priority,price,type,dateStart,dateEnd');
        $this->db->from('ec_product_discount');
        $this->db->where('productID', $productID);
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

    public function selectProductOptions($productID = '', $languageID = '') {
        $this->db->select('oc.optionID')->from('ec_option_combination oc')->where('oc.status', 'Active')->where('oc.productID', $productID);
        $result = $this->db->get();
        $resultOption = $result->row();

        if(@$resultOption){
            $this->db->select('count(od.optionValueID) as optionCount,po.quantity,o.type,o.showAs,o.optionID,odl.name,oc.required,po.combinationOptionID,po.productID');
            $this->db->from('ec_product_option po');
            $this->db->join('ec_option_combination oc', 'oc.combinationID = po.combinationOptionID', 'left');
            $this->db->join('ec_product_option_value ov', 'ov.productOptionID = po.productOptionID', 'left');
            $this->db->join('ec_option_value_detail od', 'od.optionValueID = ov.optionValueID AND `od`.`optionID` IN ('.$resultOption->optionID.')', 'left');
            $this->db->join('ec_option_detail odl', 'odl.optionID = od.optionID ', 'left');
            $this->db->join('ec_option o', 'o.optionID = od.optionID ', 'left');
            $this->db->where('po.productID', $productID);
            $this->db->where('po.status', 'Active');
            $this->db->where('o.status', 'Active');
            $this->db->where('odl.optionID IN ('.$resultOption->optionID.')');
            $this->db->group_by('od.optionID'); 
            $resultSet = $this->db->get();
            //print_r($this->db->last_query()); exit;
            return $resultSet->result_array();
        }
    }
    public function productOptionDetail($productID = '', $languageID = '') {
        $this->db->select('o.image,od.name,od.optionID,od.optionValueID,op.productOptionID,sum(op.quantity) as quantity');
        $this->db->from('ec_product_option_value po');
        $this->db->join('ec_product_option op', 'po.productOptionID = op.productOptionID');
        $this->db->join('ec_option_value o', 'po.optionValueID = o.optionValueID', 'left');
        $this->db->join('ec_option_value_detail od', 'od.optionValueID = o.optionValueID AND od.languageID=' . $languageID, 'left');
        $this->db->where('po.productID', $productID);
        $this->db->where('op.status', 'Active');
        $this->db->group_by('po.optionValueID'); 
        $this->db->order_by('o.optionID,o.sortOrder', 'Asc');

        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    public function productOptionQuantity($productID){
        $this->db->select('oc.*,')->from('ec_product_option oc')
        ->where('oc.productID', $productID)->where('oc.quantity !=', 0)->where('oc.status', 'Active')->group_by('oc.productOptionID');
        $result = $this->db->get();
        $resultOption = $result->result_array();
        //print_r($resultOption); exit;
        return $resultOption;
    }
     public function productOptionCartDetail($id='' ,$productID = '', $languageID = '') {

        $this->db->select('oD.optionValueID as ID, oD.languageID as langID,opD.name as oName, oD.name as name, o.image, otn.optionID as OID ,otn.type, o.sortOrder as sortOrder');
        $this->db->from('ec_product_option_value po');
        $this->db->join('ec_product_option op', 'op.combinationOptionID = po.optionID and po.productOptionID = op.productOptionID');
        $this->db->join('ec_option_value o', 'po.optionValueID = o.optionValueID', 'left');
        $this->db->join('ec_option_value_detail oD', 'o.optionValueID = oD.optionValueID AND oD.languageID='.$languageID);
        $this->db->join('ec_option_detail opD','oD.optionID = opD.optionID');
        $this->db->join('ec_option otn','otn.optionID = opD.optionID');
        $this->db->where('o.optionValueID', $id);
        $this->db->where('po.productID', $productID);
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    public function selectProductList($languageID = '', $serchFilter = '', $searchManufacturer = '', $searchCategory = '', $minPrice = '', $maxPrice = '', $order = '', $customerID = '',$keyWord='') {
       
        if ($customerID)
            $wishList = ",IF((SELECT productID FROM ec_customer_wishlist wish WHERE wish.customerID='" . $customerID . "' AND wish.productID=pro.productID), 'YES', 'NO') AS wishList";
        else
            $wishList = "";
        $this->db->select('pro.productID,pro.pageKey,pro.image,pro.price,proDet.description,proDet.name,pro.dateAdded' . $wishList);
        $this->db->from('ec_product pro');
        $this->db->join('ec_product_description proDet', 'proDet.productID = pro.productID AND proDet.languageID=' . $languageID, 'left');
        $this->db->join('ec_product_filter proFilter', 'proFilter.productID = pro.productID', 'left');
        $this->db->join('ec_product_to_category proCat', 'proCat.productID = pro.productID', 'left');
        $this->db->where('status', 'Active');
        if ($serchFilter)
            $this->db->where_in('proFilter.filterID', $serchFilter);
        if ($searchManufacturer)
            $this->db->where_in('pro.manufacturerID', $searchManufacturer);
        if ($searchManufacturer)
            $this->db->where_in('pro.manufacturerID', $searchManufacturer);
//        if ($searchCategory)
//            $this->db->where_in('proCat.categoryID', $searchCategory);
        if ($searchCategory)
            $this->db->where('proCat.categoryID', $searchCategory);
        if ($minPrice != '' && $maxPrice != '')
            $this->db->where("price BETWEEN " . $minPrice . " AND " . $maxPrice);
        
        if($keyWord){
            $keywordArr = (explode(",",$keyWord));
            foreach($keywordArr as $key=>$keyVal){
                if($key==0)
                    $this->db->like('proDet.searchText', $keyVal);
                else
                    $this->db->or_like('proDet.searchText', $keyVal);
            }
        }
        
        if ($order != '') {
            if ($order['0'] == 'name')
                $this->db->order_by("proDet.name", $order['1']);
            elseif ($order['0'] == 'price')
                $this->db->order_by("pro.price", $order['1']);
        }else {
            $this->db->order_by("proDet.name", "ASC");
        }

        $this->db->group_by("pro.productID");
        $resultSet = $this->db->get();
        print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

    public function selectManufacturerList($languageID = '') {
        $this->db->select('manu.manufacturerID,manDet.name as manName');
        $this->db->from('ec_manufacturer manu');
        $this->db->join('ec_manufacturer_detail manDet', 'manu.manufacturerID=manDet.manufacturerID AND manDet.languageID=' . $languageID, 'left');
        $this->db->where('manu.status', 'Active');
        $this->db->group_by('manName');
        $resultSet = $this->db->get();
//        print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

    public function selectCategoryList($languageID = '', $category = '') {
        $this->db->select('cat.categoryID,cat.pageKey, cat.parentID,catDet.name,(SELECT name FROM ec_category_detail WHERE categoryID=cat.parentID AND catDet.languageID=' . $languageID . ') as parentName');
        $this->db->from('ec_category cat');
        $this->db->join('ec_category_detail catDet', 'cat.categoryID = catDet.categoryID AND catDet.languageID=' . $languageID, 'left');
        $this->db->join('ec_category parCat', 'parCat.categoryID = cat.categoryID', 'left');
        $this->db->where('cat.status', 'Active'); 
        $this->db->group_by('parentName');
        $this->db->order_by('parentName', 'ASC');
        //$this->db->group_by('manName');
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

    public function selectFilterList($languageID = '',$categoryID='') {
        $this->db->select('fil.filterID, fil.filterGroupID,filDet.name as filterName,filGpDet.name as groupName');
        $this->db->from('ec_filter fil');
        $this->db->join('ec_filter_group filGrp', 'fil.filterGroupID = filGrp.filterGroupID', 'left');
        $this->db->join('ec_filter_detail filDet', 'fil.filterID = filDet.filterID AND filDet.languageID=' . $languageID, 'left');
        $this->db->join('ec_category_filter catFil', 'catFil.filterID = fil.filterGroupID', 'left');
        $this->db->join('ec_filter_group_detail filGpDet', 'fil.filterGroupID = filGpDet.filterGroupID AND filGpDet.languageID=' . $languageID, 'left');
        $this->db->where('filGrp.status', 'Active');
        if(@$categoryID){
        $this->db->where('catFil.categoryID', $categoryID);
        }
        $this->db->group_by('filDet.filterID');

        //$this->db->order_by('filDet.name', 'ASC');
        $this->db->order_by("fil.sortOrder ASC, filGrp.sort_order ASC");
        //$this->db->group_by('manName');
        $resultSet = $this->db->get();
        //print_r($this->db->get()); exit;
        return $resultSet->result_array();
    }

    public function getProductAttributeByProductID($productID = '', $languageID = '') {
        $this->db->select('proAttr.productID, proAttr.attributeID,proAttr.text,attrDet.name,attrGroupDet.name as groupName');
        $this->db->from('ec_product_attribute proAttr');
        $this->db->join('ec_attribute attr', 'attr.attributeID = proAttr.attributeID', 'left');
        $this->db->join('ec_attribute_detail attrDet', 'attrDet.attributeID = attr.attributeID AND attrDet.languageID=' . $languageID, 'left');
        $this->db->join('ec_attribute_group attrGroup', 'attrGroup.attributeGroupID = attr.attributeGroupID', 'left');
        $this->db->join('ec_attribute_group_detail attrGroupDet', 'attrGroupDet.attributeGroupID = attrGroup.attributeGroupID AND attrGroupDet.languageID=' . $languageID, 'left');
        $this->db->where('proAttr.productID=' . $productID);
        $this->db->where('proAttr.languageID=' . $languageID);
        $this->db->where('attr.status="Active"');
        $this->db->order_by('attrGroup.sortOrder', 'ASC');
        $resultSet = $this->db->get();
        return $resultSet->result_array();
    }

    public function reletedProductList($productID = '', $languageID = '',$customerID="") {
  
            if ($customerID)
                $wishList = ",IF((SELECT productID FROM ec_customer_wishlist wish WHERE wish.customerID='" . $customerID . "' AND wish.productID=p.productID), 'YES', 'NO') AS wishList";
            else
                $wishList = "";

        
        $this->db->select('pro.productID,pro.pageKey,pro.quantity,pro.image,pro.price,proDet.description,proDet.name,pro.dateAdded'.$wishList);
        $this->db->from('ec_product pro');
        $this->db->join('ec_product_description proDet', 'proDet.productID = pro.productID AND proDet.languageID=' . $languageID, 'left');
        $this->db->join('ec_product_related releted', 'releted.relatedID = pro.productID', 'left');
        $this->db->where('releted.productID', $productID);
        $this->db->where('releted.relatedID!=', $productID);
        $this->db->where('status', 'Active');
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

    public function productDiscount($productID = '', $customerGroupID=0) {
        $day = date('Y-m-d');
        $result = $this->db->query("SELECT price,quantity,dateStart,dateEnd,type FROM ec_product_discount WHERE productID = '" . (int) $productID . "' AND price > 0 AND customer_group_id = '" . (int) $customerGroupID . "' AND ((dateStart = '0000-00-00' OR dateStart <= '".$day."') AND (dateEnd = '0000-00-00' OR dateEnd >= '".$day."')) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");
        $resultSet = $result->row_array();
        if ($resultSet) {
            $sql = "SELECT sum(quantity) as orderQuantity from ec_order_product op inner join ec_order o on (op.orderID=o.orderID) where op.productID = " . $productID;
            if ($resultSet['dateEnd'] != '0000-00-00') {
                $sql .= " and o.dateAdded < '" . $resultSet['dateEnd'] . "'";
            }
            if ($resultSet['dateStart'] != '0000-00-00') {
                $sql .= " and o.dateAdded > '" . $resultSet['dateStart'] . "'";
            }
            $sql .= " group by op.productID";
            $result = $this->db->query($sql)->row_array();
            if ($result) {
                if ($resultSet['quantity'] > $result['orderQuantity']) {
                    return $resultSet;
                }else{
                    return ;
                }
            } else {
                return $resultSet;
            }
        }
        return $resultSet;
    }
    
    public function productDiscountOrderQuantity($productID = '',$customerGroupID=0) {
        $day = date('Y-m-d');
        $result = $this->db->query("SELECT price,quantity,dateStart,dateEnd,type FROM ec_product_discount WHERE productID = '" . (int) $productID . "' AND price > 0 AND customer_group_id = '" . (int) $customerGroupID . "' AND ((dateStart = '0000-00-00' OR dateStart <= '".$day."') AND (dateEnd = '0000-00-00' OR dateEnd >= '".$day."')) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");
        $resultSet = $result->row_array();
        if ($resultSet) {
            $sql = "SELECT sum(quantity) as orderQuantity from ec_order_product op inner join ec_order o on (op.orderID=o.orderID) where op.productID = " . $productID;
            if ($resultSet['dateEnd'] != '0000-00-00') {
                $sql .= " and o.dateAdded < '" . $resultSet['dateEnd'] . "'";
            }
            if ($resultSet['dateStart'] != '0000-00-00') {
                $sql .= " and o.dateAdded > '" . $resultSet['dateStart'] . "'";
            }
            $sql .= " group by op.productID";
            $result = $this->db->query($sql)->row_array();
            
                return $result;
               
                    
                
            
        }
       // return $resultSet;
    }
    public function selectCategoryDetail($catID) {
        $this->db->select('proAttr.productID, proAttr.attributeID,proAttr.text,attrDet.name,attrGroupDet.name as groupName');
        $this->db->from('ec_product_attribute proAttr');
        $this->db->join('ec_attribute attr', 'attr.attributeID = proAttr.attributeID', 'left');
    }

    public function getProducts($data,$paginationCount='') {
//        echo "<pre>"; print_r($data); exit;
        if($paginationCount){
            $sql = "select COUNT(p.productID) as productCount";
        }else{
            
        $customerID = $data['customerID'];
            if ($customerID)
                $wishList = ",IF((SELECT productID FROM ec_customer_wishlist wish WHERE wish.customerID='" . $customerID . "' AND wish.productID=p.productID), 'YES', 'NO') AS wishList";
            else
                $wishList = "";
            $sql = "select p.productID,p.pageKey,p.type,p.weight,p.quantity,p.image,p.price,pd.description,pd.name,p.dateAdded".$wishList;
        }
        if (!empty($data['categoryID'])) {
            if (!empty($data['subCategory'])) {
                $sql .= " FROM ec_category_path cp LEFT JOIN ec_product_to_category p2c ON (cp.categoryID = p2c.categoryID)";
            } else {
                $sql .= " FROM ec_product_to_category p2c";
            }

            if (!empty($data['filter'])) {
                $sql .= " LEFT JOIN ec_product_filter pf ON (p2c.productID = pf.productID) LEFT JOIN ec_product p ON (pf.productID = p.productID)";
            } else {
                $sql .= " LEFT JOIN ec_product p ON (p2c.productID = p.productID)";
            }
        } else {
            
            $sql .= " FROM ec_product p";
            
            if (!empty($data['filter'])) {
                $sql .= " LEFT JOIN ec_product_filter pf ON (p.productID = pf.productID)";
            }
        }

        $sql .= " LEFT JOIN ec_product_description pd ON (p.productID = pd.productID) WHERE pd.languageID = '" . (int) $data['languageID'] . "' AND p.status = 'Active' AND p.dateAvailable <= NOW() ";
        
        if (!empty($data['categoryID'])) {
            if (!empty($data['subCategory'])) {
                $sql .= " AND cp.pathID = '" . (int) $data['categoryID'] . "'";
            } else {
                $sql .= " AND p2c.categoryID = '" . (int) $data['categoryID'] . "'";
            }

            if (!empty($data['filter'])) {
                $implode = array();

                $filters = explode('~', $data['filter']);

                foreach ($filters as $filter_id) {
                    $implode[] = (int) $filter_id;
                }

                $sql .= " AND pf.filterID IN (" . implode(',', $implode) . ")";
            }
        }else{
            if (!empty($data['filter'])) {
                $implode = array();

                $filters = explode('~', $data['filter']);

                foreach ($filters as $filter_id) {
                    $implode[] = (int) $filter_id;
                }

                $sql .= " AND pf.filterID IN (" . implode(',', $implode) . ")";
            }
        }
        if (!empty($data['brand'])) {
            
            $brands = explode('~', $data['brand']);
            $implode = array();
            foreach ($brands as $bandID) {
                $implode[] = (int) $bandID;
            }
            $sql .= " AND p.manufacturerID IN (" . implode(',', $implode) . ")";
        }
        
        if(!empty($data['startPrice']) && !empty($data['endPrice'])){
            $sql .= " AND p.price BETWEEN " . $data['startPrice'] . " AND " . $data['endPrice'];
        }elseif(!empty($data['endPrice'])){
            //echo $data['endPrice'];exit;
            $sql .= " AND p.price <" . $data['endPrice'];
        }
        if(!empty($data['searchKeyWord'])){
            $keywordArr = (explode("~",$data['searchKeyWord']));
            foreach($keywordArr as $key=>$keyVal){
                if($key==0)
                    $sql .= " AND `pd`.`searchText` LIKE '%".$keyVal."%' ESCAPE '!'";
                else
                    $sql .= " OR `pd`.`searchText` LIKE '%".$keyVal."%' ESCAPE '!'";
            }
        }
        $sql .= " GROUP BY p.productID";
       
        if(!empty($data['order'])){
           $order = explode("~", $data['order']) ;
           if($order[0]=='price'){
               $sort = 'p.price';
           }else{
               $sort = 'pd.name';
           }
           $sql .= " ORDER BY ".$sort." ".$order[1]; 
        }else{
            $sql .= " ORDER BY p.sortOrder asc";
        }
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            if($paginationCount==''){
                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }
        }
       // echo $sql; exit;
//        if($paginationCount==''){
            $result = $this->db->query($sql)->result_array();
//        }else{
            //$result = $this->db->query($sql)->row_array();
//        }
        //echo $this->db->last_query(); exit;
        return $result;
    }
    
    
    public function getProductsWithOutCategory($data,$paginationCount=''){
        if($paginationCount){
            $sql = "select COUNT(p.productID) as productCount";
        }else{
            
        $customerID = $data['customerID'];
            if ($customerID)
                $wishList = ",IF((SELECT productID FROM ec_customer_wishlist wish WHERE wish.customerID='" . $customerID . "' AND wish.productID=p.productID), 'YES', 'NO') AS wishList";
            else
                $wishList = "";
            $sql = "select p.productID,p.pageKey,p.type,p.quantity,p.image,p.weight,p.price,pd.description,pd.name,p.dateAdded".$wishList;
        }
        
            $sql .= " FROM ec_product p";
        
        if (!empty($data['filter'])) {
                $sql .= " LEFT JOIN ec_product_filter pf ON (p.productID = pf.productID)";
            } 

        $sql .= " LEFT JOIN ec_product_description pd ON (p.productID = pd.productID) WHERE pd.languageID = '" . (int) $data['languageID'] . "' AND p.status = 'Active' AND p.dateAvailable <= NOW() ";

     

            if (!empty($data['filter'])) {
                $implode = array();

                $filters = explode('~', $data['filter']);

                foreach ($filters as $filter_id) {
                    $implode[] = (int) $filter_id;
                }

                $sql .= " AND pf.filterID IN (" . implode(',', $implode) . ")";
            }
        if (!empty($data['brand'])) {
            
            $brands = explode('~', $data['brand']);
            $implode = array();
            foreach ($brands as $bandID) {
                $implode[] = (int) $bandID;
            }
            $sql .= " AND p.manufacturerID IN (" . implode(',', $implode) . ")";
        }
        if(!empty($data['startPrice']) && !empty($data['endPrice'])){
            $sql .= " AND p.price BETWEEN " . $data['startPrice'] . " AND " . $data['endPrice'];
        }
        if(!empty($data['searchKeyWord'])){
            $keywordArr = (explode("~",$data['searchKeyWord']));
            foreach($keywordArr as $key=>$keyVal){
                if($key==0)
                    $sql .= " AND `pd`.`searchText` LIKE '%".$keyVal."%' ESCAPE '!'";
                else
                    $sql .= " OR `pd`.`searchText` LIKE '%".$keyVal."%' ESCAPE '!'";
            }
        }
        $sql .= " GROUP BY p.productID";
       
        if(!empty($data['order'])){
           $order = explode("~", $data['order']) ;
           if($order[0]=='price'){
               $sort = 'p.price';
           }else{
               $sort = 'pd.name';
           }
           $sql .= " ORDER BY ".$sort." ".$order[1]; 
        }else{
            $sql .= " ORDER BY p.sortOrder asc";
        }
        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }
            if($paginationCount==''){
                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }
        }
        if($paginationCount==''){
            $result = $this->db->query($sql)->result_array();
        }else{
            $result = $this->db->query($sql)->row_array();
        }
        //echo $this->db->last_query(); exit;
        return $result;
    
    }
    public function selectCategoryListInProductList($languageID = '', $category = '') {
        $this->db->select('cat.categoryID,cat.pageKey, cat.parentID,catDet.name');
        $this->db->from('ec_category cat');
        $this->db->join('ec_category_detail catDet', 'cat.categoryID = catDet.categoryID AND catDet.languageID=' . $languageID, 'left');
        $this->db->join('ec_category parCat', 'parCat.categoryID = cat.categoryID', 'left');
        $this->db->where('cat.status', 'Active');
        if ($category)
            $this->db->where('cat.parentID', $category);
        else
            $this->db->where('cat.parentID', '0');
        //$this->db->group_by('manName');
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }

    public function getCategories($parentID=0,$languageID=1){
        $this->db->select('c.*,cd.name as categoryName,cd.description');
        $this->db->from('ec_category c');
        $this->db->join('ec_category_detail cd', 'c.categoryID = cd.categoryID AND cd.languageID=' . $languageID);
        //$this->db->join('ec_product_related releted', 'releted.relatedID = pro.productID', 'left');
        $this->db->where('c.parentID', $parentID);
        $this->db->where('c.status', 'Active');
        $this->db->order_by('c.sortOrder', 'ASC');
        $resultSet = $this->db->get()->result_array();
        //echo $this->db->last_query();exit;
        return $resultSet;
    }
    
    public function updateSearchText($productID = 0,$languageID = 1){
        if($productID>0){
            $this->db->select('p.model,pd.name as productName,pd.description as productDescription,pd.highlights,GROUP_CONCAT(cd.name) as categoryName,GROUP_CONCAT(cd.description) as categoryDescription,md.name as brandName,GROUP_CONCAT(pa.text) as attributeName,GROUP_CONCAT(fd.name) as filterName');
            $this->db->from('ec_product p');
            $this->db->join('ec_product_description pd', 'pd.productID = p.productID AND pd.languageID=' . $languageID, 'left');
            $this->db->join('ec_product_to_category pdc', 'pdc.productID = p.productID', 'left');
            $this->db->join('ec_category_detail cd', 'cd.categoryID = pdc.categoryID', 'left');
            $this->db->join('ec_product_attribute pa', 'pa.productID = p.productID AND pa.languageID=' . $languageID, 'left');
            $this->db->join('ec_manufacturer_detail md', 'md.manufacturerID = p.manufacturerID AND md.languageID=' . $languageID, 'left');
            $this->db->join('ec_product_filter pf', 'pf.productID = p.productID', 'left');
            $this->db->join('ec_filter_detail fd', 'fd.filterID = pf.filterID AND fd.languageID=' . $languageID, 'left');
            $this->db->where('p.productID', $productID);
            $this->db->group_by('p.productID');
            $resultSet = $this->db->get()->row_array();
            $resultSet = array_values($resultSet);
            
            $text = "";
            foreach ($resultSet as $result){
                $text = $text."---".$result;
            }
            $this->db->where('productID', $productID);
            $this->db->where('languageID', $languageID);
            $this->db->update('ec_product_description', array('searchText' => $text));
            //echo "<pre>";print_r($resultSet);exit;
        }
    }
    
    public function productExport($params) {
       // echo $params['fields'];exit;
        $languageID =   $params['languageID'];
        if(strpos($params['fields'], 'product_to_category.category') !== false){
            $categotyQuery    =   ",(SELECT GROUP_CONCAT(DISTINCT subCatDet.name SEPARATOR ',') FROM ec_product_to_category subProCat INNER JOIN ec_category_detail subCatDet ON (subProCat.categoryID=subCatDet.categoryID AND subCatDet.languageID='".$languageID."') WHERE subProCat.productID=product.productID) AS catName";
            $params['fields']    =   str_replace(',product_to_category.category', $categotyQuery, $params['fields']);
        }
        if(strpos($params['fields'], 'product_related.relatedProduct') !== false){
            $relatedQuery    =   ",(SELECT GROUP_CONCAT(DISTINCT proDet.name SEPARATOR ',') FROM ec_product_related relPro INNER JOIN ec_product_description proDet ON (proDet.productID=relPro.productID AND proDet.languageID='".$languageID."') WHERE proDet.productID=product.productID) AS relatedProduct";
            $params['fields']    =   str_replace(',product_related.relatedProduct', $relatedQuery, $params['fields']);
        }
        if(strpos($params['fields'], 'product.manufacturerID') !== false){
            $brandQuery    =   ",(SELECT name FROM ec_manufacturer_detail WHERE manufacturerID=product.manufacturerID AND languageID='".$languageID."') AS brand";
            $params['fields']    =   str_replace(',product.manufacturerID', $brandQuery, $params['fields']);
        }
        if(strpos($params['fields'], 'product_discount.price') !== false){
            $discountPriceQuery    =   ",product_discount.price as discountPrice";
            $params['fields']    =   str_replace(',product_discount.price', $discountPriceQuery, $params['fields']);
        }
       // echo $params['fields'];exit;
        $this->db->select($params['fields']);
        $this->db->from('ec_product product');
        $this->db->join('ec_product_description product_description', 'product.productID = product_description.productID AND product_description.languageID=' . $params['languageID'], 'left');
        $this->db->join('ec_product_to_category proCat', 'product.productID=proCat.productID', 'left');
        $this->db->join('ec_category_detail catDet', 'proCat.categoryID=catDet.categoryID AND catDet.languageID=' . $params['languageID'], 'left');
        $this->db->join('ec_manufacturer_detail manDet', 'product.manufacturerID=manDet.manufacturerID AND manDet.languageID=' . $params['languageID'], 'left');
        $this->db->join('ec_product_discount product_discount', 'product.productID=product_discount.productID', 'left');


        $this->db->where('product.status=', 'Active');
        $this->db->order_by('product.productID', 'ASC');
        if (@$params['limit']) {
            $this->db->limit($params['limit'], $params['start']);
        }
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function createPageKey($value){
        $pageKey    = preg_replace("/[^a-zA-Z^0-9]+/", "-", trim(strtolower($value))); 
        //echo $pageKey;exit;
        $this->db->select('pageKey');
        $this->db->from('ec_product');
        $this->db->where('pageKey', $pageKey);
        $this->db->where('status!=', 'Deleted');
        $resultSet  = $this->db->get();
        $checkVal   =   $resultSet->row_array();
        if(!empty($checkVal)){
           $pageKey =   $pageKey.'-'.uniqid();
        }
        return $pageKey;
    }
    
     public function importData($data,$languageID){
         //echo "<pre>"; print_r($data);exit;
        $this->load->library('Import');
        $this->load->model('GeneralModel', 'generalModel');
        $rowCount   =   2;
        foreach($data as $key=>$dataVal){
           
            $uploadErr  =   0;
            if(@$dataVal['model'])
                $product['model']                       = $dataVal['model'];
            if(@$dataVal['sku'])
                $product['sku']                         = $dataVal['sku'];
            if(@$dataVal['quantity']){
                if (is_numeric($dataVal['quantity'])) 
                {
                   $product['quantity']                    = $dataVal['quantity'];
                } 
                else 
                { 
                    $uploadErr  =   1; 
                    $errorMsg[]   =   $this->import->setRowNumericMessage('product','quantity',$dataVal['quantity'],$rowCount); //Set Error message for each row
                    
                }
                
            }
            if(@$dataVal['price']){
                if (is_numeric($dataVal['price'])) 
                {
                   $product['price']                       = $dataVal['price'];
                } 
                else 
                { 
                    $uploadErr  =   1; 
                    $errorMsg[]   =   $this->import->setRowNumericMessage('product','price',$dataVal['price'],$rowCount); //Set Error message for each row
                    
                }
                
            }
            if(@$dataVal['manufacturer']){
                $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($dataVal['manufacturer']))); //Creating Import key
                $manufacturerID =   "";
                $manufacturerID   =   $this->generalModel->getFieldValue('manufacturerID', 'ec_manufacturer','importKey="'.$importKey.'" AND status="Active"');
                if($manufacturerID==''){
                    $uploadErr  =   1; 
                    $errorMsg[]   =   $this->import->setRowMessage('product','manufacturer',$dataVal['manufacturer'],$rowCount); //Set Error message for each row
                }
                $product['manufacturerID']              = $manufacturerID;
            }
            $attribute  =   "";
            
            if(@$dataVal['attribute']){
                $attribute  =   explode("~",$dataVal['attribute']);
                
                foreach($attribute as $attributeLabel){  
                    if($attributeLabel!=''){
                        $attrGroupContent   =   explode("->",$attributeLabel);
                        $attributeGroup     =   $attrGroupContent[0];
                        $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($attributeGroup)));
                       
                        $attributeGroupID   =   $this->generalModel->getFieldValue('attributeGroupID', 'ec_attribute_group','importKey="'.$importKey.'" AND status="Active"');
                     
                        if($attributeGroupID){
                            if(@$attrGroupContent[1])
                                $attributeContent  =   explode("*",$attrGroupContent[1]);

                            $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($attributeContent[0])));
                            $attributeID   =   $this->generalModel->getFieldValue('attributeID', 'ec_attribute','importKey="'.$importKey.'" AND attributeGroupID="'.$attributeGroupID.'" AND status="Active"');

                            if($attributeID==""){
                                $uploadErr  =   1;
                                    $errorMsg[]   =   $this->import->setRowMessage('product','attribute',$attributeContent[0],$rowCount);
                            }
                        }
                        else{
                            $uploadErr  =   1;
                            
                            $errorMsg[] =   $this->import->setFilterGroupNotExistMessage('product',$attributeGroup,$rowCount);
                        }
                    }
                }
            }
            if(@$dataVal['dateAvailable'])
                $product['dateAvailable']               = date('Y-m-d' ,strtotime($dataVal['dateAvailable']));
            if(@$dataVal['weight'])
                $product['weight']                      = $dataVal['weight'];
            if(@$dataVal['length'])
                $product['length']                      = $dataVal['length'];
            if(@$dataVal['width'])
                $product['width']                       = $dataVal['width'];
            if(@$dataVal['height'])
                $product['height']                      = $dataVal['height'];
            if(@$dataVal['minimum'])
                $product['minimum']                     = $dataVal['minimum'];
            if(@$dataVal['SortOrder'])
                $product['sortOrder']                      = $dataVal['SortOrder'];
            
            if(@$dataVal['sku'])
                $product['sku']                         = $dataVal['sku'];
            
            if(@$dataVal['image']){
                $image  =   explode("~",$dataVal['image']);
                $product['image']                      = $image[0];
                unset($image[0]);
             
                $additionalImage   = $image;
            }
            
            if(@$dataVal['name']){
                $productDet['name']                     = $dataVal['name'];
                    $pageKey    =   $this->createPageKey($dataVal['name']);
                    $product['pageKey']  =    $pageKey;
                    
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($dataVal['name'])));
                    $product['importKey'] =   $importKey;
                    $productID   =   $this->generalModel->getFieldValue('productID', 'ec_product','importKey="'.$importKey.'" AND model="'.$dataVal['model'].'" AND status!="Deleted"');
                    
                        if($productID!=""){
                            $uploadErr  =   1;
                                $errorMsg[]   =   $this->import->setProductExistMessage('product',$dataVal['name'],$rowCount);
                        }
            }
            if(@$dataVal['name_ar'])
                $productDet_ar['name']                  = $dataVal['name_ar'];
              
            if(@$dataVal['description'])
                $productDet['description']              = $dataVal['description'];
            
            if(@$dataVal['description_ar'])
                $productDet_ar['description']           = $dataVal['description_ar'];
            
            if(@$dataVal['highlights'])
                $productDet['highlights']               = $dataVal['highlights'];
            
            if(@$dataVal['highlights_ar'])
                $productDet_ar['highlights']            = $dataVal['highlights_ar'];
            
            if(@$dataVal['description_ar'])
                $productDet_ar['description']           = $dataVal['description_ar'];
            
            if(@$dataVal['tag'])
                $productDet['tag']                      = $dataVal['tag'];
            
            if(@$dataVal['tag_ar'])
                $productDet_ar['tag']                   = $dataVal['tag_ar'];
            
            if(@$dataVal['metaTitle'])
                $productDet['metaTitle']                = $dataVal['metaTitle'];
            
            if(@$dataVal['metaTitle_ar'])
                $productDet_ar['metaTitle']             = $dataVal['metaTitle_ar'];
            
            if(@$dataVal['metaDescription'])
                $productDet['metaDescription']           = $dataVal['metaDescription'];
            
            if(@$dataVal['metaDescription_ar'])
                $productDet_ar['metaDescription']        = $dataVal['metaDescription_ar'];
            
             if(@$dataVal['metaKeyword'])
                $productDet['metaKeyword']              = $dataVal['metaKeyword'];
            
            if(@$dataVal['metaKeyword_ar'])
                $productDet_ar['metaKeyword']        = $dataVal['metaKeyword_ar'];
            
            if(@$dataVal['category']){
                $category  =   explode("~",$dataVal['category']);
                $filterCategory =   "";
                $catIDArr   =   array();
                $catcount   =   1;
                    foreach($category as $catVal){
                        $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($catVal)));
                        $categoryID   =   $this->generalModel->getFieldValue('categoryID', 'ec_category','importKey="'.$importKey.'" AND status="Active"');
                        //echo $this->db->last_query()."</br>";
                        $catIDArr[] =   $categoryID;
                        if($categoryID==''){
                            $uploadErr  =   1;
                            $errorMsg[]   =   $this->import->setRowMessage('product','category',$catVal,$rowCount);
                        }
                        $filterCategory .=  $catVal;
                       if(count($category)!=$catcount){
                            $filterCategory .=  ",";
                       }
                        $catcount++;
                    } 
                $category['category']                   = $dataVal['category'];
            }
//            if(@$dataVal['relatedProduct']){
//                $relatedProduct     =   explode("~",$dataVal['relatedProduct']);
//                foreach($relatedProduct as $relPro){
//                    $importKey          =   trim(preg_replace("/[^a-zA-Z]+/", "-", strtolower($relPro)));
//                    $productID          =   $this->generalModel->getFieldValue('productID', 'ec_product','importKey="'.$importKey.'" AND status="Active"');
//                    if($productID==''){
//                                $uploadErr  =   1;
//                                $errorMsg[]   =   $this->import->setRowMessage('product','relatedProduct',$relPro,$rowCount);
//                    }
//                }
//                $relatedProduct['relatedProduct']            = $dataVal['relatedProduct'];
//            }
            if(@$dataVal['filter']){
                $filter     =   explode("~",$dataVal['filter']);
                foreach($filter as $filterData){
                    $importKey         =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($filterData)));
                    $filterID          =   $this->generalModel->getFieldValue('filterID', 'ec_filter','importKey="'.$importKey.'"');
                    $filterGroupID     =   $this->generalModel->getFieldValue('filterGroupID', 'ec_filter','filterID="'.$filterID.'"');
                    $filterGroup       =   $this->generalModel->getTableValue('name', 'ec_filter_group_detail','filterGroupID="'.$filterGroupID.'" AND languageID="1"');
                    $filterGroupName   =   $filterGroup['name'];
                    
                    
                    if($filterID==''){
                                $uploadErr  =   1;
                                $errorMsg[]   =   $this->import->setRowMessage('product','filter',$filterData,$rowCount);
                    }else{
                        if($catIDArr){
                            $filtercheckReturn   =   array();
                            foreach($catIDArr as $catIDval){
                               $filterCheck       =   $this->generalModel->getFieldValue('categoryID', 'ec_category_filter','categoryID="'.$catIDval.'" AND filterID="'.$filterGroupID.'"');
                                if($filterCheck)
                                    $filtercheckReturn[] =  $filterCheck; 
                            }
                        }
                        if(empty($filtercheckReturn)){
                            $uploadErr  =   1;
                                $errorMsg[]   =   $this->import->setCategoryFilterMessage($filterData,$filterGroupName,$rowCount,$filterCategory);
                        }
                        
                    }
                }
              
                $filter['filter']            = $dataVal['filter'];
            }
            if(@$dataVal['discPrice'])
                $discountProduct['price']               = $dataVal['discPrice'];
            if(@$dataVal['discQuantity'])
                $discountProduct['quantity']            = $dataVal['discQuantity'];
            if(@$dataVal['discPriority'])
                $discountProduct['priority']            = $dataVal['discPriority'];
            if(@$dataVal['discDateStart']){
                if($this->import->validateDate($dataVal['discDateStart'])){
                    
                    $discountProduct['dateStart']           = date("Y-m-d", strtotime($dataVal['discDateStart']));
                }else{
                    
                    $uploadErr  =   1;
                    $errorMsg[]   =   $this->import->setRowDateMessage('product','discDateStart',$dataVal['discDateStart'],$rowCount);
                }
            }
            if(@$dataVal['discDateEnd']){
                
                if($this->import->validateDate($dataVal['discDateEnd'])){
                   $discountProduct['dateEnd']             = date("Y-m-d", strtotime($dataVal['discDateEnd']));  
                }else{
                    $uploadErr  =   1;
                    $errorMsg[]   =   $this->import->setRowDateMessage('product','discDateEnd',$dataVal['discDateEnd'],$rowCount);
                }
            }
             if(@$dataVal['customer_group_id'])
                $discountProduct['customer_group_id']   = $dataVal['customer_group_id'];
             
             
            
            $product['dateAdded']             = date('Y-m-d H:i');
            
            //echo "<pre>"; print_r($discountProduct); exit;
            //Data adding session
           
            if($uploadErr==0){   
                if(!empty($product)){
                    //echo "<pre>"; print_r($product); 
                    $this->db->insert('ec_product', $product);
                    $productID =   $this->db->insert_id();
                    
                    if(@$dataVal['relatedProduct']){
                        $relatedProduct['relatedProduct'][]     =   $dataVal['relatedProduct'];
                        $relatedProduct['productID'][]         =   $productID;
                    }
                    
                }
                
                if(!empty($productDet)){
                    $productDet['languageID']    =   1;
                    $productDet['productID']     = $productID;
                    $this->db->insert('ec_product_description', $productDet);
                }
                
                if(!empty($productDet_ar)){
                    $productDet_ar['languageID']    =   2;
                    $productDet_ar['productID']     = $productID;
                    $this->db->insert('ec_product_description', $productDet_ar);
                }
                
                if(!empty($discountProduct)){
                    $discountProduct['productID']    =   $productID;
                    $this->db->insert('ec_product_discount', $discountProduct);
                    //echo $this->db->last_query();exit;
                }
                
                if(!empty($category)){ 
                    $category  =   explode("~",$category['category']);
                    foreach($category as $catVal){
                        $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($catVal)));
                        $categoryID   =   $this->generalModel->getFieldValue('categoryID', 'ec_category','importKey="'.$importKey.'" AND status="Active"');
                        $productCategory    =   array();
                        $productCategory['productID']    =   $productID;
                        $productCategory['categoryID']   =   $categoryID;
                        $this->db->insert('ec_product_to_category', $productCategory);
                    } 
                }
                
                
                        
                 if(!empty($filter)){
                    $filter  =   explode("~",$filter['filter']);
                   
                    foreach($filter as $productFilter){
                        $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($productFilter)));
                        $filterID   =   $this->generalModel->getFieldValue('filterID', 'ec_filter','importKey="'.$importKey.'"');
                        $filterData    =   array();
                        $filterData['productID']    =   $productID;
                        $filterData['filterID']     =   $filterID;
                        $this->db->insert('ec_product_filter', $filterData);
                    } 
                }
                
                if($attribute){
                    
                    $attribute  =   explode("~",$dataVal['attribute']);
                    
                    foreach($attribute as $attributeLabel){
                        
                        $attrGroupContent   =   explode("->",$attributeLabel);
                        if(!empty($attrGroupContent)){
                            $attributeGroup     =   $attrGroupContent[0];
                            $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($attributeGroup)));
                            $attributeGroupID   =   $this->generalModel->getFieldValue('attributeGroupID', 'ec_attribute_group','importKey="'.$importKey.'" AND status="Active"');

    //                        echo $rowCount."</br>";
    //                        echo "<pre>"; print_r($attrGroupContent); echo "</pre>";
                            if(@$attrGroupContent[1]){
                                $attributeContent  =   explode("*",$attrGroupContent[1]);

                                if($attrGroupContent[1]!=''){
                                    if(@$attributeContent[1]){
                                        $attributeLang  =   explode("|",@$attributeContent[1]);
                                    }


                                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($attributeContent[0])));
                                    $attributeID   =   $this->generalModel->getFieldValue('attributeID', 'ec_attribute','importKey="'.$importKey.'" AND attributeGroupID="'.$attributeGroupID.'" AND status="Active"');
                                   //echo "<pre>"; print_r($attributeLang); echo "</pre>";
                                    if(@$attributeLang[0]){
                                        $productAttr_eng['productID']       =   $productID;
                                        $productAttr_eng['attributeID']     =   $attributeID;
                                        $productAttr_eng['languageID']      =   1;
                                        $productAttr_eng['text']            =   $attributeLang[0];
                                        $this->db->insert('ec_product_attribute', $productAttr_eng);
                                    }

                                    if(@$attributeLang[1]){
                                        $productAttr_ar['productID']       =   $productID;
                                        $productAttr_ar['attributeID']     =   $attributeID;
                                        $productAttr_ar['languageID']      =   2;
                                        $productAttr_ar['text']            =   $attributeLang[1];
                                        $this->db->insert('ec_product_attribute', $productAttr_ar);
                                    }
                                }
                            }
                        }
                    }
                }
                
                if($additionalImage){
                    $sortOrder  =   1;
                    foreach($additionalImage as $additionalImageVal){ 
                        $image                 =   array();
                        $image['productID']    =   $productID;
                        $image['image']        =   $additionalImageVal; 
                        $image['sortOrder']    =   $sortOrder;
                        $this->db->insert('ec_product_image', $image);
                        $sortOrder++;
                    } 
                }
                $this->updateSearchText($productID, $languageID);
            }
            $rowCount++;
        }
        if(!empty($relatedProduct)){
            foreach($relatedProduct['relatedProduct'] as $relKey=>$relData){
               $related  =   explode("~",$relData);
                   
                    foreach($related as $relatedProductData){
                        $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($relatedProductData)));
                        $relatedproductID   =   $this->generalModel->getFieldValue('productID', 'ec_product','importKey="'.$importKey.'" AND status="Active"');
                        $related    =   array();
                        if($relatedproductID>0){
                            $related['productID']    =   $relatedProduct['productID'][$relKey];
                            $related['relatedID']    =   $relatedproductID;
                            $this->db->insert('ec_product_related', $related);
                        }
                    }
            }
            
             
        }
        if(!empty($errorMsg)){
            return $errorMsg;
        }
        
       return true;
    }
    
    public function selectProductDetailReport($params) {
        //echo "<pre>"; print_r($params); exit;
        $languageID =   $params['languageID'];
        $field  =   "";
        if($params['ordered'])
            $field  .=   ",sum(orderPro.quantity) AS orderedQuentity";
        if($params['manufacturerID'])
            $field  .=   ",manufaDet.name as manufacturer";
        $this->db->select('pro.productID,pro.pageKey,pro.weight,proDet.name,pro.quantity,pro.model'.$field);
        $this->db->from('ec_product pro');
        $this->db->join('ec_product_description proDet', 'proDet.productID = pro.productID AND proDet.languageID=' . $languageID, 'left');
        if($params['ordered'])
            $this->db->join('ec_order_product orderPro', 'orderPro.productID = pro.productID', 'inner');
        if($params['manufacturerID']){
            $this->db->join('ec_manufacturer_detail manufaDet', 'manufaDet.manufacturerID = pro.manufacturerID AND manufaDet.languageID=' . $languageID, 'left');
        }
        $this->db->where('pro.status', 'Active');
        
        if($params['outofstock']=='yes')
            $this->db->where('pro.quantity', '0');
            
        if($params['manufacturerID'])
            $this->db->where('pro.manufacturerID', $params['manufacturerID']);
        if($params['fromDate'])
            $this->db->where('pro.dateAdded >=', $params['fromDate']);
        if($params['toDate'])
            $this->db->where('pro.dateAdded <=', $params['toDate']);
        if($params['ordered']=='mostOrder'){   
            $this->db->group_by('orderPro.productID');
            $this->db->order_by('orderedQuentity', 'DESC');
        }elseif($params['ordered']=='lessOrder'){
            $this->db->group_by('orderPro.productID');
            $this->db->order_by('orderedQuentity', 'ASC');
        }
        if (@$params['limit']) {
            $this->db->limit($params['limit'], $params['start']);
        }
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function getBundleProducts($bundleProductID,$languageID) {
        $this->db->select('p.productID,pd.name,p.pageKey,p.model,p.type, p.image, p.quantity as remaining, p.status,pb.quantity');
        $this->db->from('ec_product p');
        $this->db->join('ec_product_description pd', 'p.productID = pd.productID AND pd.languageID='.$languageID, 'left');
        $this->db->join('ec_product_bundle pb', 'p.productID = pb.bundleProductID', 'inner');
        $this->db->where('p.productID', $bundleProductID);
        $this->db->where('p.status', "Active");
        $result = $this->db->get();
        return $result->row_array();
    }
    public function getBundleProductss($bundleProductID,$languageID) {
        $this->db->select('p.productID,pd.name,p.pageKey,p.model,p.type, p.image, p.quantity as remaining, p.status,pb.quantity');
        $this->db->from('ec_product p');
        $this->db->join('ec_product_description pd', 'p.productID = pd.productID AND pd.languageID='.$languageID, 'left');
        $this->db->join('ec_product_bundle pb', 'p.productID = pb.bundleProductID', 'inner');
        $this->db->where('p.productID', $bundleProductID);
        $this->db->where('p.status', "Active");
        $result = $this->db->get();
        return $result->row_array();
    }
     public function getProductOptionsManage($combinationID="",$productID="",$languageID=1){
        $this->db->select('proOpt.productOptionID,proOpt.quantity,proOpt.priceType,proOpt.price');
        $this->db->from('ec_product_option as proOpt');
        $this->db->where('proOpt.productID', $productID);
        $this->db->where('proOpt.combinationOptionID', $combinationID);
        $this->db->where('proOpt.status', 'Active');
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
     
    public function getProductOptions($productID,$languageID=1){
        $this->db->select('productID,combinationID,required');
        $this->db->from('ec_option_combination');
        $this->db->where('productID', $productID);
         $this->db->where('status', 'Active');
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    } 
    
     public function getProductCombinationName($combinationID,$languageID=1){
        $this->db->select('optionID');
        $this->db->from('ec_option_combination');
        $this->db->where('combinationID', $combinationID);
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        $combination    =    $resultSet->row_array();
        $combinationOption  =   explode(",",$combination['optionID']);
        
            $this->db->select('GROUP_CONCAT(name SEPARATOR " + ") AS optionName');
            $this->db->from('ec_option_detail');
            $this->db->where_in('optionID', $combinationOption);
            $this->db->where('languageID', $languageID);
            $resultSet  = $this->db->get();
            //print_r($this->db->last_query()); exit;
            return $resultSet->row_array();
        
    } 
    
    public function getProductOptionValue($productID="",$productOptionID="",$languageID=1){
        $this->db->select('pov.productOptionValueID,pov.productOptionID,pov.productID,pov.optionID,pov.optionValueID,pov.quantity,pov.priceType,pov.price,opDet.name as optionName,opValDet.name as optionValueName');
        $this->db->from('ec_product_option_value pov');
        $this->db->join('ec_option_detail opDet', 'opDet.optionID = pov.optionID AND opDet.languageID='.$languageID, 'left');
        $this->db->join('ec_option_value_detail opValDet', 'opValDet.optionValueID = pov.optionValueID AND opValDet.languageID='.$languageID, 'left');
        $this->db->where('pov.productOptionID', $productOptionID);
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    public function getProductOptionPrice($productOptionID="",$languageID=1){
        $this->db->select('pov.productOptionValueID,pov.productOptionID,pov.productID,pov.optionID,pov.optionValueID,pov.quantity,pov.priceType,pov.price,opDet.name as optionName,opValDet.name as optionValueName');
        $this->db->from('ec_product_option_value pov');
        $this->db->join('ec_option_detail opDet', 'opDet.optionID = pov.optionID AND opDet.languageID='.$languageID, 'left');
        $this->db->join('ec_option_value_detail opValDet', 'opValDet.optionValueID = pov.optionValueID AND opValDet.languageID='.$languageID, 'left');
        $this->db->where('pov.productOptionID', $productOptionID);
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->row_array();
    }
    public function getProductOptionByID($productOptionID=""){
        $this->db->select('productOptionID,productID,combinationOptionID,price');
        $this->db->from('ec_product_option');
        $this->db->where('productOptionID', $productOptionID);
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->row_array();
    }
    
    public function getProductOptionValueByProducID($productOptionID=""){
        $this->db->select('productOptionValueID,productOptionID,productID,optionID,optionValueID');
        $this->db->from('ec_product_option_value');
        $this->db->where('productOptionID', $productOptionID);
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function updateProductOptionQuantity($product_id,$quantity,$option) {
        
        $optionval = explode(',', $option);
        //print_r($option);
        $combination = array();$combo='';
        foreach($optionval as $option){
            $optionvals = explode(':', $option);$i=0;
            foreach ($optionvals as $value) { 
                $vals = explode('-', $value); 
                if($vals[0] == 'optionValueId'){
                    $optionCombo = $vals[1];
                    $combo = $vals[1];
                }
            }
            $combination[] = $combo;
        }  
        if(count(array_filter($combination)) == count($combination)) {
            $this->db->select('productOptionID,quantity,productID');
        $this->db->from('ec_product_option');
        $this->db->where('productID', $product_id);
        $this->db->where('status', 'Active');
                $resultSet  = $this->db->get();
                $productOptions = $resultSet->result_array();
        //echo "<pre>"; print_r($productOptions); exit;
        $exist  =   0;$productOptionId=''; $pQuiantity=''; $pProductId='';
        if($productOptions){
            foreach($productOptions as $key1=>$productOptionsData){
               $productOptionsValueReturn    =   array();
                $this->db->select('optionValueID');
                $this->db->from('ec_product_option_value');
                $this->db->where('productOptionID', $productOptionsData['productOptionID']);
                $this->db->where('productID', $product_id);
                $resultSetOption  = $this->db->get();
                $productOptionsValue = $resultSetOption->result_array();
                foreach($productOptionsValue as $key2=>$productOptionsValueData){
                   
                   $productOptionsValueReturn[]    = $productOptionsValueData['optionValueID'];
                }
                $checkArray =   array_diff_assoc($productOptionsValueReturn, $combination);
           // print_r($checkArray);
            if(empty($checkArray)){  
                $exist  = 1;
                $productOptionId=$productOptionsData['productOptionID'];
                $pQuiantity=$productOptionsData['quantity'];
                $pProductId=$productOptionsData['productID'];

            }
                //print_r($productOptionsValueReturn);
            }
        }
}
$this->db->query("UPDATE ec_product_option SET quantity = IF(quantity>0, ".($pQuiantity-(int) $quantity).", 0) WHERE productID = '" . $pProductId . "' AND productOptionID = '" . $productOptionId . "' "); 
        
    }
    public function updateProductOptionRemoveQuantity($productID) {
        $customerID = $this->session->userdata('moonehcustomerID');
       $apiID = ($this->session->userdata('api_id')) ? (int) $this->session->userdata('api_id') : 0;
        $sessionID = $this->session->session_id;
        $params = array(
                'customerID' => $customerID,
                'apiID' => $apiID,
                'sessionID' => $sessionID,
                'productID' => $productID
            );
        $this->db->select('*');
        $this->db->from('ec_cart');
        $this->db->where($params);
        $resultSets  = $this->db->get();
        //print_r($this->db->last_query());
        $results = $resultSets->result_array();
        //$optionval = explode(',', $option);
        //print_r($results);
        $combination = array();$combo='';
        foreach($results as $res){
            $optionval = explode(',', json_decode($res['option']));
             foreach($optionval as $option){
                $optionvals = explode(':', $option);$i=0;
                foreach ($optionvals as $value) { 
                    $vals = explode('-', $value); 
                    if($vals[0] == 'optionValueId'){
                        $optionCombo = $vals[1];
                        $combo = $vals[1];
                    }
                }
                $combination[] = $combo;
            }  
            if(count(array_filter($combination)) == count($combination)) {
                
                        $this->db->select('productOptionID,quantity,productID');
        $this->db->from('ec_product_option');
        $this->db->where('productID', $productID);
        $this->db->where('status', 'Active');
                $resultSet  = $this->db->get();
                $productOptions = $resultSet->result_array();
        //echo "<pre>"; print_r($productOptions); exit;
        $exist  =   0;$productOptionId=''; $pQuiantity=''; $pProductId='';
        if($productOptions){
            foreach($productOptions as $key1=>$productOptionsData){
               $productOptionsValueReturn    =   array();
                $this->db->select('optionValueID');
                $this->db->from('ec_product_option_value');
                $this->db->where('productOptionID', $productOptionsData['productOptionID']);
                $this->db->where('productID', $productID);
                $resultSetOption  = $this->db->get();
                $productOptionsValue = $resultSetOption->result_array();
                foreach($productOptionsValue as $key2=>$productOptionsValueData){
                   
                   $productOptionsValueReturn[]    = $productOptionsValueData['optionValueID'];
                }
                $checkArray =   array_diff_assoc($productOptionsValueReturn, $combination);
           // print_r($checkArray);
            if(empty($checkArray)){  
                $exist  = 1;
                $productOptionId=$productOptionsData['productOptionID'];
                $pQuiantity=$productOptionsData['quantity'];
                $pProductId=$productOptionsData['productID'];

            }
                //print_r($productOptionsValueReturn);
            }
        }

                //print_r($result['productOptionID']);
                $this->db->query("UPDATE ec_product_option SET quantity = ".($pQuiantity+$res['quantity'])." WHERE productID = '" . $pProductId . "' AND productOptionID = '" . $productOptionId . "' "); 
                    //print_r($optionId); echo $res['quantity']; echo "</br>";
                
            }
        }
        
    }
    public function getProductCombination($productID){
     $this->db->select('oc.optionID')->from('ec_option_combination oc')->join('ec_product_option po', 'oc.combinationID = po.combinationOptionID', 'left')
        ->where('po.productID', $productID);
        $result = $this->db->get();
        $resultOption = $result->row();
        return $resultOption;
    }
        
    public function checkOptionValueExist($combinationID="",$productID="",$optionID="",$optionValueID){
        $productOptionsValueReturn    =   array();
        $this->db->select('productOptionID');
        $this->db->from('ec_product_option');
        $this->db->where('productID', $productID);
        $this->db->where('combinationOptionID', $combinationID);
        $this->db->where('status', 'Active');
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        $productOptions = $resultSet->result_array();
        //echo "<pre>"; print_r($productOptions); exit;
        if($productOptions){
            foreach($productOptions as $key1=>$productOptionsData){
               
                $this->db->select('optionValueID');
                $this->db->from('ec_product_option_value');
                $this->db->where('productOptionID', $productOptionsData['productOptionID']);
                $this->db->where('productID', $productID);
                $resultSetOption  = $this->db->get();
                $productOptionsValue = $resultSetOption->result_array();
                foreach($productOptionsValue as $key2=>$productOptionsValueData){
                   
                   $productOptionsValueReturn[$key1][$key2]    = $productOptionsValueData['optionValueID'];
                }
            }
        }

         

        return $productOptionsValueReturn;
    }
    public function getProductCombinationValue($productID,$option, $valueOption){
     $this->db->select('coun(oc.optionID)')->from('ec_product_option_value oc')
        ->where('oc.productID', $productID)
        ->where('oc.optionID', $option)
        ->where('oc.optionValueID IN('.$valueOption.')');
        $result = $this->db->get();
         //print_r($this->db->last_query());exit;
        $resultOption = $result->row();
        return $resultOption;
        }
        public function getProductOptionVal($productID,$optionVal,$languageID){
        $this->db->select('oc.productOptionID')->from('ec_product_option_value oc')
        ->where('oc.productID', $productID)
        ->where('oc.optionValueID',$optionVal);
        $result = $this->db->get();
        //print_r($this->db->last_query()); exit;
        $resultOption = $result->result_array();
        $productOptionID =array();
        foreach($resultOption as $option){
        $productOptionID[]=$option['productOptionID'];
        }
        $productOptionIDs = implode(',',$productOptionID);

        
        $this->db->select('"yes" as optype ,o.image,od.name,po.productID,oc.required,od.optionID,opdt.name as optionName,opt.*,od.optionValueID,op.productOptionID,sum(op.quantity) as quantity');
        $this->db->from('ec_product_option_value po');
        $this->db->join('ec_product_option op', 'po.productOptionID = op.productOptionID');
        $this->db->join('ec_option_combination oc', 'oc.combinationID = op.combinationOptionID', 'left');
        $this->db->join('ec_option_value o', 'po.optionValueID = o.optionValueID', 'left');
        $this->db->join('ec_option opt', 'opt.optionID = o.optionID', 'left');
        $this->db->join('ec_option_detail opdt', 'opdt.optionID = opt.optionID', 'left');
        $this->db->join('ec_option_value_detail od', 'od.optionValueID = o.optionValueID AND od.languageID=' . $languageID, 'left');
        $this->db->where('po.productID', $productID);
        $this->db->where('po.productOptionID IN('.$productOptionIDs.')');
        $this->db->where('op.status', 'Active');
        $this->db->group_by('po.optionValueID'); 
    $query1 = $this->db->get_compiled_select(); // It resets the query just like a get()

   $this->db->select('"no" as optype ,o.image,ods.name,po.productID,oc.required,ods.optionID,opdt.name as optionName,opt.*,ods.optionValueID,op.productOptionID,op.productOptionID');
        $this->db->from('ec_product_option_value po');
        $this->db->join('ec_product_option op', 'po.productOptionID = op.productOptionID');
        $this->db->join('ec_option_combination oc', 'oc.combinationID = op.combinationOptionID', 'left');
        $this->db->join('ec_option_value o', 'po.optionValueID = o.optionValueID', 'left');
        $this->db->join('ec_option opt', 'opt.optionID = o.optionID', 'left');
        $this->db->join('ec_option_detail opdt', 'opdt.optionID = opt.optionID', 'left');
        $this->db->join('ec_option_value_detail ods', 'ods.optionValueID = o.optionValueID AND ods.languageID=' . $languageID, 'left');
        $this->db->where('po.productID', $productID);
        $this->db->where('ods.name NOT IN(SELECT `od`.`name`
        FROM `ec_product_option_value` `po` JOIN `ec_product_option` `op` ON `po`.`productOptionID` = `op`.`productOptionID` 
        LEFT JOIN `ec_option_value` `o` ON `po`.`optionValueID` = `o`.`optionValueID` 
        LEFT JOIN `ec_option` `opt` ON `opt`.`optionID` = `o`.`optionID`
        LEFT JOIN `ec_option_detail` `opdt` ON `opdt`.`optionID` = `opt`.`optionID` 
        LEFT JOIN `ec_option_value_detail` `od` ON `od`.`optionValueID` = `o`.`optionValueID` AND `od`.`languageID`=1
        WHERE `po`.`productID` = '.$productID.' AND `po`.`productOptionID` IN('.$productOptionIDs.') AND `op`.`status` = "Active" GROUP BY `po`.`optionValueID`)');
        $this->db->where('po.productOptionID NOT IN('.$productOptionIDs.')');
        $this->db->where('op.status', 'Active');
        $this->db->group_by('po.optionValueID'); 
    $query2 = $this->db->get_compiled_select(); 

    $query = $this->db->query($query1." UNION ".$query2);
        
        //$resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result_array();
        
        }
    public function checkQunatity($productOptionID){
        $this->db->select('oc.*')->from('ec_product_option oc')
        ->where('oc.productOptionID', $productOptionID)
        ->where('oc.quantity!=', 0);
        $result = $this->db->get();
         //print_r($this->db->last_query());exit;
        $resultOption = $result->row();
        return $resultOption;
    }
        public function checkDiscountQunatity($productOptionID){
        $this->db->select('oc.*')->from('ec_product_discount oc')
        ->where('oc.productID', $productOptionID)
        ->where('oc.quantity!=', 0);
        $result = $this->db->get();
         //print_r($this->db->last_query());exit;
        $discount = $result->row();
        return $discount;
    }  
    public function getOptionquentiy($productID){
        $this->db->select_sum('quantity');
        $this->db->from('ec_product_option');
        $this->db->where('productID', $productID);
        $this->db->where('status', 'Active');
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->row_array();
    }
    
    public function getOptionCombinationForAPI($productID="",$languageID=1){
        $optionData =   array();
        $this->db->select('optComb.optionID,optComb.productID,optComb.required');
        $this->db->from('ec_option_combination optComb');
        $this->db->join('ec_product_option proOpt', 'optComb.combinationID = proOpt.combinationOptionID', 'inner');
        $this->db->where('optComb.productID', $productID);
        $this->db->where('optComb.status', 'Active');
        $this->db->where('proOpt.status', 'Active');
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        $optionCombination  =   $resultSet->row_array();
        if($optionCombination){
            $optionData =   $optionCombination;
            $optionIDVls   =   explode(",",$optionCombination['optionID']);
            if($optionIDVls){
                foreach($optionIDVls as $comKey=>$optionID){
                    $this->db->select('op.optionID,op.type,op.showAs,opDet.name as optionName');
                    $this->db->from('ec_option op');
                    $this->db->join('ec_option_detail opDet', 'opDet.optionID = op.optionID AND opDet.languageID=' . $languageID, 'left');
                    $this->db->where('op.optionID', $optionID);
                    $this->db->where('op.status', 'Active');
                    $this->db->order_by('op.sortOrder', 'ASC');
                    $resultSet  = $this->db->get();
                    //print_r($this->db->last_query()); exit;
                    $option  =   $resultSet->row_array();
                    if($option)
                        $optionData['options'][$comKey] =   $option;
                }
            }
           
        }
        return  $optionData;
    }
    
    public function productOptionValuesForAPI($productID="",$optionID="",$languageID=1){
        $this->db->select('DISTINCT(pov.optionValueID),opvDet.name');
        $this->db->from('ec_product_option_value pov');
        $this->db->join('ec_option_value_detail opvDet', 'opvDet.optionValueID = pov.optionValueID AND opvDet.languageID="' . $languageID.'"', 'left');
        $this->db->join('ec_product_option proOpt', 'pov.productOptionID = proOpt.productOptionID', 'inner');
        $this->db->where('pov.productID', $productID);
        $this->db->where('pov.optionID', $optionID);
        $this->db->where('proOpt.status', 'Active');
        $resultSet  = $this->db->get();
       // print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function avilableOptionForAPI($productID="",$languageID=1){
        $this->db->select('productOptionID,quantity,priceType,price');
        $this->db->from('ec_product_option');
        $this->db->where('productID', $productID);
        $this->db->where('status', 'Active');
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        $productOptions = $resultSet->result_array();
        //echo "<pre>"; print_r($productOptions); exit;
        if($productOptions){
            foreach($productOptions as $key1=>$productOptionsData){
               
                $this->db->select('opVal.optionValueID,opVal.optionID,opvDet.name as optionValueName');
                $this->db->from('ec_product_option_value opVal');
                $this->db->join('ec_option_value_detail opvDet', 'opvDet.optionValueID = opVal.optionValueID AND opvDet.languageID="' . $languageID.'"', 'left');
                $this->db->where('productOptionID', $productOptionsData['productOptionID']);
                $this->db->where('productID', $productID);
                $resultSetOption  = $this->db->get();
                $productOptionsValue = $resultSetOption->result_array();
               $productOptions[$key1]['optionValue']    =   $productOptionsValue;
            }
        }
        return $productOptions;
    }
    
}

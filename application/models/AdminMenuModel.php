<?php
class AdminMenuModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('ec_menu.menuID,ec_menu_detail.menuName,ec_menu.url,ec_menu.sortOrder,ec_menu.status,ec_menu.parent,(SELECT menuName FROM ec_menu_detail WHERE menuID=ec_menu.parent AND ec_menu_detail.languageID='.$params['languageID'].') as parentName');
        $this->db->from('ec_menu');
        $this->db->join('ec_menu_detail','ec_menu.menuID = ec_menu_detail.menuID AND ec_menu_detail.languageID='.$params['languageID'],'left');
         if($params['menuName']){
            $this->db->where('ec_menu_detail.menuName LIKE', '%'.$params['menuName'].'%');
        }
        $this->db->where('ec_menu.status!=', 'Deleted');
       
        $this->db->order_by('menuID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
   function getTableValue($select='*',$table,$where=null,$multiple=false){
        $this->db->select($select);
        $this->db->from($table);
        if($where){
            $this->db->where($where);
        }
        $res = $this->db->get();
		
        //echo $this->db->last_query();exit;

        if($multiple){
            return $res->result_array();
        }
        else{
            return $res->row_array();
        }
    }
        public function selectMenu($id='',$languageID=''){
        $this->db->select('men.menuID,men.sortOrder, menCat.menuName,(SELECT menuName FROM ec_menu_detail WHERE menuID=men.parent AND menCat.languageID='.$languageID.') as parentName');
        $this->db->from('ec_menu men');
        $this->db->join('ec_menu_detail menCat','men.menuID = menCat.menuID AND menCat.languageID='.$languageID, 'left');
        $this->db->join('ec_menu parMen','menCat.menuID = men.menuID', 'left');
        $this->db->where('men.status!=', 'Deleted');
        $this->db->distinct();
        if($id!=='') {
            $this->db->where('men.menuID',$id);
        }
        $this->db->order_by('men.sortOrder','ASC');
        $resultSet  = $this->db->get();
        if($id!=''){
          return $resultSet->row_array();  
        }
        return $resultSet->result_array();
    }
      
}
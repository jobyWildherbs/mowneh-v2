<?php
/**
 * Description of OrderStatusModel
 *
 * @author wildherbs-user
 */
class OrderStatusModel extends CI_Model {
    //put your code here
    public function selectAll($params) {
        extract($params);
        $this->db->select('os.*, osd.name, osd.languageID');
        $this->db->from('ec_order_status os');
        $this->db->join('ec_order_status_detail osd', 'os.orderStatusID = osd.orderStatusID AND osd.languageID = '.$languageID,'left');
        if ($name!=null && $name!='') {
            $this->db->where('osd.name LIKE', '%' . $name . '%');
        }
        if ($orderStatusID !=null && $orderStatusID!=''){
            $this->db->where('os.orderStatusID',$orderStatusID);
        }
        $this->db->where('os.status!=', 'Deleted');
        $this->db->order_by('os.sortOrder', 'ASC');
        if($limit){
            $this->db->limit($limit, $start);
        }
        $resultSet = $this->db->get();
//        print_r($this->db->last_query()); exit;
        if($orderStatusID){
            return $resultSet->row_array();
        }
        return $resultSet->result_array();
    }
    
    public function selectActive($languageID){
        $this->db->select('os.*, osd.name, osd.languageID');
        $this->db->from('ec_order_status os');
        $this->db->join('ec_order_status_detail osd', 'os.orderStatusID = osd.orderStatusID AND osd.languageID = '.$languageID,'left');
        $this->db->where('os.status', 'Active');
        $resultSet = $this->db->get();
        return $resultSet->result_array();
    }
}

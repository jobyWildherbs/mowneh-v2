<?php
class BlogModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('ec_blog.blogID,ec_blog.dateAdded,ec_blog_detail.title,ec_blog.status');
        $this->db->from('ec_blog');
        $this->db->join('ec_blog_detail','ec_blog.blogID = ec_blog_detail.blogID AND ec_blog_detail.languageID='.$params['languageID'],'left');
         if($params['title']){
            $this->db->where('ec_blog_detail.title LIKE', '%'.$params['title'].'%');
        }
        $this->db->where('ec_blog.status!=', 'Deleted');
       
        $this->db->order_by('blogID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
   function getTableValue($select='*',$table,$where=null,$multiple=false){
        $this->db->select($select);
        $this->db->from($table);
        if($where){
            $this->db->where($where);
        }
        $res = $this->db->get();
		
        //echo $this->db->last_query();exit;

        if($multiple){
            return $res->result_array();
        }
        else{
            return $res->row_array();
        }
    }

    public function selectBlog($params) {
        $this->db->select('ec_blog.blogID,ec_blog.dateAdded,ec_blog_detail.description,ec_blog_detail.author,ec_blog.image,ec_blog_detail.title,ec_blog.status');
        $this->db->from('ec_blog');
        $this->db->join('ec_blog_detail', 'ec_blog.blogID = ec_blog_detail.blogID AND ec_blog_detail.languageID=' . $params['languageID'], 'left');
        $this->db->where('ec_blog.status!=', 'Deleted');
        $this->db->where('ec_blog.status!=', 'Inactive');
        if(isset($params['limit'])){
            if(isset($params['start'])){
                $this->db->limit($params['limit'],$params['start']);
            } else {
                $this->db->limit($params['limit']);
            }
        }
        $this->db->order_by('blogID', 'DESC');
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
    public function selectBlogDet($blogID='',$languageID = '') {
        $this->db->select('ec_blog.blogID,ec_blog.dateAdded,ec_blog_detail.description,ec_blog_detail.author,ec_blog.image,ec_blog_detail.title,ec_blog.status');
        $this->db->from('ec_blog');
        $this->db->join('ec_blog_detail', 'ec_blog.blogID = ec_blog_detail.blogID AND ec_blog_detail.languageID=' . $languageID, 'left');

        $this->db->where('ec_blog.blogID',$blogID);
        $this->db->where('ec_blog.status!=', 'Deleted');
        $this->db->where('ec_blog.status!=', 'Inactive');

        $this->db->order_by('blogID', 'DESC');

        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
    

}
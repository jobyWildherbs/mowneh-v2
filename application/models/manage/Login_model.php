<?php
class Login_model extends CI_Model {
    public function __construct()
    {
        
    }
    
    function checkUserEmail($userName){
        $this->db->select('*');
        $this->db->from('ec_user');
        $this->db->where('userName',$userName);
        $res = $this->db->get();
        if($res->num_rows()){
            return false;
        }
        else{
            return true;
        }
    }
    
    function getUserDetails($userName){
        $this->db->select('*');
        $this->db->from('ec_user');
        $this->db->where('userName',$userName);
        return $res = $this->db->get()->row_array();
    }
}

<?php

class TimeSloteModel extends CI_Model {
     public function selectAll($params) {
        $this->db->select('*');
        $this->db->from('ec_delevery_time_slot');
        
        $this->db->where('status!=', 'Deleted');

        $this->db->order_by('slotID', 'DESC');

        if ($params['limit']) {
            $this->db->limit($params['limit'], $params['start']);
        }

        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
}
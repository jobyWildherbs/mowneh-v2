<?php
class MenuOfferModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('ec_menu_offer_image.menuOfferId,ec_menu_offer_detail.name,ec_menu_offer_image.image,ec_menu_offer_image.status,ec_menu_offer_image.parentID,(SELECT name FROM ec_category_detail WHERE categoryID=ec_menu_offer_image.parentID AND catDet.languageID='.$params['languageID'].') as parentName');
        $this->db->from('ec_menu_offer_image');
        $this->db->join('ec_menu_offer_detail','ec_menu_offer_image.menuOfferId = ec_menu_offer_detail.menuOfferId AND ec_menu_offer_detail.languageID='.$params['languageID'],'left');
        $this->db->join('ec_category cat','cat.categoryID=ec_menu_offer_image.parentID');
        $this->db->join('ec_category_detail catDet','cat.categoryID = catDet.categoryID AND catDet.languageID='.$params['languageID'], 'left');
        $this->db->join('ec_category parCat','parCat.categoryID = cat.categoryID', 'left');
         if($params['name']){
            $this->db->where('ec_menu_offer_detail.name LIKE', '%'.$params['name'].'%');
        }
        $this->db->where('ec_menu_offer_image.status!=', 'Deleted');
       
        $this->db->order_by('menuOfferId', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
   function getTableValue($select='*',$table,$where=null,$multiple=false){
        $this->db->select($select);
        $this->db->from($table);
        if($where){
            $this->db->where($where);
        }
        $res = $this->db->get();
		
        //echo $this->db->last_query();exit;

        if($multiple){
            return $res->result_array();
        }
        else{
            return $res->row_array();
        }
    }
         public function selectBanner($languageID=''){
        $this->db->select('bann.menuOfferId,bannDet.name,bannDet.description,bann.link,bannDet.linkOnButton,bann.image,bann.sortOrder,bann.status');
        $this->db->from('ec_menu_offer_image bann');
        $this->db->join('ec_menu_offer_detail bannDet','bann.menuOfferId = bannDet.menuOfferId AND bannDet.languageID='.$languageID,'left');
//         if($params['name']){
//            $this->db->where('ec_menu_offer_detail.name LIKE', '%'.$params['name'].'%');
//        }
        $this->db->where('bann.status!=', 'Deleted');
        $this->db->where('bann.status!=', 'Inactive');
       
        $this->db->order_by('sortOrder', 'Asc'); 
        $this->db->limit(4);

//        if($params['limit']){
//            $this->db->limit($params['limit'],$params['start']);
//        }
       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
    
       public function selectCategory($id='',$languageID=''){
        $this->db->select('cat.categoryID, cat.pageKey,cat.parentID,cat.sortOrder,bann.parentID, catDet.name,(SELECT name FROM ec_category_detail WHERE categoryID=cat.parentID AND catDet.languageID='.$languageID.') as parentName,cat.status');
        $this->db->from('ec_category cat');
        $this->db->join('ec_category_detail catDet','cat.categoryID = catDet.categoryID AND catDet.languageID='.$languageID, 'left');
        $this->db->join('ec_category parCat','parCat.categoryID = cat.categoryID', 'left');
        $this->db->join('ec_menu_offer_image bann','bann.parentID= catDet.categoryID','left');
        $this->db->where('cat.status!=', 'Deleted');
        if($id!=='') {
            $this->db->where('cat.categoryID',$id);
        }
        $this->db->order_by('cat.sortOrder','ASC');
        $resultSet  = $this->db->get();
        if($id!=''){
          return $resultSet->row_array();  
        }
        return $resultSet->result_array();
    }
}
<?php
class NewsletterModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('*');
        $this->db->from('ec_newsletter');
       // $this->db->join('ec_newsletter_detail','ec_newsletter.newsletterID = ec_newsletter_detail.newsletterID AND ec_newsletter_detail.languageID='.$params['languageID'],'left');
         if($params['email']){
            $this->db->where('ec_newsletter.email LIKE', '%'.$params['email'].'%');
        }
        $this->db->where('ec_newsletter.status!=', 'Deleted');
       
        $this->db->order_by('newsletterID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
   
}
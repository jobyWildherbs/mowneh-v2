<?php
class ReviewModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('ec_review_prod.reviewID,ec_review_prod.customerID,ec_review_prod.firstname,ec_review_prod.lastname, ec_review_prod.email ,ec_review_prod.phone, ec_review_prod_detail.name,ec_review_prod_detail.comments,ec_review_prod.status');
        $this->db->from('ec_review_prod');
        $this->db->join('ec_review_prod_detail','ec_review_prod.reviewID = ec_review_prod_detail.reviewID AND ec_review_prod_detail.languageID='.$params['languageID'],'left');
         if($params['name']){
            $this->db->where('ec_review_prod_detail.name LIKE', '%'.$params['name'].'%');
        }
        $this->db->where('ec_review_prod.status!=', 'Deleted');
       
        $this->db->order_by('reviewID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
   function getTableValue($select='*',$table,$where=null,$multiple=false){
        $this->db->select($select);
        $this->db->from($table);
        if($where){
            $this->db->where($where);
        }
        $res = $this->db->get();
		
        //echo $this->db->last_query();exit;

        if($multiple){
            return $res->result_array();
        }
        else{
            return $res->row_array();
        }
    }
         public function selectBanner($languageID=''){
        $this->db->select('bann.reviewID,bannDet.name,bannDet.description,bann.link,bannDet.linkOnButton,bann.image,bann.sortOrder,bann.status');
        $this->db->from('ec_review_prod bann');
        $this->db->join('ec_review_prod_detail bannDet','bann.reviewID = bannDet.reviewID AND bannDet.languageID='.$languageID,'left');
//         if($params['name']){
//            $this->db->where('ec_review_prod_detail.name LIKE', '%'.$params['name'].'%');
//        }
        $this->db->where('bann.status!=', 'Deleted');
        $this->db->where('bann.status!=', 'Inactive');
       
        $this->db->order_by('sortOrder', 'Asc'); 
        $this->db->limit(4);

//        if($params['limit']){
//            $this->db->limit($params['limit'],$params['start']);
//        }
       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
}
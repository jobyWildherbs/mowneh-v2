<?php
class CustomerModel extends CI_Model {
    public function __construct()
    {
        
    }
    
    function checkUserEmail($email){
        $this->db->select('*');
        $this->db->from('ec_customer');
        $this->db->where('email',$email);
        $this->db->where('status!=','Deleted');
        $res = $this->db->get();
        if($res->num_rows()){
            return false;
        }
        else{
            return true;
        }
    }
    function checkUserphone($telephone){
        $this->db->select('*');
        $this->db->from('ec_customer');
        $this->db->where('telephone',$telephone);
        $this->db->where('status!=','Deleted');
        $res = $this->db->get();
        if($res->num_rows()){
            return false;
        }
        else{
            return true;
        }
    }
    
    function getUserDetails($email){
        $this->db->select('*');
        $this->db->from('ec_customer');
        $this->db->where('email',$email);
        return $res = $this->db->get()->row_array();
    }
    
    public function selectAll($params){
        $this->db->select('cust.customerID,addr.addressID,addr.address1,cust.firstname,cust.lastname,cust.email,cust.telephone,cust.status,cust.lastLogin,cust.dateAdded');
        $this->db->from('ec_customer cust');
        $this->db->join('ec_address addr','cust.addressID = addr.addressID AND cust.customerID=addr.customerID','left');
        $this->db->where('cust.status!=', 'Deleted');
        if($params['email']!=''){
            $this->db->where('email LIKE','%'.$params['email'].'%');
        }
        $this->db->order_by('cust.customerID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    } 
    
    public function customerExport($params){
        $languageID     =   $params['languageID'];
        
        if(strpos($params['fields'], 'customer.addressID') !== false){
            $parentQuery    =   ",(SELECT address1 FROM ec_address WHERE customerID=customer.customerID AND addressID=customer.addressID) as address";
            $params['fields']    =   str_replace(',customer.addressID', $parentQuery, $params['fields']);
        }
       
        $this->db->select($params['fields']);
        $this->db->from('ec_customer customer');
        $this->db->where('customer.status=', 'Active');
        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function importData($data,$languageID){

        foreach($data as $key=>$dataVal){
            if(@$dataVal['customerGroupID'])
                $customerData['customerGroupID']   = $dataVal['customerGroupID'];
            if(@$dataVal['storeID'])
                $customerData['storeID']           = $dataVal['storeID'];
            if(@$dataVal['firstname'])
                $customerData['firstname']         = $dataVal['firstname'];
            if(@$dataVal['lastname'])
                $customerData['lastname']          = $dataVal['lastname'];
            if(@$dataVal['email'])
                $customerData['email']              = $dataVal['email'];
            if(@$dataVal['password']){
                $customerData['password']           = $dataVal['password'];
                $customerData['salt']               = $dataVal['salt'];
            }
            if(@$dataVal['telephone'])
                $customerData['telephone']          = $dataVal['telephone'];
            if(@$dataVal['ip'])
                $customerData['ip']                 = $dataVal['ip'];
            if(@$dataVal['status'])
                $customerData['status']             = $dataVal['status'];
            if(!empty($customerData)){
                $this->db->insert('ec_customer', $customerData); 
            }
           
        }
       return true;
    }
    
    public function selectAllReport($params){
        //echo "<pre>"; print_r($params);exit;
        $field  =   "";
        if($params['purchaseCount']){
            $field  .=   ",(SELECT count(orderID) FROM `ec_order` WHERE customerID=cust.customerID) as purchaseCount";
        }
        if($params['amountPurchased']){
            $field  .=   ",(SELECT SUM(total) FROM `ec_order` WHERE customerID=cust.customerID) as amountPurchased";
        }
         if($params['averageCustomerStand']){
            $field  .=   ",((SELECT SUM(total) FROM `ec_order` WHERE customerID=cust.customerID)/(SELECT count(orderID) FROM `ec_order` WHERE customerID=cust.customerID)) as averageCustomerStand";
        }
        if($params['ordered'])
            $field  .=   ",sum(orderPro.quantity) AS totalProductPurchase";
        $this->db->select('cust.customerID,cust.firstname,cust.lastname,cust.email,cust.telephone,cust.status,cust.lastLogin,cust.dateAdded'.$field);
        $this->db->from('ec_customer cust');
        $this->db->join('ec_address addr','cust.addressID = addr.addressID AND cust.customerID=addr.customerID','left');
        if($params['ordered']){
            $this->db->join('ec_order order', 'order.customerID = cust.customerID', 'inner');
            $this->db->join('ec_order_product orderPro', 'orderPro.orderID = order.orderID', 'inner');
        }
        $this->db->where('cust.status', 'Active');
        if($params['fromDate'])
            $this->db->where('cust.dateAdded >=', $params['fromDate']);
        if($params['toDate'])
            $this->db->where('cust.dateAdded <=', $params['toDate']);
       
        $this->db->order_by('cust.customerID', 'DESC'); 

        if(@$params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
        if($params['ordered']=='mostOrder'){   
            $this->db->group_by('order.customerID');
         //   $this->db->order_by('orderedQuentity', 'DESC');
        }elseif($params['ordered']=='lessOrder'){
            $this->db->group_by('order.customerID');
            $this->db->order_by('totalProductPurchase', 'ASC');
        }
        $query = $this->db->get();
         //print_r($this->db->last_query()); exit;
        return $query->result_array();
    } 
}

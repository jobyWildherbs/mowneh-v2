<?php

/**
 * Description of HomeModel
 *
 * @author wildherbs-user
 */
class NotificationModel extends CI_Model {
    
     public function selectAll($params){
        $this->db->select('notificationID,title,message,image,type,parameter,status,DATE_FORMAT(createdDate, "%d-%b-%Y") as createdDate');
        $this->db->from('ec_notification');
       
         if($params['title']){
            $this->db->where('title LIKE', '%'.$params['title'].'%');
        }
        $this->db->where('status!=', 'Deleted');
       
        $this->db->order_by('notificationID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }

    //put your code here
    public function notificationList($userID) {
        $this->db->select('noti.notificationID, noti.title, noti.message, noti.image, noti.type, noti.parameter, noti.link, notiUser.seenStatus, DATE_FORMAT(noti.createdDate, "%b-%d") as createdDate');
        $this->db->from('ec_notification noti');
        $this->db->join('ec_notification_to_user notiUser', 'noti.notificationID = notiUser.notificationID');
        $this->db->where('notiUser.userID', $userID);
        $this->db->order_by('noti.createdDate','DESC');
        $this->db->limit(30);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }
    
    public function notificationCount($userID) {
        $this->db->select('noti.notificationID');
        $this->db->from('ec_notification noti');
        $this->db->join('ec_notification_to_user notiUser', 'noti.notificationID = notiUser.notificationID');
        $this->db->where('notiUser.userID', $userID);
        $this->db->where('notiUser.seenStatus', 'unseen');
        $this->db->order_by('noti.createdDate','ASC');
        
        $query = $this->db->get();
        return $query->num_rows();
        //echo $this->db->last_query();exit;
        //return $query->result_array();
    }

}

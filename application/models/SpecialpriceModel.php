<?php
class SpecialpriceModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('psp.specialPriceID, psp.priority, psp.customerGroupID, psp.price,psp.status, psp.productID,(SELECT name FROM ec_product_description WHERE productID=psp.productID) as parentName');
        $this->db->from('ec_product_special_price psp');
       
        $this->db->join('ec_product prod','prod.productID=psp.productID');
        $this->db->join('ec_product_description prodDet','prodDet.productID = prod.productID', 'left');
       
         if($params['name']){
            $this->db->where('prodDet.name LIKE', '%'.$params['name'].'%');
        }
        $this->db->where('psp.status!=', 'Deleted');
       
        $this->db->order_by('productID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }

    
       public function selectCategory($id='',$languageID=''){
        $this->db->select('prod.productID,prod.sortOrder, prodDet.name,(SELECT name FROM ec_product_description WHERE productID=prod.productID) as parentName,prod.status');
        $this->db->from('ec_product prod');
        $this->db->join('ec_product_description prodDet','prodDet.productID = prod.productID', 'left');
        $this->db->where('prod.status!=', 'Deleted');
        if($id!=='') {
            $this->db->where('prod.productID',$id);
        }
        $this->db->order_by('prod.sortOrder','ASC');
        $resultSet  = $this->db->get();
        if($id!=''){
          return $resultSet->row_array();  
        }
        return $resultSet->result_array();
    }
}
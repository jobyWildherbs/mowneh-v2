<?php

class GeneralModel extends CI_Model {

    function siteMysqlMode() {
        $sql    =   "SET sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))";
        $this->db->query($sql);
    }
    
    function getTableValue($select = '*', $table, $where = null, $multiple = false) {
        $this->db->select($select);
        $this->db->from($table);
        if ($where) {
            $this->db->where($where);
        }
        $res = $this->db->get();
        //echo $this->db->last_query();
        if ($multiple) {
            return $res->result_array();
        } else {
            return $res->row_array();
        }
    }

    function getFieldValue($select = '*', $table, $where = null) {
        $this->db->select($select);
        $this->db->from($table);
        if ($where) {
            $this->db->where($where);
        }
        $res = $this->db->get()->row_array();
//        echo $this->db->last_query();
        if (is_countable($res)) {
            return array_values($res)[0];
        } else {
            return '';
        }
    }

    function insertValue($table, $values) {
        $this->db->insert($table, $values);
        //echo $this->db->last_query();exit;
        return $this->db->insert_id();
    }

    function deleteTableValues($table, $where) {
        $this->db->where($where);
        $this->db->delete($table);
        //echo $this->db->last_query();exit;
        return TRUE;
    }

    function updateTableValues($table, $where, $values) {
        $this->db->where($where);
        $this->db->update($table, $values);
        //echo $this->db->last_query(); exit;
        return TRUE;
    }

    function getTableCount($table, $where = null, $select = '*') {
        $this->db->select($select);
        $this->db->from($table);
        if ($where) {
            $this->db->where($where);
        }
        return $res = $this->db->get()->num_rows();
    }

    function getTableValueWithLimit($select = '*', $table, $where = null, $order_by = null, $count = null, $start = null) {
        $this->db->select($select);
        $this->db->from($table);
        if ($where) {
            $this->db->where($where);
        }
        if ($order_by) {
            foreach ($order_by as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if ($count && $start) {
            $this->db->limit($count, $start);
        }
        $res = $this->db->get();
        return $res->result_array();
    }

    function getAdminMenus() {
        $this->db->select('*');
        $this->db->where('status', 'enable');
        $this->db->order_by('sortOrder', 'asc');
        $result = $this->db->get('ec_admin_menu')->result_array();

        $menus = $this->arrangeMenu($result);
        //echo "<pre>";print_r($result);exit;
        return $menus;
    }

    function arrangeMenu(array $elements, $parentId = 0) {
        $menuArray = array();

        foreach ($elements as $element) {
            if ($element['parent'] == $parentId) {
                $children = $this->arrangeMenu($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $menuArray[] = $element;
            }
            //echo "<pre>";print_r($menuArray);exit;
        }
        return $menuArray;
    }

    function getCategoryTree($lang = 1) {
        $this->db->select('c.*,cd.name as categoryName');
        $this->db->from('ec_category c');
        $this->db->join('ec_category_detail cd', 'c.categoryID = cd.categoryID');
        $this->db->where('c.status', 'Active');
        $this->db->where('cd.languageID', 1);
        $this->db->order_by('c.sortOrder', 'asc');
        $result = $this->db->get()->result_array();
        $category = $this->arrangeCategory($result);
        //foreach($categorys as $val){
        //echo "<pre>";print_r($result);exit;
        return $category;
    }

    function arrangeCategory(array $elements, $parentId = 0) {
        $menuArray = array();

        foreach ($elements as $element) {
            if ($element['parentID'] == $parentId) {
                $children = $this->arrangeCategory($elements, $element['categoryID']);
                if ($children) {
                    $element['children'] = $children;
                }
                $menuArray[] = $element;
            }
            //echo "<pre>";print_r($menuArray);exit;
        }
        return $menuArray;
    }

    function getPermissionValue($method = '', $upgID = 0) {
        if ($method != '') {
            $this->db->select('upg.value');
            $this->db->from('ec_user_permission up');
            $this->db->join('ec_user_permission_group upg', 'up.id = upg.permissionID');
            $this->db->where('up.className', $method);
            $this->db->where('up.status', 'enable');
            $this->db->where('upg.groupID', $upgID);
            $result = $this->db->get()->row_array();
            if ($result) {
                return $result['value'];
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    function getSettingValue($settingKey = '', $languageID = 1) {
        if ($settingKey != '') {
            $settingDetails = $this->getTableValue('*', 'ec_settings', array('fieldKey' => $settingKey));
            if ($settingDetails) {
                if ($settingDetails['isLanguage'] == 'yes') {
                    $value = $this->getFieldValue('value', 'ec_settings_field_details', array('settingsFieldID' => $settingDetails['id'], 'languageID' => $languageID));
                    return $value;
                } else {
                    return $settingDetails['value'];
                }
            }
        }
        return;
    }

    public function getCities($languageID) {
        $this->db->select('c.cityID,cd.name');
        $this->db->from('ec_cities c');
        $this->db->join('ec_city_detail cd', 'c.cityID=cd.cityID');
        $this->db->where('cd.languageID', $languageID);
        $this->db->where('c.status', 'Active');
        $this->db->order_by('c.sortOrder asc');
        $result = $this->db->get()->result_array();
        return $result;
    }

    public function getInformationPages($languageID = 1) {
        $this->db->select('i.pageKey,id.title');
        $this->db->from('ec_information i');
        $this->db->join('ec_information_detail id', 'i.informationID=id.informationID');
        $this->db->where('id.languageID', $languageID);
        $this->db->where('i.status', 'Active');
        $this->db->order_by('i.sortOrder asc');
        $this->db->limit(6, 0);
        $result = $this->db->get()->result_array();
        return $result;
    }
    
   public function getMenuTree($languageID){
        $sql = "SELECT c.categoryID,c.pageKey,cd.image,c.parentID,cd.name,max(cp.level) as levelNumber, GROUP_CONCAT(cp.pathID ORDER BY cp.level ASC) as levelList, cp.pathID from ec_category c left join ec_category_detail cd on c.categoryID = cd.categoryID and cd.languageID=".(int)$languageID." right JOIN ec_category_path cp on c.categoryID = cp.categoryID WHERE c.status = 'Active' GROUP by c.categoryID order BY levelNumber";
        //echo $sql; 
        $results = $this->db->query($sql)->result_array();
        $menuArray = array();
        foreach ($results as $result){
            if($result['parentID'] == 0 && $result['levelNumber']==0){
                $menuArray[$result['categoryID']] = $result;
            }else{
                $levelItems = explode(',', $result['levelList']);
                array_pop($levelItems);
                if($levelItems){
                    switch (count($levelItems)){
                        case 1:
                            $menuArray[$levelItems[0]]['child'][$result['categoryID']] = $result;
                            break;
                        case 2:
                            $menuArray[$levelItems[0]]['child'][$levelItems[1]]['child'][$result['categoryID']] = $result;
                            break;
                        case 3:
                            $menuArray[$levelItems[0]]['child'][$levelItems[1]]['child'][$levelItems[2]]['child'][$result['categoryID']] = $result;
                            break;
                        case 4:
                            $menuArray[$levelItems[0]]['child'][$levelItems[1]]['child'][$levelItems[2]]['child'][$levelItems[3]]['child'][$result['categoryID']] = $result;
                            break;
                        case 5:
                            $menuArray[$levelItems[0]]['child'][$levelItems[1]]['child'][$levelItems[2]]['child'][$levelItems[3]]['child'][$levelItems[4]]['child'][$result['categoryID']] = $result;
                            break;
                    }
                }
            }
        }
        return $menuArray;
    }
    
    public function getSearchResult($keyword = '',$languageID = 1, $customerID = 0,$params = array()){
        extract($params);
        if($keyword){
            if ($customerID)
                $wishList = ",IF((SELECT productID FROM ec_customer_wishlist wish WHERE wish.customerID='" . $customerID . "' AND wish.productID=pro.productID), 'YES', 'NO') AS wishList";
            else
                $wishList = "";
            $this->db->select('pro.productID,pro.pageKey,pro.image,pro.price,proDet.description,proDet.name,pro.dateAdded,pro.quantity' . $wishList);
            $this->db->from('ec_product pro');
            $this->db->join('ec_product_description proDet', 'proDet.productID = pro.productID AND proDet.languageID=' . $languageID, 'left');
            //$this->db->join('ec_product_filter proFilter', 'proFilter.productID = pro.productID', 'left');
            $this->db->join('ec_product_to_category pc', 'pc.productID = pro.productID', 'left');
            $this->db->where('proDet.searchText LIKE', '%' . $keyword . '%');
            $this->db->where('status', 'Active');
            if($category != ''){
                $this->db->where('pc.categoryID', $category);
            }
            $this->db->group_by('pro.productID');
            $this->db->order_by('pro.dateAdded', 'DESC');
            $resultSet = $this->db->get();
            // print_r($this->db->last_query()); exit;
            return $resultSet->result_array();
        }
    }
    
    public function getLoginAfterCart($merchantReferenceNumber){
        $this->db->select('ord.orderID,cust.customerID,cust.firstname ,cust.lastname,cust.email,cust.status');
        $this->db->from('ec_order_transaction odrTrns');
        $this->db->join('ec_order ord', 'ord.orderID = odrTrns.orderID'); 
        $this->db->join('ec_customer cust', 'cust.customerID = ord.customerID'); 
        $this->db->where('odrTrns.reference_number',$merchantReferenceNumber);
        $resultSet = $this->db->get();
        return $resultSet->row_array();
    }
    
    public function getChildMenuTree($languageID,$categoryID){
        $sql = "SELECT c.categoryID,c.pageKey,c.image,c.parentID,cd.name,max(cp.level) as levelNumber, GROUP_CONCAT(cp.pathID ORDER BY cp.level ASC) as levelList, cp.pathID from ec_category c left join ec_category_detail cd on c.categoryID = cd.categoryID and cd.languageID=".(int)$languageID." right JOIN ec_category_path cp on c.categoryID = cp.categoryID WHERE c.status = 'Active' AND c.parentID=".(int)$categoryID." GROUP by c.categoryID order BY levelNumber";
//        echo $sql;  exit;
        $results = $this->db->query($sql)->result_array();
        $menuArray = array();
        foreach ($results as $result){
//            if($result['parentID'] == 0 && $result['levelNumber']==0){
                $menuArray[$result['categoryID']] = $result;
//            }else{
//                $levelItems = explode(',', $result['levelList']);
//                array_pop($levelItems);
//                if($levelItems){
//                    switch (count($levelItems)){
//                        case 1:
//                            $menuArray[$levelItems[0]]['child'][$result['categoryID']] = $result;
//                            break;
//                        case 2:
//                            $menuArray[$levelItems[0]]['child'][$levelItems[1]]['child'][$result['categoryID']] = $result;
//                            break;
//                        case 3:
//                            $menuArray[$levelItems[0]]['child'][$levelItems[1]]['child'][$levelItems[2]]['child'][$result['categoryID']] = $result;
//                            break;
//                        case 4:
//                            $menuArray[$levelItems[0]]['child'][$levelItems[1]]['child'][$levelItems[2]]['child'][$levelItems[3]]['child'][$result['categoryID']] = $result;
//                            break;
//                        case 5:
//                            $menuArray[$levelItems[0]]['child'][$levelItems[1]]['child'][$levelItems[2]]['child'][$levelItems[3]]['child'][$levelItems[4]]['child'][$result['categoryID']] = $result;
//                            break;
//                    }
//                }
//            }
        }
        return $menuArray;
    }

}

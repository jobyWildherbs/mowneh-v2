<?php

class ManufacturesModel extends CI_Model {

    public function selectAll($params) {
        $this->db->select('ec_manufacturer.manufacturerID,ec_manufacturer_detail.name,ec_manufacturer.image,ec_manufacturer.sort_order,ec_manufacturer.status');
        $this->db->from('ec_manufacturer');
        $this->db->join('ec_manufacturer_detail', 'ec_manufacturer_detail.manufacturerID = ec_manufacturer.manufacturerID AND ec_manufacturer_detail.languageID='.$params['languageID'],'left');
        if($params['name']){
            $this->db->where('ec_manufacturer_detail.name LIKE', '%'.$params['name'].'%');
        }
        $this->db->where('ec_manufacturer.status!=', 'Deleted');
 
        $this->db->order_by('manufacturerID', 'DESC');

        if ($params['limit']) {
            $this->db->limit($params['limit'], $params['start']);
        }

        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }

    public function selectFilter($id = '', $languageID = '') {

        $this->db->select('manuFact.manufacturerID, manuFactDet.languageID,manuFactDet.name as manufactureName, manuFact.sort_order sortOrder');
        $this->db->from('ec_manufacturer manuFact');
        $this->db->join('ec_manufacturer_detail manuFactDet', 'manuFact.manufacturerID = manuFactDet.manufacturerID AND manuFactDet.languageID=' . $languageID, 'left');
        $this->db->where('manuFact.status', 'Active');
        if ($id !== '') {
            $this->db->where('manuFact.manufacturerID', $id);
        }
        $this->db->order_by('manuFact.sort_order', 'DESC');
        //$this->db->limit();
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
        if ($id != '') {
            return $resultSet->row_array();
        }
        return $resultSet->result_array();
    }
      public function selectActiveFilter($id = '', $languageID = '') {

        $this->db->select('manuFact.manufacturerID, manuFactDet.languageID,manuFactDet.name as manufactureName, manuFact.sort_order sortOrder');
        $this->db->from('ec_manufacturer manuFact');
        $this->db->join('ec_manufacturer_detail manuFactDet', 'manuFact.manufacturerID = manuFactDet.manufacturerID AND manuFactDet.languageID=' . $languageID, 'left');
        $this->db->where('manuFact.status=', 'Active');
        if ($id !== '') {
            $this->db->where('manuFact.manufacturerID', $id);
        }
        $this->db->order_by('manuFact.sort_order', 'DESC');
        //$this->db->limit();
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit; exit;
        if ($id != '') {
            return $resultSet->row_array();
        }
        return $resultSet->result_array();
    }

    function getTableValue($select = '*', $table, $where = null, $multiple = false) {
        $this->db->select($select);
        $this->db->from($table);
        if ($where) {
            $this->db->where($where);
        }
        $res = $this->db->get();

        //echo $this->db->last_query();exit;

        if ($multiple) {
            return $res->result_array();
        } else {
            return $res->row_array();
        }
    }
    
    public function selectReportFilter($languageID = '') {
        $this->db->select('manuFact.manufacturerID,manuFactDet.name');
        $this->db->from('ec_manufacturer manuFact');
        $this->db->join('ec_manufacturer_detail manuFactDet', 'manuFact.manufacturerID = manuFactDet.manufacturerID AND manuFactDet.languageID=' . $languageID, 'left');
        $this->db->where('manuFact.status', 'Active');
       
        $this->db->order_by('manuFact.sort_order', 'DESC');
        //$this->db->limit();
        $resultSet = $this->db->get();
        //print_r($this->db->last_query()); exit;
       
        return $resultSet->result_array();
    }
    
    public function brandExport($params){
       // echo "<pre>"; print_r($params); exit;
        $this->db->select($params['fields']);
        $this->db->from('ec_manufacturer manufacturer');
        $this->db->join('ec_manufacturer_detail manufacturer_detail', 'manufacturer_detail.manufacturerID = manufacturer.manufacturerID AND manufacturer_detail.languageID='.$params['languageID'],'left');
        
        $this->db->where('manufacturer.status!=', 'Deleted');
 
        $this->db->order_by('manufacturer.manufacturerID', 'DESC');

        if ($params['limit']) {
            $this->db->limit($params['limit'], $params['start']);
        }

        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
    
    public function createPageKey($value){
        $pageKey    = preg_replace("/[^a-zA-Z^0-9]+/", "-", trim(strtolower($value))); 
        //echo $pageKey;exit;
        $this->db->select('pageKey');
        $this->db->from('ec_product');
        $this->db->where('pageKey', $pageKey);
        $this->db->where('status!=', 'Deleted');
        $resultSet  = $this->db->get();
        $checkVal   =   $resultSet->row_array();
        if(!empty($checkVal)){
           $pageKey =   $pageKey.'-'.uniqid();
        }
        return $pageKey;
    }
    
    public function importData($data,$languageID){
         
        foreach($data as $key=>$dataVal){
            if(@$dataVal['status'])
                $brand['status']            = $dataVal['status'];
            if(@$dataVal['name']){
                $brandDetail['name']         = $dataVal['name'];
                $pageKey    =   $this->createPageKey($dataVal['name']);
                    $brand['pageKey']  =    $pageKey;
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($dataVal['name'])));
                    $brand['importKey']    =   $importKey;
            }
            if(@$dataVal['name_ar'])
                $brandDetail_ar['name']         = $dataVal['name_ar'];
            
            if(!empty($brand)){
                $this->db->insert('ec_manufacturer', $brand);
                $manufacturerID =   $this->db->insert_id();
                
            }
            if(!empty($brandDetail)){
                $brandDetail['manufacturerID']        =     $manufacturerID;
                $brandDetail['languageID']            =     1;
               
                $this->db->insert('ec_manufacturer_detail', $brandDetail);
                echo $this->db->last_query();
            }
            if(!empty($brandDetail_ar)){
                $brandDetail_ar['manufacturerID']        =     $manufacturerID;
                $brandDetail_ar['languageID']            =     2;
               
                $this->db->insert('ec_manufacturer_detail', $brandDetail_ar);
            }
        } 
        return true;
    }

}

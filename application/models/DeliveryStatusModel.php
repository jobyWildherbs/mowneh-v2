<?php
/**
 * Description of DeliveryStatusModel
 *
 * @author wildherbs-user
 */
class DeliveryStatusModel extends CI_Model{
    //put your code here
    public function selectAll($params) {
        extract($params);
        $this->db->select('ds.*, dsd.name, dsd.languageID');
        $this->db->from('ec_delivery_status ds');
        $this->db->join('ec_delivery_status_detail dsd', 'ds.deliveryStatusID = dsd.deliveryStatusID AND dsd.languageID = '.$languageID,'left');
        if ($name!=null && $name!='') {
            $this->db->where('dsd.name LIKE', '%' . $name . '%');
        }
        if ($deliveryStatusID !=null && $deliveryStatusID!=''){
            $this->db->where('ds.deliveryStatusID',$deliveryStatusID);
        }
        $this->db->where('ds.status!=', 'Deleted');
        $this->db->order_by('ds.sortOrder', 'ASC');
        if($limit){
            $this->db->limit($limit, $start);
        }
        $resultSet = $this->db->get();
//        print_r($this->db->last_query()); exit;
        if($deliveryStatusID){
            return $resultSet->row_array();
        }
        return $resultSet->result_array();
    }
    public function selectActive($languageID){
        $this->db->select('ds.*, dsd.name, dsd.languageID');
        $this->db->from('ec_delivery_status ds');
        $this->db->join('ec_delivery_status_detail dsd', 'ds.deliveryStatusID = dsd.deliveryStatusID AND dsd.languageID = '.$languageID,'left');
        $this->db->where('ds.status', 'Active');
        $resultSet = $this->db->get();
        return $resultSet->result_array();
    }
}

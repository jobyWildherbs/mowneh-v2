<?php
class MobilePromotionModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('prom.promotionID,promDet.title,promDet.image,prom.sortOrder,prom.status');
        $this->db->from('ec_mobile_promotion prom');
        $this->db->join('ec_mobile_promotion_detail promDet','prom.promotionID = promDet.promotionID AND promDet.languageID='.$params['languageID'],'left');
         if($params['title']){
            $this->db->where('promDet.title LIKE', '%'.$params['title'].'%');
        }
        $this->db->where('prom.status!=', 'Deleted');
       
        $this->db->order_by('promotionID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
    
       public function selectPromotion($languageID=''){
        $this->db->select('prom.promotionID,prom.categoryID,promDet.languageID,promDet.title,promDet.image,prom.sortOrder,prom.status');
        $this->db->from('ec_mobile_promotion prom');
        $this->db->join('ec_mobile_promotion_detail promDet','prom.promotionID = promDet.promotionID AND promDet.languageID='.$languageID,'left');
//         if($params['title']){
//            $this->db->where('promDet.title LIKE', '%'.$params['title'].'%');
//        }
        $this->db->where('prom.status!=', 'Deleted');
        $this->db->where('prom.status!=', 'Inactive');
       
        $this->db->order_by('sortOrder', 'Asc'); 
        $this->db->limit(4);

//        if($params['limit']){
//            $this->db->limit($params['limit'],$params['start']);
//        }
       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
    
   function getTableValue($select='*',$table,$where=null,$multiple=false){
        $this->db->select($select);
        $this->db->from($table);
        if($where){
            $this->db->where($where);
        }
        $res = $this->db->get();
		
        //echo $this->db->last_query();exit;

        if($multiple){
            return $res->result_array();
        }
        else{
            return $res->row_array();
        }
    }
}
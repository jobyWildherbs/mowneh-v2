<?php
/**
 * Description of FilterModel
 *
 * @author wildherbs-user
 */
class CategoryModel extends CI_Model {

    public function __construct() {
        
    }
    
    public function selectAll($params){ 
        $this->db->select('cat.categoryID, catDet.name,(SELECT name FROM ec_category_detail WHERE categoryID=cat.parentID AND languageID='.$params['languageID'].') as parentName,cat.dateAdded,cat.status');
        $this->db->from('ec_category cat');
        $this->db->join('ec_category_detail catDet','cat.categoryID = catDet.categoryID AND catDet.languageID='.$params['languageID'], 'left');
        $this->db->join('ec_category parCat','parCat.categoryID = cat.categoryID', 'left');
//        $this->db->join('ec_product_to_category partoCat','partoCat.categoryID = cat.categoryID', 'left');
        if(isset($params['name'])){
           $this->db->where('catDet.name LIKE', '%'.$params['name'].'%');
        }
        $this->db->where('cat.status!=', 'Deleted');
        $this->db->order_by('cat.categoryID','DESC');
//        $this->db->order_by('cat.sortOrder','DESC');
        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
        $resultSet  = $this->db->get();
       // print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function selectCategory($id='',$languageID=''){
        $this->db->select('cat.categoryID, cat.pageKey,cat.parentID,cat.sortOrder, catDet.name,(SELECT name FROM ec_category_detail WHERE categoryID=cat.parentID AND languageID='.$languageID.') as parentName,
            (SELECT pageKey FROM ec_category WHERE categoryID=cat.parentID AND languageID='.$languageID.') as parentPageKey,
                          cat.dateAdded,cat.status,catDet.description,catDet.metaTitle,catDet.metaDescription,catDet.metaKeyword,catDet.image,cat.banner');
        $this->db->from('ec_category cat');
        $this->db->join('ec_category_detail catDet','cat.categoryID = catDet.categoryID AND catDet.languageID='.$languageID, 'left');
        $this->db->join('ec_category parCat','parCat.categoryID = cat.categoryID', 'left');
        $this->db->where('cat.status!=', 'Deleted');
        if($id!=='') {
            $this->db->where('cat.categoryID',$id);
        }
        $this->db->order_by('cat.sortOrder','ASC');
        $resultSet  = $this->db->get();
        if($id!=''){
          return $resultSet->row_array();  
        }
        return $resultSet->result_array();
    }
     public function selectActiveCategory($id='',$languageID=''){
        $this->db->select('cat.categoryID, cat.pageKey,cat.parentID,cat.sortOrder, catDet.name,(SELECT name FROM ec_category_detail WHERE categoryID=cat.parentID AND languageID='.$languageID.') as parentName,
            (SELECT pageKey FROM ec_category WHERE categoryID=cat.parentID AND languageID='.$languageID.') as parentPageKey,
                          cat.dateAdded,cat.status,catDet.description,catDet.metaTitle,catDet.metaDescription,catDet.metaKeyword,catDet.image,cat.banner');
        $this->db->from('ec_category cat');
        $this->db->join('ec_category_detail catDet','cat.categoryID = catDet.categoryID AND catDet.languageID='.$languageID, 'left');
        $this->db->join('ec_category parCat','parCat.categoryID = cat.categoryID', 'left');
        $this->db->where('cat.status=', 'Active');
        if($id!=='') {
            $this->db->where('cat.categoryID',$id);
        }
        $this->db->order_by('cat.sortOrder','ASC');
        $resultSet  = $this->db->get();
        if($id!=''){
          return $resultSet->row_array();  
        }
        //print_r($resultSet->result_array());
        return $resultSet->result_array();
    }
    
    public function selectFilterNames($id) {
        $this->db->select('fD.filterID as ID, fD.languageID as langID, fD.name as name, f.sortOrder as sortOrder');
        $this->db->from('ec_filter f');
        $this->db->join('ec_filter_detail fD', 'f.filterID = fD.filterID', 'left');
        $this->db->where('f.filterGroupID', $id);
        //$this->db->order_by();
        //$this->db->limit();
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function getAllFilters($where='', $limit=''){
        $this->db->select('fD.filterID as filterID,fD.languageID as languageID, fD.filterGroupID as filterGroupID ,fD.name as filterName,f.sortOrder filterOrder, fGD.name as filterGroupName , fG.sort_order as filterGroupOrder');
        $this->db->from('ec_filter_detail fD');
        $this->db->join('ec_filter f','fD.filterID = f.filterID');
        $this->db->join('ec_filter_group fG','fD.filterGroupID = fG.filterGroupID');
        $this->db->join('ec_filter_group_detail fGD','fD.filterGroupID = fGD.filterGroupID');
        //$this->db->where();
        $this->db->order_by('f.sortOrder','ASC');
        //$this->db->limit();
        $query  = $this->db->get();
        return $query->result_array();
    }
    
    public function getActiveCategory($languageID=''){
        $this->db->select('cat.categoryID,catDet.name');
        $this->db->from('ec_category cat');
        $this->db->join('ec_category_detail catDet','cat.categoryID = catDet.categoryID AND catDet.languageID='.$languageID, 'left');
        $this->db->where('cat.status', 'Active');
        $this->db->order_by('cat.sortOrder','ASC');
        $resultSet  = $this->db->get();
        return $resultSet->result_array();
    }
    
    public function selectCategoryByParent($parentID='',$languageID=''){
        
        $this->db->select('cat.categoryID,catDet.name,catDet.image,cat.banner');
        $this->db->from('ec_category cat');
        $this->db->join('ec_category_detail catDet','cat.categoryID = catDet.categoryID AND catDet.languageID='.$languageID, 'left');
        if($parentID!='')
             $this->db->where('cat.parentID', $parentID);
        else
            $this->db->where('cat.parentID', 0);
            
        $this->db->where('cat.status', 'Active');
        $this->db->order_by('cat.sortOrder','ASC');
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function categoryExport($params){
        //echo $params['fields']; exit;
        $parentQuery    =   '';
        $languageID     =   $params['languageID'];
        if(strpos($params['fields'], 'category.parentName') !== false){
            $parentQuery    =   ",(SELECT name FROM ec_category_detail WHERE categoryID=category.parentID AND languageID='.$languageID.') as parentName";
            $params['fields']    =   str_replace(',category.parentName', $parentQuery, $params['fields']);
        }
        if(strpos($params['fields'], 'category_filter.filterID') !== false){
            $filterQuery    =   ",(SELECT  GROUP_CONCAT(FGD.name SEPARATOR ', ') FROM ec_category_filter CF LEFT JOIN ec_filter_group_detail FGD ON (FGD.filterGroupID=CF.filterID AND  FGD.languageID='.$languageID.') WHERE  CF.categoryID=category.categoryID) as filter";
            $params['fields']    =   str_replace(',category_filter.filterID', $filterQuery, $params['fields']);
        }
       
        $this->db->select($params['fields']);
        $this->db->from('ec_category category');
        $this->db->join('ec_category_detail category_detail','category.categoryID = category_detail.categoryID AND category_detail.languageID='.$languageID, 'left');
        $this->db->where('category.status=', 'Active');
        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
        $this->db->order_by('category.sortOrder','ASC');
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function createPageKey($value){
        $pageKey    = preg_replace("/[^a-zA-Z^0-9]+/", "-", trim(strtolower($value))); 
        //echo $pageKey;exit;
        $this->db->select('pageKey');
        $this->db->from('ec_category');
        $this->db->where('pageKey', $pageKey);
        $this->db->where('status!=', 'Deleted');
        $resultSet  = $this->db->get();
        $checkVal   =   $resultSet->row_array();
        if(!empty($checkVal)){
           $pageKey =   $pageKey.'-'.uniqid();
        }
        return $pageKey;
    }
    
    public function importData($data,$languageID){ 
        $this->load->library('Import');
        $this->load->model('GeneralModel', 'generalModel');
        $rowCount   =   2;
        foreach($data as $key=>$dataVal){
            $uploadErr  =   0;
            if(@$dataVal['status'])
                $catategory['status']            = $dataVal['status'];
            if(@$dataVal['image'])
                $catategory['image']            = $dataVal['image'];
            if(@$dataVal['banner'])
                $catategory['banner']            = $dataVal['banner'];
            if(@$dataVal['sortOrder'])
                $catategory['sortOrder']         = $dataVal['sortOrder'];
            if(@$dataVal['name']){
                $catDetail['name']               = $dataVal['name'];
                $pageKey    =   $this->createPageKey($dataVal['name']);
                $catategory['pageKey']  =    $pageKey;
                
                $importKeyInsert  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($dataVal['name'])));
                $catategory['importKey'] =   $importKeyInsert;
            }
            
            $parentCategoryID   =   "";
            if(@$dataVal['parentCategory']){
                $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($dataVal['parentCategory'])));
                $parentCategoryID   =   $this->generalModel->getFieldValue('categoryID', 'ec_category','importKey="'.$importKey.'" AND status="Active"');
                if($parentCategoryID==''){
                    $uploadErr  =   1;
                    $errorMsg[]   =   $this->import->setRowMessage('category','parentCategory',$dataVal['parentCategory'],$rowCount);
                }
                
            }
            $catategory['parentID']          = $parentCategoryID;
            if(@$dataVal['description'])
                $catDetail['description']        = $dataVal['description'];
            if(@$dataVal['metaKeyword'])
                $catDetail['metaKeyword']        = $dataVal['metaKeyword'];
            if(@$dataVal['metaTitle'])
                $catDetail['metaTitle']          = $dataVal['metaTitle'];
            if(@$dataVal['metaDescription'])
                $catDetail['metaDescription']          = $dataVal['metaDescription'];
            
            if(@$dataVal['filter']){
                $filter['filter']        = $dataVal['filter'];
                $filterCheck  =   explode("~",$filter['filter']);

                    foreach($filterCheck as $filterGroup){
                        $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($filterGroup)));
                        $filterGroupID   =   $this->generalModel->getFieldValue('filterGroupID', 'ec_filter_group','importKey="'.$importKey.'" AND status="Active"');
                         if($filterGroupID==''){
                            $uploadErr  =   1;
                            $errorMsg[]   =   $this->import->setRowMessage('category','filter',$filterGroup,$rowCount);
                        }
                    }
            }
            if(@$dataVal['name_ar'])
                $catDetail_ar['name']         = $dataVal['name_ar'];
           
            if(@$dataVal['description_ar'])
                $catDetail_ar['description']         = $dataVal['description_ar']; 
            
            
            if(@$dataVal['metaTitle_ar'])
                $catDetail_ar['metaTitle']         = $dataVal['metaTitle_ar'];
            
            if(@$dataVal['metaDescription_ar'])
                $catDetail_ar['metaDescription']          = $dataVal['metaDescription_ar'];
            
            
            if(@$dataVal['metaKeyword_ar'])
                $catDetail_ar['metaKeyword']         = $dataVal['metaKeyword_ar'];
           
            $catategory['dateAdded']             = date('Y-m-d H:i');
            
            if($uploadErr==0){
                if(!empty($catategory)){
                    $this->db->insert('ec_category', $catategory);
                    $categoryID =   $this->db->insert_id();
                    $catDetail['categoryID']        = $categoryID;
                }
                if(!empty($catDetail)){
                    $catDetail['languageID']    =   1;
                    $this->db->insert('ec_category_detail', $catDetail);
                }

                if(!empty($catDetail_ar)){
                    $catDetail_ar['categoryID']        = $categoryID;
                    $catDetail_ar['languageID']    =   2;
                    $this->db->insert('ec_category_detail', $catDetail_ar);
                }

                    $level = 0;
                    if($parentCategoryID){
                        $categoryPath = $this->generalModel->getTableValueWithLimit('*', 'ec_category_path', array('categoryID' => $parentCategoryID), array('level', 'asc'));
                        foreach ($categoryPath as $path) {
                            $insert = array(
                                'categoryID' => $categoryID,
                                'pathID' => $path['pathID'],
                                'level' => $level
                            );

                            $this->generalModel->insertValue('ec_category_path', $insert);
                            $level++;
                        } 
                    }
                    $insert = array(
                        'categoryID' => $categoryID,
                        'pathID' => $categoryID,
                        'level' => $level
                    );
                    $this->generalModel->insertValue('ec_category_path', $insert);


                if(!empty($filter)){
                    $filter  =   explode("~",$filter['filter']);

                    foreach($filter as $filterGroup){
                        $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($filterGroup)));
                        $filterGroupID   =   $this->generalModel->getFieldValue('filterGroupID', 'ec_filter_group','importKey="'.$importKey.'" AND status="Active"');
                        $filterData    =   array();
                        $filterData['categoryID']    =   $categoryID;
                        $filterData['filterID']      =   $filterGroupID;
                        $this->db->insert('ec_category_filter', $filterData);
                    } 
                }
            }
            $rowCount++;
        }
        if(!empty($errorMsg)){
            return $errorMsg;
        }
        return true;
    }
    
    public function selectFooterCategory($params){ 
        $this->db->select('cat.categoryID,cat.pageKey,catDet.name,cat.dateAdded,cat.status');
        $this->db->from('ec_category cat');
        $this->db->join('ec_category_detail catDet','cat.categoryID = catDet.categoryID AND catDet.languageID='.$params['languageID'], 'left');
        $this->db->join('ec_category parCat','parCat.categoryID = cat.categoryID', 'left');
        
        $this->db->where('cat.parentID', 0);
        
        $this->db->where('cat.status', 'Active');
        $this->db->order_by('cat.sortOrder','ASC');
//        $this->db->order_by('cat.sortOrder','DESC');
        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
}

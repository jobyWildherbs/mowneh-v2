<?php
class BannerModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('ec_banner.bannerID,ec_banner_detail.title,ec_banner.image,ec_banner.sortOrder,ec_banner.status');
        $this->db->from('ec_banner');
        $this->db->join('ec_banner_detail','ec_banner.bannerID = ec_banner_detail.bannerID AND ec_banner_detail.languageID='.$params['languageID'],'left');
         if($params['title']){
            $this->db->where('ec_banner_detail.title LIKE', '%'.$params['title'].'%');
        }
        $this->db->where('ec_banner.status!=', 'Deleted');
       
        $this->db->order_by('bannerID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
   function getTableValue($select='*',$table,$where=null,$multiple=false){
        $this->db->select($select);
        $this->db->from($table);
        if($where){
            $this->db->where($where);
        }
        $res = $this->db->get();
		
        //echo $this->db->last_query();exit;

        if($multiple){
            return $res->result_array();
        }
        else{
            return $res->row_array();
        }
    }
         public function selectBanner($languageID=''){
        $this->db->select('bann.bannerID,bannDet.title,bannDet.titleColor,bannDet.description,bann.link,bannDet.linkOnButton,bann.image,bann.sortOrder,bann.status');
        $this->db->from('ec_banner bann');
        $this->db->join('ec_banner_detail bannDet','bann.bannerID = bannDet.bannerID AND bannDet.languageID='.$languageID,'left');
//         if($params['title']){
//            $this->db->where('ec_banner_detail.title LIKE', '%'.$params['title'].'%');
//        }
        $this->db->where('bann.status!=', 'Deleted');
        $this->db->where('bann.status!=', 'Inactive');
       
        $this->db->order_by('sortOrder', 'Asc'); 
        $this->db->limit(4);

//        if($params['limit']){
//            $this->db->limit($params['limit'],$params['start']);
//        }
       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
}
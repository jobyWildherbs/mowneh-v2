<?php

class WarehouseModel extends CI_Model {

    public function selectAll($params) {
        $this->db->select('warehouseID,name,status');
        $this->db->from('ec_warehouse');
         if($params['name']){
            $this->db->where('ec_warehouse.name LIKE', '%'.$params['name'].'%');
        }
        $this->db->where('ec_warehouse.status!=', 'Deleted');

        $this->db->order_by('warehouseID', 'DESC');

        if ($params['limit']) {
            $this->db->limit($params['limit'], $params['start']);
        }

        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }

    function getTableValue($select = '*', $table, $where = null, $multiple = false) {
        $this->db->select($select);
        $this->db->from($table);
        if ($where) {
            $this->db->where($where);
        }
        $res = $this->db->get();

        //echo $this->db->last_query();exit;

        if ($multiple) {
            return $res->result_array();
        } else {
            return $res->row_array();
        }
    }
    
    function getWarehouseCityInfo($warehouseID = 1){
        $this->db->select('wdl.cityID,cd.name,cd.name');
        $this->db->from('ec_warehouse_delivery_location wdl');
        $this->db->join('ec_city_detail cd','cd.cityID = wdl.cityID');
        $this->db->join('ec_cities c','c.cityID = cd.cityID');
        $this->db->where('wdl.warehouseID', $warehouseID);
        $this->db->where('c.status=','Active');
        $this->db->group_by('cd.cityID');
        $res = $this->db->get()->result_array();
        return $res;
    }
    
    function getAllActiveCities($language=1){
        $this->db->select('c.cityID as mID,cd.name as municipality');
        $this->db->from('ec_cities c');
        $this->db->join('ec_city_detail cd','cd.cityID = c.cityID AND cd.languageID='.$language);
        $this->db->where('c.municipalityID=','0');
        $this->db->where('c.status=','Active');
        $res = $this->db->get()->result_array();
        foreach($res as $key=>$resData){
            $this->db->select('c.cityID,c.municipalityID,c.type,cd.name,cd.name,cd.zone');
            $this->db->from('ec_cities c');
            $this->db->join('ec_city_detail cd','cd.cityID = c.cityID AND cd.languageID='.$language);
            $this->db->where('c.municipalityID=',$resData['mID']);
            $this->db->where('c.status=','Active');
            $res[$key]['city'] = $this->db->get()->result_array();
        }
        return $res;
    }
    
     function getCityByCountry($countryID = '',$language=''){
        $this->db->select('city.cityID,cityDet.name');
        $this->db->from('ec_cities city');
        $this->db->join('ec_city_detail cityDet','cityDet.cityID = city.cityID AND cityDet.languageID='.$language,'left');
        $this->db->where('city.countryID', $countryID);
        $this->db->where('city.status','Active');
        $res = $this->db->get()->result_array();
       // echo $this->db->last_query();exit;
        return $res;
    }
    
    function warehouseCityList($params=array()){
        $this->db->select('wc.wharehouseCityID,wc.cityID,wc.expressDeliveryTime,wc.expressDeliveryCharge,wc.normalDeliveryTime,wc.normalDeliveryCharge,cityDet.name,wc.status');
        $this->db->from('ec_warehouse_delivery_location wc');
        $this->db->join('ec_city_detail cityDet','cityDet.cityID = wc.cityID AND cityDet.languageID='.$params["languageID"],'inner');
         $this->db->where('wc.status!=','Deleted');
         if($params['cityName']){
              $this->db->where('cityDet.name LIKE', '%'.$params['cityName'].'%');
         }else{
         if (@$params['limit']) {
            $this->db->limit($params['limit'], $params['start']);
        }
        }
        $res = $this->db->get()->result_array();
       // echo $this->db->last_query();exit;
        return $res;
    }

}

<?php

/**
 * Description of HomeModel
 *
 * @author wildherbs-user
 */
class HomeModel extends CI_Model {

    //put your code here
    public function getLatest($params) {
        extract($params);
        $this->db->select('*');
        $this->db->from('ec_order o');
        $this->db->join('ec_order_status os', 'o.orderStatusID = os.orderStatusID', 'left');
        $this->db->join('ec_order_status_detail osd', 'o.orderStatusID = osd.orderStatusID', 'left');
        if ($where != '') {
            $this->db->where($where);
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $this->db->order_by('o.dateAdded', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function recentActivity($params) {
        $this->db->select('ua.userID, ua.usedClass, ua.usedMethod, ua.activityTitle, ua.updatedTime, u.firstname, u.lastname');
        $this->db->from('ec_user_activity ua');
        $this->db->join('ec_user u', 'u.userID = ua.userID');
        if ($params['limit'] != '') {
            $this->db->limit($params['limit']);
        } else {
            $this->db->limit(10);
        }
        $this->db->order_by('updatedTime','DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

}

<?php
class InformationModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('ec_information.informationID,ec_information.pageKey,ec_information_detail.title,ec_information.status');
        $this->db->from('ec_information');
        $this->db->join('ec_information_detail','ec_information.informationID = ec_information_detail.informationID AND ec_information_detail.languageID='.$params['languageID'],'left');
         if($params['title']){
            $this->db->where('ec_information_detail.title LIKE', '%'.$params['title'].'%');
        }
        $this->db->where('ec_information.status!=', 'Deleted');
       
        $this->db->order_by('informationID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
   function getTableValue($select='*',$table,$where=null,$multiple=false){
        $this->db->select($select);
        $this->db->from($table);
        if($where){
            $this->db->where($where);
        }
        $res = $this->db->get();
        if($multiple){
            return $res->result_array();
        }
        else{
            return $res->row_array();
        }
    }
    public function selectInfo($key,$language){
        $this->db->select('i.informationID, iD.title, iD.description, i.metaTitle, i.metaDescription, i.metaKeyword, i.sortOrder');
        $this->db->from('ec_information i');
        $this->db->join('ec_information_detail iD','i.informationID = iD.informationID','left');
        $this->db->where('i.pageKey',$key);
        $this->db->where('iD.languageID',$language);
        $this->db->where('i.status','Active');
        $res = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $res->row_array();
    }
}
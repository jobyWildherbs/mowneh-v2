<?php

class CartModel extends CI_Model {

    public function initialize($params) {
        extract($params);
        // Remove all the expired carts with no customer ID
        $this->db->query("DELETE FROM ec_cart WHERE (apiID > '0' OR customerID = '0') AND dateAdded < DATE_SUB(NOW(), INTERVAL 2 HOUR)");
        /*if ($customerID) {
            // We want to change the session ID on all the old items in the customers cart
            $this->db->query("UPDATE ec_cart SET sessionID = " . $this->db->escape($sessionID) . " WHERE apiID = '0' AND customerID = '" . (int) $customerID . "'");
        }*/
    }

    public function getCartCount($params) {
        extract($params);
        $sessionID = $this->db->escape($sessionID);
        if(@$option){
            $option = (($option)) ? $this->db->escape(json_encode($option)) : '';
        }else{
            $option ="''";
        }
        $sql = "SELECT COUNT(*) AS total FROM ec_cart WHERE apiID = '" . $apiID . "' AND customerID = '" . (int) $customerID . "' AND sessionID = " . $sessionID . " AND productID = '" . (int) $product_id . "' AND recurringID = '" . (int) $recurring_id . "'";
        //if ($option) {
            $sql .= " AND `option` = " . $option;
        //}
        $query = $this->db->query($sql);
        //echo $this->db->last_query();
        $result = $query->row_array();
        return $result['total'];
    }

    public function addCart($params) { 
        extract($params); 
        $sessionID = $this->db->escape($sessionID);
        //print_r($params);
        //exit;
        $options = (($option)) ? $this->db->escape(json_encode($option)) : '';
        if($option){
            $option =   '"'.$option.'"';
        }
       
        $this->db->query("INSERT ec_cart SET apiID = '" . $apiID . "', customerID = '" . (int) $customerID . "', sessionID = " . $sessionID . ", productID = '" . (int) $product_id . "', recurringID = '" . (int) $recurring_id . "', `option` = " . json_encode($option) . ", quantity = '" . (int) $quantity . "', dateAdded = NOW()");
    }

    public function updateCart($params) {
        extract($params);
        $sessionID = $this->db->escape($sessionID);
        $set="";
        if(@$option){
            $options = (($option)) ? $this->db->escape(json_encode($option)) : '';
            $set ="AND `option` = '" . json_encode($option) . "'";
        }
        
        $this->db->query("UPDATE ec_cart SET quantity = (quantity + " . (int) $quantity . ") WHERE apiID = '" . $apiID . "' AND customerID = '" . (int) $customerID . "' AND sessionID = " . $sessionID . " AND productID = '" . (int) $product_id . "' AND recurringID = '" . (int) $recurring_id . "' " . $set);
    }

    public function updateCartQuantity($params) {
        extract($params);
        $sessionID = $this->db->escape($sessionID);
        $set ="";
        if(@$option){
            $set ="AND `option` = '" . $option . "'";
        }
        $this->db->query("UPDATE ec_cart SET quantity = '" . (int) $quantity . "' WHERE productID = '" . (int) $productID . "' AND apiID = '" . $apiID . "' AND customerID = '" . (int) $customerID . "' AND sessionID = " . $sessionID . "". $set);
    }
    
     public function updateCartQuantityForAPI($params) {
        extract($params);
       
        $this->db->query("UPDATE ec_cart SET quantity = '" . (int) $quantity . "' WHERE productID = '" . (int) $productID . "' AND apiID = '" . $apiID . "' AND customerID = '" . (int) $customerID . "'");
    }

    public function removeFromCart($params) {
        $this->db->delete('ec_cart', $params);
    }

    public function getCartItems($params, $multiple = true) {
        $this->db->select('*');
        $this->db->from('ec_cart');
        if ($params) {
            $this->db->where($params);
        }
        $res = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($multiple) {
            return $res->result_array();
        } else {
            return $res->row_array();
        }
    }
    public function getCartOptionItems($params,$productId, $multiple = true) {
        $this->db->select('*');
        $this->db->from('ec_cart');
        if ($params) {
            $this->db->where($params);
        }
        $res = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($multiple) {
            return $res->result_array();
        } else {
            return $res->row_array();
        }
    }

    public function getProductDiscount($params) {
        extract($params);
        $day = date('Y-m-d');
        $result = $this->db->query("SELECT price,quantity,dateStart,dateEnd,type FROM ec_product_discount WHERE productID = '" . (int) $productID . "' AND price > 0 AND customer_group_id = '" . (int) $customerGroupID . "' AND ((dateStart = '0000-00-00' OR dateStart <= '".$day."') AND (dateEnd = '0000-00-00' OR dateEnd >= '".$day."')) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");
        $res = $result->row_array();
        return $res;
    }
    
     public function decreaseProductQuentity($productID=0,$quentity=0) {
        $this->db->query("UPDATE ec_product SET quantity = (quantity - " . (int) $quentity . ") WHERE productID = '" . (int) $productID . "'");
    }
    
    public function getProductOrderQuantity($productID,$start,$end){
        $sql = "SELECT sum(quantity) as orderQuantity from ec_order_product op inner join ec_order o on (op.orderID=o.orderID) where op.productID = ".$productID;
        if($end!='0000-00-00'){
            $sql .= " and o.dateAdded < '".$end."'";
        }
        if($start!='0000-00-00'){
            $sql .= " and o.dateAdded > '".$start."'";
        }
        $sql .=" group by op.productID";
        $result = $this->db->query($sql)->row_array();
        if($result){
            return $result['orderQuantity'];
        }else{
            return 0;
        }
    }
    public function getProductOption($orderID) {
       
        $sql = "SELECT DISTINCT(productOptionID) AS pID,orderProductID FROM ec_order_option WHERE orderID = '" . $orderID . "'";
        
        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        $result = $query->result_array();
        return $result;
        
    }
      public function getProductOptionByProductId($orderID,$orderProductID) {
       
        $sql = "SELECT DISTINCT(productOptionID) AS pID,orderProductID FROM ec_order_option WHERE productOrderID = '" . $orderProductID . "' and  orderID = '" . $orderID . "'";
        
        $query = $this->db->query($sql);
        //echo $this->db->last_query(); exit;
        $result = $query->result_array();
        return $result;
        
    }
    public function decreaseProductOptionQuentity($productID=0,$quentity=0,$pID=0) { 
        $this->db->query("UPDATE ec_product_option SET quantity = (quantity - " . (int) $quentity . ") WHERE productOptionID='".$pID."' and productID = '" . (int) $productID . "'");
    }
    
    public function getProductOptionQuantity($params) {
        extract($params);
        
        $result = $this->db->query("SELECT quantity FROM ec_cart c WHERE apiID = '" . $apiID . "' AND customerID = '" . (int) $customerID . "' AND sessionID = '". $sessionID."' AND productID = '" . (int) $productID . "' AND c.option LIKE '%productOptionID-".$productOptionInfo."%'");
        $res = $result->row_array();
        if (is_countable($res)) {
            return array_values($res)[0];
        } else {
            return '';
        }
    }
    
    public function getProductOptionQuantityForAPI($params) {
        extract($params);
        
        $result = $this->db->query("SELECT quantity FROM ec_cart c WHERE customerID = '" . (int) $customerID . "' AND productID = '" . (int) $productID . "' AND c.option LIKE '%productOptionID-".$productOptionInfo."%'");
        $res = $result->row_array();
        if (is_countable($res)) {
            return array_values($res)[0];
        } else {
            return '';
        }
    }
}

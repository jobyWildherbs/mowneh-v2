<?php
class UserModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('user.userID,user.email,user.userName,user.firstname,user.lastname,user.status,user.lastLogin,user.dateAdded,userGrp.name as role');
        $this->db->from('ec_user user');
        $this->db->join('ec_user_group userGrp','userGrp.userGroupID = user.userGroupID','left');
        $this->db->where('user.status!=', 'Deleted');
        if($params['email']!=''){
            $this->db->where('user.email LIKE','%'.$params['email'].'%');
        }
        $this->db->order_by('user.userID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
    
    public function selectActiveUser($role  =   ""){
        $this->db->select('user.userID,user.email,user.userName,user.firstname,user.lastname,user.status,user.lastLogin,user.dateAdded,userGrp.name as role');
        $this->db->from('ec_user user');
        $this->db->join('ec_user_group userGrp','userGrp.userGroupID = user.userGroupID','left');
        $this->db->where('user.status', 'Active');
        $this->db->where('user.userGroupID', $role);
        $this->db->order_by('user.firstname', 'ASC'); 

       
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
       public function selectDriver($params){
        $this->db->select('user.userID,user.email,user.userName,user.firstname,user.lastname,user.status,user.lastLogin,user.dateAdded,userGrp.name as role');
        $this->db->from('ec_user user');
        $this->db->join('ec_user_group userGrp','userGrp.userGroupID = user.userGroupID','left');
        $this->db->where('user.status!=', 'Deleted');
        $this->db->where('user.userGroupID=', 5);
        if($params['email']!=''){
            $this->db->where('user.email LIKE','%'.$params['email'].'%');
        }
        $this->db->order_by('user.userID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
        $query = $this->db->get();
        // print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
}

<?php
/**
 * Description of OptionModel
 *
 * @author wildherbs-user
 */
class OptionModel extends CI_Model {

    public function __construct() {
        
    }
      public function selectAllOption($params){
        $this->db->select('o.optionID as ID, oD.languageID as langID, o.type as type , o.showAs, oD.name as name, o.sortOrder sortOrder');
        $this->db->from('ec_option o');
        $this->db->join('ec_option_detail oD','o.optionID = oD.optionID AND oD.languageID='.$params['languageID']);
        $this->db->order_by('o.sortOrder','ASC');
        //$this->db->limit();
        //print_r($params);
        if(isset($params['name'])){
           $this->db->where('oD.name LIKE', '%'.$params['name'].'%');
        }
        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
        $this->db->where('o.status!=', 'Deleted');
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        
        return $resultSet->result_array();
    }
    public function selectOption($id='',$languageID){
        $this->db->select('o.optionID as ID, oD.languageID as langID, o.type as type , o.showAs, oD.name as name, o.sortOrder sortOrder');
        $this->db->from('ec_option o');
        $this->db->join('ec_option_detail oD','o.optionID = oD.optionID AND oD.languageID='.$languageID);
        if($id!=='') {
            $this->db->where('o.optionID',$id);
        }
        $this->db->order_by('o.sortOrder','ASC');
        //$this->db->limit();
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        if($id!=''){
          return $resultSet->row_array();  
        }
        return $resultSet->result_array();
    }

    public function selectOptionNames($id,$languageID) {
        $this->db->select('oD.optionValueID as ID, oD.languageID as langID, oD.name as name, o.image, o.sortOrder as sortOrder');
        $this->db->from('ec_option_value o');
        $this->db->join('ec_option_value_detail oD', 'o.optionValueID = oD.optionValueID AND oD.languageID='.$languageID);
        $this->db->where('o.optionID', $id);
        //$this->db->order_by();
        //$this->db->limit();
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
     public function selectOptionDetail($productOptionID,$languageID,$optionValueId="") {
        $this->db->select('oD.optionValueID as ID, ,oD.languageID as langID,opD.name as oName, oD.name as name, o.image, otn.optionID as OID ,otn.type, o.sortOrder as sortOrder');
        $this->db->from('ec_option_value o');
        $this->db->join('ec_product_option_value oDv', 'o.optionValueID = oDv.optionValueID ');
        $this->db->join('ec_option_value_detail oD', 'o.optionValueID = oD.optionValueID AND oD.languageID='.$languageID);
        $this->db->join('ec_option_detail opD','oD.optionID = opD.optionID');
        $this->db->join('ec_option otn','otn.optionID = opD.optionID');
        $this->db->where('oDv.productOptionID', $productOptionID);
        if($optionValueId){
            $this->db->where('oDv.optionValueID', $optionValueId);
        }
        //$this->db->order_by();
        //$this->db->limit();
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); 
        //exit;
        return $resultSet->result_array();
    }
    public function selectOptionDetailByVal($id,$languageID) {
        $this->db->select('oD.optionValueID as ID, oD.languageID as langID,oDv.productOptionID,opD.name as oName, oD.name as name, o.image, otn.optionID as OID ,otn.type, o.sortOrder as sortOrder');
        $this->db->from('ec_option_value o');
        $this->db->join('ec_product_option_value oDv', 'o.optionValueID = oDv.optionValueID ');
        $this->db->join('ec_option_value_detail oD', 'o.optionValueID = oD.optionValueID AND oD.languageID='.$languageID);
        $this->db->join('ec_option_detail opD','oD.optionID = opD.optionID');
        $this->db->join('ec_option otn','otn.optionID = opD.optionID');
        $this->db->where('o.optionValueID', $id);
        //$this->db->order_by();
        //$this->db->limit();
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
      public function selectOptionDetailByProductOptionVal($productOptionID,$languageID,$optionValueID="") {
        $this->db->select('oD.optionValueID as ID, oD.languageID as langID,oDv.productOptionID,opD.name as oName, oD.name as name, o.image, otn.optionID as OID ,otn.type, o.sortOrder as sortOrder');
        $this->db->from('ec_option_value o');
        $this->db->join('ec_product_option_value oDv', 'o.optionValueID = oDv.optionValueID ');
        $this->db->join('ec_option_value_detail oD', 'o.optionValueID = oD.optionValueID AND oD.languageID='.$languageID);
        $this->db->join('ec_option_detail opD','oD.optionID = opD.optionID');
        $this->db->join('ec_option otn','otn.optionID = opD.optionID');
        $this->db->where('oDv.productOptionID', $productOptionID);
        if($optionValueID){
            $this->db->where('oDv.optionValueID', $optionValueID);
        }
        //$this->db->order_by();
        //$this->db->limit();
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    public function selectAllActiveOption($languageID=1){
        $this->db->select('o.optionID, o.type, o.showAs, oD.name, o.sortOrder');
        $this->db->from('ec_option o');
        $this->db->join('ec_option_detail oD','o.optionID = oD.optionID AND oD.languageID="'.$languageID.'"');
        $this->db->order_by('o.sortOrder','ASC');      
        $this->db->where('o.status', 'Active');
        $resultSet  = $this->db->get();
        return $resultSet->result_array();
    }
    
    public function selectActiveOptionByID($optionID='',$languageID=1){
        $this->db->select('o.optionID,oD.name, o.sortOrder');
        $this->db->from('ec_option o');
        $this->db->join('ec_option_detail oD','o.optionID = oD.optionID AND oD.languageID="'.$languageID.'"');
        $this->db->order_by('o.sortOrder','ASC');      
        $this->db->where('o.status', 'Active');
        $this->db->where('o.optionID', $optionID);
        $resultSet  = $this->db->get();
        return $resultSet->row_array();
    }
    
    public function selectOptionValueByOptionID($optionID="",$languageID=1){
        $this->db->select('opt.optionValueID, opt.optionID, opt.image, opt.sortOrder,optDet.name');
        $this->db->from('ec_option_value opt');
        $this->db->join('ec_option_value_detail optDet','optDet.optionValueID = opt.optionValueID AND optDet.languageID="'.$languageID.'"');
        $this->db->where('opt.optionID', $optionID);
        $this->db->order_by('opt.sortOrder','ASC');   
        $resultSet  = $this->db->get();
        
        return $resultSet->result_array();
    }
    
    public function selectOptionCombination($params){
        $this->db->select('combinationID,optionID as combinationOptionID,sortOrder');
        $this->db->from('ec_option_combination'); 
         $this->db->where('status', 'Active');
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function selectOptionCombinationName($combinationOptionID,$languageID=1){
        $this->db->select('GROUP_CONCAT(name SEPARATOR " + ") AS optionName');
        $this->db->from('ec_option_detail');
        $this->db->where_in('optionID', $combinationOptionID);
        $this->db->where('languageID', $languageID);
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->row_array();

    }  
    
    
    public function selectOptionCombinationByProductId($productId){ 
        $this->db->select('OC.combinationID,OC.optionID,OC.productID,OC.required,(SELECT GROUP_CONCAT(name SEPARATOR " + ") AS optionName FROM ec_option_detail WHERE FIND_IN_SET(optionID, OC.optionID)) AS combinationName');
        $this->db->from('ec_option_combination OC');
        $this->db->where('OC.productID', $productId);
        $this->db->where('OC.status', 'Active');

        $resultSet  = $this->db->get();
        //print_r($this->db->last_query());exit;
        return $resultSet->row_array();
    } 
    
    public function selectOptionValueByOptionValID($productID,$combination,$languageID=1){
    
     /*
        $this->db->select('productOptionID');
        $this->db->from('ec_product_option');
        $this->db->where('productID', $productID);
        $this->db->where('status', 'Active');
        $resultSet  = $this->db->get();
        
        $productOptions = $resultSet->result_array();
    //echo "<pre>"; print_r($productOptions); exit;
        $exist  =   0;$productOptionId='';
        if($productOptions){
            foreach($productOptions as $key1=>$productOptionsData){
               $productOptionsValueReturn    =   array();
                $this->db->select('optionValueID');
                $this->db->from('ec_product_option_value');
                $this->db->where('productOptionID', $productOptionsData['productOptionID']);
                $this->db->where('productID', $productID);
                $resultSetOption  = $this->db->get();
                $productOptionsValue = $resultSetOption->result_array();
                /*foreach($productOptionsValue as $key2=>$productOptionsValueData){                   
                   $productOptionsValueReturn[]    = $productOptionsValueData['optionValueID'];
                }
                $productOptionsValueReturn[]    =$productOptionsData['productOptionID'];
               
            
                //print_r($productOptionsValueReturn);
            }
        }
         $checkArray =   array_diff_assoc($productOptionsValueReturn, $combination);
        print_r($checkArray);
            if(empty($checkArray)){  
                $exist  = 1;
                $productOptionId=$productOptionsData['productOptionID'];
            }
        print_r($combination); exit;
        if(@$productOptionId !=0){*/
        $this->db->select('po.*');
        $this->db->from('ec_product_option po');
       $this->db->where('po.productOptionID', $combination[0]);
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query());exit;
        return $resultSet->row_array();
    
    }
    public function selectOptionValueByCombinationOptionID($optionID='',$languageID=1){
        $this->db->select('optVal.optionValueID, optVal.optionID, opt.type, optVal.image,optValDet.name');
        $this->db->from('ec_option_value optVal');
        $this->db->join('ec_option_value_detail optValDet','optValDet.optionValueID = optVal.optionValueID AND optValDet.languageID="'.$languageID.'"');
        $this->db->join('ec_option opt','opt.optionID = optVal.optionID');
        $this->db->where('optVal.optionID', $optionID);
        $this->db->order_by('optVal.sortOrder','ASC');   
        $resultSet  = $this->db->get();
        
        return $resultSet->result_array();
    }
    
        public function selectOptionName($optionID,$languageID) {
        $this->db->select('opt.optionID, optDet.name as optionName');
        $this->db->from('ec_option opt');
        $this->db->join('ec_option_detail optDet', 'opt.optionID = optDet.optionID AND optDet.languageID='.$languageID);
        $this->db->where('opt.optionID', $optionID);
        //$this->db->order_by();
        //$this->db->limit();
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->row_array();
    }
   
}

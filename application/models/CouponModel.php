<?php
class CouponModel extends CI_Model {
   public function selectAll($params){
        $this->db->select('*');
        $this->db->from('ec_coupon');
       // $this->db->join('ec_coupon_detail','ec_coupon.couponID = ec_coupon_detail.couponID AND ec_coupon_detail.languageID='.$params['languageID'],'left');
         if($params['name']){
            $this->db->where('ec_coupon.name LIKE', '%'.$params['name'].'%');
        }
        $this->db->where('ec_coupon.status!=', 'Deleted');
       
        $this->db->order_by('couponID', 'DESC'); 

        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
       
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
    
     public function selectCoupon($couponCode){
        $this->db->select('*');
        $this->db->from('ec_coupon');
        $this->db->where('ec_coupon.code', $couponCode);
        $this->db->where('ec_coupon.status=', 'Active');
        $this->db->where ("(ec_coupon.dateStart <= now())");
        $this->db->where ("(ec_coupon.dateEnd >= now())");
        $query = $this->db->get();
        //print_r($this->db->last_query()); exit;
       return $query->row_array();
    }
 
     public function checkOrderExist($customerID) {

        $this->db->select('o.orderID,orTr.paymentStatus');
        $this->db->from('ec_order o');
        $this->db->join('ec_order_transaction orTr', 'o.orderID = orTr.orderID AND (paymentStatus="success" OR paymentStatus="cod")', 'inner');
        $this->db->where('o.customerID', $customerID);
        $query    =    $this->db->get();
       return $query->result_array();
    }
}

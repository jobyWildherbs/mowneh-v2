<?php
/**
 * Description of FilterModel
 *
 * @author wildherbs-user
 */
class FilterModel extends CI_Model {

    public function __construct() {
        
    }
    public function selectFilter($id='',$language='',$title='',$limit='',$start=''){
    
        $this->db->select('fG.filterGroupID as ID, fGD.languageID as langID ,fGD.name as filterName, fG.sort_order sortOrder, fG.status');
        $this->db->from('ec_filter_group fG');
        $this->db->join('ec_filter_group_detail fGD','fG.filterGroupID = fGD.filterGroupID AND fGD.languageID='.$language,'left');
        $this->db->where('fG.status!=', 'Deleted');
        if ($title) {
            $this->db->where('fGD.name LIKE', '%' . $title . '%');
        }
        if($id!=='') {
            $this->db->where('fG.filterGroupID',$id);
        }
        if($limit){
            $this->db->limit($limit, $start);
        }
        //$this->db->order_by('fG.sort_order','ASC');
        $this->db->order_by('fG.filterGroupID','DESC');
        //$this->db->limit();
        $resultSet  = $this->db->get();
//        print_r($this->db->last_query()); exit;
        if($id!=''){
          return $resultSet->row_array();  
        }
        return $resultSet->result_array();
    }
    public function selectFilterNames($id,$language='') {
        $this->db->select('f.filterID as ID, fD.languageID as langID, fD.name as name, f.sortOrder as sortOrder,f.filterGroupID');
        $this->db->from('ec_filter f');
        $this->db->join('ec_filter_detail fD', 'f.filterID = fD.filterID AND fD.languageID='.$language, 'left');
        $this->db->where('f.filterGroupID', $id);
        //$this->db->order_by();
        //$this->db->limit();
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function getAllFilters($where='', $limit='',$language=''){
        $this->db->select('fD.filterID as filterID,fD.languageID as languageID, fD.filterGroupID as filterGroupID ,fD.name as filterName,f.sortOrder filterOrder, fGD.name as filterGroupName , fG.sort_order as filterGroupOrder');
        $this->db->from('ec_filter f');
        $this->db->join('ec_filter_detail  fD','fD.filterID = f.filterID AND fD.languageID='.$language, 'left');
        $this->db->join('ec_filter_group fG','fD.filterGroupID = fG.filterGroupID');
        $this->db->join('ec_filter_group_detail fGD','fD.filterGroupID = fGD.filterGroupID');
        //$this->db->where();
        $this->db->order_by('f.sortOrder','ASC');
        //$this->db->limit();
        $query  = $this->db->get();
//        print_r($this->db->last_query()); exit;
        return $query->result_array();
    }
     public function getFilterDetails($id='',$language=''){
    
        $this->db->select('ec_filter_detail.filterID, ec_filter_detail.languageID');
        $this->db->from('ec_filter_detail');
        $this->db->join('ec_category_filter','ec_filter_detail.filterID = ec_category_filter.filterID AND ec_filter_detail.languageID='.$language,'left');
        //SELECT * FROM ec_category_filter WHERE filterID IN (SELECT filterID FROM ec_filter_detail);
        if($id!=='') {
            $this->db->where('ec_filter_detail.filterID',$id);
        }
        //$this->db->order_by('ec_filter_detail.filterID','ASC');
        //$this->db->limit();
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        if($id!=''){
          return $resultSet->row_array();  
        }
        return $resultSet->result_array();
    }
    
    public function filterExport($params){
        $languageID     =   $params['languageID'];
        //echo "<pre>"; print_r($params['fields']); exit;
        
        if(strpos($params['fields'], 'filter.filterGroupID') !== false){
            $parentQuery    =   ",(SELECT name FROM ec_filter_group_detail WHERE filterGroupID=filter.filterGroupID AND languageID=".$languageID.") as filterGroupName";
            $params['fields']    =   str_replace(',filter.filterGroupID', $parentQuery, $params['fields']);
        }
       
        $this->db->select($params['fields']);
        $this->db->from('ec_filter filter');
        $this->db->join('ec_filter_detail filter_detail','filter_detail.filterID = filter.filterID AND filter_detail.languageID='.$languageID, 'left');
        if($params['limit']){
            $this->db->limit($params['limit'],$params['start']);
        }
        $resultSet  = $this->db->get();
        //print_r($this->db->last_query()); exit;
        return $resultSet->result_array();
    }
    
    public function importFilterGroupData($data,$languageID){
        foreach($data as $key=>$dataVal){
            if(@$dataVal['sort_order'])
                $filter['sort_order']   = $dataVal['sort_order'];
            if(@$dataVal['status'])
                $filter['status']       = $dataVal['status'];
            if(@$dataVal['name']){
                $filterDet['name']      = $dataVal['name'];
                $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($dataVal['name'])));
                $filter['importKey']    =   $importKey;
            }
            if(@$dataVal['name_ar'])
                $filterDet_ar['name']      = $dataVal['name_ar'];
            
            if(!empty($filter)){
                $this->db->insert('ec_filter_group', $filter); 
                $filterGroupID =   $this->db->insert_id();
                
            }
            if(!empty($filterDet)){
                $filterDet['filterGroupID'] =   $filterGroupID;
                $filterDet['languageID']    =   1;
                $this->db->insert('ec_filter_group_detail', $filterDet); 
            }
            if(!empty($filterDet_ar)){
                $filterDet_ar['filterGroupID'] =   $filterGroupID;
                $filterDet_ar['languageID']    =   2;
                $this->db->insert('ec_filter_group_detail', $filterDet_ar); 
            }
           
        }
       return true;
    }
    
    public function importData($data,$languageID){
       // echo "<pre>"; print_r($data); exit;
        $this->load->library('Import');
        $this->load->model('GeneralModel', 'generalModel');
        $rowCount   =   2;
        foreach($data as $key=>$dataVal){
            $uploadErr  =   0;
            if(@$dataVal['filterGroupName']){
                $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($dataVal['filterGroupName'])));
                $filterGroupKey =  $importKey; 
                $filterGroupDet['name']         = $dataVal['filterGroupName'];
                $filterGroup['importKey']       = $importKey;
            }
            $sortOrder  =   "";
            if(@$dataVal['sortOrder']){
                    $filterGroup['sort_order']       = $dataVal['sortOrder'];
                    $sortOrder  =   $dataVal['sortOrder'];
                    
            }
            if(@$dataVal['filterGroupName_ar'])
                    $filterGroupDet_ar['name']       = $dataVal['filterGroupName_ar'];
            
            if(@$dataVal['filterName']){
                    $filter['name']      = $dataVal['filterName'];
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($dataVal['filterName'])));
                    $filterID   =   $this->generalModel->getFieldValue('filterID', 'ec_filter','importKey="'.$importKey.'"');
                    if($filterID!=""){
                        $uploadErr  =   1;
                        $errorMsg[]   =   $this->import->setFilterExistMessage('filter',$dataVal['filterName'],$rowCount);
                    }
            }
            
            if(@$dataVal['filterName_ar'])
                $filter['name_ar'] =    $dataVal['filterName_ar'];
            
            //echo "<pre>"; print_r($filter_ar); exit;
            if($uploadErr==0){
                if($filterGroupKey){
                    $filterGroupID   =   $this->generalModel->getFieldValue('filterGroupID', 'ec_filter_group','importKey="'.$filterGroupKey.'" AND status!="Deleted"');
                    if($filterGroupID==''){
                        if(!empty($filterGroup)){
                            $this->db->insert('ec_filter_group', $filterGroup); 
                            $filterGroupID =   $this->db->insert_id();

                        }
                        if(!empty($filterGroupDet)){
                            $filterGroupDet['filterGroupID']  =  $filterGroupID; 
                            $filterGroupDet['languageID']    =   1;
                            $this->db->insert('ec_filter_group_detail', $filterGroupDet);  
                        }
                        if(!empty($filterGroupDet_ar)){
                            $filterGroupDet_ar['filterGroupID']  =  $filterGroupID; 
                            $filterGroupDet_ar['languageID']    =   2;
                            $this->db->insert('ec_filter_group_detail', $filterGroupDet_ar); 

                        }
                    }
                   
                    if(!empty($filter)){
                        $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($filter['name'])));
                        //$filterID   =   $this->generalModel->getFieldValue('filterID', 'ec_filter','importKey="'.$importKey.'"');
                        $filterVal['filterGroupID']       =   $filterGroupID;
                        $filterVal['sortOrder']           =   $sortOrder;
                        $filterVal['importKey']           =   $importKey;
                        $this->db->insert('ec_filter', $filterVal);
                        $filterID   =     $this->db->insert_id();
                        if($filter['name']){  
                            $filter_eng     =   array();
                            $filter_eng['filterID']         =   $filterID;
                            $filter_eng['languageID']       =   '1';
                            $filter_eng['filterGroupID']    =   $filterGroupID;
                            $filter_eng['name']    =   $filter['name'];
                            $this->db->insert('ec_filter_detail', $filter_eng);
                        }

                        if($filter['name_ar']){
                            $filter_ar     =   array();
                            $filter_ar['filterID']         =   $filterID;
                            $filter_ar['languageID']       =   '2';
                            $filter_ar['filterGroupID']    =   $filterGroupID;
                            $filter_ar['name']              =   $filter['name_ar'];
                            $this->db->insert('ec_filter_detail', $filter_ar);
                        }
                    }
                }
            }
           $rowCount++;
        }
        if(!empty($errorMsg)){
            return $errorMsg;
        }
       return true;
    }
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    protected $data = array();
    protected $siteLanguages = array();
    protected $siteCurrency = "QAR";

    function __construct() {
         
        //echo phpinfo(); exit;
        parent::__construct();
        //echo "<pre>"; print_r($this->session->userdata()); exit;
        $this->load->model('GeneralModel', 'gm');
        $this->load->model('CategoryModel', 'cm');
        $this->load->model('ProductModel', 'pm');
        $this->gm->siteMysqlMode();
        $this->generalSettings();
        $this->data['baseURL'] = base_url();
        $timeZone   =   $this->getSettingValue('time_zone');
        //date_default_timezone_set($timeZone);
        //echo $date = date('m/d/Y h:i:s a', time());
        // $siteAuthentication = $this->session->userdata('SITE_AUTH_PASSWORD');
        // $currentUrl = base_url(uri_string()); 
        // if (strpos($currentUrl,'api') == false) {
        //     if($siteAuthentication!='Yes'){
        //         redirect('Authentication', 'refresh');
        //     }
        // }
       
    }

    protected function render($the_view = NULL, $template = 'master') {
        if ($template == 'json' || $this->input->is_ajax_request()) {
            if ($template == 'json') {
                header('Content-Type: application/json');
                echo json_encode($this->data);
            } else {
                $this->data['the_view_content'] = $this->twig->render($the_view, $this->data);
                echo $this->data['the_view_content'];
            }
        } else {
            $this->data['the_view_content'] = $this->twig->render($the_view, $this->data);
            $this->twig->display('templates/' . $template . '_view', $this->data);
        }
    }

    protected function generalSettings() {
        $this->siteLanguages = $this->gm->getTableValue('*', 'ec_language', array('status' => 1), TRUE);
        $this->data['siteLangauges'] = $this->siteLanguages;
        $this->data['favIcon'] = base_url('uploads/siteInfo/') . $this->getSettingValue('favicon');
        $this->data['logo'] = base_url('uploads/siteInfo/') . $this->getSettingValue('logo');
    }

    function switchLang($lang = "", $manage = false) {
        $lang = ($lang != "") ? $lang : 1;
        if ($manage) {
            $this->session->set_userdata('admin_lang', $lang);
        } else {
            $this->session->set_userdata('site_lang', $lang);
        }
        redirect($_SERVER['HTTP_REFERER']);
    }

    protected function getImageDimension($settingKey = '') {
        if ($settingKey != '') {
            $settingDetails = $this->gm->getTableValue('*', 'ec_settings', array('fieldKey' => $settingKey));
            if ($settingDetails) {
                if (trim($settingDetails['value']) != '') {
                    $value = explode("x", strtolower($settingDetails['value']));
                    foreach ($value as $key => $val) {
                        $value[$key] = trim($val);
                    }
                    return $value;
                } else {
                    return array(0, 0);
                }
            } else {
                return array(0, 0);
            }
        }
        return;
    }

    protected function getSettingValue($settingKey = '') {
     
        if ($settingKey != '') {
            $settingDetails = $this->gm->getTableValue('*', 'ec_settings', array('fieldKey' => $settingKey));
            //echo "<pre>"; print_r($settingDetails);  echo "</pre>";
            if ($settingDetails) {
                if ($settingDetails['isLanguage'] == 'yes') {
                    $value = $this->gm->getFieldValue('value', 'ec_settings_field_details', array('settingsFieldID' => $settingDetails['id'], 'languageID' => $this->siteLanguageID));
                    
                    return $value;
                } else {
                    return $settingDetails['value'];
                }
            }
        }
        return;
    }
    
    protected function getSettingValueForAPI($settingKey = '',$languageID=1) {
     
        if ($settingKey != '') {
            $settingDetails = $this->gm->getTableValue('*', 'ec_settings', array('fieldKey' => $settingKey));
            //echo "<pre>"; print_r($settingDetails);  echo "</pre>";
            if ($settingDetails) {
                if ($settingDetails['isLanguage'] == 'yes') {
                    $value = $this->gm->getFieldValue('value', 'ec_settings_field_details', array('settingsFieldID' => $settingDetails['id'], 'languageID' => $languageID));
                    
                    return $value;
                } else {
                    return $settingDetails['value'];
                }
            }
        }
        return;
    }

    protected function setDirectory($path) {
        if (!is_dir($path)) {
            @mkdir($path, 0777);
        }
    }
    
  protected function sendSMS($message="",$phoneNo=""){
          
       if($message!='' && $phoneNo!=""){
            //echo $message; exit;
            $message    =    urlencode($message);
            $user = "";
            $password = "";
            $senderid = ""; //Your senderid
            $url = "https://messaging.ooredoo.qa/bms/soap/Messenger.asmx/HTTP_SendSms";
            //$mobilenumbers = $customerPhone;
            //$message = urlencode($message);
            $ch = curl_init();
            if (!$ch) {
                die("Couldn't initialize a cURL handle");
            }
            $ret = curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "customerID=4319&userName=mownehqat&userPassword=Mtc@2020&originator=mowneh&smsText=".$message."&recipientPhone=".$phoneNo."&messageType=Latin&defDate=&blink=false&flash=false&Private=false");
            $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            //If you are behind proxy then please uncomment below line and provide your proxy ip with port. //
            //$ret = curl_setopt($ch, CURLOPT_PROXY, "PROXY IP ADDRESS:PORT");
            $curlresponse = curl_exec($ch); // execute
            if (curl_errno($ch))
                echo 'curl error : ' . curl_error($ch);
            if (empty($ret)) {
                // some kind of an error happened
                die(curl_error($ch));
                curl_close($ch); // close cURL handler
            } else {
                $info = curl_getinfo($ch);
                curl_close($ch); // close cURL handler
                //echo $curlresponse; //echo "Message Sent Succesfully" ;
            }
        }
    
    }

}

class Admin_Controller extends MY_Controller {

    protected $language = "";
    protected $paginationConfig = [];
    protected $languageTitle = "";
    protected $allowedImageTypes = "";
    protected $maxImageUploadSize = "";

    function __construct() {
        parent::__construct();
        $this->load->library('UserLogin');
        $this->load->model('GeneralModel', 'gm');
        $adminLang = $this->session->userdata('admin_lang');
        $this->languageTitle = ($adminLang != "") ? $adminLang : 'english';
        $this->language = $this->gm->getFieldValue('languageID', 'ec_language', array('name' => $this->languageTitle));
        $this->data['adminmenus'] = $this->gm->getAdminMenus();
        $this->data['siteLogo'] = base_url('public/assets/img/logo.svg');

        $this->allowedImageTypes = $this->getSettingValue('allowed_image_types');
        $this->maxImageUploadSize = $this->getSettingValue('max_image_upload_size') * 1024;

//        PAGINATION
        $this->paginationConfig['full_tag_open'] = "<ul class='pagination'>";
        $this->paginationConfig['full_tag_close'] = '</ul>';
        $this->paginationConfig['num_tag_open'] = '<li>';
        $this->paginationConfig['num_tag_close'] = '</li>';
        $this->paginationConfig['cur_tag_open'] = '<li class="active"><a href="#">';
        $this->paginationConfig['cur_tag_close'] = '</a></li>';
        $this->paginationConfig['prev_tag_open'] = '<li>';
        $this->paginationConfig['prev_tag_close'] = '</li>';
        $this->paginationConfig['first_tag_open'] = '<li>';
        $this->paginationConfig['first_tag_close'] = '</li>';
        $this->paginationConfig['last_tag_open'] = '<li>';
        $this->paginationConfig['last_tag_close'] = '</li>';
        $this->paginationConfig['next_link'] = 'Next Page';
        $this->paginationConfig['next_tag_open'] = '<li><i class=""></i>';
        $this->paginationConfig['next_tag_close'] = '</li>';
        $this->paginationConfig['prev_link'] = 'Previous Page';
        $this->paginationConfig['prev_tag_open'] = '<li><i class=""></i>';
        $this->paginationConfig['prev_tag_close'] = '</li>';
        //PAGINATION CONFIG END

        foreach ($this->siteLanguages as $lang) {
            if ($lang['languageID'] == $this->language) {
                $this->data['currentLanguage'] = $lang;
            } else {
                $this->data['otherLanguages'][] = $lang;
            }
        }
        if (!$this->userlogin->loggedIn()) {
            redirect('manage/');
        }
        $userImage = $this->gm->getFieldValue('image', 'ec_user', array('userID' => $this->session->userdata('adminID')));
        $this->data['userImage'] = (trim($userImage) != '' && is_file(DIR_IMAGE . 'adminUser/' . $userImage)) ? 'uploads/adminUser/' . $userImage : 'public/manage/img/profile/1.png';
        $permissionValue = $this->userlogin->validatePermission();
        if ($permissionValue >= 0) {
            if ($permissionValue != 7) {
                $function = $this->router->fetch_method();
                if ($function == 'add' || $function == 'edit' || $function == 'changeStatus' || $function == 'index') {
                    if (!$this->checkPermission($function, $permissionValue)) {
                        redirect('manage/AccessDenied');
                    }
                } else {
                    //function which not in add edit change status
                }
            }
        } else {
            redirect('manage/AccessDenied');
        }
    }

    protected function checkPermission($function, $permissionValue) {
        $status = false;
        switch ($function) {
            case 'index':
                $status = ($permissionValue >= 0) ? true : false;
                break;
            case 'add':
                $status = ($permissionValue == 1 || $permissionValue == 3 || $permissionValue == 5) ? true : false;
                break;
            case 'edit':
                $status = ($permissionValue == 2 || $permissionValue == 3 || $permissionValue == 6) ? true : false;
                break;
            case 'changeStatus':
                $status = ($permissionValue == 4 || $permissionValue == 5 || $permissionValue == 6) ? true : false;
                break;
            default:
                $status = true;
                break;
        }
        return $status;
    }

    protected function logUserActivity($title = '') {
        $activity = array(
            'userID' => $this->session->userdata('adminID'),
            'usedClass' => $this->router->fetch_class(),
            'usedMethod' => $this->router->fetch_method(),
            'activityTitle' => $title
        );
        $this->gm->insertValue('ec_user_activity', $activity);
    }

    protected function render($the_view = NULL, $template = 'admin_master') {
        parent::render($the_view, $template);
    }

}

class Auth_Controller extends Site_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'gm');
    }

    protected function render($the_view = NULL, $template = 'site_master') {
        parent::render($the_view, $template);
    }
}

class Site_Controller extends MY_Controller {

    protected $siteLanguage = "";
    protected $siteLanguageID = "";
    protected $paginationConfig = [];

    function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'gm');
        $this->setSystemProperties();
        $this->setSiteFooter();
        $this->setMegaNav();
        //        PAGINATION
        $this->paginationConfig['full_tag_open'] = "<ul class='pagination'>";
        $this->paginationConfig['full_tag_close'] = '</ul>';
        $this->paginationConfig['num_tag_open'] = '<li>';
        $this->paginationConfig['num_tag_close'] = '</li>';
        $this->paginationConfig['cur_tag_open'] = '<li class="active"><a href="#">';
        $this->paginationConfig['cur_tag_close'] = '</a></li>';
        $this->paginationConfig['prev_tag_open'] = '<li>';
        $this->paginationConfig['prev_tag_close'] = '</li>';
        $this->paginationConfig['first_tag_open'] = '<li>';
        $this->paginationConfig['first_tag_close'] = '</li>';
        $this->paginationConfig['last_tag_open'] = '<li>';
        $this->paginationConfig['last_tag_close'] = '</li>';
        $this->paginationConfig['next_link'] = 'Next Page';
        $this->paginationConfig['next_tag_open'] = '<li><i class=""></i>';
        $this->paginationConfig['next_tag_close'] = '</li>';
        $this->paginationConfig['prev_link'] = 'Previous Page';
        $this->paginationConfig['prev_tag_open'] = '<li><i class=""></i>';
        $this->paginationConfig['prev_tag_close'] = '</li>';
        
        $this->data['priceVal'] = array(10,20,30,50,75,100,150,200,250,300,350,400,450,500);
        //PAGINATION CONFIG END
    }

    protected function setSystemProperties() {
        $this->data['title'] = $this->getSettingValue('store_name');
       
       
        list($width, $height) = $this->getImageDimension('site_logo_dimen');
        $logo = $this->getSettingValue('logo');
        
        $this->data['logo'] = $this->setSiteImage('siteInfo/' . $logo, $width, $height);
        $this->data['favicon'] = base_url() . 'uploads/siteInfo/' . $this->getSettingValue('favicon');
        $siteLang = $this->session->userdata('site_lang');
        $this->siteLanguage = ($siteLang != "") ? $siteLang : "english";
        $this->siteLanguageID = $this->gm->getFieldValue('languageID', 'ec_language', array('name' => $this->siteLanguage));
        
        $this->data['metaTitle'] = $this->getSettingValue('meta_title');
        $this->data['metaDescription'] = $this->getSettingValue('meta_description');
        $this->data['metaKeyword'] = $this->getSettingValue('meta_keyword');
        
        $this->data['cartItemCount'] = $this->carts->countProducts();
        $this->data['total'] = $this->currency->format($this->carts->getTotal(), $this->siteCurrency);
        $this->data['cartView'] = $this->cartView();

        $this->data['customerID'] = $this->customer->getID();

        foreach ($this->siteLanguages as $lang) {
            if ($lang['languageID'] == $this->siteLanguageID) {
                $this->data['currentLanguage'] = $lang;
            } else {
                $this->data['otherLanguages'][] = $lang;
            }
        }
        $this->data['cities'] = $this->gm->getCities($this->siteLanguageID);
        $this->lang->load('site', $this->siteLanguage);
        $this->data['informantionPages'] = $this->gm->getInformationPages($this->siteLanguageID);
        //$this->data['categoryTree'] = array_values($this->gm->getMenuTree());
        $this->data['categoryTree'] = $this->gm->getMenuTree($this->siteLanguageID);
        //echo "<pre>";print_r($this->data['categoryTree']);exit;
    }
    protected function setMegaNav() {
        $catParams = $params = array(
            'limit' => 30,
            'start' => 0,
            'languageID' => $this->siteLanguageID);
        $proParams = $params = array(
            'limit' => 4,
            'start' => 0,
            'languageID' => $this->siteLanguageID);
        $this->data['nav_category'] = $this->cm->selectAll($catParams);
        $products = $this->pm->selectAll($proParams);
        foreach($products as $key=>$row){
            $prod               = $this->pm->selectProductDetail($row['productID'],$this->siteLanguageID);
            if(@$prod['image']){
                $prod['image']      = $this->setSiteImage('product/'.$prod['image'], '150', '120');
            }
            
            $prodductDet[]      = $prod;
        }
        $this->data['nav_products'] = $prodductDet;
//        echo "<pre>";print_r($prodductDet);exit;
    }
    protected function setSiteFooter() {
        $this->data['address'] = $this->getSettingValue('address');
        $catParams = $params = array(
            'limit' => 4,
            'start' => 0,
            'languageID' => $this->siteLanguageID);
        $this->data['footer_category'] = $this->cm->selectFooterCategory($catParams);
    }

    protected function cartView() {
        $this->lang->load('site', $this->siteLanguage);
        $products = $this->carts->getProducts($this->siteLanguageID);
        list($width, $height) = $this->getImageDimension('cart_image_dimen');
        foreach ($products as $key => $row) {

            $products[$key]['image'] = $this->setSiteImage('product/' . $row['image'], $width, $height);
            $products[$key]['price'] = $this->currency->format($row['price'], $this->siteCurrency);
        }
        $incart['incart'] = $products;
        $incart['subtotal'] = $this->currency->format($this->carts->getSubTotal(), $this->siteCurrency);
        //$incart['shipping'] = $this->currency->format(0, $this->siteCurrency);
         //$incart['shipping'] = $this->currency->format($this->getSettingValue('shipping_charge',$this->siteLanguageID), $this->siteCurrency);
        $incart['total'] = $this->currency->format($this->carts->getTotal(), $this->siteCurrency);
        return $this->twig->render('frontEnd/site_master_cart', $incart);
    }

    protected function render($the_view = NULL, $template = 'site_master') {
        parent::render($the_view, $template);
    }

    protected function setSiteImage($filename, $width, $height) {
        //echo $filename; echo "</br>";
        //echo DIR_IMAGE;
        if (!is_file(DIR_IMAGE . $filename) ) {
            $noimage = $this->setNoImage($width, $height);
            return $noimage;
        }
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $image_old = $filename;

        list($width_orig, $height_orig, $image_type) = getimagesize(DIR_IMAGE . $image_old);
        if ($width == 0 || $height == 0) {
            $image_new = 'cache/' . $filename;
            $width = $width_orig;
            $height = $height_orig;
        } else {
            $image_new = 'cache/' . substr($filename, 0, strrpos($filename, '.')) . '-' . (int) $width . 'x' . (int) $height . '.' . $extension;
        }
        //echo $image_new;exit;
        if (!is_file(FRONT_IMG_DIR . $image_new) || (filemtime(DIR_IMAGE . $image_old) > filemtime(FRONT_IMG_DIR . $image_new))) {
            if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) {
                return DIR_IMAGE . $image_old;
            }

            $path = '';

            $directories = explode('/', dirname($image_new));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir(FRONT_IMG_DIR . $path)) {
                    @mkdir(FRONT_IMG_DIR . $path, 0777);
                }
            }

            if ($width_orig != $width || $height_orig != $height) {
                $this->load->library('Image');
                $this->image->resize($width, $height, DIR_IMAGE . $image_old, FRONT_IMG_DIR . $image_new);
            } else {
                copy(DIR_IMAGE . $image_old, FRONT_IMG_DIR . $image_new);
            }

            // fix bug when attach image on email (gmail.com). it is automatic changing space " " to +
        }
        $image_new = str_replace(' ', '%20', $image_new);
        return base_url('image/') . $image_new;
    }

    protected function setNoImage($width = 300, $height = 300) {
        $filename = 'noimage.png';
        if (is_file(FRONT_IMG_DIR . $filename)) {
            $image_old = $filename;
            $extension = pathinfo($filename, PATHINFO_EXTENSION);
            $image_new = 'cache/' . substr($filename, 0, strrpos($filename, '.')) . '-' . (int) $width . 'x' . (int) $height . '.' . $extension;
            if (!is_file(FRONT_IMG_DIR . $image_new) || (filemtime(FRONT_IMG_DIR . $image_old) > filemtime(FRONT_IMG_DIR . $image_new))) {
                $path = '';

                $directories = explode('/', dirname($image_new));

                foreach ($directories as $directory) {
                    $path = $path . '/' . $directory;

                    if (!is_dir(FRONT_IMG_DIR . $path)) {
                        @mkdir(FRONT_IMG_DIR . $path, 0777);
                    }
                }
                $this->load->library('Image');
                $this->image->resize($width, $height, FRONT_IMG_DIR . $image_old, FRONT_IMG_DIR . $image_new);
            }
            $image_new = str_replace(' ', '%20', $image_new);
            return base_url('image/') . $image_new;
        } else {
            return;
        }
    }

}

class Account_Controller extends Site_Controller {
    protected $paginationConfig = [];

    function __construct() {
        parent::__construct();
        $this->load->model('general_model', 'gm');
        if (!$this->customer->loggedIn()) {
            redirect('login/');
        }
    }

    protected function render($the_view = NULL, $template = 'site_master') {
        parent::render($the_view, $template);
    }
    
}

class API_Controller extends MY_Controller {
    protected $paginationConfig = [];
   
    function __construct() {
        parent::__construct();
        $this->load->library('CustomerLogin');
        $this->load->library('Apisupport');
        $this->load->model('general_model', 'gm');
        
        if($this->input->get('languageID'))
        {
            $this->defalutLanguage  =   $this->input->get('languageID');
        }elseif($this->input->post('languageID')){
            $this->defalutLanguage  =   $this->input->post('languageID');
        }else{
            $this->defalutLanguage  =   1;
        }
        $languageName = $this->gm->getFieldValue('name', 'ec_language', array('languageID' => $this->defalutLanguage));
        $this->lang->load('app', $languageName);
//        $this->lang->load('site', $languageName);
        
        //echo $languageName; exit;
          
        $headers    =   $this->input->request_headers();
        //echo "<pre>"; print_r($headers); exit;
       
        $userID =   '';
                if(array_key_exists('userID',$headers)){
                    $userID    =    $headers['userID'];
                }else{
                    $userID    =    @$_SERVER['HTTP_USERID'];
                }
                if($userID!=API_USER_ID){
                    $this->userID  =   $userID;
                }else{
                    $this->userID  =   '';
                }
        $class  =   $this->router->fetch_class();
        $method =   $this->router->fetch_method();
        
        if($method=='')
            $method =   'index_post'.strtolower($_SERVER['REQUEST_METHOD']);
        else 
             $method =   $method.'_'.strtolower($_SERVER['REQUEST_METHOD']);
             
      
       if(method_exists($class, $method)){
          
            $authorizedAccess    =    $this->apisupport->authorizedAccess();
            $unAuthorizedAccessPage    =    $this->apisupport->unAuthorizedAccessPage();
//            echo API_USER_ID; exit;
//            echo "<pre>"; print_r($unAuthorizedAccessPage); exit;
            
            if(!in_array(strtolower($class.'/'.$method),array_map('strtolower', $authorizedAccess))){
                $headers    =   $this->getAuthorizationHeader(); 
                $token   =   "";
               if (!empty($headers)) {
                    if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                        $token = $matches[1];
                    }
                }
                
//               $token   =   $this->input->post('token');
             
               $customer          = $this->generalModel->getTableValue('customerID,firstname,lastname,email,status,tokenTime','ec_customer',array('customerID'=> $userID,'token'=> $token));
               if(!empty($customer)){
                   
                    $interval   =   $this->apisupport->intervelCheck($customer['tokenTime']);
         
                    if($interval>20){
                        //$this->apisupport->updateToken($userID);
                    }
                }else if(in_array(strtolower($class.'/'.$method),array_map('strtolower', $unAuthorizedAccessPage))){
                     
                    if($userID!=API_USER_ID || $token!=API_TOKEN){
                            $this->output->set_status_header(401);
                            $result['status']        =   '401';
                            $result['message']       =   'UNAUTHORIZED';
                            $result['data']          =   '';
                            echo json_encode($result); exit;
                    }
                    
                }
                else{
                   
                   $this->output->set_status_header(401);
                   $result['status']        =   '401';
                   $result['message']       =   'UNAUTHORIZED';
                   $result['data']          =   '';
                   echo json_encode($result); exit;
                }
           }  
        }else{
           $this->output->set_status_header(404);
           $result['status']        =   '404';
           $result['message']       =   'Page Not Found';
           $result['data']          =   '';
           echo json_encode($result); exit;
       }
    }
    
    function getAuthorizationHeader() {
        $headers = null;
       
        if (isset($_SERVER['Authorization'])) {
           
            $headers = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (isset($_SERVER['AUTHORIZATION'])) {
             
            $headers = trim($_SERVER["AUTHORIZATION"]);
        }elseif (isset($_SERVER['authorization'])) {
           
            $headers = trim($_SERVER["authorization"]);
        }elseif (function_exists('apache_request_headers')) {
            
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
       
        return $headers;
    }
    
    protected function setSiteImage($filename, $width, $height) {
             //echo is_file(DIR_IMAGE . $filename);

             if (!is_file(DIR_IMAGE . $filename) ) {
                 $noimage = $this->setNoImage($width, $height);
                 return $noimage;
             }
             $extension = pathinfo($filename, PATHINFO_EXTENSION);
             $image_old = $filename;

             list($width_orig, $height_orig, $image_type) = getimagesize(DIR_IMAGE . $image_old);
             if ($width == 0 || $height == 0) {
                 $image_new = 'cache/' . $filename;
                 $width = $width_orig;
                 $height = $height_orig;
             } else {
                 $image_new = 'cache/' . substr($filename, 0, strrpos($filename, '.')) . '-' . (int) $width . 'x' . (int) $height . '.' . $extension;
             }
             //echo $image_new;
             if (!is_file(FRONT_IMG_DIR . $image_new) || (filemtime(DIR_IMAGE . $image_old) > filemtime(FRONT_IMG_DIR . $image_new))) {
                 if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) {
                     return DIR_IMAGE . $image_old;
                 }

                 $path = '';

                 $directories = explode('/', dirname($image_new));

                 foreach ($directories as $directory) {
                     $path = $path . '/' . $directory;

                     if (!is_dir(FRONT_IMG_DIR . $path)) {
                         @mkdir(FRONT_IMG_DIR . $path, 0777);
                     }
                 }

                 if ($width_orig != $width || $height_orig != $height) {
                     $this->load->library('Image');
                     $this->image->resize($width, $height, DIR_IMAGE . $image_old, FRONT_IMG_DIR . $image_new);
                 } else {
                     copy(DIR_IMAGE . $image_old, FRONT_IMG_DIR . $image_new);
                 }

                 // fix bug when attach image on email (gmail.com). it is automatic changing space " " to +
             }
             $image_new = str_replace(' ', '%20', $image_new);
             return base_url('image/') . $image_new;
        }
        
        protected function setNoImage($width = 300, $height = 300) {
        $filename = 'noimage.png';
        if (is_file(FRONT_IMG_DIR . $filename)) {
            $image_old = $filename;
            $extension = pathinfo($filename, PATHINFO_EXTENSION);
            $image_new = 'cache/' . substr($filename, 0, strrpos($filename, '.')) . '-' . (int) $width . 'x' . (int) $height . '.' . $extension;
            if (!is_file(FRONT_IMG_DIR . $image_new) || (filemtime(FRONT_IMG_DIR . $image_old) > filemtime(FRONT_IMG_DIR . $image_new))) {
                $path = '';

                $directories = explode('/', dirname($image_new));

                foreach ($directories as $directory) {
                    $path = $path . '/' . $directory;

                    if (!is_dir(FRONT_IMG_DIR . $path)) {
                        @mkdir(FRONT_IMG_DIR . $path, 0777);
                    }
                }
                $this->load->library('Image');
                $this->image->resize($width, $height, FRONT_IMG_DIR . $image_old, FRONT_IMG_DIR . $image_new);
            }
            $image_new = str_replace(' ', '%20', $image_new);
            return base_url('image/') . $image_new;
        } else {
            return;
        }
    }
        
        function getResposnseResult($status, $message, $data) {
            $result = array();
            $result['status'] = $status;
            /* if ($status == 200) {
              $result['message'] = $message;
              } else {
              $result['error'] = $message;
              } */
            $result['message'] = $message;
            $result['data'] = $data;
            return $result;
        }
        
         function setHeaderToken(){
               $headers    =   $this->getAuthorizationHeader();
               if (!empty($headers)) {
                    if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                        $headerToken = $matches[1];
                    }
                }  
                $token  =   '';
                $userID    =    $this->userID;
                if($headerToken!=API_TOKEN && $userID!=''){
                    $token = $this->apisupport->getToken($userID);
                }
                $this->output->set_header('token:'.$token);
            
        }
       
}

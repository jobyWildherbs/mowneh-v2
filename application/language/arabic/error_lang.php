<?php

// Cart Errors
$lang['quantity_not_available'] = "Product quantity which you look not available in store";
$lang['product_add_error'] = "Error occured while add to cart, Product may be disabled. ";
$lang['product_remove_error'] = "Error occured while remove from cart ";
$lang['product_status_error'] = "Product is disabled now, So we can't update it to cart. ";
$lang['product_date_available_error'] = "Product is only available from %s ";
$lang['product_city_select_error'] = "Please choose a location before check delivery. ";
$lang['product_city_delivery_error'] = "Delivery is not currently available in your location. ";
$lang['404_title'] = "Something's missing.";
$lang['404_msg'] = "You are looking for a page that does not exist, have been removed, name changed or is temporarily unavailable. Don't worry, we'll fix it soon.";



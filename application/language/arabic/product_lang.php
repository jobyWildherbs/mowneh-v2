<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['home']                   = "الرئيسية ";
$lang['productDetails']         = "وصف المنتج";
$lang['share']                  = "Share";
$lang['Description']            = "وصف";
$lang['Information']            = "المعلومات";
$lang['RelatedProduct']         = "المنتجات ذات الصلة";
$lang['AddToCart']              = "أضف إلى العربة";
$lang['InStock']                = "موجود بالمخزن";
$lang['OutOfStock']             = "غير متوفّر"; 
$lang['New']                    = "جديد";
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$lang['All_Categories'] = "جميع برامج الترميز";
$lang['Cash_On_Delivery'] = 'الدفع عند الاستلام';;
$lang['Online_Payment'] = 'الدفع عبر الإنترنت';
$lang['Free_Checkout'] = 'الخروج مجانا';
$lang['Success'] = 'نجاح';
$lang['Sorry_No_Data_Found'] = 'Sorry No Data Found';
$lang['Bad_request'] = 'Bad request';
$lang['Removed_From_Your_Wishlist'] = 'تمّ الإزالة من قائمة الرغبات الخاصة بك'; 
$lang['Added_To_Your_Wishlist'] = 'أُضيفت إلى قائمة الرغبات الخاصة بك';
$lang['No_Notification_Found'] = 'No Notification Found';
$lang['Failed'] = 'فشل';
$lang['cart_added_api'] = "تمت إضافة المنتج إلى العربة بنجاح.";
$lang['Coupon_Applied'] = 'تطبيق القسيمة';
$lang['Invalid_Coupon'] = 'قسيمة غير صالحة';
$lang['no_order_yet'] = 'لا يوجد طلب حتى الآن!';
$lang['Address_Added_Successfully'] = 'Address Added Successfully';
$lang['Something_Went_Wrong'] = 'Something Went Wrong';
$lang['Address_Updated_Successfully'] = 'Address Updated Successfully'; 
$lang['Password_changed_Successfully'] = 'تم تغيير كلمة المرور بتجاح';
$lang['Default_Address_Set_Successfully'] = 'Default Address Set Successfully';
$lang['Address_Deleted_Successfully'] = 'Address Deleted Successfully';
$lang['empty_search_result_msg_api'] = 'لا توجد نتائج حسب بحثك';
$lang['Sorry_No_Product_Found'] = 'Sorry No Product Found';
$lang['Registration_completed_successfully'] = 'Registration completed successfully.Please check your mail to activate your account..'; 
$lang['OTP_already_used_api'] = 'كلمة المرور لمرة واحدة مستخدمة بالفعل';
$lang['OTP_expired_api'] = 'كلمة المرور مرة واحدة انتهت صلاحيتها';
$lang['An_OTP_has_been_re_sent_to_your_registered_phone_number'] = 'An OTP (one-time-password) has been re-sent to your registered phone number.'; 
$lang['Account_Activited'] = 'Account Activited';
$lang['OTP_Invalid_api'] = 'كلمة المرور مرة واحدة غير صالح';
$lang['OTP_Send_Successfully'] = 'OTP Send Successfully';
$lang['Invalid_User_api'] = 'مستخدم غير صالح'; 
$lang['sub_total'] = 'المجموع الفرعي';

$lang['shipping_charge'] = 'رسوم الشحن';

$lang['Expected_delivery_on'] = 'التسليم المتوقع في';
$lang['contactSuccess'] = 'تم تقديم طلبك بنجاح';
$lang['something_went_wrong_Please_try_again'] = 'حدث خطأ ما. أعد المحاولة من فضلك';
$lang['This_mobile_number_is_not_registered_with_mowneh'] = 'رقم الهاتف المحمول هذا غير مسجل لدى مونة';
$lang['Your_account_is_inactive_Please_contact_mowneh'] = 'حسابك غير نشط. الرجاء الاتصال mowneh';

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$lang['All_Categories'] = "All Categories";
$lang['Cash_On_Delivery'] = 'Cash On Delivery';
$lang['Online_Payment'] = 'Online Payment';
$lang['Free_Checkout'] = 'Free Checkout';
$lang['Success'] = 'Succss';
$lang['Sorry_No_Data_Found'] = 'Sorry No Data Found';
$lang['Bad_request'] = 'Bad request';
$lang['Removed_From_Your_Wishlist'] = 'Removed From Your Wishlist'; 
$lang['Added_To_Your_Wishlist'] = 'Added To Your Wishlist';
$lang['No_Notification_Found'] = 'No Notification Found';
$lang['Failed'] = 'Failed';
$lang['cart_added_api'] = "Product added to cart successfully.";
$lang['Coupon_Applied'] = 'Coupon Applied'; 
$lang['Invalid_Coupon'] = 'Invalid Coupon';
$lang['no_order_yet'] = 'No Order Yet !';
$lang['Address_Added_Successfully'] = 'Address Added Successfully';
$lang['Something_Went_Wrong'] = 'Something Went Wrong'; 
$lang['Address_Updated_Successfully'] = 'Address Updated Successfully'; 
$lang['Password_changed_Successfully'] = 'Password Changed Successfully.';  
$lang['Default_Address_Set_Successfully'] = 'Default Address Set Successfully'; 
$lang['Address_Deleted_Successfully'] = 'Address Deleted Successfully';
$lang['empty_search_result_msg_api'] = 'No result found as per your search';
$lang['Sorry_No_Product_Found'] = 'Sorry No Product Found';
$lang['Registration_completed_successfully'] = 'Registration completed successfully.Please check your mail to activate your account..'; 
$lang['OTP_already_used_api'] = 'OTP already used';
$lang['OTP_expired_api'] = 'OTP expired'; 
$lang['An_OTP_has_been_re_sent_to_your_registered_phone_number'] = 'An OTP (one-time-password) has been re-sent to your registered phone number.'; 
$lang['Account_Activited'] = 'Account Activited';
$lang['OTP_Invalid_api'] = 'OTP Invalid'; 
$lang['OTP_Send_Successfully'] = 'OTP Send Successfully';
$lang['Invalid_User_api'] = 'Invalid User'; 
$lang['sub_total'] = 'Sub Total';
$lang['shipping_charge'] = 'Shipping Charge'; 
$lang['Expected_delivery_on'] = 'Expected delivery  on';
$lang['contactSuccess'] = 'Your request has been submitted successfully';
$lang['something_went_wrong_Please_try_again'] = 'something went wrong Please try again';
$lang['This_mobile_number_is_not_registered_with_mowneh'] = 'This mobile number is not registered with mowneh';
$lang['Your_account_is_inactive_Please_contact_mowneh'] = 'Your account is inactive. Please contact mowneh';
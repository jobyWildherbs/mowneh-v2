<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$lang['home']                   = "Home";
$lang['productDetails']         = "Product details";
$lang['share']                  = "Share";
$lang['Description']            = "Description";
$lang['Information']            = "Information";
$lang['RelatedProduct']         = "Related Product";
$lang['AddToCart']              = "Add to cart";
$lang['InStock']                = "In stock";
$lang['OutOfStock']             = "Out of stock"; 
$lang['New']                    = "New";
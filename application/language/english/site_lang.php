<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$lang['home'] = "Home";
$lang['login'] = "login";
$lang['register'] = "Register";
$lang['username'] = "Username or email";
$lang['passwords'] = "Passwords";
$lang['remember'] = "Remember Me";
//Home Page
$lang['login_button'] = "Login";
$lang['already_customer'] = "Already a customer?";
$lang['no_account'] = "Donâ€™t have an account?";
$lang['register_here'] = "Register here";
$lang['or_register_here'] = "Or Register Here";
$lang['new_customer'] = "New customer?";
$lang['city'] = "City";
$lang['select_city'] = "Select City";
$lang['check_avail_button'] = "CHECK AVAILABILITY";
$lang['about_us'] = "About us";
$lang['about_us_button'] = "ABOUT US";
$lang['featured_product'] = "Featured Product";
$lang['get_more_mooneh'] = "Get More From Mooneh";
//Footer Section
$lang['newsletter_now_title'] = "Join Our Newsletter Now";
$lang['Stay_Informed'] = "Stay Informed";
$lang['newsletter_now_description'] = "Submit your email address to stay informed about our offers and latest news.";
$lang['email_address_placeholder'] = "Your Email Address";
$lang['get_in_touch'] = "Get in Touch:";
$lang['copyright'] = "Copyright";
$lang['all_right_reserved'] = "All Rights Reserved";
$lang['subscribe'] = "SUBSCRIBE";
// Cart Section
$lang['cart_added'] = "Product added to cart successfully.";
$lang['cart_removed'] = "Product removed from cart successfully.";
$lang['cart_updated'] = "cart updated  successfully.";
$lang['cart'] = "cart";
$lang['Product'] = "Product"; 
$lang['Price'] = "Price";
$lang['Quantity'] = "Quantity";
$lang['Total'] = "Total";
$lang['Areyouremove'] = 'Are you sure to remove "Vestibulum suscipit"?'; 
$lang['ContinueShopping'] = 'Continue Shopping';
$lang['Cartempty'] = 'Cart is empty';
$lang['Coupon'] = 'Coupon';
$lang['Entercouponcode'] = 'Enter your coupon code if you have one.';
$lang['CartTotals'] = 'Cart Totals';
$lang['Subtotal'] = 'Sub Total';
$lang['Total'] = 'Grand Total';
$lang['ProceedCheckout'] = 'Proceed to Checkout';
$lang['Checkout'] = 'Checkout';
$lang['AddToCart'] = 'Add To Cart';
$lang['ShippingAddress'] = 'Shipping Address';
$lang['Payment'] = 'Payment';
$lang['discounts'] = 'Discounts';
$lang['check_delivery'] = 'Check delivery';
$lang['select_location'] = 'Select Location';
$lang['description'] = 'Description';
$lang['attribute'] = 'Information';
$lang['item-code'] = 'Item Code / Model';
$lang['instock'] = 'In stock';
$lang['outstock'] = 'Out of stock';
$lang['backtohome'] = 'Back to home page';

//Register And Login
$lang['REGISTER'] = 'REGISTER';
$lang['FirstName'] = 'First Name'; 
$lang['LastName'] = 'Last Name'; 
$lang['EmailAddress'] = 'Email Address'; 
$lang['Phone'] = 'Phone';
$lang['Accountpassword'] = 'Account password';
$lang['Confirmpassword'] = 'Confirm password';
$lang['Create_Password'] = 'Create Password';
$lang['Existing_user_login'] = 'Existing user? Login here';
$lang['termasAgree'] = 'I agree to the terms & conditions';
$lang['Login'] = 'Login';
$lang['PLEASELOGIN'] = 'Please Login'; 
$lang['Usernameoremail'] = 'Username Or Email'; 
$lang['Password'] = 'Password'; 
$lang['Register'] = 'Register'; 
$lang['informations'] = 'Informations';
$lang['contact'] = 'Contact us';
$lang['my_account'] = 'My Account';
$lang['wishlist'] = 'Wishlist';
$lang['change_Password'] = 'Change Password';
$lang['signout'] = 'Sign Out';
$lang['product_city_delivery_success'] = 'Delivery available';
$lang['new'] = 'New';

//My Account 
$lang['Newpassword'] = 'New Password';
$lang['Account_Det'] = 'Account Details';
$lang['orders'] = 'Orders';
$lang['wishlist'] = 'Wishlist';
$lang['manage_addr'] = 'Manage Address';
$lang['manage_addres'] = 'Manage Addresses';
$lang['change_password'] = 'Change Password';
$lang['logout'] = 'Logout';
$lang['ord_details'] = 'Order Details';
$lang['order_id'] = 'Order id';
$lang['invoice_no'] = 'Invoice No';
$lang['view_details'] = 'View Details';

$lang['ordered_on'] = 'Ordered on';
$lang['quantity'] = 'Qty ';
$lang['Model'] = 'Model';
$lang['bill_addr'] = 'Billing Address';
$lang['ship_addr'] = 'Shipping Address';
$lang['price_det'] = 'Price Details';
$lang['sub_total'] = 'Sub Total';
$lang['coupon'] = 'Coupon';
$lang['offers'] = 'Offers';
$lang['shipping'] = 'Shipping';
$lang['tax'] = 'Tax';
$lang['total_amt'] = 'Total Amount';

$lang['no_order_yet'] = 'No Order Yet !';
$lang['no_wishlist_item'] = 'No products in the wishlist. Start adding your favorite products to the wishlist now.';
$lang['click_here'] = 'Click Here';
$lang['add_to_cart'] = 'Add to cart';
$lang['remove'] = 'Remove';
$lang['Address_line_1'] = 'Address Line 1';
$lang['Address_line_2'] = 'Address Line 2';
$lang['city'] = 'City';
$lang['phone'] = 'Phone';
$lang['po_box'] = 'Post Box';
$lang['po_bx'] = 'PO Box';
$lang['country'] = 'Country';

$lang['edit_addr'] = 'Edit';
$lang['delete_addr'] = 'Delete';
$lang['set_addr'] = 'Set as default';
$lang['add_new_addr'] = 'Add New Address';
$lang['del_msg_addr'] = 'Deleting Address!';
$lang['confirm_addr'] = 'Your address will be deleted. To continue, click confirm button';
$lang['del_msg_wish'] = 'Remove Wishlist!';
$lang['confirm_wish'] = 'Are your sure to remove this from wishlist?';
$lang['cancel'] = 'cancel';
$lang['confirm'] = 'confirm';
$lang['mowneh'] = 'Mowneh'; 
$lang['customers'] = 'Customers';
$lang['Old_Password'] = 'Old password'; 
$lang['Incorrect_Old_Password'] = 'Incorrect Old Password';  
$lang['Password_changed_Successfully'] = 'Password Changed Successfully.';   
$lang['Failed_To_Update'] = 'Failed To Update';
$lang['Products'] = 'Products';
$lang['There_Are'] = 'There Are';
$lang['Name_A_Z'] = 'Name, A To Z';
$lang['Name_Z_A'] = 'Name, Z to A'; 
$lang['Price_Low_High'] = 'Price, Low To High';  
$lang['Price_High_Low'] = 'Price, High To Low';
$lang['Filter'] = 'Filter'; 
$lang['There_Are_No_Product'] = 'There Are No Product';  
$lang['Showing'] = 'Showing';   
$lang['Added_To_Your_Wishlist'] = 'Added To Your Wishlist';
$lang['Wishlist_Added'] = 'Wishlist Added';
$lang['Removed_From_Your_Wishlist'] = 'Removed From Your Wishlist'; 
$lang['Wishlist_Removed'] = 'Wishlist Removed';
$lang['Reviews'] = 'Reviews';
$lang['Save_Change'] = 'Save Change';
$lang['Related_Product'] = 'Related Product';
$lang['New'] = 'New'; 
$lang['Price'] = 'Price'; 
$lang['Brands'] = 'Brands';
$lang['Sort_By'] = 'Sort By'; 
$lang['Filter_Your_Items'] = 'Filter your items';
$lang['Building_No'] = 'Building No';
$lang['Building_Name'] = 'Building Name';
$lang['Street_No'] = 'Street No';
$lang['Street_Name'] = 'Street Name';
$lang['Zone_No'] = 'Zone No';

// NIKHIL
$lang['your_cart_empty'] = 'Your cart is empty!';
$lang['help_center'] = 'Help center';
$lang['address'] = 'Address';
$lang['quick_view'] = 'Quick View';
$lang['your_account'] = 'Your account';
$lang['site_name'] = 'Mowneh';
$lang['privacy_policy'] = 'Privacy Policy';
$lang['categories'] = 'Categories';
$lang['remove_item'] = 'Remove item';
$lang['continue'] = 'Continue';
$lang['delivery_address'] = 'Delivery Address';
$lang['p_o_box'] = 'P.O.Box';
$lang['address_1'] = 'Address 1';
$lang['address_2'] = 'Address 2';
$lang['select'] = 'Select';
$lang['failed'] = 'Failed';
$lang['go_back'] = 'Go back to cart';
$lang['order_success_msg_head'] = 'Your order placed successfully.';
$lang['order_success_msg_body'] = 'Thank you for your order. We\'ll get back to you soon.';
$lang['location_details'] = 'Location & Details';
$lang['email'] = 'Email';
$lang['promotions'] = 'Promotions';
$lang['your_otp'] = 'Your OTP';
$lang['filter_by_price'] = 'Filter By Price';
$lang['options'] = 'Options';
$lang['favourites'] = 'Favourites';
$lang['options'] = 'Options';
$lang['options'] = 'Options';
$lang['search_result'] = 'Search Result';
$lang['choose_category'] = 'Choose Category';
$lang['search'] = 'Search';
$lang['empty_search_result_msg'] = 'No result found as per your search';
$lang['search_our_catalog'] = 'Search out catalog';
$lang['Coupon_code'] = 'Coupon code';
$lang['Apply_Coupon'] = 'Apply Coupon';
$lang['Invalid_Coupon'] = 'Invalid Coupon';
$lang['Coupon_Applied'] = 'Coupon Applied'; 
$lang['Your_account_is_inactive_now'] = 'Your account is inactive now';
$lang['Login_Sucussfully'] = 'Login Sucussfully'; 
$lang['Login_Attempt_Over'] = 'Login Attempt Over';  
$lang['Invalid_User_Name_Password'] = 'Invalid User Name & Password'; 
$lang['Invalid_User'] = 'Invalid User'; 
$lang['Invalid_Values'] = 'Invalid Values';
$lang['Account_already_exists'] = 'Account already exists with this email. Please login here'; 
$lang['Please_provide_valid_email_address'] = 'Please provide a valid email address'; 
$lang['This_number_already_associated_with_another_account'] = 'This number is already associated with another account'; 
$lang['Please_provide_valid_phone_number'] = 'Please provide a valid phone number'; 
$lang['Registration_completed_successfully'] = 'Registration completed successfully.Please check your mail to activate your account..'; 
$lang['Product_Tags'] = 'Product Tags'; 
$lang['Forgot_Password'] = 'Forgot Password ?'; 
$lang['Submit'] = 'Submit'; 
$lang['Registered_Email_Address'] = 'Registered Email Address';
$lang['Account_Not_Verified'] = 'Account Is Not Verified';
$lang['User_Not_Found'] = 'User Not Found';
$lang['ForgotPasswordSuccess'] = 'Your password reset link has been sent to your email';
$lang['Reset_Password'] = 'Reset Password';
$lang['Something_went_wrong_please_try_again'] = 'Something went wrong please try again';
$lang['shipping_charge'] = 'Shipping Charge'; 
$lang['Login_Here'] = 'Login Here';
$lang['Please_Provide_Address'] = 'Please Provide Address';
$lang['Blog_list'] = 'Blog List';
$lang['Cash_On_Delivery'] = 'Cash On Delivery';
$lang['Online_Payment'] = 'Online Payment';
$lang['Free_Checkout'] = 'Free Checkout';
$lang['Products_Available'] = 'Products Available';
$lang['Your_account_is_not_activated'] = 'Your account is not activated';
$lang['Cod_confirm'] = 'You have selected Cash On Delivery. Do you want to proceed and place your order?';
$lang['Cod_Confirm_title'] = 'Confirm';
$lang['delivery_details'] = 'Delivery Details';
$lang['express_delivery'] = 'Express Delivery';
$lang['normal_delivery'] = 'Normal Delivery';
$lang['Expected_Delivery_Date'] = 'Expected Delivery Date';
$lang['Time_Slote'] = 'Time Slot';
$lang['Time_Slot'] = 'Time Slot';
$lang['Delivery_Type'] = 'Delivery Type';


$lang['OTP_has_been_sent_to_your_registered_phone_number'] = 'A One-Time-Password(OTP) has been sent to your registered phone number.';
$lang['OTP_Invalid'] = 'OTP Invalid';
$lang['OTP_expired'] = 'OTP expired';
$lang['OTP_already_used'] = 'OTP already used';

$lang['Download_the_official_mowneh_app_now'] = 'Download the official mowneh app now!'; 
$lang['Additional_Direction'] = 'Additional Direction';

$lang['total'] = 'Grand Total111'; 
$lang['coupon_discount'] = 'Coupon Applied';
$lang['Connect_with_us'] = 'Connect with us';
$lang['Your_email-id'] = 'Your email-id';




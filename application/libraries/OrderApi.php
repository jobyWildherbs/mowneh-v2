<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class OrderApi {

    protected $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('GeneralModel', 'gm');
        $this->CI->load->model('OrderModel', 'om');
        $this->CI->load->library('Customer');
        $this->CI->load->library('CartsApi');
        $this->CI->load->library('Currency');
        $this->CI->load->model('OptionModel', 'op');
    }

    public function setOrderData($languageID = 1, $currency = 'QAR',$paymentMethod = 1,$customerID=0,$moonehDeliveryAddressID='',$couponData=array(),$deleveryType='normal') {
        
        $orderData = array();
        $orderData['languageID'] = $languageID;
        $orderData['invoicePrefix'] = $this->CI->gm->getSettingValue('invoice_prefix', $languageID);
        $orderData['storeName'] = $this->CI->gm->getSettingValue('store_name', $languageID);
        $orderData['storeID'] = 1;
        $orderData['storeUrl'] = base_url();
        $orderData['customerID'] = $customerID;
        $orderData['paymentMethod'] = $paymentMethod;
        $orderData['paymentCode'] = '';
        $orderData['shippingMethod'] = '';
        $orderData['shippingCode'] = '';
        $orderData['total'] = $this->CI->cartsapi->getTotal($languageID,$customerID,$couponData); 
        $orderData['orderStatusID'] = 1;
        $orderData['affiliateID'] = 0;
        $orderData['commission'] = 0;
        $orderData['marketingID'] = 0;
        $orderData['tracking'] = '';
        $orderData['currencyID'] = $this->CI->currency->getID($currency);  
        $orderData['currencyCode'] = $currency;
        $orderData['currencyValue'] = $this->CI->currency->getValue($currency);
//        $orderData['ip'] = $this->CI->input->ip_address();
        $orderData['ip'] = '';
        $orderData['userAgent'] = $this->CI->input->user_agent(); 

        if (!empty($this->CI->input->server['HTTP_X_FORWARDED_FOR'])) {
            $orderData['forwardedIp'] = $this->CI->input->server['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($this->CI->input->server['HTTP_CLIENT_IP'])) {
            $orderData['forwardedIp'] = $this->CI->input->server['HTTP_CLIENT_IP'];
        } else {
            $orderData['forwardedIp'] = '';
        }

        if (isset($this->CI->input->server['HTTP_ACCEPT_LANGUAGE'])) {
            $orderData['acceptLanguage'] = $this->CI->input->server['HTTP_ACCEPT_LANGUAGE'];
        } else {
            $orderData['acceptLanguage'] = '';
        }
        $orderData['dateAdded'] = date('Y-m-d H:i:s');
        $orderData['dateModified'] = date('Y-m-d H:i:s');

        if ($orderData['customerID'] > 0) {
            $orderData['customerGroupID'] = $this->CI->gm->getFieldValue('customerGroupID','ec_customer','customerID='.$customerID);
            $orderData['firstname'] = $this->CI->gm->getFieldValue('firstname','ec_customer','customerID='.$customerID);
            $orderData['lastname'] = $this->CI->gm->getFieldValue('lastname','ec_customer','customerID='.$customerID);
            $orderData['email'] = $this->CI->gm->getFieldValue('email','ec_customer','customerID='.$customerID);
            $orderData['telephone'] = $this->CI->gm->getFieldValue('telephone','ec_customer','customerID='.$customerID);
            $orderData['fax'] = '';
            $orderData['customField'] = '';
        } else {
            return FALSE;
        }
         
        if ($moonehDeliveryAddressID) {
           
            $userAddress = $this->CI->gm->getTableValue('*', 'ec_address', array('addressID' => $moonehDeliveryAddressID, 'customerID' => $orderData['customerID']));
            
            if(empty($userAddress))
            {
                 return FALSE;
            }
          
            $countryName = $this->CI->gm->getFieldValue('name', 'ec_country', array('countryID' => $userAddress['countryID'], 'status' => 'Active'));
            if ($userAddress['zone_id'] != '') {
                $zoneName = $this->CI->gm->getFieldValue('name', 'ec_state', array('stateID' => $userAddress['zone_id'], 'countryID' => $userAddress['countryID']));
            } else {
                $zoneName = '';
            }
            $orderData['paymentFirstname'] = $userAddress['firstname'];
            $orderData['paymentLastname'] = $userAddress['lastname'];
            $orderData['paymentCompany'] = $userAddress['company'];
            $orderData['paymentAddress1'] = $userAddress['address1'];
            $orderData['paymentAddress2'] = $userAddress['address2'];
            $orderData['paymentCity'] = $userAddress['city'];
            $orderData['paymentPostcode'] = $userAddress['postcode'];
            $orderData['paymentCountryID'] = $userAddress['countryID'];
            $orderData['paymentZoneID'] = $userAddress['zone_id'];
            $orderData['paymentCustomField'] = $userAddress['custom_field'];
            $orderData['paymentCountry'] = $countryName;
            $orderData['paymentZone'] = $userAddress['zone'];
            $orderData['paymentAddressFormat'] = '';
            $orderData['paymentZone_additional_direction'] = $userAddress['additional_direction'];

            $orderData['shippingFirstname'] = $userAddress['firstname'];
            $orderData['shippingLastname'] = $userAddress['lastname'];
            $orderData['shippingCompany'] = $userAddress['company'];
            $orderData['shippingAddress1'] = $userAddress['address1'];
            $orderData['shippingAddress2'] = $userAddress['address2'];
            $orderData['shippingCity'] = $userAddress['city'];
            $orderData['shippingPostcode'] = $userAddress['postcode'];
            $orderData['shippingCountryID'] = $userAddress['countryID'];
            $orderData['shippingZoneID'] = $userAddress['zone_id'];
            $orderData['shippingCustomField'] = $userAddress['custom_field'];
            $orderData['shippingCountry'] = $countryName;
            $orderData['shippingZone'] = $userAddress['zone'];
            $orderData['shippingAddressFormat'] = '';
            $orderData['shipping_additional_direction'] = $userAddress['additional_direction'];
            
            
        } else {
            
            return FALSE;
        }
       
        $invoice = $this->CI->gm->getTableValue('MAX(invoiceNo) as invoice', 'ec_order');
        if ($invoice['invoice'] != '') {
            $invoiceNumber = $invoice['invoice'] + 1;
        } else {
            $invoiceNumber = $this->CI->gm->getSettingValue('invoice_start_from', $languageID);
        }
        
        $orderData['invoiceNo'] = $invoiceNumber;
        $cartProduct    =   $this->CI->cartsapi->getProducts($languageID,$customerID);
        $orderID    =   '';
        if($cartProduct){
            $orderID = $this->CI->gm->insertValue('ec_order', $orderData);
            if ($orderID) {

                $orderProductData = array();
                foreach ($this->CI->cartsapi->getProducts($languageID,$customerID) as $product) {
                    
                    $orderProductData['orderID'] = $orderID;
                    $orderProductData['productID'] = $product['productID'];
                    $orderProductData['name'] = $product['name'];
                    $orderProductData['model'] = $product['model'];
                    $orderProductData['quantity'] = $product['quantity'];
                    $orderProductData['price'] = $product['price'];
                    $orderProductData['total'] = $product['total'];
                    $orderProductData['tax'] = 0;
                    $orderProductData['reward'] = 0;
                    $orderProductID =$this->CI->gm->insertValue('ec_order_product', $orderProductData);
                    $opt = array();
                    $optionArray = array();
                    $optionDataVals =   array();
                    foreach($product['option'] as $productOptionKey=>$option){
                        $optionvals = explode(':', $option);
                        foreach ($optionvals as $value) {
                            $vals = explode('-', $value);
                            $optionArray[$vals[0]]  =  $vals[1]; 
                        }
                        //echo "<pre>"; print_r($optionArray); echo "</pre>";
                          if($optionArray){
                            $option = $this->CI->op->selectOptionDetailByProductOptionVal($optionArray['productOptionID'],$languageID,$optionArray['optionValueId']);
//                           $optionDataVals[$productOptionKey]   =   $option;
                        }
                        if($option){
                            $orderOptionData['orderID'] = $orderID;
                            $orderOptionData['productOrderID'] = $orderProductID;
                            $orderOptionData['orderProductID'] = $product['productID'];
                            $orderOptionData['productOptionID'] = $option[0]['productOptionID'];
                            $orderOptionData['productOptionValueID'] = $option[0]['ID']; 
                            $orderOptionData['name'] = $option[0]['oName'];
                            $orderOptionData['value'] = $option[0]['name'];
                            $orderOptionData['type'] = $option[0]['type'];     
                            $this->CI->gm->insertValue('ec_order_option', $orderOptionData);
                        }
                        
                    }
                     
                  
                    
                } 

                $totals = $this->CI->cartsapi->getTotalItemsList($languageID,$customerID,$couponData,$userAddress['cityID'],$deleveryType);
                foreach ($totals as $total){
                    $orderTotals['orderID'] = $orderID;
                    $orderTotals['code'] = $total['code'];
                    $orderTotals['title'] = $total['title'];
                    $orderTotals['value'] = $total['value'];
                    $orderTotals['sortOrder'] = $total['sort_order'];
                    $orderTotals['languageKey'] = $total['languageKey'];
                    $this->CI->gm->insertValue('ec_order_total', $orderTotals);
                }
            }
        }
        
        if ($orderID) {
            return $orderID;
        } else {
            return false;
        }
    }

}

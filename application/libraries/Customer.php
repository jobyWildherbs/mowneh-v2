<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer {

    protected $CI;
    private $customerID = 0;
    private $firstname = '';
    private $lastname = '';
    private $customerGroupID = 0;
    private $email = '';
    private $telephone = '';
    private $newsletter;
    private $addressID = 0;

    public function __construct() {
        $this->CI = & get_instance();

        $this->CI->load->model('GeneralModel', 'gm');
        //print_r($this->CI->session->userdata); 
        if ($this->CI->session->userdata('moonehcustomerID') && $this->CI->session->userdata('moonehcustomerID') != '') {

            $customer = $this->CI->gm->getTableValue('*', 'ec_customer', array('customerID' => $this->CI->session->userdata('moonehcustomerID'), 'status' => 'Active'));
            if ($customer) {
                $this->customerID = $customer['customerID'];
                $this->firstname = $customer['firstname'];
                $this->lastname = $customer['lastname'];
                $this->email = $customer['email'];
                $this->telephone = $customer['telephone'];
                $this->addressID = $customer['addressID'];
                $this->newsletter = $customer['newsletter'];
                $this->customerGroupID = $customer['customerGroupID'];
            }else{
                $this->logout();
            }
        } else {
            $this->logout();
        }
    }

    public function loggedIn() {
        if ($this->CI->session->userdata('moonehLoggedIn') == TRUE) {
            if ($this->CI->session->userdata('moonehcustomerID') != '' || $this->CI->session->userdata('moonehFirstname') != '' || $this->CI->session->userdata('moonehEmail') != '') {
                
                return true;
            } else {
                $this->logout();
                
                return false;
            }
        } else {
            
            return false;
        }
    }

    public function logout() {
        $newdata = array(
            'moonehcustomerID' => '',
            'moonehFirstname' => '',
            'moonehEmail' => '',
            'moonehStatus' => '',
            'moonehLastname' => '',
            'moonehLoggedIn' => FALSE
        );
        $this->CI->session->set_userdata($newdata);
    }

    public function isLogged() {
        return $this->customerID;
    }

    public function getId() {
        return $this->customerID;
    }

    public function getFirstName() {
        return $this->firstname;
    }

    public function getLastName() {
        return $this->lastname;
    }

    public function getGroupId() {
        return $this->customerGroupID;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getTelephone() {
        return $this->telephone;
    }

    public function getNewsletter() {
        return $this->newsletter;
    }

    public function getAddressId() {
        return $this->addressID;
    }

}

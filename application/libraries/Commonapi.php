<?php
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
defined('BASEPATH') OR exit('No direct script access allowed');
class Commonapi extends REST_Controller{
    public function __construct()
    {
        parent::__construct();  
         
    }

    
    public function  notFound(){
        
        $result['status']	= parent::HTTP_NOT_FOUND;
        $result['message']      = 'Not found';
        $result['data']		= '';
        $this->response($result, REST_Controller::HTTP_OK);
    }
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserLogin {

    protected $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('manage/Login_model', 'loginModel');
        $this->CI->load->model('GeneralModel', 'generalModel');
        $this->CI->load->helper('common_helper');
    }

    public function createCredentials($email, $password) {
        // new password & salt create
        if ($email != '' && $password != '') {
            if ($this->CI->loginModel->checkUserEmail($email)) {
                $random_string = getRandomString(15);
                $salt = hash('sha256', sha1($random_string . SITE_SALT));
                $hashed_password = hash('sha256', sha1($salt . SITE_SALT . $password));
                $result = array("salt" => $salt, "password" => $hashed_password);
                return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getCredentials($email, $password) {
        // new password & salt create
        if ($email != '' && $password != '') {

            if (!$this->CI->loginModel->checkUserEmail($email)) {

                $random_string = getRandomString(15);
                $salt = hash('sha256', sha1($random_string . SITE_SALT));
                $hashed_password = hash('sha256', sha1($salt . SITE_SALT . $password));
                $result = array("salt" => $salt, "password" => $hashed_password);
                return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function checkCredentials($userName, $password) {
        $flag = 1;
        $message = '';
        if ($userName != '' && $password != '') {
            // user name is existing or not
            if (!$this->CI->loginModel->checkUserEmail($userName)) {

                $salt = $this->CI->generalModel->getTableValue('*', 'ec_user', array('userName' => $userName));

                $hashed_password = hash('sha256', sha1($salt['salt'] . SITE_SALT . $password));
                // login attempt more than 5 time check
                $loginAttempt = $this->CI->generalModel->getTableValue('count(attemptID) as attempt', 'ec_login_attempts', array('login' => $userName));
                if ($salt['status'] == 'Inactive') {
                    $flag = 0;
                    $message = "Your account is inactive now";
                } else if ($loginAttempt['attempt'] == 5) {
                    $flag = 0;
                    $message = "Login Attempt Over";
                } else if($salt['status'] == 'Deleted'){
                    $flag = 0;
                    $message = "Your account is removed, Please contact administrator";
                } else {
                    // password matcing
                    if ($hashed_password == $salt['password']) {
                        $flag = 1;
                        $message = "Login Sucussfully";
                    } else {
                        $flag = 0;
                        $message = "Invalid User Name  & Password";
                        $ip_address = $this->CI->input->ip_address();
                        $loginAttempts = array(
                            'ipAddress' => $ip_address,
                            'login' => $userName,
                            'dateAdded' => date('Y-m-d h:i:s')
                        );
                        $this->CI->generalModel->insertValue('ec_login_attempts', $loginAttempts);
                        // login attemot table insert
                    }
                }
            } else {
                $flag = 0;
                $message = "Invalid User";
            }
        } else {
            $flag = 0;
            $message = "Invalid Values";
        }
        $result = array('status' => $flag, 'message' => $message);
        return $result;
    }

    public function loggedIn() {
        if ($this->CI->session->userdata('adminLoggedIn') == TRUE) {
            if ($this->CI->session->userdata('adminID') != '' || $this->CI->session->userdata('adminUserName') != '' || $this->CI->session->userdata('adminEmail') != '' || $this->CI->session->userdata('userGroupID') != '') {
                return true;
            } else {
                $this->logout();
                return false;
            }
        } else {
            return false;
        }
    }

    public function logout() {
        $newdata = array(
            'adminID' => '',
            'adminUserName' => '',
            'adminEmail' => '',
            'adminLastLogin' => '',
            'userGroupID' => '',
            'adminLoggedIn' => FALSE
        );
        $this->CI->session->set_userdata($newdata);
    }

    public function isSuperAdmin() {
        if ($this->loggedIn()) {
            if ($this->CI->session->userdata('userGroupID') == 1) {
                return true;
            } else {
                return false;
            }
        } else {
            $this->logout();
            return false;
        }
    }

    public function validatePermission() {
        if (!$this->isSuperAdmin()) {
            $controller = $this->CI->router->fetch_class();
            if (strtolower($controller) != 'accessdenied') {
                $permissionValue = $this->CI->generalModel->getPermissionValue($controller, $this->CI->session->userdata('userGroupID'));
                return $permissionValue;
            }else{
               return 7; 
            }
        } else {
            return 7;
        }
    }

}
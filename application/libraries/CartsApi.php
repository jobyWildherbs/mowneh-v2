<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CartsApi {

    protected $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->database();
        $this->CI->load->model('GeneralModel', 'generalModel');
        $this->CI->load->model('CartModel', 'cartmodel');
        $this->CI->load->model('ProductModel', 'productmodel');
        $this->CI->load->library('Customer');
//        $this->initializeCart();
    }

    private function initializeCart() {
        $customerID = $this->CI->customer->getId();
        $apiID = ($this->CI->session->userdata('api_id')) ? (int) $this->CI->session->userdata('api_id') : 0;
        $sessionID = $this->CI->session->session_id;
        $params = array(
            'customerID' => $customerID,
            'apiID' => $apiID,
            'sessionID' => $sessionID
        );
        $this->CI->cartmodel->initialize($params);
        if ($customerID) {
            $customerCartItems = $this->CI->generalModel->getTableValue('*', 'ec_cart', array("apiID" => 0, 'customerID' => $customerID), true);
            foreach ($customerCartItems as $value) {
                $this->CI->cartmodel->removeFromCart(array('cartID' => $value['cartID']));
                $this->add($value['productID'], $value['quantity'], json_decode($value['option']), $value['recurringID']);
            }
            // Once the customer is logged in we want to update the customers cart
            $cartItems = $this->CI->generalModel->getTableValue('*', 'ec_cart', array("apiID" => 0, 'customerID' => 0, 'sessionID' => $sessionID), true);
            foreach ($cartItems as $cart) {
                $this->CI->cartmodel->removeFromCart(array('cartID' => $cart['cartID']));
                $this->add($cart['productID'], $cart['quantity'], json_decode($cart['option']), $cart['recurringID']);
            }
        }
    }

    public function add($product_id, $quantity = 1, $option = '', $recurring_id = 0,$userID='',$token='') {
      
        $apiID = ($this->CI->session->userdata('api_id')) ? (int) $this->CI->session->userdata('api_id') : 0;
        //$sessionID = $this->CI->session->session_id;
        $params = array(
            'product_id' => $product_id,
            'quantity' => $quantity,
            'option' => $option,
            'recurring_id' => $recurring_id,
            'customerID' => $userID,
            'apiID' => $apiID,
//            'sessionID' => $token
            'sessionID' => ''
        );
        //echo "<pre>"; print_r($params); exit;
        if (!$this->CI->cartmodel->getCartCount($params)) {
            $this->CI->cartmodel->addCart($params);
        } else {
            $this->CI->cartmodel->updateCart($params);
        }
    }

    public function update($productID, $quantity,$userID) {
        
        $apiID = '';
        $sessionID = $this->CI->session->session_id;
        $params = array(
            'quantity' => $quantity,
            'customerID' => $userID,
            'apiID' => $apiID,
//            'sessionID' => $sessionID,
            'productID' => $productID
        );
        $this->CI->cartmodel->updateCartQuantityForAPI($params);
    }

    public function remove($cartID,$customerID,$productID) {
        if ($productID > 0) {
            $params = array(
                'customerID' => $customerID,
                'cartID' => $cartID,
                'productID' => $productID
            );
            $this->CI->cartmodel->removeFromCart($params);
        }
    }

    public function clear($userID=0,$apiID=0,$sessionID='') {
//        $params = array(
//            'customerID' => $userID,
//            'apiID' => $apiID,
//            'sessionID' => $sessionID
//        );
        $params = array(
            'customerID' => $userID,
        );
        $this->CI->cartmodel->removeFromCart($params);
    }

    public function getProducts($languageID = 1,$userID='') {
        $product_data = array();
        $params = array(
            'customerID' => $userID,
            //'apiID' => $apiID,
            //'sessionID' => $sessionID
        );
        $cartItems = $this->CI->cartmodel->getCartItems($params);
        foreach ($cartItems as $cart) { 
            $stock = true;
            $productInfo = $this->CI->productmodel->selectProductDetail($cart['productID'], $languageID);
            
            if ($productInfo && ($cart['quantity'] > 0)) {
                $option_price = 0;
                $option_points = 0;
                $option_weight = 0;
                $optionPrice=0;$add=0;$optionPrice=0; 
                $optionval  =   array();
                if(@$cart['option']){
                  $optionCombo=0;
                  $optionval = explode(',', json_decode($cart['option']));  
                  $s='';
                  $combination = array();$productOptionID='';
                  foreach($optionval as $option){
                      $optionvals = explode(':', $option);$i=0;
                      foreach ($optionvals as $value) { 
                          $sign='';
                          $vals = explode('-', $value); 
                          if($vals[0] == 'priceType'){
                            if($vals[1] == 'Increment'){
                              $sign = '+';
                            }else{
                              $sign ='-';
                            }
                          } 
                          if($vals[0] == 'productOptionID'){
                            $optionCombo = $vals[1];
                            $productOptionID = $vals[1];
                          }
                          //echo $combo; echo "</br>";
                          $s= $sign;
                          if($vals[0] == 'price'){  
                            $optionPrice=$vals[1];
                          }
                          //echo $sign.$optionPrice; 
                      }
                      $combination[] = $productOptionID;
                    
                  }
                 
                  //$where .=" pd.productID =".$cart['productID'];
                 $optionInfo = $this->CI->optionmodel->selectOptionValueByOptionValID($cart['productID'],array_unique($combination), $languageID);
                 //print_r($optionInfo);
                 if(@$optionInfo){
                    $optionPrice += $optionInfo['price'];
                 }
                    
                  
                }
                 if($optionPrice != 0){
                  $price = round($optionPrice,2,PHP_ROUND_HALF_UP);
                }else{
                  $price = round($productInfo['price'],2,PHP_ROUND_HALF_UP);;
                }
                
                $product_data[] = array(
                    'cartID' => $cart['cartID'],
                    'productID' => $productInfo['productID'],
                    'name' => $productInfo['name'],
                    'option' => $optionval,
                    'model' => $productInfo['model'],
                    'image' => $productInfo['image'],
                    'quantity' => $cart['quantity'],
                    'minimum' => $productInfo['minimum'],
                    'price' => ($price + $option_price),
                    'total' => ($price + $option_price) * $cart['quantity']
                );
            } else {
               
                $this->remove($cart['cartID'],$userID,$productInfo['productID']);
            }
        }
//        echo "<pre>"; print_r($product_data); exit;
        return $product_data;
    }

    public function getSubTotal($languageID='',$userID='') {
        $total = 0;
      
        foreach ($this->getProducts($languageID,$userID) as $product) {
            $total += $product['total'];
        }

        return $total;
    }

    public function getTotal($languageID='',$userID='',$couponData=array(),$cityID   =   "" , $deleveryType  =   'normal') {
        $shippingCharge =   0;
        $discountValue  =   0;
        $deliveryLocation = $this->CI->gm->getTableValue('*', 'ec_warehouse_delivery_location', 'cityID="' .$cityID . '"', FALSE);

        if($deleveryType=='normal')
        {
        if(@$deliveryLocation['removeNorDelPrice']){
            if($this->getSubTotal($languageID,$userID) >= $deliveryLocation['removeNorDelPrice'] && $deliveryLocation['removeNorDelPrice']!=0)

            {
               $shippingCharge  =   0;
            }else{
                $shippingCharge =   $deliveryLocation['normalDeliveryCharge'];
            }
            }
        }else if($deleveryType=='express'){
             if($this->getSubTotal($languageID,$userID) >= $deliveryLocation['removeExpDelPrice'] && $deliveryLocation['removeExpDelPrice']!=0)
            {
               $shippingCharge  =   0;
            }else{
                $shippingCharge =   $deliveryLocation['expressDeliveryCharge'];
            }
            
        }
        
       if(!empty($couponData)) {
             $couponID   =   $couponData['couponData']['couponID'];
            $couponData =   $this->CI->gm->getTableValue('*','ec_coupon',array('couponID'=>$couponID));
            //echo "<pre>"; print_r($couponData); exit;
            $discountValue =$this->getCouponDiscountValue($couponData,$languageID,$userID,$shippingCharge);
            $totals[] = array(
                'code' => 'coupon_discount',
                'title' => $this->CI->lang->line('Coupon_Applied'),
                'value' => $discountValue,
                'sort_order' => 4,
                'languageKey' => 'Coupon_Applied'
            );
       }
        // 0 means shipping value if any changes we can update here
        $total = $this->getSubTotal($languageID,$userID) - $this->getProductDiscounts($languageID,$userID)+ $shippingCharge -$discountValue;
        if($total < 0){
        $total =0;
        }else{
        $total=$total;
        }
        return $total;
    }

    public function getProductDiscounts($languageID,$userID) {
 
        $totalDiscount = 0;
        $hasProducts    =   $this->getProducts($languageID,$userID);
        //echo "<pre>";        print_r($hasProducts); exit;
        foreach ($hasProducts as $product) {
            $productOffer    = $this->CI->generalModel->getTableValue('*', 'ec_premotion_to_product', 'productID = '.$product['productID'].' AND priority=(select max(priority) from ec_premotion_to_product)  AND dateStart<=NOW() AND dateEnd>=NOW()', FALSE);
            if($productOffer){
                            $amount = ($product['price']*$productOffer['offer'])/100; 
                            $discountPrice = $amount*$product['quantity'];
                            $totalDiscount += $discountPrice;
            }else{
                 $productParam = array(
                    'productID' => $product['productID'],
                    'customerGroupID' => $this->CI->customer->getGroupId(),
                    'discountQuantity' => $product['quantity']
                );
                $product_discount = $this->CI->cartmodel->getProductDiscount($productParam);
                $discountPrice = 0;
                if($product_discount){
                    $orderQuantity = $this->CI->cartmodel->getProductOrderQuantity($product['productID'],$product_discount['dateStart'],$product_discount['dateEnd']);
                    //print_r($orderQuantity);exit;
                    if($product_discount['quantity']>($orderQuantity+$product['quantity'])){
                        if($product_discount['type']=='fixed'){
                            $discountPrice = $product_discount['price']*$product['quantity'];
                        }else if($product_discount['type']=='percentage' || $product_discount['type']==''){
                            $amount = ($product['price']*$product_discount['price'])/100;
                            $discountPrice = $amount*$product['quantity'];
                        }
                    }else if(($product_discount['quantity']-$orderQuantity) >0){
                        $discountQuantity = $product_discount['quantity']-$orderQuantity;
                        if($product_discount['type']=='fixed'){
                            $discountPrice = $product_discount['price']*$discountQuantity;
                        }else if($product_discount['type']=='percentage'){
                            $amount = ($product['price']*$product_discount['price'])/100;
                            $discountPrice = $amount*$discountQuantity;
                        }
                    }
                }
                $totalDiscount += $discountPrice;
            }
           
        }
        return $totalDiscount;
    }

    public function getTotalItemsList($languageID = 1,$userID='',$couponDataVal=array(),$cityID   =   "" , $deleveryType  =   'normal') {
       
        //$shippingCharge  = $this->getSettingValue('shipping_charge',$languageID);
        
        $language = $this->CI->gm->getFieldValue('name','ec_language',array('languageID'=>$languageID));
        
        $this->CI->lang->load('cart', $language);
         
        $deliveryLocation = $this->CI->gm->getTableValue('*', 'ec_warehouse_delivery_location', 'cityID="' .$cityID . '"', FALSE);
        
//        echo "<pre>"; print_r($deliveryLocation); exit;
       
        if($deleveryType=='normal')
        {
            if($this->getSubTotal($languageID,$userID) >= $deliveryLocation['removeNorDelPrice'])
            {
               $shippingCharge  =   0;
            }else{
                $shippingCharge =   $deliveryLocation['normalDeliveryCharge'];
            }
        }else if($deleveryType=='express'){
             if($this->getSubTotal($languageID,$userID) >= $deliveryLocation['removeExpDelPrice'])
            {
               $shippingCharge  =   0;
            }else{
                $shippingCharge =   $deliveryLocation['expressDeliveryCharge'];
            }
            
        }
        
        $totals[] = array(
            'code' => 'sub_total',
            'title' => $this->CI->lang->line('Subtotal'),
            'value' => $this->getSubTotal($languageID,$userID),
            'sort_order' => 1,
            'languageKey' => 'Subtotal'
        );
      
        if ($this->getProductDiscounts($languageID,$userID)) {
            $totals[] = array(
                'code' => 'discounts',
                'title' => $this->CI->lang->line('discounts'),
                'value' => $this->getProductDiscounts($languageID,$userID),
                'sort_order' => 2,
                'languageKey' => 'discounts'
            );
        }
        
        if ($shippingCharge>0) {
            $totals[] = array(
                'code' => 'shipping_charge',
                'title' => $this->CI->lang->line('shipping_charge'),
                'value' => $shippingCharge,
                'sort_order' => 3,
                'languageKey' => 'shipping_charge',
            );
        }
       
        
        if (!empty($couponDataVal)) {
            //echo "<pre>"; print_r($couponDataVal); exit;
            //$discountValue =0;
            $couponID   =   $couponDataVal['couponData']['couponID'];
            $couponData =   $this->CI->gm->getTableValue('*','ec_coupon',array('couponID'=>$couponID));
            //echo "<pre>"; print_r($couponData); exit;
            //echo $couponData['type'];
            $discountValue =$this->getCouponDiscountValue($couponData,$languageID,$userID,$shippingCharge);
            $totals[] = array(
                'code' => 'coupon_discount',
                'title' => $this->CI->lang->line('Coupon_Applied'),
                'value' => $discountValue,
                'sort_order' => 4,
                'languageKey' => 'Coupon_Applied'
            );
        
        }
         //echo "<pre>"; print_r($couponDataVal); exit;
        $totals[] = array(
            'code' => 'total',
            'title' => $this->CI->lang->line('Total'),
            'value' => $this->getTotal($languageID,$userID,$couponDataVal,$cityID,$deleveryType),
            'sort_order' => 3,
            'languageKey' => 'Total'
        );
        return $totals;
    }
    
    public function getCouponDiscountValue($couponData=array(),$languageID=1,$userID='',$shippingCharge=0){
        //print_r($couponData);
        $discountValue = 0;
        //echo $userID;exit;
        if(!empty($couponData) && $userID!=''){
        if(($this->getSubTotal($languageID,$userID)-$this->getProductDiscounts($languageID,$userID))>=$couponData['minimum_amount_purchase']){
                if($couponData['applayFor']=='delivery'){
                if($couponData['type']=='Percentage'){
                   
                    if($couponData['minimum_amount_purchase']!=0){
                        if($couponData['minimum_amount_purchase']<=($this->getSubTotal($languageID,$userID)-$this->getProductDiscounts($languageID,$userID))){
                            
                       $discountValue  =   ($shippingCharge*$couponData['discount'])/100;
                        if($discountValue > $couponData['max_discount']){
                             $discountValue  =   $couponData['max_discount'];
                        
                            }
                        }else{
                            $discountValue  =  0;
                        }
                    }else{
                        $discountValue  =   ($shippingCharge*$couponData['discount'])/100;
                        if($discountValue > $couponData['max_discount']){
                             $discountValue  =   $couponData['max_discount'];
                        
                        }
                    }
                }elseif($couponData['type']=='Fixed'){
                    if($couponData['minimum_amount_purchase']!=0){
                        if($couponData['minimum_amount_purchase']<=($this->getSubTotal($languageID,$userID)-$this->getProductDiscounts($languageID,$userID))){
                            $discountValue  =   $couponData['discount'];
                        }else{
                            $discountValue  =  0;
                        }
                    }else{
                        $discountValue  =   $couponData['discount'];
                    }
                }
            }else{
               if($couponData['type']=='Percentage'){ 
                    if($couponData['minimum_amount_purchase']!=0){
                       
                        if($couponData['minimum_amount_purchase']<=($this->getSubTotal($languageID,$userID)-$this->getProductDiscounts($languageID,$userID))){
                        $discountValue  =   (($this->getSubTotal($languageID,$userID)-$this->getProductDiscounts($languageID,$userID))*$couponData['discount'])/100;
                            if($discountValue > $couponData['max_discount']){
                             $discountValue  =   $couponData['max_discount'];
                        
                            }
                        }else{
                            $discountValue  =   $couponData['max_discount'];
                        }
                    }else{
//                        $discountValue  =   (($this->getSubTotal($languageID,$userID)-$this->getProductDiscounts($languageID,$userID))*$couponData['discount'])/100;
//                        if($discountValue > $couponData['max_discount']){
//                             $discountValue  =   $couponData['max_discount'];
//                        
//                            }
                            
                            
                        if($couponData['max_discount']!=0){
                           $discountValue  =   (($this->getSubTotal($languageID,$userID)-$this->getProductDiscounts($languageID,$userID))*$couponData['discount'])/100;
                            if($discountValue > $couponData['max_discount']){
                                $discountValue  =   $couponData['max_discount'];
                        
                            }
                       }else{
                           $discountValue  =   (($this->getSubTotal($languageID,$userID)-$this->getProductDiscounts($languageID,$userID))*$couponData['discount'])/100;
                           
                       }
                    }
                }elseif($couponData['type']=='Fixed'){
                    if($couponData['minimum_amount_purchase']!=0){
                        if($couponData['minimum_amount_purchase']<=($this->getSubTotal($languageID,$userID)-$this->getProductDiscounts($languageID,$userID))){
                        $discountValue  =   $couponData['discount'];
                            
                        }else{
                            $discountValue  =  0;
                        }
                    }else{
                        $discountValue  =   $couponData['discount'];
                    }
                }
                }
            }
        }
            return $discountValue;
    }

    public function countProducts($languageID,$userID) {
        $product_total = 0;

        $products = $this->getProducts($languageID,$userID);

        foreach ($products as $product) {
            $product_total += $product['quantity'];
        }

        return $product_total;
    }

    public function hasProducts($languageID='',$userID='') {
        return count($this->getProducts($languageID,$userID));
    }

    public function getProductQuantity($productID,$customerID) {
//        $customerID = $this->CI->customer->getId();
//        $apiID = ($this->CI->session->userdata('api_id')) ? (int) $this->CI->session->userdata('api_id') : 0;
//        $sessionID = $this->CI->session->session_id;
        $params = array(
            'customerID' => $customerID,
//            'apiID' => $apiID,
//            'sessionID' => $sessionID,
            'productID' => $productID
        );
        $value = $this->CI->generalModel->getFieldValue('quantity', 'ec_cart', $params);
        
        return ($value) ? (int) $value : 0;
    }
    
     public function getProductOptionQuantity($productID,$productOptionInfo,$customerID) {
        $params = array(
            'customerID' => $customerID,
            'productID' => $productID,
            'productOptionInfo'=>$productOptionInfo
        );
        $value = $this->CI->cartmodel->getProductOptionQuantityForAPI($params);
        return ($value) ? (int) $value : 0;
    }

    public function isValidCart() {
        $flag = 0;
        $products = $this->getProducts();
        foreach ($products as $product) {
            $productInfo = $this->CI->generalModel->getTableValue('quantity,minimum,status,dateAvailable', 'ec_product', array("productID" => $product['productID'], 'status' => 'Active'));
            if ($productInfo['status'] != 'Active') {
                $flag = 1;
            }
            if ($productInfo['dateAvailable'] != '0000-00-00' && $productInfo['dateAvailable'] > date('Y-m-d')) {
                $flag = 1;
            }
            if ($productInfo['quantity'] < $this->getProductQuantity($product['productID'])) {
                $flag = 1;
            }
        }
        if ($flag == 1) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
     protected function getSettingValue($settingKey = '',$languageID=1) {
        if ($settingKey != '') {
            $settingDetails = $this->CI->generalModel->getTableValue('*', 'ec_settings', array('fieldKey' => $settingKey));
            if ($settingDetails) {
                if ($settingDetails['isLanguage'] == 'yes') {
                    $value = $this->CI->generalModel->getFieldValue('value', 'ec_settings_field_details', array('settingsFieldID' => $settingDetails['id'], 'languageID' => $languageID));
                    return $value;
                } else {
                    return $settingDetails['value'];
                }
            }
        }
        return;
    }

}

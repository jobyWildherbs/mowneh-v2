<?php
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
defined('BASEPATH') OR exit('No direct script access allowed');

class Import {

    protected $CI;
    
     // Columns names after parsing
    private $fields;
    // Separator used to explode each line
    private $separator = ',';
    // Enclosure used to decorate each field
    private $enclosure = '"';
    // Maximum row size to be used for decoding
    private $max_row_size = 4096;
    
    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->database();
        $this->CI->load->model('GeneralModel', 'generalModel');
       
    }
    
    public function exportFile($fileName='demo.xlsx',$header,$data){
        
        $cells    =    $this->letterArray(count($header));
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        foreach($header as $key=>$headVal){
            $sheet->setCellValue($cells[$key].'1', $headVal);
        }   
        $rows = 2;
        
        foreach ($data as $key=>$dataVal){
            $cellCount  =   0;
            foreach($dataVal as $innerKey=>$val){
                $sheet->setCellValue($cells[$cellCount].$rows, $val);
                $cellCount++;
            }
            $rows++;
        } 
        $writer = new Xlsx($spreadsheet);
		$writer->save("uploads/temp/".$fileName);
		header("Content-Type: application/vnd.ms-excel");
        redirect(base_url()."/uploads/temp/".$fileName); 
        return TRUE;
    }
    
    function letterArray($count){  
        $result =   array();
        for($i=1;$i<=$count;$i++){
            $result[]   =   $this->columnLetter($i);
        }
        return $result;    
    }
    
    
    function columnLetter($c){  
        $c = intval($c);
        if ($c <= '0') 
        {
            return '';

        }
        $letter = '';        
        while($c != 0){
           $p = ($c - 1) % 26;
           $c = intval(($c - $p) / 26);
           $letter = chr(65 + $p) . $letter;
        }
        return $letter;    
    }
    
    public function requiredField($exportPage=""){
        $requiredField  =   array();
        if($exportPage=='category'){
            $requiredField             =   array('name','description','filter');
        }elseif($exportPage=='product'){
            $requiredField             =   array('name','model','quantity','price','manufacturer','attribute','minimum','description','highlights','category','filter');
        }elseif($exportPage=='customers'){
            $requiredField             =   array('firstname','lastname','email','password','telephone');
        }elseif($exportPage=='brands'){
            $requiredField             =   array('name');
        }elseif($exportPage=='filter'){
            $requiredField             =   array('filterGroupName','sortOrder','filterName');
        }elseif($exportPage=='attribute'){
            $requiredField             =   array('attributeGroupName','sortOrder','attributeName');
        }
       
        return $requiredField;
    }
    public function  tableFields($exportPage){  
        $exportField    =   array();
        if($exportPage=='category'){
//            $exportField['status']           =    'status';
            $exportField['parentCategory']   =    'parent Category';
            $exportField['name']             =    'Name';
            $exportField['name_ar']          =    'Arabic Name';
            $exportField['description']      =    'Description';
             $exportField['description_ar']  =    'Arabic Description';
            $exportField['metaTitle']        =    'Meta Title';
            $exportField['metaTitle_ar']     =    'Arabic Meta Title';
            $exportField['metaDescription']  =    'Meta Description';
            $exportField['metaDescription_ar']  = 'Arabic Meta Description';
            $exportField['metaKeyword']      =    'Meta Keyword';
            $exportField['metaKeyword_ar']   =    'Arabic Meta Keyword';
            $exportField['filter']           =    'Filter Group Name';
            
            $exportField['image']            =    'Listing Image';
            $exportField['banner']           =    'banner Image';
            $exportField['sortOrder']        =    'Sort Order';
        }elseif($exportPage=='product'){
            $exportField['name']             =   'Name';
            $exportField['name_ar']          =   'Arabic Name';
            $exportField['model']            =   'Model';
            $exportField['sku']              =   'SKU';
            $exportField['quantity']         =   'Quantity'; 
            $exportField['price']            =   'Price';
            $exportField['image']            =   'Images';
            
            $exportField['manufacturer']     =   'Brand';
            $exportField['attribute']        =   'Attribute';
            $exportField['weight']           =   'Weight';
            $exportField['length']           =   'Length';
            $exportField['width']            =   'Width';
            $exportField['height']           =   'Height'; 
            $exportField['minimum']          =   'Minimum Quantity';
            
            $exportField['status']           =   'status';
                    
            $exportField['description']      =   'Description';
            $exportField['description_ar']   =   'Arabic Description';
            $exportField['highlights']       =   'Highlights';
            $exportField['highlights_ar']    =   'Arabic Highlights';
            $exportField['tag']              =   'Tag';
            $exportField['tag_ar']           =   'Arabic Tag';
            $exportField['metaTitle']        =   'Meta Title';
            $exportField['metaTitle_ar']     =   'Arabic Meta Title';
            $exportField['metaDescription']  =    'Meta Description';
            $exportField['metaDescription_ar']= 'Arabic Meta Description';
            $exportField['metaKeyword']      =    'Meta Keyword';
            $exportField['metaKeyword_ar']   =    'Arabic Meta Keyword';
            
            
            $exportField['category']                =   'Category';
            $exportField['relatedProduct']          =   'Related Product';
            
            $exportField['discPrice']               =   'Discount (%)';
            $exportField['discQuantity']            =   'Discount Quantity';
            $exportField['discPriority']            =   'Discount Priority';
            $exportField['discDateStart']           =   'Discount Date Start';
            $exportField['discDateEnd']             =   'Discount Date End';
            $exportField['filter']                  =   'Filter';
            $exportField['SortOrder']               =   'Sort Order';
            $exportField['status']                  =   'Status';
            
        }elseif($exportPage=='brands'){
//            $exportField['pageKey']          =   'Page Key';
            $exportField['status']           =   'Status';
            $exportField['name']             =   'Name';
            $exportField['name_ar']          =   'Arabic Name';
        }elseif($exportPage=='customers'){
//            $exportField['customerGroupID']             =   'Customer Group ID';
//            $exportField['storeID']                     =   'Store ID'; 
            $exportField['firstname']                   =   'First Name';
            $exportField['lastname']                    =   'Last Name'; 
            $exportField['email']                       =   'Email';
            $exportField['password']                    =   'Password';
            $exportField['telephone']                   =   'Telephone';
//            $exportField['ip']                          =   'IP';
            //$exportField['status']                      =   'Status';
        }elseif($exportPage=='filter'){
            $exportField['filterGroupName']         =   'Filter Group Name';
            $exportField['filterGroupName_ar']      =   'Arabic Filter Group Name';
            $exportField['sortOrder']               =   'Sort Order'; 
            $exportField['filterName']              =   'Filter Name';
            $exportField['filterName_ar']           =   'Arabic Filter Name';
        }elseif($exportPage=='attribute'){
            $exportField['attributeGroupName']      =   'Attribute Group Name';
             $exportField['attributeGroupName_ar']  =   'Arabic Attribute Group Name';
             $exportField['sortOrder']              =   'Sort Order';
             $exportField['attributeName']          =   'Attribute Name';
             $exportField['attributeName_ar']       =   'Arabic Attribute Name';
        }
        
        
        
        return $exportField;    
    }
    
    function parse_Productcsv($filepath){
        
        if (ob_get_length()) ob_end_clean();
        ob_start();
        // If file doesn't exist, return false
        if(!file_exists($filepath)){        
            return FALSE;            
        }
        
        // Open uploaded CSV file with read-only mode
        $csvFile = fopen($filepath, 'r');
        
        // Get Fields and values
        $this->fields = fgetcsv($csvFile, $this->max_row_size, $this->separator, $this->enclosure);
        $keys = $this->escape_string($this->fields);
     
        
     
        
        $newkey =   array();
        $tempAttGrp =   "";
            foreach($keys as $InnerKey=>$keyvalue){
                if($this->startsWith($keyvalue, "~")){
                    
                    $attributeval   =   explode("~",$keyvalue);
                    $attGrpImportKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($attributeval[2])));
                    $attributeGroupID   =   $this->CI->generalModel->getFieldValue('attributeGroupID', 'ec_attribute_group','importKey="'.$attGrpImportKey.'" AND status="Active"');
                    if($attributeGroupID==''){
                        $err[]  =   $this->attributeGrpNotExist($attributeval[2]);
                    }
                    
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($attributeval[1])));
                    $attributeID   =   $this->CI->generalModel->getFieldValue('attributeID', 'ec_attribute','importKey="'.$importKey.'" AND attributeGroupID="'.$attributeGroupID.'" AND status="Active"');
                    if($attributeID==''){
                        $err[]  =   $this->attributeNotExist($attributeval[2]);
                        
                    }
                    $keys[$InnerKey] =   $attributeval[2].'->'.$attributeval[1].'~Attribute';
                }
            }
            $error  =   0;
            if(!empty($err)){
                $err['error']  =   1;
                $err    =   array_unique($err, SORT_STRING );
               
                return $err; exit;
            }
        // Store CSV data in an array
        $csvData = array();
        $i = 0;
        
        while(($row = fgetcsv($csvFile, $this->max_row_size, $this->separator)) !== FALSE){
            //echo "<pre>"; print_r($row);   echo "</pre>";
            if(count($keys) == count($row)){
                    $arr        = array();
                    $new_values = array();
                    $new_values = $this->escape_string($row);
                    $attribute  =   '';
                    for($j = 0; $j < count($keys); $j++){
                       
                        if($new_values[$j]){
                            if($keys[$j] != ""){
                                $explode =   explode("~",$keys[$j]); 

                                if(@$explode['1']=='Attribute'){
                                    $attribute  .= $explode['0'].'*'.$new_values[$j]."~";
                                //$arr['Attribute'] = strip_tags(htmlentities($attribute));
                                    $arr['Attribute'] = $attribute;
                                }
                                else{
                                $arr[$keys[$j]] = strip_tags($new_values[$j]);
                                }
                            }
                        }
                    }
                    
                    $csvData[$i] = $arr;
                    $i++;
                }
            }
//            echo "<pre>"; print_r($csvData); 
//            exit;
        fclose($csvFile);
      
        return $csvData;
    }
    
    
     function parse_csv($filepath){
        
        ob_end_clean();
        ob_start();
        // If file doesn't exist, return false
        if(!file_exists($filepath)){        
            return FALSE;            
        }
        
        // Open uploaded CSV file with read-only mode
        $csvFile = fopen($filepath, 'r');
        
        // Get Fields and values
        $this->fields = fgetcsv($csvFile, $this->max_row_size, $this->separator, $this->enclosure);
        $keys = $this->escape_string($this->fields);
          
        // Store CSV data in an array
        $csvData = array();
        $i = 0;
        
        while(($row = fgetcsv($csvFile, $this->max_row_size, $this->separator)) !== FALSE){
            if(count($keys) == count($row)){
                    $arr        = array();
                    $new_values = array();
                    $new_values = $this->escape_string($row);
                    for($j = 0; $j < count($keys); $j++){
                        if($keys[$j] != ""){
                            $arr[$keys[$j]] = $new_values[$j];
                        }
                    }
                    $csvData[$i] = $arr;
                    $i++;
                }
            }
        fclose($csvFile);
      
        return $csvData;
    }

    function escape_string($data){
        $result = array();
        foreach($data as $row){
            $result[] = str_replace('"', '', $row);
        }
        return $result;
    }
    
    public function setRowMessage($exportPage='',$key='',$rowValue='',$rowCount=0){
        $field  =   $this->tableFields($exportPage);
        $fieldValue =   $field[$key];
        $message    =   "Row number ".$rowCount." was not inserted.Given ".$fieldValue." (".$rowValue.") not found.";
        return $message;
    }
    
    public function setRowNumericMessage($exportPage='',$key='',$rowValue='',$rowCount=0){
        $field  =   $this->tableFields($exportPage);
        $fieldValue =   $field[$key];
        $message    =   "Row number ".$rowCount." was not inserted.Incorrect  ".$fieldValue." (".$rowValue.").Must be numeric.";
        return $message;
    }
    
    public function setRowDateMessage($exportPage='',$key='',$rowValue='',$rowCount=0){
      
        $field  =   $this->tableFields($exportPage);
        $fieldValue =   $field[$key];
        $message    =   "Row number ".$rowCount." was not inserted.Incorrect  ".$fieldValue." (".$rowValue.").Must be d-m-Y.";
        return $message;
    }
    
    public function setProductExistMessage($exportPage='',$rowValue='',$rowCount=0){
        $message    =   "Row number ".$rowCount." was not inserted.Given product  (".$rowValue.") already exist.";
        return $message;
    }
    
    public function setAttributeExistMessage($exportPage='',$rowValue='',$rowCount=0){
        $message    =   "Row number ".$rowCount." was not inserted.Given attribute  (".$rowValue.") already exist.";
        return $message;
    }
    
    public function setFilterExistMessage($exportPage='',$rowValue='',$rowCount=0){
        $message    =   "Row number ".$rowCount." was not inserted.Given filter  (".$rowValue.") already exist.";
        return $message;
    }
    
     public function setFilterGroupNotExistMessage($exportPage='',$rowValue='',$rowCount=0){
        $message    =   "Row number ".$rowCount." was not inserted.Given attribute group name (".$rowValue.") not found.";
        return $message;
    }
    
    public function setValidationMessage($field='',$rowCount=0){
        $message    =   $field." not found in row number ".$rowCount;
        return $message;
    }
    
    public function setCategoryFilterMessage($filterData='',$filterGroupName='',$rowCount=0,$filterCategory=''){
        $message    =   "Row number ".$rowCount." was not inserted.Given Filter  (".$filterData.") Group (".$filterGroupName.") is not applicable to the selected category (".$filterCategory.").";
   
        return $message;
    }

    public function startsWith($string, $startString) { 
        $len = strlen($startString); 
        return (substr($string, 0, $len) === $startString); 
    } 
    
    public function validateDate($date, $format = 'd-m-Y')
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }
    
    public function attributeGrpNotExist($attributeName){
        $message    =   "Given attribute group ".$attributeName." not exist";
        return $message;
    }
    
    public function attributeNotExist($attributeName){
        $message    =   "Given attribute ".$attributeName." not exist";
        return $message;
    }
   
    
   
}
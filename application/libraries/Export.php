<?php
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
defined('BASEPATH') OR exit('No direct script access allowed');

class Export {

    protected $CI;

    public function __construct() {
    }
    
    public function exportFile($fileName='demo.csv',$header,$data){
        //echo "<pre>"; print_r($data); exit;
        $cells    =    $this->letterArray(count($header));
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        foreach($header as $key=>$headVal){
            $sheet->setCellValue($cells[$key].'1', $headVal);
        }   
        $rows = 2;
        
        foreach ($data as $key=>$dataVal){
            $cellCount  =   0;
            foreach($dataVal as $innerKey=>$val){
                $sheet->setCellValue($cells[$cellCount].$rows, $val);
                $cellCount++;
            }
            $rows++;
        } 
//        $writer = new Xlsx($spreadsheet);
        $writer = new Csv($spreadsheet);
		$writer->save("uploads/temp/".$fileName);
                //header("Content-Type: application/vnd.ms-excel");
		header("Content-Type: text/x-csv");
        redirect(base_url()."/uploads/temp/".$fileName); 
        return TRUE;
    }
    
    function letterArray($count){  
        $result =   array();
        for($i=1;$i<=$count;$i++){
            $result[]   =   $this->columnLetter($i);
        }
        return $result;    
    }
    
    
    function columnLetter($c){  
        $c = intval($c);
        if ($c <= '0') 
        {
            return '';

        }
        $letter = '';        
        while($c != 0){
           $p = ($c - 1) % 26;
           $c = intval(($c - $p) / 26);
           $letter = chr(65 + $p) . $letter;
        }
        return $letter;    
    }
    
    public function  tableRows($exportPage){  
        $exportField    =   array(); 
        if($exportPage=='category'){
            $exportField['category']['categoryID']                =   'Category ID';
            $exportField['category']['pageKey']                   =   'Page Key';
            $exportField['category']['status']                    =    'status';
            $exportField['category']['parentID']                  =    'parent Category Id';
            $exportField['category_detail']['name']               =    'Name';
            $exportField['category_detail']['description']        =    'Description';
            $exportField['category']['parentName']                =    'parent Category Name';
            $exportField['category_detail']['metaTitle']          =    'Meta Title';
            $exportField['category_detail']['metaDescription']    =    'Meta Description';
            $exportField['category_detail']['metaKeyword']        =    'Meta Keyword';
            $exportField['category_filter']['filterID']           =    'Filter';
        }elseif($exportPage=='product'){
            $exportField['product']['productID']                    =   'Product ID';
            $exportField['product_description']['name']             =   'Name';
            $exportField['product']['model']                        =   'Model';
            $exportField['product']['sku']                          =   'SKU';
            $exportField['product']['quantity']                     =   'Quantity';
            $exportField['product']['price']                        =   'Price';
            $exportField['product']['manufacturerID']               =   'Brand';
            $exportField['product']['dateAvailable']                =   'Date Available';
            $exportField['product']['weight']                       =   'Weight';
            $exportField['product']['length']                       =   'Length';
            $exportField['product']['width']                        =   'Width';
            $exportField['product']['height']                       =   'Height'; 
            $exportField['product']['minimum']                      =   'Minimum Order';
            $exportField['product']['dateAdded']                    =   'Date Added';
            $exportField['product']['status']                       =   'status';

            $exportField['product_description']['description']      =   'Description';
            $exportField['product_description']['highlights']       =   'Highlights';
            $exportField['product_description']['highlights']       =   'Highlights';
            $exportField['product_to_category']['category']         =   'Category';
            $exportField['product_related']['relatedProduct']       =   'Related Product';
            $exportField['product_discount']['price']               =   'Discount Price';
            $exportField['product_discount']['quantity']            =   'Discount Quantity';
            $exportField['product_discount']['priority']            =   'Discount Priority';
            $exportField['product_discount']['dateStart']           =   'Discount Date Start';
            $exportField['product_discount']['dateEnd']             =   'Discount Date End';
        }elseif($exportPage=='brands'){
            $exportField['manufacturer']['manufacturerID']          =   'manufacturer ID';
            $exportField['manufacturer']['pageKey']                 =   'Page Key';
            $exportField['manufacturer']['status']                  =   'Status';
            
            $exportField['manufacturer_detail']['name']             =   'Name';
        }elseif($exportPage=='order'){
            $exportField['order']['orderID']                        =   'Order ID';
            $exportField['order']['invoiceNo']                      =   'Invoice No';
            $exportField['order']['invoicePrefix']                  =   'Invoice Prefix'; 
            $exportField['order']['storeID']                        =   'Store ID'; 
            $exportField['order']['storeName']                      =   'Store Name';
            $exportField['order']['storeUrl']                       =   'storeUrl';
            $exportField['order']['customerID']                     =   'Customer ID';
            $exportField['order']['customerName']                   =   'Customer Name'; 
            $exportField['order']['customerGroupID']                =   'Customer Group ID'; 
            $exportField['order']['firstname']                      =   'First Name'; 
            $exportField['order']['lastname']                       =   'Last Name'; 
            $exportField['order']['email']                          =   'Email';
            $exportField['order']['telephone']                      =   'Telephone';
            $exportField['order']['telephone']                      =   'Telephone';
            $exportField['order']['paymentFirstname']               =   'Payment First Name';
            $exportField['order']['paymentLastname']                =   'Payment Last Name';
            $exportField['order']['paymentCompany']                 =   'Payment Company';
            $exportField['order']['paymentAddress1']                =   'Payment Address1';
            $exportField['order']['paymentAddress2']                =   'Payment Address2';
            $exportField['order']['paymentCity']                    =   'Payment City';
            $exportField['order']['paymentPostcode']                =   'Payment Post Code';
            $exportField['order']['total']                          =   'Total';
            
            $exportField['order_status_detail']['name']             =   'Order Status';
        }elseif($exportPage=='customers'){
            $exportField['customer']['customerID']                  =   'Customer ID';
//            $exportField['customer']['customerGroupID']             =   'Customer Group ID';
            $exportField['customer']['storeID']                     =   'Store ID'; 
            $exportField['customer']['firstname']                   =   'First Name';
            $exportField['customer']['lastname']                    =   'Last Name'; 
            $exportField['customer']['email']                       =   'Email';
            $exportField['customer']['telephone']                   =   'Telephone';
            $exportField['customer']['addressID']                   =   'Address'; 
            $exportField['customer']['ip']                          =   'IP';
            $exportField['customer']['status']                      =   'Status';
            $exportField['customer']['lastLogin']                   =   'Last Login';
            $exportField['customer']['dateAdded']                   =   'Date Added';
        }elseif($exportPage=='filter'){
            $exportField['filter']['filterID']                      =   'Filter ID';
            $exportField['filter']['filterGroupID']                 =   'Filter Group';
            $exportField['filter']['sortOrder']                     =   'Sort Order'; 
            $exportField['filter_detail']['name']                   =   'Name';
        }
        
        
        return $exportField;    
    }


    
   
}
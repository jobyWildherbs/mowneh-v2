<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Apisupport {

    protected $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('CustomerModel', 'customerModel');
        $this->CI->load->model('GeneralModel', 'generalModel');
        $this->CI->load->helper('common_helper');
    }
    
    public function intervelCheck($tokenTime){
//        date_default_timezone_set('Asia/Kolkata'); 
        $hour1 = 0; $hour2 = 0;      
        $datetimeObj1 = new DateTime($tokenTime);
        $datetimeObj2 = new DateTime(date('Y-m-d H:i:s'));
        $interval = $datetimeObj1->diff($datetimeObj2);
        if($interval->format('%a') > 0){
        $hour1 = $interval->format('%a')*24;
        }
        if($interval->format('%h') > 0){
        $hour2 = $interval->format('%h');

        }
        $diff = $hour1 + $hour2;

        return $diff;
    }
    
    public function updateToken($userID){
        $token             = $this->createToken(getRandomString(10));
        $tokenVal   =   array();
//        date_default_timezone_set('Asia/Kolkata'); 
        $tokenVal['token']      =   $token;
        $tokenVal['tokenTime']  =   date('Y-m-d H:i:s');
        $this->CI->generalModel->updateTableValues('ec_customer','customerID='.$userID,$tokenVal);
        //Token end
    }
    
    public function authorizedAccess(){
        $authorizedPage =   array('Login/userLogin_post',
                                 'Login/forgotPassword_post',
                                 'Login/userLoginEmailOrPhone_post',
                                 'Login/userLoginWithOtp_post',
                                 'Login/resendLoginOtp_post',
                                 'Register/userRegister_post',
                                 'Register/emailPhonenumberCheck_post',
                                 'Register/registerOtp_post',
                                 'Register/activeOtp_get',
                                 'Register/resentOtp_get',
                                 'General/getCountryCode_get'
                            );
        return $authorizedPage;
    }
    
    public function unAuthorizedAccessPage(){
        $authorizedPage =   array('General/listHomeContent_get',
                                 'Product/listProduct_get',
                                 'Product/productDetail_get',
                                 'General/listBrandsAndFilter_get',
                                 'Product/listProductWithSubCategory_get',
                                 'General/firebaseTokenSave_post',
                                 'General/listNotification_get',
                                 'General/notificationSatatusChange_post',
                                 'General/notificationCount_get',
                            );
        return $authorizedPage;
    }
    
    public function createToken($token){
        if($token){
            $hashed_token = hash('sha256', sha1(SITE_SALT . $token));
            return $hashed_token;
        }
    }
    
    public function getToken($userID){
        if($userID){
            $token =    $this->CI->generalModel->getFieldValue('token','ec_customer','customerID='.$userID);
            return $token;
        }
    }
    
    public function setUserID($userID){
        if($userID){
            $token =    $this->CI->generalModel->getFieldValue('token','ec_customer','customerID='.$userID);
            return $token;
        }
    }
    
    
}
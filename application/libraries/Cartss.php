<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Carts {

    protected $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->database();
        $this->CI->load->model('GeneralModel', 'generalModel');
        $this->CI->load->model('CartModel', 'cartmodel');
        $this->CI->load->model('ProductModel', 'productmodel');
        $this->CI->load->library('Customer');
        $this->initializeCart();
    }

    private function initializeCart() {
        $customerID = $this->CI->customer->getId();
        $apiID = ($this->CI->session->userdata('api_id')) ? (int) $this->CI->session->userdata('api_id') : 0;
        $sessionID = $this->CI->session->session_id;
        $params = array(
            'customerID' => $customerID,
            'apiID' => $apiID,
            'sessionID' => $sessionID
        );
        $this->CI->cartmodel->initialize($params);
        if ($customerID) {
            $customerCartItems = $this->CI->generalModel->getTableValue('*', 'ec_cart', array("apiID" => 0, 'customerID' => $customerID), true);
            foreach ($customerCartItems as $value) {
                $this->CI->cartmodel->removeFromCart(array('cartID' => $value['cartID']));
                $this->add($value['productID'], $value['quantity'], json_decode($value['option']), $value['recurringID']);
            }
            // Once the customer is logged in we want to update the customers cart
            $cartItems = $this->CI->generalModel->getTableValue('*', 'ec_cart', array("apiID" => 0, 'customerID' => 0, 'sessionID' => $sessionID), true);
            foreach ($cartItems as $cart) {
                $this->CI->cartmodel->removeFromCart(array('cartID' => $cart['cartID']));
                $this->add($cart['productID'], $cart['quantity'], json_decode($cart['option']), $cart['recurringID']);
            }
        }
    }

    public function add($product_id, $quantity = 1, $option = '', $recurring_id = 0) {
        $customerID = $this->CI->customer->getId();
        $apiID = ($this->CI->session->userdata('api_id')) ? (int) $this->CI->session->userdata('api_id') : 0;
        $sessionID = $this->CI->session->session_id;
        $params = array(
            'product_id' => $product_id,
            'quantity' => $quantity,
            'option' => $option,
            'recurring_id' => $recurring_id,
            'customerID' => $customerID,
            'apiID' => $apiID,
            'sessionID' => $sessionID
        );
        if (!$this->CI->cartmodel->getCartCount($params)) {
            $this->CI->cartmodel->addCart($params);
        } else {

            $this->CI->cartmodel->updateCart($params);
        }
    }

    public function update($productID, $quantity) {
        $customerID = $this->CI->customer->getId();
        $apiID = ($this->CI->session->userdata('api_id')) ? (int) $this->CI->session->userdata('api_id') : 0;
        $sessionID = $this->CI->session->session_id;
        $params = array(
            'quantity' => $quantity,
            'customerID' => $customerID,
            'apiID' => $apiID,
            'sessionID' => $sessionID,
            'productID' => $productID
        );
        $this->CI->cartmodel->updateCartQuantity($params);
    }

    public function remove($productID = 0) {
        $customerID = $this->CI->customer->getId();
        $apiID = ($this->CI->session->userdata('api_id')) ? (int) $this->CI->session->userdata('api_id') : 0;
        $sessionID = $this->CI->session->session_id;
        if ($productID > 0) {
            $params = array(
                'customerID' => $customerID,
                'apiID' => $apiID,
                'sessionID' => $sessionID,
                'productID' => $productID
            );
            $this->CI->cartmodel->removeFromCart($params);
        }
    }

    public function clear() {
        $customerID = $this->CI->customer->getId();
        $apiID = ($this->CI->session->userdata('api_id')) ? (int) $this->CI->session->userdata('api_id') : 0;
        $sessionID = $this->CI->session->session_id;
        $params = array(
            'customerID' => $customerID,
            'apiID' => $apiID,
            'sessionID' => $sessionID
        );
        $this->CI->cartmodel->removeFromCart($params);
    }

    public function getProducts($languageID = 1) {
        $product_data = array();
        $customerID = $this->CI->customer->getId();
        $apiID = ($this->CI->session->userdata('api_id')) ? (int) $this->CI->session->userdata('api_id') : 0;
        $sessionID = $this->CI->session->session_id;
        $params = array(
            'customerID' => $customerID,
            'apiID' => $apiID,
            'sessionID' => $sessionID
        );
        $cartItems = $this->CI->cartmodel->getCartItems($params);
        foreach ($cartItems as $cart) {
            $stock = true;
            $productInfo = $this->CI->productmodel->selectProductDetail($cart['productID'], $languageID);
            //print_r($productInfo);
            if ($productInfo && ($cart['quantity'] > 0)) {
                $option_price = 0;
                $option_points = 0;
                $option_weight = 0;

                $price = $productInfo['price'];

                // Product Discounts
                //$discount_quantity = 0;

                /* foreach ($cartItems as $cart_2) {
                  if ($cart_2['productID'] == $cart['productID']) {
                  $discount_quantity += $cart_2['quantity'];
                  }
                  }
                  $productParam = array(
                  'productID' => $cart['productID'],
                  'customerGroupID' => $this->CI->customer->getGroupId(),
                  'discountQuantity' => $discount_quantity
                  ); */

                //$product_discount = $this->CI->cartmodel->getProductDiscount($productParam);

                /* if ($product_discount) {
                  $price = $price - $product_discount['price'];
                  } */

                // Product Specials
                //$product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int) $cart['product_id'] . "' AND customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");

                /* if ($product_special_query->num_rows) {
                  $price = $product_special_query->row['price'];
                  } */

                // Reward Points
                /* $product_reward_query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int) $cart['product_id'] . "' AND customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "'");

                  if ($product_reward_query->num_rows) {
                  $reward = $product_reward_query->row['points'];
                  } else {
                  $reward = 0;
                  } */

                // Downloads
                /* $download_data = array();

                  $download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download p2d LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE p2d.product_id = '" . (int) $cart['product_id'] . "' AND dd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

                  foreach ($download_query->rows as $download) {
                  $download_data[] = array(
                  'download_id' => $download['download_id'],
                  'name' => $download['name'],
                  'filename' => $download['filename'],
                  'mask' => $download['mask']
                  );
                  }

                  // Stock
                  if (!$product_query->row['quantity'] || ($product_query->row['quantity'] < $cart['quantity'])) {
                  $stock = false;
                  }

                  $recurring_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "recurring r LEFT JOIN " . DB_PREFIX . "product_recurring pr ON (r.recurring_id = pr.recurring_id) LEFT JOIN " . DB_PREFIX . "recurring_description rd ON (r.recurring_id = rd.recurring_id) WHERE r.recurring_id = '" . (int) $cart['recurring_id'] . "' AND pr.product_id = '" . (int) $cart['product_id'] . "' AND rd.language_id = " . (int) $this->config->get('config_language_id') . " AND r.status = 1 AND pr.customer_group_id = '" . (int) $this->config->get('config_customer_group_id') . "'");

                  if ($recurring_query->num_rows) {
                  $recurring = array(
                  'recurring_id' => $cart['recurring_id'],
                  'name' => $recurring_query->row['name'],
                  'frequency' => $recurring_query->row['frequency'],
                  'price' => $recurring_query->row['price'],
                  'cycle' => $recurring_query->row['cycle'],
                  'duration' => $recurring_query->row['duration'],
                  'trial' => $recurring_query->row['trial_status'],
                  'trial_frequency' => $recurring_query->row['trial_frequency'],
                  'trial_price' => $recurring_query->row['trial_price'],
                  'trial_cycle' => $recurring_query->row['trial_cycle'],
                  'trial_duration' => $recurring_query->row['trial_duration']
                  );
                  } else {
                  $recurring = false;
                  } */

                $product_data[] = array(
                    'cartID' => $cart['cartID'],
                    'productID' => $productInfo['productID'],
                    'name' => $productInfo['name'],
                    'model' => $productInfo['model'],
                    'image' => $productInfo['image'],
                    'quantity' => $cart['quantity'],
                    'minimum' => $productInfo['minimum'],
                    'price' => ($price + $option_price),
                    'total' => ($price + $option_price) * $cart['quantity']
                );
            } else {
                $this->remove($cart['cartID']);
            }
        }
//print_r($product_data);
        return $product_data;
    }

    public function getSubTotal() {
        $total = 0;

        foreach ($this->getProducts() as $product) {
            $total += $product['total'];
        }

        return $total;
    }

    public function getTotal($languageID=1 , $cityID   =   "" , $deleveryType  =   'normal') {
//        if($this->getSubTotal()>0)
//            $shippingCharge  = $this->getSettingValue('shipping_charge',$languageID);
//        else
            $shippingCharge =   0;
        $deliveryLocation = $this->CI->gm->getTableValue('*', 'ec_warehouse_delivery_location', 'cityID="' .$cityID . '"', FALSE);

        if($deleveryType=='normal')
        {
if(@$deliveryLocation['removeNorDelPrice']){
            if($this->getSubTotal() >= $deliveryLocation['removeNorDelPrice'] && $deliveryLocation['removeNorDelPrice']!=0)

            {
               $shippingCharge  =   0;
            }else{
                $shippingCharge =   $deliveryLocation['normalDeliveryCharge'];
            }
            }
        }else if($deleveryType=='express'){
             if($this->getSubTotal() >= $deliveryLocation['removeExpDelPrice'] && $deliveryLocation['removeExpDelPrice']!=0)
            {
               $shippingCharge  =   0;
            }else{
                $shippingCharge =   $deliveryLocation['expressDeliveryCharge'];
            }
            
        }
        
        $discountValue  =   0;
       if(!empty($this->CI->session->userdata['couponData'])) {
       /*if($this->CI->session->userdata['couponData']['type'] == 'Percentage'){
       $discountValue  =   ($this->getSubTotal()*$this->CI->session->userdata['couponData']['discount'])/100;
       }else{
       $discountValue  =   $this->CI->session->userdata['couponData']['discount'];
       }*/
            $couponID   =   $this->CI->session->userdata['couponData']['couponID'];
            $couponData =   $this->CI->gm->getTableValue('*','ec_coupon',array('couponID'=>$couponID));
            //echo "<pre>"; print_r($couponData); exit;
            if($this->getSubTotal()>=$couponData['minimum_amount_purchase']){
            if($couponData['applayFor']=='delivery'){
                if($couponData['type']=='Percentage'){
                    if($couponData['minimum_amount_purchase']!=0){
                        if($couponData['minimum_amount_purchase']<=$this->getSubTotal() && $this->getSubTotal()<=$couponData['maximum_amount_purchase']){
                       $discountValue  =   ($shippingCharge*$couponData['discount'])/100;
                        }else{
                            $discountValue  =   $couponData['max_discount'];
                        }
                    }else{
                        $discountValue  =   ($shippingCharge*$couponData['discount'])/100;
                    }
                }elseif($couponData['type']=='Fixed'){
                    if($couponData['minimum_amount_purchase']!=0){
                        if($couponData['minimum_amount_purchase']<=$this->getSubTotal() && $this->getSubTotal()<=$couponData['maximum_amount_purchase']){
                            $discountValue  =   $couponData['discount'];
                        }else{
                            $discountValue  =   $couponData['max_discount'];
                        }
                    }else{
                        $discountValue  =   $couponData['discount'];
                    }
                }
            }else{
               if($couponData['type']=='Percentage'){
                    if($couponData['minimum_amount_purchase']!=0){
                       
                        if($couponData['minimum_amount_purchase']<=$this->getSubTotal() && $this->getSubTotal()<=$couponData['maximum_amount_purchase']){
                        $discountValue  =   ($this->getSubTotal()*$couponData['discount'])/100;
                            //echo $couponData['discount'];
                        }else{
                            $discountValue  =   $couponData['max_discount'];
                        }
                    }else{
                        $discountValue  =   ($this->getSubTotal()*$couponData['discount'])/100;
                    }
                }elseif($couponData['type']=='Fixed'){
                    if($couponData['minimum_amount_purchase']!=0){
                        if($couponData['minimum_amount_purchase']<=$this->getSubTotal() && $this->getSubTotal()<=$couponData['maximum_amount_purchase']){
                        $discountValue  =   $couponData['discount'];
                            
                        }else{
                            $discountValue  =   $couponData['max_discount'];
                        }
                    }else{
                        $discountValue  =   $couponData['discount'];
                    }
                }
            }
            //echo $discountValue;
            }
            $totals[] = array(
                'code' => 'coupon_discount',
                'title' => $this->CI->lang->line('Coupon_Applied'),
                'value' => $discountValue,
                'sort_order' => 4,
                'languageKey' => 'Coupon_Applied'
            );
       }
        // 0 means shipping value if any changes we can update here
        $total = $this->getSubTotal() - $this->getProductDiscounts() + $shippingCharge -$discountValue;
        if($total < 0){
        $total =0;
        }else{
        $total=$total;
        }
        return $total;
    }

    public function getProductDiscounts() {
        $totalDiscount = 0;
         
        foreach ($this->getProducts() as $product) {
           
            $productOffer    = $this->CI->generalModel->getTableValue('*', 'ec_premotion_to_product', 'productID = '.$product['productID'].' AND priority=(select max(priority) from ec_premotion_to_product)  AND dateStart<=NOW() AND dateEnd>=NOW()', FALSE);
            //echo "<pre>"; print_r($productOffer); exit;
            if($productOffer){
                            $amount = ($product['price']*$productOffer['offePrice'])/100; 
                            $discountPrice = $amount*$product['quantity'];
                            $totalDiscount += $discountPrice;
            }else{
                    $productParam = array(
                    'productID' => $product['productID'],
                    'customerGroupID' => $this->CI->customer->getGroupId(),
                    'discountQuantity' => $product['quantity']
                );
                $product_discount = $this->CI->cartmodel->getProductDiscount($productParam);
                $discountPrice = 0;
                if($product_discount){
                    $orderQuantity = $this->CI->cartmodel->getProductOrderQuantity($product['productID'],$product_discount['dateStart'],$product_discount['dateEnd']);
                    //print_r($orderQuantity);exit;
                    if($product_discount['quantity']>($orderQuantity+$product['quantity'])){
                        if($product_discount['type']=='fixed'){
                            $discountPrice = $product_discount['price']*$product['quantity'];
                        }else if($product_discount['type']=='percentage' || $product_discount['type']==''){
                            $amount = ($product['price']*$product_discount['price'])/100;
                            $discountPrice = $amount*$product['quantity'];
                        }
                    }else if(($product_discount['quantity']-$orderQuantity) >0){
                        $discountQuantity = $product_discount['quantity']-$orderQuantity;
                        if($product_discount['type']=='fixed'){
                            $discountPrice = $product_discount['price']*$discountQuantity;
                        }else if($product_discount['type']=='percentage'){
                            $amount = ($product['price']*$product_discount['type'])/100;
                            $discountPrice = $amount*$discountQuantity;
                        }
                    }
                }
                $totalDiscount += $discountPrice;
            }
            
            
        }
        return $totalDiscount;
    }

    public function getTotalItemsList($languageID = 1,$cityID   =   "" , $deleveryType  =   'normal') {
        $shippingCharge =   0;
       //$shippingCharge  = $this->getSettingValue('shipping_charge',$languageID);
        $deliveryLocation = $this->CI->gm->getTableValue('*', 'ec_warehouse_delivery_location', 'cityID="' .$cityID . '"', FALSE);
        
//       echo "<pre>"; print_r($deliveryLocation); exit;
        if($deleveryType=='normal')
        {
        if(@$deliveryLocation['removeNorDelPrice']){
            if($this->getSubTotal() >= $deliveryLocation['removeNorDelPrice'] && $deliveryLocation['removeNorDelPrice']!=0)
            {
               $shippingCharge  =   0;
            }else{
                $shippingCharge =   $deliveryLocation['normalDeliveryCharge'];
            }
            }
        }else if($deleveryType=='express'){
             if($this->getSubTotal() >= $deliveryLocation['removeExpDelPrice'] && $deliveryLocation['removeExpDelPrice']!=0)
            {
               $shippingCharge  =   0;
            }else{
                $shippingCharge =   $deliveryLocation['expressDeliveryCharge'];
            }
            
        }
        
        $language = $this->CI->gm->getFieldValue('name','ec_language',array('languageID'=>$languageID));
        $this->CI->lang->load('cart', $language);
        $totals[] = array(
            'code' => 'sub_total',
            'title' => $this->CI->lang->line('Subtotal'),
            'value' => $this->getSubTotal(),
            'sort_order' => 1,
            'languageKey' => 'Subtotal'
        );
        if ($this->getProductDiscounts()) {
            $totals[] = array(
                'code' => 'discounts',
                'title' => $this->CI->lang->line('discounts'),
                'value' => $this->getProductDiscounts(),
                'sort_order' => 2,
                'languageKey' => 'discounts'
            );
        }
       
        if ($shippingCharge>0) {
            $totals[] = array(
                'code' => 'shipping_charge',
                'title' => $this->CI->lang->line('shipping_charge'),
                'value' => $shippingCharge,
                'sort_order' => 3,
                'languageKey' => 'shipping_charge'
            );
        }
        
        $discountValue =0;
        //echo "<pre>"; print_r($this->CI->session->userdata['couponData']); exit;
        if (!empty($this->CI->session->userdata['couponData'])) {
        //print_r($this->CI->session->all_userdata());
        /*if($this->CI->session->userdata['couponData']['type'] == 'Percentage'){
        $discountValue  =   ($this->getSubTotal()*$this->CI->session->userdata['couponData']['discount'])/100;
        }else{
        $discountValue  =   ($this->CI->session->userdata['couponData']['discount']);
        }*/
            
              $couponID   =   $this->CI->session->userdata['couponData']['couponID'];
            $couponData =   $this->CI->gm->getTableValue('*','ec_coupon',array('couponID'=>$couponID));
            //echo "<pre>"; print_r($couponData); exit;
            if($this->getSubTotal()>=$couponData['minimum_amount_purchase']){
            if($couponData['applayFor']=='delivery'){
                if($couponData['type']=='Percentage'){
                    if($couponData['minimum_amount_purchase']!=0){
                        if($couponData['minimum_amount_purchase']<=$this->getSubTotal() && $this->getSubTotal()<=$couponData['maximum_amount_purchase']){
                        $discountValue  =   ($shippingCharge*$couponData['discount'])/100;
                            
                        }else{
                            $discountValue  =   $couponData['max_discount'];
                        }
                    }else{
                        $discountValue  =   ($shippingCharge*$couponData['discount'])/100;
                    }
                }elseif($couponData['type']=='Fixed'){
                    if($couponData['minimum_amount_purchase']!=0){
                        if($couponData['minimum_amount_purchase']<=$this->getSubTotal() && $this->getSubTotal()<=$couponData['maximum_amount_purchase']){
                        $discountValue  =   $couponData['discount'];
                            
                        }else{
                            $discountValue  =   $couponData['max_discount'];
                        }
                    }else{
                        $discountValue  =   $couponData['discount'];
                    }
                }
            }else{
               if($couponData['type']=='Percentage'){ 
                    if($couponData['minimum_amount_purchase']!=0){
                       
                        if($couponData['minimum_amount_purchase']<=$this->getSubTotal() && $this->getSubTotal()<=$couponData['maximum_amount_purchase']){
                        $discountValue  =   ($this->getSubTotal()*$couponData['discount'])/100;
                            
                        }else{
                            $discountValue  =   $couponData['max_discount'];
                        }
                    }else{
                        $discountValue  =   ($this->getSubTotal()*$couponData['discount'])/100;
                    }
                }elseif($couponData['type']=='Fixed'){
                    if($couponData['minimum_amount_purchase']!=0){
                        if($couponData['minimum_amount_purchase']<=$this->getSubTotal() && $this->getSubTotal()<=$couponData['maximum_amount_purchase']){
                        $discountValue  =   $couponData['discount'];
                            
                        }else{
                            $discountValue  =   $couponData['max_discount'];
                        }
                    }else{
                        $discountValue  =   $couponData['discount'];
                    }
                }
            }
           }
           //echo $couponData['type'];
            
            $totals[] = array(
                'code' => 'coupon_discount',
                'title' => $this->CI->lang->line('Coupon_Applied'),
                'value' => $discountValue,
                'sort_order' => 4,
                'languageKey' => 'Coupon_Applied'
            );
        }
        
        $totals[] = array(
            'code' => 'total',
            'title' => $this->CI->lang->line('Total'),
            'value' => $this->getTotal($languageID,$cityID,$deleveryType),
            'sort_order' => 5,
            'languageKey' => 'Total'
        );
        
        return $totals;
    }

    public function countProducts() {
        $product_total = 0;

        $products = $this->getProducts();

        foreach ($products as $product) {
            $product_total += $product['quantity'];
        }

        return $product_total;
    }

    public function hasProducts() {
        return count($this->getProducts());
    }

    public function getProductQuantity($productID) {
        $customerID = $this->CI->customer->getId();
        $apiID = ($this->CI->session->userdata('api_id')) ? (int) $this->CI->session->userdata('api_id') : 0;
        $sessionID = $this->CI->session->session_id;
        $params = array(
            'customerID' => $customerID,
            'apiID' => $apiID,
            'sessionID' => $sessionID,
            'productID' => $productID
        );
        $value = $this->CI->generalModel->getFieldValue('quantity', 'ec_cart', $params);
        return ($value) ? (int) $value : 0;
    }

    public function isValidCart() {
        $flag = 0;
        $products = $this->getProducts();
        foreach ($products as $product) {
            $productInfo = $this->CI->generalModel->getTableValue('quantity,minimum,status,dateAvailable', 'ec_product', array("productID" => $product['productID'], 'status' => 'Active'));
            if ($productInfo['status'] != 'Active') {
                $flag = 1;
            }
            if ($productInfo['dateAvailable'] != '0000-00-00' && $productInfo['dateAvailable'] > date('Y-m-d')) {
                $flag = 1;
            }
            if ($productInfo['quantity'] < $this->getProductQuantity($product['productID'])) {
                $flag = 1;
            }
        }
        if ($flag == 1) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    protected function getSettingValue($settingKey = '',$languageID=1) {
        if ($settingKey != '') {
            $settingDetails = $this->CI->generalModel->getTableValue('*', 'ec_settings', array('fieldKey' => $settingKey));
            if ($settingDetails) {
                if ($settingDetails['isLanguage'] == 'yes') {
                    $value = $this->CI->generalModel->getFieldValue('value', 'ec_settings_field_details', array('settingsFieldID' => $settingDetails['id'], 'languageID' => $languageID));
                    return $value;
                } else {
                    return $settingDetails['value'];
                }
            }
        }
        return;
    }

}

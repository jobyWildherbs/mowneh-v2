<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Currency {

    protected $CI;
    private $currencies = array();

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('GeneralModel', 'gm');
        $currencyList = $this->CI->gm->getTableValue('*', 'ec_currency', array('status' => 'enable'), TRUE);
        foreach ($currencyList as $result) {
            $this->currencies[$result['code']] = array(
                'currency_id' => $result['currencyID'],
                'title' => $result['title'],
                'symbol_left' => $result['symbolLeft'],
                'symbol_right' => $result['symbolRight'],
                'decimal_place' => $result['decimalPlace'],
                'value' => $result['value']
            );
        }
    }

    public function format($number, $currency, $value = '', $format = true,$mode=PHP_ROUND_HALF_UP) {
        
        $symbol_left = $this->currencies[$currency]['symbol_left'];
        $symbol_right = $this->currencies[$currency]['symbol_right'];
        $decimal_place = $this->currencies[$currency]['decimal_place'];

        if (!$value) {
            $value = $this->currencies[$currency]['value'];
        }

        $amount = $value ? (float) $number * $value : (float) $number;

        $amount = round($amount, (int) $decimal_place,$mode);
        //echo "<pre>"; print_r($amount);
        if (!$format) {
            return $amount;
        }

        $string = '';

        if ($symbol_left) {
            $string .= $symbol_left;
        }

        $string .= number_format($amount, (int) $decimal_place, '.', ',');

        if ($symbol_right) {
            $string .= $symbol_right;
        }

        return $string;
    }
    
    public function formatAPI($number, $currency, $value = '', $format = true) {
        $symbol_left = $this->currencies[$currency]['symbol_left'];
        $symbol_right = $this->currencies[$currency]['symbol_right'];
        $decimal_place = $this->currencies[$currency]['decimal_place'];

        if (!$value) {
            $value = $this->currencies[$currency]['value'];
        }

        $amount = $value ? (float) $number * $value : (float) $number;

        $amount = round($amount, (int) $decimal_place);

        if (!$format) {
            return $amount;
        }

        $string = '';

//        if ($symbol_left) {
//            $string .= $symbol_left;
//        }

        $string .= number_format($amount, (int) $decimal_place, '.', ',');

//        if ($symbol_right) {
//            $string .= $symbol_right;
//        }

        return $string;
    }

    public function convert($value, $from, $to) {
        if (isset($this->currencies[$from])) {
            $from = $this->currencies[$from]['value'];
        } else {
            $from = 1;
        }

        if (isset($this->currencies[$to])) {
            $to = $this->currencies[$to]['value'];
        } else {
            $to = 1;
        }

        return $value * ($to / $from);
    }

    public function getId($currency) {
        if (isset($this->currencies[$currency])) {
            return $this->currencies[$currency]['currency_id'];
        } else {
            return 0;
        }
    }

    public function getSymbolLeft($currency) {
        if (isset($this->currencies[$currency])) {
            return $this->currencies[$currency]['symbol_left'];
        } else {
            return '';
        }
    }

    public function getSymbolRight($currency) {
        if (isset($this->currencies[$currency])) {
            return $this->currencies[$currency]['symbol_right'];
        } else {
            return '';
        }
    }

    public function getDecimalPlace($currency) {
        if (isset($this->currencies[$currency])) {
            return $this->currencies[$currency]['decimal_place'];
        } else {
            return 0;
        }
    }

    public function getValue($currency) {
        if (isset($this->currencies[$currency])) {
            return $this->currencies[$currency]['value'];
        } else {
            return 0;
        }
    }

    public function has($currency) {
        return isset($this->currencies[$currency]);
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerLogin {

    protected $CI;

    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('CustomerModel', 'customerModel');
        $this->CI->load->model('GeneralModel', 'generalModel');
        $this->CI->load->helper('common_helper'); 
    }

    public function createCredentials($email, $password) {

        // new password & salt create
        if ($email != '' && $password != '') {

            if ($this->CI->customerModel->checkUserEmail($email)) {
                $random_string = getRandomString(15);
                $salt = hash('sha256', sha1($random_string . SITE_SALT));
                $hashed_password = hash('sha256', sha1($salt . SITE_SALT . $password));
                $result = array("salt" => $salt, "password" => $hashed_password);
                return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getCredentials($email, $password) {
        // new password & salt create
        if ($email != '' && $password != '') {

            if (!$this->CI->customerModel->checkUserEmail($email)) {

                $random_string = getRandomString(15);
                $salt = hash('sha256', sha1($random_string . SITE_SALT));
                $hashed_password = hash('sha256', sha1($salt . SITE_SALT . $password));
                $result = array("salt" => $salt, "password" => $hashed_password);
                return $result;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function checkCredentials($email, $password, $languageID=1) {
        $this->CI->lang->load('site', $languageID);
        $flag = 1;
        $apiStatus  =   '';
        $message = '';
        if ($email != '' && $password != '') {
            // user name is existing or not
            if (!$this->CI->customerModel->checkUserEmail($email)) {

                $salt = $this->CI->generalModel->getTableValue('*', 'ec_customer', array('email' => $email,'status!='=>'Deleted'));
                //echo "<pre>"; print_r($salt); exit;
                //echo $this->CI->db->last_query();exit;

                $hashed_password = hash('sha256', sha1($salt['salt'] . SITE_SALT . $password));
               
                // login attempt more than 5 time check
                $loginAttempt = $this->CI->generalModel->getTableValue('count(attemptID) as attempt', 'ec_login_attempts', array('login' => $email));

                if ($salt['status'] == 'Inactive') {
                    $flag = 0;
                    $message = lang('Your_account_is_inactive_now');
                }
                else if ($salt['status'] == 'Notverified') {
                    $flag = 0;
                    $apiStatus  =   1;
                    $message = lang('Your_account_is_not_activated');
                }
                else if ($loginAttempt['attempt'] == 5) {
                    $flag = 0;
                    $message = lang('Login_Attempt_Over');
                } else {
//                           echo $hashed_password; echo "<br>";
//                           echo $salt['password'];exit;
                    // password matcing
                    if ($hashed_password == $salt['password']) {
                        $flag = 1;
                        $message = lang('Login_Sucussfully');
                    } else {
                        $flag = 0;
                        $message = lang('Invalid_User_Name_Password'); 
                        $ip_address = $this->CI->input->ip_address();
                        $loginAttempts = array(
                            'ipAddress' => $ip_address,
                            'login' => $email,
                            'dateAdded' => date('Y-m-d h:i:s')
                        );
                        $this->CI->generalModel->insertValue('ec_login_attempts', $loginAttempts);
                        // login attemot table insert
                    }
                }
            } else {
                $flag = 0;
                $message =  lang('Invalid_User');
            }
        } else {
            $flag = 0;
            $message = lang('Invalid_Values');
        }
        $result = array('status' => $flag, 'message' => $message, 'apiStatus' => $apiStatus);
        return $result;
    }
    public function checkPhCredentials($telephone, $languageID=1) {
        $this->CI->lang->load('site', $languageID);
        $flag = 1;
        $apiStatus  =   '';
        $message = ''; $customerID = '';
        if ($telephone != '') {
            // user name is existing or not
            if (!$this->CI->customerModel->checkUserphone($telephone)) {

                $salt = $this->CI->generalModel->getTableValue('*', 'ec_customer', array('telephone' => $telephone,'status!='=>'Deleted'));
                //echo "<pre>"; print_r($salt); exit;
                //echo $this->CI->db->last_query();exit;

                //$hashed_password = hash('sha256', sha1($salt['salt'] . SITE_SALT . $password));
               
                // login attempt more than 5 time check
                $customerID =  $salt['customerID'];
                //$loginAttempt = $this->CI->generalModel->getTableValue('count(attemptID) as attempt', 'ec_login_attempts', array('login' => $email));

                if ($salt['status'] == 'Inactive') {
                    $flag = 0;
                    $message = lang('Your_account_is_inactive_now');
                }
                else if ($salt['status'] == 'Notverified') {
                    $flag = 0;
                    $apiStatus  =   1;
                    $message = lang('Your_account_is_not_activated');
                }
                
//                           echo $hashed_password; echo "<br>";
//                           echo $salt['password'];exit;
                    // password matcing
                    if ($telephone == $salt['telephone']) {
                        $flag = 1;
                        $message = lang('Login_Sucussfully');
                    } else {
                        $flag = 0;
                        $message = "Invalid Email or Phone No"; 
                        $ip_address = $this->CI->input->ip_address();
                        $loginAttempts = array(
                            'ipAddress' => $ip_address,
                            'login' => $email,
                            'dateAdded' => date('Y-m-d h:i:s')
                        );
                        $this->CI->generalModel->insertValue('ec_login_attempts', $loginAttempts);
                        // login attemot table insert
                    }
                
            } else {
                $flag = 0;
                $customerID='';
                $message =  "Invalid Phone No";
            }
        } else {
            $flag = 0;
            $message = lang('Invalid_Values');
        }
        $result = array('status' => $flag, 'message' => $message, 'apiStatus' => $apiStatus, 'customerID' => $customerID);
        return $result;
    }
    
    public function resetCredentials($password) {

        // new password & salt create
        if ($password != '') {
            $random_string = getRandomString(15);
            $salt = hash('sha256', sha1($random_string . SITE_SALT));
            $hashed_password = hash('sha256', sha1($salt . SITE_SALT . $password));
            $result = array("salt" => $salt, "password" => $hashed_password);
            return $result;
        } else {
            return false;
        }
    }
    
     public function checkOldPassword($email, $password) {
     
        $flag = 0;
        $message = '';
        if ($email != '' && $password != '') {
            
            // user name is existing or not
            if (!$this->CI->customerModel->checkUserEmail($email)) {
                $salt = $this->CI->generalModel->getTableValue('*', 'ec_customer', array('email' => $email));

                $hashed_password = hash('sha256', sha1($salt['salt'] . SITE_SALT . $password));
                    if ($hashed_password == $salt['password']) {
                      
                        $flag = 1;
                    }
            } else {
                $flag = 0;
            }
        } else {
            $flag = 0;
        }
        $result = array('status' => $flag);
        return $result;
    }
    
   

}

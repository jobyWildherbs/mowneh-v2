<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReportSupport {

    protected $CI;
    public function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->database();
    }
    
    public function filterList($page=""){
       
       
        $filter =   array();
        if($page=='product'){
            $brand      =   $this->brandFilter(1); 
            
            $filter[0]['label']         =   'From Date';
            $filter[0]['type']          =   'text';
            $filter[0]['name']          =   'fromDate';
            $filter[0]['field']         =   'date';
            $filter[0]['option']        =   '';
            $filter[0]['multiple']      =   '';
            $filter[0]['value']         =   '';

            $filter[1]['label']         =   'TO Date';
            $filter[1]['type']          =   'text';
            $filter[1]['name']          =   'toDate';
            $filter[1]['field']         =   'date';
            $filter[1]['option']        =   '';
            $filter[1]['multiple']      =   '';
            $filter[1]['value']         =   '';

            $filter[2]['label']         =   'Most ordered';
            $filter[2]['type']          =   'radio';
            $filter[2]['name']          =   'ordered';
            $filter[2]['field']         =   '';
            $filter[2]['option']        =   '';
            $filter[2]['multiple']      =   '';
            $filter[2]['value']         =   'mostOrder';

            $filter[3]['label']         =   'Less ordered';
            $filter[3]['type']          =   'radio';
            $filter[3]['name']          =   'ordered';
            $filter[3]['field']         =   '';
            $filter[3]['option']        =   '';
            $filter[3]['multiple']      =   '';
            $filter[3]['value']         =   'lessOrder';

            $filter[4]['label']         =   'Out of stock';
            $filter[4]['type']          =   'checkbox';
            $filter[4]['name']          =   'outofstock';
            $filter[4]['field']         =   '';
            $filter[4]['option']        =   '';
            $filter[4]['multiple']      =   '';
            $filter[4]['value']         =   'yes';

            $filter[5]['label']         =   'Brand';
            $filter[5]['type']          =   'select';
            $filter[5]['name']          =   'manufacturerID';
            $filter[5]['field']         =   '';
            $filter[5]['option']        =   $brand;
            $filter[5]['multiple']      =   '';
            $filter[5]['value']         =   '';
        }
        elseif($page=='customer'){
            $filter[0]['label']         =   'From Date';
            $filter[0]['type']          =   'text';
            $filter[0]['name']          =   'fromDate';
            $filter[0]['field']         =   'date';
            $filter[0]['option']        =   '';
            $filter[0]['multiple']      =   '';
            $filter[0]['value']         =   '';

            $filter[1]['label']         =   'TO Date';
            $filter[1]['type']          =   'text';
            $filter[1]['name']          =   'toDate';
            $filter[1]['field']         =   'date';
            $filter[1]['option']        =   '';
            $filter[1]['multiple']      =   '';
            $filter[1]['value']         =   '';
            
            $filter[2]['label']         =   'Most ordered';
            $filter[2]['type']          =   'radio';
            $filter[2]['name']          =   'ordered';
            $filter[2]['field']         =   '';
            $filter[2]['option']        =   '';
            $filter[2]['multiple']      =   '';
            $filter[2]['value']         =   'mostOrder';

            $filter[3]['label']         =   'Less ordered';
            $filter[3]['type']          =   'radio';
            $filter[3]['name']          =   'ordered';
            $filter[3]['field']         =   '';
            $filter[3]['option']        =   '';
            $filter[3]['multiple']      =   '';
            $filter[3]['value']         =   'lessOrder';
            
            $filter[4]['label']         =   'No of times purchased';
            $filter[4]['type']          =   'checkbox';
            $filter[4]['name']          =   'purchaseCount';
            $filter[4]['field']         =   '';
            $filter[4]['option']        =   '';
            $filter[4]['multiple']      =   '';
            $filter[4]['value']         =   'yes';
            
            $filter[5]['label']         =   'Total amount purchased';
            $filter[5]['type']          =   'checkbox';
            $filter[5]['name']          =   'amountPurchased';
            $filter[5]['field']         =   '';
            $filter[5]['option']        =   '';
            $filter[5]['multiple']      =   '';
            $filter[5]['value']         =   'yes';
            
            $filter[6]['label']         =   'Average customer stand';
            $filter[6]['type']          =   'checkbox';
            $filter[6]['name']          =   'averageCustomerStand';
            $filter[6]['field']         =   '';
            $filter[6]['option']        =   '';
            $filter[6]['multiple']      =   '';
            $filter[6]['value']         =   'yes';
        }elseif($page=='order'){
            $orderStatus      =   $this->orderStatus(1);
             
            $filter[0]['label']         =   'From Date';
            $filter[0]['type']          =   'text';
            $filter[0]['name']          =   'fromDate';
            $filter[0]['field']         =   'date';
            $filter[0]['option']        =   '';
            $filter[0]['multiple']      =   '';
            $filter[0]['value']         =   '';

            $filter[1]['label']         =   'TO Date';
            $filter[1]['type']          =   'text';
            $filter[1]['name']          =   'toDate';
            $filter[1]['field']         =   'date';
            $filter[1]['option']        =   '';
            $filter[1]['multiple']      =   '';
            $filter[1]['value']         =   '';
            
            $filter[5]['label']         =   'Order Status';
            $filter[5]['type']          =   'select';
            $filter[5]['name']          =   'orderStatusID';
            $filter[5]['field']         =   '';
            $filter[5]['option']        =   $orderStatus;
            $filter[5]['multiple']      =   '';
            $filter[5]['value']         =   '';
        }
        
        return $filter;
    }
    
    public function filterInput($exportPage){
        $input  =   array();
        $filterArray=$this->filterList($exportPage);
        foreach($filterArray as $key=>$arrVal){
            $name   =   $arrVal['name'];
            $type   =   $arrVal['type'];
            $value  =  $arrVal['value'];
            
            $multiple   =   '';
            if($arrVal['multiple']!='')
                $multiple   =   '[]';
            
            $class  =   '';
             if($arrVal['field']=='date')
                $class   =   'datePicker';
            
            $name   =   $arrVal['name'].$multiple;
            if($arrVal['type']=='text'){
              $input[$key]['field']  =  '<input type="'.$type.'" name="'.$name.'" class="form-control '.$class.'" value="'.$value.'">';
              $input[$key]['label']  =  $arrVal['label'];
            }elseif($arrVal['type']=='checkbox'){
              $input[$key]['field']  =  '<input type="'.$type.'" name="'.$name.'" class="'.$class.'" value="'.$value.'">';
              $input[$key]['label']  =  $arrVal['label'];
            }elseif($arrVal['type']=='radio'){
              $input[$key]['field']  =  '<input type="'.$type.'" name="'.$name.'" class="'.$class.'" value="'.$value.'">';
              $input[$key]['label']  =  $arrVal['label'];
            }elseif($arrVal['type']=='select'){
//                echo "<pre>"; print_r($arrVal);exit;
                $option =   "<option value=''>Select</option>";
                foreach($arrVal['option'] as $optionVal){
                    if($optionVal['id']!='' && $optionVal['name']!=''){
                        $option .=   "<option value='".$optionVal['id']."'>".$optionVal['name']."</option>";
                    }
                }
                
              $input[$key]['field']  =  '<select name="'.$name.'" class="form-control '.$class.'">'.$option.'</select>';
              $input[$key]['label']  =  $arrVal['label'];
            }
           
        }
        return $input;
    }
    
    public function brandFilter($languageID = 1){
        $this->CI->load->model('ManufacturesModel', 'manufacturesModel');
        $brand  =   $this->CI->manufacturesModel->selectReportFilter($languageID);
        $data   =   array();
        foreach($brand as $key=>$value){
            $data[$key]['id']       =   $value['manufacturerID'];
            $data[$key]['name']     =   $value['name'];
        }
        return $data;
    }
    
    public function orderStatus($languageID = 1){
        $this->CI->load->model('OrderModel', 'orderModel');
        $orderStatus  =   $this->CI->orderModel->selectStatusFilter($languageID);
       
        $data   =   array();
        foreach($orderStatus as $key=>$value){
            $data[$key]['id']       =   $value['orderStatusID'];
            $data[$key]['name']     =   $value['name'];
        }
        return $data;
    }

}
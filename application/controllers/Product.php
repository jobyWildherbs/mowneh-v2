<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Site_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ProductModel', 'productModel');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('CategoryModel', 'categoryModel');
        $this->lang->load('site', $this->siteLanguage);
        $this->customerID = $this->customer->getID();
    }

    public function index($categoryKey = "") {
    
        $categoryID = 0;
        $childStatus = false;
        if ($categoryKey != "") {
            $categoryID = $this->generalModel->getFieldValue('categoryID', 'ec_category', array('pageKey' => $categoryKey , 'status' => 'Active'));
            $subcategories = $this->generalModel->getTableValue('*', 'ec_category', array('parentID' => $categoryID, 'status' => 'Active'), TRUE);
            if ($subcategories) {
                $childStatus = TRUE;
            }
        }
        $this->data['startPrice'] = '';
        $this->data['endPrice'] = '';
        $this->data['redirectUrl'] = base_url('product/') . $categoryKey;
        $filters = $this->input->get('filter');
        $brand = $this->input->get('manufacturerID');
        $priceRange = $this->input->get('price');
        $order = $this->input->get('order');
        $this->data['manufacturerList'] = $this->productModel->selectManufacturerList($this->siteLanguageID);
        $this->data['categoryList'] = $this->productModel->selectCategoryListInProductList($this->siteLanguageID, $categoryID);
        $filter = $this->productModel->selectFilterList($this->siteLanguageID,$categoryID);
        $filterArr = array();
        foreach ($filter as $key => $filVal) {
            $filterArr[$filVal['groupName']][$key]['filterName'] = $filVal['filterName'];
            $filterArr[$filVal['groupName']][$key]['filterID'] = $filVal['filterID'];
        }
        $this->data['filterArry'] = $filterArr;
        $this->data['selectedFilter'] = $filters;
        $this->data['selectedFilterArray'] = explode("~", $filters);
        $this->data['selectedManufacturerArry'] = explode("~", $brand);
        $this->data['selectedPriceArry'] = explode("~", $priceRange);
        $this->data['order'] = $order;
        if ($categoryID != 0) {
            $categortDetail = $this->categoryModel->selectCategory($categoryID, $this->siteLanguageID);
           // echo "<pre>"; print_r($categortDetail); exit;
            $categortDetail['description'] = substr(strip_tags($categortDetail['description']), 0, 200);

            list($width, $height) = $this->getImageDimension('banner_category_dimen');
            $categortDetail['banner'] = $this->setSiteImage("category/banner/" . $categortDetail['banner'], $width, $height);
            $this->data['categortDetail'] = $categortDetail;
        }
        $data = array(
            'customerID' => $this->customer->getId(),
            'languageID' => $this->siteLanguageID,
        );
        if ($categoryID) {
            $data['categoryID'] = $categoryID;
        }
        if ($filters) {
            $data['filter'] = $filters;
        }
        if ($brand) {
            $data['brand'] = $brand;
        }
        if ($order) {
            $data['order'] = $order;
        }
        if ($childStatus) {
            $data['subCategory'] = $childStatus;
        }
        if ($priceRange) {
            $priceVal = explode("~", $priceRange);
            $data['startPrice'] = $priceVal[0];
            $data['endPrice'] = $priceVal[1];
            $this->data['startPrice'] = $priceVal[0];
            $this->data['endPrice'] = $priceVal[1];
            $prices = $this->data['priceVal'];
            if ($priceVal[0] == 0 && $priceVal[1] == 500) {
                $width = 100;
                $left = 0;
            } else {
                $start = array_search($priceVal[0], $prices);
                $end = array_search($priceVal[1], $prices);
                $count = ($end - $start) + 1;
                $width = $count * 100 / count($prices);
                $left = (100 / count($prices)) * $start;
            }
            $this->data['rangeWidth'] = $width;
            $this->data['rangeLeft'] = $left;
        }
        
        //pagination starts
        $this->load->library("pagination");
        $productCount    =   $this->productModel->getProducts($data,'Yes');
//        echo "<pre>"; print_r($productCount); exit;
        $config = $this->paginationConfig;
        $config['reuse_query_string'] = true;
        $config['per_page'] = $this->getSettingValue('product_listing_limit');;
        //$config['total_rows'] = $productCount['productCount']; //data count for pagination
        $config['total_rows'] =  count($productCount); //data count for pagination
        $this->data["totalProduct"]    =    $config['total_rows'];
        $categoryKeyString  =   "";
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0; 
        if(!is_numeric($categoryKey)){
            $categoryKeyString    =   "/".$categoryKey;
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        }
       
        $config['base_url'] = base_url() . 'Product'.$categoryKeyString;

        $this->pagination->initialize($config);
        //echo $this->pagination->create_links(); exit;
        $this->data["links"] = $this->pagination->create_links();
       
        $this->data['currentPage'] = $page + 1;
        $data['limit'] = $this->getSettingValue('product_listing_limit');
        $data['start'] = $page;
         //pagination Ends
        
        $productList = $this->productModel->getProducts($data);
        list($width, $height) = $this->getImageDimension('list_product_dimen');
        foreach ($productList as $key => $productData) {


            
            $productID  =  $productData['productID']; 
            $productOffer    = $this->generalModel->getTableValue('*', 'ec_premotion_to_product', 'productID = '.$productID.' AND priority=(select max(priority) from ec_premotion_to_product)  AND dateStart<=NOW() AND dateEnd>=NOW()', FALSE);
            //print_r($this->db->last_query()); exit;
            //echo "<pre>"; print_r($productOffer); echo "<pre>"; echo "1";
            if($productOffer){
                 $productList[$key]['productDiscount'] = $productOffer;
                 $productList[$key]['discountPrice'] = $this->currency->format($productOffer['offePrice'], $this->siteCurrency,'',TRUE,PHP_ROUND_HALF_DOWN);
                 $productList[$key]['offPersentage'] = $productOffer['offer'];
            } else {
                $productDiscount = $this->productModel->productDiscount($productData['productID']);
                $discountPrice = "";
                    if (!empty($productDiscount)) {
                        if ($productDiscount['type'] == 'fixed') {
                            $discountPrice = $productData['price'] - $productDiscount['price'];
                            $offPersentage = ($productDiscount['price'] / $productData['price']) * 100;
                            $productList[$key]['offPersentage'] = $offPersentage;
                        } else {
                            $offPersentage = $productDiscount['price'];
                            $productList[$key]['offPersentage'] = $offPersentage;
                            $discountPrice = $productData['price'] - ($productData['price'] * $productDiscount['price'] / 100);
                        }
                    }
                    if ($discountPrice != '')
                        $productList[$key]['discountPrice'] = $this->currency->format($discountPrice, $this->siteCurrency,'',TRUE,PHP_ROUND_HALF_DOWN);
                    
                    $productList[$key]['productDiscount'] = $productDiscount;
                
            }
           
            
            
            $productList[$key]['price'] = $this->currency->format($productData['price'], $this->siteCurrency);
            $productList[$key]['image'] = $this->setSiteImage("product/" . $productData['image'], $width, $height);
            //exit;
           


            $now = time(); // or your date as well
            $dateAdded = strtotime($productData['dateAdded']);
            $datediff = $now - $dateAdded;
            if (round($datediff / (60 * 60 * 24)) < 7) {
                $productList[$key]['new'] = 'new';
            }
        } 
        $this->data['sidebarCategory'] = $this->productModel->getCategories(0, $this->siteLanguageID);
        if ($categoryKey != '') {
            $categoryLevel = $this->generalModel->getTableValue('*', 'ec_category_path', array('categoryID' => $categoryID), TRUE);

            /* foreach ($categories as $category) {
              $children_data = array();
              $children = $this->productModel->getCategories($category['categoryID'], $this->siteLanguageID);
              foreach ($children as $child) {
              $children_data[] = array(
              'name' => $child['categoryName'],
              'href' => ''
              );
              }
              $data['categories'][] = array(
              'name' => $category['categoryName'],
              'children' => $children_data,
              'href' => ''
              );
              } */
        } else {
            
        }
        //echo "<pre>";print_r($this->data['sidebarCategory']);exit;
        $this->data['productList'] = $productList;
       
        $this->render('frontEnd/product/ProductList');
    }

    public function addtowishlist() {
        //echo "<pre>"; print_r($_POST); exit;

        if ($this->customer->loggedIn() != true) {
            $status['info'] = 'login';
            //$status['msg'] = 'Removed from Wishlist';
        } else {
            $productID = $this->input->post('productID');
            $customerID = $this->customer->getID();
            //echo "<pre>"; print_r($customerID);exit;
            $value['productID'] = $productID;
            $value['customerID'] = $customerID;
            $listItem = $this->generalModel->getTableValue('*', 'ec_customer_wishlist', array('customerID' => $customerID, 'productID' => $productID), FALSE);
            if ($listItem) {

                //OR DELETE FROM TALE
                $this->generalModel->deleteTableValues('ec_customer_wishlist', array('customerID' => $customerID, 'productID' => $productID), FALSE);
                $status['info'] = 'removed';
                $status['msg'] = 'Removed from Wishlist';
            } else {
                $addWishList = $this->generalModel->insertValue('ec_customer_wishlist', $value);
                $status['info'] = 'inserted';
                $status['msg'] = 'Added to Wishlist';
            }
        }
        echo json_encode($status);
        //$wishList  =   array();
        // $this->render('manage/frontEnd/ajaxWhishList');
    }

    public function detail() {
        $productKey = $this->uri->segment(3);
        $bundleProducts =   array();
        $productID = $this->generalModel->getFieldValue('productID', 'ec_product', 'pageKey="' . $productKey . '" AND status="Active"', TRUE); //for getting product id by key
//        echo $productID; exit;
        //$productID = $this->uri->segment(3);
        $productDetail = $this->productModel->selectProductDetail($productID, $this->siteLanguageID);
//        echo "<pre>"; print_r($productDetail); exit;
        $productOptionPrice=0;
        if($productDetail['type']=='Product'){
            $productOption = $this->productModel->selectProductOptions($productID, $this->siteLanguageID);
             if(@$productOption){
                $productOptionDetail = $this->productModel->productOptionDetail($productID, $this->siteLanguageID);
                $productOptionQuantity = $this->productModel->productOptionQuantity($productID, $this->siteLanguageID);
                $productOptiondetailById = $this->productModel->getProductOptionValueByProducID($productOptionDetail[0]['productOptionID']);
                $optionValue=array();
                foreach($productOptiondetailById as $option){
                    $optionValue[]=$option['optionValueID'];
                }
               
                $this->data['productOption']    = $productOption;
                $this->data['productOptionDetail']    = $productOptionDetail;
                $this->data['productOptionQuantity']    = $productOptionQuantity;
                $this->data['productOptionDetailById']    = $optionValue;
                $this->data['productOptionId']    = $productOptionDetail[0]['productOptionID'];
                
                foreach($productOptionQuantity as $option){
                    if($option['productOptionID'] == $productOptionDetail[0]['productOptionID']){
                        $productOptionPrice = $option['price'];
                    }
                }
                
                $this->data['optionPrice'] = $this->currency->format($productOptionPrice, $this->siteCurrency);
            }
        }
        //echo "<pre>"; print_r($productOptionPrice); exit;
        $additionalImage = $this->generalModel->getTableValue('productImageID,image', 'ec_product_image', 'productID=' . $productID, TRUE); //data count for pagination
        list($mainWidth, $mainHeight) = $this->getImageDimension('product_popup_dimen');
        list($smallWidth, $smallHeight) = $this->getImageDimension('product_page_thumb_dimen');
        $mainImage = $productDetail['image'];
        $productDetail['image'] = array(
            'popup' => $this->setSiteImage("product/" . $mainImage, $mainWidth, $mainHeight),
            'thumb' => $this->setSiteImage("product/" . $mainImage, $smallWidth, $smallHeight)
        );
        //$productDetail['additionalImage'] = $additionalImage;
        $images = array();
        foreach ($additionalImage as $imageVal) { //print_r($imageVal['image']);
            $images[] = array(
                'popup' => $this->setSiteImage("product/" . $imageVal['image'], $mainWidth, $mainHeight),
                'thumb' => $this->setSiteImage("product/" . $imageVal['image'], $smallWidth, $smallHeight)
            );
        }
        $productDetail['additionalImage'] = $images;
        $productAttr = $this->productModel->getProductAttributeByProductID($productID, $this->siteLanguageID);

        $productAttribute = array();

        foreach ($productAttr as $key => $data) {
            $productAttribute[$data['groupName']][$key]['attribute'] = $data['name'];
            $productAttribute[$data['groupName']][$key]['value'] = $data['text'];
        }

        //echo "<pre>";print_r($productAttribute);exit;
        list($width, $height) = $this->getImageDimension('list_product_dimen');
        if($productDetail['type']=='Bundle'){
            $bundleProducts =   array();
            $bundleProduct    = $this->generalModel->getTableValue('*', 'ec_product_bundle', 'productID = '.$productDetail['productID'], TRUE);
           // echo "<pre>";print_r($bundleProduct);exit;
            if($bundleProduct){
                foreach($bundleProduct as $bundleData){ 
                    $bundleProductData    =   $this->productModel->getBundleProductss($bundleData['bundleProductID'],$this->siteLanguageID);
                    if($bundleProductData){
                        if(@$bundleProductData['image']){
                            $bundleProductData['image'] =  $this->setSiteImage("product/" . $bundleProductData['image'], $smallWidth, $smallHeight);
                        }
                    
                        $bundleProducts[]   =   $bundleProductData;
                    }
                    //print_r($bundleProductData);
                    
                  
                }
            }
            
        } 
         //echo "<pre>";print_r($bundleProduct);exit;
        $relatedProduct = $this->productModel->reletedProductList($productID, $this->siteLanguageID);
        
        
         
        foreach ($relatedProduct as $key => $reletadData) {
           
            $productOffer    = $this->generalModel->getTableValue('*', 'ec_premotion_to_product', 'productID = '.$reletadData['productID'].' AND priority=(select max(priority) from ec_premotion_to_product)', FALSE);
            if($productOffer){
                $relatedProduct[$key]['productDiscount'] = $productOffer;
                $relatedProduct[$key]['discountPrice'] = $this->currency->format($productOffer['offePrice'], $this->siteCurrency,'',TRUE,PHP_ROUND_HALF_DOWN);
                $relatedProduct[$key]['offPersentage']  = $productOffer['offer'];
            }else{
                $productDiscount = $this->productModel->productDiscount($reletadData['productID']);
                $discountPrice = "";
                if (!empty($productDiscount)) {
                    if ($productDiscount['type'] == 'fixed') {
                        $discountPrice = $reletadData['price'] - $productDiscount['price'];
                        $offPersentage = ($productDiscount['price'] / $reletadData['price']) * 100;
                        $relatedProduct[$key]['offPersentage'] = $offPersentage;
                    } else {
                        $offPersentage = $productDiscount['price'];
                        $relatedProduct[$key]['offPersentage'] = $offPersentage;
                        $discountPrice = $reletadData['price'] - ($reletadData['price'] * $productDiscount['price'] / 100);
                    }
                }
                if ($discountPrice != '')
                    $relatedProduct[$key]['discountPrice'] = $this->currency->format($discountPrice, $this->siteCurrency,'',TRUE,PHP_ROUND_HALF_DOWN);
                $relatedProduct[$key]['productDiscount'] = $productDiscount;
            }
            
            
            $relatedProduct[$key]['price'] = $this->currency->format($reletadData['price'], $this->siteCurrency);
            $relatedProduct[$key]['image'] = $this->setSiteImage("product/" . $reletadData['image'], $width, $height);
            //exit;
            
            
            $relatedProduct[$key]['image'] = $this->setSiteImage("product/" . $reletadData['image'], $width, $height);
        } 
        
        $productOffer    = $this->generalModel->getTableValue('*', 'ec_premotion_to_product', 'productID = '.$productID.' AND priority=(select max(priority) from ec_premotion_to_product)', FALSE);
        //echo "<pre>"; print_r($productOffer); exit;
        if($productOffer){
            if($productOptionPrice != 0){
                $discount = ($productOptionPrice*$productOffer['offer'])/100; 
                $discountPrice = $productOptionPrice-$discount;
                       // $totalDiscount += $discountPrice;
                $productDetail['productDiscount'] = $productOffer;
                $productDetail['discountPrice'] = $this->currency->format($discountPrice, $this->siteCurrency,'',TRUE,PHP_ROUND_HALF_DOWN);
                $this->data['productDiscount']  = $productOffer;
                $productDetail['offPersentage'] = $productOffer['offer'];
            }else{
                $productDetail['productDiscount'] = $productOffer;
                $productDetail['discountPrice'] = $this->currency->format($productOffer['offePrice'], $this->siteCurrency,'',TRUE,PHP_ROUND_HALF_DOWN);
                $this->data['productDiscount']  = $productOffer;
                $productDetail['offPersentage'] = $productOffer['offer'];
            }
            $productDetail['productDiscountQty'] = '';
            $this->data['productDiscountQty']  = '';
                
        }else{
            $productDiscount = $this->productModel->productDiscount($productID);
            $productDiscountOrederPrice = $this->productModel->productDiscountOrderQuantity($productID);
            $discountPrice = "";
            if (!empty($productDiscount)) {
                if ($productDiscount['type'] == 'fixed') {
                    if($productOptionPrice != 0){
                        $discountPrice = $productOptionPrice - $productDiscount['price'];
                        $offPersentage = ($productDiscount['price'] / $productOptionPrice) * 100;
                        $productDetail['offPersentage'] = $offPersentage;
                    }else{
                        $discountPrice = $productDetail['price'] - $productDiscount['price'];
                        $offPersentage = ($productDiscount['price'] / $productDetail['price']) * 100;
                        $productDetail['offPersentage'] = $offPersentage;
                    }
                    
                } else {
                    if($productOptionPrice != 0){
                        $offPersentage = $productDiscount['price'];
                        $productDetail['offPersentage'] = $offPersentage;
                        $discountPrice = $productOptionPrice - ($productOptionPrice * $productDiscount['price'] / 100);
                    }else{
                        $offPersentage = $productDiscount['price'];
                        $productDetail['offPersentage'] = $offPersentage;
                        $discountPrice = $productDetail['price'] - ($productDetail['price'] * $productDiscount['price'] / 100);
                    }
                }
                //$discountPrice = $productDetail['price'] - $productDiscount['price'];
            }

            if ($discountPrice != '') {
                $productDetail['discountPrice'] = $this->currency->format($discountPrice, $this->siteCurrency,'',TRUE,PHP_ROUND_HALF_DOWN);
            }
            $productDetail['productDiscountQty'] = $productDiscount['quantity'];
            $this->data['productDiscount']  = $productDiscount;
             $this->data['productDiscountQty']  = $productDiscount['quantity'];
             if (!empty($productDiscountOrederPrice)) {
                $this->data['availableDiscount']  = $productDiscount['quantity'] - $productDiscountOrederPrice['orderQuantity'];
            }else{
                $this->data['availableDiscount']  = $productDiscount['quantity'];
            }
        }
       
        
        $productDetail['price'] = $this->currency->format($productDetail['price'], $this->siteCurrency);
       //echo"<pre>";print_r($productDetail//);exit;
       
        $this->data['productDetail']    = $productDetail;
        $this->data['productAttribute'] = $productAttribute;
        $this->data['relatedProduct']   = $relatedProduct;
        $this->data['bundleProducts']   = $bundleProducts;
        $this->data['productDetail']   = $productDetail;
        
        $customerID = $this->session->userdata('moonehcustomerID');
        
        $this->data['action'] = base_url('product/detail');
        if ($this->session->flashdata('success') != '') {
            $this->data['success'] = $this->session->flashdata('success');
        }
        if ($this->session->flashdata('failed') != '') {
            $this->data['failed'] = $this->session->flashdata('failed');
        }
        if($customerID!= ''){
            $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_customer', 'customerID=' . "$customerID", FALSE); //data for edit
       
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
        $this->form_validation->set_rules('telephone', 'Phone Number', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('comments', 'comments', 'required');

        if ($this->form_validation->run() === TRUE) {
            //echo "<pre>"; print_r($_FILES);exit;
            $errVal = 0;

            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'phone' => $this->input->post('telephone'),
                    //'status' => $this->input->post('status')
                'email'=>$this->input->post('email'),
                'phone'=>$this->input->post('telephone'),
                'customerID'=> $customerID,
                'productID'=>$productID,
                'valid_customer'=> 'Yes'
                
            );
          if ($customerID !='') {
            $resData['nonXss']['status'] = 'Inactive';
            }
           
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_review_prod',  $resData['xssData']); // insert detail data
            if($result){
           $reviewDetail['nonXss']=array(
               'reviewID'=>  $result,
               'languageID' => $this->siteLanguageID,
                'comments' => $this->input->post('comments'),
           );
              $reviewDetail['xssData'] = $this->security->xss_clean($reviewDetail['nonXss']);
            $this->generalModel->insertValue('ec_review_prod_detail',  $reviewDetail['xssData']); // insert detail data
            }
        }
//        else{
//            echo "print";
//            exit();
//        }

        }else{
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
        $this->form_validation->set_rules('telephone', 'Phone Number', 'required|min_length[8]|max_length[8]');
        $this->form_validation->set_rules('email', 'Email', 'required|is_unique[ec_review_prod.email]');
        $this->form_validation->set_rules('comments', 'comments', 'required');

        if ($this->form_validation->run() === TRUE) {
            //echo "<pre>"; print_r($_FILES);exit;
            $errVal = 0;

            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'phone' => $this->input->post('telephone'),
                    //'status' => $this->input->post('status')
                'email'=>$this->input->post('email'),
                'phone'=>$this->input->post('telephone'),
                //'customerID'=> $customerID,
                'productID'=>$productID,
                'valid_customer'=> 'No'
            );
           if ($customerID =='') {
            $resData['nonXss']['status'] = 'Inactive';
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_review_prod',  $resData['xssData']); // insert detail data
            if($result){
           $reviewDetail['nonXss']=array(
               'reviewID'=>  $result,
               'languageID' => $this->siteLanguageID,
                'comments' => $this->input->post('comments'),
           );
              $reviewDetail['xssData'] = $this->security->xss_clean($reviewDetail['nonXss']);
            $this->generalModel->insertValue('ec_review_prod_detail',  $reviewDetail['xssData']); // insert detail data
            }
         
        }
        if($productDetail){
            $this->data['title']    =   $productDetail['metaTitle'];
             $this->data['metaTitle']    =   $productDetail['metaTitle'];
            $this->data['metaDescription']    =   $productDetail['metaDescription'];
            $this->data['metaKeyword']    =   $productDetail['metaKeyword'];
        }

//        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_review_prod', 'customerID=' . "$customerID", FALSE); //data for edit
        }
        $this->render('frontEnd/product/productDetail');
    }

    public function _remap($method) {
        $param_offset = 2;

        if (!method_exists($this, $method)) {
            $param_offset = 1;
            $method = 'index';
        }
// Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
// Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function filterProduct() {
        $this->render('frontEnd/product/filter');
    }

    public function priceRange() {
        $json = array();
        $prices = $this->data['priceVal'];
        $min = ($this->input->post('minPrice')) ? $this->input->post('minPrice') : 0;
        $max = ($this->input->post('maxPrice')) ? $this->input->post('maxPrice') : 500;

        if ($min == 0 && $max == 500) {
            $json['width'] = 100;
            $json['left'] = 0;
        } else {
            $start = array_search($min, $prices);
            $end = array_search($max, $prices);
            $count = ($end - $start) + 1;
            $width = $count * 100 / count($prices);
            $left = (100 / count($prices)) * $start;
            $json['width'] = $width;
            $json['left'] = $left;
        }
        echo json_encode($json);
    }
    
    public function categories($parent=''){
        if($parent){
            
            $categoryID =   $this->gm->getFieldValue('categoryID','ec_category','pageKey="'.$parent.'"');
            $this->data['categoryTree'] =   $this->gm->getChildMenuTree($this->siteLanguageID,$categoryID);
        }
       $this->render('frontEnd/product/categories');
        
    }
    public function checkProductAvailability()
    {
        $optionValuId=array();
        $optionValue = $this->input->post('option');
        foreach($optionValue as $option){
            $optionValuId[]=$option['optionValueId'];
            
             //$valueOption = explode(",",$optionValuId);
             
        }
        $valueOption = implode(",",$optionValuId);
        
        $result = $this->productModel->getProductCombination( $this->input->post('productID'));
        $valueOptions = explode(",",$result->optionID);
        //print_r($valueOptions);
             foreach($valueOptions as $option){
                $results = $this->productModel->getProductCombinationValue( $this->input->post('productID'),$option,$valueOption);
            }
       
        
    }
    public function getProductOption(){
    $result = $this->productModel->getProductOptionVal( $this->input->post('productID'),$this->input->post('optionVal'),$this->siteLanguageID);
    
    echo json_encode(array_unique($result, SORT_REGULAR));
    }
    public function getProductOptionPrice(){
    $result = $this->productModel->getProductOptionByID( $this->input->post('productOptionId'));
    echo json_encode($this->currency->format($result['price'], $this->siteCurrency));
    }
    public function checkQunatity(){ 
    if(@$this->input->post('option')){
    $optionval = $this->input->post('option');  $price=array(); $optionArray = array();
    //$cartItems = $this->CI->cartmodel->getCartOptionItems($params);
            foreach($optionval as $option){ 
                $optionArray[]= $option['productOptionID'];
                        
                
                
            }
            $qunatity=0;
            foreach(array_unique($optionArray) as $options){ 
                $resultDiscount = $this->productModel->checkDiscountQunatity($this->input->post('product'));
        
    $result = $this->productModel->checkQunatity($options);
    
                if(@$resultDiscount){
                 
                }
                    if(@$result)
                    $qunatity = $result->quantity;
                    else
                    $qunatity =0;
                
                
            }
            //exit;
    }else{
        //echo $this->input->post('productOptionId');
        $resultDiscount = $this->productModel->checkDiscountQunatity($this->input->post('product'));
        $qunatity =0;
    //$result = $this->productModel->checkQunatity($options);
                if(@$resultDiscount){
                 
                }
                
    }
    echo json_encode($qunatity);
    }
    

}

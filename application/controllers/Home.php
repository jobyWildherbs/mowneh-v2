<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Site_Controller {

    public function __construct() {
        parent::__construct();
        //$this->load->library('CustomerLogin');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('PromotionModel', 'promotionModel');
        $this->load->model('BannerModel', 'bannerModel');
        $this->load->model('WidgetModel', 'widgetModel');
        $this->load->model('ProductModel', 'productModel');
         $this->load->model('AddBannerModel', 'addBannerModel');
        $this->load->library('form_validation');
        $this->lang->load('site', $this->siteLanguage);
        $this->customerID = $this->customer->getID();
    }

    public function index() {
        $sliderList = $this->bannerModel->selectBanner($this->siteLanguageID);
        list($width, $height) = $this->getImageDimension('home_banner_slider_dimen');
        foreach ($sliderList as $key => $sliderData) {

            $sliderList[$key]['image'] = $this->setSiteImage("banner/" . $sliderData['image'], $width, $height);
        }
        $this->data['sliderList'] = $sliderList;
        

        $promotionList = $this->promotionModel->selectPromotion($this->siteLanguageID);
        list($width, $height) = $this->getImageDimension('promotion_banner_dimen');
        foreach ($promotionList as $key => $promotionData) {
            $promotionList[$key]['image'] = $this->setSiteImage("promotion/" . $promotionData['image'], $width, $height);
        }
        $this->data['promotionList'] = $promotionList;
        $addBanner = $this->addBannerModel->selectForHomePaage($this->siteLanguageID);
        $this->data['addBanner'] = $addBanner;
       

        $this->data['aboutus'] = $this->getSettingValue('site_description');

        $widget = $this->widgetModel->widgetHomeShow($this->siteLanguageID, $this->customerID);
       //echo"<pre>"; print_r($widget);exit;
        if (!empty($widget)) {
            foreach ($widget as $key => $widgetData) {
                $dimen = $widgetData['imageDimension'];
                $explodeDimen = explode("x", $dimen);
                if (is_array($explodeDimen) && count($explodeDimen) == 2) {
                    $promWidth = $explodeDimen[0];
                    $promHeight = $explodeDimen[1];
                } else {
                    $promWidth = "";
                    $promHeight = "";
                }
                if ($widgetData['typeKey'] != 'category') {
                    //echo "<pre>"; print_r($widgetData['widgetProduct']); exit;
                    foreach ($widgetData['widgetProduct'] as $innerKey => $productData) {
                        $productOffer    = $this->generalModel->getTableValue('*', 'ec_premotion_to_product', 'productID = '.$productData['productID'].' AND priority=(select max(priority) from ec_premotion_to_product)  AND dateStart<=NOW() AND dateEnd>=NOW()', FALSE);
                        //print_r($this->db->last_query()); exit;
                        //echo "<pre>"; print_r($productOffer); echo "<pre>"; echo "1";
                        if($productOffer){ 
                             $productDiscount= $productOffer;
                             $discountPrice = $productOffer['offePrice'];

                        } 
                        else{
                        $productDiscount = $this->productModel->productDiscount($productData['productID']);
                        $discountPrice = "";
                        if (!empty($productDiscount)) {
                            if ($productDiscount['type'] == 'fixed') {
                                $discountPrice = $productData['price'] - $productDiscount['price'];
                                $offPersentage = ($productDiscount['price'] / $productData['price']) * 100;
                                $widget[$key]['widgetProduct'][$innerKey]['offPersentage'] = $offPersentage;
                            } else {
                                $offPersentage = $productDiscount['price'];
                                $widget[$key]['widgetProduct'][$innerKey]['offPersentage'] = $offPersentage;
                                $discountPrice = $productData['price'] - ($productData['price'] * $productDiscount['price'] / 100);
                            }
                        }
                        
                    }
                        if ($discountPrice != '')
                            $widget[$key]['widgetProduct'][$innerKey]['discountPrice'] = $this->currency->format($discountPrice, $this->siteCurrency);

                        $now = time(); // or your date as well
                        $dateAdded = strtotime($productData['dateAdded']);
                        $datediff = $now - $dateAdded;
                        if (round($datediff / (60 * 60 * 24)) < 7) {
                            $widget[$key]['widgetProduct'][$innerKey]['new'] = 'new';
                        }
                        $widget[$key]['widgetProduct'][$innerKey]['price'] = $this->currency->format($productData['price'], $this->siteCurrency);
                        //echo "<pre>";print_r($promHeight);exit;
                        $widget[$key]['widgetProduct'][$innerKey]['image'] = $this->setSiteImage("product/" . $productData['image'], $promWidth, $promHeight);
                        $widget[$key]['widgetProduct'][$innerKey]['productDiscount'] = $productDiscount;
                        
                    }
                } elseif ($widgetData['typeKey'] == 'category') {
                    foreach ($widgetData['widgetCategory'] as $innerKey => $productData) { 
                        $widget[$key]['widgetCategory'][$innerKey]['image'] = $this->setSiteImage("category/" . $productData['image'], $promWidth, $promHeight);
                    }

                    // echo"<pre>"; print_r($widget[$key]['widgetProduct'][$innerKey]['image']);exit;
                }
            }
        }
        //echo "<pre>"; print_r($widget);exit;
        $this->data['widget'] = $widget;
//            $this->data['metaTitle']    =   "metaTitle";
//            $this->data['metaDescription']    =   "metaDescription";
//            $this->data['metaKeyword']    =   "metaKeyword";
        $this->render('frontEnd/home/Home');
    }

    public function addtowishlist() {
        //echo "<pre>"; print_r($_POST); exit;

        if ($this->customer->loggedIn() != true) {
            $status['info'] = 'login';
            //$this->session->set_userdata('moonehCallBackURL', '');
            //$status['msg'] = 'Removed from Wishlist';
        } else {
            $productID = $this->input->post('productID');
            $customerID = $this->customer->getID();

            //echo "<pre>"; print_r($customerID);exit;
            $value['productID'] = $productID;
            $value['customerID'] = $customerID;
            $listItem = $this->generalModel->getTableValue('*', 'ec_customer_wishlist', array('customerID' => $customerID, 'productID' => $productID), FALSE);
            if ($listItem) {

                //OR DELETE FROM TALE
                $this->generalModel->deleteTableValues('ec_customer_wishlist', array('customerID' => $customerID, 'productID' => $productID), FALSE);
                $status['info'] = 'removed';
                $status['msg'] = 'Removed from Wishlist';
            } else {
                $addWishList = $this->generalModel->insertValue('ec_customer_wishlist', $value);
                $status['info'] = 'inserted';
                $status['msg'] = 'Added to Wishlist';
            }
        }
        echo json_encode($status);
        //$wishList  =   array();
        // $this->render('manage/frontEnd/ajaxWhishList');
    }

    function checkDeliveryStatus() {
        $json = array();
        $this->lang->load('error', $this->siteLanguage);
        $cityID = ($this->input->post('city')) ? $this->input->post('city') : 0;
        if ($cityID > 0) {
            if ($cityID == 11 || $cityID == 38 || $cityID == 40) {
                $json['success']['city'] = $this->lang->line('product_city_delivery_success');
            } else {
                $json['error']['delivery'] = $this->lang->line('product_city_delivery_error');
            }
        } else {
            $json['error']['city'] = $this->lang->line('product_city_select_error');
        }
        echo json_encode($json);
    }

    public function add_new() {
        if ($this->input->post()) {

            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|is_unique[ec_newsletter.email]', array(
                'is_unique' => 'Account already exists with this email. Please Use Another Email',
                'valid_email' => 'Please provide a valid email address',
                'xss_clean' => 'Please provide a valid email address'
            ));
            if ($this->form_validation->run() === TRUE) { 
                $resData['nonXss'] = array(
                    'email' => $this->input->post('email'),
                    'dateAdded' => date('Y-m-d H:i')
                );

                $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
                $insert = $this->generalModel->insertValue('ec_newsletter', $resData['xssData']);
                echo(json_encode($insert));
            }
        }
    }



 public function comingSoon(){
        $this->load->view('Authentication');
    }

    public function sendSMS123(){
        $user = "";
        $password = "";
        $senderid = ""; //Your senderid
        $url = "https://messaging.ooredoo.qa/bms/soap/Messenger.asmx/HTTP_SendSms";
        //$mobilenumbers = $customerPhone;
        //$message = urlencode($message);
        $ch = curl_init();
        if (!$ch) {
            die("Couldn't initialize a cURL handle");
        }
        $ret = curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "customerID=4319&userName=mownehqat&userPassword=Mtc@2020&originator=mowneh&smsText=Mowheh Registration Successfully completed pease check your registred email to activate your account&recipientPhone=917293473206&messageType=Latin&defDate=&blink=false&flash=false&Private=false");
        $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //If you are behind proxy then please uncomment below line and provide your proxy ip with port. //
        //$ret = curl_setopt($ch, CURLOPT_PROXY, "PROXY IP ADDRESS:PORT");
        $curlresponse = curl_exec($ch); // execute
        if (curl_errno($ch))
            echo 'curl error : ' . curl_error($ch);
        if (empty($ret)) {
            // some kind of an error happened
            die(curl_error($ch));
            curl_close($ch); // close cURL handler
        } else {
            $info = curl_getinfo($ch);
            curl_close($ch); // close cURL handler
            //echo $curlresponse; //echo "Message Sent Succesfully" ;
        }
    }
    
     public function sendSMSSecond(){
        $message    =   "Mowheh Registration Successfully completed pease check your registred email to activate your account";
         $this->sendSMS($message,'919074674360');
         exit;
        $url    =   "https://messaging.ooredoo.qa/bms/soap/Messenger.asmx/HTTP_SendSms?customerID=4319&userName=mownehqat&userPassword=Mtc@2020&originator=mowneh&smsText=Mowheh Registration Successfully completed pease check your registred email to activate your account&recipientPhone=917293473206&messageType=Latin&defDate=&blink=false&flash=false&Private=false";
      
        // must set $url first....
        $http = curl_init($url);
        // do your curl thing here
        $result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);
        echo $http_status;
    }
    
    
    public function sendEmaiTest(){
        $this->load->library('email'); // Note: no $config param needed
        $this->email->from('jobyjamespg@gmail.com', 'Mowneh');
        $this->email->to('jobyjames103@gmail.com');
        $this->email->subject('Test email from CI and Gmail');
        $this->email->message('This is a test.');
        if ($this->email->send()) {
            echo 'Your Email has successfully been sent.';
        } else {
            show_error($this->email->print_debugger());
        }
    }
    
}

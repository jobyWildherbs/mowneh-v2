<?php defined('BASEPATH') OR exit('No direct script access allowed');

class OptionCombination extends Admin_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('OptionModel', 'optionModel');
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;

        $imageDimension = $this->getImageDimension('option_image');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
        $this->setDirectory(FCPATH . "uploads/options/");

        $this->imageConfig = array(
            'upload_path' => FCPATH . "uploads/options/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );

    }
    public function index() {
        if ($this->input->post('action')) {
            $id = implode(",", $this->input->post('multyCheck'));
            $status['status'] = $this->input->post('action');
            $this->generalModel->updateTableValues('ec_option', 'optionID IN (' . $id . ')', $status);
        }
        $this->data['breadcrumbs'] = "All Options Combination";
        $this->data['pageTitle'] = "All Options Combination";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(combinationID) as tblCount', 'ec_option_combination', '', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/OptionCombination/index';
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'name' => $this->input->get('name'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language);
        
        $combination = $this->optionModel->selectOptionCombination($params);
        if($combination){
            foreach($combination as $key=>$combinationVal){
                $combinationOptionID    = explode(",",$combinationVal['combinationOptionID']);
                $optionName =   $this->optionModel->selectOptionCombinationName($combinationOptionID,$this->language);
                $combination[$key]['optionName']    =   $optionName['optionName'];
            }
        }
        //echo "<pre>"; print_r($combination); exit;
        $this->data['result'] = $combination;
        $this->render('manage/optionCombination/List');
    }
    /*     * *********************************************************************
     * JUST AS FILTERS     
     * ********************************************************************* */
    public function add() {
        $this->data['breadcrumbs'] = "Add Options Combination";
        $this->data['pageTitle'] = "Add Options Combination";
        $this->form_validation->set_rules('optionGroup[]', 'Option Name', 'required|xss_clean'); 
        $this->form_validation->set_rules('sortOrder', 'Sort Order', 'required|numeric|xss_clean');
        if ($this->form_validation->run() === TRUE) {
            $optionID   =   implode(",",$this->input->post('optionGroup[]'));
            
            $option['nonXss'] = array(
                'optionID'     => $optionID,
                'sortOrder'     => $this->input->post('sortOrder'),
            );
            $option['xssData']  = $this->security->xss_clean($option['nonXss']);
            $result = $this->generalModel->insertValue('ec_option_combination', $option['xssData']); //Group Added, $result will have the new ID
            if ($result) {
                $this->logUserActivity('Option Combination Added');
                $this->data['message'] = "Data added successfully";
            } else {
                $this->data['errMessage'] = "Failed";
            }
        }
        $this->data['optionsList']   =   $this->optionModel->selectAllActiveOption($this->language);
        //echo "<pre>"; print_r($this->data['optionsList']); exit;
        $this->render('manage/optionCombination/Add');
    }
    public function edit($combinationID) {
        $this->data['breadcrumbs'] = "Add Options Combination";
        $this->data['pageTitle'] = "Add Options Combination";
        $this->form_validation->set_rules('optionGroup[]', 'Option Name', 'required|xss_clean');
        if ($this->form_validation->run() === TRUE) {
            $optionID   =   implode(",",$this->input->post('optionGroup[]'));
            
            $option['nonXss'] = array(
                'optionID'     => $optionID,
                'sortOrder'     => $this->input->post('sortOrder'),
            );
            $option['xssData'] = $this->security->xss_clean($option['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_option_combination', 'combinationID = ' . $combinationID, $option['xssData']);
            if ($result) {
                $this->logUserActivity('Option Combination Updated');
                $this->data['message'] = "Data Updated";
            } else {
                $this->data['errMessage'] = "Failed";
            }
            }
            
        
        $optionCombination = $this->generalModel->getTableValue('*', 'ec_option_combination', 'combinationID=' . "$combinationID" . ' AND status!="Deleted"', FALSE); //data for edit
        if($optionCombination){
            $options    =  explode(",",$optionCombination['optionID']);
            $optionCombination['optionID']  =   $options;
        }
       
        //echo "<pre>"; print_r($optionCombination); exit;
        $this->data['content'] =   $optionCombination;
       $this->data['optionsList']   =   $this->optionModel->selectAllActiveOption($this->language);
        $this->render('manage/optionCombination/Add');
    }
    
    public function changeStatus() {

        $combinationID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $result = $this->generalModel->updateTableValues('ec_option_combination', 'combinationID=' . $combinationID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }
   
}

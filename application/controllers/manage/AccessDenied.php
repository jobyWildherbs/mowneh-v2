<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AccessDenied extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }
    public function index() {
        echo "you have no access to view this page .....";
    }
}

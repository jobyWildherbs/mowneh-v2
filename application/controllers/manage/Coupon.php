<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon extends Admin_Controller {

    private $imageConfig = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('CouponModel', 'couponModel');
//        $imageDimension = $this->getImageDimension('home_coupon_slider_dimen');
//        $imageSet = implode(' x ', $imageDimension);
//        $this->data['imageDimension'] = $imageDimension;
//        $this->data['imageSet'] = $imageSet;
//        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
//        $this->data['allowedType'] = $this->allowedImageTypes;
//        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
//        $this->imageConfig = array(
//            'upload_path' => "./uploads/coupon/",
//            'allowed_types' => $this->allowedImageTypes,
//            'max_size' => $this->maxImageUploadSize,
//            'max_width' => $imageDimension[0],
//            'max_height' => $imageDimension[1],
//            'min_width' => $imageDimension[0],
//            'min_height' => $imageDimension[1],
//            'encrypt_name' => TRUE,
//        );
    }

    public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_coupon', 'couponID IN (' . $id . ')', $status);
            }
        }
        $this->data['breadcrumbs'] = "All Coupon Page";
        $this->data['pageTitle'] = "All Coupon Page";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(couponID) as tblCount', 'ec_coupon', 'couponID!=1 AND status!="Deleted"', FALSE); //data count for pagination
        
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Coupon/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'name' => $this->input->get('name'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );

        $this->data['result'] = $this->couponModel->selectAll($params);
        if(@$_GET['msg']){
            if($_GET['msg'] == 'active'){
             $this->data['success'] = "Coupon activated Successfully";
            }else if($_GET['msg'] == 'inactive'){
             $this->data['success'] = "Coupon inactivated Successfully";
            }else if($_GET['msg'] == 'delete'){
             $this->data['success'] = "Coupon deleted Successfully";
            }
        }
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/coupon/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Coupon";
        $this->data['pageTitle'] = "Add Coupon";

        //form validation  is_unique[ec_coupon.code]
        $this->form_validation->set_rules('name', 'Coupon Name', 'required|xss_clean');
     	 $this->form_validation->set_rules('code', 'Coupon Code', 'required|min_length[5]|max_length[8]|callback_checkCouponUnique|strtoupper', array(
                'required' => 'Please Provide Coupon Code',
                'checkCouponUnique' => 'This code is already exist'
            ));
        $this->form_validation->set_rules('type', 'Coupon Type', 'required|xss_clean');
         $this->form_validation->set_rules('applayFor', 'Apply For', 'required|xss_clean');
         $this->form_validation->set_rules('discount', 'Discount', 'required|xss_clean');
         $this->form_validation->set_rules('dateStart', 'Date Start', 'required|xss_clean');
         $this->form_validation->set_rules('dateEnd', 'Date End', 'required|xss_clean');
            if($this->input->post('type') == 'Percentage'){
                $this->form_validation->set_rules('discount', 'discount', 'greater_than_equal_to[0]|less_than_equal_to[100]');
            }
            
            if($this->input->post('type') == 'Fixed'){
                $this->form_validation->set_rules('discount', 'Discount', 'less_than_equal_to['.$this->input->post('minimum_amount_purchase').']',array('less_than_equal_to' => 'The Discount field must contain a number less than or equal to '.$this->input->post('minimum_amount_purchase').' (Minimum Amount Purchase).'));
            }
	 $this->form_validation->set_rules('minimum_amount_purchase', 'minimum amount', 'greater_than_equal_to[0]');
     //$this->form_validation->set_rules('maximum_amount_purchase', 'maximum amount', 'greater_than_equal_to[0]');
	 $this->form_validation->set_rules('max_discount', 'maximum discount', 'greater_than_equal_to[0]');
        if ($this->form_validation->run() === TRUE) {
        if(@$this->input->post('utype')){
        $foruser=$this->input->post('utype');
        }
        else{
        $foruser="";
        }
//            $errVal = 0;
//            $_POST['image'] = '';
//            if ($_FILES['image']['size'] > 0) {
//                $this->load->library('upload', $this->imageConfig);
//                if ($this->upload->do_upload('image')) {
//                    $_POST['image'] = $this->upload->data('file_name');
//                    // in case you need to save it into a database
//                } else {
//                    $this->data['imgErr'] = $this->upload->display_errors();
//                    $errVal = 1;
//                    $_POST['image'] = '';
//                }
//            } else {
//                $this->data['imgErr'] = "No Image File Uploaded";
//                $errVal = 1;
//                $_POST['image'] = '';
//            }
//
//            $this->session->set_flashdata('imgErr', $this->data['imgErr']);
//            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'type' => $this->input->post('type'),
                'applayFor' => $this->input->post('applayFor'),
                'applayForUser' => $foruser,
                'discount' => $this->input->post('discount'),
                'comment' => $this->input->post('comment'),
                'minimum_amount_purchase' => $this->input->post('minimum_amount_purchase'),
                'maximum_amount_purchase' => $this->input->post('maximum_amount_purchase'),
                'max_discount' => $this->input->post('max_discount'),
                'total' => $this->input->post('total'),
                //'logged' => $this->input->post('logged'),
                //'shipping' => $this->input->post('shipping'),
                'dateStart' => $this->input->post('dateStart'),
                'dateEnd' => $this->input->post('dateEnd'),
                'usesTotal' => $this->input->post('usesTotal'),
                'usesCustomer' => $this->input->post('usesCustomer'),
                'status' => $this->input->post('status')
            );
             if($this->input->post('logged')=='on')
                $resData['nonXss']['logged']  =   1;
            else
                $resData['nonXss']['logged']  =   0;
             if($this->input->post('shipping')=='on')
                $resData['nonXss']['shipping']  =   1;
            else
                $resData['nonXss']['shipping']  =   0;
           
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_coupon', $resData['xssData']);

            if ($result) {
                
                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Coupon Added');
//                if ($errVal == 0) {
                    redirect('manage/coupon/index');
//                } else {
//                    redirect('manage/coupon/edit/' . $result . '/imgErr');
//                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/coupon/index');
            }
        }
        $this->render('manage/coupon/add');
    }

    public function edit() {
        //$this->data = array();
        
        $this->data['breadcrumbs'] = "Edit Coupon";
        $this->data['pageTitle'] = "Edit Coupon";

        $couponID = $this->uri->segment(4);
//        $imgErr = $this->uri->segment(5);
//        if ($imgErr) {
//            $this->data['imgErr'] = "Invalid Image, Please try again";
//            $this->data['errVal'] = 1;
//        }
//        if ($this->session->flashdata('imgErr') != '') {
//            $this->data['imgErr'] = $this->session->flashdata('imgErr');
//            $this->data['errVal'] = 1;
//        }
          $code = $this->input->post('code');
	    $original_value = $this->generalModel->getTableValue('*', 'ec_coupon', 'code="' . "$code" . '"', FALSE);
	    //print_r($original_value['code']);
	    //exit;
	    $is_unique =  '';
	    if(@$original_value){
	    if( $couponID != $original_value['couponID']) {
	       $is_unique =  'is_unique[ec_coupon.code]';
	    
	    } else {
	       $is_unique =  '';
	    }
	    }
        //form validation  '.$is_unique.'|
       $this->form_validation->set_rules('name', 'Coupon Name', 'required|xss_clean');
       $this->form_validation->set_rules('code', 'Coupon Code', 'required|min_length[5]|max_length[8]|callback_checkCouponUnique|strtoupper', array(
                'required' => 'Please Provide Coupon Code',
                'checkCouponUnique' => 'This code is already exist'
            ));
         $this->form_validation->set_rules('type', 'Coupon Type', 'required|xss_clean');
         $this->form_validation->set_rules('applayFor', 'Apply For', 'required|xss_clean');
         $this->form_validation->set_rules('discount', 'Discount', 'required|xss_clean');
         $this->form_validation->set_rules('dateStart', 'Date Start', 'required|xss_clean');
         $this->form_validation->set_rules('dateEnd', 'Date End', 'required|xss_clean');
         if($this->input->post('type') == 'Percentage'){
            $this->form_validation->set_rules('discount', 'discount', 'greater_than_equal_to[0]|less_than_equal_to[100]');
            }
            if($this->input->post('type') == 'Fixed'){
                $this->form_validation->set_rules('discount', 'Discount', 'less_than_equal_to['.$this->input->post('minimum_amount_purchase').']',array('less_than_equal_to' => 'The Discount field must contain a number less than or equal to '.$this->input->post('minimum_amount_purchase').' (Minimum Amount Purchase).'));
            }
	 $this->form_validation->set_rules('minimum_amount_purchase', 'minimum amount', 'greater_than_equal_to[0]');
	 //$this->form_validation->set_rules('maximum_amount_purchase', 'maximum amount', 'greater_than_equal_to[0]');
	 $this->form_validation->set_rules('max_discount', 'maximum discount', 'greater_than_equal_to[0]');
        //image upload
        if ($this->form_validation->run() === TRUE) {
        if(@$this->input->post('utype')){
        $foruser=$this->input->post('utype');
        }
        else{
        $foruser="";
        }
//            $errVal = 0;
//            if ($_FILES['image']['size'] > 0) {
//
//                $this->load->library('upload', $this->imageConfig);
//                if ($this->upload->do_upload('image')) {
//                    $_POST['image'] = $this->upload->data('file_name');
//                    // in case you need to save it into a database
//                } else {
//                    $this->data['imgErr'] = $this->upload->display_errors();
//                    //$_POST['image'] = '';
//                    $errVal = 1;
//                }
//            }
//
//            $this->data['errVal'] = $errVal;

            $resData['nonXss'] = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'type' => $this->input->post('type'),
                'applayFor' => $this->input->post('applayFor'),
                'applayForUser' => $foruser,
                'discount' => $this->input->post('discount'),
                'minimum_amount_purchase' => $this->input->post('minimum_amount_purchase'),
                'maximum_amount_purchase' => $this->input->post('maximum_amount_purchase'),
                'max_discount' => $this->input->post('max_discount'),
                'total' => $this->input->post('total'),
                'dateStart' => $this->input->post('dateStart'),
                'comment' => $this->input->post('comment'),
                //'logged' => $this->input->post('logged'),
                //'shipping' => $this->input->post('shipping'),
                'dateEnd' => $this->input->post('dateEnd'),
                'usesTotal' => $this->input->post('usesTotal'),
                'usesCustomer' => $this->input->post('usesCustomer'),
                'status' => $this->input->post('status')
            );
            
             if($this->input->post('logged')=='on')
                $resData['nonXss']['logged']  =   1;
            else
                $resData['nonXss']['logged']  =   0;
            if($this->input->post('shipping')=='on')
                $resData['nonXss']['shipping']  =   1;
            else
                $resData['nonXss']['shipping']  =   0;
            //if ($errVal == 1) {
            //$resData['nonXss']['status'] = 'Inactive';
            //}
//            if ($this->input->post('image')) {
//                $resData['nonXss']['image'] = $this->input->post('image');
//            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_coupon', 'couponID=' . $couponID, $resData['xssData']);
//            $couponDetail['nonXss'] = array(
//                'couponID' => $couponID,
//                'languageID' => $this->language,
//                'title' => $this->input->post('title'),
//                'linkOnButton' => $this->input->post('linkOnButton'),
//                'description' => $this->input->post('description')
//            );
//
//            $couponDetail['xssData'] = $this->security->xss_clean($couponDetail['nonXss']);
//            $checkCouponDetail = $this->generalModel->getTableValue('couponID', 'ec_coupon_detail', 'couponID=' . $couponID . ' AND languageID=' . $this->language, FALSE);
//            if ($checkCouponDetail) {
//                $this->generalModel->updateTableValues('ec_coupon_detail', 'couponID=' . $couponID . ' AND languageID=' . $this->language, $couponDetail['xssData']); //information Added
//            } else {
//                $this->generalModel->insertValue('ec_coupon_detail', $couponDetail['xssData']); // insert detail data
//            }
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Coupon Updated');
//                if ($errVal == 0) {
                    redirect('manage/coupon/index');
//                } else {
//                    $this->session->set_flashdata('imgErr', $this->data['imgErr']);
//                    redirect('manage/coupon/edit/' . $couponID);
//                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/coupon/index');
            }
        }

        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_coupon', 'couponID=' . "$couponID" . ' AND status!="Deleted"', FALSE); //data for edit
        

        $this->render('manage/coupon/add');
    }
    
    public function checkCouponUnique(){
        $code   =   $this->input->post('code');
        $couponID = $this->uri->segment(4);
        $idPresent  =   "";
        if($couponID){
            $idPresent  =   'couponID!="' . $couponID . '" AND ';
        }
        $couponreturn   =   $this->generalModel->getTableValue('*', 'ec_coupon', $idPresent.'code="'.$code.'" AND status!="Deleted"', FALSE);
       // echo $this->db->last_query();exit;
        if($couponreturn) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function changeStatus() {

        $couponID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_coupon', 'couponID=' . $couponID, $this->data['data']);
        if($status == 'Active'){
            redirect('manage/Coupon?msg=active');
        }else if($status =='Inactive'){
            redirect('manage/Coupon?msg=inactive');
        }else if($status == 'Deleted'){
            redirect('manage/Coupon?msg=delete');
        }else{
            redirect('manage/Coupon');
        }
        //redirect($_SERVER['HTTP_REFERER']);
    }
    public function getActiveUser() {

       $result = $this->generalModel->getTableValue('email', 'ec_customer', 'status!="Deleted"', TRUE); 
       $email = array();
       foreach($result as $res){
       $email[]=$res;
       }
         echo json_encode($email);
    }
    

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Attribute extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('AttributeModel', 'attributeModel');
    }

    public function index() {

        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_attribute', 'attributeID IN (' . $id . ')', $status);
            }
        }
        
        $this->data['breadcrumbs'] = "All Attributes";
        $this->data['pageTitle'] = "All Attributes";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(attributeID) as tblCount', 'ec_attribute', 'status!="Deleted"', FALSE); //data count for pagination
        //echo "<pre>";print_r($totalRws);exit;
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Attribute/index';
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'title' => $this->input->get('name'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );
        
        $selectedAttributes    = $this->generalModel->getTableValue('distinct(attributeID)','ec_product_attribute',null,true);
        $attArray = array();
        foreach ($selectedAttributes as $value) {
            $attArray[] = $value['attributeID'];
        }
        $this->data['usedAttributes'] = $attArray;

        $this->data['result'] = $this->attributeModel->selectAll($params, $this->language);
        if(@$_GET['msg']){
            if($_GET['msg'] == 'active'){
             $this->data['success'] = "Attribute activated Successfully";
            }else if($_GET['msg'] == 'inactive'){
             $this->data['success'] = "Attribute inactivated Successfully";
            }else if($_GET['msg'] == 'delete'){
             $this->data['success'] = "Attribute deleted Successfully";
            }
        }
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }
        $this->render('manage/attributes/list');
    }

    public function add() {
        $this->data['breadcrumbs']  = "Add Attribute";
        $this->data['pageTitle']    = "Add Attribute";

        //$this->form_validation->set_rules('attributeName', 'Attribute Group Name', 'required|xss_clean|is_unique[ec_attribute_detail.name]');
        
        $this->form_validation->set_rules('attributeGroup', 'Attribute Group ', 'required');
        $this->form_validation->set_rules('attributeName', 'Attribute Name ', 'required|xss_clean|callback_uniqueAttribute');
        $this->form_validation->set_rules('sortOrder', 'Attribute Sort Order', 'xss_clean|numeric');

        if ($this->form_validation->run() === TRUE) {

            $attribute['nonXss'] = array(
                'attributeGroupID' => $this->input->post('attributeGroup'),
//              'attributeGroupID' => 1,
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status')
            );
            
            if($this->language==1){
                if($this->input->post('attributeName')){
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($this->input->post('attributeName'))));
                    $attribute['nonXss']['importKey'] =   $importKey;
                }
            }
            
            $attribute['xssData'] = $this->security->xss_clean($attribute['nonXss']);
            $result = $this->generalModel->insertValue('ec_attribute', $attribute['xssData']); //Group Added, $result will have the new ID

            if ($result) {
                $attributeDetail['nonXss'] = array(
                    'attributeID' => $result,
                    'languageID' => $this->language,
                    'name' => $this->input->post('attributeName')
                );
                $attributeDetail['xssData'] = $this->security->xss_clean($attributeDetail['nonXss']);
                $this->generalModel->insertValue('ec_attribute_detail', $attributeDetail['xssData']); //Attribute Deatail Added

                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Attribute Added');

                redirect('manage/attribute/index');
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/attribute/index');
            }
        }
        $this->data['content'] = ['groups' => $this->attributeModel->selectGroup('',$this->language)];
        $this->render('manage/attributes/add');
    }

    public function edit() {

        $this->data['breadcrumbs'] = "Edit Attribute";
        $this->data['pageTitle'] = "Edit Attribute";
        $attributeID = $this->uri->segment(4);

        $this->form_validation->set_rules('attributeName', 'Attribute Name', 'required|xss_clean');
        $this->form_validation->set_rules('attributeGroup', 'Attribute Group', 'required');
        $this->form_validation->set_rules('sortOrder', 'Attribute Sort Order', 'xss_clean|numeric');

        if ($this->form_validation->run() === TRUE) {

            $attribute['nonXss'] = array(
                'attributeGroupID' => $this->input->post('attributeGroup'),
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status')
            );
            
             if($this->language==1){
                if($this->input->post('attributeName')){
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($this->input->post('attributeName'))));
                    $attribute['nonXss']['importKey'] =   $importKey;
                }
            }
            
            $attribute['xssData'] = $this->security->xss_clean($attribute['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_attribute', 'attributeID = ' . $attributeID, $attribute['xssData']);
//UPDATE FILTER GROUP DETAIL

            $langID = $this->language;
            $attributeLangInfo = $this->generalModel->getTableValue('*', 'ec_attribute_detail', array('attributeID' => $attributeID, 'languageID' => $langID));
//            echo "<pre>";print_r($attributeLangInfo);exit;
            if ($attributeLangInfo) {
// FIELD EXISTS IN THE LANGUAGE 
// UPDATE CURRENT ROW
                $attributeDetail['nonXss'] = array(
                    'name' => $this->input->post('attributeName')
                );
                $attributeDetail['xssData'] = $this->security->xss_clean($attributeDetail['nonXss']);
                $result1 = $this->generalModel->updateTableValues('ec_attribute_detail', 'attributeID = ' . $attributeID . ' AND languageID=' . $langID, $attributeDetail['xssData']);
            } else {
// NEW LANUGUAGE
// INSERT INTo TABLE
                $attributeDetail['nonXss'] = array(
                    'name' => $this->input->post('attributeName'),
                    'languageID' => $this->language,
                    'attributeID' => $attributeID
                );
                $attributeDetail['xssData'] = $this->security->xss_clean($attributeDetail['nonXss']);
                $result1 = $this->generalModel->insertValue('ec_attribute_detail', $attributeDetail['xssData']); //Attribute Deatail Added
            }



            if ($result) {
                $this->session->set_flashdata('message', 'Data Updated Successfully');
                $this->logUserActivity('Attribute Added');

                redirect('manage/attribute/index');
                $this->logUserActivity('Attribute Updated');
                //$this->data['message'] = "Data Updated";
            } else {
                $this->data['errMessage'] = "Failed";
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                 redirect('manage/attribute/index');
            }
        }


        $main = $this->attributeModel->select($attributeID, $this->language);
        $groups = $this->attributeModel->selectGroup('',$this->language);

        $this->data['content'] = ['main' => $main, 'groups' => $groups];
//        echo "<pre>"; print_r($this->data);exit;
        $this->render('manage/attributes/add');
    }

    public function uniqueAttribute(){
        $attributeName  =   $this->input->post('attributeName');
        $attributeGroupID = $this->input->post('attributeGroup');
        $this->form_validation->set_message('uniqueAttribute', 'Given attribute '.$attributeName.' already exist in this group');
        $this->db->select('att.attributeID');
        $this->db->from('ec_attribute att');
        $this->db->join('ec_attribute_detail attDet', 'att.attributeID = attDet.attributeID AND attDet.languageID = 1');
        $this->db->where('attDet.name', $attributeName);
        $this->db->where('att.attributeGroupID', $attributeGroupID);
        $query = $this->db->get();
        $num = $query->num_rows();
        if ($num > 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    public function changeStatus() {

        $attributeID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity('Attribute status changed - ' . $status);
        $result = $this->generalModel->updateTableValues('ec_attribute', 'attributeID=' . $attributeID, $this->data['data']);
        if($status == 'Active'){
            redirect('manage/Attribute?msg=active');
        }else if($status =='Inactive'){
            redirect('manage/Attribute?msg=inactive');
        }else if($status == 'Deleted'){
            redirect('manage/Attribute?msg=delete');
        }else{
            redirect('manage/Attribute');
        }
       // redirect($_SERVER['HTTP_REFERER']);
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PromotionBanner extends Admin_Controller {

    private $imageConfig = array();
    
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('PromotionModel', 'promotionModel');
        $imageDimension = $this->getImageDimension('promotion_banner_dimen');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
        $this->imageConfig = array(
            'upload_path' => "./uploads/promotion/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );
    }

    public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_promotion', 'promotionID IN (' . $id . ')', $status);
            }
        }
        $this->data['breadcrumbs'] = "All Promotion & Offer Banner";
        $this->data['pageTitle'] = "All Promotion & Offer Banner";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(promotionID) as tblCount', 'ec_promotion', 'promotionID!=1 AND status!="Deleted"', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/PromotionBanner/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;


        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'title' => $this->input->get('title'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );

        $this->data['result'] = $this->promotionModel->selectAll($params);

        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/promotion/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "All Promotion & Offer Banner";
        $this->data['pageTitle'] = "All Promotion & Offer Banner";

        //form validation 
        $this->form_validation->set_rules('title', 'Promotion/Offers Banner Title', 'required|xss_clean');
        $this->form_validation->set_rules('description', 'Description ', 'required');
        $this->form_validation->set_rules('link', 'Button Link', 'required');
        $this->form_validation->set_rules('images', 'Image', 'required|xss_clean');
        $this->form_validation->set_rules('linkOnButton', 'Text On Button', 'required');
        $this->form_validation->set_rules('sortOrder', 'sort order', 'required|numeric');

        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            $_POST['image'] = '';
            $this->data['imgErr'] = '';
            if ($_FILES['image']['size'] > 0) {
                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    $errVal = 1;
                    $_POST['image'] = '';
                }
            } else {
                $this->data['imgErr'] = "No Image File Uploaded";
                $errVal = 1;
                $_POST['image'] = '';
            }

            $this->session->set_flashdata('imgErr', $this->data['imgErr']);
            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'link' => $this->input->post('link'),
                'image' => $this->input->post('image'),
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status')
            );
            if ($errVal == 1 || $_POST['image'] == '') {
                $resData['nonXss']['status'] = 'Inactive';
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_promotion', $resData['xssData']);

            if ($result) {
                $bannerDetail['nonXss'] = array(
                    'promotionID' => $result,
                    'languageID' => $this->language,
                    'title' => $this->input->post('title'),
                    'linkOnButton' => $this->input->post('linkOnButton'),
                    'description' => $this->input->post('description')
                );
                $bannerDetail['xssData'] = $this->security->xss_clean($bannerDetail['nonXss']);
                $this->generalModel->insertValue('ec_promotion_detail', $bannerDetail['xssData']); //information Added

                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Promotion Banner Added');
                if ($errVal == 0) {
                    redirect('manage/promotionBanner/');
                } else {
                    redirect('manage/promotionBanner/edit/' . $result . '/imgErr');
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/promotionBanner/');
            }
        }
        $this->render('manage/promotion/Add');
    }

    public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Promotion & Offer Banner";
        $this->data['pageTitle'] = "Edit Promotion & Offer Banner";

        $promotionID = $this->uri->segment(4);
        $imgErr = $this->uri->segment(5);
        if ($imgErr) {
            $this->data['imgErr'] = "Invalid Image, Please try again";
            $this->data['errVal'] = 1;
        }
        if ($this->session->flashdata('imgErr') != '') {
            $this->data['imgErr'] = $this->session->flashdata('imgErr');
            $this->data['errVal'] = 1;
        }
        //form validation 
        $this->form_validation->set_rules('title', 'Promotion/Offers Banner Title', 'required|xss_clean');
        $this->form_validation->set_rules('description', 'Description ', 'required');
        $this->form_validation->set_rules('link', 'Button Link', 'required');
        $this->form_validation->set_rules('images', 'Image', 'required|xss_clean');
        $this->form_validation->set_rules('linkOnButton', 'Text On Button', 'required');
        $this->form_validation->set_rules('sortOrder', 'sort order', 'required|numeric');
        //image upload
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            $this->data['imgErr']='';
            $_POST['image']='';
            if ($_FILES['image']['size'] > 0) {

                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    //$_POST['image'] = '';
                    $errVal = 1;
                }
            }

            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'link' => $this->input->post('link'),
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status')
            );
//            if ($errVal == 1) {
//                $resData['nonXss']['status'] = 'Inactive';
//            }
            if ($this->input->post('image')) {
                $resData['nonXss']['image'] = $this->input->post('image');
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_promotion', 'promotionID=' . $promotionID, $resData['xssData']);
            $bannerDetail['nonXss'] = array(
                'promotionID' => $promotionID,
                'languageID' => $this->language,
                'title' => $this->input->post('title'),
                'linkOnButton' => $this->input->post('linkOnButton'),
                'description' => $this->input->post('description')
            );

            $bannerDetail['xssData'] = $this->security->xss_clean($bannerDetail['nonXss']);
            $checkBannerDetail = $this->generalModel->getTableValue('promotionID', 'ec_promotion_detail', 'promotionID=' . $promotionID . ' AND languageID=' . $this->language, FALSE);
            if ($checkBannerDetail) {
                $this->generalModel->updateTableValues('ec_promotion_detail', 'promotionID=' . $promotionID . ' AND languageID=' . $this->language, $bannerDetail['xssData']); //information Added
            } else {
                $this->generalModel->insertValue('ec_promotion_detail', $bannerDetail['xssData']); // insert detail data
            }
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Promotion Banner Updated');
                if ($errVal == 0) {
                    redirect('manage/promotionBanner/');
                } else {
                    $this->session->set_flashdata('imgErr', $this->data['imgErr']);
                    redirect('manage/promotionBanner/edit/' . $promotionID);
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/promotionBanner/');
            }
        }

        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_promotion', 'promotionID=' . "$promotionID" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->data['content']['detail'] = $this->generalModel->getTableValue('*', 'ec_promotion_detail', 'promotionID=' . "$promotionID" . ' AND languageID=' . $this->language, FALSE); //data for edit

        $this->render('manage/promotion/Add');
    }

    public function changeStatus() {

        $promotionID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity('Promotion banner status changed - '.$status);
        $result = $this->generalModel->updateTableValues('ec_promotion', 'promotionID=' . $promotionID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function valid_url_format($str) {
        //echo $str."======test";exit;
        if (empty($str)) {
            $this->form_validation->set_message('valid_url_format', 'The {field} field can not be empty');
            return FALSE;
        } elseif (preg_match('/^(?:([^:]*)\:)?\/\/(.+)$/', $str, $matches)) {
            if (empty($matches[2])) {
                $this->form_validation->set_message('valid_url_format', 'Invalid url');
                return FALSE;
            } elseif (!in_array(strtolower($matches[1]), array('http', 'https'), TRUE)) {
                $this->form_validation->set_message('valid_url_format', 'Invalid url');
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $this->form_validation->set_message('valid_url_format', 'Invalid url');
            return FALSE;
        }
    }

}

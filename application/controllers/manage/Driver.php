<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Driver extends Admin_Controller {

    //private $languageID;

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('TimeSloteModel', 'timeSloteModelModel');
        $this->load->model('UserModel', 'userModel');
        //$this->languageID = 1;
    }

    public function index() {
            if ($this->input->post('action')) {
            $id = implode(",", $this->input->post('multyCheck'));
            $status['status'] = $this->input->post('action');
            $this->generalModel->updateTableValues('ec_user', 'userID IN (' . $id . ')', $status);
        }

        $this->data['breadcrumbs'] = "All User";
        $this->data['pageTitle'] = "All User";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(userID) as tblCount', 'ec_user', 'status!="Deleted " AND userGroupID=5', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Driver/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;      

        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $this->data['currentPage'] = $page + 1;
        $params = array(
            'email' => $this->input->get('email'),
            'limit' => $config['per_page'],
            'start' => $page);

        $this->data['result'] = $this->userModel->selectDriver($params);
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/driver/list');
    }

   public function add() {


        $this->data['breadcrumbs'] = "Add Driver";
        $this->data['pageTitle'] = "Add Driver";
        //form validation 

        $this->form_validation->set_rules('userName', 'User Name', 'trim|required|xss_clean|is_unique[ec_user.userName]');
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|is_unique[ec_user.email]');
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
           
            $this->data['errVal'] = $errVal;


            $resData['nonXss'] = array(
                'userName' => $this->input->post('userName'),
                'userGroupID' => 5,
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'email' => $this->input->post('email'),
                'status' => $this->input->post('status')
            );
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $resData['xssData']['dateAdded'] = date('Y-m-d H:i');
            $resData['xssData']['ip'] = $this->input->ip_address();
            $result = $this->generalModel->insertValue('ec_user', $resData['xssData']);
            if ($result) {
                $this->session->set_flashdata('message', 'Data Added Successfully');

                if ($errVal == 0) {
                    $this->logUserActivity('Driver Added');
                    redirect('manage/Driver/index');
                } else {
                    redirect('manage/Driver/edit/' . $result);
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/information/index');
            }
        }
        $this->data['roles'] = $this->generalModel->getTableValue('*', 'ec_user_group', array('userGroupID!=' => 1), true);

        $this->render('manage/driver/Add');
    }

   public function edit() {
        //$data = array();
        $this->data['breadcrumbs'] = "Edit Driver";
        $this->data['pageTitle'] = "Edit Driver";

        $userID = $this->uri->segment(4);
        $imgErr = $this->uri->segment(5);

        $this->form_validation->set_rules('userName', 'User Name', 'required');
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
       

        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
           
            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
               'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'status' => $this->input->post('status')
            );
            
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $xssEmail = $this->input->post('email');
           
            $resData['xssData']['dateAdded'] = date('Y-m-d H:i');
            $resData['xssData']['ip'] = $this->input->ip_address();
            $result = $this->generalModel->updateTableValues('ec_user', 'userID=' . $userID, $resData['xssData']);


            $resData['nonXss'] = array(
                'userGroupID' =>5,
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'status' => $this->input->post('status')
            );
           
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $xssEmail = $this->input->post('email');
           
            $resData['xssData']['dateAdded'] = date('Y-m-d H:i');
            $resData['xssData']['ip'] = $this->input->ip_address();
            $result = $this->generalModel->updateTableValues('ec_user', 'userID=' . $userID, $resData['xssData']);

            if ($result) {
                $this->session->set_flashdata('message', 'Data Updated Successfully');
                if ($errVal == '0')
                    $this->logUserActivity('Driver Updated');
                redirect('manage/Driver/index');
//                $this->data['message'] = "Data Updated Successfully";
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                //$this->data['message'] = "Failed";
                //redirect('manage/User/index');
            }
        }
        $this->data['roles'] = $this->generalModel->getTableValue('*', 'ec_user_group', array('userGroupID!=' => 1), true);
        $this->data['content'] = $this->generalModel->getTableValue('*', 'ec_user', 'userID=' . "$userID" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->render('manage/driver/edit');
    }

   public function changeStatus() {

        $userID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->logUserActivity('User status changed to - '.$status);
        $data['status'] = $status;
        $result = $this->generalModel->updateTableValues('ec_user', 'userID=' . $userID, $data);
        redirect($_SERVER['HTTP_REFERER']);
    }
    
//    public function warehouseCityAdd() {
//        $cityList = $this->warehouseModel->getTableValue('cityID,countryID', 'ec_cities', 'countryID="173" AND status="Active"', TRUE);
//       
//        foreach($cityList as $cityData){
//            $resData['nonXss'] = array(
//                'warehouseID' => 1,
//                'cityID' => $cityData["cityID"],
//            );
//            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
//          $this->generalModel->insertValue('ec_warehouse_delivery_location', $resData['xssData']);
//        }
//    }
    

}

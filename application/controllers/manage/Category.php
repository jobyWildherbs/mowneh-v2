<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('UserModel', 'userModel');
        $this->load->model('FilterModel', 'filterModel');
        $this->load->model('CategoryModel', 'categoryModel');

        $imageDimension = $this->getImageDimension('list_category_dimen');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;

        $this->imageConfig = array(
            'upload_path' => "./uploads/category/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );

        $imageBannDimension = $this->getImageDimension('banner_category_dimen');
        $imageSetBann = implode(' x ', $imageBannDimension);
        $this->data['imageDimensionBann'] = $imageBannDimension;
        $this->data['imageSetBann'] = $imageSetBann;

        $this->imageConfigBann = array(
            'upload_path' => "./uploads/category/banner/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageBannDimension[0],
            'max_height' => $imageBannDimension[1],
            'min_width' => $imageBannDimension[0],
            'min_height' => $imageBannDimension[1],
            'encrypt_name' => TRUE,
        );
    }

    public function index() {

        if ($this->input->post('action')) {
            $id = implode(",", $this->input->post('multyCheck'));
            $status['status'] = $this->input->post('action');
            $this->generalModel->updateTableValues('ec_category', 'categoryID IN (' . $id . ')', $status);
        }
        $this->data['breadcrumbs'] = "All Category";
        $this->data['pageTitle'] = "All Category";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(categoryID) as tblCount', 'ec_category', 'status!="Deleted"', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Category/index';

        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;

        $params = array(
            'name' => $this->input->get('name'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language);

        $this->data['result'] = $this->categoryModel->selectAll($params);
        //echo "<pre>"; print_r($this->data['result']); exit;

        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $selectedCategories = $this->generalModel->getTableValue('distinct(categoryID)', 'ec_product_to_category', null, true);
      
        $catArray = array();
        foreach ($selectedCategories as $value) {
            $catArray[] = $value['categoryID'];
        }
        
        $this->data['usedCategory'] = $catArray;

        //echo "<pre>"; print_r($this->data['usedCategory']); exit;
        $this->render('manage/category/list');
    }

    public function add() {

        $this->data['breadcrumbs'] = "Add Category";
        $this->data['pageTitle'] = "Add Category";


        $this->data['filterList'] = $this->filterModel->selectFilter('', $this->language); // Filter List
        $this->data['parentCategory'] = $this->categoryModel->selectCategory('', $this->language); // parent category List


        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('sortOrder', 'Sort Order', 'required');
        $this->form_validation->set_rules('filterID[]', 'Filter', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('pageKey', 'Page Key', 'trim|required|xss_clean|is_unique[ec_category.pageKey]');


        if ($this->form_validation->run() === TRUE) {
            $imageError = array();
            $errVal = 0;
            $imageUpload = 0;
            $_POST['image'] = '';
            if ($_FILES['image']['size'] > 0) {

                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $imageError['image'] = $this->upload->display_errors();
                    $errVal = 1;
                    $_POST['image'] = '';
                }
                $imageUpload = 1;
            }
            //$this->session->set_flashdata('imgErr', $this->data['imgErr']);
            $this->data['errVal'] = $errVal;

            if ($_FILES['banner']['size'] > 0) {
                if ($imageUpload == '1')
                    $this->upload->initialize($this->imageConfigBann);
                else
                    $this->load->library('upload', $this->imageConfigBann);
                if ($this->upload->do_upload('banner')) {
                    $_POST['banner'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $imageError['banner'] = $this->upload->display_errors();
                    $errVal = 1;
                    $_POST['image'] = '';
                }
                $imageUpload = 1;
            }


            $this->session->set_flashdata('imgErr', $imageError);
            $this->data['errVal'] = $errVal;
            
            
            $catData['nonXss'] = array(
                'pageKey' => $this->input->post('pageKey'),
                
                'banner' => $this->input->post('banner'),
                'parentID' => $this->input->post('parentID'),
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status'),
                'dateAdded' => date('Y-m-d H:i')
            );
            
            if($this->language==1){
                if($this->input->post('name')){
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($this->input->post('name'))));
                    $catData['nonXss']['importKey'] =   $importKey;
                }
            }
            
            $catData['xssData'] = $this->security->xss_clean($catData['nonXss']);

            $categoryID = $this->generalModel->insertValue('ec_category', $catData['xssData']);
            if ($categoryID) {
                $catDetData['nonXss'] = array(
                    'categoryID' => $categoryID,
                    'languageID' => $this->language,
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description'),
                    'image' => $this->input->post('image'),
                    'metaTitle' => $this->input->post('metaTitle'),
                    'metaDescription' => $this->input->post('metaDescription'),
                    'metaKeyword' => $this->input->post('metaKeyword')
                );
                $catDetData['xssData'] = $this->security->xss_clean($catDetData['nonXss']);
                $this->generalModel->insertValue('ec_category_detail', $catDetData['xssData']); // insert detail data

                $filterData = array();
                foreach ($this->input->post('filterID[]') as $key => $filterID) {
                    $filterData[$key]['categoryID'] = $this->security->xss_clean($categoryID);
                    $filterData[$key]['filterID'] = $this->security->xss_clean($filterID);
                    $this->generalModel->insertValue('ec_category_filter', $filterData[$key]); // insert Filter data
                }
                $categoryPath = $this->generalModel->getTableValueWithLimit('*', 'ec_category_path', array('categoryID' => $this->input->post('parentID')), array('level', 'asc'));
                $level = 0;
                foreach ($categoryPath as $path) {
                    $insert = array(
                        'categoryID' => $categoryID,
                        'pathID' => $path['pathID'],
                        'level' => $level
                    );
                    $this->generalModel->insertValue('ec_category_path', $insert);
                    $level++;
                }
                $insert = array(
                    'categoryID' => $categoryID,
                    'pathID' => $categoryID,
                    'level' => $level
                );
                $this->generalModel->insertValue('ec_category_path', $insert);

                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Category Added');
                if ($errVal == 0) {
                    redirect('manage/Category/index');
                } else {
                    redirect('manage/Category/edit/' . $categoryID . '/imgErr');
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                $this->logUserActivity('Category Added');
                redirect('manage/Category/index');
            }
        }
        $this->render('manage/category/Add');
    }

    public function edit() {

        $this->data['breadcrumbs'] = "Edit Category";
        $this->data['pageTitle'] = "Edit Category";

        $categoryID = $this->uri->segment(4);
        $imgErr = $this->uri->segment(5);
        if ($imgErr) {
            $this->data['imgErr'] = "Invalid Image, Please try again";
            $this->data['errVal'] = 1;
        }
        if ($this->session->flashdata('imgErr') != '') {
            $this->data['imgErr'] = $this->session->flashdata('imgErr');
            $this->data['errVal'] = 1;
        }

        $this->data['filterList'] = $this->filterModel->selectFilter('', $this->language); // Filter List
        $this->data['parentCategory'] = $this->categoryModel->selectCategory('', $this->language); // parent category List

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('sortOrder', 'Sort Order', 'required');
        $this->form_validation->set_rules('filterID[]', 'Filter', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');


        if ($this->form_validation->run() === TRUE) {
            $imageError = array();
            $errVal = 0;
            $imageUpload = 0;
            if ($_FILES['image']['size'] > 0) {

                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $imageError['image'] = $this->upload->display_errors();
                    $errVal = 1;
                }
                $imageUpload = 1;
            }

            if ($_FILES['banner']['size'] > 0) {
                if ($imageUpload == '1')
                    $this->upload->initialize($this->imageConfigBann);
                else
                    $this->load->library('upload', $this->imageConfigBann);

                if ($this->upload->do_upload('banner')) {
                    $_POST['banner'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $imageError['banner'] = $this->upload->display_errors();
                    $errVal = 1;
                }
                $imageUpload = 1;
            }

            $this->session->set_flashdata('imgErr', $imageError);
            
            
            $catData['nonXss'] = array(
                //'image' => $this->input->post('image'),
                //'banner' => $this->input->post('banner'),
                'parentID' => $this->input->post('parentID'),
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status'),
                'dateAdded' => date('Y-m-d H:i')
            );
            if($this->language==1){
                if($this->input->post('name')){
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($this->input->post('name'))));
                    $catData['nonXss']['importKey'] =   $importKey;
                }
            }
          
            
            if ($this->input->post('banner')) {
                $catData['nonXss']['banner'] = $this->input->post('banner');
            }
            $catData['xssData'] = $this->security->xss_clean($catData['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_category', 'categoryID=' . $categoryID, $catData['xssData']);
            
            
            $catDetData['nonXss'] = array(
                'categoryID' => $categoryID,
                'name' => $this->input->post('name'),
                'languageID' => $this->language,
                'description' => $this->input->post('description'),
                'metaTitle' => $this->input->post('metaTitle'),
                'metaDescription' => $this->input->post('metaDescription'),
                'metaKeyword' => $this->input->post('metaKeyword')
            );
            if ($this->input->post('image')) {
                $catDetData['nonXss']['image'] = $this->input->post('image');
            }
            $catDetData['xssData'] = $this->security->xss_clean($catDetData['nonXss']);
            $checkCatDetail = $this->generalModel->getTableValue('categoryID', 'ec_category_detail', 'categoryID=' . $categoryID . ' AND languageID=' . $this->language, FALSE);
            if ($checkCatDetail) {
                $this->generalModel->updateTableValues('ec_category_detail', 'categoryID=' . $categoryID . ' AND languageID=' . $this->language, $catDetData['xssData']); // insert detail data
            } else {
                $this->generalModel->insertValue('ec_category_detail', $catDetData['xssData']); // insert detail data
            }
            $this->generalModel->deleteTableValues('ec_category_filter', array('categoryID' => $categoryID));
            $filterData = array();
            foreach ($this->input->post('filterID[]') as $key => $filterID) {
                $filterData[$key]['categoryID'] = $this->security->xss_clean($categoryID);
                $filterData[$key]['filterID'] = $this->security->xss_clean($filterID);
                $this->generalModel->insertValue('ec_category_filter', $filterData[$key]); // insert Filter data
            }
            
            $this->generalModel->deleteTableValues('ec_category_path', array('categoryID'=>$categoryID));
            $categoryPath = $this->generalModel->getTableValueWithLimit('*', 'ec_category_path', array('categoryID' => $this->input->post('parentID')), array('level', 'asc'));
            $level = 0;
            foreach ($categoryPath as $path) {
                $insert = array(
                    'categoryID' => $categoryID,
                    'pathID' => $path['pathID'],
                    'level' => $level
                );
                $this->generalModel->insertValue('ec_category_path', $insert);
                $level++;
            }
            $insert = array(
                'categoryID' => $categoryID,
                'pathID' => $categoryID,
                'level' => $level
            );
            $this->generalModel->insertValue('ec_category_path', $insert);
            //echo $this->db->last_query();exit;
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Category Updated');
                if ($errVal == 0)
                    redirect('manage/Category/index');
                else
                    redirect('manage/Category/edit/' . $categoryID);
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                $this->logUserActivity('Category Updated');
                redirect('manage/Category/index');
            }
        }

        $this->data['content'] = $this->categoryModel->selectCategory($categoryID, $this->language); //data for edit
        $filterData = $this->generalModel->getTableValue('filterID', 'ec_category_filter', 'categoryID=' . $categoryID, TRUE); //data for edit
        foreach ($filterData as $key => $filters) {
            $this->data['content']['filterData'][$key] = $filters['filterID'];
        }
        $this->render('manage/category/Add');
    }

    public function changeStatus() {
        $categoryID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity('Category status changed - ', $status);
        $result = $this->generalModel->updateTableValues('ec_category', 'categoryID=' . $categoryID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }

}

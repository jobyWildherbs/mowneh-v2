<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends Admin_Controller {

    

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('NewsletterModel', 'newsletterModel');
//        $imageDimension = $this->getImageDimension('home_newsletter_slider_dimen');
//        $imageSet = implode(' x ', $imageDimension);
//        $this->data['imageDimension'] = $imageDimension;
//        $this->data['imageSet'] = $imageSet;
//        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
//        $this->data['allowedType'] = $this->allowedImageTypes;
//        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
//        $this->imageConfig = array(
//            'upload_path' => "./uploads/newsletter/",
//            'allowed_types' => $this->allowedImageTypes,
//            'max_size' => $this->maxImageUploadSize,
//            'max_width' => $imageDimension[0],
//            'max_height' => $imageDimension[1],
//            'min_width' => $imageDimension[0],
//            'min_height' => $imageDimension[1],
//            'encrypt_name' => TRUE,
//        );
    }

    public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_newsletter', 'newsletterID IN (' . $id . ')', $status);
            }
        }
        $this->data['breadcrumbs'] = "All Newsletter Page";
        $this->data['pageTitle'] = "All Newsletter Page";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(newsletterID) as tblCount', 'ec_newsletter', 'newsletterID!=1 AND status!="delete"', FALSE); //data count for pagination
        
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Newsletter/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'email' => $this->input->get('email'),
            'limit' => $config['per_page'],
            'start' => $page,
            
        );

        $this->data['result'] = $this->newsletterModel->selectAll($params);

        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/newsletter/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Newsletter";
        $this->data['pageTitle'] = "Add Newsletter";

        //form validation 
        $this->form_validation->set_rules('name', 'Newsletter Name', 'required|xss_clean');
     

        if ($this->form_validation->run() === TRUE) {

            $resData['nonXss'] = array(
                'name' => $this->input->post('name'),
                
                'status' => $this->input->post('status')
            );
           
           
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_newsletter', $resData['xssData']);

            if ($result) {
                
                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Newsletter Added');

                    redirect('manage/newsletter/index');

            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/newsletter/index');
            }
        }
        $this->render('manage/newsletter/Add');
    }

    public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Newsletter";
        $this->data['pageTitle'] = "Edit Newsletter";

        $newsletterID = $this->uri->segment(4);

        //form validation 
       $this->form_validation->set_rules('name', 'Newsletter Name', 'required|xss_clean');
        
        //image upload
        if ($this->form_validation->run() === TRUE) {

            $resData['nonXss'] = array(
                'name' => $this->input->post('name'),
                
                'status' => $this->input->post('status')
            );

            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_newsletter', 'newsletterID=' . $newsletterID, $resData['xssData']);

            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Newsletter Updated');

                    redirect('manage/newsletter/index');

            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/newsletter/index');
            }
        }

        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_newsletter', 'newsletterID=' . "$newsletterID" . ' AND status!="Deleted"', FALSE); //data for edit
        

        $this->render('manage/newsletter/Add');
    }

    public function changeStatus() {

        $newsletterID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_newsletter', 'newsletterID=' . $newsletterID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }

    

}

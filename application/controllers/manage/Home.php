<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('GeneralModel', 'gm');
        $this->load->model('HomeModel', 'hm');
        $this->load->model('OrderModel', 'om');
        $this->load->model('ProductModel', 'pm');
        $this->load->library('form_validation');
    }

    public function index() {
        $this->data['breadcrumbs'] = "Dashboard";
        $this->data['pageTitle'] = "Dashboard";

//        GET TOTAL ORDERS
        $this->data['count']['orders']      = $this->gm->getTableCount('ec_order', '', '');
//        GET TOTAL SALES
        $totals                            = $this->gm->getTableValue('sum(total) as total','ec_order','',TRUE);
        $this->data['count']['sales']       = $this->currency->format($totals[0]['total'],$this->siteCurrency);
//        GET TOTAL CUSTOMERS
        $this->data['count']['customers']   = $this->gm->getTableCount('ec_customer', '', '');
//        GET TOTAL PRODUCTS
        $this->data['count']['products']    = $this->gm->getTableCount('ec_product', array('status!=' => 'Deleted'), '');

//        LATEST ORDERS
        if ($this->data['count']['orders']) {
            
        }
        
        $params     = array(
            'where'         => null,
            'languageID'    => $this->language,
            'limit'         => 10
        );
        
        $this->data['orders'] = $this->om->selectAll($params);
        
        
//        RECENT ACTIVITIES
        $params2['languageID'] = $this->language;
        $params2['limit'] = 5;
        $params2['order'] = '';
        $result = $this->hm->recentActivity($params2);
        
        if ($result) {
            foreach ($result as $key => $row) {
                
                $dt = new DateTime($row['updatedTime']);
                $cdate = $dt->format('d/M/Y');
                $ctime = $dt->format('h:i:s A');
                $result[$key]['ctime'] = $ctime;
                $result[$key]['cdate'] = $cdate;
//                if($row['usedMethod'] == 'changeStatus')    {
//                    $result[$key]['activityTitle'] = $row['usedClass']. " status changed to " . $row['activityTitle'];
//                }
            }
        }
        $this->data['activities'] = $result;
//        echo "<pre>";print_r($this->data['activities']);exit;
        $this->render('manage/home/Dashboard');
    }
    
    public function pushNotification() {
        $url = "https://fcm.googleapis.com/fcm/send";
        $token = "cxyK3-ztTouH5l3spIL4hY:APA91bF2A6Dy3wDO46CnajpW_KYsN_gFVlA6U7Z0GaA3EbVmpu8aqA-xLsnYyayN-yDknUIgphaOxaWk1wnbwuzo8ZgiabG6L2wTCu2tJN14tng6atBKUvN2DKJ2r1bwbVzDfgLPDydM";
        $serverKey = 'AAAAtLlUYc0:APA91bG3NULL6yVXfConMcbJYNuF0-lyoY2RPHmkzuS_pTnqmAKSQQ4UQi0PGTlaDHk_tADlQ0xPXdt5YmWM5gfBmcutxA-jYcFelsreB1eVfWcqiVT1zUWupPoxo4WNSzSfLolT7TIU';
        $title = "Title";
        $body = "Body of the message";
        $notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');
        $arrayToSend = array('to' => $token, 'notification' => $notification,'priority'=>'high');
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. $serverKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,

        "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        //Send the request
        $response = curl_exec($ch);
        //Close request
        if ($response === FALSE) {
        die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
    }

}

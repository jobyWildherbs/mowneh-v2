<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Admin_Controller {

    private $imageConfig = array();
    private $bannerConfig = array();
    private $isValidationError = false;
    private $redirectValue = '';

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('UserModel', 'userModel');
        $this->load->model('FilterModel', 'filterModel');
        $this->load->model('CategoryModel', 'categoryModel');
        $this->load->model('ManufacturesModel', 'manufacturesModel');
        $this->load->model('ProductModel', 'productModel');
        $this->load->model('AttributeModel', 'attributeModel');
        $this->load->model('OptionModel', 'optionModel');

        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;

        $imageDimension = $this->getImageDimension('product_popup_dimen');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->setDirectory(FCPATH . "uploads/product/");
        $this->setDirectory(FCPATH . "uploads/product/banner/");
        $this->imageConfig = array(
            'upload_path' => FCPATH . "uploads/product/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );

        $imageDimensionBanner = $this->getImageDimension('banner_product_dimen');
        $imageSetBanner = implode(' x ', $imageDimensionBanner);
        $this->data['imageDimension_banner'] = $imageDimensionBanner;
        $this->data['imageSet_banner'] = $imageSetBanner;
        $this->bannerConfig = array(
            'upload_path' => FCPATH . "uploads/product/banner/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimensionBanner[0],
            'max_height' => $imageDimensionBanner[1],
            'min_width' => $imageDimensionBanner[0],
            'min_height' => $imageDimensionBanner[1],
            'encrypt_name' => TRUE,
        );
    }

    public function index() {
        if ($this->input->post('action')) {
            $id = implode(",", $this->input->post('multyCheck'));
            $status['status'] = $this->input->post('action');
            $this->generalModel->updateTableValues('ec_product', 'productID IN (' . $id . ')', $status);
        }

        if ($this->input->post('searchButton')) {
            $searchUrl = "";
            $urlArray = array();
            $name       = $this->security->xss_clean($this->input->post('name'));
            $type       = $this->security->xss_clean($this->input->post('type'));
            $category   = $this->security->xss_clean($this->input->post('category'));
            $manufacturer  = $this->security->xss_clean($this->input->post('manufacturer'));
            $dateAdded  = $this->security->xss_clean($this->input->post('dateAdded'));
            $fromPrice  = $this->security->xss_clean($this->input->post('fromPrice'));
            $toPrice    = $this->security->xss_clean($this->input->post('toPrice')); 
            if ($name)
                $urlArray['name'] = $name;
            if ($type)
                $urlArray['type'] = $type;
            if ($manufacturer)
                $urlArray['manufacturer'] = $manufacturer;
            if ($category)
                $urlArray['category'] = $category;
            if ($dateAdded)
                $urlArray['dateAdded'] = $dateAdded;
            if ($fromPrice)
                $urlArray['fromPrice'] = $fromPrice;
            if ($toPrice)
                $urlArray['toPrice'] = $toPrice;

            redirect('manage/Product?' . http_build_query($urlArray));
        }

        $this->data['breadcrumbs'] = "All Products";
        $this->data['pageTitle'] = "All Products";
       
        
        

        $params = array(
            'name'              => $this->security->xss_clean($this->input->get('name')),
            'type'              => $this->security->xss_clean($this->input->get('type')),
            'manufacturerID'    => $this->security->xss_clean($this->input->get('manufacturer')),
            'category'          => $this->security->xss_clean($this->input->get('category')),
            'dateAdded'         => $this->security->xss_clean($this->input->get('dateAdded')),
            'fromPrice'         => $this->security->xss_clean($this->input->get('fromPrice')),
            'toPrice'           => $this->security->xss_clean($this->input->get('toPrice')),
            'languageID'        => $this->language);

        $this->data['manufacturesList'] = $this->manufacturesModel->selectFilter('', $this->language); // Manufacture List For List
        $this->data['mainCategory']     = $this->categoryModel->selectCategory('', $this->language); // parent category List manufacturerID
        
        
        $this->load->library("pagination");
//        $totalRws = $this->generalModel->getTableValue('count(productID) as tblCount', 'ec_product', 'status!="Deleted"', FALSE); //data count for pagination

        $config = $this->paginationConfig;
        $config['reuse_query_string'] = true;
        $config['per_page'] = ADMIN_DATA_LIMIT;
       // $config['total_rows'] = $totalRws['tblCount']; count($this->productModel->selectAll($params))
        $config['total_rows'] = count($this->productModel->selectAll($params));
        $config['base_url'] = base_url() . 'manage/Product/index';
       

        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        
        $params['limit']    =   $config['per_page'];
        $params['start']    =   $page;
        $this->data['currentPage'] = $page + 1;
        $this->data['result']           = $this->productModel->selectAll($params);
        
        if ($this->session->flashdata('success') != '') {
            $this->data['success'] = $this->session->flashdata('success');
        } else {
            //echo $this->data['success'];
            $this->data['failed'] = $this->session->flashdata('failed');
        }
        $this->render('manage/product/list');
    }

    public function add() {
        $this->data['breadcrumbs'] = "Add Product";
        $this->data['pageTitle'] = "Add Product";
        $this->data['pdType'] = 'start';

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('pageKey', 'Page Key', 'trim|required|alpha_dash|xss_clean|is_unique[ec_product.pageKey]');
        $this->form_validation->set_rules('metaTitle', 'Meta Title', 'required');
        $this->form_validation->set_rules('highlights', 'Highlights', 'required');
        $this->form_validation->set_rules('highlights', 'Highlights', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        if ($this->form_validation->run() === TRUE) {
            /*             * ************************ Product Add Start********************* */
            $product['nonXss'] = array(
                'pageKey'           => $this->input->post('pageKey'),
                'type'              => $this->input->post('type'),
                'quantity'          => 0,
                'manufacturerID'    => 0,
                'sortOrder'         => 0,
                'status'            => 'Inactive',
                'dateAdded'         => date('Y-m-d H:i')
            );
            
            if($this->language==1){
                if($this->input->post('name')){
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($this->input->post('name'))));
                    $product['nonXss']['importKey'] =   $importKey;
                }
            }

            $productData['xssData'] = $this->security->xss_clean($product['nonXss']);
            $productID = $this->generalModel->insertValue('ec_product', $productData['xssData']); // Insert Product Data
            /*             * ******************* Product Add End ************************** */

            /*             * ******************** Product Details Add Start***************** */
            if ($productID) {
                $proDetData['nonXss'] = array(
                    'productID' => $productID,
                    'languageID' => $this->language,
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description'),
                    'tag' => $this->input->post('tag'),
                    'highlights' => $this->input->post('highlights'),
                    'metaTitle' => $this->input->post('metaTitle'),
                    'metaDescription' => $this->input->post('metaDescription'),
                    'metaKeyword' => $this->input->post('metaKeyword')
                );
                $proDetData['xssData'] = $this->security->xss_clean($proDetData['nonXss']);
                $this->generalModel->insertValue('ec_product_description', $proDetData['xssData']); // insert detail data
                /*                 * ****************** Product Details Add End***************** */
                $this->logUserActivity('Product Added');
                redirect('manage/product/edit/' . $productID . '/data/');
            } else {
                $this->session->set_flashdata('failed', "Product added failed. Please try again");
                redirect('manage/product');
            }
        }
        $this->render('manage/product/Add');
    }

    public function edit($productID = 0, $pdType = '') {
        $this->data['breadcrumbs']  = "Edit Product";
        $this->data['pageTitle']    = "Edit Product";
        $this->data['pdType']       = $pdType;
        $this->data['productID']    = $productID;
        if ($productID == 0) {
            $productID = $this->uri->segment(4);
        }
        if ($pdType == '') {
            $pdType = 'general';
        }
        //echo $pdType;exit;
        $this->data['general_tab']      = 'manage/product/edit/' . $productID . '/general';
        $this->data['data_tab']         = 'manage/product/edit/' . $productID . '/data';
        $this->data['links_tab']        = 'manage/product/edit/' . $productID . '/links';
        $this->data['attribute_tab']    = 'manage/product/edit/' . $productID . '/attribute';
        $this->data['discount_tab']     = 'manage/product/edit/' . $productID . '/discount';
        $this->data['images_tab']       = 'manage/product/edit/' . $productID . '/images';
        $this->data['option_tab']       = 'manage/product/edit/' . $productID . '/option';

        $this->data['content']          = $this->productModel->selectProductDetail($productID, $this->language, 'admin');
        //echo "<pre>"; print_r($this->data['content']); exit;
        $this->data['mainCategory']     = $this->categoryModel->selectCategory('', $this->language); // parent category List manufacturerID
        $this->data['manufacturesList'] = $this->manufacturesModel->selectFilter('', $this->language); // Manufacture List
        $this->data['productList']      = $this->productModel->selectProduct('', $this->language); // Product List
        $this->data['productListForBundle']      = $this->productModel->selectProductForBundle('', $this->language); // Product List
        
        $this->data['attributesList']   = $this->attributeModel->selectAttribute('', $this->language); // Product List
        $this->data['parentCategory']   = $this->categoryModel->selectCategory('', $this->language); // parent category List
        $this->data['optionsList']      = $this->optionModel->selectAllActiveOption($this->language);
       
        
         
        if ($pdType != '' && $this->input->post()) {
            switch ($pdType) {
                // save data from general tab
                case 'general':
                    $this->save_general($productID);
                    break;
                case 'data' :
                    $this->save_data($productID);
                    break;
                case 'links' :
                    $this->save_links($productID);
                    break;
                case 'attribute' :
                    $this->save_attribute($productID);
                    break;
                case 'discount' :
                    $this->save_discount($productID);
                    break;
                case 'images' :
                    $this->save_images($productID);
                    break;
                case 'option' :
                    $this->save_option($productID);
                    break;
                default : $this->save_general($productID);
            }
        }

        if ($this->session->flashdata('imageError') != '') {
            $this->data['imageError'] = $this->session->flashdata('imageError');
            $this->data['errVal'] = 1;
        }
        if ($this->session->flashdata('imgErr') != '') {
            $this->data['imgErr'] = $this->session->flashdata('imgErr');
            $this->data['errVal'] = 1;
        }
        if ($this->session->flashdata('bannerImgErr') != '') {
            $this->data['bannerImgErr'] = $this->session->flashdata('bannerImgErr');
            $this->data['errVal'] = 1;
        }
        if ($this->session->flashdata('additionalImgErr') != '') {
            $this->data['additionalImgErr'] = $this->session->flashdata('additionalImgErr');
            $this->data['errVal'] = 1;
        }

        //data for edit  
        /*         * *******************************Product Category List Start********************** */
        $productCategory = $this->generalModel->getTableValue('productID,categoryID', 'ec_product_to_category', 'productID=' . $productID, TRUE); //data for edit
        foreach ($productCategory as $key => $proCat) {
            $this->data['content']['categoryData'][$key] = $proCat['categoryID'];
        }
        /*         * *******************************Product Category List End********************** */

        /*         * *******************************Filter List By Category start********************** */
        //echo "<pre>";print_r($this->data['content']);exit;
        if (isset($this->data['content']['categoryData'])) {
            $proCategory = implode(",", $this->data['content']['categoryData']);
            $cateFilter = $this->generalModel->getTableValue('filterID,categoryID', 'ec_category_filter', 'categoryID IN (' . $proCategory . ')', TRUE);
            $filterGrp = array();
            foreach ($cateFilter as $key => $filterVal) {
                $filterGrp[$key] = $filterVal['filterID'];
            }
            $this->data['filterList']   =   "";
            if($filterGrp){
                $filterGroupID = implode(",", $filterGrp);
                $this->data['filterList'] = $this->generalModel->getTableValue('filterID,filterGroupID,name', 'ec_filter_detail', 'filterGroupID IN (' . $filterGroupID . ') AND languageID='.$this->language, TRUE);
            }
                
        } else {
            $proCategory = 0;
            $this->data['filterList'] = array();
        }



        $productFilter = $this->generalModel->getTableValue('productID,filterID', 'ec_product_filter', 'productID=' . $productID, TRUE); //data for edit
        foreach ($productFilter as $key => $proFilter) {
            $this->data['content']['productFilter'][$key] = $proFilter['filterID'];
        }

        $reletedProduct = $this->generalModel->getTableValue('productID,relatedID', 'ec_product_related', 'productID=' . $productID, TRUE); //data for edit
        foreach ($reletedProduct as $key => $reletedPro) {
            $this->data['content']['reletedProData'][$key] = $reletedPro['relatedID'];
        }
        
        $this->data['content']['bundleProduct'] = $this->generalModel->getTableValue('bundleID,productID,bundleProductID,quantity', 'ec_product_bundle', 'productID=' . $productID, TRUE); //data for edit

        $this->data['content']['productAttribute'] = $this->productModel->selectProductAttributes($productID, $this->language); //data for edit
        //echo "<pre>"; print_r($this->data['content']['productAttribute']); exit;
        $this->data['content']['productDiscount'] = $this->productModel->selectProductDiscountList($productID, $this->language); //data for edit
        $this->data['content']['addImage'] = $this->generalModel->getTableValue('productImageID,image,sortOrder', 'ec_product_image', 'productID=' . $productID, TRUE); //data for edit
        
        $optionValues   =   array();
        $this->data['content']['productOptionCombination']    =   $this->optionModel->selectOptionCombinationByProductId($productID);
        $combinationID  =   "";
        $productOption  =   array();
        if(@$this->data['content']['productOptionCombination']['optionID']){
            $optionIDVal    =   explode(",",$this->data['content']['productOptionCombination']['optionID']);
            
            foreach($optionIDVal as $key=>$optionID){
                $optionName  =    $this->optionModel->selectOptionName($optionID,$this->language);
                $optionValues[$key]['optionName']       =  $optionName ['optionName'];
                $optionValues[$key]['optionID']         =  $optionName ['optionID'];
                $optionValues[$key]['values']          =   $this->optionModel->selectOptionValueByCombinationOptionID($optionID,$this->language);
            }
            $combinationID  =   $this->data['content']['productOptionCombination']['combinationID'];
            
            $productOption =   $this->productModel->getProductOptionsManage($combinationID,$productID,$this->language);
            foreach($productOption as $key=>$productOptionData){
                $optionValID    =   array();
                $optionValIDadded    =   $this->generalModel->getTableValue('optionValueID', 'ec_product_option_value', 'productOptionID=' . $productOptionData['productOptionID'], TRUE);
                
                if($optionValIDadded){
                    foreach($optionValIDadded as $keyVal=>$optionValData){
                    $optionValID[$keyVal]    =   $optionValData['optionValueID'];
                    }
                    $optionVal    =   $this->generalModel->getTableValue('GROUP_CONCAT(name SEPARATOR " + ") AS optionValueName', 'ec_option_value_detail', 'optionValueID IN(' . implode(",",$optionValID) .') AND languageID='.$this->language, FALSE);
                    $productOption[$key]['optionValueName'] =  $optionVal['optionValueName'];
                }
                
            }
            
        }
        
        $this->data['content']['productOption'] =   $productOption;
       
        $this->data['optionValues']    =   $optionValues;
        $this->data['combinationID']    =   $combinationID;

        if (!$this->isValidationError && $this->redirectValue != '') {
            $this->logUserActivity('Product Updated');
            redirect($this->redirectValue);
        }
    
        $this->render('manage/product/edit');
    }

    public function changeStatus() {

        $productID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $result = $this->generalModel->updateTableValues('ec_product', 'productID=' . $productID, $this->data['data']);
        $this->logUserActivity('Product Status Changed - '.$status);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function ajaxLoadFilters() {
        $categortID = $this->input->post('categoryID');
        $cateFilter = $this->generalModel->getTableValue('filterID,categoryID', 'ec_category_filter', 'categoryID=' . $categortID, TRUE);
        $filterGrp = array();
        foreach ($cateFilter as $key => $filterVal) {
            $filterGrp[$key] = $filterVal['filterID'];
        }
        $filterList =   "";
        $filterGroupID = implode(",", $filterGrp);
        $filterList = $this->generalModel->getTableValue('filterID,filterGroupID,name', 'ec_filter_detail', 'filterGroupID IN (' . $filterGroupID . ') AND languageID='.$this->language, TRUE);
        $this->data['filterList'] = $filterList;
        
//        echo "<pre>";print_r($filterList);
        $this->render('manage/product/ajaxFilteList');
    }

    protected function save_general($productID = 0) {
        if ($productID != 0) {
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('pageKey', 'Page Key', 'trim|required|alpha_dash');
            $this->form_validation->set_rules('metaTitle', 'Meta Title', 'required');
            $this->form_validation->set_rules('highlights', 'Highlights', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            $validationErrors = array();
            if ($this->form_validation->run() === true) {
                
                if($this->language==1){
                    if($this->input->post('name')){
                        $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($this->input->post('name'))));
                        $product['nonXss'] = array(
                            'pageKey'           => $this->input->post('pageKey'),
                            'importKey' => $importKey,
                            'type' => $this->input->post('type'),
                         );
//                        $product['nonXss']['importKey'] =   $importKey;
                        $productData['xssData'] = $this->security->xss_clean($product['nonXss']);
                        $this->generalModel->updateTableValues('ec_product', 'productID=' . $productID, $productData['xssData']); // insert detail data
                    }
                }else{
                    $product['nonXss'] = array(
                            'type' => $this->input->post('type'),
                         );
//                        $product['nonXss']['importKey'] =   $importKey;
                        $productData['xssData'] = $this->security->xss_clean($product['nonXss']);
                        $this->generalModel->updateTableValues('ec_product', 'productID=' . $productID, $productData['xssData']); // insert detail data
                }

                $proDetData['nonXss'] = array(
                    'productID' => $productID,
                    'languageID' => $this->language,
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description'),
                    'tag' => $this->input->post('tag'),
                    'highlights' => $this->input->post('highlights'),
                    'metaTitle' => $this->input->post('metaTitle'),
                    'metaDescription' => $this->input->post('metaDescription'),
                    'metaKeyword' => $this->input->post('metaKeyword')
                );
                $proDetData['xssData'] = $this->security->xss_clean($proDetData['nonXss']);
                $checkProDetail = $this->generalModel->getTableValue('productID', 'ec_product_description', 'productID=' . $productID . ' AND languageID=' . $this->language, FALSE);
                if ($checkProDetail) {
                    $this->generalModel->updateTableValues('ec_product_description', 'productID=' . $productID . ' AND languageID=' . $this->language, $proDetData['xssData']); // insert detail data
                } else {
                    $this->generalModel->insertValue('ec_product_description', $proDetData['xssData']); // insert detail data
                }
                /*                 * ****************** Product Details Add End***************** */
                $this->productModel->updateSearchText($productID, $this->language);
                $this->logUserActivity('Product general data Updated');
                $this->redirectValue = 'manage/product/edit/' . $productID . '/data';
                
            } else {
                $this->isValidationError = true;
            }
        }
    }

    protected function save_data($productID = 0) {
        if ($productID != 0) {
            $this->form_validation->set_rules('model', 'Item Code', 'required');
            $this->form_validation->set_rules('price', 'Price', 'required|greater_than[0]');
            $this->form_validation->set_rules('quantity', 'Quantity', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('minimum', 'Minimum Quantity', 'required|is_natural_no_zero');
            $this->form_validation->set_rules('sortOrder', 'Sort Order', 'required|is_natural_no_zero');
            if ($this->form_validation->run() === true) {
                $product['nonXss'] = array(
                    'model' => $this->input->post('model'),
                    'sku' => $this->input->post('sku'),
                    'price' => $this->input->post('price'),
                    'quantity' => $this->input->post('quantity'),
                    'minimum' => $this->input->post('minimum'),
                    'weight' => $this->input->post('weight'),
                    'length' => $this->input->post('length'),
                    'height' => $this->input->post('height'),
                    'width' => $this->input->post('width'),
                    'sortOrder' => $this->input->post('sortOrder'),
                    'status' => $this->input->post('status'),
                    'dateAdded' => date('Y-m-d H:i')
                );
                
                $productData['xssData'] = $this->security->xss_clean($product['nonXss']);
                $result = $this->generalModel->updateTableValues('ec_product', 'productID=' . $productID, $productData['xssData']);
                $this->productModel->updateSearchText($productID, $this->language);
                $this->logUserActivity('Product details Updated');
                $this->redirectValue = 'manage/product/edit/' . $productID . '/links';
            } else {
                $this->isValidationError = true;
            }
        }
    }

    protected function save_links($productID = 0) {
        if ($productID != 0) {
            $this->form_validation->set_rules('categoryID[]', 'Category', 'required'); 
            $this->form_validation->set_rules('manufacturerID', 'Brand', 'required');
            if ($this->form_validation->run() === true) {
                //UPDATE MANUFACTURE
                $product['nonXss'] = array(
                    'manufacturerID' => $this->input->post('manufacturerID'),
                );

                $productData['xssData'] = $this->security->xss_clean($product['nonXss']);
                $result = $this->generalModel->updateTableValues('ec_product', 'productID=' . $productID, $productData['xssData']);
                //!END UPDATE MANUFACTURE
                //                
                //UPDATE CATEGORY
               
                if($this->input->post('categoryID[]')){
                     $category   =   $this->input->post('categoryID[]');
                $this->generalModel->deleteTableValues('ec_product_to_category','productID='.$productID);
                    foreach($category as $key=>$catData){
                        $proCategoryData    =   array();
//                     
                            
                            $proCategoryData['nonXss'] = array(
                                'productID'     => $productID,
                                'categoryID'    => $catData
                            );
                          
                            $proCategoryData['xssData'] = $this->security->xss_clean($proCategoryData['nonXss']);
                            $this->generalModel->insertValue('ec_product_to_category', $proCategoryData['xssData']);                
                    }
                 } 

//!END UPDATE CATEGORY
//                
//UPDATE FILTER DATA
                $this->generalModel->deleteTableValues('ec_product_filter', 'productID=' . $productID);
                $filterData = array();
                if ($this->input->post('filterID[]')) {
                    foreach ($this->input->post('filterID[]') as $key => $filterID) {
                        $filterData[$key]['productID'] = $this->security->xss_clean($productID);
                        $filterData[$key]['filterID'] = $this->security->xss_clean($filterID);
                        $this->generalModel->insertValue('ec_product_filter', $filterData[$key]); // insert Filter data
                    }
                }

//!END UPDATE FILTER DATA
                //UPDATE BUNDLE PRODUCT
                 
                $this->generalModel->deleteTableValues('ec_product_bundle', 'productID=' . $productID);    
                if (!empty($_POST['bundleProductID'])) {
                        $quantity   =   $_POST['quantity'];
                    foreach ($_POST['bundleProductID'] as $key => $bundleProductID) {
                       
                        $bundleProduct[$key]['productID']       = $this->security->xss_clean($productID);
                        $bundleProduct[$key]['bundleProductID'] = $this->security->xss_clean($bundleProductID);
                        $bundleProduct[$key]['quantity']        = $this->security->xss_clean($quantity[$key]);

                        $this->generalModel->insertValue('ec_product_bundle', $bundleProduct[$key]); // insert Filter data 
                    }
                }
//UPDATE RELATED PRODUCTS
                $this->generalModel->deleteTableValues('ec_product_related', 'productID=' . $productID);
                $reletedProduct = array();
                if (!empty($_POST['reletedproductID'])) {
                    foreach ($_POST['reletedproductID'] as $key => $reletedproductID) {
                        $reletedProduct[$key]['productID'] = $this->security->xss_clean($productID);
                        $reletedProduct[$key]['relatedID'] = $this->security->xss_clean($reletedproductID);

                        $this->generalModel->insertValue('ec_product_related', $reletedProduct[$key]); // insert Filter data 
                    }
                }
//!END UPDATE RELATED PRODUCT
                $this->productModel->updateSearchText($productID, $this->language);
                $this->logUserActivity('Product links Updated');
                $this->redirectValue = 'manage/product/edit/' . $productID . '/attribute';
            } else {
               
                $this->isValidationError = true;
            }
        }
    }

    protected function save_attribute($productID = 0) {
        if ($productID != 0) {
            $this->generalModel->deleteTableValues('ec_product_attribute', 'productID=' . $productID);
            $attributeData = array();
            $attributeText = $this->input->post('attributeText[]');
            foreach ($this->input->post('attributeID[]') as $key => $attribute) {
                $attributeData[$key]['productID'] = $this->security->xss_clean($productID);
                $attributeData[$key]['attributeID'] = $this->security->xss_clean($attribute);
                $attributeData[$key]['languageID'] = $this->language;
                $attributeData[$key]['text'] = htmlspecialchars_decode($this->security->xss_clean($attributeText[$key]));
                $this->generalModel->insertValue('ec_product_attribute', $attributeData[$key]); // insert Filter data
            }
            $this->productModel->updateSearchText($productID, $this->language);
            $this->logUserActivity('Product attribute Updated');
            $productType = $this->generalModel->getTableValue('type', 'ec_product', 'productID='.$productID, FALSE);
//            echo "<pre>"; print_r($productType); exit;
            if($productType['type']=='Product')
                $this->redirectValue = 'manage/product/edit/' . $productID . '/option';
            elseif($productType['type']=='Bundle')
               $this->redirectValue = 'manage/product/edit/' . $productID . '/discount'; 
            else
                $this->redirectValue = 'manage/product/edit/' . $productID . '/option';
        }
    }
    
    protected function save_option($productID = 0){
        $this->redirectValue = 'manage/product/edit/' . $productID . '/discount';
    }

    protected function save_discount($productID = 0) {
        
//$this->form_validation->set_rules('second_field', 'Second Field', 'trim|required|is_natural_no_zero|callback_check_equal_less['.$this->input->post('first_field').']');
        if ($productID != 0) {
            $this->form_validation->set_rules('discountQuantity[]', 'Quantity', 'callback_check_quentity['.$productID.']'); 
            if ($this->form_validation->run() === TRUE) {
                $this->generalModel->deleteTableValues('ec_product_discount', 'productID=' . $productID);
                $discountData       = array();
                $customer_group_id  = $this->input->post('customer_group_id[]');
                $quantity           = $this->input->post('discountQuantity[]');
                $priority           = $this->input->post('priority[]');
                $discountPrice      = $this->input->post('discountPrice[]');
                $type               = $this->input->post('type[]');
                $dateStart          = $this->input->post('dateStart[]');
                $dateEnd            = $this->input->post('dateEnd[]');
                foreach ($quantity as $key => $quantityval) {
                    $discountData[$key]['productID']            = $this->security->xss_clean($productID);
                    $discountData[$key]['customer_group_id']    = $this->security->xss_clean($customer_group_id[$key]);
                    $discountData[$key]['quantity']             = $this->security->xss_clean($quantityval);
                    $discountData[$key]['priority']             = $this->security->xss_clean($priority[$key]);
                    $discountData[$key]['price']                = $this->security->xss_clean($discountPrice[$key]);
                    $discountData[$key]['type']                 = $this->security->xss_clean($type[$key]);
                    $discountData[$key]['dateStart']            = $this->security->xss_clean($dateStart[$key]);
                    $discountData[$key]['dateEnd']              = $this->security->xss_clean($dateEnd[$key]);

                    $this->generalModel->insertValue('ec_product_discount', $discountData[$key]); // insert Filter data
                }
                $this->productModel->updateSearchText($productID, $this->language);
                $this->logUserActivity('Product discount Updated');
                $this->redirectValue = 'manage/product/edit/' . $productID . '/images';
            }
        }
    }

    protected function save_images($productID = 0) {
        $this->form_validation->set_rules('defField', 'defField', 'required');
        //echo "<pre>"; print_r($_FILES); exit;
        if($this->input->post('prodUploadedImage')==""){
            $this->form_validation->set_rules('productImage', 'Product Image', 'callback_productImage_check');
        }
        if ($this->form_validation->run($this))
        {
            if ($productID != 0) {
                $flagImg = 0;
                $errVal = 0;
                $_POST['image'] = $this->data['content']['image'];
                $_POST['bannerImage'] = $this->data['content']['bannerImage'];
                if ($_FILES['productImage']['size'] > 0) {
                   
                    $flagImg = 1;
                    $this->load->library('upload', $this->imageConfig);
                    if ($this->upload->do_upload('productImage')) {
                        $product['nonXss'] = array(
                            'image' => $this->upload->data('file_name'),
                        );
                        $productData['xssData'] = $this->security->xss_clean($product['nonXss']);
                        $result = $this->generalModel->updateTableValues('ec_product', 'productID=' . $productID, $productData['xssData']);
                        // in case you need to save it into a database
                    } else {
                        $this->session->set_flashdata('imgErr', $this->upload->display_errors());
                        $errVal = 1;
                    }
                }

                if (@$_FILES['bannerImage']['size'] > 0) {

                    if ($flagImg == 1)
                        $this->upload->initialize($this->bannerConfig);
                    else {
                        $flagImg = 1;
                        $this->load->library('upload', $this->bannerConfig);
                    }

                    if ($this->upload->do_upload('bannerImage')) {
                        $product['nonXss'] = array(
                            'bannerImage' => $this->upload->data('file_name'),
                        );
                        $productData['xssData'] = $this->security->xss_clean($product['nonXss']);
                        $result = $this->generalModel->updateTableValues('ec_product', 'productID=' . $productID, $productData['xssData']);
                        // in case you need to save it into a database
                    } else {
                        $this->session->set_flashdata('bannerImgErr', $this->upload->display_errors());
                        $errVal = 1;
                    }
                }

                $_POST['additionalImage'] = array(); //Set null array for additional image
                if (!empty($_FILES['file']['size'])) {
                    if ($flagImg == 1)
                        $this->upload->initialize($this->imageConfig);
                    else {
                        $flagImg = 1;
                        $this->load->library('upload', $this->imageConfig);
                    }
                    $errorArray = array();
                    foreach ($_FILES['file']['name'] as $key => $addImage) {
                        if ($_FILES['file']['size'][$key] > 0) {
                            $_FILES['addImg']['name'] = $addImage;
                            $_FILES['addImg']['type'] = $_FILES['file']['type'][$key];
                            $_FILES['addImg']['tmp_name'] = $_FILES['file']['tmp_name'][$key];
                            $_FILES['addImg']['error'] = $_FILES['file']['error'][$key];
                            $_FILES['addImg']['size'] = $_FILES['file']['size'][$key];
                            if ($this->upload->do_upload('addImg')) {
                                $_POST['additionalImage'][$key] = $this->upload->data('file_name');
                                // in case you need to save it into a database
                            } else {
                                $errorArray[$key] = $this->upload->display_errors();
                                $errVal = 1;
                            }
                        }
                    }
                    $this->session->set_flashdata('additionalImgErr', $errorArray);
                }

                if (!empty($_POST['additionalImageEdit'])) {
                    if (is_countable($_POST['additionalImage'])) {
                        $i = count($_POST['additionalImage']) + 1;
                    } else {
                        $i = 0;
                    }
                    foreach ($_POST['additionalImageEdit'] as $key => $additionalImageEdit) {
                        $_POST['additionalImage'][$i] = $additionalImageEdit;
                        $i++;
                    }
                }

                $addImg = array();

                $this->generalModel->deleteTableValues('ec_product_image', 'productID=' . $productID);
                if (!empty($_POST['additionalImage'])) {
                    foreach ($_POST['additionalImage'] as $key => $additionalImage) {
                        $addImg[$key]['productID'] = $this->security->xss_clean($productID);
                        $addImg[$key]['image'] = $this->security->xss_clean($additionalImage);
                        $addImg[$key]['sortOrder'] = $this->security->xss_clean($key + 1);

                        $this->generalModel->insertValue('ec_product_image', $addImg[$key]); // insert Filter data
                    }
                }
                if ($errVal == 0) {
                    $this->session->set_flashdata('success', "product updated successfully");
                    $this->productModel->updateSearchText($productID, $this->language);
                    $this->logUserActivity('Product Images Updated');
                    $this->redirectValue = 'manage/product/';
                } else {
                    $this->isValidationError = true;
                }
            }
        }
    }
    
    function productImage_check(){
        $this->form_validation->set_message('productImage_check', 'Please select product image.');
        if (empty($_FILES['productImage']['name'])) {
            return false;
        }else{
            return true;
        }
    }
    
    function check_quentity($fieldVal,$productID) 
    { 
        $productQuentity    =   $this->generalModel->getFieldValue('quantity', 'ec_product', 'productID='.$productID);
        if ($productQuentity < $fieldVal) 
        { 
            $this->form_validation->set_message('check_quentity', 'Discount quantity must be grater than product quantity'); 
            return false; 
        }
         else
             { 
             return true; 
             
             } 
    }
    
    
    function bundelProductCheck(){
        $quantity   =   $this->input->post('quantity');
        $i  =   0;
        $place  =   0;
        $outArray   =   array();
        foreach($this->input->post('bundleProductID') as $key=>$bundleProduct){
            $totalRws = $this->generalModel->getTableValue('quantity', 'ec_product', 'productID='.$bundleProduct, FALSE); //data count for pagination
            if($bundleProduct==""){
                $outArray[$i]['place']  =   $place;
                $outArray[$i]['message']  =   "Bundle Product field is required.";
                $i++;
            } elseif($quantity[$key]==""){
                $outArray[$i]['place']  =   $place;
                $outArray[$i]['message']  =   "Quantity field is required.";
                $i++;
            }else{
                if($totalRws['quantity']<$quantity[$key]){
                    $outArray[$i]['place']  =   $place;
                    $outArray[$i]['message']  =   "Only ".$totalRws['quantity']." stock available";
                    $i++;
                }
            }
           
            $place++;
        }
        echo json_encode($outArray);
 
    }
    
    public function getOptionValue(){
        $optionValue    =   array();
        $combinationID  =   $this->input->post('combinationID');
        $count          =   $this->input->post('count');
        $optionCount    =   $this->input->post('optionCount');
        $optionsCombination        =   $this->generalModel->getTableValue('optionID ', 'ec_option_combination', 'combinationID='.$combinationID, FALSE); 
        if($optionsCombination){
            $optionsCombination    =   explode(",",$optionsCombination['optionID']);
            //echo "<pre>"; print_r($optionsCombination); exit;
            foreach($optionsCombination as $key=>$optionID){
               
                $optionDetails  =   $this->optionModel->selectActiveOptionByID($optionID,$this->language);

                $optionValue[$key]['optionID']      =  $optionDetails['optionID'];
                $optionValue[$key]['optionName']    =  $optionDetails['name'];
                
                $optionValue[$key]['optionValue']    =   $this->optionModel->selectOptionValueByOptionID($optionID,$this->language);
            }
        }
      
            
        $this->data['optionValue']      = $optionValue;
        $this->data['optionID']         = $optionID;
        $this->data['count']            = $count;
        $this->data['optionCount']      = $optionCount;
        $this->render('manage/product/loadOptionValue');
    }
    
    public function saveProductOptionCombination(){ 
    $this->form_validation->set_rules('optionID[]', 'Option', 'required|trim|xss_clean');
    $this->form_validation->set_rules('required', 'Required', 'required');
       if ($this->form_validation->run() === TRUE) {
           
            if(count($this->input->post('optionID')) != count(array_unique($this->input->post('optionID')))){
                    $arrays = array(
                        'error'   => true,
                        'optionValue' => 'Same option select as multiple',
                       );
		   echo json_encode($arrays);
		   exit;
            }
           
            $optionID   =   implode(",",$this->input->post('optionID'));
            $productCombination['nonXss']    = array(
                'optionID'              => $optionID,
                'productID'              => $this->input->post('productID'),
                'required'              => $this->input->post('required'),
            );
            

            $productCombination['xssData'] = $this->security->xss_clean($productCombination['nonXss']);
            $combinationID = $this->generalModel->insertValue('ec_option_combination', $productCombination['xssData']); // Insert Product Option
            if($combinationID){
                $arrays = array(
                    'error'   => false,
		    'action'   => 'success',
		   );   
            }else{
                 $arrays = array(
                     'error'   => false,
		    'action'   => 'failed',
		   );
            }
            echo json_encode($arrays);
		   exit;
           
         }else{ 
                $arrays = array(
		    'error'   => true,
		    'optionValue' => form_error('optionID[]'),
		    'required'=> form_error('required')
		   );
		   echo json_encode($arrays);
		   exit;
         }  
           
    }
    
    public function saveProductOptionValue(){
        //echo "<pre>"; print_r($this->input->post()); exit; |callback_optionCombinationExist
        $this->form_validation->set_rules('optionValueID[]', 'Option Value', 'required|callback_optionCombinationExist|trim|xss_clean');
        //$this->form_validation->set_rules('priceType', 'Price Type', 'required|trim|xss_clean');
        //$this->form_validation->set_rules('price', 'Price', 'required|numeric|callback_productPriceCheck|trim|xss_clean');
        $this->form_validation->set_rules('price', 'Price', 'required|greater_than[0]|numeric|trim|xss_clean');
        $this->form_validation->set_rules('quantity', 'Quantity', 'required|greater_than[0]|callback_quentityCheck|trim|xss_clean');
        $this->form_validation->set_message('optionCombinationExist', 'This option already exist for this product');
        $this->form_validation->set_message('quentityCheck', 'Unable to add,quantity greater than product quantity');
        $this->form_validation->set_message('productPriceCheck', 'Unable to add, Price greater than product price or less than or equal to 0');
        //xit;
        //echo "<pre>"; print_r($this->input->post('optionValueID')); exit;
       if ($this->form_validation->run() === TRUE) {  
            $productOption['nonXss']    = array(
                'productID'                 => $this->input->post('productID'),
                'combinationOptionID'       => $this->input->post('combinationOptionID'),
                //'priceType'                 => $this->input->post('priceType'),
                'price'                     => $this->input->post('price'),
                'quantity'                  => $this->input->post('quantity'),
            );
            

            $productOption['xssData'] = $this->security->xss_clean($productOption['nonXss']);
            $productOptionID = $this->generalModel->insertValue('ec_product_option', $productOption['xssData']); // Insert Product Option
            
            if($this->input->post('optionValueID')){
                $optionValueID  =   $this->input->post('optionValueID');
                $optionID       =   $this->input->post('optionID');
                
                foreach($optionValueID  as $key=>$optionValueData){
                     $productOptionValue['nonXss']    = array(
                        'productOptionID'           => $productOptionID,
                        'productID'                 => $this->input->post('productID'),
                        'optionID'                  => $optionID[$key],
                        'optionValueID'             => $optionValueData,
                    );


                    $productOptionValue['xssData'] = $this->security->xss_clean($productOptionValue['nonXss']);
                    $this->generalModel->insertValue('ec_product_option_value', $productOptionValue['xssData']); // Insert Product Option
                }
            }
            
            if($productOptionID){
                $arrays = array(
                    'error'   => false,
		    'action'   => 'success',
		   );   
            }else{
                 $arrays = array(
                     'error'   => false,
		    'action'   => 'failed',
		   );
            }
            echo json_encode($arrays);
		   exit;
           
         }else{ 
               $arrays = array(
		    'error'   => true,
		    'optionValueID' => form_error('optionValueID[]'),
		    //'priceType'=> form_error('priceType'),
                    'price'=> form_error('price'),
                    'quantity'=> form_error('quantity')
		   );
		   echo json_encode($arrays);
		   exit;
         }  
    }
    
    public function optionCombinationExist(){
        $exist  =   0;
        $combinationID          =   $this->input->post('combinationOptionID');
        $productID              =   $this->input->post('productID');
        $optionID               =   $this->input->post('optionID');
        $optionValueID          =   $this->input->post('optionValueID');
        $checkExist =   $this->productModel->checkOptionValueExist($combinationID,$productID,$optionID,$optionValueID);
        foreach($checkExist as $checkExistData){
            $checkArray =   array_diff_assoc($checkExistData, $optionValueID);
            if(empty($checkArray)){
                $exist  =   1;
            }
        }
        //echo $exist; exit;
        if($exist==1){
            return FALSE;
        }else{
            return TRUE;
        }
    }
    
    public function quentityCheck(){
        $productID               =   $this->input->post('productID');
        //echo $productID;
        $quantity                =   $this->input->post('quantity') ? (int) $this->input->post('quantity') : 0 ;
        $optionQuentityVal       =   $this->productModel->getOptionquentiy($productID);
        if(@$optionQuentityVal['quantity']){ 
            $optionQuentityVals = $optionQuentityVal['quantity'];
        }else{
            $optionQuentityVals = 0;
        }
        
            $optionQuentity          =    $optionQuentityVals+$quantity;

            $productQuentity    =   $this->generalModel->getTableValue('quantity', 'ec_product', 'productID='.$productID.' AND status!="Deleted"', FALSE);
            $productQuentity    =   $productQuentity['quantity']; 
            //echo $productQuentity;exit;
            if($optionQuentity>$productQuentity){
                return FALSE;
            }
        
        
         return TRUE;
    }
    public function productPriceCheck(){
        $productID               =   $this->input->post('productID');
        $price                =   $this->input->post('price') ? $this->input->post('price') : 0 ;
        $productPrice    =   $this->generalModel->getTableValue('price', 'ec_product', 'productID='.$productID.' AND status="Active"', FALSE);
        //echo $price; echo $productPrice['price'];
            if($price>$productPrice['price'] || $price<=0){ 
                return FALSE;
            }
        
        
         return TRUE;
    }

    public function loadEditProductOptions(){
        $productOptionID    =    $this->input->get('productOptionID');

        $optionCombitnation   =     $this->optionModel->selectOptionCombination('');
        if($optionCombitnation){
            foreach($optionCombitnation as $key=>$combinationVal){
                $combinationOptionID    = explode(",",$combinationVal['combinationOptionID']);
                $optionName =   $this->optionModel->selectOptionCombinationName($combinationOptionID,$this->language);
                $optionCombitnation[$key]['optionName']    =   $optionName['optionName'];
            }
        }
        
//        ,$this->language
        $productOptions         =   $this->productModel->getProductOptionByID($productOptionID);
        $productOptionValues    =   $this->productModel->getProductOptionValueByProducID($productOptionID);
        $combinationID          =   $productOptions['combinationOptionID'];
        $optionsCombination     =   $this->generalModel->getTableValue('optionID ', 'ec_option_combination', 'combinationID='.$combinationID, FALSE); 
        if($optionsCombination){
            $optionsCombination    =   explode(",",$optionsCombination['optionID']);
            //echo "<pre>"; print_r($optionsCombination); exit;
            foreach($optionsCombination as $key=>$optionID){
               
                $optionDetails  =   $this->optionModel->selectActiveOptionByID($optionID,$this->language);

                $optionValue[$key]['optionID']      =  $optionDetails['optionID'];
                $optionValue[$key]['optionName']    =  $optionDetails['name'];
                
                $optionValue[$key]['optionValue']    =   $this->optionModel->selectOptionValueByOptionID($optionID,$this->language);
            }
        }
        //echo "<pre>"; print_r($productOptions);   echo "</pre>"; 
        $this->data['productOptionsSaved']          =   $productOptions;
        $this->data['productOptionValuesSaved']     =   $productOptionValues;
        $this->data['optionsList']                  =   $optionCombitnation;
        $this->data['optionValue']                  =   $optionValue;
        
        $this->render('manage/product/loadOptionEdit');
        
    }
    
    public function deleteProductOptions(){
        $productID    =    $this->input->post('productID');
        $combinationID   =    $this->input->post('combinationID');
        $data['status'] = 'Deleted';
        $this->generalModel->updateTableValues('ec_option_combination', 'productID=' . $productID .' AND combinationID=' . $combinationID, $data);
        $this->generalModel->updateTableValues('ec_product_option', 'productID=' . $productID .' AND combinationOptionID=' . $combinationID, $data);
    }
    
    public function deleteProductOptionValue(){
        $productOptionID    =    $this->input->post('productOptionID');
        $data['status'] = 'Deleted';
        $this->generalModel->updateTableValues('ec_product_option', 'productOptionID=' . $productOptionID, $data);
    }
    
    
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usergroup extends Admin_Controller {

    private $imageConfig = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('UsergroupModel', 'usergroupModel');
//        $imageDimension = $this->getImageDimension('home_usergroup_slider_dimen');
//        $imageSet = implode(' x ', $imageDimension);
//        $this->data['imageDimension'] = $imageDimension;
//        $this->data['imageSet'] = $imageSet;
//        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
//        $this->data['allowedType'] = $this->allowedImageTypes;
//        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
//        $this->imageConfig = array(
//            'upload_path' => "./uploads/usergroup/",
//            'allowed_types' => $this->allowedImageTypes,
//            'max_size' => $this->maxImageUploadSize,
//            'max_width' => $imageDimension[0],
//            'max_height' => $imageDimension[1],
//            'min_width' => $imageDimension[0],
//            'min_height' => $imageDimension[1],
//            'encrypt_name' => TRUE,
//        );
    }

    public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_user_group', 'userGroupID IN (' . $id . ')', $status);
            }
        }
        $this->data['breadcrumbs'] = "All Usergroup Page";
        $this->data['pageTitle'] = "All Usergroup Page";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(userGroupID) as tblCount', 'ec_user_group', 'userGroupID!=1 AND status!="delete"', FALSE); //data count for pagination
        
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Usergroup/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'name' => $this->input->get('name'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );

        $this->data['result'] = $this->usergroupModel->selectAll($params);

        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/usergroup/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Usergroup";
        $this->data['pageTitle'] = "Add Usergroup";

        //form validation 
        $this->form_validation->set_rules('name', 'Usergroup Name', 'required|xss_clean');
     

        if ($this->form_validation->run() === TRUE) {

            $resData['nonXss'] = array(
                'name' => $this->input->post('name'),
                
                'status' => $this->input->post('status')
            );
           
           
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_user_group', $resData['xssData']);

            if ($result) {
                
                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Usergroup Added');

                    redirect('manage/usergroup/index');

            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/usergroup/index');
            }
        }
        $this->render('manage/usergroup/Add');
    }

    public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Usergroup";
        $this->data['pageTitle'] = "Edit Usergroup";

        $userGroupID = $this->uri->segment(4);

        //form validation 
       $this->form_validation->set_rules('name', 'Usergroup Name', 'required|xss_clean');
        
        //image upload
        if ($this->form_validation->run() === TRUE) {

            $resData['nonXss'] = array(
                'name' => $this->input->post('name'),
                
                'status' => $this->input->post('status')
            );

            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_user_group', 'userGroupID=' . $userGroupID, $resData['xssData']);

            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Usergroup Updated');

                    redirect('manage/usergroup/index');

            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/usergroup/index');
            }
        }

        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_user_group', 'userGroupID=' . "$userGroupID" . ' AND status!="Deleted"', FALSE); //data for edit
        

        $this->render('manage/usergroup/Add');
    }

    public function changeStatus() {

        $userGroupID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_user_group', 'userGroupID=' . $userGroupID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }

    

}

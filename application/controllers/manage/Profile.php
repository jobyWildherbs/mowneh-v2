<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('UserLogin');
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'gm');
        $this->load->model('UserModel', 'userModel');
        $imageDimension = $this->getImageDimension('user_profile_dimen');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
        $this->imageConfig = array(
            'upload_path' => "./uploads/adminUser/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );
    }

    public function index() {
        //$data = array();
        $this->data['breadcrumbs'] = "Update User Profile";
        $this->data['pageTitle'] = "Update User Profile";

        $userID = $this->session->userdata('adminID');
        $this->data['action'] = base_url('manage/profile/');
        $this->form_validation->set_rules('firstname', 'First Name', 'required');

        if ($this->form_validation->run() === TRUE) {
            //echo "<pre>"; print_r($_FILES);exit;
            $errVal = 0;
            if ($_FILES['image']['size'] > 0) {

              
                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {

                    $this->data['imgErr'] = $this->upload->display_errors();
                    $errVal = 1;
                }
            }
            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'status' => $this->input->post('status')
            );
            if ($this->input->post('image')) {
                $resData['nonXss']['image'] = $this->input->post('image');
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $resData['xssData']['dateAdded'] = date('Y-m-d H:i');
            $resData['xssData']['ip'] = $this->input->ip_address();
            $result = $this->generalModel->updateTableValues('ec_user', 'userID=' . $userID, $resData['xssData']);

            if ($result) {
                if ($errVal == 0) {
                    $this->data['message'] = 'Data Updated Successfully';
                    $this->logUserActivity('Profile Updated');
                    redirect('manage/profile');
                }
                //$this->data['message'] = "Data added Updated";
            } else {
                $this->data['errMessage'] = 'Failed';
                //redirect('manage/profile');
            }
        }

        $this->data['content'] = $this->generalModel->getTableValue('*', 'ec_user', 'userID=' . "$userID" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->render('manage/profile/profile');
    }

    public function changePassword() {
        $this->data['breadcrumbs'] = "Change Password";
        $this->data['pageTitle'] = "Change Password";
        $userID = $this->session->userdata('adminID');
        $userEmail = $this->session->userdata('adminEmail');
        $this->data['action'] = base_url('manage/profile/changePassword/');
        if ($this->session->flashdata('success') != '') {
            $this->data['success'] = $this->session->flashdata('success');
        }
        if ($this->session->flashdata('failed') != '') {
            $this->data['failed'] = $this->session->flashdata('failed');
        }
        $this->form_validation->set_rules('password', 'New Password', 'required');
        $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'required|matches[password]');
        if ($this->form_validation->run() === TRUE) {
            $resData['nonXss'] = array(
                'password' => $this->input->post('password')
            );
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $passwordCreate = $this->userlogin->createCredentials($userEmail, $resData['xssData']['password']);
            $resData['xssData']['password'] = $passwordCreate['password'];
            $resData['xssData']['salt'] = $passwordCreate['salt'];

            $result = $this->gm->updateTableValues('ec_user', 'userID=' . $userID, $resData['xssData']);

            if ($result) {
                $this->session->set_flashdata('success', 'Password chnged Successfully');
                $this->logUserActivity('password changed');
                redirect('manage/profile/changePassword');
            } else {
                $this->session->set_flashdata('failed', 'Failed To Update');
                redirect('manage/profile/changePassword');
            }
        }
        $this->render('manage/profile/change_password');
    }

}

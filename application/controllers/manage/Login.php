<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('UserLogin');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->library('form_validation');
        $this->load->helper('common_helper');
        if ($this->userlogin->loggedIn()) {
            //redirect them to the login page
            redirect('manage/Home');
        }
    }

    public function index() {
        //$data   =   array();

        $data['title'] = 'Mowneh Admin | Login page';
        if ($this->input->post()) {

            $this->form_validation->set_rules('userName', 'User Name', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() === TRUE) {
                $resData['nonXss'] = array(
                    'userName' => $this->input->post('userName'),
                    'password' => $this->input->post('password')
                );
                $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

                $resultStatus = $this->userlogin->checkCredentials($resData['xssData']['userName'], $resData['xssData']['password']);

                if ($resultStatus['status'] == 1) {

                    // login atttempt table clear
                    $where = array('login' => $resData['xssData']['userName']);
                    $this->generalModel->deleteTableValues('ec_login_attempts', $where);
                    // login ip address update

                    $ipAddress = $this->input->ip_address();
                    $values = array('ip' => $ipAddress, 'lastLogin' => date('Y-m-d h:i:s'));
                    $where = array('userName' => $resData['xssData']['userName']);
                    $this->generalModel->updateTableValues('ec_user', $where, $values);
                    // admin Detials 
                    $admin = $this->generalModel->getTableValue('*', 'ec_user', array('userName' => $resData['xssData']['userName']));
                    $adminData = array(
                        'adminID' => $admin['userID'],
                        'adminUserName' => $admin['userName'],
                        'adminEmail' => $admin['email'],
                        'adminLastLogin' => $admin['lastLogin'],
                        'userGroupID' => $admin['userGroupID'],
                        'adminLoggedIn' => TRUE
                    );
                    $this->session->set_userdata($adminData);
                    $this->session->set_flashdata('message', $resultStatus['message']);

                    $data['message'] = $resultStatus['message'];
                    redirect('manage/Home', 'refresh');
                } else {
                    $this->session->set_flashdata('message', $resultStatus['message']);
                    $data['message'] = $resultStatus['message'];
                    $this->load->helper('form');
                    //redirect('siteadmin', 'refresh');
                }
            }
        }
        $data['favIcon'] = base_url('uploads/siteInfo/') . $this->getSettingValue('favicon');
        $data['logo'] = base_url('uploads/siteInfo/') . $this->getSettingValue('logo');
        $this->twig->display('manage/login/Login', $data);
    }

    public function resetPassword() {
        $data = array();

        if ($this->input->post('submitOtp')) {
            $email = $this->input->post('email');
            $emailVal = $this->security->xss_clean($email);
            if (empty($emailVal)) {
                $data['nullEmailMessage'] = " * please enter a valid email address";
            } else {
                $emailCheck = $this->generalModel->getTableValue('email,userId', 'ec_user', array('email' => $email));
                //echo "<pre>";print_r($emailCheck['userId']);
//            if(!empty($emailCheck)){
//                    $otp                          =   randomNumber(4);
//                    $resOtpData['otp']            =   $otp;
//                    $resOtpData['customerID']     =   $emailCheck['userId'];
//                    $resOtpData['otpType']        =   'adminUserAuthentication';
//                    $resOtpData['otpTime']        =   date("Y-m-d H:s:i");
//                    $resOtpData['addedDate']      =   date("Y-m-d H:s:i");
//                    $otpID   =   $this->generalModel->insertValue('ec_otp',$resOtpData);
//                    if($otpID){
//                         redirect(base_url()."manage/Login/activeOtp/".$otpID."/".$emailCheck[userId]);
//                    }
//                //$data['otpMessage'] =   "Your OTP is".$otp;
//            }else{
//                $data['otpMessage'] =   "No user Found..";
//            }
            }

            //Password send to email
            if (!empty($emailCheck)) {
                 $pwd = getRandomString(8);

                $from_email = "mooneh@gmail.com";
                $message ="Your Reset Password is " . $pwd  ;
                $salt = hash('sha256', sha1($pwd . SITE_SALT));
                $hashed_password = hash('sha256', sha1($salt . SITE_SALT . $pwd));
                //$result = array("salt" => $salt, "password" => $hashed_password);
                $this->generalModel->updateTableValues('ec_user', 'userID=' . $emailCheck['userId'], array("salt" => $salt, "password" => $hashed_password));
                //$message ="Your salt Password is " . $salt."and". $hashed_password ;
                $to_email = $this->input->post('email');
                //echo $pwd;exit;

                //Load email library 
                $this->load->library('email');

                $this->email->from($from_email, 'Mooneh');
                $this->email->to($to_email);
                $this->email->subject('Password Reset Email');
                $this->email->message($message);


                //Send mail 
                if ($this->email->send())
                    $data['success']="Email sent successfully.";
                else
                    $data['success']="Error in sending Email.";
            }
        }
        $this->twig->display('manage/login/forgotPass', $data);
    }

    public function checkemail() {
        echo "here iam working...";
    }

    public function logout() {
        $this->userlogin->logout();
        redirect('manage/');
    }

    public function activeOtp() {
        $data = array();
        $emailCheck['userId'] = '';
        $otpID = $this->uri->segment(4);
        $emailCheck['userId'] = $this->uri->segment(5);
        $otpDet = $this->generalModel->getTableValue('otpID,otp', 'ec_otp', 'otpID =' . "$otpID" . ' AND customerID=' . $emailCheck['userId'] . ' AND otpType="adminUserAuthentication" AND status="Active"');

//echo "<pre>";print_r($otpDet);exit;
        $data['otp'] = $otpDet['otp'];
        $data['otpMessage'] = "Your OTP is" . $data['otp'];
        $this->twig->display('manage/login/otpforgotPass', $data);
    }

    protected function getSettingValue($settingKey = '') {
        if ($settingKey != '') {
            $settingDetails = $this->generalModel->getTableValue('*', 'ec_settings', array('fieldKey' => $settingKey));
            if ($settingDetails) {
                if ($settingDetails['isLanguage'] == 'yes') {
                    $value = $this->generalModel->getFieldValue('*', 'ec_settings_field_details', array('settingsFieldID' => $settingDetails['id'], 'languageID' => $this->siteLanguageID));
                    return $value;
                } else {
                    return $settingDetails['value'];
                }
            }
        }
        return;
    }

}

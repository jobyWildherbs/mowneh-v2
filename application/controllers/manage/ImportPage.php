<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ImportPage extends Admin_Controller {

    public function __construct() {
         
        parent::__construct();
       
        $this->load->library('Import');
        $this->load->library('Export');
        $this->load->model('CategoryModel', 'categoryModel');
        $this->load->model('ProductModel', 'productModel');
        $this->load->model('ManufacturesModel', 'manufacturesModel');
        $this->load->model('OrderModel','orderModel');
        $this->load->model('CustomerModel','customerModel');
        $this->load->model('FilterModel','filterModel'); 
        $this->load->model('AttributeModel','attModel');
        
        $this->load->library('CustomerLogin');
        
        $this->csvConfig = array(
            'upload_path' => "./uploads/temp/importCsv/",
            'allowed_types' => 'text/plain|text/csv|csv',
            'encrypt_name' => TRUE,
        );
    }
    
    public function index(){
        
        $this->data['breadcrumbs']  = "Upload";
        $this->data['pageTitle']    = "Upload";
        
        $exportPage     =   $this->input->get('exportPage');
        $exportField    =   $this->import->tableFields($exportPage);
        
        $requiredField    =   $this->import->requiredField($exportPage);
       
        $this->data['exportField']      =   $exportField;
        $this->data['exportPage']       =   $exportPage;
        $this->data['requiredField']    =   $requiredField;
        
         $this->form_validation->set_rules('exportPage', 'Page', 'required');
//         $this->form_validation->set_rules('field[]', 'Fields', 'required');
         $this->form_validation->set_rules('csvFile', 'File', 'callback_file_check');

        if ($this->form_validation->run() === TRUE) {
        
            if( $this->input->post('sampleCsv')){
                if($exportPage!="product"){
                    $fileName = $this->input->post('exportPage').'.csv'; 
                    $pageName = $this->input->post('exportPage'); 
                    $field  =   $this->input->post('field[]');
                    $fields  =   '';
                    $count  =   1;
                    if($field){
                        foreach($field as $fieldVal){
                            $fields  .=  $fieldVal;
                            if(count($field)!=$count){
                                $fields  .=  ',';
                            }
                            $count++;

                        }
                        foreach($field as $key=>$fieldVal){
                            $selectedField[]      =   $fieldVal;
                        }
                    }
                        $params     =   array();
                        $params = array(
                        'fields' => $fields,
                        'start'  => $this->input->post('page'),
                        'limit'  => $this->input->post('limit'),
                        'languageID' => $this->language);




                        $fielsAdded =   $exportField;
                       $selectedField   =   array_keys($exportField);
                        foreach($selectedField as $key=>$selectedKey){
    //                        $additional =   "";
    //                        if($selectedKey=='categoryID' || $selectedKey=='relatedID' || $selectedKey=='filter' || $selectedKey=='additionalImage'){
    //                            $additional =   "(Multiple Value separate with ~ symbol)";
    //                        }
    //                        if($selectedKey=='attribute'){
    //                            $additional =   "(Multiple Value separate with ~ symbol and multiple language separate with * eg: attribute*valueEnglish*valuearabic)";
    //                        }
    //                        if($selectedKey=='filterName' || $selectedKey=='attributeName'){
    //                           $additional =   "(Multiple value separate with ~ symbol and  multiple language separate with * eg: Filter name English*Filter name Arabic)";
    //                        }
    //                        
    //                        if($selectedKey=='discDateStart' || $selectedKey=='discDateEnd' || $selectedKey=='dateAvailable'){
    //                            $additional =   "(YYY-MM-DD)";
    //                        }
                            //$header[]  =   $fielsAdded[$selectedKey].' '.$additional;
                            $header[]  =   $fielsAdded[$selectedKey];
                        }
                        $data   =   array();
                        $export =   $this->export->exportFile($fileName,$header,$data);
                        redirect($_SERVER['HTTP_REFERER']); 
                }
                else{
                    $this->load->helper('download');
                    $file   =   base_url('uploads/temp/productSample/product.csv');
                    $fileName   =   'product.csv';
                    $data = file_get_contents($file);
                    force_download($fileName, $data);
                }
            }
            
            if( $this->input->post('importFile')){
               $this->session->unset_userdata('errmsg');
                $field      =   $this->input->post('field[]');
                $pageName = $this->input->post('exportPage');
                if ($_FILES['csvFile']['size'] > 0) {
                    $this->load->library('upload', $this->csvConfig);
                    if ($this->upload->do_upload('csvFile')) {
                       
                        $csvFile = $this->upload->data('file_name');
                        // in case you need to save it into a database
                    } else {
                        $this->data['csvErr'] = $this->upload->display_errors();
                        $errVal = 1;
                        $csvFile = '';
                    }
                } else {
                    $this->data['csvErr'] = "No Image File Uploaded";
                    $errVal = 1;
                    $csvFile = '';
                }
                if($csvFile){
                     
                     $path='uploads/temp/importCsv/'.$csvFile;
                   //echo "<pre>"; print_r($data); echo "</pre>";
                    //echo "<pre>"; print_r($exportField); echo "</pre>";
                   //echo "<pre>"; print_r($data); echo "</pre>";
//                    if($pageName=='product'){
                        $content =   array();
                        $data   =   $this->import->parse_Productcsv($path);
                        $arrkey ="";
                        if($data['error']==1)
                        {
                            unset($data['error']);
                            $result = $data;  
                        }else{
                            foreach($data as $key=>$dataVal){
                                foreach($dataVal as $innerKey=>$val){
                                    $innerKey = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', trim($innerKey));
                                    if (in_array($innerKey, $exportField)) {

                                        $arrkey = array_search($innerKey, $exportField);
                                        $content[$key][$arrkey]= $val;
                                    }
                                }
                             }
                        $result  =   "";
                        //echo "<pre>"; print_r($content); exit;
                         if(!empty($content)){
                             $requiredField   =   $this->import->requiredField($pageName);

                            $rowCount   =   2;
                            $valErrMsg  =   array();
                            $result     =   array();
                             foreach($content as $key=>$validation){
                                $validationFields  = array_keys($validation);
                                foreach($requiredField as $required){
                                    if (!in_array($required, $validationFields)) {
                                        $valErrMsg[]   =   $this->import->setValidationMessage($exportField[$required],$rowCount);
                                    } 
                                }
                                $rowCount++;
                            }
                            if(empty($valErrMsg)){
                                if($pageName=='category'){ // For category   
                                    $result    =   $this->categoryModel->importData($content,$this->language);  
                                }
                                else if($pageName=='product'){ // For Product

                                     $result = $this->productModel->importData($content,$this->language);    
                                }
                                else if($pageName=='brands'){ // For Brand
                                     $result = $this->manufacturesModel->importData($content,$this->language);    
                                }
                                else if($pageName=='order'){ // For Order
                                     $result = $this->orderModel->importData($content,$this->language);    
                                }
                                else if($pageName=='customers'){ // For customers

                                    foreach($content as $key=>$contentPass){
                                       if($contentPass['password']!="" && $contentPass['password']!="email"){
                                           $passwordCreate = $this->customerlogin->createCredentials($contentPass['password'],$contentPass['password']); 
                                           $content[$key]['password']   = $passwordCreate['password'];
                                           $content[$key]['salt']       = $passwordCreate['salt'];
                                        }
                                        else {
                                            $content[$key]['password']  =   "";
                                        }
                                    }

                                    $passwordCreate = $this->customerlogin->createCredentials($resData['xssData']['email'], $resData['xssData']['password']);
                                    $result = $this->customerModel->importData($content,$this->language);    
                                }

                                else if($pageName=='filter'){ // For Filter
                                     $result = $this->filterModel->importData($content,$this->language);    
                                } 

                                else if($pageName=='attribute'){ // For Attribute
                                     $result = $this->attModel->importData($content,$this->language);    
                                }
                            }else{
                                $result =   $valErrMsg;
                            }
                              unlink($path);
                         }
                     }
                     
                     if($result){
                         if($result==1)
                            $this->session->set_flashdata('message', 'Data Added Successfully');
                        else {
                            $this->session->set_flashdata('uploadErr', $result);
                            $this->session->set_userdata('errmsg',$result);
//                            $export =   $this->export->exportFile('csvErrorlog.csv','error',$result);
//                            redirect($_SERVER['HTTP_REFERER']); 
                            
                        }
                         redirect('manage/ImportPage/index');
                     }
                     else{
                          $this->session->set_flashdata('failMessage', 'Failed To Add');
                          redirect('manage/ImportPage/index');
                     }
                }
              
            }
        }
        
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        }
        elseif ($this->session->flashdata('uploadErr') != '') {
            $this->data['uploadErr'] = $this->session->flashdata('uploadErr');
            $this->data['errlog']   = 1;
        }
        else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }
        $this->render('manage/import/Add');
    }

    public function downloadErrorLog(){
        $result =   $this->session->userdata('errmsg');
        $this->session->unset_userdata('errmsg');
        foreach($result as $key=>$resultData){
            $data[$key][0]  =   $resultData;
        }
        $header[]   =   'error';
        $export =   $this->export->exportFile('csvErrorlog.csv',$header,$data);
        $this->session->unset_userdata('errmsg');
        redirect($_SERVER['HTTP_REFERER']); 
    }
     /*
     * file value and type check during validation
     */
    public function file_check($str){
        if($this->input->post('importFile')){
            $allowed_mime_type_arr = array('text/x-csv','text/x-comma-separated-values');
            $mime = get_mime_by_extension($_FILES['csvFile']['name']);
            if(isset($_FILES['csvFile']['name']) && $_FILES['csvFile']['name']!=""){
                if(in_array($mime, $allowed_mime_type_arr)){
                    return true;
                }else{
                    $this->form_validation->set_message('file_check', 'Please select only CSV.');
                    return false;
                }
            }else{
                $this->form_validation->set_message('file_check', 'Please choose a CSV file to upload.');
                return false;
            }
        }
        else{
            return true;
        }
    }
}
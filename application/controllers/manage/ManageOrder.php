<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ManageOrder extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'gm');
        $this->load->model('AddressModel', 'am');
        $this->load->model('OrderModel', 'om');
        $this->load->model('OrderStatusModel', 'osm');
        $this->load->model('DeliveryStatusModel', 'dsm');
        $this->load->model('UserModel', 'user');
    }

    public function index() {
        $orderID = $this->uri->segment(4);
        $this->lang->load('site', 1);
        $this->data['breadcrumbs'] = "Manage Order";
        $this->data['pageTitle'] = "Manage Order";
        $params = array(
            'title' => $this->input->get('title'),
            'orderID' => $orderID,
            'languageID' => $this->language
        );
        
        $this->data['content'] = $this->om->getOrderDetails($params);
        $totalData  =   $this->om->orderTotal($orderID);
        $optionData  =   $this->om->orderOption($orderID);
        if(@$optionData){
            $this->data['content']['option'] = $optionData;
        }
        //print_r($optionData);
        foreach($totalData as $key=>$totData){
           
            $totalData[$key]['title']   =   lang($totData['languageKey']);
            $totalData[$key]['value']   =   $this->currency->format($totData['value'], $this->siteCurrency);
        }
       
        $this->data['content']['defAddress'] = $this->am->getAddress($this->data['content']['customerID'], $this->data['content']['addressID']);
        $products = $this->om->getOrderProducts($params);
        
        foreach($products as $key=>$productData){
            if($productData['type']=='Bundle'){
                $bundle_products    =   $this->gm->getTableValue('bundleID,productID,bundleProductID', 'ec_product_bundle', 'productID=' . $productData['productID'],TRUE);
                if($bundle_products){
                    foreach($bundle_products as $bunPro){
                        $bundleProductID    =   $bunPro['bundleProductID'];
                        $bundleProductData    =   $this->om->getBundleProducts($bundleProductID,$this->language);
                        $bundleProductData['quantity']  =   $productData['quantity']*$bundleProductData['quantity'];
                         $bundleProduct[]=$bundleProductData;
                    }
                    $products[$key]['bundleProducts']   =   $bundleProduct;
                }
                
               // echo "<pre>"; print_r($bundle_products);  echo "</pre>";
            }

        }
        //echo "<pre>"; print_r($products); exit;
        
        $this->data['content']['products']  =   $products;
        //echo "<pre>"; print_r($this->data['content']['products']); exit;
//        $this->data['content']['orderTotal']['subTotal'] = $this->currency->format($this->gm->getFieldValue('value', 'ec_order_total', 'orderID=' . $orderID . ' AND code="sub_total"'), $this->siteCurrency);
//        $this->data['content']['orderTotal']['total'] = $this->currency->format($this->gm->getFieldValue('value', 'ec_order_total', 'orderID=' . $orderID . ' AND code="total"'), $this->siteCurrency);
//        $this->data['content']['orderTotal']['discounts'] = $this->currency->format($this->gm->getFieldValue('value', 'ec_order_total', 'orderID=' . $orderID . ' AND code="discounts"'), $this->siteCurrency);
        $this->data['content']['orderTotal']   =   $totalData;
        foreach ($this->data['content']['products'] as $key => $item) {
            $this->data['content']['products'][$key]['price'] = $this->currency->format($item['price'], $this->siteCurrency);
            $this->data['content']['products'][$key]['total'] = $this->currency->format($item['total'], $this->siteCurrency);
        }
        
        $this->data['content']['status'] = $this->osm->selectActive($this->language);
        $this->data['content']['deliver'] = $this->dsm->selectActive($this->language);
        $this->data['content']['driverList'] = $this->user->selectActiveUser(5);
        $this->data['content']['total'] = $this->currency->format($this->data['content']['total'], $this->siteCurrency);
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }
        $this->render('manage/order/manage');
    }

      public function changeStatus() {
        $orderID = $this->input->post('orderID');
        $orderStatusID = $this->input->post('orderStatusID');
        $this->data['data']['orderStatusID'] = $orderStatusID;
        $this->logUserActivity($orderStatusID);
        $customerData = $this->gm->getTableValue('telephone,orderID,invoiceNo,total','ec_order', 'orderID=' . $orderID,FALSE);
        
        $result = $this->generalModel->updateTableValues('ec_order', 'orderID=' . $orderID, $this->data['data']);
        if($result){
            if($orderStatusID == 4){
                $phoneNo    =  "974".$customerData['telephone']; 
                       $smsMessage =   "Your order has been shipped.Your order id : ".$customerData['orderID']." ,invoice no : ".$customerData['invoiceNo'].", amount : ".$customerData['total'];
                        $this->sendSMS($smsMessage,$phoneNo);
                        }
             $response = array(
            'msg' => '<div class="alert alert-success">Status Updated Successfully</div> '
            );
        }else{
             $response = array(
                'msg' => '<div class="alert alert-danger">Failed To Update</div> '
            );
        }
       
        echo json_encode($response);
    }

    public function deliveryUpdate() {

        $this->form_validation->set_rules('update', 'Order/Product', 'required');
        $this->form_validation->set_rules('comment', 'Comment', 'required');
        $this->form_validation->set_rules('pickDate', 'Date', 'required');
        $this->form_validation->set_rules('pickTime', 'Time', 'required');
        $this->form_validation->set_rules('location', 'Location', 'required');

        if ($this->form_validation->run() === TRUE) {
            $update     = $this->input->post('update');
            if ($update == 'product') {
                $products = array();
                $products = $this->input->post('productID');

//                INSERT FOR EACH PRODUCT
                foreach ($products as $item) {
                    $deliveryStatus['nonXss'] = array(
                        'deliveryStatusID' => $this->input->post('deliveryStatus'),
                        'orderID' => $this->input->post('orderID'),
                        'comment' => $this->input->post('comment'),
                        'date' => date('Y-m-d', strtotime($this->input->post('pickDate'))),
                        'time' => $this->input->post('pickTime'),
                        'location' => $this->input->post('location'),
                        'orderProductID' => $item
                    );
                    $deliveryStatus['xssData']  = $this->security->xss_clean($deliveryStatus['nonXss']);
                    $result     = $this->gm->insertValue('ec_order_delivery',$deliveryStatus['xssData']);
                    $msg        = array(
                        'type'      => 'success',
                        'message'   => 'Data inserted uccessfully'
                    );
                }
                $this->session->set_flashdata('message', 'Data Added Successfully');
            } else {
                $deliveryStatus['nonXss'] = array(
                    'deliveryStatusID' => $this->input->post('deliveryStatus'),
                    'orderID' => $this->input->post('orderID'),
                    'comment' => $this->input->post('comment'),
                    'date' => date('Y-m-d', strtotime($this->input->post('pickDate'))),
                    'time' => $this->input->post('pickTime'),
                    'location' => $this->input->post('location'),
                    'orderProductID' => 0,
                );
                $deliveryStatus['xssData']  = $this->security->xss_clean($deliveryStatus['nonXss']);
                $result     = $this->gm->insertValue('ec_order_delivery',$deliveryStatus['xssData']);
                $msg        = array(
                        'type'      => 'success',
                        'message'   => 'Data inserted uccessfully'
                    );
                $this->session->set_flashdata('message', 'Data Added Successfully');
            }
        } else {
            $msg        = array(
                        'type'      => 'failed',
                        'message'   => 'Data insertion failed. Validation Errors'
                    );
            $this->session->set_flashdata('failMessage', 'Data insertion failed. Validation Errors');
        }
        echo json_encode($msg);
    }
    
    public function driverAssign() {
        $this->form_validation->set_rules('driverID', 'driverID', 'required');
       

        if ($this->form_validation->run() === TRUE) {
            $driverID   =   $this->input->post('driverID');
            $orderID   =   $this->input->post('orderID');
            $result     = $this->gm->getTableCount('ec_order_to_driver','orderID='.$orderID);
            $customerData = $this->gm->getTableValue('telephone,orderID,countryCode,invoiceNo,total','ec_order', 'orderID=' . $orderID,FALSE);
            if($result==0){
            $phoneNo    =  $customerData['countryCode'].$customerData['telephone']; 
                       $smsMessage =   "Your order has been picked up. Order id : ".$customerData['orderID'];
                        $this->sendSMS($smsMessage,$phoneNo);
                        //exit;
                 $driverContent['nonXss'] = array(
                    'driverID' => $this->input->post('driverID'),
                    'orderID' => $this->input->post('orderID'),
                    
                );
                $driverContent['xssData']  = $this->security->xss_clean($driverContent['nonXss']);
                $return     = $this->gm->insertValue('ec_order_to_driver',$driverContent['xssData']);
            }else{
                $driverContent['nonXss'] = array(
                    'driverID' => $this->input->post('driverID'),
                    
                );
                $driverContent['xssData']  = $this->security->xss_clean($driverContent['nonXss']);
                $return     = $this->gm->updateTableValues('ec_order_to_driver','orderID='.$orderID,$driverContent['xssData']);
            }
            if($return){
                 $msg        = array(
                      'type'      => 'success',
                        'message'   => 'Data inserted uccessfully'
                    );
                $this->session->set_flashdata('message', 'Data Added Successfully');
            }else{
                $msg        = array(
                        'type'      => 'failed',
                        'errMessage'   => 'Data insertion failed.'
                    );
                $this->session->set_flashdata('failMessage', 'Data insertion failed.');
            }
            
        } else {
            $msg        = array(
                        'type'      => 'failed',
                        'errMessage'   => 'Data insertion failed. Validation Errors'
                    );
            $this->session->set_flashdata('failMessage', 'Data insertion failed. Validation Errors');
        }
        echo json_encode($msg);
    }

}

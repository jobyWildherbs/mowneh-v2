<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Admin_Controller {

    protected $imgConfig;

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('SettingsModel', 'settingsModel');
    }

    public function index($settingsKey = "general") {
        
        $breadCrumb = '';
        switch ($settingsKey) {
            case "general" :
                $breadCrumb = "General settings";
                break;
            case "image":
                $breadCrumb = "Image settings";
                break;
            case "security":
                $breadCrumb = "Security settings";
                break;
            case "mail":
                $breadCrumb = "Mail settings";
                break;
            default :
                $breadCrumb = "Settings";
                break;
        }
        if ($this->session->flashdata('imgErr') != '') {
            //print_r($this->session->flashdata('imgErr'));exit;
            $this->data['imgErr'] = $this->session->flashdata('imgErr');
            $this->data['errVal'] = 1;
        }
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }
        $this->data['editorFields'] = array('address');
        $this->data['breadcrumbs'] = $breadCrumb;
        $this->data['settingsGroupKey'] = $settingsKey;
        $this->data['pageTitle'] = $breadCrumb;
        $this->data['action'] = base_url('manage/settings/') . $settingsKey;
        if($this->session->userdata('adminID')==1){
            $this->data['result'] = $this->settingsModel->select($settingsKey, $this->language,true);
            //echo "<pre>"; print_r($this->data['result']); exit;
        }
        else {
            $this->data['result'] = $this->settingsModel->select($settingsKey, $this->language,false);
            
        }
        $this->data['settingGroup'] = $this->generalModel->getTableValue('*', 'ec_settings_group', array('status' => 'enable'), true);
        $this->data['timeZone'] =   $this->getTimeZone();
        //echo "<pre>"; print_r($this->data['timeZone']); exit;
        if ($this->input->post()) {
            //print_r($this->input->post());
            foreach ($this->data['result'] as $key => $row) {
                if ($row['validation'] != '') {
                    $this->form_validation->set_rules($row['fieldKey'], $row['fieldName'], $row['validation']);
                }
            }
            if ($this->form_validation->run() === TRUE) {
                $POST = $this->input->post();
                $errVal = 0;
                $errArray = array();
                if ($_FILES) {
                    foreach ($_FILES as $key => $value) {
                        $settingInfo = $this->generalModel->getTableValue('*', 'ec_settings', array('fieldKey' => $key));
                        if ($key == 'logo') {
                            list($width, $height) = $this->getImageDimension('site_logo_dimen');
                            $this->imgConfig = array(
                                'upload_path' => "./uploads/siteInfo/",
                                'allowed_types' => $this->allowedImageTypes,
                                'max_width' => $width,
                                'min_width' => $width,
                                'min_height' => $height,
                                'max_height' => $height,
                                'max_size' => $this->maxImageUploadSize,
                                'encrypt_name' => TRUE,
                            );
                        }
                        $this->imgConfig = array(
                            'upload_path' => "./uploads/siteInfo/",
                            'allowed_types' => $this->allowedImageTypes,
                            'max_size' => $this->maxImageUploadSize,
                            'encrypt_name' => TRUE,
                        );
                        if ($settingInfo['fieldType'] == 'image' || $settingInfo['fieldType'] == 'file') {
                            if ($_FILES[$key]['size'] > 0) {

                                $this->load->library('upload', $this->imgConfig);
                                if ($this->upload->do_upload($key)) {
                                    $this->generalModel->updateTableValues('ec_settings', array('id' => $settingInfo['id']), array('value' => $this->upload->data('file_name')));
                                } else {

                                    $errArray[$key] = $this->upload->display_errors();
                                    $errVal = 1;
                                }
                            } else {
                                $errVal = 1;
                            }
                        }
                    }
                }
                if ($errVal == 1) {
                    //print_r($errArray);exit;
                    $this->session->set_flashdata('imgErr', $errArray);
                }

                foreach ($POST as $key => $value) {
                    $settingInfo = $this->generalModel->getTableValue('*', 'ec_settings', array('fieldKey' => $key));
                    if ($settingInfo['relatedParent'] != 0 && $settingInfo['relatedParent'] != '') {
                        //GET CURRENT VALUE OF PARENT
                        $parentValue = $this->generalModel->getFieldValue('value', 'ec_settings', array('id' => $settingInfo['relatedParent']));
                        //UPDATE VALUE FIELD
                        if ($settingInfo['isLanguage'] == 'yes') {
                            $settingLanguageInfo = $this->generalModel->getTableValue('*', 'ec_settings_field_details', array('settingsFieldID' => $settingInfo['id'], 'languageID' => $this->language, 'relatedParent' => $settingInfo['relatedParent'], 'relationValue' => $parentValue));
                            if ($settingLanguageInfo) {
                                $this->generalModel->updateTableValues('ec_settings_field_details', array('settingsFieldID' => $settingInfo['id'], 'languageID' => $this->language, 'relationValue' => $parentValue), array('value' => $value));
                            } else {
                                $array = array(
                                    'settingsFieldID' => $settingInfo['id'],
                                    'languageID' => $this->language,
                                    'value' => $value
                                );
                                $this->generalModel->insertValue('ec_settings_field_details', $array);
                            }
                        } else {
                            $this->generalModel->updateTableValues('ec_settings', array('id' => $settingInfo['id'], 'relatedParent' => $settingInfo['relatedParent'], 'relationValue' => $parentValue), array('value' => $value));
                        }
                    } elseif ($settingInfo['fieldType'] == 'image' || $settingInfo['fieldType'] == 'file') {
                        
                    } elseif ($settingInfo['fieldType'] == 'checkbox') {

                        $this->generalModel->updateTableValues('ec_settings', array('id' => $settingInfo['id']), array('value' => implode(",", $value)));
                    } else {

                        if ($settingInfo['isLanguage'] == 'yes') {
                            $settingLanguageInfo = $this->generalModel->getTableValue('*', 'ec_settings_field_details', array('settingsFieldID' => $settingInfo['id'], 'languageID' => $this->language));
                            if ($settingLanguageInfo) {
                                $this->generalModel->updateTableValues('ec_settings_field_details', array('settingsFieldID' => $settingInfo['id'], 'languageID' => $this->language), array('value' => $value));
                            } else {
                                $array = array(
                                    'settingsFieldID' => $settingInfo['id'],
                                    'languageID' => $this->language,
                                    'value' => $value
                                );
                                $this->generalModel->insertValue('ec_settings_field_details', $array);
                            }
                        } else {

                            $this->generalModel->updateTableValues('ec_settings', array('id' => $settingInfo['id']), array('value' => $value));
                        }
                    }
                }
                $this->logUserActivity($settingsKey . ' Settings Updated');
                $this->session->set_flashdata('message', 'Data Updated Successfully');
                $this->data['success'] = $this->session->flashdata('message');
                redirect('manage/settings/' . $settingsKey);
            } else {
                $this->session->set_flashdata('failMessage', 'Updation Failed..! Validation Errors.');
                $this->data['failMessage'] = $this->session->flashdata('failMessage');
            }
        }

        $this->render('manage/settings/settings');
    }

    public function _remap($method) {
        $param_offset = 2;

        if (!method_exists($this, $method)) {
            $param_offset = 1;
            $method = 'index';
        }
// Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
// Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function check_dimension($dimen) {
        $dimen = str_replace(" ", "", $dimen);
        if ($dimen != '') {
            if (preg_match("/^[0-9]+[ ]*[xX][ ]*[0-9]+$/", $dimen, $match)) {
                return TRUE;
            } else {
                $this->form_validation->set_message('check_dimension', 'Please use the format "[width] X [height]"');
                return FALSE;
            }
        } else {
            return TRUE;
        }
    }

    public function loadRelatedFields() {
        //FOR AJAX CALL FROM THE TWIG
        if ($this->input->post()) {
            $params['fieldKey'] = $this->input->post('key');
            $params['parentID'] = $this->input->post('id');
            $params['parentValue'] = $this->input->post('pvalue');
            $params['language'] = $this->language;
            $fields = $this->settingsModel->selectRelated($params);
//            echo "<pre>";print_r($fields);exit;
            $this->data['relatedFieldList'] = $fields;
            $this->render('manage/settings/ajaxRelatedFields');
        }
    }
     function getTimeZone(){
        $tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
       return $tzlist;
    }

}

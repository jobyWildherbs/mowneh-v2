<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminMenu extends Admin_Controller {

    

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('AdminMenuModel', 'adminMenuModel');
       
    }

    public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_menu', 'menuID IN (' . $id . ')', $status);
            }
        }
        $this->data['breadcrumbs'] = "All Admin Menu";
        $this->data['pageTitle'] = "All Admin Menu";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(menuID) as tblCount', 'ec_menu', ' status!="Deleted"', FALSE); //data count for pagination
        
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/AdminMenu/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'menuName' => $this->input->get('menuName'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );

        $this->data['result'] = $this->adminMenuModel->selectAll($params);

        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/adminmenu/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Admin Menu";
        $this->data['pageTitle'] = "Add Admin Menu";
$this->data['parentMenu'] = $this->adminMenuModel->selectMenu('', $this->language); // parent category List
        //form validation 
        $this->form_validation->set_rules('menuName', 'name', 'required|xss_clean');
       
        $this->form_validation->set_rules('sortOrder', 'sort order', 'required|numeric');

        if ($this->form_validation->run() === TRUE) {
            
            $resData['nonXss'] = array(
                'parent' => $this->input->post('parent'),
                'url' => $this->input->post('url'),
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status')
            );
           
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_menu', $resData['xssData']);

            if ($result) {
                $adminmenuDetail['nonXss'] = array(
                    'menuID' => $result,
                    'languageID' => $this->language,
                    'menuName' => $this->input->post('menuName')
                    
                );
                $adminmenuDetail['xssData'] = $this->security->xss_clean($adminmenuDetail['nonXss']);
                $this->generalModel->insertValue('ec_menu_detail', $adminmenuDetail['xssData']); //information Added

                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('AdminMenu Added');
               
                    redirect('manage/adminmenu/index');
                
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/adminmenu/index');
            }
        }
        $this->render('manage/adminmenu/Add');
    }

    public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Admin Menu";
        $this->data['pageTitle'] = "Edit Admin Menu";
$this->data['parentMenu'] = $this->adminMenuModel->selectMenu('', $this->language); // parent category List
        $id = $this->uri->segment(4);
//        $imgErr = $this->uri->segment(5);
//        if ($imgErr) {
//            $this->data['imgErr'] = "Invalid Image, Please try again";
//            $this->data['errVal'] = 1;
//        }
//        if ($this->session->flashdata('imgErr') != '') {
//            $this->data['imgErr'] = $this->session->flashdata('imgErr');
//            $this->data['errVal'] = 1;
//        }
        //form validation 
         $this->form_validation->set_rules('menuName', 'name', 'required|xss_clean');
       
        $this->form_validation->set_rules('sortOrder', 'sort order', 'required|numeric');
        //image upload
        if ($this->form_validation->run() === TRUE) {
//            $errVal = 0;
//            if ($_FILES['image']['size'] > 0) {
//
//                $this->load->library('upload', $this->imageConfig);
//                if ($this->upload->do_upload('image')) {
//                    $_POST['image'] = $this->upload->data('file_name');
//                    // in case you need to save it into a database
//                } else {
//                    $this->data['imgErr'] = $this->upload->display_errors();
//                    //$_POST['image'] = '';
//                    $errVal = 1;
//                }
//            }
//
//            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'parent' => $this->input->post('parent'),
                'url' => $this->input->post('url'),
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status')
            );
            //if ($errVal == 1) {
            //$resData['nonXss']['status'] = 'Inactive';
            //}
            if ($this->input->post('image')) {
                $resData['nonXss']['image'] = $this->input->post('image');
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_menu', 'menuID=' . $id, $resData['xssData']);
            $adminmenuDetail['nonXss'] = array(
                 'menuID' => $id,
                
                    'languageID' => $this->language,
                    'menuName' => $this->input->post('menuName')
            );

            $adminmenuDetail['xssData'] = $this->security->xss_clean($adminmenuDetail['nonXss']);
            $checkAdminMenuDetail = $this->generalModel->getTableValue('menuID', 'ec_menu_detail', 'menuID=' . $id . ' AND languageID=' . $this->language, FALSE);
            if ($checkAdminMenuDetail) {
                $this->generalModel->updateTableValues('ec_menu_detail', 'menuID=' . $id . ' AND languageID=' . $this->language, $adminmenuDetail['xssData']); //information Added
            } else {
                $this->generalModel->insertValue('ec_menu_detail', $adminmenuDetail['xssData']); // insert detail data
            }
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('AdminMenu Updated');
                
                    redirect('manage/adminmenu/index');
             
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/adminmenu/index');
            }
        }

        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_menu', 'menuID=' . "$id" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->data['content']['detail'] = $this->generalModel->getTableValue('*', 'ec_menu_detail', 'menuID=' . "$id" . ' AND languageID=' . $this->language, FALSE); //data for edit

        $this->render('manage/adminmenu/Add');
    }

    public function changeStatus() {

        $id = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_menu', 'menuID=' . $id, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function valid_url_format($str) {
        //echo $str."======test";exit;
        if (empty($str)) {
            $this->form_validation->set_message('valid_url_format', 'The {field} field can not be empty');
            return FALSE;
        } elseif (preg_match('/^(?:([^:]*)\:)?\/\/(.+)$/', $str, $matches)) {
            if (empty($matches[2])) {
                $this->form_validation->set_message('valid_url_format', 'Invalid url');
                return FALSE;
            } elseif (!in_array(strtolower($matches[1]), array('http', 'https'), TRUE)) {
                $this->form_validation->set_message('valid_url_format', 'Invalid url');
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $this->form_validation->set_message('valid_url_format', 'Invalid url');
            return FALSE;
        }
    }

}

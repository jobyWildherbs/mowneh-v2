<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('UserLogin');
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('UserModel', 'userModel');
        $imageDimension = $this->getImageDimension('user_profile_dimen');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
        $this->imageConfig = array(
            'upload_path' => "./uploads/adminUser/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );
    }

    public function index() {
        //$this->data = array();

        if ($this->input->post('action')) {
            $id = implode(",", $this->input->post('multyCheck'));
            $status['status'] = $this->input->post('action');
            $this->generalModel->updateTableValues('ec_user', 'userID IN (' . $id . ')', $status);
        }

        $this->data['breadcrumbs'] = "All User";
        $this->data['pageTitle'] = "All User";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(userID) as tblCount', 'ec_user', 'userID!=1 AND status!="Deleted"', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/User/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;      

        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $this->data['currentPage'] = $page + 1;
        $params = array(
            'email' => $this->input->get('email'),
            'limit' => $config['per_page'],
            'start' => $page);

        $this->data['result'] = $this->userModel->selectAll($params);
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/user/list');
    }

    public function add() {


        $this->data['breadcrumbs'] = "Add User";
        $this->data['pageTitle'] = "Add User";
        //form validation 

        $this->form_validation->set_rules('userName', 'User Name', 'trim|required|xss_clean|is_unique[ec_user.userName]');
        $this->form_validation->set_rules('userGroupID', 'User Group', 'required');
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|is_unique[ec_user.email]');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('cPassword', 'Confirm Password', 'required|matches[password]');
        $this->form_validation->set_rules('images', 'Image', 'required|xss_clean');
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            if ($_FILES['image']['size'] > 0) {


                $this->load->library('upload', $this->imageConfig);

                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    $errVal = 1;
                    $_POST['image'] = '';
                }
            }
            $this->session->set_flashdata('imgErr', $this->data['imgErr']);
            $this->data['errVal'] = $errVal;


            $resData['nonXss'] = array(
                'userName' => $this->input->post('userName'),
                'userGroupID' => $this->input->post('userGroupID'),
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'image' => $this->input->post('image'),
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password'),
                'status' => $this->input->post('status')
            );
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $passwordCreate = $this->userlogin->createCredentials($resData['xssData']['email'], $resData['xssData']['password']);
            $resData['xssData']['password'] = $passwordCreate['password'];
            $resData['xssData']['salt'] = $passwordCreate['salt'];
            $resData['xssData']['dateAdded'] = date('Y-m-d H:i');
            $resData['xssData']['ip'] = $this->input->ip_address();
            $result = $this->generalModel->insertValue('ec_user', $resData['xssData']);
            if ($result) {
                $this->session->set_flashdata('message', 'Data Added Successfully');

                if ($errVal == 0) {
                    $this->logUserActivity('User Added');
                    redirect('manage/user/index');
                } else {
                    redirect('manage/user/edit/' . $result . '/imgErr');
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/information/index');
            }
        }
        $this->data['roles'] = $this->generalModel->getTableValue('*', 'ec_user_group', array('userGroupID!=' => 1), true);

        $this->render('manage/user/Add');
    }

    public function edit() {
        //$data = array();
        $this->data['breadcrumbs'] = "Edit User";
        $this->data['pageTitle'] = "Edit User";

        $userID = $this->uri->segment(4);
        $imgErr = $this->uri->segment(5);

        $this->form_validation->set_rules('userName', 'User Name', 'required');
        $this->form_validation->set_rules('userGroupID', 'User Group', 'required');
        $this->form_validation->set_rules('firstname', 'First Name', 'required');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        if ($this->input->post('password')) {

            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('cPassword', 'Password', 'required|matches[password]');
        }
        $this->form_validation->set_rules('images', 'Image', 'required|xss_clean');
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            if ($_FILES['image']['size'] > 0) {

                $this->load->library('upload', $this->imageConfig);

                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    $errVal = 1;
                }
            }
            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'userGroupID' => $this->input->post('userGroupID'),
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'status' => $this->input->post('status')
            );
            if ($this->input->post('password')) {
                $resData['nonXss']['password'] = $this->input->post('password');
            }
            if ($this->input->post('image')) {
                $resData['nonXss']['image'] = $this->input->post('image');
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $xssEmail = $this->input->post('email');
            if ($this->input->post('password')) {
                $passwordCreate = $this->userlogin->createCredentials($xssEmail, $resData['xssData']['password']);
                $resData['xssData']['password'] = $passwordCreate['password'];
                $resData['xssData']['salt'] = $passwordCreate['salt'];
            }
            $resData['xssData']['dateAdded'] = date('Y-m-d H:i');
            $resData['xssData']['ip'] = $this->input->ip_address();
            $result = $this->generalModel->updateTableValues('ec_user', 'userID=' . $userID, $resData['xssData']);


            $resData['nonXss'] = array(
                'userGroupID' => $this->input->post('userGroupID'),
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'status' => $this->input->post('status')
            );
            if ($this->input->post('password')) {
                $resData['nonXss']['password'] = $this->input->post('password');
            }
            if ($this->input->post('image')) {
                $resData['nonXss']['image'] = $this->input->post('image');
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $xssEmail = $this->input->post('email');
            if ($this->input->post('password')) {
                $passwordCreate = $this->userlogin->createCredentials($xssEmail, $resData['xssData']['password']);
                $resData['xssData']['password'] = $passwordCreate['password'];
                $resData['xssData']['salt'] = $passwordCreate['salt'];
            }
            $resData['xssData']['dateAdded'] = date('Y-m-d H:i');
            $resData['xssData']['ip'] = $this->input->ip_address();
            $result = $this->generalModel->updateTableValues('ec_user', 'userID=' . $userID, $resData['xssData']);

            if ($result) {
                $this->session->set_flashdata('message', 'Data Updated Successfully');
                if ($errVal == '0')
                    $this->logUserActivity('User Updated');
                redirect('manage/User/index');
//                $this->data['message'] = "Data Updated Successfully";
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                //$this->data['message'] = "Failed";
                //redirect('manage/User/index');
            }
        }
        $this->data['roles'] = $this->generalModel->getTableValue('*', 'ec_user_group', array('userGroupID!=' => 1), true);
        $this->data['content'] = $this->generalModel->getTableValue('*', 'ec_user', 'userID=' . "$userID" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->render('manage/user/edit');
    }

    public function changeStatus() {

        $userID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->logUserActivity('User status changed to - '.$status);
        $data['status'] = $status;
        $result = $this->generalModel->updateTableValues('ec_user', 'userID=' . $userID, $data);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function logout() {
        $this->userlogin->logout();
        $this->logUserActivity('User LoggedOut');
        redirect('manage/');
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TimeSlote extends Admin_Controller {

    //private $languageID;

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('TimeSloteModel', 'timeSloteModelModel');
        //$this->languageID = 1;
    }

    public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
              
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_delevery_time_slot', 'slotID IN (' . $id . ')', $status);
            }
        }
        $this->data['breadcrumbs'] = "All Time Slot";
        $this->data['pageTitle'] = "All Time Slot";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(slotID) as tblCount', 'ec_delevery_time_slot', '', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/TimeSlote/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

       
        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
         $params = array(
            'limit' => $config['per_page'],
            'start' => $page);

        $this->data['result'] = $this->timeSloteModelModel->selectAll($params);
        //echo "<pre>"; print_r($this->data['result']); exit;
         if(@$_GET['msg']){
            if($_GET['msg'] == 'active'){
             $this->data['success'] = "Time Slot activated Successfully";
            }else if($_GET['msg'] == 'inactive'){
             $this->data['success'] = "Time Slot inactivated Successfully";
            }else if($_GET['msg'] == 'delete'){
             $this->data['success'] = "Time Slot deleted Successfully";
            }
        }
        if($this->session->flashdata('message')!= ''){
        $this->data['success']     =  $this->session->flashdata('message');
        }else{
        //echo $this->data['success'];
        $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }
        $this->render('manage/timeSlote/list');
    }

    public function add() {
        
        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Time Slot";
        $this->data['pageTitle'] = "Add Time Slot";
        // form validation 
        $this->form_validation->set_rules('fromTime', 'From Time', 'required');
        $this->form_validation->set_rules('toTime', 'To Time', 'required');



        if ($this->form_validation->run() === TRUE) {
            $resData['nonXss'] = array(
                'fromTime' => $this->input->post('fromTime'),
                'toTime' => $this->input->post('toTime'),
                'status' => $this->input->post('status')
            );
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $result = $this->generalModel->insertValue('ec_delevery_time_slot', $resData['xssData']);


            if ($result) {

                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Warehouse Added');
                redirect('manage/timeSlote/index');
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/timeSlote/index');
            }
        }
        $this->render('manage/timeSlote/Add');
    }

    public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Time Slot";
        $this->data['pageTitle'] = "Edit Time Slot";
        $slotID = $this->uri->segment(4);
        $this->form_validation->set_rules('fromTime', 'From Time', 'required');
        $this->form_validation->set_rules('toTime', 'To Time', 'required');
        //image upload
        if ($this->form_validation->run() === TRUE) {


            $resData['nonXss'] = array(
                'fromTime' => $this->input->post('fromTime'),
                'toTime' => $this->input->post('toTime'),
                'status' => $this->input->post('status')
            );


            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $result = $this->generalModel->updateTableValues('ec_delevery_time_slot', 'slotID=' . $slotID, $resData['xssData']);

            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Warehouse Updated');
                redirect('manage/timeSlote/index');
            } else {
                 $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/timeSlote/index');
            }
        }

        $this->data['content'] = $this->generalModel->getTableValue('*', 'ec_delevery_time_slot', 'slotID=' . "$slotID" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->render('manage/timeSlote/Add');
    }

    public function changeStatus() {

        $slotID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_delevery_time_slot', 'slotID=' . $slotID, $this->data['data']);
        if($status == 'Active'){
            redirect('manage/timeSlote?msg=active');
        }else if($status =='Inactive'){
            redirect('manage/timeSlote?msg=inactive');
        }else if($status == 'Deleted'){
            redirect('manage/timeSlote?msg=delete');
        }else{
            redirect('manage/timeSlote');
        }
        //redirect($_SERVER['HTTP_REFERER']);
    }
    
//    public function warehouseCityAdd() {
//        $cityList = $this->warehouseModel->getTableValue('cityID,countryID', 'ec_cities', 'countryID="173" AND status="Active"', TRUE);
//       
//        foreach($cityList as $cityData){
//            $resData['nonXss'] = array(
//                'warehouseID' => 1,
//                'cityID' => $cityData["cityID"],
//            );
//            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
//          $this->generalModel->insertValue('ec_warehouse_delivery_location', $resData['xssData']);
//        }
//    }
    
     public function unique_city_edit($cityID,$wharehouseCityID){
        $email = $this->input->post('email');
        $otpDet = $this->generalModel->getTableValue('wharehouseCityID', 'ec_warehouse_delivery_location', 'cityID ="'.$cityID.'" AND wharehouseCityID!="'.$wharehouseCityID.'" AND status!="Deleted"',FALSE);
        if($otpDet){
            return FALSE;   
        }   
        else{
             return TRUE;  
        }
    }
    
     public function unique_city_add($cityID){
        $email = $this->input->post('email');
        $otpDet = $this->generalModel->getTableValue('wharehouseCityID', 'ec_warehouse_delivery_location', 'cityID ="'.$cityID.'" AND status!="Deleted"',FALSE);
        if($otpDet){
            return FALSE;   
        }   
        else{
             return TRUE;  
        }
    }

}

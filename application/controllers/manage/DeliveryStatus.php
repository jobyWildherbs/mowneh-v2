<?php
/**
 * Description of DeliveryStatus
 *
 * @author wildherbs-user
 */
class DeliveryStatus extends Admin_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'gm');
        $this->load->model('DeliveryStatusModel', 'dsm');
    }

    public function index() {

        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->gm->updateTableValues('ec_delivery_status', 'deliveryStatusID IN (' . $id . ')', $status);
            }
        }

        $this->data['breadcrumbs'] = "All Delivery-Status";
        $this->data['pageTitle'] = "All Delivery-Status";
        $this->load->library("pagination");
        $totalRws = $this->gm->getTableValue('count(deliveryStatusID) as tblCount', 'ec_delivery_status', '', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/DeliveryStatus/index';
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'name' => $this->input->get('name'),
            'deliveryStatusID' => null,
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );
        $this->data['result'] = $this->dsm->selectAll($params);
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }
        $this->render('manage/deliverystatus/list');
    }

    public function add() {

        $this->data['breadcrumbs'] = "Add DeliveryStatus";
        $this->data['pageTitle'] = "Add DeliveryStatus";

        $this->form_validation->set_rules('statusName', 'DeliveryStatus Name', 'required|xss_clean|is_unique[ec_attribute_group_detail.name]');
        $this->form_validation->set_rules('sortOrder', 'DeliveryStatus SortOrder', 'xss_clean|numeric');

        if ($this->form_validation->run() === TRUE) {
            $deliveryStatus['nonXss'] = array(
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status')
            );
            $deliveryStatus['xssData'] = $this->security->xss_clean($deliveryStatus['nonXss']);
            $result = $this->gm->insertValue('ec_delivery_status', $deliveryStatus['xssData']); //Group Added, $result will have the new ID

            if ($result) {
                $deliveryStatusDetail['nonXss'] = array(
                    'deliveryStatusID' => $result,
                    'languageID' => $this->language,
                    'name' => $this->input->post('statusName')
                );
                $deliveryStatusDetail['xssData'] = $this->security->xss_clean($deliveryStatusDetail['nonXss']);
                $this->gm->insertValue('ec_delivery_status_detail', $deliveryStatusDetail['xssData']); //GroupDetail Added
                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('New Order Status Added');

                redirect('manage/DeliveryStatus/index');
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/DeliveryStatus/index');
            }
        }
        $this->render('manage/deliverystatus/add');
    }

    public function edit() {
        $this->data['breadcrumbs'] = "Edit DeliveryStatus";
        $this->data['pageTitle'] = "Edit DeliveryStatus";
        $deliveryStatusID = $this->uri->segment(4);

        $this->form_validation->set_rules('statusName', 'DeliveryStatus Name', 'required|xss_clean');
        $this->form_validation->set_rules('sortOrder', 'DeliveryStatus SortOrder', 'xss_clean|numeric');

        if ($this->form_validation->run() === TRUE) {
            $deliveryStatus['nonXss'] = array(
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status')
            );
            $deliveryStatus['xssData'] = $this->security->xss_clean($deliveryStatus['nonXss']);
            $result = $this->gm->updateTableValues('ec_delivery_status', 'deliveryStatusID = ' . $deliveryStatusID, $deliveryStatus['nonXss']);
            //UPDATE FILTER GROUP DETAIL
            $deliveryStatusDetail['nonXss'] = array(
                'deliveryStatusID' => $deliveryStatusID,
                'name' => $this->input->post('statusName'),
                'languageID' => $this->language
            );
            
            $deliveryStatusDetail['xssData'] = $this->security->xss_clean($deliveryStatusDetail['nonXss']);
            $checkDeliveryStatusDetail = $this->gm->getTableValue('*', 'ec_delivery_status_detail', 'deliveryStatusID = ' . $deliveryStatusID . ' AND languageID=' . $this->language, false);
            if ($checkDeliveryStatusDetail) {
                $result1 = $this->gm->updateTableValues('ec_delivery_status_detail', 'deliveryStatusID = ' . $deliveryStatusID . ' AND languageID=' . $this->language, $deliveryStatusDetail['xssData']);
            } else {
                $result1 = $this->gm->insertValue('ec_delivery_status_detail', $deliveryStatusDetail['xssData']);
            }

            if ($result1) {
                $this->logUserActivity('DeliveryStatus Updated');
                $this->session->set_flashdata('message', 'Data Updated Successfully');
                redirect('manage/deliverystatus/index');
            } else {
                $this->session->set_flashdata('failMessage', 'Failed to Update');
            }
        }
        $deliveryStatusParams = array(
            'name' => null,
            'deliveryStatusID' => $deliveryStatusID,
            'languageID' => $this->language,
            'limit' => null,
        );
        $this->data['content'] = $this->dsm->selectAll($deliveryStatusParams);
        $this->render('manage/deliverystatus/add');
    }

    public function changeStatus() {

        $deliveryStatusID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity('DeliveryStatus Status changed - ' . $status);
        $result = $this->gm->updateTableValues('ec_delivery_status', 'deliveryStatusID=' . $deliveryStatusID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Options extends Admin_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('OptionModel', 'optionModel');
        

        $imageDimension = $this->getImageDimension('option_image');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
        $this->setDirectory(FCPATH . "uploads/options/");

        $this->imageConfig = array(
            'upload_path' => FCPATH . "uploads/options/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );

    }
    public function index() {
        if ($this->input->post('action')) {
            $id = implode(",", $this->input->post('multyCheck'));
            $status['status'] = $this->input->post('action');
            $this->generalModel->updateTableValues('ec_option', 'optionID IN (' . $id . ')', $status);
        }
        $this->data['breadcrumbs'] = "All Options";
        $this->data['pageTitle'] = "All Options";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(optionID) as tblCount', 'ec_option', '', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Options/index';
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'name' => $this->input->get('name'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language);
         if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        }
           if(@$_GET['msg']){
            if($_GET['msg'] == 'active'){
             $this->data['success'] = "Options activated Successfully";
            }else if($_GET['msg'] == 'inactive'){
             $this->data['success'] = "Options inactivated Successfully";
            }else if($_GET['msg'] == 'delete'){
             $this->data['success'] = "Options deleted Successfully";
            }
        }
        $this->data['result'] = $this->optionModel->selectAllOption($params);
        $this->render('manage/options/List');
    }
    /*     * *********************************************************************
     * JUST AS FILTERS     
     * ********************************************************************* */
    public function add() {
        $errVal = 0;
        $this->data['breadcrumbs'] = "Add Options";
        $this->data['pageTitle'] = "Add Options";
        $this->form_validation->set_rules('optionName', 'Option group name', 'required|xss_clean|is_unique[ec_option_detail.name]');
        $this->form_validation->set_rules('optionType', 'Option type', 'required|xss_clean');
        $this->form_validation->set_rules('optionOrder', 'Option sort order', 'xss_clean|numeric');
        $this->form_validation->set_rules('optionvalue', 'Option value', 'required|xss_clean');
        if ($this->form_validation->run() === TRUE) {
            $option['nonXss'] = array(
                'type'          => $this->input->post('optionType'),
                'sortOrder'     => $this->input->post('optionOrder'),
                'showAs'              => 'Text'
            );
            $option['xssData']  = $this->security->xss_clean($option['nonXss']);
            $result = $this->generalModel->insertValue('ec_option', $option['xssData']); //Group Added, $result will have the new ID
            if ($result) {
                $optionDetail['nonXss'] = array(
                    'optionID'          => $result,
                    'languageID'        => $this->language,
                    'name'              => $this->input->post('optionName'),
                    
                );
                $optionDetail['xssData'] = $this->security->xss_clean($optionDetail['nonXss']);
                $this->generalModel->insertValue('ec_option_detail', $optionDetail['xssData']); //GroupDetail Added
               if (!empty($this->input->post('option'))) { 
                     $_POST['image'] = array(); //Set null array for additional image
                if (!empty($_FILES['image']['size'])) { 
                    $this->load->library('upload', $this->imageConfig);
                    
                    $errorArray = array();
                    $errVal = 0;
                    foreach ($_FILES['image']['name'] as $key => $addImage) {  
                        if ($_FILES['image']['size'][$key] > 0) { 
                            $_FILES['addImg']['name'] = $addImage;
                            $_FILES['addImg']['type'] = $_FILES['image']['type'][$key];
                            $_FILES['addImg']['tmp_name'] = $_FILES['image']['tmp_name'][$key];
                            $_FILES['addImg']['error'] = $_FILES['image']['error'][$key];
                            $_FILES['addImg']['size'] = $_FILES['image']['size'][$key];
                             
                            if ($this->upload->do_upload('addImg')) { echo "success";  
                                $_POST['image'][$key] = $this->upload->data('file_name');
                                // in case you need to save it into a database
                            } else {
                                $errorArray[$key] = $this->upload->display_errors();
                                $errVal = 1;
                            }
                        }
                    }
                    $this->session->set_flashdata('imgErr', $errorArray);
                    $this->data['errVal'] = $errVal;
                }
                    $options = $this->input->post('option'); 
                    $i=0;
                    foreach ($options as $key => $option) {
                        if (!empty($option['name'])) {
                            if (!empty($_POST['image'][$i])) {
                                $option['nonXss'] = array(
                                    'optionID'  => $result,
                                    //'image' => $_POST['image'][$i],
                                    'sortOrder' => $option['sort_order']
                                );
                            }else{
                                $option['nonXss'] = array(
                                    'optionID'  => $result,
                                    'sortOrder' => $option['sort_order']
                                );
                            }
                            
                            
                            $option['xssData'] = $this->security->xss_clean($option['nonXss']);
                            $result1 = $this->generalModel->insertValue('ec_option_value', $option['xssData']);
                            if ($result1) {
                                $optionDetail['nonXss'] = array(
                                    'optionValueID' => $result1,
                                    'optionID' => $result,
                                    'languageID' => $this->language,
                                    'name' => $option['name']
                                    
                                );
                                $optionDetail['xssData'] = $this->security->xss_clean($optionDetail['nonXss']);
                                $this->generalModel->insertValue('ec_option_value_detail', $optionDetail['xssData']);
                            }
                        }
                    $i++;
                    }
                     
                }
                $this->logUserActivity('Option Added');
                $this->session->set_flashdata('message', 'Data Added Successfully');
                if ($errVal == 0) {
                    redirect('manage/Options/index');
                } else {
                    redirect('manage/Options/edit/' . $result . '/imgErr');
                }
                $this->data['message'] = "Data added successfully";
                redirect('manage/Options', 'refresh');
            } else {
                $this->data['errMessage'] = "Failed";
            }
            
        }
        $this->render('manage/options/Add');
    }
    public function edit() {
        $this->data['breadcrumbs'] = "Edit Option";
        $this->data['pageTitle'] = "Edit Option";
        $optionID = $this->uri->segment(4);
        $imgErr = $this->uri->segment(5);
        if ($imgErr) {
            $this->data['imgErr'] = "The upload image path does not appear to be valid.";
            $this->data['errVal'] = 1;
        }
        if ($this->session->flashdata('imgErr') != '') {
            $this->data['imgErr'] = $this->session->flashdata('imgErr');
            $this->data['errVal'] = 1;
        }
        $this->form_validation->set_rules('optionOrder', 'Option sort order', 'xss_clean|numeric');
        $this->form_validation->set_rules('optionType', 'Option type', 'required|xss_clean');
        $this->form_validation->set_rules('optionvalue', 'Option value', 'required|xss_clean');
        if ($this->form_validation->run() === TRUE) {
            $option['nonXss'] = array(
                'sortOrder' => $this->input->post('optionOrder'),
                'type'              => $this->input->post('optionType'),
                'showAs'              => 'Text'
            );
            $option['xssData'] = $this->security->xss_clean($option['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_option', 'optionID = ' . $optionID, $option['nonXss']);
            $optionDetail['nonXss'] = array(
                'name' => $this->input->post('optionName')
            );
            $langID = $this->input->post('languageID');
            $optionDetail['xssData'] = $this->security->xss_clean($optionDetail['nonXss']);
            $this->generalModel->updateTableValues('ec_option_detail', 'optionID = ' . $optionID . ' AND languageID=' . $langID, $optionDetail['nonXss']);

            if (!empty($this->input->post('option'))) {
                $options = $this->input->post('option');
                $_POST['image'] = array(); //Set null array for additional image
                $this->load->library('upload', $this->imageConfig);
                    
                    $errorArray = array(); $errVal = 0;
                    if(!empty($_FILES['image'])){
                         foreach ($_FILES['image']['name'] as $key => $addImage) {  
                        if ($_FILES['image']['size'][$key] > 0) { 
                            $_FILES['addImg']['name'] = $addImage;
                            $_FILES['addImg']['type'] = $_FILES['image']['type'][$key];
                            $_FILES['addImg']['tmp_name'] = $_FILES['image']['tmp_name'][$key];
                            $_FILES['addImg']['error'] = $_FILES['image']['error'][$key];
                            $_FILES['addImg']['size'] = $_FILES['image']['size'][$key];
                             
                            if ($this->upload->do_upload('addImg')) {  
                                $_POST['image'][$key] = $this->upload->data('file_name');
                                // in case you need to save it into a database
                            } else {
                                $errorArray[$key] = $this->upload->display_errors();
                                $errVal = 1;
                            }
                        }
                    }
                    }
                   
                    $this->session->set_flashdata('imgErr', $errorArray);
                    $this->data['errVal'] = $errVal;
                }
                $i=0;
                foreach ($options as $key => $option) {
                    if (!empty($option['option_id']) && !empty($option['language_id'])) { 
                        // UPDATE ! Changed already existing option values
                        $optionValueID   = $option['option_id'];
                        $langID     = $option['language_id'];

                            
                        if (!empty($option['name'])) { 
                            if(!empty($_POST['image'][$i])){
                                $option['nonXss'] = array(
                                        'sortOrder' => $option['sort_order'],
                                        //'image' => $_POST['image'][$i]
                                    );
                            
                                
                            }else{
                                $option['nonXss'] = array(
                                    'sortOrder' => $option['sort_order']
                                );  
                            }
                            
                            
                            $option['xssData'] = $this->security->xss_clean($option['nonXss']);
                            $this->generalModel->updateTableValues('ec_option_value','optionValueID = ' . $optionValueID , $option['xssData']);
                            $optionDetail['nonXss'] = array(
                                'name' => $option['name']
                            );
                            $optionDetail['xssData'] = $this->security->xss_clean($optionDetail['nonXss']);
                            $this->generalModel->updateTableValues('ec_option_value_detail','optionValueID = ' . $optionValueID . ' AND languageID=' . $langID, $optionDetail['xssData']);
                        }
                    } else {
                        
                        // INSERT ! Added new Option values
                        if (!empty($option['name'])) {
                            $option['nonXss'] = array(
                                'optionID' => $optionID,
                                'sortOrder'     => $option['sort_order'],
                                //'image' => $option['name']

                                    
                            );
                            $option['xssData'] = $this->security->xss_clean($option['nonXss']);
                            $result1 = $this->generalModel->insertValue('ec_option_value', $option['xssData']);
                            if ($result1) {
                                $optionDetail['nonXss'] = array(
                                    'optionValueID' => $result1,
                                    'optionID' => $optionID,
                                    'languageID' => $this->language,
                                    'name' => $option['name']

                                );
                                $optionDetail['xssData'] = $this->security->xss_clean($optionDetail['nonXss']);
                                $this->generalModel->insertValue('ec_option_value_detail', $optionDetail['xssData']);
                            }
                        }
                    }
                
                $i++;
                
            }
            if ($result) {
                $this->logUserActivity('Option Updated');
                /*if ($errVal == 0) {
                    $this->data['message'] = "Data Updated";
                redirect('manage/Options', 'refresh');
                } else {
                    redirect('manage/Options/edit/' . $result . '/imgErr');
                }*/
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->data['message'] = "Data Updated";
                redirect('manage/Options', 'refresh');
            } else {
                $this->data['errMessage'] = "Failed";
            }
        }
        $languageID = $this->language;
        //$this->data['content'] = $this->generalModel->getTableValue('*', 'ec_user', 'userID=' . "$userID" . ' AND status!="Deleted"', FALSE); //data for edit
        $main               = $this->optionModel->selectOption($optionID,$languageID);
        $sub                = $this->optionModel->selectOptionNames($optionID,$languageID);
        $this->data['content']    = ['main' => $main, 'sub' => $sub];
        //echo "<pre>"; print_r($this->data);exit;
        $this->render('manage/options/Add');
    }
    public function changeStatus() {

       $userID = $this->uri->segment(4);
       $status = $this->uri->segment(5);
       $this->data['data']['status'] = $status;
        $this->logUserActivity('Option status changed - ', $status);
      $result = $this->generalModel->updateTableValues('ec_option', 'optionID=' . $userID,$this->data['data']);
       if($status == 'Active'){
            redirect('manage/Options?msg=active');
        }else if($status =='Inactive'){
            redirect('manage/Options?msg=inactive');
        }else if($status == 'Deleted'){
            redirect('manage/Options?msg=delete');
        }else{
            redirect('manage/Options');
        }
        //redirect($_SERVER['HTTP_REFERER']);
    }
    public function removeOptionValue() {
       $optionValueId = $_GET['optionValueId'];

        $option = $this->generalModel->deleteTableValues('ec_option_value', 'optionValueID=' . $optionValueId);
        $options = $this->generalModel->deleteTableValues('ec_option_value_detail', 'optionValueID=' . $optionValueId);

        echo json_encode($options);

    }

}

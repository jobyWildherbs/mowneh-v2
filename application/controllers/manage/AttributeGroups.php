<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AttributeGroups extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('AttributeModel', 'attributeModel');
    }

    public function index() {

        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_attribute_group', 'attributeGroupID IN (' . $id . ')', $status);
            }
        }

        $this->data['breadcrumbs'] = "Attribute Groups";
        $this->data['pageTitle'] = "Attribute Groups";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(attributeGroupID) as tblCount', 'ec_attribute_group', 'status!="Deleted"', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/AttributeGroups/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'title' => $this->input->get('name'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );
        $this->data['result'] = $this->attributeModel->selectAllGroup($params);
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }
        $this->render('manage/attributes/listGroup');
    }

    public function add() {


        $this->data['breadcrumbs'] = "Add Attribute Group";
        $this->data['pageTitle'] = "Add Attribute Group";

        $this->form_validation->set_rules('attributeGroupName', 'Attribute Group Name', 'required|xss_clean|is_unique[ec_attribute_group_detail.name]');
        $this->form_validation->set_rules('groupOrder', 'Filter Sort Order', 'xss_clean|numeric');

        if ($this->form_validation->run() === TRUE) {
            $attributeGroup['nonXss'] = array(
                'sortOrder' => $this->input->post('groupOrder'),
                'status' => $this->input->post('status')
            );
            if($this->language==1){
                if($this->input->post('attributeGroupName')){
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($this->input->post('attributeGroupName'))));
                    $attributeGroup['nonXss']['importKey'] =   $importKey;
                }
            }
            $attributeGroup['xssData'] = $this->security->xss_clean($attributeGroup['nonXss']);
            $result = $this->generalModel->insertValue('ec_attribute_group', $attributeGroup['xssData']); //Group Added, $result will have the new ID
//            echo "<pre>"; print_r($result);exit;

            if ($result) {
                $attributeGroupDetail['nonXss'] = array(
                    'attributeGroupID' => $result,
                    'languageID' => $this->language,
                    'name' => $this->input->post('attributeGroupName')
                );
                $attributeGroupDetail['xssData'] = $this->security->xss_clean($attributeGroupDetail['nonXss']);
                $this->generalModel->insertValue('ec_attribute_group_detail', $attributeGroupDetail['xssData']); //GroupDetail Added
                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Attribute Group Added');

                redirect('manage/AttributeGroups/index');
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                    redirect('manage/AttributeGroups/index');
            }
        }
        $this->render('manage/attributes/addGroup');
    }

    public function edit() {
        $this->data['breadcrumbs'] = "Edit Attribute Group";
        $this->data['pageTitle'] = "Edit Attribute Group";
        $attributeGroupID = $this->uri->segment(4);
       
        $this->form_validation->set_rules('attributeGroupName', 'Attribute Group Name', 'required|xss_clean');
        $this->form_validation->set_rules('groupOrder', 'Attribute Group SortOrder', 'xss_clean|numeric');

        if ($this->form_validation->run() === TRUE) {
            $attributeGroup['nonXss'] = array(
                'sortOrder' => $this->input->post('groupOrder'),
                'status' => $this->input->post('status')
            );
            if($this->language==1){
                if($this->input->post('attributeGroupName')){
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($this->input->post('attributeGroupName'))));
                    $attributeGroup['nonXss']['importKey'] =   $importKey;
                }
            }
            $attributeGroup['xssData'] = $this->security->xss_clean($attributeGroup['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_attribute_group', 'attributeGroupID = ' . $attributeGroupID, $attributeGroup['nonXss']);
            //UPDATE FILTER GROUP DETAIL
            $attributeGroupDetail['nonXss'] = array(
                'attributeGroupID' => $attributeGroupID,
                'name' => $this->input->post('attributeGroupName'),
                'languageID' => $this->language
            );
            $langID = $this->input->post('languageID');
            $attributeGroupDetail['xssData'] = $this->security->xss_clean($attributeGroupDetail['nonXss']);
             $checkAttrDetail = $this->generalModel->getTableValue('attributeGroupID', 'ec_attribute_group_detail', 'attributeGroupID=' . $attributeGroupID . ' AND languageID=' . $this->language, FALSE);
            if ($checkAttrDetail) {
                $this->generalModel->updateTableValues('ec_attribute_group_detail', 'attributeGroupID = ' . $attributeGroupID . ' AND languageID=' . $langID, $attributeGroupDetail['xssData']); //information Added
            } else {
                $this->generalModel->insertValue('ec_attribute_group_detail', $attributeGroupDetail['xssData']); // insert detail data
            }
            

            if ($result) {
                $this->logUserActivity('Attribute Group Updated');
                $this->data['message'] = "Data Updated";
            } else {
                $this->data['errMessage'] = "Failed";
            }
        }

        //$this->data['content'] = $this->generalModel->getTableValue('*', 'ec_user', 'userID=' . "$userID" . ' AND status!="Deleted"', FALSE); //data for edit
        //$main               = $this->attributeModel->selectGroup($filterGroupID);
        $this->data['attributes']   = $this->attributeModel->selectAttribute($attributeGroupID,$this->language);
//        print_r($this->data['attributes']);exit;
        if( is_countable($this->data['attributes'])){
            $this->data['filterDataCount']  = count($this->data['attributes']);
        } else {
            $this->data['filterDataCount']  = 0;
        }
        $this->data['content']      = $this->attributeModel->selectGroup($attributeGroupID,$this->language);
//        echo "<pre>"; print_r($this->data);exit;
        $this->render('manage/attributes/addGroup');
    }

    public function changeStatus() {

        $attributeGroupID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity('Attribute Group status changed - '.$status);
        $result = $this->generalModel->updateTableValues('ec_attribute_group', 'attributeGroupID=' . $attributeGroupID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }

}

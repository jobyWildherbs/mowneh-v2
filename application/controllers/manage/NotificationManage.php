<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class NotificationManage extends Admin_Controller {

    private $imageConfig = array();
    
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');  
        $this->load->model('NotificationModel', 'notificationModel');
        $imageDimension = $this->getImageDimension('notification_dimen');
        $this->load->model('CategoryModel', 'categoryModel');
        $this->load->model('ProductModel', 'productModel');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
        $this->imageConfig = array(
            'upload_path' => "./uploads/notification/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );
    }

    public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_promotion', 'promotionID IN (' . $id . ')', $status);
            }
        }
        $this->data['breadcrumbs'] = "Manage Notification";
        $this->data['pageTitle'] = "Manage Notification";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(notificationID) as tblCount', 'ec_notification', 'status!="Deleted"', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/notificationManage/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;


        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'title' => $this->input->get('title'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );

        $this->data['result'] = $this->notificationModel->selectAll($params);
        //echo "<pre>"; print_r($this->data['result'] ); exit;

        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/notification/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Manage Notification";
        $this->data['pageTitle'] = "Manage Notification";

        //form validation 
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        $this->form_validation->set_rules('message', 'message', 'required');
        $this->form_validation->set_rules('type', 'type', 'required');
//        $this->form_validation->set_rules('link', 'URL Key', 'callback_valid_url_format');

//        $this->form_validation->set_rules('linkOnButton', 'link on button', 'required');
//        $this->form_validation->set_rules('sortOrder', 'sort order', 'required|numeric');
        $this->data['imgErr'] = '';
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            $_POST['image'] = '';
            if ($_FILES['image']['size'] > 0) {
                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    $errVal = 1;
                    $_POST['image'] = '';
                }
            } else {
               // $this->data['imgErr'] = "No Image File Uploaded";
                //$errVal = 1;
                $_POST['image'] = '';
            }
            if($this->input->post('type')=='category'){
                $parameter  = $this->input->post('categoryID');
            }else{
                $parameter  = $this->input->post('productID');
            }
              
            $this->session->set_flashdata('imgErr', $this->data['imgErr']);
            $this->data['errVal'] = $errVal;
            
            $resData['nonXss'] = array(
                'title' => $this->input->post('title'),
                'image' => $this->input->post('image'),
                'message' => $this->input->post('message'),
                'link' => $this->input->post('link'),
                'type' => $this->input->post('type'),
                'parameter' => $parameter,
                'createdDate' => date('Y-m-d H:i')
            );
           
           
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $notificationID = $this->generalModel->insertValue('ec_notification', $resData['xssData']);

            if ($notificationID) {
                $notification = $this->generalModel->getTableValue('*', 'ec_notification', 'notificationID='. $notificationID .' AND status!="Deleted"', FALSE);
                //echo "<pre>"; print_r($notification); exit;
                $title = $notification['title'];
                $body  = $notification['message'];
                
                $firebase = $this->generalModel->getTableValue('userID,firebaseToken', 'ec_firebase_token', 'userID!="0"', TRUE);
                //echo "<pre>"; print_r($firebase); exit;
                if($firebase){
                    foreach($firebase as $firebaseData){
                            
                           
                            $url = "https://fcm.googleapis.com/fcm/send";
                            $token = $firebaseData['firebaseToken'];
                            $serverKey = 'AAAAtLlUYc0:APA91bG3NULL6yVXfConMcbJYNuF0-lyoY2RPHmkzuS_pTnqmAKSQQ4UQi0PGTlaDHk_tADlQ0xPXdt5YmWM5gfBmcutxA-jYcFelsreB1eVfWcqiVT1zUWupPoxo4WNSzSfLolT7TIU';
                            
                            $notification = array('title' =>$title , 'text' => $body, 'sound' => 'default', 'badge' => '1');
                            $arrayToSend  = array('to' => $token, 'notification' => $notification,'priority'=>'high');
                            $json = json_encode($arrayToSend);
                            $headers = array();
                            $headers[] = 'Content-Type: application/json';
                            $headers[] = 'Authorization: key='. $serverKey;
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);

                            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,

                            "POST");
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
                            curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            //Send the request
                            $response = curl_exec($ch);
                            //echo $response['multicast_id']; exit;
                            $responseArray  =  json_decode($response); 
                           //echo $response;
                           //echo $responseArray->multicast_id."/";
                            //Close request
                            if ($response === FALSE) {
                               
                            die('FCM Send Error: ' . curl_error($ch));
                            }
                            else{
                                if($responseArray->success == 1){
                                    $notification = $this->generalModel->getTableValue('*', 'ec_notification', 'notificationID='. $notificationID .' AND status!="Deleted"', FALSE);
                                    
                                    $noteSuccess['nonXss'] = array(
                                        'notificationID' => $notification['notificationID'],
                                        'token' => $token,
                                        'userID' => $firebaseData['userID'],
                                        'status' => 1,
                                        'multicast_id' => $responseArray->multicast_id,
                                    );

                                    $noteSuccess['xssData'] = $this->security->xss_clean($noteSuccess['nonXss']);
                                    $this->generalModel->insertValue('ec_notification_to_user', $noteSuccess['xssData']);
                                    //echo $this->db->last_query();
                                    //echo "<pre>"; print_r($noteSuccess); 
                                    
                                }else{
                                   $this->generalModel->deleteTableValues('ec_firebase_token','firebaseToken="'.$token.'"');
                                }
                                
                            }
                            curl_close($ch);
                    }
                } 
               
                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Notification Added');
                 
                if ($errVal == 0) {
                    redirect('manage/notificationManage/');
                } else {
                    redirect('manage/notificationManage/edit/' . $notificationID . '/imgErr');
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/notificationManage/');
            }
        }
        $this->data['productList']      = $this->productModel->selectProduct('', $this->language); // Product List
        $this->data['mainCategory']     = $this->categoryModel->selectCategory('', $this->language); // parent category 
        $this->render('manage/notification/Add');
    }

    public function edit() {

        $notificationID = $this->uri->segment(4);
        //$imgErr = $this->uri->segment(5);
//        if ($imgErr) {
//            $this->data['imgErr'] = "Invalid Image, Please try again";
//            $this->data['errVal'] = 1;
//        }
//        if ($this->session->flashdata('imgErr') != '') {
//            $this->data['imgErr'] = $this->session->flashdata('imgErr');
//            $this->data['errVal'] = 1;
//        }
        //form validation 
        $this->data['breadcrumbs'] = "Manage Notification";
        $this->data['pageTitle'] = "Manage Notification";

        //form validation 
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        $this->form_validation->set_rules('message', 'message', 'required');
        $this->form_validation->set_rules('type', 'type', 'required');
        //image upload
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            if ($_FILES['image']['size'] > 0) {

                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    //$_POST['image'] = '';
                    $errVal = 1;
                }
            }

            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'title' => $this->input->post('title'),
                'message' => $this->input->post('message'),
                'link' => $this->input->post('link'),
                'type' => $this->input->post('type'),
                'parameter' => $this->input->post('parameter'),
            );
//            if ($errVal == 1) {
//                $resData['nonXss']['status'] = 'Inactive';
//            }
            if ($this->input->post('image')) {
                $resData['nonXss']['image'] = $this->input->post('image');
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_notification', 'notificationID=' . $notificationID, $resData['xssData']);
            
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Notification Updated');
                if ($errVal == 0) {
                    redirect('manage/notificationManage/');
                } else {
                    $this->session->set_flashdata('imgErr', $this->data['imgErr']);
                    redirect('manage/notificationManage/edit/' . $promotionID);
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/notificationManage/');
            }
        }

        $this->data['content'] = $this->generalModel->getTableValue('*', 'ec_notification', 'notificationID=' . "$notificationID" . ' AND status!="Deleted"', FALSE); //data for edit

        $this->render('manage/notification/Add');
    }

    public function changeStatus() {

        $promotionID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity('Promotion banner status changed - '.$status);
        $result = $this->generalModel->updateTableValues('ec_promotion', 'promotionID=' . $promotionID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function valid_url_format($str) {
        //echo $str."======test";exit;
        if (empty($str)) {
            $this->form_validation->set_message('valid_url_format', 'The {field} field can not be empty');
            return FALSE;
        } elseif (preg_match('/^(?:([^:]*)\:)?\/\/(.+)$/', $str, $matches)) {
            if (empty($matches[2])) {
                $this->form_validation->set_message('valid_url_format', 'Invalid url');
                return FALSE;
            } elseif (!in_array(strtolower($matches[1]), array('http', 'https'), TRUE)) {
                $this->form_validation->set_message('valid_url_format', 'Invalid url');
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $this->form_validation->set_message('valid_url_format', 'Invalid url');
            return FALSE;
        }
    }

}

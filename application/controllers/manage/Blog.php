<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends Admin_Controller {

    //private $languageID;

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('BlogModel', 'blogModel');
                $this->setDirectory(FCPATH . "uploads/blog/");
        $imageDimension = $this->getImageDimension('blog_image_dimen');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
        $this->imageConfig = array(
            'upload_path' => "./uploads/blog/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );
    }

    public function index() {
        //$this->data = array();

        if ($this->input->post('action')) {
            $id = implode(",", $this->input->post('multyCheck'));
            $status['status'] = $this->input->post('action');
            $this->generalModel->updateTableValues('ec_blog', 'blogID IN (' . $id . ')', $status);
        }
        $this->data['breadcrumbs'] = "All Blog Page";
        $this->data['pageTitle'] = "All Blog Page";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(blogID) as tblCount', 'ec_blog', 'blogID!=1 AND status!="Deleted"', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Blog/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;


        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'title' => $this->input->get('title'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language);

        $this->data['result'] = $this->blogModel->selectAll($params);

        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/blog/list');
    }

    public function add() {

        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Blog";
        $this->data['pageTitle'] = "Add Blog";

        //form validation 
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('author', 'Author', 'required');
        $this->form_validation->set_rules('images', 'Image', 'required');

        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            $_POST['image'] = '';
            $this->data['imgErr']='';
            if ($_FILES['image']['size'] > 0) {
               
                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    $errVal = 1;
                    $_POST['image'] = '';
                }
            }
            $this->session->set_flashdata('imgErr', $this->data['imgErr']);
            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'image' => $this->input->post('image'),
                'status' => $this->input->post('status')
            );
            
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $resData['xssData']['dateadded'] = date('Y-m-d H:i');
            $result = $this->generalModel->insertValue('ec_blog', $resData['xssData']);

            if ($result) {
                $blogDetail['nonXss'] = array(
                    'blogID' => $result,
                    'languageID' => $this->language,
                    'title' => $this->input->post('title'),
                    'description' => $this->input->post('description'),
                    'author' => $this->input->post('author')
                );
                $blogDetail['xssData'] = $this->security->xss_clean($blogDetail['nonXss']);
                $this->generalModel->insertValue('ec_blog_detail', $blogDetail['xssData']); //Blog Added

                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Blog Added');
                if ($errVal == 0) {
                    redirect('manage/blog/index');
                } else {
                    redirect('manage/blog/edit/' . $result . '/imgErr');
                }
            } else {

                $this->session->set_flashdata('failMessage', 'Failed To Add');
                
                redirect('manage/blog/index');

            }
        }
        $this->render('manage/blog/Add');
    }

    public function edit() {

        // $this->data = array();
        $this->data['breadcrumbs'] = "Edit Blog";
        $this->data['pageTitle'] = "Edit Blog";
        $blogID = $this->uri->segment(4);
        $imgErr = $this->uri->segment(5);
            if ($imgErr) {
            $this->data['imgErr'] = "Invalid Image, Please try again";
            $this->data['errVal'] = 1;
        }
        if ($this->session->flashdata('imgErr') != '') {
            $this->data['imgErr'] = $this->session->flashdata('imgErr');
            $this->data['errVal'] = 1;
        }
        //form validation 
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('author', 'Author', 'required');
         $this->form_validation->set_rules('images', 'Image', 'required');
        //image upload
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            if ($_FILES['image']['size'] > 0) {
                
                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    $errVal = 1;
                }
            }
            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'status' => $this->input->post('status')
            );

            if ($this->input->post('image')) {
                $resData['nonXss']['image'] = $this->input->post('image');
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $result = $this->generalModel->updateTableValues('ec_blog', 'blogID=' . $blogID, $resData['xssData']);


            $blogDetail['nonXss'] = array(
                'blogID' => $blogID,
                'languageID' => $this->language,
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'author' => $this->input->post('author')
            );
            $blogDetail['xssData'] = $this->security->xss_clean($blogDetail['nonXss']);
            $checkBlogDetail = $this->generalModel->getTableValue('blogID', 'ec_blog_detail', 'blogID=' . $blogID . ' AND languageID=' . $this->language, FALSE);
            if ($checkBlogDetail) {

                $this->generalModel->updateTableValues('ec_blog_detail', 'blogID=' . $blogID . ' AND languageID=' . $this->language, $blogDetail['xssData']); //information Added
            } else {
                $this->generalModel->insertValue('ec_blog_detail', $blogDetail['xssData']); // insert detail data
            }
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Blog Updated');
                if ($errVal == 0)
                    redirect('manage/Blog/index');
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/Blog/index');
            }
        }

        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_blog', 'blogID=' . "$blogID" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->data['content']['detail'] = $this->generalModel->getTableValue('*', 'ec_blog_detail', 'blogID=' . "$blogID" . ' AND languageID=' . $this->language, FALSE); //data for edit

        $this->render('manage/blog/Add');
    }

    public function changeStatus() {

        $blogID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity('Blog status changed - '.$status);
        $result = $this->generalModel->updateTableValues('ec_blog', 'blogID=' . $blogID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }

}

<?php

/**
 * Description of Order
 *
 * @author wildherbs-user
 */
class Order extends Admin_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('OrderModel','om');
        $this->load->library('ReportSupport');
    }

    public function index() {
        $this->data['breadcrumbs'] = "All Orders";
        $this->data['pageTitle'] = "All Orders";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(orderID) as tblCount', 'ec_order', '', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/order/index';
        
        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $this->data['currentPage'] = $page + 1;
        $params = array(
            'orderID' => $this->input->get('order'),
            'limit' => $config['per_page'],
            'languageID'=>$this->language,
            'start' => $page);
        $orders = $this->om->selectAll($params);
        foreach($orders as $key=>$row){
            $orders[$key]['invoice']    = $row['invoicePrefix'].$row['invoiceNo'];
            $orders[$key]['customer']   = $row['firstname'].' '.$row['lastname'];
        }
        $this->data['orders']           = $orders;
        
        $filter =   $this->reportsupport->filterInput('order');
        $this->data['filter']   =   $filter;
        
        $this->render('manage/order/list');
    }
    
    public function orderReportExport(){
        $this->load->library('Export');
        $fromDate = $this->input->post('fromDate'); 
        $params['fromDate']             = $this->input->post('fromDate');
        $params['toDate']               = $this->input->post('toDate');
        $params['orderStatusID']        = $this->input->post('orderStatusID');
        $params['languageID']           = $this->language;
        $order = $this->om->selectReport($params);
       // echo "<pre>"; print_r($order); exit;
        if(!empty($order)){
            $header  =   array_keys($order[0]);
            foreach($order as $key=>$value){
                foreach($value as $innerKey=>$innerVal){
                   $data[$key][$innerKey]         =   strip_tags($value[$innerKey]);
                }
            }
        }
            $date   = date('d-m-Y');
            $fileName  =   'order'.$date.'.csv'; 
            $export =   $this->export->exportFile($fileName,$header,$data);
            redirect($_SERVER['HTTP_REFERER']);
    }
      public function changeStatus() {
        $orderID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity('Order status changed - ', $status);
        $result = $this->generalModel->updateTableValues('ec_order', 'orderID=' . $orderID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }

}

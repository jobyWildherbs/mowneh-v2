<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class OrderStatus extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'gm');
        $this->load->model('OrderStatusModel', 'osm');
    }

    public function index() {

        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->gm->updateTableValues('ec_order_status', 'orderStatusID IN (' . $id . ')', $status);
            }
        }

        $this->data['breadcrumbs'] = "All Order-Status";
        $this->data['pageTitle'] = "All Order-Status";
        $this->load->library("pagination");
        $totalRws = $this->gm->getTableValue('count(orderStatusID) as tblCount', 'ec_order_status', '', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/OrderStatus/index';
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'name' => $this->input->get('name'),
            'orderStatusID' => null,
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );
        $this->data['result'] = $this->osm->selectAll($params);
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }
        $this->render('manage/orderstatus/list');
    }

    public function add() {

        $this->data['breadcrumbs'] = "Add OrderStatus";
        $this->data['pageTitle'] = "Add OrderStatus";

        $this->form_validation->set_rules('statusName', 'OrderStatus Name', 'required|xss_clean|is_unique[ec_attribute_group_detail.name]');
        $this->form_validation->set_rules('sortOrder', 'OrderStatus SortOrder', 'xss_clean|numeric');

        if ($this->form_validation->run() === TRUE) {
            $orderStatus['nonXss'] = array(
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status')
            );
            $orderStatus['xssData'] = $this->security->xss_clean($orderStatus['nonXss']);
            $result = $this->gm->insertValue('ec_order_status', $orderStatus['xssData']); //Group Added, $result will have the new ID

            if ($result) {
                $orderStatusDetail['nonXss'] = array(
                    'orderStatusID' => $result,
                    'languageID' => $this->language,
                    'name' => $this->input->post('statusName')
                );
                $orderStatusDetail['xssData'] = $this->security->xss_clean($orderStatusDetail['nonXss']);
                $this->gm->insertValue('ec_order_status_detail', $orderStatusDetail['xssData']); //GroupDetail Added
                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('New Order Status Added');

                redirect('manage/OrderStatus/index');
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/OrderStatus/index');
            }
        }
        $this->render('manage/orderstatus/add');
    }

    public function edit() {
        $this->data['breadcrumbs'] = "Edit OrderStatus";
        $this->data['pageTitle'] = "Edit OrderStatus";
        $orderStatusID = $this->uri->segment(4);

        $this->form_validation->set_rules('statusName', 'OrderStatus Name', 'required|xss_clean');
        $this->form_validation->set_rules('sortOrder', 'OrderStatus SortOrder', 'xss_clean|numeric');

        if ($this->form_validation->run() === TRUE) {
            $orderStatus['nonXss'] = array(
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status')
            );
            $orderStatus['xssData'] = $this->security->xss_clean($orderStatus['nonXss']);
            $result = $this->gm->updateTableValues('ec_order_status', 'orderStatusID = ' . $orderStatusID, $orderStatus['nonXss']);
            //UPDATE FILTER GROUP DETAIL
            $orderStatusDetail['nonXss'] = array(
                'orderStatusID' => $orderStatusID,
                'name' => $this->input->post('statusName'),
                'languageID' => $this->language
            );
            $langID = $this->input->post('languageID');
            $orderStatusDetail['xssData'] = $this->security->xss_clean($orderStatusDetail['nonXss']);
            $checkOrderStatusDetail = $this->gm->getTableValue('*', 'ec_order_status_detail', 'orderStatusID = ' . $orderStatusID . ' AND languageID=' . $langID, false);
            if ($checkOrderStatusDetail) {
                $result1 = $this->gm->updateTableValues('ec_order_status_detail', 'orderStatusID = ' . $orderStatusID . ' AND languageID=' . $langID, $orderStatusDetail['xssData']);
            } else {
                $result1 = $this->gm->insertValue('ec_order_status_detail', $orderStatusDetail['xssData']);
            }

            if ($result1) {
                $this->logUserActivity('OrderStatus Updated');
                $this->session->set_flashdata('message', 'Data Updated Successfully');
                redirect('manage/orderstatus/index');
            } else {
                $this->session->set_flashdata('failMessage', 'Failed to Update');
            }
        }
        $orderStatusParams = array(
            'name' => null,
            'orderStatusID' => $orderStatusID,
            'languageID' => $this->language,
            'limit' => null,
        );
        $this->data['content'] = $this->osm->selectAll($orderStatusParams);
        $this->render('manage/orderstatus/add');
    }

    public function changeStatus() {

        $orderStatusID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity('OrderStatus status changed - ' . $status);
        $result = $this->gm->updateTableValues('ec_order_status', 'orderStatusID=' . $orderStatusID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }

}

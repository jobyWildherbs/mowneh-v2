<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AddBanner extends Admin_Controller {

    private $imageConfig = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('AddBannerModel', 'addbannerModel');
                        $this->setDirectory(FCPATH . "uploads/addBanner/");

        $imageDimension = $this->getImageDimension('side_banner_add_dimen');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
        $this->imageConfig = array(
            'upload_path' => "./uploads/addBanner/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );
    }
    
      public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
       
        $this->data['breadcrumbs'] = "All Ad Banner";
        $this->data['pageTitle'] = "All Ad Banner";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(addID) as tblCount', 'ec_add_banner', '', FALSE); //data count for pagination
        
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/AddBanner/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'title' => $this->input->get('title'),
            'limit' => $config['per_page'],
            'start' => $page,
            
        );
        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
    
        $this->data['result'] = $this->addbannerModel->selectAll( $this->language,$params);
        
       // echo "<pre>"; print_r($this->data['result']); exit;

        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/addBanner/list');
    }
    
     public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Ad Banner";
        $this->data['pageTitle'] = "Edit Ad Banner";

        $addID = $this->uri->segment(4);
        $imgErr = $this->uri->segment(5);
        if ($imgErr) {
            $this->data['imgErr'] = "Invalid Image, Please try again";
            $this->data['errVal'] = 1;
        }
        if ($this->session->flashdata('imgErr') != '') {
            $this->data['imgErr'] = $this->session->flashdata('imgErr');
            $this->data['errVal'] = 1;
        }
        //form validation 
        $this->form_validation->set_rules('addUrl', 'URL Key', 'callback_valid_url_format');
        //image upload
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            if ($_FILES['image']['size'] > 0) {

                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    //$_POST['image'] = '';
                    $errVal = 1;
                }
            }

            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'addUrl' => $this->input->post('addUrl'),
                'status' => $this->input->post('status')
            );
           
            
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_add_banner', 'addID=' . $addID, $resData['xssData']);
 
            $bannerDetail['nonXss'] = array(
                'addID'      => $addID,
                'languageID'    => $this->language
            );
            if ($this->input->post('image')) {
                $bannerDetail['nonXss']['image'] = $this->input->post('image');
            }

            $bannerDetail['xssData'] = $this->security->xss_clean($bannerDetail['nonXss']);
            $checkBannerDetail = $this->generalModel->getTableValue('addID', 'ec_add_banner_detail', 'addID=' . $addID . ' AND languageID=' . $this->language, FALSE);
            if ($checkBannerDetail) {
                $this->generalModel->updateTableValues('ec_add_banner_detail', 'addID=' . $addID . ' AND languageID=' . $this->language, $bannerDetail['xssData']); //information Added
            } else {
                $this->generalModel->insertValue('ec_add_banner_detail', $bannerDetail['xssData']); // insert detail data
            }
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Add Banner Updated');
                if ($errVal == 0) {
                    redirect('manage/addBanner/index');
                } else {
                    $this->session->set_flashdata('imgErr', $this->data['imgErr']);
                    redirect('manage/addBanner/edit/' . $addID);
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/addBanner/index');
            }
        }

        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_add_banner', 'addID=' . "$addID" . ' AND status!="Deleted"', FALSE); //data for edit
        
        $this->data['content']['detail'] = $this->generalModel->getTableValue('*', 'ec_add_banner_detail', 'addID=' . "$addID" . ' AND languageID=' . $this->language, FALSE); //data for edit
        //echo "<pre>"; print_r($this->data['content']['detail']); exit;
        $this->render('manage/addBanner/Add');
    }
    
    public function valid_url_format($str) {
        //echo $str."======test";exit;
        if (empty($str)) {
            $this->form_validation->set_message('valid_url_format', 'The {field} field can not be empty');
            return FALSE;
        } elseif (preg_match('/^(?:([^:]*)\:)?\/\/(.+)$/', $str, $matches)) {
            if (empty($matches[2])) {
                $this->form_validation->set_message('valid_url_format', 'Invalid url');
                return FALSE;
            } elseif (!in_array(strtolower($matches[1]), array('http', 'https'), TRUE)) {
                $this->form_validation->set_message('valid_url_format', 'Invalid url');
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $this->form_validation->set_message('valid_url_format', 'Invalid url');
            return FALSE;
        }
    }
    
    public function changeStatus() {

        $addID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_add_banner', 'addID=' . $addID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }
}
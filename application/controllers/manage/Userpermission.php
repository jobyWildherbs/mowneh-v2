<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Userpermission extends Admin_Controller {



    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('UserpermissionModel', 'userpermissionModel');

    }

    public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_user_permission', 'id IN (' . $id . ')', $status);
            }
        }
        $this->data['breadcrumbs'] = "All Userpermission Page";
        $this->data['pageTitle'] = "All Userpermission Page";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(id) as tblCount', 'ec_user_permission', 'id!=1 AND status!="delete"', FALSE); //data count for pagination
        
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Userpermission/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'permissionName' => $this->input->get('permissionName'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );

        $this->data['result'] = $this->userpermissionModel->selectAll($params);

        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/userpermission/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Userpermission";
        $this->data['pageTitle'] = "Add Userpermission";

        //form validation 
        $this->form_validation->set_rules('name', 'Userpermission Name', 'required|xss_clean');
     

        if ($this->form_validation->run() === TRUE) {

            $resData['nonXss'] = array(
                'permissionName' => $this->input->post('permissionName'),
                'className' => $this->input->post('className'),
                
                'status' => $this->input->post('status')
            );
           
           
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_user_permission', $resData['xssData']);

            if ($result) {
                
                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Userpermission Added');

                    redirect('manage/userpermission/index');

            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/userpermission/index');
            }
        }
        $this->render('manage/userpermission/Add');
    }

    public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Userpermission";
        $this->data['pageTitle'] = "Edit Userpermission";

        $id = $this->uri->segment(4);

        //form validation 
       $this->form_validation->set_rules('name', 'Userpermission Name', 'required|xss_clean');
        
        //image upload
        if ($this->form_validation->run() === TRUE) {

            $resData['nonXss'] = array(
                'permissionName' => $this->input->post('permissionName'),
                'className' => $this->input->post('className'),
                
                'status' => $this->input->post('status')
            );

            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_user_permission', 'id=' . $id, $resData['xssData']);

            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Userpermission Updated');

                    redirect('manage/userpermission/index');

            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/userpermission/index');
            }
        }

        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_user_permission', 'id=' . "$id" . ' AND status!="Deleted"', FALSE); //data for edit
        

        $this->render('manage/userpermission/Add');
    }

    public function changeStatus() {

        $id = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_user_permission', 'id=' . $id, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }

    

}

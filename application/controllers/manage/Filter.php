<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Filter extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('FilterModel', 'filterModel');
    }

    public function index() {
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
               
                $this->generalModel->updateTableValues('ec_filter_group', 'filterGroupID IN (' . $id . ')', $status);
                if($status['status']=='Deleted'){
                     $this->generalModel->deleteTableValues('ec_filter', 'filterGroupID IN (' . $id . ')');
                }
            }
        }

        $this->data['breadcrumbs'] = "All Filters";
        $this->data['pageTitle'] = "All Filters";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(filterGroupID) as tblCount', 'ec_filter_group', 'status!="Deleted"', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Filter/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $this->pagination->initialize($config);
        $this->data["links"] = $this->pagination->create_links();
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        
        $selectedFilters    = $this->generalModel->getTableValue('distinct(filterID)','ec_category_filter',null,true);
        $filArray = array();
        foreach ($selectedFilters as $value) {
            $filArray[] = $value['filterID'];
        }
        $this->data['usedFilters'] = $filArray;
        if(@$_GET['msg']){
            if($_GET['msg'] == 'active'){
             $this->data['success'] = "Filter activated Successfully";
            }else if($_GET['msg'] == 'inactive'){
             $this->data['success'] = "Filter inactivated Successfully";
            }else if($_GET['msg'] == 'delete'){
             $this->data['success'] = "Filter deleted Successfully";
            }
        }
        if ($this->session->flashdata('success') != '') {
            $this->data['success'] = $this->session->flashdata('success');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failed');
        }
        $limit =    $config['per_page'];
        $start = $page;
        $title = $this->input->get('name');
        $this->data['result'] = $this->filterModel->selectFilter('', $this->language,$title,$limit,$start);
        $this->render('manage/filter/list');
    }

    /*     * *********************************************************************
     * There are four tables to manage filters
     * Insertion is done by 4 steps
     * First FilterGroup is inserted and the ID is generated
     * which is used to insert the FilterGroupDetails
     * For each FilterVlues, data will be added to Filter and Filter_Deatil
     * along with the FilterGruopID     
     * ********************************************************************* */

    public function add() {


        $this->data['breadcrumbs'] = "Add Filters";
        $this->data['pageTitle'] = "Add Filters";

        $this->form_validation->set_rules('filterGroupName', 'Filter Group Name', 'required|xss_clean|is_unique[ec_filter_group_detail.name]');
        $this->form_validation->set_rules('filterGroupOrder', 'Filter Sort Order', 'xss_clean|numeric');
        $this->form_validation->set_rules('filtervalue', 'Filter value', 'required|xss_clean');
        if ($this->form_validation->run() === TRUE) {


            $filterGroup['nonXss'] = array(
                'sort_order' => $this->input->post('filterGroupOrder'),
                'status' => $this->input->post('status')
            );
            
            if($this->language==1){
                if($this->input->post('filterGroupName')){
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($this->input->post('filterGroupName'))));
                    $filterGroup['nonXss']['importKey'] =   $importKey;
                }
            }
            
            
            $filterGroup['xssData'] = $this->security->xss_clean($filterGroup['nonXss']);
            $result = $this->generalModel->insertValue('ec_filter_group', $filterGroup['xssData']); //Group Added, $result will have the new ID

            if ($result) {
                $filterGroupDetail['nonXss'] = array(
                    'filterGroupID' => $result,
                    'languageID' => $this->language,
                    'name' => $this->input->post('filterGroupName')
                );
                $filterGroupDetail['xssData'] = $this->security->xss_clean($filterGroupDetail['nonXss']);
                $this->generalModel->insertValue('ec_filter_group_detail', $filterGroupDetail['xssData']); //GroupDetail Added

                if (!empty($this->input->post('filter'))) {
                    $filters = $this->input->post('filter');
                    foreach ($filters as $key => $filter) {
                        if (!empty($filter['name'])) {
                            
                                
                            $filter['nonXss'] = array(
                                'filterGroupID' => $result,
                                'sortOrder' => $filter['sort_order']
                            );
                            if($this->language==1){
                                $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($filter['name'])));
                                $filter['nonXss']['importKey'] =   $importKey;
                            }
                            $filter['xssData'] = $this->security->xss_clean($filter['nonXss']);
                            $result1 = $this->generalModel->insertValue('ec_filter', $filter['xssData']);
                            if ($result1) {
                                $filterDetail['nonXss'] = array(
                                    'filterID' => $result1,
                                    'filterGroupID' => $result,
                                    'languageID' => $this->language,
                                    'name' => $filter['name']
                                );
                                $filterDetail['xssData'] = $this->security->xss_clean($filterDetail['nonXss']);
                                $this->generalModel->insertValue('ec_filter_detail', $filterDetail['xssData']);
                            }
                        }
                    }
                }
                $this->logUserActivity('Filter Added');
                $this->session->set_flashdata('success', 'Filter added successfully');
                redirect('manage/Filter/index');
            } else {
                $this->session->set_flashdata('failed', 'Failed to add filter');
            }
        }
        $this->render('manage/filter/Add');
    }

    public function edit() {

        $this->data['breadcrumbs'] = "Edit Filter";
        $this->data['pageTitle'] = "Edit Filter";
        $filterGroupID = $this->uri->segment(4);

        $this->form_validation->set_rules('filterGroupOrder', 'Filter Sort Order', 'xss_clean|numeric');

        if ($this->form_validation->run() === TRUE) {
            //UPDATE FilterGroup
            $filterGroup['nonXss'] = array(
                'sort_order' => $this->input->post('filterGroupOrder'),
                'status' => $this->input->post('status')
            );
            if($this->language==1){
                if($this->input->post('filterGroupName')){
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($this->input->post('filterGroupName'))));
                    $filterGroup['nonXss']['importKey'] =   $importKey;
                }
            }
            
            $filterGroup['xssData'] = $this->security->xss_clean($filterGroup['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_filter_group', 'filterGroupID = ' . $filterGroupID, $filterGroup['nonXss']);
            //UPDATE FILTER GROUP DETAIL
            $filterGroupDetail['nonXss'] = array(
                'name' => $this->input->post('filterGroupName')
            );
            $langID = $this->input->post('languageID');
            //----------------------------------------------------------------
            // CHECK IF THERE IS ALREADY A ROW WITH LANGUAGE_ID AND FILTER_ID
            // IF YES UPDATE
            // ELSE INSERT
            //----------------------------------------------------------------
            $filterGroupLanguageInfo = $this->generalModel->getTableValue('*', 'ec_filter_group_detail', array('filterGroupID' => $filterGroupID, 'languageID' => $this->language));
            // UPDATE
            if ($filterGroupLanguageInfo) {
                $this->generalModel->updateTableValues('ec_filter_group_detail', 'filterGroupID = ' . $filterGroupID . ' AND languageID=' . $this->language, $filterGroupDetail['nonXss']);
            }
            // INSERT
            else {
                $array = array(
                    'filterGroupID' => $filterGroupID,
                    'languageID' => $this->language,
                    'name' => $this->input->post('filterGroupName')
                );
                $this->generalModel->insertValue('ec_filter_group_detail', $array);
            }

            $filterGroupDetail['xssData'] = $this->security->xss_clean($filterGroupDetail['nonXss']);
            //Remove filter start
            $removeFilter   = explode(",",$this->input->post('removeFilter')); 
            foreach($removeFilter as $removeFilterData){
                if($removeFilterData!=""){
                    $this->generalModel->deleteTableValues('ec_filter','filterID='.$removeFilterData);
                    $this->generalModel->deleteTableValues('ec_filter_detail','filterID='.$removeFilterData);
                }
               
            }
            
            //Remove filter end
            //UPDATE FILTER
            //UPDATE FILTER DETAIL
//            ---------------------------------------------------------------------------------------
            /*if (!empty($this->input->post('filter'))) {
                $filters = $this->input->post('filter');
                //echo "<pre>"; print_r($filters);exit;
                foreach ($filters as $key => $filter) {

                    if (!empty($filter['filter_id'])) {
                        // UPDATE ! Changed already existing filter values
                        $filterID = $filter['filter_id'];

                        if (!empty($filter['name'])) {
                            $filter['nonXss'] = array(
                                'sortOrder' => $filter['sort_order']
                            );
                             if($this->language==1){
                                        $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($filter['name'])));
                                        $filter['nonXss']['importKey'] =   $importKey;
                                }
                            $filter['xssData'] = $this->security->xss_clean($filter['nonXss']);

                            $this->generalModel->updateTableValues('ec_filter', 'filterID = ' . $filterID, $filter['xssData']);

                            $filterDetail['nonXss'] = array(
                                'name' => $filter['name']
                            );

                            $filterLanguageInfo = $this->generalModel->getTableValue('*', 'ec_filter_detail', array('filterID' => $filterID, 'languageID' => $this->language));
                            if ($filterLanguageInfo) {

                                $this->generalModel->updateTableValues('ec_filter_detail', 'filterID = ' . $filterID . ' AND languageID=' . $this->language, $filterDetail['nonXss']);
                            }
                            // INSERT
                            else {
                                $array = array(
                                    'filterID' => $filterID,
                                    'filterGroupID' => $filterGroupID,
                                    'languageID' => $this->language,
                                    'name' => $filter['name']
                                );
                                $this->generalModel->insertValue('ec_filter_detail', $array);
                            }
                        }
                    } else {
                        // INSERT ! Added new filter values
                        if (!empty($filter['name'])) {
                            //echo "HELOO:: ".$filter['name'];exit;
                            $filter['nonXss'] = array(
                                'filterGroupID' => $filterGroupID,
                                'sortOrder' => $filter['sort_order']
                            );
                            if($this->language==1){
                                        $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($filter['name'])));
                                        $filter['nonXss']['importKey'] =   $importKey;
                                }
                            $filter['xssData'] = $this->security->xss_clean($filter['nonXss']);
                            $result1 = $this->generalModel->insertValue('ec_filter', $filter['xssData']);
                            if ($result1) {
//                                echo "Inside RESULT!";exit;
                                $filterDetail['nonXss'] = array(
                                    'filterID' => $result1,
                                    'filterGroupID' => $filterGroupID,
                                    'languageID' => $this->language,
                                    'name' => $filter['name']
                                );
                                $filterDetail['xssData'] = $this->security->xss_clean($filterDetail['nonXss']);
                                $this->generalModel->insertValue('ec_filter_detail', $filterDetail['xssData']);
                            }
                        }
                    }
                }
            }*/

            if ($result) {
                $this->logUserActivity('Filter Updated');
                $this->session->set_flashdata('success', 'Filter updated successfully!');
                redirect('manage/Filter/index');
            } else {
                $this->session->set_flashdata('failed', 'Failed to update filter data!');
            }
        }

        //$this->data['content'] = $this->generalModel->getTableValue('*', 'ec_user', 'userID=' . "$userID" . ' AND status!="Deleted"', FALSE); //data for edit
        $main = $this->filterModel->selectFilter($filterGroupID, $this->language);
        $sub = $this->filterModel->selectFilterNames($filterGroupID, $this->language);
        $this->data['formLanguage'] = $this->language;
//        echo "<pre>";print_r($sub);exit;
        $this->data['content'] = ['main' => $main, 'sub' => $sub];
        //echo "<pre>"; print_r($this->data);exit;
        $this->render('manage/filter/Edit');
    }
    public function removeFilterData(){
        $this->generalModel->deleteTableValues('ec_filter','filterID='.$_POST['id']);
                    $this->generalModel->deleteTableValues('ec_filter_detail','filterID='.$_POST['id']);
                    echo json_encode($_POST['id']);

    }
public function updateFilterData() {

        
        $filterGroupID = $_POST['groupid'];
           if ($_POST['filterid'] != 0) {
                        // UPDATE ! Changed already existing filter values
                        $filterID = $_POST['filterid'];

                        if ($_POST['name'] != '') {
                            $filter['nonXss'] = array(
                                'sortOrder' => $_POST['sort']
                            );
                             if($this->language==1){
                                        $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($_POST['name'])));
                                        $filter['nonXss']['importKey'] =   $importKey;
                                }
                            $filter['xssData'] = $this->security->xss_clean($filter['nonXss']);

                            $this->generalModel->updateTableValues('ec_filter', 'filterID = ' . $filterID, $filter['xssData']);

                            $filterDetail['nonXss'] = array(
                                'name' => $_POST['name']
                            );

                            $filterLanguageInfo = $this->generalModel->getTableValue('*', 'ec_filter_detail', array('filterID' => $filterID, 'languageID' => $this->language));
                            if ($filterLanguageInfo) {

                                $this->generalModel->updateTableValues('ec_filter_detail', 'filterID = ' . $filterID . ' AND languageID=' . $this->language, $filterDetail['nonXss']);
                            }
                            // INSERT
                            else {
                                $array = array(
                                    'filterID' => $filterID,
                                    'filterGroupID' => $filterGroupID,
                                    'languageID' => $this->language,
                                    'name' => $_POST['name']
                                );
                                $this->generalModel->insertValue('ec_filter_detail', $array);
                            }
                        }
                        echo json_encode((int)$filterID);

                    } else {
                        // INSERT ! Added new filter values
                        if ($_POST['name'] != '') {
                            //echo "HELOO:: ".$filter['name'];exit;
                            $filter['nonXss'] = array(
                                'filterGroupID' => $filterGroupID,
                                'sortOrder' => $_POST['sort']
                            );
                            if($this->language==1){
                                        $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($_POST['name'])));
                                        $filter['nonXss']['importKey'] =   $importKey;
                                }
                            $filter['xssData'] = $this->security->xss_clean($filter['nonXss']);
                            $result1 = $this->generalModel->insertValue('ec_filter', $filter['xssData']);
                            if ($result1) {
//                                echo "Inside RESULT!";exit;
                                $filterDetail['nonXss'] = array(
                                    'filterID' => $result1,
                                    'filterGroupID' => $filterGroupID,
                                    'languageID' => $this->language,
                                    'name' => $_POST['name']
                                );
                                $filterDetail['xssData'] = $this->security->xss_clean($filterDetail['nonXss']);
                                $this->generalModel->insertValue('ec_filter_detail', $filterDetail['xssData']);
                            }
                        }
                        echo json_encode($result1);
                    }
        //redirect($_SERVER['HTTP_REFERER']);
    }
    public function changeStatus() {


        $filterGroupID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $result = $this->generalModel->updateTableValues('ec_filter_group', 'filterGroupID=' . $filterGroupID, $this->data['data']);
        if($status=='Deleted'){
            $this->generalModel->deleteTableValues('ec_filter', 'filterGroupID=' . $filterGroupID);
        }
        if($status == 'Active'){
            redirect('manage/Filter?msg=active');
        }else if($status =='Inactive'){
            redirect('manage/Filter?msg=inactive');
        }else if($status == 'Deleted'){
            redirect('manage/Filter?msg=delete');
        }else{
            redirect('manage/Filter');
        }
        //redirect($_SERVER['HTTP_REFERER']);
    }

}

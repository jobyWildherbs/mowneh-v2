<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('Customer');
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('CustomerModel', 'customerModel');
        $this->load->library('ReportSupport');
    }

    public function index() {
        //$this->data = array();

        if ($this->input->post('action')) {
            $id = implode(",", $this->input->post('multyCheck'));
            $status['status'] = $this->input->post('action');
            $this->generalModel->updateTableValues('ec_customer', 'customerID IN (' . $id . ')', $status);
        }

        $this->data['breadcrumbs'] = "All Customer";
        $this->data['pageTitle'] = "All Customer";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(customerID) as tblCount', 'ec_customer', 'customerID!=1 AND status!="Deleted"', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Customers/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;      

        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $this->data['currentPage'] = $page + 1;
        $params = array(
            'email' => $this->input->get('email'),
            'limit' => $config['per_page'],
            'start' => $page);

        $this->data['result'] = $this->customerModel->selectAll($params);
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }
        
        
        
        $filter =   $this->reportsupport->filterInput('customer');
        $this->data['filter']   =   $filter;
        
        
        
        

        $this->render('manage/customer/list');
    }

    
    public function changeStatus() {

        $customerID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->logUserActivity('User status changed to - '.$status);
        $data['status'] = $status;
        $result = $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $customerID, $data);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function logout() {
        $this->userlogin->logout();
        $this->logUserActivity('User LoggedOut');
        redirect('manage/');
    }
    
    public function customerReport(){
        $this->load->library('Export');
        $params['fromDate']             = $this->input->post('fromDate');
        $params['toDate']               = $this->input->post('toDate');
        $params['ordered']              = $this->input->post('ordered');
        $params['purchaseCount']        = $this->input->post('purchaseCount');
        $params['amountPurchased']      = $this->input->post('amountPurchased');
        $params['averageCustomerStand'] = $this->input->post('averageCustomerStand');
        $customer = $this->customerModel->selectAllReport($params);
        //echo "<pre>"; print_r($customer); exit;
        if(!empty($customer)){
            $header  =   array_keys($customer[0]);
            foreach($customer as $key=>$value){
                foreach($value as $innerKey=>$innerVal){
                   $data[$key][$innerKey]         =   strip_tags($value[$innerKey]);
                }
            }
        }
        
        if($this->input->post('exportVal')=='exportVal'){
            $date   = date('d-m-Y');
            $fileName  =   'customers'.$date.'.csv'; 
            $export =   $this->export->exportFile($fileName,$header,$data);
            redirect($_SERVER['HTTP_REFERER']);
        }
        
        $this->data['header']   = $header;
        $this->data['data']     = $data;
        
        
         $this->render('manage/customer/ajaxCusomerReport');
    }
    
    public function customerReportExport(){
        $this->load->library('Export');
        $params['fromDate']             = $this->input->post('fromDate');
        $params['toDate']               = $this->input->post('toDate');
        $params['ordered']              = $this->input->post('ordered');
        $params['purchaseCount']        = $this->input->post('purchaseCount');
        $params['amountPurchased']      = $this->input->post('amountPurchased');
        $params['averageCustomerStand'] = $this->input->post('averageCustomerStand');
        $customer = $this->customerModel->selectAllReport($params);
        //echo "<pre>"; print_r($customer); exit;
        if(!empty($customer)){
            $header  =   array_keys($customer[0]);
            foreach($customer as $key=>$value){
                foreach($value as $innerKey=>$innerVal){
                   $data[$key][$innerKey]         =   strip_tags($value[$innerKey]);
                }
            }
        }
            $date   = date('d-m-Y');
            $fileName  =   'customers'.$date.'.csv'; 
            $export =   $this->export->exportFile($fileName,$header,$data);
            redirect($_SERVER['HTTP_REFERER']);
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Information extends Admin_Controller {

    //private $languageID;

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url_helper');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('InformationModel', 'informationModel');
        $this->setDirectory(FCPATH . "uploads/information/");
        $imageDimension = $this->getImageDimension('information_banner_dimen');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
        $this->imageConfig = array(
            'upload_path' => "./uploads/information/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );
    }

    public function index() {
        //$this->data = array();

        if ($this->input->post('action')) {
            $id = implode(",", $this->input->post('multyCheck'));
            $status['status'] = $this->input->post('action');
            $this->generalModel->updateTableValues('ec_information', 'informationID IN (' . $id . ')', $status);
        }
        $this->data['breadcrumbs'] = "All Information Page";
        $this->data['pageTitle'] = "All Information Page";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(informationID) as tblCount', 'ec_information', 'informationID!=1 AND status!="Deleted"', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Information/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;


        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'title' => $this->input->get('title'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );

        $this->data['result'] = $this->informationModel->selectAll($params);
         if(@$_GET['msg']){
            if($_GET['msg'] == 'active'){
             $this->data['success'] = "Information activated Successfully";
            }else if($_GET['msg'] == 'inactive'){
             $this->data['success'] = "Information inactivated Successfully";
            }else if($_GET['msg'] == 'delete'){
             $this->data['success'] = "Information deleted Successfully";
            }
        }
        if ($this->session->flashdata('message') != '') {
            $this->logUserActivity('Information Added');
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }
        $this->render('manage/information/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Information";
        $this->data['pageTitle'] = "Add Information";

        //form validation 
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        $this->form_validation->set_rules('description', 'Description', 'required');
//        $this->form_validation->set_rules('urlKey', 'URL Key', 'required|callback_check_slug');
        $this->form_validation->set_rules('metaTitle', 'Meta Title', 'required');
        $this->form_validation->set_rules('metaDescription', 'Meta Description', 'required');
        $this->form_validation->set_rules('metaKeyword', 'Meta Keyword', 'required');
        $this->form_validation->set_rules('sortOrder', 'Sort Order', 'required|numeric');
//$this->form_validation->set_rules('images', 'Image', 'required');
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            $_POST['image'] = '';
            $this->data['imgErr'] = '';
            if ($_FILES['image']['size'] > 0) {

                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    $errVal = 1;
                    $_POST['image'] = '';
                }
            }
            $this->session->set_flashdata('imgErr', $this->data['imgErr']);
            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'pageKey'   => $this->input->post('pageKey'),
//                'urlKey'    => url_title($this->input->post('urlKey'), 'dash', true),
                'metaTitle' => $this->input->post('metaTitle'),
                'metaDescription' => $this->input->post('metaDescription'),
                'metaKeyword' => $this->input->post('metaKeyword'),
                'sortOrder' => $this->input->post('sortOrder'),
                'image' => $this->input->post('image'),
                'status' => $this->input->post('status')
            );
            
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_information', $resData['xssData']);

            if ($result) {
                $informationDetail['nonXss'] = array(
                    'informationID' => $result,
                    'languageID' => $this->language,
                    'title' => $this->input->post('title'),
                    'description' => $this->input->post('description')
                );
                $informationDetail['xssData'] = $this->security->xss_clean($informationDetail['nonXss']);
                $this->generalModel->insertValue('ec_information_detail', $informationDetail['xssData']); //information Added

                $this->session->set_flashdata('message', 'Data Added Successfully');
                
                if ($errVal == 0) {
                    $this->logUserActivity('Information Updated');
                    redirect('manage/Information/index');
                } else {
                    redirect('manage/information/edit/' . $result . '/imgErr');
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/information/index');
            }
        }
        $this->render('manage/information/Add');
    }

    public function edit() {

        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Information";
        $this->data['pageTitle'] = "Edit Information";
       
        $informationID = $this->uri->segment(4);
        $imgErr = $this->uri->segment(5);
         if ($imgErr) {
            $this->data['imgErr'] = "Invalid Image, Please try again";
            $this->data['errVal'] = 1;
        }
        if ($this->session->flashdata('imgErr') != '') {
            $this->data['imgErr'] = $this->session->flashdata('imgErr');
            $this->data['errVal'] = 1;
        }
        //form validation 
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        $this->form_validation->set_rules('description', 'Description', 'required');
//        $this->form_validation->set_rules('urlKey', 'URL Key', 'required');
        $this->form_validation->set_rules('metaTitle', 'Meta Title', 'required');
        $this->form_validation->set_rules('metaDescription', 'Meta Description', 'required');
        $this->form_validation->set_rules('metaKeyword', 'Meta Keyword', 'required');
        $this->form_validation->set_rules('sortOrder', 'Sort Order', 'required|numeric');
        //image upload
        $this->form_validation->set_rules('images', 'Image', 'required');
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;

            if ($_FILES['image']['size'] > 0) {
               
                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    $errVal = 1;
                }
            }
            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
//                'urlKey' => url_title($this->input->post('urlKey'), 'dash', true),
                'metaTitle' => $this->input->post('metaTitle'),
                'metaDescription' => $this->input->post('metaDescription'),
                'metaKeyword' => $this->input->post('metaKeyword'),
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status')
            );

            if ($this->input->post('image')) {
                $resData['nonXss']['image'] = $this->input->post('image');
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $result = $this->generalModel->updateTableValues('ec_information', 'informationID=' . $informationID, $resData['xssData']);


            $informationDetail['nonXss'] = array(
                'informationID' => $informationID,
                'languageID' => $this->language,
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description')
            );
            $informationDetail['xssData'] = $this->security->xss_clean($informationDetail['nonXss']);
            $checkInformationDetail = $this->generalModel->getTableValue('informationID', 'ec_information_detail', 'informationID=' . $informationID . ' AND languageID=' . $this->language, FALSE);
            if ($checkInformationDetail) {

                $this->generalModel->updateTableValues('ec_information_detail', 'informationID=' . $informationID . ' AND languageID=' . $this->language, $informationDetail['xssData']); //information Added
            } else {
                $this->generalModel->insertValue('ec_information_detail', $informationDetail['xssData']); // insert detail data
            }
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Information Updated');
                if ($errVal == 0)
                    redirect('manage/Information/index');
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/Information/index');
            }
        }

        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_information', 'informationID=' . "$informationID" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->data['content']['detail'] = $this->generalModel->getTableValue('*', 'ec_information_detail', 'informationID=' . "$informationID" . ' AND languageID=' . $this->language, FALSE); //data for edit

        $this->render('manage/information/Add');
    }

    public function changeStatus() {

        $informationID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_information', 'informationID=' . $informationID, $this->data['data']);
        if($status == 'Active'){
            redirect('manage/Information?msg=active');
        }else if($status =='Inactive'){
            redirect('manage/Information?msg=inactive');
        }else if($status == 'Deleted'){
            redirect('manage/Information?msg=delete');
        }else{
            redirect('manage/Information');
        }
        //redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function check_slug($key) {
        if ($key != '') {
            $slug = url_title($key, 'dash', true);
            $res = $this->generalModel->getFieldValue('urlKey', 'ec_information', array('urlKey' => $slug));
            if ($res) {
                $this->form_validation->set_message('check_slug', 'URLKey already used.');
                return FALSE;
            } else {
                return true;
            }
        } else {
            $this->form_validation->set_message('check_slug', 'Please try another key');
            return FALSE;
        }
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Report extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('ReportSupport');
        $this->load->library('Export');
        $this->load->model('CategoryModel', 'categoryModel');
        $this->load->model('ProductModel', 'productModel');
        $this->load->model('ManufacturesModel', 'manufacturesModel');
        $this->load->model('OrderModel','orderModel');
        $this->load->model('CustomerModel','customerModel'); 
        $this->load->model('FilterModel','filterModel'); 
    }
    
    public function index(){
        $this->data['breadcrumbs'] = "Report";
        $this->data['pageTitle'] = "Report";
        $exportPage     =   $this->input->get('exportPage');
        $filter =   $this->reportsupport->filterInput($exportPage);
        $this->data['filter']   =   $filter;
        
        $this->data['exportPage']   =   $exportPage;
        
        $this->form_validation->set_rules('exportPage', 'Page', 'required');
        if ($this->form_validation->run() === TRUE) {
            
                $pageName = $this->input->post('exportPage'); 
                $date   = date('d-m-Y');
                $fileName = $this->input->post('exportPage').$date.'.csv'; 
                $params     =   array();
                $params = array(
                'start'  => $this->input->post('page'),
                'limit'  => $this->input->post('limit'),
                'languageID' => $this->language);  
                if($pageName=='customer'){ // For customers
                   
                    $params['fromDate']             = $this->input->post('fromDate');
                    $params['toDate']               = $this->input->post('toDate');
                    $params['ordered']              = $this->input->post('ordered');
                    $params['purchaseCount']        = $this->input->post('purchaseCount');
                    $params['amountPurchased']      = $this->input->post('amountPurchased');
                    $params['averageCustomerStand'] = $this->input->post('averageCustomerStand');
                    $customer = $this->customerModel->selectAllReport($params);
                    //echo "<pre>"; print_r($customer); exit;
                    if(!empty($customer)){
                        $header  =   array_keys($customer[0]);
                        foreach($customer as $key=>$value){
                            foreach($value as $innerKey=>$innerVal){
                               $data[$key][$innerKey]         =   strip_tags($value[$innerKey]);
                            }
                        }
                    }
                }
                elseif($pageName=='order'){ // For Order
                    $fromDate = $this->input->post('fromDate'); 
                    $params['fromDate']             = $this->input->post('fromDate');
                    $params['toDate']               = $this->input->post('toDate');
                    $params['orderStatusID']        = $this->input->post('orderStatusID');
                    $order = $this->orderModel->selectReport($params);
                   // echo "<pre>"; print_r($order); exit;
                    if(!empty($order)){
                        $header  =   array_keys($order[0]);
                        foreach($order as $key=>$value){
                            foreach($value as $innerKey=>$innerVal){
                               $data[$key][$innerKey]         =   strip_tags($value[$innerKey]);
                            }
                        }
                    }
                }
                elseif($pageName=='product'){ // For Product
                    $fromDate = $this->input->post('fromDate'); 
                    $params['fromDate']         = $this->input->post('fromDate');
                    $params['toDate']           = $this->input->post('toDate');
                    $params['ordered']          = $this->input->post('ordered');
                    $params['outofstock']       = $this->input->post('outofstock');
                    $params['manufacturerID']   = $this->input->post('manufacturerID');
                    $product = $this->productModel->selectProductDetailReport($params);
                    if(!empty($product)){
                        $header  =   array_keys($product[0]);
                        foreach($product as $key=>$productValue){
                            foreach($productValue as $innerKey=>$innerVal){
                               $data[$key][$innerKey]         =   strip_tags($productValue[$innerKey]);
                            }
                        }
                    }
                }
              
                if(!empty($data)){
                    $this->data['header']   = $header;
                    $this->data['data']     = $data;
                    $this->session->set_userdata('exportVal',$data);
                    $this->session->set_userdata('fileName',$fileName);
                }
                
        }
        
        if($this->uri->segment(4)=='exportVal'){
            $data       =   $this->session->userdata('exportVal');
            $fileName   =   $this->session->userdata('fileName');
            $header  =   array_keys($data[0]);
            $export =   $this->export->exportFile($fileName,$header,$data);
            redirect($_SERVER['HTTP_REFERER']);
        }
        
        $this->render('manage/report/Add');
    }
}
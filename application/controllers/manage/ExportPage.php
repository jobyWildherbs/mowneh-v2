<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ExportPage extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('Export');
        $this->load->model('CategoryModel', 'categoryModel');
        $this->load->model('ProductModel', 'productModel');
        $this->load->model('ManufacturesModel', 'manufacturesModel');
        $this->load->model('OrderModel','orderModel');
        $this->load->model('CustomerModel','customerModel'); 
        $this->load->model('FilterModel','filterModel'); 
    }
    
    public function index(){
        $this->data['breadcrumbs'] = "Download";
        $this->data['pageTitle'] = "Download";
        $this->form_validation->set_rules('exportPage', 'Page', 'required');
        $this->form_validation->set_rules('field[]', 'Fields', 'required');
        $exportPage     =   $this->input->get('exportPage');
        $exportField    =   $this->export->tableRows($exportPage);
        $this->data['exportField']  =   $exportField;
        $this->data['exportPage']   =   $exportPage;
        
        if ($this->form_validation->run() === TRUE) {
            $fileName = $this->input->post('exportPage').'.csv'; 
            $pageName = $this->input->post('exportPage'); 
            $field  =   $this->input->post('field[]');
            $fields  =   '';
            $count  =   1;
            foreach($field as $fieldVal){
                $fields  .=  $fieldVal;
                if(count($field)!=$count){
                    $fields  .=  ',';
                }
                $count++;
                
            }
                $params     =   array();
                $params = array(
                'fields' => $fields,
                'start'  => $this->input->post('page'),
                'limit'  => $this->input->post('limit'),
                'languageID' => $this->language);
		
                
                
                foreach($field as $fieldVal){
                    $selectedFieldExp   =   explode(".",$fieldVal);
                    $selectedField[]      =   $selectedFieldExp[1];
                    
                }
                foreach($exportField as $key=>$expfield){
                    foreach($expfield as $keyVal=>$expfieldValue){
                        $fielsAdded[$keyVal]    =   $expfieldValue;
                    }
                }
                foreach($selectedField as $selectedKey){
                     $header[]  =   $fielsAdded[$selectedKey];
                }
                $data   =   array();
                
                if($pageName=='category'){ // For category
                    $category = $this->categoryModel->categoryExport($params);
                    if(!empty($category)){
                        foreach($category as $key=>$catValue){
                            foreach($catValue as $innerKey=>$innerVal){
                               $data[$key][$innerKey]         =   strip_tags($catValue[$innerKey]);
                            }
                        }
                    }
                }
                else if($pageName=='product'){ // For Product
                    $product = $this->productModel->productExport($params);
                    //echo "<pre>"; print_r($product); exit;
                    if(!empty($product)){
                        foreach($product as $key=>$productValue){
                            foreach($productValue as $innerKey=>$innerVal){
                               $data[$key][$innerKey]         =   strip_tags($productValue[$innerKey]);
                            }
                        }
                    }
                }
                else if($pageName=='brands'){ // For Product
                    $brand = $this->manufacturesModel->brandExport($params);
                    //echo "<pre>"; print_r($brand); exit;
                    if(!empty($brand)){
                        foreach($brand as $key=>$brandValue){
                            foreach($brandValue as $innerKey=>$innerVal){
                               $data[$key][$innerKey]         =   strip_tags($brandValue[$innerKey]);
                            }
                        }
                    }
                }else if($pageName=='order'){ // For Product
                    
                    $order = $this->orderModel->orderExport($params);
                    //echo "<pre>"; print_r($order); exit;
                    if(!empty($order)){
                        foreach($order as $key=>$orderValue){
                            foreach($orderValue as $innerKey=>$innerVal){
                               $data[$key][$innerKey]         =   strip_tags($orderValue[$innerKey]);
                            }
                        }
                    }
                }else if($pageName=='customers'){ // For customers
                    $customer = $this->customerModel->customerExport($params);
                    
                    if(!empty($customer)){
                        foreach($customer as $key=>$customerValue){
                            foreach($customerValue as $innerKey=>$innerVal){
                               $data[$key][$innerKey]         =   strip_tags($customerValue[$innerKey]);
                            }
                        }
                    }
                }
                else if($pageName=='filter'){ // For customers
                    $filter = $this->filterModel->filterExport($params);
                    
                    if(!empty($filter)){
                        foreach($filter as $key=>$filterValue){
                            foreach($filterValue as $innerKey=>$innerVal){
                               $data[$key][$innerKey]         =   strip_tags($filterValue[$innerKey]);
                            }
                        }
                    }
                }
      
                $export =   $this->export->exportFile($fileName,$header,$data);
                redirect($_SERVER['HTTP_REFERER']); 
            
        }
        $this->render('manage/export/Add');
    }
}


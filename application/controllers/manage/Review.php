<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends Admin_Controller {

    private $imageConfig = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('ReviewModel', 'reviewModel');
//        $imageDimension = $this->getImageDimension('home_review_slider_dimen');
//        $imageSet = implode(' x ', $imageDimension);
//        $this->data['imageDimension'] = $imageDimension;
//        $this->data['imageSet'] = $imageSet;
//        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
//        $this->data['allowedType'] = $this->allowedImageTypes;
//        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
//        $this->imageConfig = array(
//            'upload_path' => "./uploads/review/",
//            'allowed_types' => $this->allowedImageTypes,
//            'max_size' => $this->maxImageUploadSize,
//            'max_width' => $imageDimension[0],
//            'max_height' => $imageDimension[1],
//            'min_width' => $imageDimension[0],
//            'min_height' => $imageDimension[1],
//            'encrypt_name' => TRUE,
//        );
    }

    public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_review_prod', 'reviewID IN (' . $id . ')', $status);
            }
        }
        $this->data['breadcrumbs'] = "All Review";
        $this->data['pageTitle'] = "All Review";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(reviewID) as tblCount', 'ec_review_prod', 'reviewID!=0 AND status!="Deleted"', FALSE); //data count for pagination
        
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Review/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'name' => $this->input->get('name'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );

        $this->data['result'] = $this->reviewModel->selectAll($params);

        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/review/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Review";
        $this->data['pageTitle'] = "Add Review";

        //form validation 
        $this->form_validation->set_rules('firstname', 'name', 'required|xss_clean');
        $this->form_validation->set_rules('comments', 'comments', 'required');
       

        if ($this->form_validation->run() === TRUE) {
//            $errVal = 0;
//            $_POST['image'] = '';
//            if ($_FILES['image']['size'] > 0) {
//                $this->load->library('upload', $this->imageConfig);
//                if ($this->upload->do_upload('image')) {
//                    $_POST['image'] = $this->upload->data('file_name');
//                    // in case you need to save it into a database
//                } else {
//                    $this->data['imgErr'] = $this->upload->display_errors();
//                    $errVal = 1;
//                    $_POST['image'] = '';
//                }
//            } else {
//                $this->data['imgErr'] = "No Image File Uploaded";
//                $errVal = 1;
//                $_POST['image'] = '';
//            }
//
//            $this->session->set_flashdata('imgErr', $this->data['imgErr']);
//            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'status' => $this->input->post('status'),
                'rating' => $this->input->post('rating')
            );
//            if ($errVal == 1 || $_POST['image'] == '') {
//                $resData['nonXss']['status'] = 'Inactive';
//            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_review_prod', $resData['xssData']);
echo $result;exit;
            if ($result) {
                $reviewDetail['nonXss'] = array(
                    'reviewID' => $result,
                    'languageID' => $this->language,
                    'name' => $this->input->post('name'),
                    'comments' => $this->input->post('comments'),
                    
                );
                $reviewDetail['xssData'] = $this->security->xss_clean($reviewDetail['nonXss']);
                //echo $reviewDetail;exit;
                $this->generalModel->insertValue('ec_review_prod_detail', $reviewDetail['xssData']); //information Added

                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Review Added');
//                if ($errVal == 0) {
                    redirect('manage/review/index');
//                } else {
//                    redirect('manage/review/edit/' . $result . '/imgErr');
//                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/review/index');
            }
        }
        $this->render('manage/review/Add');
    }

    public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Review";
        $this->data['pageTitle'] = "Edit Review";

        $reviewID = $this->uri->segment(4);
//        $imgErr = $this->uri->segment(5);
//        if ($imgErr) {
//            $this->data['imgErr'] = "Invalid Image, Please try again";
//            $this->data['errVal'] = 1;
//        }
//        if ($this->session->flashdata('imgErr') != '') {
//            $this->data['imgErr'] = $this->session->flashdata('imgErr');
//            $this->data['errVal'] = 1;
//        }
        //form validation 
          $this->form_validation->set_rules('name', 'name', 'required|xss_clean');
        $this->form_validation->set_rules('comments', 'comments', 'required');
        //image upload
        if ($this->form_validation->run() === TRUE) {
//            $errVal = 0;
//            if ($_FILES['image']['size'] > 0) {
//
//                $this->load->library('upload', $this->imageConfig);
//                if ($this->upload->do_upload('image')) {
//                    $_POST['image'] = $this->upload->data('file_name');
//                    // in case you need to save it into a database
//                } else {
//                    $this->data['imgErr'] = $this->upload->display_errors();
//                    //$_POST['image'] = '';
//                    $errVal = 1;
//                }
//            }
//
//            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
               'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'status' => $this->input->post('status'),
                'rating' => $this->input->post('rating')
            );
            //if ($errVal == 1) {
            //$resData['nonXss']['status'] = 'Inactive';
            //}
            if ($this->input->post('image')) {
                $resData['nonXss']['image'] = $this->input->post('image');
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_review_prod', 'reviewID=' . $reviewID, $resData['xssData']);
            $reviewDetail['nonXss'] = array(
                'reviewID' => $reviewID,
                'languageID' => $this->language,
                'name' => $this->input->post('name'),
                'comments' => $this->input->post('comments'),
                    
            );

            $reviewDetail['xssData'] = $this->security->xss_clean($reviewDetail['nonXss']);
            $checkReviewDetail = $this->generalModel->getTableValue('reviewID', 'ec_review_prod_detail', 'reviewID=' . $reviewID . ' AND languageID=' . $this->language, FALSE);
            if ($checkReviewDetail) {
                $this->generalModel->updateTableValues('ec_review_prod_detail', 'reviewID=' . $reviewID . ' AND languageID=' . $this->language, $reviewDetail['xssData']); //information Added
            } else {
                $this->generalModel->insertValue('ec_review_prod_detail', $reviewDetail['xssData']); // insert detail data
            }
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Review Updated');
//                if ($errVal == 0) {
                    redirect('manage/review/index');
//                } else {
//                    $this->session->set_flashdata('imgErr', $this->data['imgErr']);
//                    redirect('manage/review/edit/' . $reviewID);
//                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/review/index');
            }
        }

        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_review_prod', 'reviewID=' . "$reviewID" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->data['content']['detail'] = $this->generalModel->getTableValue('*', 'ec_review_prod_detail', 'reviewID=' . "$reviewID" . ' AND languageID=' . $this->language, FALSE); //data for edit

        $this->render('manage/review/Add');
    }

    public function changeStatus() {

        $reviewID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_review_prod', 'reviewID=' . $reviewID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }



}

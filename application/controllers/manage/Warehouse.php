<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse extends Admin_Controller {

    //private $languageID;

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('WarehouseModel', 'warehouseModel');
        //$this->languageID = 1;
    }

    public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_warehouse', 'warehouseID IN (' . $id . ')', $status);
            }
        }
        $this->data['breadcrumbs'] = "All Warehouse";
        $this->data['pageTitle'] = "All Warehouse";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(warehouseID) as tblCount', 'ec_warehouse', 'warehouseID!=1 AND status!="Deleted"', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Warehouse/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

       
        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
         $params = array(
             'name' => $this->input->get('name'),
            'limit' => $config['per_page'],
            'start' => $page);

        $this->data['result'] = $this->warehouseModel->selectAll($params);
//        if($this->session->flashdata('message')!= ''){
//        $this->data['success']     =  $this->session->flashdata('message');
//        }else{
//        //echo $this->data['success'];
//        $this->data['failMessage'] = $this->session->flashdata('failMessage');
//        }
        $this->render('manage/warehouse/list');
    }

    public function add() {

        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Warehouse";
        $this->data['pageTitle'] = "Add Warehouse";
        $this->data['stateList'] = $this->warehouseModel->getTableValue('stateID,name', 'ec_state', 'countryID="248" AND status="Active"', TRUE);
        $this->data['countryList'] = $this->warehouseModel->getTableValue('name', 'ec_country', 'countryID="173" AND status="Active"', TRUE);

        // form validation 
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean');
        $this->form_validation->set_rules('addressLine1', 'addressLine1', 'required|xss_clean');
        $this->form_validation->set_rules('addressLine2', 'addressLine2', 'required|xss_clean');
        // $this->form_validation->set_rules('city', 'city', 'required|xss_clean');
        // $this->form_validation->set_rules('country', 'country', 'required|xss_clean');
        $this->form_validation->set_rules('postCode', 'postCode', 'required|xss_clean');
        $this->form_validation->set_rules('longitude', 'longitude', 'required|xss_clean');
        $this->form_validation->set_rules('latitude', 'latitude', 'required|xss_clean');



        if ($this->form_validation->run() === TRUE) {
            $resData['nonXss'] = array(
                'name' => $this->input->post('name'),
                'addressLine1' => $this->input->post('addressLine1'),
                'addressLine2' => $this->input->post('addressLine2'),
                //'city' => $this->input->post('city'),
                'stateID' => $this->input->post('stateID'),
                'countryID' => $this->input->post('countryID'),
                'postCode' => $this->input->post('postCode'),
                'longitude' => $this->input->post('longitude'),
                'latitude' => $this->input->post('latitude'),
                'status' => $this->input->post('status')
            );
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $result = $this->generalModel->insertValue('ec_warehouse', $resData['xssData']);


            if ($result) {

                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Warehouse Added');
                redirect('manage/Warehouse/index');
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/Warehouse/index');
            }
        }
        $this->render('manage/warehouse/Add');
    }

    public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Warehouse";
        $this->data['pageTitle'] = "Edit Warehouse";
        $warehouseID = $this->uri->segment(4);
        $this->data['stateList'] = $this->warehouseModel->getTableValue('stateID,name', 'ec_state', 'countryID="248" AND status="Active"', TRUE);
        $this->data['countryList'] = $this->warehouseModel->getTableValue('countryID,name', 'ec_country', 'countryID="173" AND status="Active"', TRUE);
        //form validation 
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean');
        $this->form_validation->set_rules('addressLine1', 'addressLine1', 'required|xss_clean');
        $this->form_validation->set_rules('addressLine2', 'addressLine2', 'required|xss_clean');
        //$this->form_validation->set_rules('city', 'city', 'required|xss_clean');
        //$this->form_validation->set_rules('country', 'country', 'required|xss_clean');
        $this->form_validation->set_rules('postCode', 'postCode', 'required|xss_clean');
        $this->form_validation->set_rules('longitude', 'longitude', 'required|xss_clean');
        $this->form_validation->set_rules('latitude', 'latitude', 'required|xss_clean');
        //image upload
        if ($this->form_validation->run() === TRUE) {


            $resData['nonXss'] = array(
                'name' => $this->input->post('name'),
                'addressLine1' => $this->input->post('addressLine1'),
                'addressLine2' => $this->input->post('addressLine2'),
                'stateID' => $this->input->post('stateID'),
                'countryID' => $this->input->post('countryID'),
                'postCode' => $this->input->post('postCode'),
                'longitude' => $this->input->post('longitude'),
                'latitude' => $this->input->post('latitude'),
                'status' => $this->input->post('status')
            );


            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $result = $this->generalModel->updateTableValues('ec_warehouse', 'warehouseID=' . $warehouseID, $resData['xssData']);

            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Warehouse Updated');
                redirect('manage/Warehouse/index');
            } else {
                 $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/Warehouse/index');
            }
        }

        $this->data['content'] = $this->generalModel->getTableValue('*', 'ec_warehouse', 'warehouseID=' . "$warehouseID" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->render('manage/warehouse/Add');
    }

    public function changeStatus() {

        $warehouseID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_warehouse', 'warehouseID=' . $warehouseID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }
    
//    public function warehouseCityAdd() {
//        $cityList = $this->warehouseModel->getTableValue('cityID,countryID', 'ec_cities', 'countryID="173" AND status="Active"', TRUE);
//       
//        foreach($cityList as $cityData){
//            $resData['nonXss'] = array(
//                'warehouseID' => 1,
//                'cityID' => $cityData["cityID"],
//            );
//            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
//          $this->generalModel->insertValue('ec_warehouse_delivery_location', $resData['xssData']);
//        }
//    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MobileBanner extends Admin_Controller {

    private $imageConfig = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('MobileBannerModel', 'mobileBannerModel');
        $imageDimension = $this->getImageDimension('mobile_banner_slider_dimen');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
        $this->imageConfig = array(
            'upload_path' => "./uploads/banner/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );
    }

    public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_api_banner', 'bannerID IN (' . $id . ')', $status);
            }
        }
        $this->data['breadcrumbs'] = "All Home Page Slider";
        $this->data['pageTitle'] = "All Home Page Slider";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(bannerID) as tblCount', 'ec_api_banner', 'bannerID!=1 AND status!="Deleted"', FALSE); //data count for pagination
        
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/MobileBanner/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'title' => $this->input->get('title'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );

        $this->data['result'] = $this->mobileBannerModel->selectAll($params);
        if(@$_GET['msg']){
            if($_GET['msg'] == 'active'){
             $this->data['success'] = "Mobile Banner activated Successfully";
            }else if($_GET['msg'] == 'inactive'){
             $this->data['success'] = "Mobile Banner inactivated Successfully";
            }else if($_GET['msg'] == 'delete'){
             $this->data['success'] = "Mobile Banner deleted Successfully";
            }
        }
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/mobileBanner/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Mobile Home Page Slider";
        $this->data['pageTitle'] = "Add Mobile Home Page Slider";

        //form validation 
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        $this->form_validation->set_rules('images', 'Image', 'required|xss_clean');
        $this->form_validation->set_rules('sortOrder', 'sort order', 'required|numeric');

        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            $_POST['image'] = '';
            $this->data['imgErr'] = '';
            if ($_FILES['image']['size'] > 0) {
                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    $errVal = 1;
                    $_POST['image'] = '';
                }
            } else {
                $this->data['imgErr'] = "No Image File Uploaded";
                $errVal = 1;
                $_POST['image'] = '';
            }

            $this->session->set_flashdata('imgErr', $this->data['imgErr']);
            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status')
            );
            if ($errVal == 1 || $_POST['image'] == '') {
                $resData['nonXss']['status'] = 'Inactive';
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_api_banner', $resData['xssData']);

            if ($result) {
                $bannerDetail['nonXss'] = array(
                    'bannerID' => $result,
                    'languageID' => $this->language,
                    'title' => $this->input->post('title'),
                    'image' => $this->input->post('image')
                );
                $bannerDetail['xssData'] = $this->security->xss_clean($bannerDetail['nonXss']);
                $this->generalModel->insertValue('ec_api_banner_detail', $bannerDetail['xssData']); //information Added

                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Banner Added');
                if ($errVal == 0) {
                    redirect('manage/MobileBanner/index');
                } else {
                    redirect('manage/MobileBanner/edit/' . $result . '/imgErr');
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/MobileBanner/index');
            }
        }
        $this->render('manage/mobileBanner/Add');
    }

    public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Mobile Slider";
        $this->data['pageTitle'] = "Edit Mobile Slider";

        $bannerID = $this->uri->segment(4);
        $imgErr = $this->uri->segment(5);
        if ($imgErr) {
            $this->data['imgErr'] = "Invalid Image, Please try again";
            $this->data['errVal'] = 1;
        }
        if ($this->session->flashdata('imgErr') != '') {
            $this->data['imgErr'] = $this->session->flashdata('imgErr');
            $this->data['errVal'] = 1;
        }
        //form validation 
//        $this->form_validation->set_rules('images', 'Image', 'required|xss_clean');
        $this->form_validation->set_rules('title', 'title', 'required|xss_clean');
        $this->form_validation->set_rules('sortOrder', 'Sort Order', 'required|numeric');
        //image upload
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            if ($_FILES['image']['size'] > 0) {

                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    //$_POST['image'] = '';
                    $errVal = 1;
                }
            }

            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'sortOrder' => $this->input->post('sortOrder'),
                'status' => $this->input->post('status')
            );
            //if ($errVal == 1) {
            //$resData['nonXss']['status'] = 'Inactive';
            //}
            
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_api_banner', 'bannerID=' . $bannerID, $resData['xssData']);
            $bannerDetail['nonXss'] = array(
                'bannerID' => $bannerID,
                'languageID' => $this->language,
                'title' => $this->input->post('title')
            );
            
            if ($this->input->post('image')) {
                $bannerDetail['nonXss']['image'] = $this->input->post('image');
            }

            $bannerDetail['xssData'] = $this->security->xss_clean($bannerDetail['nonXss']);
            $checkBannerDetail = $this->generalModel->getTableValue('bannerID', 'ec_api_banner_detail', 'bannerID=' . $bannerID . ' AND languageID=' . $this->language, FALSE);
            if ($checkBannerDetail) {
                $this->generalModel->updateTableValues('ec_api_banner_detail', 'bannerID=' . $bannerID . ' AND languageID=' . $this->language, $bannerDetail['xssData']); //information Added
            } else {
                $this->generalModel->insertValue('ec_api_banner_detail', $bannerDetail['xssData']); // insert detail data
            }
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Banner Updated');
                if ($errVal == 0) {
                    redirect('manage/MobileBanner/index');
                } else {
                    $this->session->set_flashdata('imgErr', $this->data['imgErr']);
                    redirect('manage/MobileBanner/edit/' . $bannerID);
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/MobileBanner/index');
            }
        }

        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_api_banner', 'bannerID=' . "$bannerID" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->data['content']['detail'] = $this->generalModel->getTableValue('*', 'ec_api_banner_detail', 'bannerID=' . "$bannerID" . ' AND languageID=' . $this->language, FALSE); //data for edit

        $this->render('manage/mobileBanner/Add');
    }

    public function changeStatus() {

        $bannerID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_api_banner', 'bannerID=' . $bannerID, $this->data['data']);
        if($status == 'Active'){
            redirect('manage/MobileBanner?msg=active');
        }else if($status =='Inactive'){
            redirect('manage/MobileBanner?msg=inactive');
        }else if($status == 'Deleted'){
            redirect('manage/MobileBanner?msg=delete');
        }else{
            redirect('manage/MobileBanner');
        }
        //redirect($_SERVER['HTTP_REFERER']);
    }

    public function valid_url_format($str) {
        //echo $str."======test";exit;
        if (empty($str)) {
            $this->form_validation->set_message('valid_url_format', 'The {field} field can not be empty');
            return FALSE;
        } elseif (preg_match('/^(?:([^:]*)\:)?\/\/(.+)$/', $str, $matches)) {
            if (empty($matches[2])) {
                $this->form_validation->set_message('valid_url_format', 'Invalid url');
                return FALSE;
            } elseif (!in_array(strtolower($matches[1]), array('http', 'https'), TRUE)) {
                $this->form_validation->set_message('valid_url_format', 'Invalid url');
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $this->form_validation->set_message('valid_url_format', 'Invalid url');
            return FALSE;
        }
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MenuOffer extends Admin_Controller {

    private $imageConfig = array();

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('MenuOfferModel', 'menuOfferModel');
        //$this->load->model('CategoryModel', 'categoryModel');
        $imageDimension = $this->getImageDimension('menu_offer_image_dimen');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
        $this->imageConfig = array(
            'upload_path' => "./uploads/menuOffer/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );
    }

    public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_menu_offer_image', 'menuOfferId IN (' . $id . ')', $status);
            }
        }
        $this->data['breadcrumbs'] = "All Menu Offer Image";
        $this->data['pageTitle'] = "All Menu Offer Image";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(menuOfferId) as tblCount', 'ec_menu_offer_image', 'menuOfferId!=1 AND status!="Deleted"', FALSE); //data count for pagination
        
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/MenuOffer/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'name' => $this->input->get('name'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );

        $this->data['result'] = $this->menuOfferModel->selectAll($params);

        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/menuoffer/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Menu Offer Image";
        $this->data['pageTitle'] = "Add Menu Offer Image";
        $this->data['parentCategory'] = $this->menuOfferModel->selectCategory('', $this->language); // parent category List
       // $this->data['result'] = $this->menuOfferModel->selectAll($params);
        //form validation 
        $this->form_validation->set_rules('name', 'title', 'required|xss_clean');
        $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('url', 'URL Key', 'callback_valid_url_format');

        
        

        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            $_POST['image'] = '';
            if ($_FILES['image']['size'] > 0) {
                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    $errVal = 1;
                    $_POST['image'] = '';
                }
            } else {
                $this->data['imgErr'] = "No Image File Uploaded";
                $errVal = 1;
                $_POST['image'] = '';
            }

            $this->session->set_flashdata('imgErr', $this->data['imgErr']);
            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'url' => $this->input->post('url'),
                'image' => $this->input->post('image'),
                'parentID' => $this->input->post('parentID'),
                'categoryID'=>$this->input->post('categoryID'),
                'status' => $this->input->post('status')
            );
            if ($errVal == 1 || $_POST['image'] == '') {
                $resData['nonXss']['status'] = 'Inactive';
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_menu_offer_image', $resData['xssData']);

            if ($result) {
                $menuofferDetail['nonXss'] = array(
                    'menuOfferId' => $result,
                    'languageID' => $this->language,
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description')
                );
                $menuofferDetail['xssData'] = $this->security->xss_clean($menuofferDetail['nonXss']);
                $this->generalModel->insertValue('ec_menu_offer_detail', $menuofferDetail['xssData']); //information Added

                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('MenuOffer Added');
                if ($errVal == 0) {
                    redirect('manage/menuoffer/index');
                } else {
                    redirect('manage/menuoffer/edit/' . $result . '/imgErr');
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/menuoffer/index');
            }
        }
        $this->render('manage/menuoffer/Add');
    }

    public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Menu Offer";
        $this->data['pageTitle'] = "Edit Menu Offer";
$this->data['parentCategory'] = $this->menuOfferModel->selectCategory('', $this->language); // parent category List
        $menuOfferId = $this->uri->segment(4);
        $imgErr = $this->uri->segment(5);
        if ($imgErr) {
            $this->data['imgErr'] = "Invalid Image, Please try again";
            $this->data['errVal'] = 1;
        }
        if ($this->session->flashdata('imgErr') != '') {
            $this->data['imgErr'] = $this->session->flashdata('imgErr');
            $this->data['errVal'] = 1;
        }
        //form validation 
        $this->form_validation->set_rules('name', 'title', 'required|xss_clean');
        $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('url', 'URL Key', 'callback_valid_url_format');
       
        //image upload
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            if ($_FILES['image']['size'] > 0) {

                $this->load->library('upload', $this->imageConfig);
                if ($this->upload->do_upload('image')) {
                    $_POST['image'] = $this->upload->data('file_name');
                    // in case you need to save it into a database
                } else {
                    $this->data['imgErr'] = $this->upload->display_errors();
                    //$_POST['image'] = '';
                    $errVal = 1;
                }
            }

            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'url' => $this->input->post('url'),
                //'image' => $this->input->post('image'),
                'parentID'=>$this->input->post('parentID'),
                'status' => $this->input->post('status')
            );
            //if ($errVal == 1) {
            //$resData['nonXss']['status'] = 'Inactive';
            //}
            if ($this->input->post('image')) {
                $resData['nonXss']['image'] = $this->input->post('image');
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_menu_offer_image', 'menuOfferId=' . $menuOfferId, $resData['xssData']);
            $menuofferDetail['nonXss'] = array(
                'menuOfferId' => $menuOfferId,
                    'languageID' => $this->language,
                    'name' => $this->input->post('name'),
                    'description' => $this->input->post('description')
            );

            $menuofferDetail['xssData'] = $this->security->xss_clean($menuofferDetail['nonXss']);
            $checkMenuOfferDetail = $this->generalModel->getTableValue('menuOfferId', 'ec_menu_offer_detail', 'menuOfferId=' . $menuOfferId . ' AND languageID=' . $this->language, FALSE);
            if ($checkMenuOfferDetail) {
                $this->generalModel->updateTableValues('ec_menu_offer_detail', 'menuOfferId=' . $menuOfferId . ' AND languageID=' . $this->language, $menuofferDetail['xssData']); //information Added
            } else {
                $this->generalModel->insertValue('ec_menu_offer_detail', $menuofferDetail['xssData']); // insert detail data
            }
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('MenuOffer Updated');
                if ($errVal == 0) {
                    redirect('manage/menuoffer/index');
                } else {
                    $this->session->set_flashdata('imgErr', $this->data['imgErr']);
                    redirect('manage/menuoffer/edit/' . $menuOfferId);
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/menuoffer/index');
            }
        }

        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_menu_offer_image', 'menuOfferId=' . "$menuOfferId" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->data['content']['detail'] = $this->generalModel->getTableValue('*', 'ec_menu_offer_detail', 'menuOfferId=' . "$menuOfferId" . ' AND languageID=' . $this->language, FALSE); //data for edit

        $this->render('manage/menuoffer/Add');
    }

    public function changeStatus() {

        $menuOfferId = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_menu_offer_image', 'menuOfferId=' . $menuOfferId, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function valid_url_format($str) {
        //echo $str."======test";exit;
        if (empty($str)) {
            $this->form_validation->set_message('valid_url_format', 'The {field} field can not be empty');
            return FALSE;
        } elseif (preg_match('/^(?:([^:]*)\:)?\/\/(.+)$/', $str, $matches)) {
            if (empty($matches[2])) {
                $this->form_validation->set_message('valid_url_format', 'Invalid url');
                return FALSE;
            } elseif (!in_array(strtolower($matches[1]), array('http', 'https'), TRUE)) {
                $this->form_validation->set_message('valid_url_format', 'Invalid url');
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $this->form_validation->set_message('valid_url_format', 'Invalid url');
            return FALSE;
        }
    }

}

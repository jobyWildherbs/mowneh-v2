<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Specialprice extends Admin_Controller {

    

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('SpecialpriceModel', 'specialpriceModel');

    }

    public function index() {
        //$this->data = array();
        $this->form_validation->set_rules('multyCheck[]', 'Check box', 'required');
        //form validation 
        if ($this->form_validation->run() === TRUE) {
            if ($this->input->post('action')) {
                $id = implode(",", $this->input->post('multyCheck'));
                $status['status'] = $this->input->post('action');
                $this->generalModel->updateTableValues('ec_product_special_price', 'specialPriceID IN (' . $id . ')', $status);
            }
        }
        $this->data['breadcrumbs'] = "All Special Price";
        $this->data['pageTitle'] = "All Special Price";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(specialPriceID) as tblCount', 'ec_product_special_price', 'specialPriceID!=1 AND status!="Deleted"', FALSE); //data count for pagination
        
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Specialprice/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'name' => $this->input->get('name'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language
        );

        $this->data['result'] = $this->specialpriceModel->selectAll($params);

        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }

        $this->render('manage/specialprice/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Special Price";
        $this->data['pageTitle'] = "Add Special Price";
        $this->data['parentCategory'] = $this->specialpriceModel->selectCategory('', $this->language); // parent category List
       // $this->data['result'] = $this->specialpriceModel->selectAll($params);
        //form validation 
        $this->form_validation->set_rules('productID', 'Product', 'required|xss_clean');
        
        
        

        if ($this->form_validation->run() === TRUE) {
           
            $resData['nonXss'] = array(
                'productID' => $this->input->post('productID'),
                'customerGroupID' => $this->input->post('customerGroupID'),
                'priority' => $this->input->post('priority'),
                'price'=>$this->input->post('price'),
                'dateStart'=>$this->input->post('dateStart'),
                'dateEnd'=>$this->input->post('dateEnd'),
                'status' => $this->input->post('status')
            );
//            if ($errVal == 1 || $_POST['image'] == '') {
//                $resData['nonXss']['status'] = 'Inactive';
//            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_product_special_price', $resData['xssData']);

            if ($result) {
               

                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Specialprice Added');
                
                    redirect('manage/specialprice/index');
                
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/specialprice/index');
            }
        }
        $this->render('manage/specialprice/Add');
    }

    public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Menu Offer";
        $this->data['pageTitle'] = "Edit Menu Offer";
        $this->data['parentCategory'] = $this->specialpriceModel->selectCategory('', $this->language); // parent category List
        $specialPriceID = $this->uri->segment(4);
        
        //form validation 
        $this->form_validation->set_rules('productID', 'Product', 'required|xss_clean');
       
        //image upload
        if ($this->form_validation->run() === TRUE) {
           
            $resData['nonXss'] = array(
                 'productID' => $this->input->post('productID'),
                'customerGroupID' => $this->input->post('customerGroupID'),
                'priority' => $this->input->post('priority'),
                'price'=>$this->input->post('price'),
                'dateStart'=>$this->input->post('dateStart'),
                'dateEnd'=>$this->input->post('dateEnd'),
                'status' => $this->input->post('status')
            );
            //if ($errVal == 1) {
            //$resData['nonXss']['status'] = 'Inactive';
            //}
//            if ($this->input->post('image')) {
//                $resData['nonXss']['image'] = $this->input->post('image');
//            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->updateTableValues('ec_product_special_price', 'specialPriceID=' . $specialPriceID, $resData['xssData']);
            
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Specialprice Updated');
               
                    redirect('manage/specialprice/index');
                
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('manage/specialprice/index');
            }
        }

        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_product_special_price', 'specialPriceID=' . "$specialPriceID" . ' AND status!="Deleted"', FALSE); //data for edit
        

        $this->render('manage/specialprice/Add');
    }

    public function changeStatus() {

        $specialPriceID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_product_special_price', 'specialPriceID=' . $specialPriceID, $this->data['data']);
        redirect($_SERVER['HTTP_REFERER']);
    }

 

}

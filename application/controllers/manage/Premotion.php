<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Premotion extends Admin_Controller {

    //private $languageID;
    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('PremotionModel', 'premotionModel');
        $this->load->model('CategoryModel', 'categoryModel');
       $this->load->model('ManufacturesModel', 'manufacturesModel');
    }

    public function index() {
        //$this->data = array();

        if ($this->input->post('action')) {
            $id = implode(",", $this->input->post('multyCheck'));
            $status['status'] = $this->input->post('action');
            $this->generalModel->updateTableValues('ec_premotion', 'premotionID IN (' . $id . ')', $status);
        }

        $this->data['breadcrumbs'] = "All Promotion";
        $this->data['pageTitle'] = "All Promotion";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(premotionID) as tblCount', 'ec_premotion', 'status!="Deleted"', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Premotion/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;


        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $this->data['currentPage'] = $page + 1;
        $params = array(
            'name' => $this->input->get('name'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language);
        
       
        $this->data['result'] = $this->premotionModel->selectAll($params);
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }
        $this->render('manage/premotion/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Promotion";
        $this->data['pageTitle'] = "Add Promotion";
        $this->data['mainCategory']     = $this->categoryModel->selectActiveCategory('', $this->language); // parent category List manufacturerID
        $this->data['manufacturesList'] = $this->manufacturesModel->selectActiveFilter('', $this->language); // Manufacture List For List
        //echo "<pre>"; print_r($this->data['manufacturesList']); exit;
        //form validation 
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('offer', 'offer', 'required|less_than_equal_to[100]|greater_than_equal_to[0]');
        $this->form_validation->set_rules('type', 'type', 'required');
        $this->form_validation->set_rules('priority', 'priority', 'required');
        $this->form_validation->set_rules('dateStart', 'start date', 'required');
        $this->form_validation->set_rules('dateEnd', 'end date', 'required');
       
        if ($this->input->post('type') == 'brand') {
            $this->form_validation->set_rules('brandID[]', 'brand', 'required');
        }
        if ($this->input->post('type') == 'category') {
             $this->form_validation->set_rules('categoryID[]', 'category', 'required');
        }
       
        

        if ($this->form_validation->run() === TRUE) {

            $resData['nonXss'] = array(
                'type'        => $this->input->post('type'),
                'offer'        => $this->input->post('offer'),
                'priority'             => $this->input->post('priority'),
                'dateStart'        => $this->input->post('dateStart'),
                'dateEnd'        => $this->input->post('dateEnd'),
                'status'            => 'Active',
                
            );
//            if ($errVal == 1 || $_POST['image'] == '') {
//                $resData['nonXss']['status'] = 'Inactive';
//            }
            if($this->language==1){
                if($this->input->post('name')){
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($this->input->post('name'))));
                    $resData['nonXss']['importKey'] =   $importKey;
                }
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_premotion', $resData['xssData']);

            if ($result) {
                $manufacturerDetail['nonXss'] = array(
                    'premotionID' => $result,
                    'languageID' => $this->language,
                    'title' => $this->input->post('title')
                );
                $manufacturerDetail['xssData'] = $this->security->xss_clean($manufacturerDetail['nonXss']);
                $this->generalModel->insertValue('ec_premotion_detail', $manufacturerDetail['xssData']); //GroupDetail Added
                
                $this->generalModel->deleteTableValues('ec_premotion_to_category', 'premotionID=' . $result);
                $categoryInsert = $this->input->post('categoryID');
                if($this->input->post('type')=='category'){
                    if($categoryInsert){
                        foreach($categoryInsert as $categoryID){
                            $catDetail['nonXss'] = array(
                                'premotionID' => $result,
                                'categoryID' => $categoryID
                            );
                            $catDetail['xssData'] = $this->security->xss_clean($catDetail['nonXss']);
                            $this->generalModel->insertValue('ec_premotion_to_category', $catDetail['xssData']); //GroupDetail Added
                        }
                    }
                }
                   
                
                 $this->generalModel->deleteTableValues('ec_premotion_to_brand', 'premotionID=' . $result);
                if($this->input->post('type')=='brand'){
                    $brandInsert = $this->input->post('brandID');
                    if($brandInsert){
                        foreach($brandInsert as $brandID){
                            $brandDetail['nonXss'] = array(
                                'premotionID' => $result,
                                'brandID' => $brandID
                            );
                            $brandDetail['xssData'] = $this->security->xss_clean($brandDetail['nonXss']);
                            $this->generalModel->insertValue('ec_premotion_to_brand', $brandDetail['xssData']); //GroupDetail Added
                        }
                    }
                }
                
                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Manufacturer Added');

                    redirect('manage/Premotion/index');
               
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/Premotion/index');
            }
        }
        $this->render('manage/premotion/Add');
    }

    public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Promotion";
        $this->data['pageTitle'] = "Edit Promotion";
        $premotionID = $this->uri->segment(4);
       $this->data['mainCategory']     = $this->categoryModel->selectActiveCategory('', $this->language); // parent category List manufacturerID
        $this->data['manufacturesList'] = $this->manufacturesModel->selectActiveFilter('', $this->language); //
        //form validation 
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('offer', 'offer', 'required|less_than_equal_to[100]|greater_than_equal_to[0]');
        $this->form_validation->set_rules('type', 'type', 'required');
        $this->form_validation->set_rules('priority', 'priority', 'required');
        if ($this->input->post('type') == 'brand') {
            $this->form_validation->set_rules('brandID[]', 'brand', 'required');
        }
        if ($this->input->post('type') == 'category') {
             $this->form_validation->set_rules('categoryID[]', 'category', 'required');
        }
        //image upload
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
            $resData['nonXss'] = array(
                'type'        => $this->input->post('type'),
                'offer'        => $this->input->post('offer'),
                'priority'             => $this->input->post('priority'),
                'dateStart'        => $this->input->post('dateStart'),
                'dateEnd'        => $this->input->post('dateEnd'),
                'status'            => 'Active',
                'applyStatus'            => 'Remove',
            );  
            
            
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $result = $this->generalModel->updateTableValues('ec_premotion', 'premotionID=' . $premotionID, $resData['xssData']);


            $premotionDetail['nonXss'] = array(
                'premotionID' => $premotionID,
                'languageID' => $this->language,
                'title' => $this->input->post('title')
            );
            $premotionDetail['xssData'] = $this->security->xss_clean($premotionDetail['nonXss']);
            $checkManufacturerDetail = $this->generalModel->getTableValue('premotionID', 'ec_premotion_detail', 'premotionID=' . $premotionID . ' AND languageID=' . $this->language, FALSE);
            if ($checkManufacturerDetail) {
                $this->generalModel->updateTableValues('ec_premotion_detail', 'premotionID=' . $premotionID . ' AND languageID=' . $this->language, $premotionDetail['xssData']); //GroupDetail updated
            } else {
                $this->generalModel->insertValue('ec_premotion_detail', $premotionDetail['xssData']); // insert detail data
            }
                $this->generalModel->deleteTableValues('ec_premotion_to_category', 'premotionID=' . $premotionID);
                if($this->input->post('type')=='category'){
                    $categoryInsert = $this->input->post('categoryID');
                    if($categoryInsert){
                        foreach($categoryInsert as $categoryID){
                            $catDetail['nonXss'] = array(
                                'premotionID' => $premotionID,
                                'categoryID' => $categoryID
                            );
                            $catDetail['xssData'] = $this->security->xss_clean($catDetail['nonXss']);
                            $this->generalModel->insertValue('ec_premotion_to_category', $catDetail['xssData']); //GroupDetail Added
                        }
                    }
                }
                
                $this->generalModel->deleteTableValues('ec_premotion_to_brand', 'premotionID=' . $premotionID);
                if($this->input->post('type')=='brand'){
                    $brandInsert = $this->input->post('brandID');
                    if($brandInsert){
                        foreach($brandInsert as $brandID){
                            $brandDetail['nonXss'] = array(
                                'premotionID' => $premotionID,
                                'brandID' => $brandID
                            );
                            $brandDetail['xssData'] = $this->security->xss_clean($brandDetail['nonXss']);
                            $this->generalModel->insertValue('ec_premotion_to_brand', $brandDetail['xssData']); //GroupDetail Added
                        }
                    }
                }
                $this->generalModel->deleteTableValues('ec_premotion_to_product', 'premotionID=' . $premotionID);
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Manufacturer Updated');
                if ($errVal == 0)
                    redirect('manage/Premotion/index');
            } else {
                $this->session->set_flashdata('failMessage', 'Fail To Update');
                redirect('manage/Premotion/index');
            }
        }
        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_premotion', 'premotionID=' . "$premotionID" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->data['content']['detail'] = $this->generalModel->getTableValue('*', 'ec_premotion_detail', 'premotionID=' . "$premotionID" . ' AND languageID=' . $this->language, FALSE); //data for edit
        $premotioncategory = $this->generalModel->getTableValue('*', 'ec_premotion_to_category', 'premotionID='.$premotionID, TRUE); //data for edit
        $premoCatData   =   array();
        foreach($premotioncategory as $key=>$preCat){
            $premoCatData[$key] =   $preCat['categoryID'];
        }
        
        $premotionBrand = $this->generalModel->getTableValue('*', 'ec_premotion_to_brand', 'premotionID='.$premotionID, TRUE); //data for edit
        $premoBrandData   =   array();
        foreach($premotionBrand as $key=>$preBrand){
            $premoBrandData[$key] =   $preBrand['brandID'];
        }
        $this->data['content']['categoryData']  =   $premoCatData;
        $this->data['content']['premoBrandData']  =   $premoBrandData;
       
        
        
        $this->render('manage/premotion/Add');
    }

    public function changeStatus() {

        $premotionID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status; 
        $this->data['data']['applyStatus'] = 'Remove';
        $this->logUserActivity('Product promotion status changed - '.$status);
        $result = $this->generalModel->updateTableValues('ec_premotion', 'premotionID=' . $premotionID, $this->data['data']);
        if($result){
            $this->generalModel->deleteTableValues('ec_premotion_to_product', 'premotionID=' . $premotionID);
        }
        
        redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function changeApplyStatus() {

        $premotionID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['applyStatus'] = $status;
        $this->logUserActivity('Product promotion status changed - '.$status);
        $result = $this->generalModel->updateTableValues('ec_premotion', 'premotionID=' . $premotionID, $this->data['data']);
        $this->generalModel->deleteTableValues('ec_premotion_to_product', 'premotionID=' . $premotionID);
        if($status=='Apply'){
            $premotion   =   $this->generalModel->getTableValue('*', 'ec_premotion', 'premotionID=' . "$premotionID" . ' AND status="Active"', FALSE);
           //echo "<pre>"; print_r($premotion); exit;
            if($premotion['type']=='brand'){
                $premotion_to_brand   =   $this->generalModel->getTableValue('brandID', 'ec_premotion_to_brand', 'premotionID=' . "$premotionID" . '', TRUE);
               
                if($premotion_to_brand){
                    foreach($premotion_to_brand  as $brandData)
                    {
                        $brandID   = $brandData['brandID'];
                        $product    =   $this->generalModel->getTableValue('productID,price', 'ec_product', 'manufacturerID=' . "$brandID" . ' AND  status="Active"', TRUE);
                        
                        if($product){
                            foreach($product as $productData){
                                $offerPersentage  =   ($premotion['offer'] / 100) * $productData['price']; 
                                
                                $offerPrice =   $productData['price']-$offerPersentage;
                               
                                $premotionProduct['nonXss'] = array(
                                   'premotionID'    => $premotionID,
                                   'productID'      => $productData['productID'],
                                   'type'           => $premotion['type'],
                                   'priority'       => $premotion['priority'],
                                   'dateStart'      => $premotion['dateStart'],
                                   'dateEnd'        => $premotion['dateEnd'],
                                    'offer'     =>   $premotion['offer'],
                                   'offePrice'      =>   $offerPrice,
                               );
                               $premotionProduct['xssData'] = $this->security->xss_clean($premotionProduct['nonXss']);
                               $this->generalModel->insertValue('ec_premotion_to_product', $premotionProduct['xssData']);
                            }
                        }
                        
                    }
                }
            }elseif($premotion['type']=='category'){
                
               $premotion_to_category   =   $this->generalModel->getTableValue('categoryID', 'ec_premotion_to_category', 'premotionID=' . "$premotionID" . '', TRUE);
              // echo "<pre>"; print_r($premotion_to_category); exit;
               if($premotion_to_category){
                    foreach($premotion_to_category  as $categoryData){
                        $categoryID   = $categoryData['categoryID'];
                         $catProduct    =   $this->generalModel->getTableValue('productID,categoryID', 'ec_product_to_category', 'categoryID=' . "$categoryID" . '', TRUE);
                         
                        if($catProduct){
                            foreach($catProduct as $catProductData){
                                $productID  =   $catProductData['productID'];
                                $product    =   $this->generalModel->getTableValue('productID,price', 'ec_product', 'productID=' . "$productID" . ' AND  status="Active"', False);
                                if($product){
                                    $offerPersentage  =   ($premotion['offer'] / 100) * $product['price']; 
                                
                                    $offerPrice =   $product['price']-$offerPersentage;

                                    $premotionProduct['nonXss'] = array(
                                       'premotionID'    => $premotionID,
                                       'productID'      => $product['productID'],
                                       'type'           => $premotion['type'],
                                       'priority'       => $premotion['priority'],
                                        'dateStart'       => $premotion['dateStart'],
                                        'dateEnd'       => $premotion['dateEnd'],
                                         'offer'     =>   $premotion['offer'],
                                       'offePrice'     =>   $offerPrice,
                                   );
                                   $premotionProduct['xssData'] = $this->security->xss_clean($premotionProduct['nonXss']);
                                   $this->generalModel->insertValue('ec_premotion_to_product', $premotionProduct['xssData']);
                                    }
                                    
                            }
                            
                        }
                    }
                  
                }
            }
        }
        
        redirect($_SERVER['HTTP_REFERER']);
    }
    

}

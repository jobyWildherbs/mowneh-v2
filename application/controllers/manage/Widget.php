<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Widget extends Admin_Controller {

    //private $languageID;

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('ProductModel', 'productModel');
        $this->load->model('WidgetModel', 'widgetModel');
        $this->load->model('CategoryModel', 'categoryModel');

    }

    public function index() {
        //$this->data = array();

        if ($this->input->post('action')) {
            $id = implode(",", $this->input->post('multyCheck'));
            $status['status'] = $this->input->post('action');
            $this->generalModel->updateTableValues('ec_widget', 'widgetID IN (' . $id . ')', $status);
        }
        $this->data['breadcrumbs'] = "All Widget Page";
        $this->data['pageTitle'] = "All Widget Page";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(widgetID) as tblCount', 'ec_widget', 'widgetID!=1 AND status!="Deleted"', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Widget/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;


        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $this->data['currentPage'] = $page + 1;
        $params = array(
            'widgetName' => $this->input->get('title'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language);

        $this->data['result'] = $this->widgetModel->selectAll($params);
          if(@$_GET['msg']){
            if($_GET['msg'] == 'active'){
             $this->data['success'] = "Widget activated Successfully";
            }else if($_GET['msg'] == 'inactive'){
             $this->data['success'] = "Widget inactivated Successfully";
            }else if($_GET['msg'] == 'delete'){
             $this->data['success'] = "Widget deleted Successfully";
            }
        }
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }
        
        

        $this->render('manage/widget/list');
    }
    
    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs']      =  "Add Widget";
        $this->data['pageTitle']        =  "Add Widget";
        $this->data['widgetType']       =  $this->widgetModel->getWidgetType();
        $this->data['productList']      =  $this->productModel->selectProduct('', $this->language); // Product List
        $this->data['categoryList']     =  $this->categoryModel->getActiveCategory($this->language); // Category List
        
        
        $this->form_validation->set_rules('widgetName', 'Widget Name', 'required|xss_clean');
        $this->form_validation->set_rules('typeID', 'Type', 'required');
        $this->form_validation->set_rules('imageDimension', 'Image Dimension', 'required');
        if ($this->form_validation->run() === TRUE) {
            $resData['nonXss'] = array(
                'typeID'                => $this->input->post('typeID'),
                'imageDimension'        => $this->input->post('imageDimension'),
                'startDate'             => $this->input->post('startDate'),
                'endDate'               => $this->input->post('endDate'),
                'sortOrder'             => $this->input->post('sortOrder'),
                'status'                => $this->input->post('status'),
                'dateAdded'             => date('Y-m-d H:i')
            );
            
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $widgetID    =  $this->generalModel->insertValue('ec_widget', $resData['xssData']); // Insert widget Data
            if($widgetID){
                $resDetailData['nonXss'] = array(
                    'widgetID'              => $widgetID,
                    'widgetName'            => $this->input->post('widgetName'),
                    'languageID'            => $this->language
                );
                $resDetailData['xssData'] = $this->security->xss_clean($resDetailData['nonXss']);
                $this->generalModel->insertValue('ec_widget_detail', $resDetailData['xssData']); // Insert widget Detail Data
                if($this->input->post('customCategory')=='category'){
                    $catData = array();
                    foreach ($this->input->post('categoryID[]') as $key => $categoryID) {
                        $catData[$key]['categoryID'] = $this->security->xss_clean($categoryID);
                        $catData[$key]['widgetID'] = $this->security->xss_clean($widgetID);
                        $this->generalModel->insertValue('ec_widget_to_category', $catData[$key]); // insert Filter data
                    }
                }
                elseif($this->input->post('customCategory')=='category_product'){
                        $catData    =   array();
                        $catData['categoryID'] = $this->security->xss_clean($this->input->post('proCategoryID'));
                        $catData['widgetID'] = $this->security->xss_clean($widgetID);
                        $this->generalModel->insertValue('ec_widget_to_category', $catData); // insert Filter data
                }
                elseif($this->input->post('customProductCheck')=='YES'){
                    $filterData = array();
                    foreach ($this->input->post('widgetProduct[]') as $key => $productID) {
                        $filterData[$key]['productID'] = $this->security->xss_clean($productID);
                        $filterData[$key]['widgetID'] = $this->security->xss_clean($widgetID);
                        $this->generalModel->insertValue('ec_widget_to_product', $filterData[$key]); // insert Filter data
                    }
                }
                
               $this->session->set_flashdata('message', 'Data Added Successfully');
                redirect('manage/Widget');
            }
            else{
                $this->session->set_flashdata('failMessage', 'Failed');
                redirect('manage/Widget');
            }
        }
        $this->render('manage/widget/Add');
    }
    
    public function edit() {
        $widgetID = $this->uri->segment(4);

        $this->data['breadcrumbs']      =   "Edit Widget";
        $this->data['pageTitle']        =   "Edit Widget";
        $this->data['widgetType']       =   $this->widgetModel->getWidgetType();
        $this->data['productList']      =   $this->productModel->selectProduct('', $this->language); // Product List  
        $this->data['content']          =   $this->widgetModel->selectWidgetDetail($widgetID, $this->language); 
        $this->data['categoryList']     =  $this->categoryModel->getActiveCategory($this->language); // Category List
       
        $widgetProduct              = $this->widgetModel->widgetProduct($widgetID);
        $this->data['content']['widgetProduct'] =   array();
        foreach($widgetProduct as $key=>$data){
            $this->data['content']['widgetProduct'][$key] =   $data['productID'];
        }
        
        $widgetCategory              = $this->widgetModel->widgetCategory($widgetID);
        
        
        $this->data['content']['widgetCategory'] =   array();
        foreach($widgetCategory as $key=>$data){
            $this->data['content']['widgetCategory'][$key] =   $data['categoryID'];
        }
        //echo "<pre>"; print_r($this->data['content']['widgetCategory']); exit;
        //$this->data['content']['widgetProduct']
        $this->form_validation->set_rules('widgetName', 'Widget Name', 'required|xss_clean');
        $this->form_validation->set_rules('typeID', 'Type', 'required');
        $this->form_validation->set_rules('imageDimension', 'Image Dimension', 'required');
        if($this->input->post('typeID') ==2){
        $this->form_validation->set_rules('widgetProduct[]', 'widget Product', 'required');
        }
        if($this->input->post('typeID') ==3){
        $this->form_validation->set_rules('categoryID[]', 'Category', 'required');
        }
        if($this->input->post('typeID') ==4){
        $this->form_validation->set_rules('proCategoryID', 'proCategoryID', 'required');
        }
        
        
        if ($this->form_validation->run() === TRUE) {
            $resData['nonXss'] = array(
                'typeID'                => $this->input->post('typeID'),
                'imageDimension'        => $this->input->post('imageDimension'),
                'startDate'             => $this->input->post('startDate'),
                'endDate'               => $this->input->post('endDate'),
                'sortOrder'             => $this->input->post('sortOrder'),
                'status'                => $this->input->post('status')
            );
            
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);  
            $widgetUpdate   =   $this->generalModel->updateTableValues('ec_widget','widgetID='.$widgetID, $resData['xssData']); // Insert widget Data
            if($widgetUpdate){
                
                $resDetailData['nonXss'] = array(
                    'widgetID'              => $widgetID,
                    'widgetName'            => $this->input->post('widgetName'),
                    'languageID'            => $this->language
                );
                $resDetailData['xssData'] = $this->security->xss_clean($resDetailData['nonXss']);
                $checkWidgetDetail =   $this->generalModel->getTableValue('widgetID','ec_widget_detail','widgetID='.$widgetID.' AND languageID='.$this->language,FALSE);
                if($checkWidgetDetail){
                    $this->generalModel->updateTableValues('ec_widget_detail','widgetID='.$widgetID.' AND languageID='.$this->language ,$resDetailData['xssData']); // insert detail data
                }else{
                    $this->generalModel->insertValue('ec_widget_detail', $resDetailData['xssData']); // insert detail data
                }
                
                $this->generalModel->deleteTableValues('ec_widget_to_product','widgetID='.$widgetID);
                $this->generalModel->deleteTableValues('ec_widget_to_category','widgetID='.$widgetID);
                if($this->input->post('customCategory')=='category'){
                    $catData = array();
                   
                    foreach ($this->input->post('categoryID[]') as $key => $categoryID) {
                        $catData[$key]['categoryID'] = $this->security->xss_clean($categoryID);
                        $catData[$key]['widgetID'] = $this->security->xss_clean($widgetID);
                        $this->generalModel->insertValue('ec_widget_to_category', $catData[$key]); // insert Filter data
                    }
                }
                elseif($this->input->post('customCategory')=='category_product'){
                        $catData    =   array();
                        $catData['categoryID'] = $this->security->xss_clean($this->input->post('proCategoryID'));
                        $catData['widgetID'] = $this->security->xss_clean($widgetID);
                        $this->generalModel->insertValue('ec_widget_to_category', $catData); // insert Filter data
                }
                elseif($this->input->post('customProductCheck')=='YES'){
                    $filterData = array();
                    foreach ($this->input->post('widgetProduct[]') as $key => $productID) {
                        $filterData[$key]['productID'] = $this->security->xss_clean($productID);
                        $filterData[$key]['widgetID'] = $this->security->xss_clean($widgetID);
                        $this->generalModel->insertValue('ec_widget_to_product', $filterData[$key]); // insert Filter data
                    }
                }
                
               $this->session->set_flashdata('message', 'Data Updated Successfully');
                redirect('manage/Widget');
            }
            else{
                $this->session->set_flashdata('failMessage', 'Failed');
                redirect('manage/Widget');
            }
        }
        $this->render('manage/widget/Add');
    }
    
    public function ajaxCustomProductCheck(){
        $typeID = $this->security->xss_clean($this->input->post('typeID'));
        $customProduct = $this->widgetModel->getcustomProduct($typeID); //data count for pagination
        echo $customProduct['customProduct']."~".$customProduct['typeKey']; exit;
    }
        public function changeStatus() {

        $widgetID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity($status);
        $result = $this->generalModel->updateTableValues('ec_widget', 'widgetID=' . $widgetID, $this->data['data']);
        if($status == 'Active'){
            redirect('manage/Widget?msg=active');
        }else if($status =='Inactive'){
            redirect('manage/Widget?msg=inactive');
        }else if($status == 'Deleted'){
            redirect('manage/Widget?msg=delete');
        }else{
            redirect('manage/Widget');
        }
        //redirect($_SERVER['HTTP_REFERER']);
    }


}

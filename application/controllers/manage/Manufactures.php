<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manufactures extends Admin_Controller {

    //private $languageID;
    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('ManufacturesModel', 'manufacturesModel');
        $imageDimension = $this->getImageDimension('brand_image_dimen');
        $imageSet = implode(' x ', $imageDimension);
        $this->data['imageDimension'] = $imageDimension;
        $this->data['imageSet'] = $imageSet;
        $this->data['maxImageSizeJs'] = $this->maxImageUploadSize * 1024;
        $this->data['allowedType'] = $this->allowedImageTypes;
        $this->data['maxSize'] = $this->maxImageUploadSize / 1024;
        $this->imageConfig = array(
            'upload_path' => "./uploads/manufactures/",
            'allowed_types' => $this->allowedImageTypes,
            'max_size' => $this->maxImageUploadSize,
            'max_width' => $imageDimension[0],
            'max_height' => $imageDimension[1],
            'min_width' => $imageDimension[0],
            'min_height' => $imageDimension[1],
            'encrypt_name' => TRUE,
        );
    }

    public function index() {
        //$this->data = array();

        if ($this->input->post('action')) {
            $id = implode(",", $this->input->post('multyCheck'));
            $status['status'] = $this->input->post('action');
            $this->generalModel->updateTableValues('ec_manufacturer', 'manufacturerID IN (' . $id . ')', $status);
        }

        $this->data['breadcrumbs'] = "All Brands";
        $this->data['pageTitle'] = "All Brands";
        $this->load->library("pagination");
        $totalRws = $this->generalModel->getTableValue('count(manufacturerID) as tblCount', 'ec_manufacturer', 'manufacturerID!=1 AND status!="Deleted"', FALSE); //data count for pagination
        $config = $this->paginationConfig;
        $config['per_page'] = ADMIN_DATA_LIMIT;
        $config['total_rows'] = $totalRws['tblCount'];
        $config['base_url'] = base_url() . 'manage/Manufactures/index';
        //$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;


        $this->pagination->initialize($config);

        $this->data["links"] = $this->pagination->create_links();

        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $this->data['currentPage'] = $page + 1;
        $params = array(
            'name' => $this->input->get('name'),
            'limit' => $config['per_page'],
            'start' => $page,
            'languageID' => $this->language);
        
        $selectedBrands    = $this->generalModel->getTableValue('distinct(manufacturerID)','ec_product',null,true);
        $brandArray = array();
        foreach ($selectedBrands as $value) {
            $brandArray[] = $value['manufacturerID'];
        }
        $this->data['usedBrands'] = $brandArray;
        
        $this->data['result'] = $this->manufacturesModel->selectAll($params);
          if(@$_GET['msg']){
            if($_GET['msg'] == 'active'){
             $this->data['success'] = "Manufactures activated Successfully";
            }else if($_GET['msg'] == 'inactive'){
             $this->data['success'] = "Manufactures inactivated Successfully";
            }else if($_GET['msg'] == 'delete'){
             $this->data['success'] = "Manufactures deleted Successfully";
            }
        }
        if ($this->session->flashdata('message') != '') {
            $this->data['success'] = $this->session->flashdata('message');
        } else {
            //echo $this->data['success'];
            $this->data['failMessage'] = $this->session->flashdata('failMessage');
        }
        $this->render('manage/manufactures/list');
    }

    public function add() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Add Brand";
        $this->data['pageTitle'] = "Add Brand";

        //form validation 
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean|is_unique[ec_manufacturer_detail.name]');
        $this->form_validation->set_rules('sort_order', 'Sort Order', 'required|numeric');
         $this->form_validation->set_rules('pageKey', 'Page Key', 'trim|required|xss_clean|is_unique[ec_manufacturer.pageKey]');

        if ($this->form_validation->run() === TRUE) {

            $errVal = 0;
//            $_POST['image'] = '';
//            if ($_FILES['image']['size'] > 0) {
//
//                $this->load->library('upload', $this->imageConfig);
//                if ($this->upload->do_upload('image')) {
//                    $_POST['image'] = $this->upload->data('file_name');
//                    // in case you need to save it into a database
//                } else {
//                    $this->data['imgErr'] = $this->upload->display_errors();
//                    $errVal = 1;
//                    $_POST['image'] = '';
//                }
//            } else {
//                $this->data['imgErr'] = "No Image File Uploaded";
//                $errVal = 1;
//                $_POST['image'] = '';
//            }

            //$this->session->set_flashdata('imgErr', $this->data['imgErr']);
            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'pageKey'           => $this->input->post('pageKey'),
                'sort_order'        => $this->input->post('sort_order'),
                'image'             => $this->input->post('image'),
                'status'            => $this->input->post('status')
            );
//            if ($errVal == 1 || $_POST['image'] == '') {
//                $resData['nonXss']['status'] = 'Inactive';
//            }
            if($this->language==1){
                if($this->input->post('name')){
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($this->input->post('name'))));
                    $resData['nonXss']['importKey'] =   $importKey;
                }
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $result = $this->generalModel->insertValue('ec_manufacturer', $resData['xssData']);

            if ($result) {
                $manufacturerDetail['nonXss'] = array(
                    'manufacturerID' => $result,
                    'languageID' => $this->language,
                    'name' => $this->input->post('name')
                );
                $manufacturerDetail['xssData'] = $this->security->xss_clean($manufacturerDetail['nonXss']);
                $this->generalModel->insertValue('ec_manufacturer_detail', $manufacturerDetail['xssData']); //GroupDetail Added

                $this->session->set_flashdata('message', 'Data Added Successfully');
                $this->logUserActivity('Manufacturer Added');
                if ($errVal == 0) {
                    redirect('manage/manufactures/index');
                } else {
                    redirect('manage/manufactures/edit/' . $result . '/imgErr');
                }
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Add');
                redirect('manage/manufactures/index');
            }
        }
        $this->render('manage/manufactures/Add');
    }

    public function edit() {
        //$this->data = array();
        $this->data['breadcrumbs'] = "Edit Brand";
        $this->data['pageTitle'] = "Edit Brand";
        $manufacturerID = $this->uri->segment(4);
        $imgErr = $this->uri->segment(5);
        if ($imgErr) {
            $this->data['imgErr'] = "Invalid Image, Please try again";
            $this->data['errVal'] = 1;
        }
        if ($this->session->flashdata('imgErr') != '') {
            $this->data['imgErr'] = $this->session->flashdata('imgErr');
            $this->data['errVal'] = 1;
        }
        //form validation 
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('sort_order', 'Sort order', 'required|numeric');
        //image upload
        if ($this->form_validation->run() === TRUE) {
            $errVal = 0;
//            if ($_FILES['image']['size'] > 0) {
//
//                $this->load->library('upload', $this->imageConfig);
//                if ($this->upload->do_upload('image')) {
//                    $_POST['image'] = $this->upload->data('file_name');
//                    // in case you need to save it into a database
//                } else {
//                    $this->data['imgErr'] = $this->upload->display_errors();
//                    $errVal = 1;
//                }
//            }
//            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                //'name'       => $this->input->post('name'),
                'sort_order' => $this->input->post('sort_order'),
                'status' => $this->input->post('status')
            );
            
             if($this->language==1){
                if($this->input->post('name')){
                    $importKey  =   trim(preg_replace("/[^a-zA-Z^0-9]+/", "-", strtolower($this->input->post('name'))));
                    $resData['nonXss']['importKey'] =   $importKey;
                }
            }

            if ($this->input->post('image')) {
                $resData['nonXss']['image'] = $this->input->post('image');
            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $result = $this->generalModel->updateTableValues('ec_manufacturer', 'manufacturerID=' . $manufacturerID, $resData['xssData']);


            $manufacturerDetail['nonXss'] = array(
                'manufacturerID' => $manufacturerID,
                'languageID' => $this->language,
                'name' => $this->input->post('name')
            );
            $manufacturerDetail['xssData'] = $this->security->xss_clean($manufacturerDetail['nonXss']);
            $checkManufacturerDetail = $this->generalModel->getTableValue('manufacturerID', 'ec_manufacturer_detail', 'manufacturerID=' . $manufacturerID . ' AND languageID=' . $this->language, FALSE);
            if ($checkManufacturerDetail) {
                $this->generalModel->updateTableValues('ec_manufacturer_detail', 'manufacturerID=' . $manufacturerID . ' AND languageID=' . $this->language, $manufacturerDetail['xssData']); //GroupDetail updated
            } else {
                $this->generalModel->insertValue('ec_manufacturer_detail', $manufacturerDetail['xssData']); // insert detail data
            }
            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                $this->logUserActivity('Manufacturer Updated');
                if ($errVal == 0)
                    redirect('manage/manufactures/index');
            } else {
                $this->session->set_flashdata('failMessage', 'Fail To Update');
                redirect('manage/manufactures/index');
            }
        }
        $this->data['content']['main'] = $this->manufacturesModel->selectFilter($manufacturerID, $this->language); //data for edit
        $this->data['content']['detail'] = $this->manufacturesModel->selectFilter($manufacturerID, $this->language); //data for edit
        $this->data['content']['main'] = $this->generalModel->getTableValue('*', 'ec_manufacturer', 'manufacturerID=' . "$manufacturerID" . ' AND status!="Deleted"', FALSE); //data for edit
        $this->data['content']['detail'] = $this->generalModel->getTableValue('*', 'ec_manufacturer_detail', 'manufacturerID=' . "$manufacturerID" . ' AND languageID=' . $this->language, FALSE); //data for edit
        $this->render('manage/manufactures/Add');
    }

    public function changeStatus() {

        $manufacturerID = $this->uri->segment(4);
        $status = $this->uri->segment(5);
        $this->data['data']['status'] = $status;
        $this->logUserActivity('Brand status changed - '.$status);
        $result = $this->generalModel->updateTableValues('ec_manufacturer', 'manufacturerID=' . $manufacturerID, $this->data['data']);
        if($status == 'Active'){
            redirect('manage/manufactures?msg=active');
        }else if($status =='Inactive'){
            redirect('manage/manufactures?msg=inactive');
        }else if($status == 'Deleted'){
            redirect('manage/manufactures?msg=delete');
        }else{
            redirect('manage/manufactures');
        }
        //redirect($_SERVER['HTTP_REFERER']);
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register extends Auth_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('CustomerLogin');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->library('form_validation');
        $this->load->library('user_agent');
        $this->lang->load('site', $this->siteLanguage);
    }
    
     public function index() {
        
        
        if($this->session->userdata['moonehLoggedIn']==1){
             redirect('Home', 'refresh');
        }
        $data = array();
        if ($this->input->post()) {
            $this->form_validation->set_rules('firstname', 'First Name', 'required',array('required' => 'Please Provide First Name'));
            $this->form_validation->set_rules('lastname', 'Last Name', 'required',array('required' => 'Please Provide Last Name'));
//            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|is_unique[ec_customer.email AND status!="Deleted"]', array(
//                'required'  => 'Please Provide A Valid Email',
//                'is_unique' => 'Account already exists with this email.',
//                'valid_email' => 'Please provide a valid email address',
//                'xss_clean' => 'Please provide a valid email address'
//            ));
             $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|callback_checkEmailExist', array(
                'required'  => 'Please Provide A Valid Email',
                'checkEmailExist' => 'Account already exists with this email.',
                'valid_email' => 'Please provide a valid email address',
                'xss_clean' => 'Please provide a valid email address'
            ));
             
             if($this->input->post('countryCode')=='974'){
                    $this->form_validation->set_rules('telephone', 'Phone Number', 'required|min_length[8]|max_length[8]|is_unique[ec_customer.telephone]', array(
                        'required' => 'Please Provide Phone Number',
                        'is_unique' => 'This number is already associated with another account',
                        'min_length' => 'Please provide a valid phone number',
                        'max_length' => 'Please provide a valid phone number'
                    ));
             }else{
                    $this->form_validation->set_rules('telephone', 'Phone Number', 'required|min_length[10]|max_length[10]|is_unique[ec_customer.telephone]', array(
                        'required' => 'Please Provide Phone Number',
                        'is_unique' => 'This number is already associated with another account',
                        'min_length' => 'Please provide a valid phone number',
                        'max_length' => 'Please provide a valid phone number'
                    ));
             }
           
            
            $this->form_validation->set_rules('password', 'Password', 'required',array('required' => 'Please Provide A Valid Password'));
            $this->form_validation->set_rules('cPassword', 'Confirm Password', 'required|matches[password]',array(
                'required' => 'Please Make Sure Your Confirm Password Match',
                'matches' => 'Please Make Sure Your Confirm Password Match',
                ));
            $this->form_validation->set_rules('condition', '', 'required',array('required' => 'Please Agree To The Terms And Conditions'));
            if ($this->form_validation->run() === TRUE) {
                $resData['nonXss'] = array(
                    'firstname'     => $this->input->post('firstname'),
                    'lastname'      => $this->input->post('lastname'),
                    'email'         => $this->input->post('email'),
                    'countryCode'   => $this->input->post('countryCode'),
                    'telephone'     => $this->input->post('telephone'),
                    'password'      => $this->input->post('password'),
                    'status'        => 'Notverified'
                );
                $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
                $passwordCreate = $this->customerlogin->createCredentials($resData['xssData']['email'], $resData['xssData']['password']);
                $resData['xssData']['password'] = $passwordCreate['password'];
                $resData['xssData']['salt'] = $passwordCreate['salt'];
                $resData['xssData']['dateAdded'] = date('Y-m-d H:i');
                $resData['xssData']['ip'] = $this->input->ip_address();

                $userID = $this->generalModel->insertValue('ec_customer', $resData['xssData']);

                $this->data['message'] = lang('Registration_completed_successfully');
                if ($userID) {
                    $value['status'] = 'expired';
                    $this->generalModel->updateTableValues('ec_otp', 'customerID=' . $userID .' AND otpType="userAuthentication"', $value);
                    
                    $otp    =   substr(number_format(time() * rand(),0,'',''),0,6); 
                    $resOtpData['otp']            =   $otp;
                    $resOtpData['customerID']     =   $userID;
                    $resOtpData['otpType']        =   'userAuthentication';
                    $resOtpData['otpTime']        =   date("Y-m-d H:i:s");
                    $resOtpData['addedDate']      =   date("Y-m-d H:i:s");
                    $otpID   =   $this->generalModel->insertValue('ec_otp',$resOtpData);
                    if($otpID){
                        $smsMessage =   "Dear customer, thank you for registering with mowneh.com. Please enter ".$otp." to complete your registration. This OTP will expire in 5 minutes.";
                        $phoneNo    =  $this->input->post('countryCode').''.$this->input->post('telephone'); 
                        $this->sendSMS($smsMessage,$phoneNo);

                        $config['protocol'] = 'sendmail';
                        $config['mailtype'] = 'html';

                        $activationURL = base_url() . "Register/activateAccount/" . $userID;
                        $from_email =   $this->getSettingValue('email');

                        $message = $this->registerConfirmMessage($userID,$otp);
                        $to_email = $this->input->post('email');
                        //Load email library 
                        $this->load->library('email');
                        $this->email->initialize($config);

                        $this->email->from($from_email, 'Mooneh');
                        $this->email->to($to_email);
                        $this->email->subject('Mooneh verify your email');
                        $this->email->message($message);
//                        $this->email->send();
                        
                        $this->session->set_userdata("regCustomerId",$userID);
                        $this->session->set_userdata("regOtp",$otp);
                        redirect('Register/registerOtp', 'refresh');
                    }
                }

            }
        }
        $this->render('frontEnd/login/Register');
    }
    
    public function registerOtp(){
        $message    =   "";
        $errMessage =   "";
        $regCustomerId  =   $this->session->userdata('regCustomerId');
        $this->form_validation->set_rules('otp', 'OTP', 'required');
        if(@$this->input->post('expire')){
            $value['status'] = 'expired';
            $this->generalModel->updateTableValues('ec_otp', 'customerID=' . $regCustomerId .' AND otpType="userAuthentication" AND otp='. $this->session->userdata('regOtp'), $value);
            echo "1";
            exit;
        }
        
        if($this->form_validation->run()===TRUE)
        {
            $dateTime   =   date("Y-m-d H:i:s");
            $otpExist   =   $this->generalModel->getTableValue('otpID,status', 'ec_otp','otp ="'. $this->input->post('otp').'" AND otpType="userAuthentication" AND addedDate >= date_sub("'.$dateTime.'",interval 5 minute) AND customerID ="'. $regCustomerId.'"',FALSE);
       
            if(@$otpExist){ 
                if($otpExist['status']=='Active'){ 
                    $cust['status'] = 'Active';
                     $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $regCustomerId, $cust);
                    $customer          = $this->generalModel->getTableValue('customerID,firstname,lastname,email,telephone,status','ec_customer',array('customerID'=> $regCustomerId,'status'=>'Active'));
                    $value['status'] = 'used';
                    $this->generalModel->updateTableValues('ec_otp', 'customerID=' . $regCustomerId .' AND otpType="userAuthentication" AND otp='. $this->input->post('otp'), $value);
                    
                    $customerData        = array(
                        'moonehcustomerID'          => $customer['customerID'],
                        'moonehFirstname'           => $customer['firstname'],
                        'moonehLastname'            => $customer['lastname'],
                        'moonehEmail'               => $customer['email'],
                        'moonehStatus'              => $customer['status'],
                        'moonehLoggedIn'            => TRUE
                    ); 
                   
                    $this->session->set_userdata($customerData);
                    redirect('Home', 'refresh');
                    exit;
                }if(@$otpExist['status']=='used'){ 
                     $this->data['errMessage']  =   "OTP already used";
                     $this->render('frontEnd/login/RegisterOtp');
                     //exit;
                }
                if(@$otpExist['status']=='expired'){ 
                     $this->data['errMessage']  =   "OTP expired";
                     $this->render('frontEnd/login/RegisterOtp');
                    
                }
                
            }else{
                $this->data['errMessage']  =   "OTP Invalid";
                     $this->render('frontEnd/login/LoginOtp');
            }
            $this->data['message']  =   "";
        }else{
            $this->data['message']  =   "OTP has been sent to your registered phone number";
        }
        $this->render('frontEnd/login/RegisterOtp');
    }
    
    public function resendOtp(){
                    $userID =   $this->session->userdata('regCustomerId');
                    if($userID){
                        $otpExist   =   $this->generalModel->getTableValue('otpID,otp', 'ec_otp','otpType="userAuthentication" AND customerID ="'. $userID.'" AND status="Active"',FALSE);
                        $user       =   $this->generalModel->getTableValue('customerID,firstname,lastname,email,countryCode,telephone,status', 'ec_customer','customerID ="'. $userID.'"',FALSE);
                        if($otpExist){
                            $value['otpTime']        =   date("Y-m-d H:i:s");
                            $value['addedDate']      =   date("Y-m-d H:i:s");

                            $this->generalModel->updateTableValues('ec_otp', 'otpID=' . $otpExist['otpID'] .' AND otpType="userAuthentication"', $value);
                            $otp    =   $otpExist['otp'];
                            $phoneNo    =   $user['countryCode']."".$user['telephone'];
                            $smsMessage =   "Dear customer, thank you for registering with mowneh.com. Please enter ".$otp." to complete your registration. This OTP will expire in 5 minutes.";
                            $this->sendSMS($smsMessage,$phoneNo);
                        }else{
                            $value['status'] = 'expired';
                            $this->generalModel->updateTableValues('ec_otp', 'customerID=' . $userID .' AND otpType="userAuthentication"', $value);
                            $otp    =   substr(number_format(time() * rand(),0,'',''),0,6); 
                            
                            $resOtpData['otp']            =   $otp;
                            $resOtpData['customerID']     =   $userID;
                            $resOtpData['otpType']        =   'userAuthentication';
                            $resOtpData['otpTime']        =   date("Y-m-d H:i:s");
                            $resOtpData['addedDate']      =   date("Y-m-d H:i:s");
                            $otpID   =   $this->generalModel->insertValue('ec_otp',$resOtpData);
                            
                            $phoneNo    =   $user['countryCode']."".$user['telephone'];
                            $smsMessage =   "Dear customer, thank you for registering with mowneh.com. Please enter ".$otp." to complete your registration. This OTP will expire in 5 minutes.";
                            $this->sendSMS($smsMessage,$phoneNo);
                        }
                          echo 1;  
                    }else{
                        echo "0";
                    }
                   
    }

    public function indexBackup() {
        
        
        if($this->session->userdata['moonehLoggedIn']==1){
             redirect('Home', 'refresh');
        }
        $data = array();
        if ($this->input->post()) {
            $this->form_validation->set_rules('firstname', 'First Name', 'required',array('required' => 'Please Provide First Name'));
            $this->form_validation->set_rules('lastname', 'Last Name', 'required',array('required' => 'Please Provide Last Name'));
//            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|is_unique[ec_customer.email AND status!="Deleted"]', array(
//                'required'  => 'Please Provide A Valid Email',
//                'is_unique' => 'Account already exists with this email.',
//                'valid_email' => 'Please provide a valid email address',
//                'xss_clean' => 'Please provide a valid email address'
//            ));
             $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|callback_checkEmailExist', array(
                'required'  => 'Please Provide A Valid Email',
                'checkEmailExist' => 'Account already exists with this email.',
                'valid_email' => 'Please provide a valid email address',
                'xss_clean' => 'Please provide a valid email address'
            ));
            $this->form_validation->set_rules('telephone', 'Phone Number', 'required|min_length[8]|max_length[8]|is_unique[ec_customer.telephone]', array(
                'required' => 'Please Provide Phone Number',
                'is_unique' => 'This number is already associated with another account',
                'min_length' => 'Please provide a valid phone number',
                'max_length' => 'Please provide a valid phone number'
            ));
            $this->form_validation->set_rules('password', 'Password', 'required',array('required' => 'Please Provide A Valid Password'));
            $this->form_validation->set_rules('cPassword', 'Confirm Password', 'required|matches[password]',array(
                'required' => 'Please Make Sure Your Confirm Password Match',
                'matches' => 'Please Make Sure Your Confirm Password Match',
                ));
            $this->form_validation->set_rules('condition', '', 'required',array('required' => 'Please Agree To The Terms And Conditions'));
            if ($this->form_validation->run() === TRUE) {
                $resData['nonXss'] = array(
                    'firstname' => $this->input->post('firstname'),
                    'lastname' => $this->input->post('lastname'),
                    'email' => $this->input->post('email'),
                    'telephone' => $this->input->post('telephone'),
                    'password' => $this->input->post('password'),
                    'status' => 'Notverified'
                );
                $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
                $passwordCreate = $this->customerlogin->createCredentials($resData['xssData']['email'], $resData['xssData']['password']);
                $resData['xssData']['password'] = $passwordCreate['password'];
                $resData['xssData']['salt'] = $passwordCreate['salt'];
                $resData['xssData']['dateAdded'] = date('Y-m-d H:i');
                $resData['xssData']['ip'] = $this->input->ip_address();

                $userID = $this->generalModel->insertValue('ec_customer', $resData['xssData']);

                $this->data['message'] = lang('Registration_completed_successfully');
                if ($userID) {
                    $smsMessage =   "Mowheh registration registration successfully completed.Please check your registred email to activate your account";
                    $phoneNo    =  "974".$this->input->post('telephone'); 
                    $this->sendSMS($smsMessage,$phoneNo);

                    $config['protocol'] = 'sendmail';
                    $config['mailtype'] = 'html';
                    
                    $activationURL = base_url() . "Register/activateAccount/" . $userID;
                    $from_email =   $this->getSettingValue('email');
                     $message = $this->registerConfirmMessage($userID);
                    $to_email = $this->input->post('email');
                    //Load email library 
                    $this->load->library('email');
                    $this->email->initialize($config);
                    
                    $this->email->from($from_email, 'Mooneh');
                    $this->email->to($to_email);
                    $this->email->subject('Mooneh verify your email');
                    $this->email->message($message);
                    //$this->email->send();
                    
                   
                }

            }
        }
        $this->render('frontEnd/login/Register');
    }

    public function activeOtp() {
        $data = array();
        $otpID = $this->uri->segment(3);
        $userID = $this->uri->segment(4);
        $otpDet = $this->generalModel->getTableValue('otpID,otp', 'ec_otp', 'otpID =' . "$otpID" . ' AND customerID=' . "$userID" . ' AND otpType="userAuthentication" AND status="Active"');
        $data['otp'] = $otpDet['otp'];
        if ($this->input->post()) {
            $postOtp = $this->security->xss_clean($this->input->post('postOtp'));
            $postOtpDet = $this->generalModel->getTableValue('otpID,otp', 'ec_otp', 'customerID=' . "$userID" . ' AND otpType="userAuthentication" AND otp=' . "$postOtp" . ' AND status="Active"');
            if ($otpDet['otpID'] == $postOtpDet['otpID']) {
                $value['status'] = 'Active';
                $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $userID, $value);
                $otpVal['status'] = 'used';
                $this->generalModel->updateTableValues('ec_otp', 'otpID=' . $otpID, $otpVal);
                $customer = $this->generalModel->getTableValue('customerID,firstname,lastname,email,status', 'ec_customer', 'customerID =' . "$userID" . ' AND status="Active"');

                $customerData = array(
                    'moonehcustomerID' => $customer['customerID'],
                    'moonehFirstname' => $customer['firstname'],
                    'moonehLastname' => $customer['lastname'],
                    'moonehEmail' => $customer['email'],
                    'moonehStatus' => $customer['status'],
                    'moonehLoggedIn' => TRUE
                );
                $this->session->set_userdata($customerData);
                redirect('Home', 'refresh');
            }
        }
        $this->twig->display('frontEnd/login/RegisterOtp', $data);
    }

    public function activateAccount($userID) {
        $value['status'] = 'Active';
        $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $userID, $value);
        $customer = $this->generalModel->getTableValue('customerID,firstname,lastname,email,status', 'ec_customer', 'customerID =' . "$userID" . ' AND status="Active"');

        $customerData = array(
            'moonehcustomerID' => $customer['customerID'],
            'moonehFirstname' => $customer['firstname'],
            'moonehLastname' => $customer['lastname'],
            'moonehEmail' => $customer['email'],
            'moonehStatus' => $customer['status'],
            'moonehLoggedIn' => TRUE
        );
        $this->session->set_userdata($customerData);
        redirect('Home', 'refresh');
    }
    
    function checkEmailExist(){
        $email = $this->input->post('email');
        $otpDet = $this->generalModel->getTableValue('customerID', 'ec_customer', 'email ="'.$email.'" AND status!="Deleted"',FALSE);
        if($otpDet){
            return FALSE;   
        }   
        else{
             return TRUE;  
        }
           
   
    }
    
    public function registerConfirmMessage($customerID="",$otp=""){
        $logo  = base_url('uploads/siteInfo/') . $this->getSettingValue('logo');
        $customerData   = $this->generalModel->getTableValue('firstname,lastname,email', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
        $activationURL = base_url() . "Register/activateAccount/" . $customerID;
        $email      =   $this->getSettingValue('email');
        $phone      =   $this->getSettingValue('phone');
        $address    =   $this->getSettingValue('address', $this->siteLanguageID, true);
        
            $message    =   '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                                <html xmlns="http://www.w3.org/1999/xhtml">
                                <head>
                                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                                        <title>Activate Account</title>

                                        <style type="text/css">
                                        @font-face {font-family: "Mary Ann"; src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot"); src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot?#iefix") format("embedded-opentype"); }
                                                body {
                                                        Margin: 0;
                                                        padding: 0;
                                                        font-family: Mary Ann;
                                                        color: #1d1d1d;
                                                }
                                                table {
                                                        border-spacing: 0;
                                                }
                                                td {
                                                        padding: 0;
                                                }
                                                img {
                                                        border: 0;
                                                }
                                                .wrapper
                                                {
                                                        width: 100%;
                                                        table-layout: fixed;
                                                        background-color: #f6f6f6;
                                                }
                                                .webkit
                                                {
                                                        max-width: 600px;
                                                        background-color: #fff;
                                                }
                                                .content
                                                {
                                                        padding: 30px 20px;
                                                }
                                                .reset a
                                                {
                                                        color: #ffffff;
                                                        font-weight: 400;
                                                        text-transform: uppercase;
                                                        background-color: #F06C00;
                                                        border: 1px solid #F06C00;
                                                        text-decoration: none;
                                                        padding: 5px 8px;
                                                        text-align: center;
                                                        border-radius: 5px;
                                                }
                                                .reset a:hover
                                                {
                                                        color: #F06C00;
                                                        background-color: #fff;
                                                        border: 1px solid #F06C00;
                                                }
                                                .content p
                                                {
                                                        font-size: 15px;
                                                } 
                                                .footer
                                                {
                                                        border-top: 1px solid #f2f2f2;
                                                        padding: 30px 0px;
                                                }
                                                @media screen and (max-width: 600px) { 
                                                }
                                                @media screen and (max-width: 400px) { 
                                                }
                                        </style>
                                </head>
                                <body>
                                 <center class="wrapper">
                                         <div class="webkit">
                                                 <table class="outer" align="center">
                                                         <tr>
                                                                 <td>
                                                                         <table width="100%" style="border-spacing: 0;">
                                                                            <tr>
                                                                                        <td style="background-color:#F06C00;padding: 10px;text-align: center;">
                                                                                                <a href="'.base_url().'"><img style="filter:brightness(100) contrast(100%) sepia(99) grayscale(1)" src="'.$logo.'" width="120" alt="logo" /></a>
                                                                                        </td>
                                                                                </tr>
                                                                         </table>
                                                                 </td>
                                                         </tr>
                                                         <tr>
                                                                 <td class="content" style="text-align:left;">
                                                                        <h3>Hello '.$customerData['firstname'].' '.$customerData['lastname'].',</h3>
                                                                        <p> Welcome to Mowneh!
                                                                                <br/>Thank you for registering with mowneh.com. Please enter '.$otp.' to complete your registration. This OTP will expire in 5 minutes.</p>
                                                                                <p>
                                                                                <br/>If you did not create an account on Mowneh.com with this email address, please ignore this email or contact us on <a style="color: #1d1d1d;" href=" mailto:'.$email.'">'.$email.'</a>
                                                                                <br/>
                                                                                <br/>Regards,
                                                                                <br/>Mowneh Team

                                                                        </p>
                                                                 </td>
                                                         </tr>
                                                         <tr>
                                                                 <td>
                                                                        <table width="100%" style="border-spacing: 0;">
                                                                                <tr>
                                                                                        <td class="footer" style="border-top:1px solid #f2f2f2;padding: 15px;text-align:left;"> 
                                                                                                <p style="font-size: 12px;color: #a7a7a7;">
                                                                                                        '.$address.'<br/>Copyright © 2020 <a style="color: #a7a7a7;" href="'.base_url().'">Mowneh.</a> All Rights Reserved
                                                                                                </p>

                                                                                        </td>
                                                                                        <td style="border-top:1px solid #f2f2f2;padding: 15px;text-align:right;">
                                                                                                <a href="'.base_url().'"><img src="'.base_url("image/cache/siteInfo/2c0523d0dad5f48f434a75ab2136b33c-140x45.png").'" width="80" alt="logo"></a>
                                                                                                <br/>


                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td colspan="2" style="padding: 5px; background-color: #F06C00;">
                                                                                        </td>
                                                                                </tr>
                                                                        </table>

                                                                 </td>
                                                         </tr>
                                                 </table>
                                         </div>
                                 </center>
                                </body>
                                </html>';
            return $message;
    }
    
    
       public function registerConfirmMessageOLD($customerID=""){
        $customerData   = $this->generalModel->getTableValue('firstname,lastname,email', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
        $activationURL = base_url() . "Register/activateAccount/" . $customerID;
        $email      =   $this->getSettingValue('email');
        $phone      =   $this->getSettingValue('phone');
        $address    =   $this->getSettingValue('address', $this->siteLanguageID, true);
        
            $message    =   '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                                <html xmlns="http://www.w3.org/1999/xhtml">
                                <head>
                                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                                        <title>Activate Account</title>

                                        <style type="text/css">
                                        @font-face {font-family: "Mary Ann"; src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot"); src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot?#iefix") format("embedded-opentype"); }
                                                body {
                                                        Margin: 0;
                                                        padding: 0;
                                                        font-family: Mary Ann;
                                                        color: #1d1d1d;
                                                }
                                                table {
                                                        border-spacing: 0;
                                                }
                                                td {
                                                        padding: 0;
                                                }
                                                img {
                                                        border: 0;
                                                }
                                                .wrapper
                                                {
                                                        width: 100%;
                                                        table-layout: fixed;
                                                        background-color: #f6f6f6;
                                                }
                                                .webkit
                                                {
                                                        max-width: 600px;
                                                        background-color: #fff;
                                                }
                                                .content
                                                {
                                                        padding: 30px 20px;
                                                }
                                                .reset a
                                                {
                                                        color: #ffffff;
                                                        font-weight: 400;
                                                        text-transform: uppercase;
                                                        background-color: #F06C00;
                                                        border: 1px solid #F06C00;
                                                        text-decoration: none;
                                                        padding: 5px 8px;
                                                        text-align: center;
                                                        border-radius: 5px;
                                                }
                                                .reset a:hover
                                                {
                                                        color: #F06C00;
                                                        background-color: #fff;
                                                        border: 1px solid #F06C00;
                                                }
                                                .content p
                                                {
                                                        font-size: 15px;
                                                } 
                                                .footer
                                                {
                                                        border-top: 1px solid #f2f2f2;
                                                        padding: 30px 0px;
                                                }
                                                @media screen and (max-width: 600px) { 
                                                }
                                                @media screen and (max-width: 400px) { 
                                                }
                                        </style>
                                </head>
                                <body>
                                 <center class="wrapper">
                                         <div class="webkit">
                                                 <table class="outer" align="center">
                                                         <tr>
                                                                 <td>
                                                                         <table width="100%" style="border-spacing: 0;">
                                                                            <tr>
                                                                                        <td style="background-color:#F06C00;padding: 10px;text-align: center;">
                                                                                                <a href="'.base_url().'"><img style="filter:brightness(100) contrast(100%) sepia(99) grayscale(1)" src="'.base_url("image/cache/siteInfo/2c0523d0dad5f48f434a75ab2136b33c-140x45.png").'" width="120" alt="logo" /></a>
                                                                                        </td>
                                                                                </tr>
                                                                         </table>
                                                                 </td>
                                                         </tr>
                                                         <tr>
                                                                 <td class="content" style="text-align:left;">
                                                                        <h3>Hello '.$customerData['firstname'].' '.$customerData['lastname'].',</h3>
                                                                        <p> Welcome to Mowneh!
                                                                                <br/>Please click the link below to verify your email address and activated your account.</p>
//                                                                                <br/><a href="'.$activationURL.'">'.$activationURL.'</a>
                                                                                <p>
                                                                                <br/>If you did not create an account on Mowneh.com with this email address, please ignore this email or contact us on <a style="color: #1d1d1d;" href=" mailto:'.$email.'">'.$email.'</a>
                                                                                <br/>
                                                                                <br/>Regards,
                                                                                <br/>Mowneh Team

                                                                        </p>
                                                                 </td>
                                                         </tr>
                                                         <tr>
                                                                 <td>
                                                                        <table width="100%" style="border-spacing: 0;">
                                                                                <tr>
                                                                                        <td class="footer" style="border-top:1px solid #f2f2f2;padding: 15px;text-align:left;"> 
                                                                                                <p style="font-size: 12px;color: #a7a7a7;">
                                                                                                        '.$address.'<br/>Copyright © 2020 <a style="color: #a7a7a7;" href="'.base_url().'">Mowneh.</a> All Rights Reserved
                                                                                                </p>

                                                                                        </td>
                                                                                        <td style="border-top:1px solid #f2f2f2;padding: 15px;text-align:right;">
                                                                                                <a href="'.base_url().'"><img src="'.base_url("image/cache/siteInfo/2c0523d0dad5f48f434a75ab2136b33c-140x45.png").'" width="80" alt="logo"></a>
                                                                                                <br/>


                                                                                        </td>
                                                                                </tr>
                                                                                <tr>
                                                                                        <td colspan="2" style="padding: 5px; background-color: #F06C00;">
                                                                                        </td>
                                                                                </tr>
                                                                        </table>

                                                                 </td>
                                                         </tr>
                                                 </table>
                                         </div>
                                 </center>
                                </body>
                                </html>';
            return $message;
    }
    
    
    
}
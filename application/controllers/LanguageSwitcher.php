<?php

if (!defined('BASEPATH'))
    exit('Direct access allowed');

class LanguageSwitcher extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function switchLang($language = "", $side = 'frontend') {
        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
        redirect($_SERVER['HTTP_REFERER']);
    }

    function switchLangAjax($language = "english", $side = 'frontend') {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, OPTIONS");
        //echo $language;echo $side;
        $language = ($language != "") ? $language : 1;
        if ($side == 'frontend') {
            $this->session->set_userdata('site_lang', $language);
            if ($this->session->userdata('site_lang') == $language) {
                $res = array(
                    'status' => 'success',
                    'message' => 'Language changed successfully'
                );
            } else {
                $res = array(
                    'status' => 'Failed',
                    'message' => 'Language not changed '
                );
            }
        } else {
            $this->session->set_userdata('admin_lang', $language);
            if ($this->session->userdata('admin_lang') == $language) {
                $res = array(
                    'status' => 'success',
                    'message' => 'Language changed successfully'
                );
            } else {
                $res = array(
                    'status' => 'Failed',
                    'message' => 'Language not changed '
                );
            }
        }
        echo json_encode($res);
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Myaccount extends Account_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('Customer');
        $this->load->library('CustomerLogin');
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('AccountModel', 'accountModel');
        $this->load->model('OrderModel', 'orderModel');
        $this->load->model('CustomerModel', 'customerModel');
        $this->load->model('WarehouseModel', 'warehouseModel');
        $this->customerID = $this->customer->getID();
    }

    public function index() {
        //$data = array();
        //echo
        $this->data['breadcrumbs'] = "My Account";
        $this->data['pageTitle'] = "My Account";

        $customerID = $this->session->userdata('moonehcustomerID');
        $this->data['action'] = base_url('myaccount');
        if ($this->session->flashdata('success') != '') {
            $this->data['success'] = $this->session->flashdata('success');
        }
        if ($this->session->flashdata('failed') != '') {
            $this->data['failed'] = $this->session->flashdata('failed');
        }
        $this->form_validation->set_rules('firstname', 'First Name', 'required',array('required' => 'Please Provide First Name'));
        $this->form_validation->set_rules('lastname', 'Last Name', 'required',array('required' => 'Please Provide Last Name'));
        $this->form_validation->set_rules('telephone', 'Phone Number', 'required|min_length[8]|max_length[8]');
        $this->form_validation->set_rules('email', 'Email', 'required');

        if ($this->form_validation->run() === TRUE) {
            //echo "<pre>"; print_r($_FILES);exit;
            $errVal = 0;

            $this->data['errVal'] = $errVal;
            $resData['nonXss'] = array(
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
//                'telephone' => $this->input->post('telephone'),
                    //'status' => $this->input->post('status')
            );
//            if ($this->input->post('image')) {
//                $resData['nonXss']['image'] = $this->input->post('image');
//            }
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $resData['xssData']['dateAdded'] = date('Y-m-d H:i');
            $resData['xssData']['ip'] = $this->input->ip_address();
            $result = $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $customerID, $resData['xssData']);

            if ($result) {
                if ($errVal == 0) {
                    $this->session->set_flashdata('success', 'Updated Successfully');
                    // $this->logUserActivity('Myaccount Updated');
                    redirect('myaccount');
                }
                //$this->data['message'] = "Data added Updated";
            } else {
                $this->session->set_flashdata('failed', 'Failed To Update');
                redirect('myaccount');
            }
        }

        $this->data['content'] = $this->generalModel->getTableValue('*', 'ec_customer', 'customerID=' . "$customerID", FALSE); //data for edit
        $this->render('frontEnd/myaccount/accountDetails');
    }

    public function wishlist() {
        $customerID = $this->session->userdata('moonehcustomerID');
        $wishList = $this->accountModel->selectWishList($this->siteLanguageID, $this->customerID);
        foreach ($wishList as $key => $row) {
            if (isset($row['price']))
                $wishList[$key]['price'] = $this->currency->format($row['price'], $this->siteCurrency);
        }
        $this->data['wishList'] = $wishList;
        $this->render('frontEnd/myaccount/wishlist');
    }

    public function logout() {
        $this->customer->logout();
        //$this->logUserActivity('Customer LoggedOut');
        redirect('home');
    }

    public function changePassword() {
        $this->data['breadcrumbs'] = "Change Password";
        $this->data['pageTitle'] = "Change Password";
        $customerID = $this->session->userdata('moonehcustomerID');

        //$userEmail = $this->session->userdata('moonehEmail');

        $this->data['action'] = base_url('myaccount/changePassword/');
        if ($this->session->flashdata('success') != '') {
            $this->data['success'] = $this->session->flashdata('success');
        }
        if ($this->session->flashdata('failed') != '') {
            $this->data['failed'] = $this->session->flashdata('failed');
        }
        $this->form_validation->set_rules('OldPassword', 'Old Password', 'required');
        $this->form_validation->set_rules('password', 'New Password', 'required'); 
        $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'required|matches[password]');
        if ($this->form_validation->run() === TRUE) {
              $varUserData = $this->session->userdata;

            $resultStatus      = $this->customerlogin->checkOldPassword($varUserData['moonehEmail'], $this->security->xss_clean($this->input->post('OldPassword')));
            if($resultStatus['status']==0){
                 $this->session->set_flashdata('failed', lang('Incorrect_Old_Password'));
                 redirect('myaccount/changePassword');
            }
            
            $resData['nonXss'] = array(
                'password' => $this->input->post('password')
            );
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $passwordCreate = $this->customerlogin->resetCredentials($resData['xssData']['password']);
            //echo "<pre>"; print_r($passwordCreate);exit;
            //echo "<pre>"; print_r($passwordCreate); exit;
            $resData['xssData']['password'] = $passwordCreate['password'];
            $resData['xssData']['salt'] = $passwordCreate['salt'];
            //echo "<pre>";print_r($resData['xssData']['password']);exit;
           
            $result = $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $customerID, $resData['xssData']);
         

            if ($result) {
                $this->session->set_flashdata('success', lang('Password_changed_Successfully'));
                //$this->logUserActivity('password changed');
                redirect('myaccount/changePassword');
            } else {
                $this->session->set_flashdata('failed', lang('Failed_To_Update'));
                redirect('myaccount/changePassword');
            }
        }
        $this->render('frontEnd/myaccount/customer_change_password');
    }

    public function manageAddress() {
        $this->data['content'] = $this->accountModel->selectAddress($this->customerID);
        $this->data['defaultAddrID'] = $this->accountModel->selectAddressDefault($this->customerID);
        // echo '<pre>';print_r($this->data['content']);exit;
        $this->render('frontEnd/myaccount/address');
    }

    public function add() {

        $this->data['countryList'] = $this->warehouseModel->getTableValue('name', 'ec_country', 'countryID="173" AND status="Active"', TRUE);
        $this->data['stateList'] = $this->accountModel->selectStateList($this->siteLanguageID, $this->customerID);
        //$this->data['warehouseCity'] = $this->warehouseModel->getWarehouseCityInfo();
          $this->data['warehouseCity'] = $this->warehouseModel->getAllActiveCities($this->siteLanguageID);
         //echo "<pre>"; print_r($citylist); exit;
    
        //echo '<pre>';print_r($this->data['stateList']);exit;
        $customerID = $this->customer->getID();
        $countryID = 173;
        if ($this->session->flashdata('success') != '') {
            $this->data['success'] = $this->session->flashdata('success');
        }
        if ($this->session->flashdata('failed') != '') {
            $this->data['failed'] = $this->session->flashdata('failed');
        }
        // form validation 
        $this->form_validation->set_rules('firstname', 'First Name', 'required|xss_clean');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required|xss_clean');
        $this->form_validation->set_rules('address1', 'Address1', 'required|xss_clean');
        $this->form_validation->set_rules('address2', 'Address2', 'required|xss_clean');
        $this->form_validation->set_rules('city', 'city', 'required|xss_clean');
        $this->form_validation->set_rules('address3', 'Zone', 'required|xss_clean');
        // $this->form_validation->set_rules('country', 'country', 'required|xss_clean');
        $this->form_validation->set_rules('postcode', 'Post Code', 'xss_clean');
        $this->form_validation->set_rules('phone', 'Phone Number', 'required|min_length[8]|max_length[8]', array(
            'min_length' => 'Please provide a valid phone number',
            'max_length' => 'Please provide a valid phone number'
        ));
        
        if ($this->form_validation->run() === TRUE) {
            $city    = explode("~",$this->input->post('city'));
            $resData['nonXss'] = array(
                'customerID' => $customerID,
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'phone' => $this->input->post('phone'),
                'address1' => $this->input->post('address1'),
                'address2' => $this->input->post('address2'),
                'zone'          => $this->input->post('address3'),
                'city'          => $city[0],
                'cityID'        =>  $city[1],
                'postcode' => $this->input->post('postcode'),
                'countryID' => $countryID, 
                'additional_direction' => $this->input->post('additional_direction')
            );
            //echo "<pre>"; print_r($resData); exit;
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $result = $this->generalModel->insertValue('ec_address', $resData['xssData']);


            if ($result) {
                $this->session->set_flashdata('success', 'Password changed Successfully');
                //$this->logUserActivity('password changed');
                redirect('myaccount/manageAddress');
            } else {
                $this->session->set_flashdata('failed', 'Failed To Update');
                redirect('myaccount/manageAddress');
            }
        }
        $this->render('frontEnd/myaccount/addAddress');
    }

    public function edit() {

        $addressID = $this->uri->segment(3);
        $this->data['stateList'] = $this->accountModel->selectStateList($this->siteLanguageID, $this->customerID);
        $citylist = $this->warehouseModel->getAllActiveCities($this->siteLanguageID);
         //echo "<pre>"; print_r($citylist); exit;
         
        $this->data['warehouseCity']=$citylist;
        
        $this->data['countryList'] = $this->warehouseModel->getTableValue('countryID,name', 'ec_country', 'countryID="173" AND status="Active"', TRUE);
        // form validation 
        $this->form_validation->set_rules('firstname', 'First Name', 'required|xss_clean');
        $this->form_validation->set_rules('lastname', 'Last Name', 'required|xss_clean');
        $this->form_validation->set_rules('address1', 'Address1', 'required|xss_clean');
        $this->form_validation->set_rules('address2', 'Address2', 'required|xss_clean');
        $this->form_validation->set_rules('city', 'city', 'required|xss_clean');
        $this->form_validation->set_rules('address3', 'Zone', 'required|xss_clean');
        // $this->form_validation->set_rules('country', 'country', 'required|xss_clean');
        //$this->form_validation->set_rules('postcode', 'Post Code', 'required|xss_clean');
        $this->form_validation->set_rules('phone', 'phone', 'required|xss_clean');
        //image upload
        if ($this->form_validation->run() === TRUE) {
            $city    = explode("~",$this->input->post('city'));

            $resData['nonXss'] = array(
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'address1' => $this->input->post('address1'),
                'address2' => $this->input->post('address2'),
                'zone'          => $this->input->post('address3'),
                'city'          => $city[0],
                'cityID'        =>  $city[1],
                'countryID' => $this->input->post('countryID'),
                'postcode' => $this->input->post('postcode'),
                'phone' => $this->input->post('phone'),
                'additional_direction' => $this->input->post('additional_direction')
            );


            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $result = $this->generalModel->updateTableValues('ec_address', 'addressID=' . $addressID, $resData['xssData']);

            if ($result) {
                $this->session->set_flashdata('message', 'Data Update Successfully');
                //$this->logUserActivity('address Updated');
                redirect('Myaccount/manageAddress');
            } else {
                $this->session->set_flashdata('failMessage', 'Failed To Update');
                redirect('Myaccount/manageAddress');
            }
        }

        $this->data['content'] = $this->generalModel->getTableValue('*', 'ec_address', 'addressID=' . "$addressID", FALSE); //data for edit
        $this->render('frontEnd/myaccount/addAddress');
    }

    public function delete() {
        $addressID = $this->uri->segment(3);

        $customerID = $this->customer->getID();
        $this->generalModel->deleteTableValues('ec_address', array('customerID' => $customerID, 'addressID' => $addressID), FALSE);
        redirect('Myaccount/manageAddress');
    }

    public function deleteWishList() {
        $addressID = $this->uri->segment(3);
        $this->generalModel->deleteTableValues('ec_customer_wishlist', array('customerID' => $customerID, 'productID' => productID), FALSE);
        redirect('Myaccount/wishlist');
    }

    public function setAsDefault() {
        $addressID = $this->uri->segment(3);
        $customerID = $this->customer->getID();
        $resData = array(
            'addressID' => $addressID
        );
        $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $customerID, $resData);
        redirect('Myaccount/manageAddress');
    }

    public function ordersList() {
        $customerID = $this->session->userdata('moonehcustomerID');
//        $orderList = $this->orderModel->selectOrderList($this->customerID);
        $params =   array();
        $params['customerID']   =   $customerID;
        $params['languageID']   =   $this->siteLanguageID;
        $params['myAcc']        =   "Yes";
        $orderList = $this->orderModel->selectOrderList($params);
        foreach ($orderList as $key => $row) {
            if (isset($row['total']))
                $orderList[$key]['total'] = $this->currency->format($row['total'], $this->siteCurrency);
        }
        $this->data['orderList'] = $orderList;
//        echo '<pre>';print_r($this->data['orderList']);exit;
        $this->render('frontEnd/myaccount/order_list');
    }

    public function ordersDetails() {
        $customerID = $this->session->userdata('moonehcustomerID');
        $orderID = $this->uri->segment(3);
        //$orderID = $this->input->post('orderID');
        $customerID = $this->customer->getID();
        $orderDetails = $this->orderModel->selectDetails($this->customerID, $orderID);
        //echo "<pre>"; print_r($orderDetails); exit;
        list($width, $height) = $this->getSettingValue('order_listing_dimen');$optionData='';
        foreach ($orderDetails as $key => $row) {
            if($row['type']=='Bundle'){
                $bundle_products    =   $this->gm->getTableValue('bundleID,productID,bundleProductID', 'ec_product_bundle', 'productID=' . $row['productID'],TRUE);
                if($bundle_products){
                    foreach($bundle_products as $bunPro){
                        $bundleProductID    =   $bunPro['bundleProductID'];
                        $bundleProductData    =   $this->orderModel->getBundleProducts($bundleProductID,$this->siteLanguageID);
                        $bundleProductData['quantity']  =   $row['quantity']*$bundleProductData['quantity'];
                         $bundleProduct[]=$bundleProductData;
                    }
                    $orderDetails[$key]['bundleProducts']   =   $bundleProduct;
                }
                
                //echo "<pre>"; print_r($orderDetails[$key]['bundleProducts']);  echo "</pre>";
            }
            $orderDetails[$key]['image']    = $this->setSiteImage('product/' . $row['image'], $width, $height);
            $orderDetails[$key]['sumProd']  = $this->currency->format($row['sumProd'], $this->siteCurrency);
            $orderDetails[$key]['price']    = $this->currency->format($row['price'], $this->siteCurrency);
            $orderDetails[$key]['subtotal'] = $this->currency->format($row['price'], $this->siteCurrency);
            $orderDetails[$key]['coupon']   = $this->currency->format(0, $this->siteCurrency);
            $orderDetails[$key]['offer']    = $this->currency->format(0, $this->siteCurrency);
            $orderDetails[$key]['shipping'] = $this->currency->format(0, $this->siteCurrency);
            $orderDetails[$key]['tax']      = $this->currency->format(0, $this->siteCurrency);
            $orderDetails[$key]['totalSum'] = $this->currency->format($row['totalSum'], $this->siteCurrency);
            $totalData  =   $this->orderModel->orderTotal($row['orderID']);
            foreach($totalData as $keys=>$totData){

                $totalData[$keys]['title']   =   lang($totData['languageKey']);
                $totalData[$keys]['value']   =   $this->currency->format($totData['value'], $this->siteCurrency);
            }
            $optionData  =   $this->orderModel->orderOptionDetail($row['orderID'],$row['orderProductID']);
            if(@$optionData)
            $orderDetails[$key]['orderOptions']   = $optionData;
            
        }

        
        
        $this->data['orderDetails'] = $orderDetails;
        
        $this->data['totalData'] = $totalData;
        //print_r($this->data['orderOptions']); exit;
       
        $this->render('frontEnd/myaccount/order_details');
    }

    public function loadZone(){
        $cityID =   $this->input->post('cityID');
        $this->siteLanguageID;
        $result = $this->generalModel->getFieldValue('zone', 'ec_city_detail', 'cityID='.$cityID.' AND languageID=1');
        echo $result;
    }
}

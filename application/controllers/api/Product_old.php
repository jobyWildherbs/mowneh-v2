<?php
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends REST_Controller{
    public function __construct()
    {
        parent::__construct();  
        $this->load->model('GeneralModel','generalModel');
        $this->load->model('CategoryModel','catModel');
        $this->load->model('ProductModel', 'productModel');
        $this->load->library('form_validation');   
    }
    
    public function listProduct_get(){
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        
        if($valErr==0){ 
            $categoryID =   $this->input->get('categoryID');
            if(!$categoryID)
                $categoryID =   0;
            $searchKeyWord  = $this->security->xss_clean($this->input->get('searchKeyWord')) ? $_GET['searchKeyWord'] : '';
            $subCategory    = $this->security->xss_clean($this->input->get('subCategory')) ? (int) $_GET['subCategory'] : '';
            $filters        = $this->security->xss_clean($this->input->get('filter')) ? $_GET['filter'] : '';
            $brand          = $this->security->xss_clean($this->input->get('manufacturerID')) ? (int) $_GET['manufacturerID'] : '';
            $priceRange     = $this->security->xss_clean($this->input->get('price')) ? (int) $_GET['price'] : '';
            $order          = $this->security->xss_clean($this->input->get('order')) ?  $_GET['order'] : '';
            $page           = $this->security->xss_clean($this->input->get('page')) ? (int) $_GET['page'] : 0;
            $limit          = $this->security->xss_clean($this->input->get('limit')) ? (int) $_GET['limit'] : 20;
            

           
           
            $params = array(
                'customerID' => $this->userID,
                'languageID' => $languageID,
            );
            if ($searchKeyWord) {
                $params['searchKeyWord'] = $searchKeyWord;
            }
            if ($categoryID) {
                $params['categoryID'] = $categoryID;
            }
            if ($filters) {
                $params['filter'] = $filters;
            }
            if ($brand) {
                $params['brand'] = $brand;
            }
            if ($order) {
                $params['order'] = $order;
            }
            if ($subCategory) 
                $params['subCategory'] = $subCategory;
            
            if ($priceRange) {
                $priceVal = explode("~", $priceRange);
                $params['startPrice'] = $priceVal[0];
                $params['endPrice'] = $priceVal[1]; 
            }
            
            $params['limit'] = $limit;
            $params['start'] = $page;
            if($categoryID){
                $productList = $this->productModel->getProducts($params);
            }else{
                $productList = $this->productModel->getProductsWithOutCategory($params);
            }
          
            list($width, $height) = $this->getImageDimension('list_product_dimen');
                foreach ($productList as $key => $productData) {
                    unset($productList[$key]['description']);
                    unset($productList[$key]['pageKey']);
                    $productList[$key]['offPersentage'] = '';
                    $productList[$key]['discountPrice'] = '';
                    $productDiscount = $this->productModel->productDiscount($productData['productID']);
                    
                    $discountPrice = "";
                    if (!empty($productDiscount)) {
                        if ($productDiscount['type'] == 'fixed') {
                            $discountPrice = $productData['price'] - $productDiscount['price'];
                            $offPersentage = ($productDiscount['price'] / $productData['price']) * 100;
                            $productList[$key]['offPersentage'] = $offPersentage;
                        } else {
                            $offPersentage = $productDiscount['price'];
                            $productList[$key]['offPersentage'] = $offPersentage;
                            $discountPrice = $productData['price'] - ($productData['price'] * $productDiscount['price'] / 100);
                        }
                    }
                    if ($discountPrice != '')
                        $productList[$key]['discountPrice'] = $this->currency->format($discountPrice, $this->siteCurrency);
                    else 
                        $productList[$key]['discountPrice'] =   '';

                    $productList[$key]['price'] = $this->currency->format($productData['price'], $this->siteCurrency);
                    $productList[$key]['image'] = $this->setSiteImage("product/" . $productData['image'], $width, $height);
                    //exit;
//                    $productDiscount    =   ""; //temproraly
//                    $productList[$key]['productDiscount'] = $productDiscount;

                    $now = time(); // or your date as well
                    $dateAdded = strtotime($productData['dateAdded']);
                    $datediff = $now - $dateAdded;
                    if (round($datediff / (60 * 60 * 24)) < 7) {
                        $productList[$key]['new'] = 'new';
                    }
                }
                $data   =   array();
                if($productList){
                    $status      =   parent::HTTP_OK;
                    $message     =   'Success'; 
                    
                    $data['productList']        =   $productList; 
                    $data['pageOffset'] = $page+$limit;

                }else{
                    $status      =   parent::HTTP_BAD_REQUEST;
                    $message     =   'Sorry No Product Found'; 
                    $data        =   '';
                }
            }else{
                $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
                $message    =   $msgvalue;
                $status      =   parent::HTTP_BAD_REQUEST;
            } 
           $this->setHeaderToken(); 
            
                
        
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function productDetail_get() {
        
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $resultData     =   array();
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        $productID      = $this->security->xss_clean($this->input->get('productID')) ? (int) $_GET['productID'] : 0;
        
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        
        if($productID){
            if (!is_numeric($productID)){
                $valErr =   1;
                $valErrMsg['productID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['productID']    =  'productID missing';   
        }
        
        if($valErr==0){ 
            $userID =   $this->userID;
                $productDetail = $this->productModel->selectProductDetail($productID, $languageID,'',$userID);
               //echo "<pre>"; print_r($productDetail); exit;
                if(!empty($productDetail)){
                    $additionalImage = $this->generalModel->getTableValue('productImageID,image', 'ec_product_image', 'productID=' . $productID, TRUE); //data count for pagination
                   
                    list($mainWidth, $mainHeight) = $this->getImageDimension('product_popup_dimen');
                    list($smallWidth, $smallHeight) = $this->getImageDimension('product_page_thumb_dimen');
                    $mainImage = $productDetail['image'];
                    $productDetail['image'] = array(
                        'popup' => $this->setSiteImage("product/" . $mainImage, $mainWidth, $mainHeight),
                        'thumb' => $this->setSiteImage("product/" . $mainImage, $smallWidth, $smallHeight)
                    );
                    //$productDetail['additionalImage'] = $additionalImage;
                    $images = array();
                    $images[]   =   array(
                        'popup' => $this->setSiteImage("product/" . $mainImage, $mainWidth, $mainHeight),
                        'thumb' => $this->setSiteImage("product/" . $mainImage, $smallWidth, $smallHeight)
                    );
                    foreach ($additionalImage as $imageVal) {
                        $images[] = array(
                            'popup' => $this->setSiteImage("product/" . $imageVal['image'], $mainWidth, $mainHeight),
                            'thumb' => $this->setSiteImage("product/" . $imageVal['image'], $smallWidth, $smallHeight)
                        );
                    }
                    $productDetail['additionalImage'] = $images;
                    $productAttr = $this->productModel->getProductAttributeByProductID($productID, $languageID);
                    
                    $attribute  =   '';
                    $productAttributeData = array();
                    $productAttribute   =   array();
                    if($productAttr){
                        foreach ($productAttr as $key => $data) {
                            $productAttributeData[$data['groupName']][$key]['attribute'] = $data['name'];
                            $productAttributeData[$data['groupName']][$key]['value'] = $data['text'];
                        }
                       //echo "<pre>"; print_r($productAttributeData); exit;
                         $i=0;
                         foreach ($productAttributeData as $key => $attrArrData) {
                            
                            $productAttribute[$i]['attributeGroup'] =   $key; 
                            $j  =   0;
                            foreach ($attrArrData as $keyCount => $attrArrDataVal) {
                                $productAttribute[$i]['attribute'][$j]['name'] =   $attrArrDataVal['attribute'];   
                                $productAttribute[$i]['attribute'][$j]['value'] =   $attrArrDataVal['value'];
                                $j++;
                            }
                            $i++;
                        }
                    } 
                   
 
                    //echo "<pre>";print_r($productAttribute);exit;
                    list($width, $height) = $this->getImageDimension('list_product_dimen');
                    $relatedProduct = $this->productModel->reletedProductList($productID, $languageID);

                    foreach ($relatedProduct as $key => $reletadData) {
                        $relatedProduct[$key]['price'] =    $this->currency->format($reletadData['price'], $this->siteCurrency);
                        $relatedProduct[$key]['image'] =    $this->setSiteImage("product/" . $reletadData['image'], $width, $height);
                    }

                    $productDiscount = $this->productModel->productDiscount($productID);
                    //echo $this->db->last_query();
                    //print_r($productDiscount);exit;
                    $discountPrice = "";
                    if (!empty($productDiscount)) {
                        if ($productDiscount['type'] == 'fixed') {
                            $discountPrice = $productDetail['price'] - $productDiscount['price'];
                            $offPersentage = ($productDiscount['price'] / $productDetail['price']) * 100;
                            $productDetail['offPersentage'] = $offPersentage;
                        } else {
                            $offPersentage = $productDiscount['price'];
                            $productDetail['offPersentage'] = $offPersentage;
                            $discountPrice = $productDetail['price'] - ($productDetail['price'] * $productDiscount['price'] / 100);
                        }
                    }
                    if ($productDetail['price'] != '') {
                        $productDetail['price'] = $this->currency->formatAPI($productDetail['price'], $this->siteCurrency);
                       
                         $productDetail['pricePrifix'] =   $this->siteCurrency;
                    }
                    $productDetail['productDiscount']   =   "";
                    if ($discountPrice != '') {
                        $productDetail['discountPrice'] = $this->currency->format($discountPrice, $this->siteCurrency);
                        $productDetail['productDiscount']   = $this->currency->format($discountPrice, $this->siteCurrency);
                    }
                    $productDetail['productAttribute']  =   $productAttribute;
                    $productDetail['relatedProduct']    =   $relatedProduct;
//                    $productDetail['productDiscount']   = $productDiscount;$productDetail['price'] - ($productDetail['price'] * $productDiscount['price'] / 100);
                    
                    $this->setHeaderToken(); 
                    //echo "<pre>"; print_r($productDetail); exit;
//                    $resultData['productDetail']    = $productDetail;
//                    $resultData['productAttribute'] = $productAttribute;
//                    $resultData['relatedProduct']   = $relatedProduct;
//                    $resultData['productDiscount']  = $productDiscount;
                     //echo "<pre>"; print_r($resultData); exit;
                    
                }
                
              
                
            if($productDetail){
                $status      =   parent::HTTP_OK;
                $message     =   'Succss'; 
                $data        =   $productDetail; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   'Sorry No Data Found'; 
                $data        =   '';
            }
                
        }
       else{
                $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
                $message    =   $msgvalue;
               $status      =   parent::HTTP_BAD_REQUEST;
               
            }
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
  
}
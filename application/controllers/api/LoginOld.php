<?php
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends REST_Controller{
    public function __construct()
    {
        parent::__construct();  
        $this->load->library('CustomerLogin');
        $this->load->model('GeneralModel','generalModel');
        $this->load->library('form_validation');   
    }

    
    public function  userLogin_post(){
        
        if($this->input->post()){
            
            
            $config = [
                            [
                                    'field' => 'email',
                                    'label' => 'Email',
                                    'rules' => 'required|valid_email|xss_clean',
                                    'errors' => [
                                            'required'      => 'Please enter email',
                                            'valid_email'   => 'Please enter valid email',

                                    ],
                            ],

                            [
                                    'field' => 'password',
                                    'label' => 'Password',
                                    'rules' => 'required',
                                    'errors' => [
                                            'required' => 'Please enter password',
                                            ],
                            ],
                        ];

                $data = $this->input->post();
                $this->form_validation->set_data($data);
                $this->form_validation->set_rules($config);
           
            if($this->form_validation->run()===TRUE)
            {  
                $resData['nonXss']    = array(
                            'email'         => $this->input->post('email'),
                            'password'      => $this->input->post('password')
                            );
                 $resData['xssData']   = $this->security->xss_clean($resData['nonXss']);

                 $resultStatus      = $this->customerlogin->checkCredentials($resData['xssData']['email'], $resData['xssData']['password']);   
                 
                 if ($resultStatus['status']==1){   
                    
                    // login atttempt table clear
                    $where          = array('login'=>$resData['xssData']['email']);
                    $this->generalModel->deleteTableValues('ec_login_attempts',$where);
                    // login ip address update
                   
                    $ipAddress      = $this->input->ip_address();
                    $values         = array('ip' => $ipAddress,'lastLogin' => date('Y-m-d h:i:s'));
                    $where          = array('email'      => $resData['xssData']['email']);
                    $this->generalModel->updateTableValues('ec_customer',$where,$values);
                    // admin Detials 
                    $customer          = $this->generalModel->getTableValue('customerID,firstname,lastname,email,telephone,status','ec_customer',array('email'=> $resData['xssData']['email'],'status!='=>'Deleted'));
                    
                   
                    //setting token to mobile App
                        $this->apisupport->updateToken($customer['customerID']);
                    //Token end
                    
                    $customerData        = array(
                        'moonehcustomerID'          => $customer['customerID'],
                        'moonehFirstname'           => $customer['firstname'],
                        'moonehLastname'            => $customer['lastname'],
                        'moonehEmail'               => $customer['email'],
                        'telephone'                 => $customer['telephone'],
                        'moonehStatus'              => $customer['status'],
                        'moonehLoggedIn'            => TRUE
                    );
                    
                    if($customer){
                            $message        = "Success";
                            $token          = $this->apisupport->getToken($customer['customerID']);
                            $status         = parent::HTTP_OK;
                            $data           =  $customerData;
                    }else{
                            $message    = $resultStatus['message'];
                            $status     = parent::HTTP_PRECONDITION_FAILED;
                            $data       =   '';
                            $token      =   '';
                    }
                    
                }elseif ($resultStatus['apiStatus']==1){
                   
                     // login atttempt table clear
                    $where          = array('login'=>$resData['xssData']['email']);
                    $this->generalModel->deleteTableValues('ec_login_attempts',$where);
                    // login ip address update
                   
                    $ipAddress      = $this->input->ip_address();
                    $values         = array('ip' => $ipAddress,'lastLogin' => date('Y-m-d h:i:s'));
                    $where          = array('email'      => $resData['xssData']['email']);
                    $this->generalModel->updateTableValues('ec_customer',$where,$values);
                    // admin Detials 
                    $customer          = $this->generalModel->getTableValue('customerID,firstname,lastname,email,telephone,status','ec_customer',array('email'=> $resData['xssData']['email'],'status!='=>'Deleted'));
                     
                   
                    //setting token to mobile App
                        $this->apisupport->updateToken($customer['customerID']);
                    //Token end
                    
                    $customerData        = array(
                        'moonehcustomerID'          => $customer['customerID']
                    );
                   
                    if($customer){
                        
                            $message        = $resultStatus['message'];
                            
                            $token          = '';
                            $status         = parent::HTTP_UNPROCESSABLE_ENTITY;
                            $data           =  $customerData;
                    }else{
                            $message    = $resultStatus['message'];
                            $status     = parent::HTTP_PRECONDITION_FAILED;
                            $data       =   '';
                            $token      =   '';
                    }
                    
                }
                else{ 
                            $message    = $resultStatus['message'];;
                            $status     = parent::HTTP_PRECONDITION_FAILED;
                            $data       =   '';
                            $token      =   '';
                }
              
            }
             else{
               $msgvalue    =   '';
                $i  =   1;
                foreach($this->form_validation->error_array() as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $message     =   $msgvalue; 
               $data        =   '';
               $token      =   '';
               
            }
        }  
        
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $result['token']        = $token;
//        $status                 =   'REST_Controller::'.$status;
        $this->response($result, $status);
    }
    
     public function forgotPassword_post(){
        $message    =   "";
        $errMessage =   "";
         $config = [
                            [
                                    'field' => 'email',
                                    'label' => 'Email',
                                    'rules' => 'required|valid_email|xss_clean',
                                    'errors' => [
                                            'required'      => 'Please enter email',
                                            'valid_email'   => 'Please enter valid email',

                                    ],
                            ],

                        ];

                $data = $this->input->post();
                $this->form_validation->set_data($data);
                $this->form_validation->set_rules($config);
        if($this->form_validation->run()===TRUE)
        {
            $email  =   $this->input->post('email');
            $user   =   $this->generalModel->getTableValue('customerID,firstname,lastname,email,status', 'ec_customer', array("email" => $email, 'status!=' => 'Deleted'));
            if ($user['status'] == 'Active') {
                $resetLink  =   base_url('/Login/resetPassword/'.$user['customerID']);
                $config['protocol'] = 'sendmail';
                $config['mailtype'] = 'html';
                $from_email =   $this->getSettingValue('email');
                
                
                    $firstLine = "Dear " . $user['firstname'] . " " . $user['lastname'];
                    $secondLine = "Please click below link to reset your password";
                    $emailMessage = '<table border="0"><tr><td>'.$firstLine.'</td></tr>'
                            . '<tr><td>'.$secondLine.'</td></tr>'
                            . '<tr><td><a href="'.$resetLink.'">Click Here</a></td></tr>'
                            . '<tr><td>URL : '.$resetLink.'</td></tr>'
                            . '<tr><td>Thank You</td></tr></table>';
                    //$to_email = $this->input->post('email');
                    //Load email library 
                    $this->load->library('email');
                    $this->email->initialize($config);
                    
                    $this->email->from($from_email, 'Mooneh');
//                    $this->email->to($to_email);
                    $this->email->to($user['email']);
                    $this->email->subject('mowneh reset password');
                    $this->email->message($emailMessage);
                    if($this->email->send()){
                        $message = lang('ForgotPasswordSuccess');
                    }else{
                         $message = lang('Something_went_wrong_please_try_again');
                    }
                    
            }elseif ($user['status'] == 'Inactive') {
                $message = lang('Your_account_is_inactive_now');
            }elseif ($user['status'] == 'Notverified') {
                $message = lang('Account_Not_Verified');
            }else{
                $message = lang('User_Not_Found');
            }
            $status         = parent::HTTP_OK;
        }else{
               $msgvalue    =   '';
                $i  =   1;
                foreach($this->form_validation->error_array() as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $message     =   $msgvalue; 
               $data        =   '';
               $token      =   '';
               
            }  
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
//        $status                 =   'REST_Controller::'.$status;
        $this->response($result, $status);
    }
    
    
    public function  userLoginEmailOrPhone_post(){
        if($this->input->post()){
            
            
            $config = [
                            [
                                    'field' => 'emailPhone',
                                    'label' => 'Email',
                                    'rules' => 'required|xss_clean',
                                    'errors' => [
                                            'required'      => 'Please enter email/phone',

                                    ],
                            ],
                        ];

                $data = $this->input->post();
                $this->form_validation->set_data($data);
                $this->form_validation->set_rules($config);
           
            if($this->form_validation->run()===TRUE)
            {  
                $resData['nonXss']    = array(
                            'emailPhone'         => $this->input->post('emailPhone')
                            );
                 $resData['xssData']   = $this->security->xss_clean($resData['nonXss']);
                 $emailPhone    =   $resData['xssData']['emailPhone'];
               
                 
                $user   =   $this->generalModel->getTableValue('customerID,firstname,lastname,email,countryCode,telephone,status', 'ec_customer','(email ="'. $emailPhone.'" OR telephone="'. $emailPhone.'") AND status!="Deleted"',FALSE);
              
                if($user['status']=='Active' || $user['status']=='Notverified'){
                    
                    $userID =   $user['customerID'];
                    $value['status'] = 'expired';
                    $this->generalModel->updateTableValues('ec_otp', 'customerID=' . $userID .' AND otpType="userLogin"', $value);
                    $otp    =   substr(number_format(time() * rand(),0,'',''),0,6); 
                   // $otpExist   =    $this->generalModel->getTableValue('otpID', 'ec_otp','otp ="'. $otp.'" AND otpType="userLogin" AND status="Active"',FALSE);

                    $resOtpData['otp']            =   $otp;
                    $resOtpData['customerID']     =   $userID;
                    $resOtpData['otpType']        =   'userLogin';
                    $resOtpData['otpTime']        =   date("Y-m-d H:s:i");
                    $resOtpData['addedDate']      =   date("Y-m-d H:s:i");
                    $otpID   =   $this->generalModel->insertValue('ec_otp',$resOtpData);
                    if($otpID){
                        $phoneNo    =   $user['countryCode'].''.$user['telephone'];
                        $smsMessage =   "Use ".$otp." as your verification code on mowneh.com . The OTP expires in 5 minutes. Please do not share with anyone.";
                        
                        $this->sendSMS($smsMessage,$phoneNo);
                        $message        = "Success";
                        $status         = parent::HTTP_OK;
                        $data           =  array('userID'=>$userID);
                    }else{
                        $message    =   "something went wrong Please try again";
                        $status         = parent::HTTP_PRECONDITION_FAILED;
                        $data           =  '';
                    }
                    
                }elseif ($user['status']=='Inactive') {
                    $message    =   "Your account is inactive. Please contact mowneh";
                    $status         = parent::HTTP_PRECONDITION_FAILED;
                    $data           =  '';
                }
                else{
                    $message    =   "This mobile number is not registered with mowneh";
                    $status         = parent::HTTP_PRECONDITION_FAILED;
                    $data           =  '';
                }
              
            }
             else{
               $msgvalue    =   '';
                $i  =   1;
                foreach($this->form_validation->error_array() as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $message     =   $msgvalue; 
               $data        =   '';
               $token      =   '';
               
            }
        }  
        
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function  userLoginWithOtp_post(){
        $customerData   =   array();
        if($this->input->post()){
            $config = [
                            [
                                    'field' => 'userID',
                                    'label' => 'User ID',
                                    'rules' => 'required|xss_clean',
                                    'errors' => [
                                            'required'      => 'Please enter User ID',

                                    ],
                            ],
                            [
                                    'field' => 'otp',
                                    'label' => 'OTP',
                                    'rules' => 'required|xss_clean',
                                    'errors' => [
                                            'required'      => 'Please enter OTP',

                                    ],
                            ],
                        ];

                $data = $this->input->post();
                $this->form_validation->set_data($data);
                $this->form_validation->set_rules($config);
           
            if($this->form_validation->run()===TRUE)
            {  
                $resData['nonXss']    = array(
                            'userID'         => $this->input->post('userID'),
                            'otp'            => $this->input->post('otp')
                            );
                 $resData['xssData']   = $this->security->xss_clean($resData['nonXss']);
                 $userID    =   $resData['xssData']['userID'];
                 $otp       =   $resData['xssData']['otp'];
               
                 
                $otpExist   =   $this->generalModel->getTableValue('otpID', 'ec_otp','otp ="'. $otp.'" AND otpType="userLogin" AND customerID ="'. $userID.'" AND status="Active" AND TIMESTAMPDIFF(SECOND,ec_otp.addedDate,NOW()) < 300',FALSE);
                if($otpExist){
                    $customer          = $this->generalModel->getTableValue('customerID,firstname,lastname,email,telephone,status','ec_customer','customerID=' .$userID .' AND (status="Active" OR status="Notverified")');
                    $value['status'] = 'used';
                    $this->generalModel->updateTableValues('ec_otp', 'customerID=' . $userID .' AND otpType="userLogin" AND otp='. $otp, $value);
                    
                    $this->apisupport->updateToken($customer['customerID']);
                    //Token end
                    if($customer){
                        $statusData['nonXss'] = array(
                            'status'     => 'Active',
                        );
                        $statusData['xssData'] = $this->security->xss_clean($statusData['nonXss']);
                        $this->generalModel->updateTableValues('ec_customer', 'customerID = ' . $userID, $statusData['xssData']);
                        
                        
                         $customerData        = array(
                            'moonehcustomerID'          => $customer['customerID'],
                            'moonehFirstname'           => $customer['firstname'],
                            'moonehLastname'            => $customer['lastname'],
                            'moonehEmail'               => $customer['email'],
                            'telephone'                 => $customer['telephone'],
                            'moonehStatus'              => $customer['status'],
                            'moonehLoggedIn'            => TRUE
                        );
                    }
                       
                   
                
                    
                    if($customerData){
                            $message        = "Success";
                            $token          = $this->apisupport->getToken($customer['customerID']);
                            $status         = parent::HTTP_OK;
                            $data           =  $customerData;
                    }else{
                            $message    = 'Failed';
                            $status     = parent::HTTP_PRECONDITION_FAILED;
                            $data       =   '';
                            $token      =   '';
                    }
                    
                }else{
                    $message    =   "Invalid OTP";
                    $status         = parent::HTTP_OK;
                    $data           =  '';
                    $token      =   '';
                }
              
            }
             else{
               $msgvalue    =   '';
                $i  =   1;
                foreach($this->form_validation->error_array() as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $message     =   $msgvalue; 
               $data        =   '';
               $token      =   '';
               
            }
        }  
        
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
         $result['token']        = $token;
        $this->response($result, $status);
    }
    
    public function  resendLoginOtp_post(){
        if($this->input->post()){
            
            
            $config = [
                           [
                                    'field' => 'userID',
                                    'label' => 'User ID',
                                    'rules' => 'required|xss_clean',
                                    'errors' => [
                                            'required'      => 'Please enter User ID',

                                    ],
                            ],
                        ];

                $data = $this->input->post();
                $this->form_validation->set_data($data);
                $this->form_validation->set_rules($config);
           
            if($this->form_validation->run()===TRUE)
            {  
                $resData['nonXss']    = array(
                            'userID'         => $this->input->post('userID')
                            );
                 $resData['xssData']   = $this->security->xss_clean($resData['nonXss']);
                 $userID    =   $resData['xssData']['userID'];
               
                 
                $otpExist   =   $this->generalModel->getTableValue('otpID,otp', 'ec_otp','otpType="userLogin" AND customerID ="'. $userID.'" AND status="Active"',FALSE);
                $user   =   $this->generalModel->getTableValue('customerID,firstname,lastname,email,countryCode,telephone,status', 'ec_customer','customerID="'.$userID.'"',FALSE);
                //echo "<pre>"; print_r($otpExist); exit;
                if($otpExist){
                        $value['otpTime']        =   date("Y-m-d H:s:i");
                        $value['addedDate']      =   date("Y-m-d H:s:i");
                        
                        $this->generalModel->updateTableValues('ec_otp', 'otpID=' . $otpExist['otpID'] .' AND otpType="userLogin"', $value);
                        $otp    =   $otpExist['otp'];
                        $phoneNo    =   $user['countryCode']."".$user['telephone'];
                        $smsMessage =   "Use ".$otp." as your verification code on mowneh.com . The OTP expires in 5 minutes. Please do not share with anyone.";
                        $this->sendSMS($smsMessage,$phoneNo);
                        
                        $message        = "OTP has been re-sent to the registered mobile.";
                        $status         = parent::HTTP_OK;
                        $data           =  array('userID'=>$userID);
                }else{
                    
                   
                    $value['status'] = 'expired';
                    $this->generalModel->updateTableValues('ec_otp', 'customerID=' . $userID .' AND otpType="userLogin"', $value);
                    $otp    =   substr(number_format(time() * rand(),0,'',''),0,6); 
                   // $otpExist   =    $this->generalModel->getTableValue('otpID', 'ec_otp','otp ="'. $otp.'" AND otpType="userLogin" AND status="Active"',FALSE);
                   
                
                    $resOtpData['otp']            =   $otp;
                    $resOtpData['customerID']     =   $userID;
                    $resOtpData['otpType']        =   'userLogin';
                    $resOtpData['otpTime']        =   date("Y-m-d H:s:i");
                    $resOtpData['addedDate']      =   date("Y-m-d H:s:i");
                    $otpID   =   $this->generalModel->insertValue('ec_otp',$resOtpData);
                    if($otpID){
                        $phoneNo    =   $user['countryCode']."".$user['telephone'];
                        $smsMessage =   "Use ".$otp." as your verification code on mowneh.com . The OTP expires in 5 minutes. Please do not share with anyone.";
                        $this->sendSMS($smsMessage,$phoneNo);
                        $message        = "An OTP (one-time-password) has been re-sent to your registered phone number.";
                        $status         = parent::HTTP_OK;
                        $data           =  array('userID'=>$userID);
                    }else{
                        $message    =   "something went wrong Please try again";
                        $status         = parent::HTTP_PRECONDITION_FAILED;
                        $data           =  '';
                    }
                    
                }
//                else{
//                    $message    =   "Invalid User";
//                    $status         = parent::HTTP_OK;
//                    $data           =  '';
//                }
              
            }
             else{
               $msgvalue    =   '';
                $i  =   1;
                foreach($this->form_validation->error_array() as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $message     =   $msgvalue; 
               $data        =   '';
               $token      =   '';
               
            }
        }  
        
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
}
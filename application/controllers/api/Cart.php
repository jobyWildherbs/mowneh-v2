<?php
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
defined('BASEPATH') OR exit('No direct script access allowed');
class Cart extends REST_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->library('CustomerLogin');
        $this->load->model('CartModel', 'cartModel');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('ProductModel', 'pm');
        $this->load->model('AccountModel', 'accountModel');
        $this->load->model('OrderModel', 'orderModel');
        $this->load->model('WarehouseModel', 'wm');
        $this->load->library('OrderApi');
        $this->load->library('CartsApi');
        $this->load->model('CouponModel', 'coupon');
        $this->load->model('OptionModel', 'optionModel');
        $timeZone   =   $this->getSettingValue('time_zone');
        date_default_timezone_set($timeZone);
//        $languageID     =   $this->input->post('languageID');
//        if(!$languageID)
//            $languageID =   '';
//        $this->lang->load('site', $languageID);
//        $this->lang->load('error', $languageID);
       $this->customerID = $this->customer->getID();
    } 
    
     public function cartList_get() {
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        
            
        
        if($valErr==0){
            $cartData   =   array();
            $userID         =   $this->userID;
            $cartItems      =   $this->cartsapi->getProducts($languageID,$userID); 
            //echo "<pre>"; print_r($cartItems); exit;
            list($width, $height) = $this->getImageDimension('cart_image_dimen');
            
            foreach ($cartItems as $key => $cart) {
//                $cartItems[$key]['price'] = $this->currency->format($cart['price'], $this->siteCurrency);
                $cartItems[$key]['price'] = $this->currency->formatAPI($cart['price'], $this->siteCurrency);
                $cartItems[$key]['total'] = $this->currency->formatAPI($cart['total'], $this->siteCurrency);
                $cartItems[$key]['pricePrifix'] = $this->siteCurrency;
                $cartItems[$key]['image'] = $this->setSiteImage('product/' . $cart['image'], $width, $height);
                if($cart['option']){
                    $optionDataVals =   array();
                    foreach($cart['option'] as $cartOptionKey=>$option){
                        $i  =   0;
                        
                        $optionvals = explode(':', $option);
                        foreach($optionvals as $optionvalData){
                            $optionData = explode('-', $optionvalData);
                            $optionDataVals[$optionData[0]] =   $optionData[1];
                           $i++;
                        }
                        $productOptions   =   $this->optionModel->selectOptionDetail($optionDataVals['productOptionID'],$languageID,$optionDataVals['optionValueId']);
                        
                        foreach($productOptions as $productOptionsData){
                           $cartItems[$key]['optionValue'][]   =   $productOptionsData['oName']."-".$productOptionsData['name'];
                        }
              
                    }
                }
                
            }
//            echo "<pre>"; print_r($cartItems);
//             exit;
           $cartDataItems['cartItems'] =   $cartItems;
           $couponData =   array();
            if ($this->input->get('couponCode')) {
                $couponCode =   $this->input->get('couponCode');
                $coupon =   $this->coupon->selectCoupon($couponCode);
                if(!empty($coupon)){
                    $couponData        = array('couponData'=>array(
                        'couponID'          => $coupon['couponID'],
                        'code'              => $coupon['code'],
                        'type'              => $coupon['type'],
                        'discount'          => $coupon['discount'],
                        'status'            => $coupon['status']
                    ));
                }
            }
            if (!empty($cartItems)) {
                $allTotals = $this->cartsapi->getTotalItemsList($languageID,$userID,$couponData);
//                echo "<pre>"; print_r($allTotals); exit;
                foreach ($allTotals as $key => $tolVal) {
                    $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency);
                    $allTotals[$key]['languageKey'] = lang($allTotals[$key]['code']);
                }
                $cartDataItems['subTotal'] = $allTotals;
            }
            
           // echo "<pre>"; print_r($cartDataItems); exit;
            if(!empty($cartDataItems)){

                $status      =   parent::HTTP_OK;
                $message     =   lang('Success');  
                $data        =   $cartDataItems; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   lang('Sorry_No_Data_Found');
                $data        =   '';
            }
        }
        else{
               $status      =   parent::HTTP_BAD_REQUEST;
               $message     =   $valErrMsg; 
               $data        =   '';
               $token      =   ''; 
            }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function addToCart_post() {
         $config = [
                        [
                                'field' => 'languageID',
                                'label' => 'languageID',
                                'rules' => 'required|numeric',
                                'errors' => [
                                        'required'      => 'languageID missing',
                                        'numeric'       => 'Only numbers allowed',
                                ],
                        ],
                        [
                            'field' => 'productID',
                            'label' => 'productID',
                            'rules' => 'required|numeric',
                            'errors' => [
                                'required'      => 'productID missing',
                                'numeric'       => 'Only numbers allowed',

                            ],
                        ],
                        [
                            'field' => 'quantity',
                            'label' => 'quantity',
                            'rules' => 'required|numeric',
                            'errors' => [
                                'required'      => 'quantity missing',
                                'numeric'       => 'Only numbers allowed',

                            ],
                        ]
                    ];
        $_POST['languageID']     =   $this->security->xss_clean($this->input->post('languageID')) ? (int) $_POST['languageID'] : $this->defalutLanguage;
        $data = $this->input->post();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() === TRUE) {
            $data = array();
            $message    ='';
            $userID         =   $this->userID;
            
            $productID      =   $this->security->xss_clean($this->input->post('productID'));
            $quantity       =   $this->security->xss_clean($this->input->post('quantity'));
            $languageID     =   $this->security->xss_clean($this->input->post('languageID'));
            
            $option         =   $this->security->xss_clean($this->input->post('option')) ? $_POST['option'] : '';
          
            if ($productID > 0) {
                $productInfo = $this->generalModel->getTableValue('quantity,minimum,status,dateAvailable', 'ec_product', array("productID" => $productID, 'status' => 'Active'));
               
                //echo "<pre>"; print_r($productInfo); exit;
                if ($productInfo['status'] != 'Active') {
                    $message    =   "Inactive product";
                }
                if ($productInfo['dateAvailable'] != '0000-00-00' && $productInfo['dateAvailable'] > date('Y-m-d')) {
                    $message = sprintf($this->lang->line('product_date_available_error'), date('data, F Y', strtotime($productInfo['dateAvailable'])));
                }
                if ($quantity < $productInfo['minimum']) {
                    $quantity = $productInfo['minimum'];
                }
                
                if ($productInfo['quantity'] < ($this->cartsapi->getProductQuantity($productID,$userID) + $quantity)) {
                    $message = $this->lang->line('quantity_not_available');
                }
                
                if($option){
                    $optionval = explode(',', $option);  
                    $productOptionID=0;
                    $optionArray = array();
                    foreach($optionval as $option){
                        $optionvals = explode(':', $option);$i=0;
                        $optionArray[] = json_encode($option);
                        foreach ($optionvals as $value) {
                            $vals = explode('-', $value);
                            if($vals[0] == 'productOptionID'){
                                $productOptionID = $vals[1];

                            }
                        }

                    }
                   
                    $productOptionInfo = $this->generalModel->getTableValue('quantity', 'ec_product_option', array("productID" => $productID, 'status' => 'Active',"productOptionID" => $productOptionID));
                   //echo $productOptionInfo['quantity']; exit;
                    //echo $this->db->last_query();exit;
                    //echo "<pre>"; print_r($productOptionInfo); exit;
                    //echo $this->cartsapi->getProductOptionQuantity($productID,$productOptionID,$userID) + $quantity; exit;
                    if ($productOptionInfo['quantity'] < ($this->cartsapi->getProductOptionQuantity($productID,$productOptionID,$userID) + $quantity)) {
                        $message = $this->lang->line('quantity_not_available');
                    }
                }
               
                if ($message=='') {
                    $this->cartsapi->add($productID, $quantity,$option,'',$userID,$this->apisupport->getToken($userID));
                    if ($this->cartsapi->hasProducts($languageID,$userID)) {
                        $message = $this->lang->line('cart_added');
                        $status          =   parent::HTTP_OK;
                        $data['totalProducts'] = $this->cartsapi->countProducts($languageID,$userID);
                        $data['subtoal'] = $this->currency->format($this->cartsapi->getSubTotal($languageID,$userID), $this->siteCurrency);
                        $data['shipping'] = $this->currency->format(0, $this->siteCurrency);
                        $data['totalAmout'] = $this->currency->format($this->cartsapi->getTotal($languageID,$userID), $this->siteCurrency);
                  
                        if (!empty($data)) {
                            $allTotals = $this->cartsapi->getTotalItemsList($languageID,$userID);

                            foreach ($allTotals as $key => $tolVal) {
                                $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency);
                                $allTotals[$key]['languageKey'] = lang($allTotals[$key]['code']);
                            }
                            $data['subTotalData'] = $allTotals;
                        }
                    } else {
                        $message = $this->lang->line('product_add_error');
                    }
                }
            } else {
                $message = $this->lang->line('product_add_error');
            }
            
            if(!empty($data)){

                $status      =   parent::HTTP_OK;
                $message     =   lang('cart_added_api'); 
                $data        =   $data; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   $message; 
                $data        =   '';
            }
        }
        else{
            $i  =   1;
            $msgvalue    =   '';
                foreach($this->form_validation->error_array() as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
               $status      =   parent::HTTP_BAD_REQUEST;
               $message     =   $msgvalue; 
               $data        =   '';
               
            }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);

    }
    
    public function updateCart_post() {
         $config = [
                        [
                                'field' => 'languageID',
                                'label' => 'languageID',
                                'rules' => 'required|numeric',
                                'errors' => [
                                        'required'      => 'languageID missing',
                                        'numeric'       => 'Only numbers allowed',
                                ],
                        ],
                        [
                            'field' => 'productID',
                            'label' => 'productID',
                            'rules' => 'required|numeric',
                            'errors' => [
                                'required'      => 'productID missing',
                                'numeric'       => 'Only numbers allowed',

                            ],
                        ],
                        [
                            'field' => 'quantity',
                            'label' => 'quantity',
                            'rules' => 'required|numeric',
                            'errors' => [
                                'required'      => 'quantity missing',
                                'numeric'       => 'Only numbers allowed',

                            ],
                        ]
                    ];
        $messageData = array();
        $_POST['languageID']     =   $this->security->xss_clean($this->input->post('languageID')) ? (int) $_POST['languageID'] : $this->defalutLanguage;
        $dataPost = $this->input->post();
        $this->form_validation->set_data($dataPost);
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() === TRUE) 
        {
            $languageID =   $this->input->post('languageID');
            $productID = ($this->input->post('productID')) ? $this->input->post('productID') : 0;
            $quantity = ($this->input->post('quantity')) ? $this->input->post('quantity') : 1;
            $option         =   $this->security->xss_clean($this->input->post('option')) ? $_POST['option'] : '';
            $userID         =   $this->userID;
            $msgvalue    =   '';
            if ($productID > 0) {
                $customerID = $this->userID;
                $apiID = '';
                $sessionID = '';
                $params = array(
                    'customerID' => $customerID,
                    'apiID' => $apiID,
                    'sessionID' => $sessionID,
                    'productID' => $productID
                );
                if ($productID) {
                    $productInfo = $this->generalModel->getTableValue('quantity,minimum,status,dateAvailable', 'ec_product', array("productID" => $productID, 'status' => 'Active'));
                    if($productInfo){
                        if ($productInfo['status'] != 'Active') {
                            $messageData['status'] = $this->lang->line('product_status_error');
                        }
                        if ($productInfo['dateAvailable'] != '0000-00-00' && $productInfo['dateAvailable'] > date('Y-m-d')) {
                            $messageData['dateAvailable'] = sprintf($this->lang->line('product_date_available_error'), date('jS, F Y', strtotime($productInfo['dateAvailable'])));
                        }
                        if ($quantity < $productInfo['minimum']) {
                            $quantity = $productInfo['minimum'];
                        }
                        if ($productInfo['quantity'] < $quantity) {
                            $messageData['quantity'] = $this->lang->line('quantity_not_available');
                        }
                        
                         if($option){
                            $optionval = explode(',', $option);  
                            $productOptionID=0;
                            $optionArray = array();
                            foreach($optionval as $option){
                                $optionvals = explode(':', $option);$i=0;
                                $optionArray[] = json_encode($option);
                                foreach ($optionvals as $value) {
                                    $vals = explode('-', $value);
                                    if($vals[0] == 'productOptionID'){
                                        $productOptionID = $vals[1];

                                    }
                                }

                            }

                            $productOptionInfo = $this->generalModel->getTableValue('quantity', 'ec_product_option', array("productID" => $productID, 'status' => 'Active',"productOptionID" => $productOptionID));
                            //echo $this->db->last_query();exit;
                            //echo "<pre>"; print_r($productOptionInfo); exit;
                            //echo $quantity; exit;
                            if ($productOptionInfo['quantity'] < $quantity) {
                                $messageData['quantity'] = $this->lang->line('quantity_not_available');
                            }
                        }

                        if (!$messageData) {
                            
                            $totalProducts = $this->cartsapi->countProducts($languageID,$customerID);
                            $this->cartsapi->update($productID, $quantity,$customerID);
                            $newProductCount = $this->cartsapi->countProducts($languageID,$productID);
                            //echo $totalProducts;exit;
                            //if ($newProductCount != $totalProducts) {
                                $messageData['success'] = $this->lang->line('cart_updated');
                               
                                $data['totalProducts'] = $this->cartsapi->countProducts($languageID,$customerID);
                                $data['subtoal'] = $this->currency->format($this->cartsapi->getSubTotal($languageID,$customerID), $this->siteCurrency);
                                $data['shipping'] = $this->currency->format(0, $this->siteCurrency);
                                $data['totalAmout'] = $this->currency->format($this->cartsapi->getTotal($languageID,$customerID), $this->siteCurrency);
                                if (!empty($data)) {
                                $allTotals = $this->cartsapi->getTotalItemsList($languageID,$userID);

                                foreach ($allTotals as $key => $tolVal) {
                                    $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency);
                                    $allTotals[$key]['languageKey'] = lang($allTotals[$key]['code']);
                                }
                                $data['subTotalData'] = $allTotals;
                            }
                                $status      =   parent::HTTP_OK;
                        } else{
                       
                            $status      =   parent::HTTP_BAD_REQUEST;
                            $msgvalue     =   $messageData['quantity']; 
                            $data        =   '';
                        }
                    }else{
                       
                        $status      =   parent::HTTP_BAD_REQUEST;
                        $msgvalue     =   lang('Sorry_No_Data_Found'); 
                        $data        =   '';
                    }
                     
                $i  =   1;
//                foreach($messageData as $key=>$msgData){
//                   
//                    $msgvalue   .= $msgData;
//                    if($i!=count($messageData)){
//                        $msgvalue   .= ",";
//                    }
//                    $i++;
//                } 
                $message    =   $msgvalue;
                }
            }
        }
        else{
            $i  =   1;
            $msgvalue    =   '';
                foreach($this->form_validation->error_array() as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $message     =   $msgvalue; 
               $data        =   '';   
        }
        
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
       
    }
    
    
     public function removeFromCart_post() {
         
         $config = [
                        [
                                'field' => 'cartID',
                                'label' => 'cartID',
                                'rules' => 'required|numeric',
                                'errors' => [
                                        'required'      => 'cartID missing',
                                        'numeric'       => 'Only numbers allowed',
                                ],
                        ],
                        [
                            'field' => 'productID',
                            'label' => 'productID',
                            'rules' => 'required|numeric',
                            'errors' => [
                                'required'      => 'productID missing',
                                'numeric'       => 'Only numbers allowed',

                            ],
                        ]
                    ];
        $messageData = array();
        $data = $this->input->post();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() === TRUE) 
        {
            $languageID     =   $this->security->xss_clean($this->input->post('languageID')) ? (int) $_POST['languageID'] : $this->defalutLanguage;
            $productID = $this->security->xss_clean($this->input->post('productID')) ? $this->input->post('productID') : 0;
            $totalProducts = $this->cartsapi->countProducts($languageID,$productID);
            $cartID = ($this->input->post('cartID')) ? $this->input->post('cartID') : 0;
            $userID             = $this->userID;
            if ($cartID > 0) {
                
                $this->cartsapi->remove($cartID,$userID,$productID);
                $newProductCount = $this->cartsapi->countProducts($languageID,$productID);
                //if ($totalProducts > $newProductCount) {
                    $message = $this->lang->line('cart_removed');
                    $data['totalProducts'] = $this->cartsapi->countProducts($languageID,$productID);
                    $data['totalAmout'] = $this->currency->format($this->cartsapi->getTotal($languageID,$userID), $this->siteCurrency);
                    if (!empty($data)) {
                            $allTotals = $this->cartsapi->getTotalItemsList($languageID,$userID);

                            foreach ($allTotals as $key => $tolVal) {
                                $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency);
                                $allTotals[$key]['languageKey'] = lang($allTotals[$key]['code']);
                            }
                            $data['subTotal'] = $allTotals;
                        }
                    $status      =   parent::HTTP_OK;
//                } else {
//                    $message = $this->lang->line('product_add_error');
//                    $status      =   parent::HTTP_BAD_REQUEST;
//                }
                
            } else {
                $message = $this->lang->line('product_remove_error');
                $status      =   parent::HTTP_BAD_REQUEST;
            }
        }
         else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($this->form_validation->error_array() as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $message     =   $msgvalue; 
               $data        =   '';
               
            }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
        public function checkOut_get() {
            $valErr =   0;
            $valErrMsg  =   array();
            $data        =    array();
            $nextDeleveryDateTime   =   "";
            $nextDate               =   "";
            $expectedDelevery       =   "";
            $expectedDeleveryDateTime   =   "";
            $languageID                     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
            $moonehDeliveryAddressID        =   $this->security->xss_clean($this->input->get('deliveryAddressID')) ? (int) $_GET['deliveryAddressID'] : 0;
            $deleveryType                   =   $this->security->xss_clean($this->input->get('deleveryType')) ?  $_GET['deleveryType'] : 'normal';
            $couponCode                     =   $this->security->xss_clean($this->input->get('couponCode')) ?  $_GET['couponCode'] : '';
            
            if($languageID){
                if (!is_numeric($languageID)){
                    $valErr =   1;
                    $valErrMsg['languageID']    =  'Only numbers allowed';   
                }
            } else {
                $valErr =   1;
                $valErrMsg['languageID']    =  'languageID missing';   
            }
        
            if($moonehDeliveryAddressID){
                if (!is_numeric($moonehDeliveryAddressID)){
                    $valErr =   1;
                    $valErrMsg['moonehDeliveryAddressID']    =  'Only numbers allowed';   
                }
            } else {
                $valErr =   1;
                $valErrMsg['moonehDeliveryAddressID']    =  'Address ID missing';   
            }
        
            if($valErr==0){ 
                $userID                         =   $this->security->xss_clean($this->userID);

                $cartItems = $this->cartsapi->getProducts($languageID,$userID);

                list($width, $height) = $this->getImageDimension('cart_image_dimen');
                foreach ($cartItems as $key => $cart) {
                    $cartItems[$key]['price'] = $this->currency->format($cart['price'], $this->siteCurrency);
                    $cartItems[$key]['image'] = $this->setSiteImage('product/' . $cart['image'], $width, $height);
                    $cartItems[$key]['total'] = $this->currency->format($cart['total'], $this->siteCurrency);
                    
                    if($cart['option']){
                        $optionDataVals =   array();
                        foreach($cart['option'] as $cartOptionKey=>$option){
                            $i  =   0;

                            $optionvals = explode(':', $option);
                            foreach($optionvals as $optionvalData){
                                $optionData = explode('-', $optionvalData);
                                $optionDataVals[$optionData[0]] =   $optionData[1];
                               $i++;
                            }
                            $productOptions   =   $this->optionModel->selectOptionDetail($optionDataVals['productOptionID'],$languageID,$optionDataVals['optionValueId']);

                            foreach($productOptions as $productOptionsData){
                               $cartItems[$key]['optionValue'][]   =   $productOptionsData['oName']."-".$productOptionsData['name'];
                            }

                        }
                    }
                }
                $data['cartItems'] = $cartItems;
        //        if (!empty($cartItems)) {
        //            $this->data['cartItems']['subTotal'] = $this->currency->format($this->carts->getSubTotal(), $this->siteCurrency);
        //        }
                $moonehDeliveryAddress = $this->generalModel->getTableValue('*', 'ec_address', 'addressID="' . $moonehDeliveryAddressID . '"', FALSE);  
               // echo "<pre>"; print_r($moonehDeliveryAddress); exit;
                $deliveryLocation = $this->generalModel->getTableValue('*', 'ec_warehouse_delivery_location', 'cityID="' . $moonehDeliveryAddress['cityID'] . '"', FALSE);
               //echo "<pre>"; print_r($deliveryLocation); exit;
                if (!empty($cartItems)) {
                    if($deleveryType=='normal'){
                        $expectedHour   =   $deliveryLocation['normalDeliveryTime'];
                                   
                    }elseif($deleveryType=='express'){
                        $expectedHour   =   $deliveryLocation['expressDeliveryTime'];
                    }
                    
                   // echo $expectedHour; exit;
//                        date_default_timezone_set('Asia/Kolkata'); 
                        //set an date and time to work with
                        $start =  date('m/d/Y h:i:s a', time());
                        //display the converted time
                        $expectedDeleveryDateTime   =    date('Y-m-d H:i',strtotime('+'.$expectedHour.' hour +0 minutes',strtotime($start)));  
                        $expectDate =   $expectedDeleveryDateTime;
                        $week   =   date('l', strtotime($expectedDeleveryDateTime));
                        //echo $week;
                        $expectedDelevery   =   lang("Expected_delivery_on")." ".date('l d-m-Y', strtotime($expectedDeleveryDateTime));
                        $expectedTime   =   date('H:i', strtotime($expectedDeleveryDateTime));
                       
                    $timeSlotedata  = $this->generalModel->getTableValue('*', 'ec_delevery_time_slot', 'fromTime >=TIME("' . $expectedTime . '") AND status="Active"', TRUE);
                   
                    if(empty($timeSlotedata)){
                        $start =  date('m/d/Y', time() + 86400);
                     
                        //display the converted time
                        $expectedDeleveryDateTime   =    date('Y-m-d H:i',strtotime('+'.$expectedHour.' hour +0 minutes',strtotime($start)));  
                        //echo $expectedDeleveryDateTime; exit;
                        $week   =   date('l', strtotime($expectedDeleveryDateTime));
                        //echo $week;
                        $expectedDelevery   =   lang("Expected_delivery_on")." ".date('l d-m-Y', strtotime($expectedDeleveryDateTime));
                        $expectedTime   =   date('H:i', strtotime($expectedDeleveryDateTime));
                        
                        $timeSlotedata  = $this->generalModel->getTableValue('*', 'ec_delevery_time_slot', 'fromTime >=TIME("' . $expectedTime . '")  AND status="Active"', TRUE);
                        
                        
                    }
                    //echo "<pre>"; print_r($timeSlotedata); exit;
                    if($timeSlotedata){
                       
                        $currentSlot    =   "";
                        $i  =   1;
                            foreach($timeSlotedata as $key=>$currentSlotData){
                                $timeSlotedata[$key]['fromTime']    =  date('h:i a', strtotime($currentSlotData['fromTime'])); 
                                $timeSlotedata[$key]['toTime']    =  date('h:i a', strtotime($currentSlotData['toTime'])); 
                                $currentSlot    .=   $currentSlotData['slotID'];
                                if(count($timeSlotedata)!=$i){
                                    $currentSlot    .= ",";
                                }
                                $i++;
                            }
                            if($currentSlot){
                                //echo strtotime($start);exit;
                                $nextSloteTime  = $this->generalModel->getTableValue('*', 'ec_delevery_time_slot', 'slotID NOT IN ('.$currentSlot.') AND status="Active"', TRUE);
                                if($nextSloteTime){
                                    foreach($nextSloteTime as $key=>$nextSlotData){
                                        $nextSloteTime[$key]['fromTime']    =  date('h:i a', strtotime($nextSlotData['fromTime'])); 
                                        $nextSloteTime[$key]['toTime']    =  date('h:i a', strtotime($nextSlotData['toTime'])); 
                                    }
                                    //echo $this->db->last_query();exit;
                                    $nextDeleveryDateTime   =   date('Y-m-d', strtotime($expectedDeleveryDateTime . ' +1 day'));
                                    $nextDate   =   $nextDeleveryDateTime; 
                                    $nextDeleveryDateTime   =  date('l d-m-Y', strtotime($nextDeleveryDateTime));
                                }
                               
                                  
                            }
                           
                    }
                    
                        $couponData =   array();
                        if ($this->input->get('couponCode')) {
                            
                                $couponCode =   $this->input->get('couponCode');
                                $coupon =   $this->coupon->selectCoupon($couponCode);
                               
                                $allTotals = $this->cartsapi->getTotalItemsList($languageID,$userID,'');
                                //echo "<pre>"; print_r($allTotals); exit;
                                if($allTotals['0']['code']=='sub_total'){
                                    $sVAl = explode(' ',$allTotals[0]['value']); 
                                }
                            if(!empty($coupon) && $sVAl[0] >= $coupon['minimum_amount_purchase']){
                                
                                if(@coupon['applayForUser']){
                                    $applyUser = explode(',',$coupon['applayForUser']);
                                }else{
                                    $applyUser = array("Not found");
                                }

                                $applyUsers = array();
                                foreach($applyUser as $key => $apply){
                                    $apply = trim($apply);
                                    if (!empty($apply))
                                        $applyUsers[] = $apply;
                                }

                                if(!empty($coupon)){
                                   
                                    if($coupon['applayFor']=='cart'){
                                        $couponData        = array('couponData'=>array(
                                            'couponID'          => $coupon['couponID'],
                                            'code'              => $coupon['code'],
                                            'type'              => $coupon['type'],
                                            'applayFor'         => $coupon['applayFor'],
                                            'discount'          => $coupon['discount'],
                                            'status'            => $coupon['status']
                                        ));
                                    } elseif($coupon['applayFor']=='delivery'){
                                        $couponData        = array('couponData'=>array(
                                            'couponID'          => $coupon['couponID'],
                                            'code'              => $coupon['code'],
                                            'type'              => $coupon['type'],
                                            'applayFor'         => $coupon['applayFor'],
                                            'discount'          => $coupon['discount'],
                                            'status'            => $coupon['status']
                                        ));
                                    }elseif($coupon['applayFor']=='user'){ 
                                        
                                        if(in_array($userEmail,$applyUsers)){
                                            $couponData        = array('couponData'=>array(
                                                'couponID'          => $coupon['couponID'],
                                                'code'              => $coupon['code'],
                                                'type'              => $coupon['type'],
                                                'applayFor'         => $coupon['applayFor'],
                                                'discount'          => $coupon['discount'],
                                                'status'            => $coupon['status']
                                            ));
                                        }
                                    }elseif($coupon['applayFor']=='firstPurchase'){
                                         $checkOrder = $this->coupon->checkOrderExist($userID);
                                          if(empty($checkOrder)){
                                             $couponData        = array('couponData'=>array(
                                                'couponID'          => $coupon['couponID'],
                                                'code'              => $coupon['code'],
                                                'type'              => $coupon['type'],
                                                'applayFor'         => $coupon['applayFor'],
                                                'discount'          => $coupon['discount'],
                                                'status'            => $coupon['status']
                                            ));
                                          }
                                    }
                                }
                                
                            }
                            
                        }
                  
                    $allTotals = $this->cartsapi->getTotalItemsList($languageID,$userID,$couponData,$moonehDeliveryAddress['cityID'],$deleveryType);
                     
                    foreach ($allTotals as $key => $tolVal) {
                        $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency);
                        $allTotals[$key]['languageKey'] = lang($allTotals[$key]['code']);
                    }
                }
                $data['expectedDelevery'] = $expectedDelevery;
                $data['expectedDeleveryDateTime'] = $expectedDeleveryDateTime;
                $data['timeSloteData'] = $timeSlotedata;
                $data['nextDeleveryDateTime'] = $nextDeleveryDateTime;
                $data['nextDate'] = $nextDate;
                $data['nextSloteTime'] = $nextSloteTime;
                $data['subTotal'] = $allTotals;
                $data['content'] = $this->accountModel->selectAddress($userID);


                if ($moonehDeliveryAddressID) {
                    $data['moonehDeliveryAddress'] = $this->generalModel->getTableValue('*', 'ec_address', 'addressID="' . $moonehDeliveryAddressID . '"', FALSE);
                }
                //$data['stateList'] = $this->accountModel->selectStateList($languageID, $userID);
                //$data['addressErr'] = $this->session->flashdata('addressErr');
                //$data['countryList'] = $this->generalModel->getTableValue('countryID,name', 'ec_country', 'countryID="173" AND status="Active"', TRUE);
                //$data['warehouseCity'] = $this->wm->getWarehouseCityInfo();

                if(!empty($data)){

                    $status      =   parent::HTTP_OK;
                    $message     =   lang('Success');
                    $data        =   $data; 

                }else{
                    $status      =   parent::HTTP_BAD_REQUEST;
                    $message     =   $message; 
                    $data        =   '';
                }
            }
            else{
               $status      =   parent::HTTP_BAD_REQUEST;
               $message     =   $valErrMsg; 
               $data        =   '';
               
            }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
            
    }
    
    public function paymentMethod_get() {
        $payment[0]['id']   =   1;
        $payment[0]['name']   =   'Free Checkout';
        $payment[1]['id']   =   2;
        $payment[1]['name']   =   'Cash On Delivery';
        $data   =   $payment;
        if(!empty($data)){

                    $status      =   parent::HTTP_OK;
                    $message     =   lang('Success'); 
                    $data        =   $data; 

        }else{
            $status      =   parent::HTTP_BAD_REQUEST;
            $message     =   $message; 
            $data        =   '';
        }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function payment_get() {
        $valErr =   0;
        $valErrMsg  =   array();
        $languageID                     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        $moonehDeliveryAddressID        =   $this->security->xss_clean($this->input->get('deliveryAddressID')) ? (int) $_GET['deliveryAddressID'] : 0;
        $deleveryType                   =   $this->security->xss_clean($this->input->get('deleveryType')) ?  $_GET['deleveryType'] : 'normal';
        $paymentMethod = 3;
        
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        if($moonehDeliveryAddressID){
            if (!is_numeric($moonehDeliveryAddressID)){
                $valErr =   1;
                $valErrMsg['moonehDeliveryAddressID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['moonehDeliveryAddressID']    =  'Need to select address';   
        }
            
        if($paymentMethod){
            if (!is_numeric($paymentMethod)){
                $valErr =   1;
                $valErrMsg['paymentMethod']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['paymentMethod']    =  'Payment Method Missing';   
        }
        if($valErr==0){
            $couponData =   array();
            $formDet    =   array();
            if ($this->input->get('couponCode')) {
                $couponCode =   $this->input->get('couponCode');
                $coupon =   $this->coupon->selectCoupon($couponCode);
                if(!empty($coupon)){
                    $couponData        = array('couponData'=>array(
                        'couponID'          => $coupon['couponID'],
                        'code'              => $coupon['code'],
                        'type'              => $coupon['type'],
                        'discount'          => $coupon['discount'],
                        'status'            => $coupon['status']
                    ));
                }
            }
            
            $userID                         =   $this->security->xss_clean($this->userID);
            $customer = $this->generalModel->getTableValue('email', 'ec_customer', 'customerID="' . $userID . '"', FALSE);
            
            $confirm = $this->orderapi->setOrderData($languageID, $this->siteCurrency, $paymentMethod,$userID,$moonehDeliveryAddressID,$couponData,$deleveryType);

                if ($moonehDeliveryAddressID) {
                    $delAddress = $this->generalModel->getTableValue('*', 'ec_address', 'addressID="' . $moonehDeliveryAddressID . '"', FALSE);
                }
                
            if($confirm){
                $transactionUUID = $this->get_unique_key(25, 'ec_order_transaction', 'transaction_uuid');
                $referenceNumber = $this->get_unique_key(25, 'ec_order_transaction', 'reference_number');
                $amount = $this->generalModel->getFieldValue('value', 'ec_order_total', array('orderID' => $confirm, 'code' => 'total'));

                $amount =   number_format(floor($amount*100)/100,2, '.', '');
                $transactionArray = array(
                    'orderID' => $confirm,
                    'transaction_uuid' => $transactionUUID,
                    'reference_number' => $referenceNumber,
                    'amount' => $amount,
                    'currency' => "QAR",
                    'paymentStatus' => "started"
                );
                
                $formDet['orderID'] =   $confirm;
                $array = array(
                    'access_key' => "199d034c2dfc3846ae819928166a81a9",
                    'profile_id' => "9BA016D8-646E-4518-8803-07D0BC6462FF",
                    'transaction_uuid' => $transactionUUID,
                    'signed_field_names' => "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,ccAuthService_run,ccCaptureService_run,bill_to_address_city,bill_to_address_country,bill_to_address_line1,bill_to_address_postal_code,bill_to_forename,bill_to_phone,bill_to_surname,bill_to_email,merchant_defined_data1,customer_ip_address",
                    'unsigned_field_names' => "",
                    'signed_date_time' => gmdate("Y-m-d\TH:i:s\Z"),
                    'locale' => "en",
                    'transaction_type' => "sale",
                    'reference_number' => $referenceNumber,
                    'amount' => $amount,
                    'currency' => "QAR",
                    //'device_fingerprint_id' => $orderData['invoicePrefix'] . "-" . $orderData['invoiceNo'],
                    'ccAuthService_run' => 'true',
                    'ccCaptureService_run' => 'true',
                    'bill_to_address_city' => $delAddress['city'],
                    'bill_to_address_country' => 'QA',
                    'bill_to_address_line1' => $delAddress['address1'],
                    'bill_to_address_postal_code' => $delAddress['postcode'],
                    'bill_to_forename' => $delAddress['firstname'],
                    'bill_to_phone' => $delAddress['phone'],
                    'bill_to_email' => $customer['email'],
                    'bill_to_surname' => $delAddress['lastname'],
                    'merchant_defined_data1' => 'MA',
                    'customer_ip_address' => $this->getIPAddress()
                );
                $array['signature'] = $this->sign($array);
                $formDet['payForm'] = $array;
                $formDet['payFormAction'] = "https://secureacceptance.cybersource.com/pay";
                
                $this->generalModel->insertValue('ec_order_transaction', $transactionArray);

                $status      =   parent::HTTP_OK;
                $message     =   lang('Success');
                $data        =   $formDet; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   lang('Failed');
                $data        =   '';
            }
        }
        else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
           $status      =   parent::HTTP_BAD_REQUEST;
           $message     =   $msgvalue; 
           $data        =   '';

        }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    function getIPAddress() {  
        //whether ip is from the share internet  
        if(!empty($_SERVER['HTTP_CLIENT_IP'])) {  
            $ip = $_SERVER['HTTP_CLIENT_IP'];  
        }  
        //whether ip is from the proxy  
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];  
        }  
        //whether ip is from the remote address  
        else{  
            $ip = $_SERVER['REMOTE_ADDR'];  
        }  
        return $ip;  
    }  
    
    public function cod_get() {
        $valErr =   0;
        $valErrMsg  =   array();
        $languageID                     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        $moonehDeliveryAddressID        =   $this->security->xss_clean($this->input->get('deliveryAddressID')) ? (int) $_GET['deliveryAddressID'] : 0;
        $deleveryType                   =   $this->security->xss_clean($this->input->get('deleveryType')) ?  $_GET['deleveryType'] : 'normal';
        $paymentMethod = 2;
        $email          =   $this->getSettingValue('email');
        
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        if($moonehDeliveryAddressID){
            if (!is_numeric($moonehDeliveryAddressID)){
                $valErr =   1;
                $valErrMsg['moonehDeliveryAddressID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['moonehDeliveryAddressID']    =  'Need to select address';   
        }
            
        if($paymentMethod){
            if (!is_numeric($paymentMethod)){
                $valErr =   1;
                $valErrMsg['paymentMethod']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['paymentMethod']    =  'Payment Method Missing';   
        }
        if($valErr==0){
            $couponData =   array();
            $formDet    =   array();
            if ($this->input->get('couponCode')) {
                $couponCode =   $this->input->get('couponCode');
                $coupon =   $this->coupon->selectCoupon($couponCode);
                if(!empty($coupon)){
                    $couponData        = array('couponData'=>array(
                        'couponID'          => $coupon['couponID'],
                        'code'              => $coupon['code'],
                        'type'              => $coupon['type'],
                        'discount'          => $coupon['discount'],
                        'status'            => $coupon['status']
                    ));
                }
            }
            
            $userID                         =   $this->security->xss_clean($this->userID);
            $customer = $this->generalModel->getTableValue('email', 'ec_customer', 'customerID="' . $userID . '"', FALSE);
            
            $confirm = $this->orderapi->setOrderData($languageID, $this->siteCurrency, $paymentMethod,$userID,$moonehDeliveryAddressID,$couponData,$deleveryType);

                if ($moonehDeliveryAddressID) {
                    $delAddress = $this->generalModel->getTableValue('*', 'ec_address', 'addressID="' . $moonehDeliveryAddressID . '"', FALSE);
                }
                
            if($confirm){
                $transactionUUID = $this->get_unique_key(25, 'ec_order_transaction', 'transaction_uuid');
                $referenceNumber = $this->get_unique_key(25, 'ec_order_transaction', 'reference_number');
                $amount = $this->generalModel->getFieldValue('value', 'ec_order_total', array('orderID' => $confirm, 'code' => 'total'));

                $amount =   number_format(floor($amount*100)/100,2, '.', '');
                $transactionArray = array(
                    'orderID' => $confirm,
                    'transaction_uuid' => $transactionUUID,
                    'reference_number' => $referenceNumber,
                    'amount' => $amount,
                    'currency' => "QAR",
                    'paymentStatus' => "cod"
                );
                
                $formDet['orderID'] =   $confirm;
                $this->generalModel->insertValue('ec_order_transaction', $transactionArray);
                
                $orderID    =   $confirm;
                if (@$orderID != "") {
                            $orderProduct = $this->generalModel->getTableValue('productID,quantity', 'ec_order_product', array("orderID" => $orderID), TRUE);
//                            echo "<pre>"; print_r($orderProduct); exit;
                            
                            if ($orderProduct) {
                                foreach ($orderProduct as $key => $proVal) {
                                    $this->cartModel->decreaseProductQuentity($proVal['productID'], $proVal['quantity']);
                                    $product    =   $this->generalModel->getTableValue('productID,type', 'ec_product', array("productID" => $proVal['productID']), FALSE);
                                    if($product['type']=='Bundle'){
                                        $bundleProduct    =   $this->generalModel->getTableValue('productID,bundleProductID,quantity', 'ec_product_bundle', array("productID" => $proVal['productID']), TRUE);
                                        if($bundleProduct){
                                            foreach($bundleProduct as $bundleProductData){
                                                $this->cartModel->decreaseProductQuentity($bundleProductData['bundleProductID'], $bundleProductData['quantity']);
                                                
                                            }
                                        }
                                    }
                                    $orderOptionProduct = $this->cartModel->getProductOption($orderID);
                                    //print_r($orderOptionProduct);
                                    if($orderOptionProduct){ 
                                        foreach($orderOptionProduct as $option){
                                            $this->cartModel->decreaseProductOptionQuentity($proVal['productID'], $proVal['quantity'],$option['pID']);
                                        }

                                    }
                                }
                            }
                           
                        }
                //Send SMS Start
                        $total =    $this->currency->format($this->generalModel->getFieldValue('value', 'ec_order_total', 'orderID=' . $orderID . ' AND code="total"'), $this->siteCurrency);
                        $customerData   = $this->generalModel->getTableValue('firstname,lastname,email,countryCode,telephone', 'ec_customer', array("customerID" => $userID, 'status' => 'Active'));
                        $orderDataSMS      = $this->generalModel->getTableValue('orderID,invoiceNo', 'ec_order', array("orderID" => $orderID));
                        //echo "<pre>"; print_r($orderData); exit;
                        //$phoneNo    =  "974".$customerData['telephone']; 
                        $phoneNo    =  $customerData['countryCode'].$customerData['telephone']; 
                        //$smsMessage =   "Your order successfully completed.Your order id : ".$orderDataSMS['orderID']." ,invoice no : ".$orderDataSMS['orderID'].", amount : ".$total;
                        
                        $smsMessage =   "Your order has been placed on Mowneh. OrderID : ".$orderDataSMS['orderID'].", Order amount: ".$total.". Ordered on ".date('d-m-Y').". Thank You!";
                        $this->sendSMS($smsMessage,$phoneNo);
                        
                        $smsMessage =   "New order has been placed on Mowneh. OrderID : ".$orderDataSMS['orderID'].", Order amount: ".$total.". Ordered on ".date('d-m-Y').". Thank You!";
                        $this->sendSMS($smsMessage,'97466714422'); // SMS TO ADMIN
                        //Send SMS ENd
                        
                        $customerID     =   $userID;

//Send SMS Start
                        
                        //echo "<pre>"; print_r($orderData); exit;
                       
                        //Send SMS ENd
                        $from_email = $email;
                        //$message = "Your order has been successfully placed. Your total amout:" . $total;
                        $message    =   $this->orderConfirmMessage($customerID,$orderID,$languageID);                   
                        $customerEmil = $this->generalModel->getTableValue('email', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
                        $config['protocol'] = 'sendmail';
                        $config['mailtype'] = 'html';
                        $to_email = $customerEmil['email'];
                        //Load email library 
                        $this->load->library('email');
                        $this->email->initialize($config);
                        $this->email->from($from_email, 'Mooneh');
                        $this->email->to($to_email);
                        $this->email->subject('Order Confirmation');
                        $this->email->message($message);
                        if($this->email->send()){
                             $from_email = $email;
                             $to_email = $from_email;
                            $this->load->library('email');
                            $this->email->initialize($config);
                            $this->email->from($from_email, 'Mooneh');
                            $this->email->to($to_email);
                            $this->email->subject('Order Confirmation');
                            $message    =   $this->orderConfirmMessageAdmin($customerID,$orderID,$languageID);
                            $this->email->message($message);
                            $this->email->send();
                        }

                $status      =   parent::HTTP_OK;
                $message     =   lang('Success');
                $data        =   $formDet; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   lang('Failed');
                $data        =   '';
            }
        }
        else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
           $status      =   parent::HTTP_BAD_REQUEST;
           $message     =   $msgvalue; 
           $data        =   '';

        }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function cartCount_get() {
        $userID                         =   $this->security->xss_clean($this->userID);
        $languageID                     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        $cartItemCount = $this->cartsapi->countProducts($languageID,$userID);

        $data['cartCount']  =  $cartItemCount; 
        $this->setHeaderToken(); 
        
        $status                 = parent::HTTP_OK;
        $result['status']	= $status;
        $result['message']      = '';
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    function randString($length) {
        $char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $char = str_shuffle($char);
        for ($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
            $rand .= $char{mt_rand(0, $l)};
        }
        return $rand;
    }

    function get_unique_key($length = 16, $table, $field = "transaction_uuid") {
        $i = 0;
        do {
            $unique_id = $this->randString($length);
            $this->db->from($table);
            $this->db->where($field, $unique_id);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $i = 0;
            } else {
                $i = 1;
            }
        } while ($i = 0);
        return $unique_id;
    }
    
    public function success_get() {
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        $orderID        =   $this->security->xss_clean($this->input->get('orderID')) ? (int) $_GET['orderID'] : 0;
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        if($orderID){
            if (!is_numeric($orderID)){
                $valErr =   1;
                $valErrMsg['orderID']    =  'Only numbers allowed';   
            }
        }else {
            $valErr =   1;
            $valErrMsg['orderID']    =  'orderID missing';   
        }
        
        if($valErr==0) {
            
            $orderID        =   $this->security->xss_clean($this->input->get('orderID'));
            $userID         =   $this->userID;
            
            
            $orderID        =   $this->security->xss_clean($this->input->get('orderID'));
            $userID         =   $this->userID;
            $customerID     =   $userID;
            
                $total =    $this->currency->format($this->generalModel->getFieldValue('value', 'ec_order_total', 'orderID=' . $orderID . ' AND code="total"'), $this->siteCurrency);
                $this->cartsapi->clear($userID);
//                $resultData['orderID']    =   $orderID;
                $resultData    =   $this->orderModel->orderDetailSuccess($orderID);
                $resultData['amount']   =   strval(floatval($resultData['amount']));

                if($resultData){
                    $status      =   parent::HTTP_OK;
                    $message     =   lang('Success'); 
                    $data        =   $resultData; 
                }else{
                    $status      =   parent::HTTP_BAD_REQUEST;
                    $message     =   lang('Bad_request');
                    $data        =   '';
                    $token      =   ''; 
                }
          
        }else{
               $status      =   parent::HTTP_BAD_REQUEST;
               $message     =   $valErrMsg; 
               $data        =   '';
               $token      =   ''; 
        }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
       function sign($params) {
        $secretKey = "e9e6ee6272934d5da33c865dbdac36a05bf7acbebc7948bcb9c51d1c4654b8271c1412afc28d480ebb893a19e7541244f243f10d488c437581b22d3fb14c87934a0379f9090b440caf7d21143622658f8d111a5120a34501b136766176c84538e259f8da72654e9b971434e583a7cf12f161b4c50d3a448cb4c47aea4120bea5";
        return $this->signData($this->buildDataToSign($params), $secretKey);
    }

    function signData($data, $secretKey) {
        return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
    }

    function buildDataToSign($params) {
        $signedFieldNames = explode(",", $params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
            $dataToSign[] = $field . "=" . $params[$field];
        }
        return $this->commaSeparate($dataToSign);
    }

    function commaSeparate($dataToSign) {
        return implode(",", $dataToSign);
    }
    
    public function applyCoupon_get() {
        
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   array();
        $couponCode                     =   $this->security->xss_clean($this->input->get('couponCode'));
        $languageID                     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        $moonehDeliveryAddressID        =   $this->security->xss_clean($this->input->get('deliveryAddressID')) ? (int) $_GET['deliveryAddressID'] : 0;
        $moonehDeliveryType             =   $this->security->xss_clean($this->input->get('deliveryType')) ? $_GET['deliveryType'] : 'normal';
        
        $addressInfo = $this->generalModel->getTableValue('*', 'ec_address', 'addressID="' . $moonehDeliveryAddressID . '"', FALSE);
        if($addressInfo){
            $addressCityID = $addressInfo['cityID'];
        }else{
            $addressCityID = 0;
        }
        $languageName = $this->generalModel->getFieldValue('name', 'ec_language', array('languageID' => $languageID));
        $this->lang->load('site', $languageName);
       
            if($languageID){
                if (!is_numeric($languageID)){
                    $valErr =   1;
                    $valErrMsg['languageID']    =  'Only numbers allowed';   
                }
            } else {
                $valErr =   1;
                $valErrMsg['languageID']    =  'languageID missing';   
            }
        
            if($moonehDeliveryAddressID){
                if (!is_numeric($moonehDeliveryAddressID)){
                    $valErr =   1;
                    $valErrMsg['moonehDeliveryAddressID']    =  'Only numbers allowed';   
                }
            } else {
                $valErr =   1;
                $valErrMsg['moonehDeliveryAddressID']    =  'Address ID missing';   
            }
        
        if(!$couponCode){
            $valErr =   1;
            $valErrMsg['couponCode']    =  'couponCode missing';   
        }
        
        if($valErr==0) {
           
                $coupon =   $this->coupon->selectCoupon($couponCode);
                if(!empty($coupon)){
                     
                    $userID                         =   $this->security->xss_clean($this->userID);
                    
                    $moonehUser  = $this->generalModel->getTableValue('email', 'ec_customer', 'customerID="'.$userID.'"', False);
                    $userEmail  =   $moonehUser['email'];
                   
                    $allTotals = $this->cartsapi->getTotalItemsList($languageID,$userID,'');
                    //echo "<pre>"; print_r($allTotals); exit;
                    if($allTotals['0']['code']=='sub_total'){
                        $sVAl = explode(' ',$allTotals[0]['value']); 
                    }
                    $cartItems = $this->cartsapi->getProducts($languageID,$userID);
                    if($cartItems){
                        list($width, $height) = $this->getImageDimension('cart_image_dimen');
                        foreach ($cartItems as $key => $cart) {
                            $cartItems[$key]['price'] = $this->currency->format($cart['price'], $this->siteCurrency);
                            $cartItems[$key]['image'] = $this->setSiteImage('product/' . $cart['image'], $width, $height);
                        }
                        //$data['cartItems'] = $cartItems;
                        $couponData =   array();
                        if ($this->input->get('couponCode')) {
                            
                                $couponCode =   $this->input->get('couponCode');
                                $coupon =   $this->coupon->selectCoupon($couponCode);
                               //echo "<pre>"; print_r($coupon); exit;
                            if(!empty($coupon) && $sVAl[0] >= $coupon['minimum_amount_purchase']){
                                
                                if(@coupon['applayForUser']){
                                    $applyUser = explode(',',$coupon['applayForUser']);
                                }else{
                                    $applyUser = array("Not found");
                                }

                                $applyUsers = array();
                                foreach($applyUser as $key => $apply){
                                    $apply = trim($apply);
                                    if (!empty($apply))
                                        $applyUsers[] = $apply;
                                }

                                if(!empty($coupon)){
                                   
                                    if($coupon['applayFor']=='cart'){
                                        $couponData        = array('couponData'=>array(
                                            'couponID'          => $coupon['couponID'],
                                            'code'              => $coupon['code'],
                                            'type'              => $coupon['type'],
                                            'applayFor'         => $coupon['applayFor'],
                                            'discount'          => $coupon['discount'],
                                            'status'            => $coupon['status']
                                        ));
                                    } elseif($coupon['applayFor']=='delivery'){
                                        $couponData        = array('couponData'=>array(
                                            'couponID'          => $coupon['couponID'],
                                            'code'              => $coupon['code'],
                                            'type'              => $coupon['type'],
                                            'applayFor'         => $coupon['applayFor'],
                                            'discount'          => $coupon['discount'],
                                            'status'            => $coupon['status']
                                        ));
                                    }elseif($coupon['applayFor']=='user'){ 
                                        
                                        if(in_array($userEmail,$applyUsers)){
                                            $couponData        = array('couponData'=>array(
                                                'couponID'          => $coupon['couponID'],
                                                'code'              => $coupon['code'],
                                                'type'              => $coupon['type'],
                                                'applayFor'         => $coupon['applayFor'],
                                                'discount'          => $coupon['discount'],
                                                'status'            => $coupon['status']
                                            ));
                                        }
                                    }elseif($coupon['applayFor']=='firstPurchase'){
                                         $checkOrder = $this->coupon->checkOrderExist($userID);
                                          if(empty($checkOrder)){
                                             $couponData        = array('couponData'=>array(
                                                'couponID'          => $coupon['couponID'],
                                                'code'              => $coupon['code'],
                                                'type'              => $coupon['type'],
                                                'applayFor'         => $coupon['applayFor'],
                                                'discount'          => $coupon['discount'],
                                                'status'            => $coupon['status']
                                            ));
                                          }
                                    }
                                }
                                
                            }
                            
                        }

                        if (!empty($cartItems)) {
                            //$allTotals = $this->carts->getTotalItemsList();
                            $allTotals = $this->cartsapi->getTotalItemsList($languageID,$userID,$couponData,$addressCityID,$moonehDeliveryType);
                            foreach ($allTotals as $key => $tolVal) {
                                $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency);
                                $allTotals[$key]['languageKey'] = lang($allTotals[$key]['code']);
                            }
                        }
                    }
                    
                    $data['subTotal'] = $allTotals;
                    //$data['content'] = $this->accountModel->selectAddress($userID);


                    if ($moonehDeliveryAddressID) {
                       // $data['moonehDeliveryAddress'] = $this->generalModel->getTableValue('*', 'ec_address', 'addressID="' . $moonehDeliveryAddressID . '"', FALSE);
                    }
                    if($couponData){
                        $status      =   parent::HTTP_OK;
                        $message     =   lang('Coupon_Applied');
                        $data        =   $data;
                    }else{
                        $status      =   parent::HTTP_BAD_REQUEST;
                        $message     =   lang('Invalid_Coupon');
                        $data        =   '';
                    }
                }else{
                    $status      =   parent::HTTP_BAD_REQUEST;
                    $message     =   lang('Invalid_Coupon'); 
                    $data        =   '';
                }
          
        }else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $message     =   $msgvalue; 
               $data        =   '';
        }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    
     public function deliveryTimeAdd_get() {
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $orderID                =   $this->security->xss_clean($this->input->get('orderID')) ? (int) $_GET['orderID'] : 0;
        $timeSlotID             =   $this->security->xss_clean($this->input->get('timeSlotID')) ? (int) $_GET['timeSlotID'] : 0;
        $deliveryDate           =   $this->security->xss_clean($this->input->get('deliveryDate')) ?  $_GET['deliveryDate'] : '';
        $orderDeliveryType      =   $this->security->xss_clean($this->input->get('orderDeliveryType')) ?  $_GET['orderDeliveryType'] : '';
     
        if($orderID){
            if (!is_numeric($orderID)){
                $valErr =   1;
                $valErrMsg['orderID']    =  'Only numbers allowed';   
            }
        }else {
            $valErr =   1;
            $valErrMsg['orderID']    =  'orderID missing';   
        }
        
        if($timeSlotID){
            if (!is_numeric($timeSlotID)){
                $valErr =   1;
                $valErrMsg['timeSlotID']    =  'Only numbers allowed';   
            }
        }else {
            $valErr =   1;
            $valErrMsg['timeSlotID']    =  'timeSlotID missing';   
        }
        
        if($deliveryDate==''){
            $valErr =   1;
            $valErrMsg['deliveryDate']    =  'deliveryDate missing';
        }
        
        if($orderDeliveryType==''){
            $valErr =   1;
            $valErrMsg['orderDeliveryType']    =  'orderDeliveryType missing';
        }
        
        if($valErr==0) {
            
            $orderID        =   $this->security->xss_clean($this->input->get('orderID'));
            $userID         =   $this->userID;
            $email          =   $this->getSettingValue('email');
            
                $resData['nonXss'] = array(
                    'orderID'           => $orderID,
                    'timeSlotID'        => $timeSlotID,
                    'deliveryDate'      => $deliveryDate,
                    'orderDeliveryType' => $orderDeliveryType,
                );
                $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
                $return = $this->generalModel->insertValue('ec_order_to_delivery', $resData['xssData']);
                if($return){
                    $status      =   parent::HTTP_OK;
                    $message     =   lang('Success');
                    $data        =   ''; 
                }else{
                    $status      =   parent::HTTP_BAD_REQUEST;
                    $message     =    lang('Bad_request');
                    $data        =   '';
                }
          
        }else{
               $status      =   parent::HTTP_BAD_REQUEST;
               $message     =   $valErrMsg; 
               $data        =   '';
        }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    
     public function orderConfirmMessage($customerID="",$orderID="",$languageID=1){
        $logo  = base_url('uploads/siteInfo/') . $this->getSettingValue('logo');
        $customerData   = $this->generalModel->getTableValue('firstname,lastname,email', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
        $orderData      = $this->generalModel->getTableValue('orderID,invoiceNo,total', 'ec_order', array("orderID" => $orderID));
        $email      =   $this->getSettingValueForAPI('email');
        $phone      =   $this->getSettingValueForAPI('phone');
        $address    =   $this->getSettingValueForAPI('address', $languageID, true);  
        $params = array(
            'orderID' => $orderID
        );
        $products = $this->om->getOrderProducts($params);   
       $productsDetail      = $this->generalModel->getTableValue('*', 'ec_order_total', array("orderID" => $orderID),true);
            $message    =   '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>Order Confirmation</title>
                <style type="text/css">
                @font-face {font-family: "Mary Ann"; src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot"); src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot?#iefix") format("embedded-opentype"); }
                        body {
                                Margin: 0;
                                padding: 0;
                                font-family: Mary Ann;
                                color: #1d1d1d;
                        }
                        table {
                                border-spacing: 0;
                        }
                        td {
                                padding: 0;
                        }
                        img {
                                border: 0;
                        }
                        .wrapper
                        {
                                width: 100%;
                                table-layout: fixed;
                                background-color: #f6f6f6;
                        }
                        .webkit
                        {
                                max-width: 600px;
                                background-color: #fff;
                        }
                        .content
                        {
                            padding: 30px 20px;
                        }
                        .reset a
                        {
                                color: #ffffff;
                                font-weight: 400;
                                text-transform: uppercase;
                                background-color: #F06C00;
                                border: 1px solid #F06C00;
                                text-decoration: none;
                                padding: 5px 8px;
                                text-align: center;
                                border-radius: 5px;
                        }
                        .reset a:hover
                        {
                                color: #F06C00;
                                background-color: #fff;
                                border: 1px solid #F06C00;
                        }
                        .content p
                        {
                                font-size: 15px;
                        } 
                        .footer
                        {
                                border-top: 1px solid #f2f2f2;
                                padding: 30px 0px;
                        } 
                        @media screen and (max-width: 600px) { 
                        }
                        @media screen and (max-width: 400px) { 
                        }
                        

                </style>
        </head>
        <body>
         <center class="wrapper">
                 <div class="webkit">
                        <table class="outer" align="center">
                                 <tr>
                                         <td>
                                                 <table width="100%" style="border-spacing: 0;">
                                                    <tr>
                                                                <td style="background-color:#F06C00;padding: 10px;text-align: center;">
                                                                    <span style="filter: brightness(100) contrast(100%) sepia(99) grayscale(1);">
                                                                        <a href="'.base_url().'" ><img  src="'.$logo.'" width="120" alt="logo" /></a>
                                                                    </span>
                                                                </td>
                                                        </tr>
                                                 </table>
                                         </td>
                                 </tr>
                                 <tr>
                                         <td class="content" style="text-align:left;">
                                                <h3>Hello '.$customerData["firstname"].' '.$customerData["lastname"].',</h3>
                                                <p> Thank you for shopping with Mowneh!
                                                       <br/>Your order '.$orderData["orderID"].' is confirmed and attached herewith is your invoice.</p> 
                                                        <table class="invoice" width="100%" style="border-spacing: 0;border:1px solid #1d1d1d;">
                                                                <tr>
                                                                    <td style="border-right:1px solid #1d1d1d;padding:5px;">Order id : '.$orderData["orderID"].'</td>
                                                                    <td style="border-right:1px solid #1d1d1d;padding:5px;">Invoice No : '.$orderData["invoiceNo"].'</td>
                                                                    <td style="padding:5px;">'.$this->currency->format($orderData["total"], $this->siteCurrency).'</td>
                                                                </tr></table><br/><table class="invoice" width="100%" style="border-spacing: 0;border:1px solid #1d1d1d;">
                                                                <tr><th>Product</th><th>Total</th></tr>';
                                                                foreach( $products as $product){ 
                                                                 $message    .=   ' <tr>
                                                                    <td style="border-right:1px solid #1d1d1d;padding:5px;">'.$product["name"].'</td>
                                                                    <td style="border-right:1px solid #1d1d1d;padding:5px;">'.$this->currency->format($product["total"], $this->siteCurrency).'</td>
                                                                    
                                                                </tr>';
                                                                }
                                                                foreach( $productsDetail as $detail){ 
                                                                 $message    .=   ' <tr>
                                                                    <td style="border-right:1px solid #1d1d1d;padding:5px;">'.$detail["title"].'</td>
                                                                    <td style="border-right:1px solid #1d1d1d;padding:5px;">'.$this->currency->format($detail["value"], $this->siteCurrency).'</td>
                                                                    
                                                                </tr>';
                                                                }
                                                       $message    .=   ' </table>	
                                                        <p>
                                                        <br/>Please let us know if you have any issues with your purchase, by replying to this email or calling our hotline '.$phone.'.
                                                        <br/>
                                                        <br/>Regards,
                                                        <br/>Mowneh Team
                                                </p>
                                         </td>
                                 </tr>
                                 <tr>
                                         <td>
                                                <table width="100%" style="border-spacing: 0;">
                                                        <tr>
                                                                <td class="footer" style="border-top:1px solid #f2f2f2;padding: 15px;text-align:left;"> 
                                                                        <p style="font-size: 12px;color: #a7a7a7;">
                                                                                '.$address.'<br/>Copyright © 2020 <a style="color: #a7a7a7;" href="http://www.mowneh.com">Mowneh.</a> All Rights Reserved
                                                                        </p>

                                                                </td>
                                                                <td style="border-top:1px solid #f2f2f2;padding: 15px;text-align:right;">
                                                                       <a href="'.base_url().'"><img src="'.$logo.'" width="80" alt="logo"></a>
                                                                        <br/>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td colspan="2" style="padding: 5px; background-color: #F06C00;">
                                                                </td>
                                                        </tr>
                                                </table>
                                         </td>
                                 </tr>
                         </table>
                 </div>
         </center>
        </body>
        </html>';
        
            return $message;

    }
    
    public function orderConfirmMessageAdmin($customerID="",$orderID="",$languageID=1){
        $logo  = base_url('uploads/siteInfo/') . $this->getSettingValue('logo');
        $customerData   = $this->generalModel->getTableValue('firstname,lastname,email', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
        $orderData      = $this->generalModel->getTableValue('orderID,invoiceNo,total', 'ec_order', array("orderID" => $orderID));
        $email      =   $this->getSettingValueForAPI('email');
        $phone      =   $this->getSettingValueForAPI('phone');
        $address    =   $this->getSettingValueForAPI('address', $languageID, true);
            $message    =   '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>Order Confirmation</title>
                <style type="text/css">
                @font-face {font-family: "Mary Ann"; src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot"); src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot?#iefix") format("embedded-opentype"); }
                        body {
                                Margin: 0;
                                padding: 0;
                                font-family: Mary Ann;
                                color: #1d1d1d;
                        }
                        table {
                                border-spacing: 0;
                        }
                        td {
                                padding: 0;
                        }
                        img {
                                border: 0;
                        }
                        .wrapper
                        {
                                width: 100%;
                                table-layout: fixed;
                                background-color: #f6f6f6;
                        }
                        .webkit
                        {
                                max-width: 600px;
                                background-color: #fff;
                        }
                        .content
                        {
                               padding: 30px 20px;
                        }
                        .reset a
                        {
                                color: #ffffff;
                                font-weight: 400;
                                text-transform: uppercase;
                                background-color: #F06C00;
                                border: 1px solid #F06C00;
                                text-decoration: none;
                                padding: 5px 8px;
                                text-align: center;
                                border-radius: 5px;
                        }
                        .reset a:hover
                        {
                                color: #F06C00;
                                background-color: #fff;
                                border: 1px solid #F06C00;
                        }
                        .content p
                        {
                                font-size: 15px;
                        } 
                        .footer
                        {
                                border-top: 1px solid #f2f2f2;
                                padding: 30px 0px;
                        } 
                        @media screen and (max-width: 600px) { 
                        }
                        @media screen and (max-width: 400px) { 
                        }
                </style>
        </head>
        <body>
         <center class="wrapper">
                 <div class="webkit">
                         <table class="outer" align="center">
                                 <tr>
                                         <td>
                                                <table width="100%" style="border-spacing: 0;">
                                                    <tr>
                                                                <td style="background-color:#F06C00;padding: 10px;text-align: center;">
                                                                    <span style="filter:brightness(100) contrast(100%) sepia(99) grayscale(1)">
                                                                        <a href="'.base_url().'" ><img  src="'.$logo.'" width="120" alt="logo" /></a>
                                                                    </span>
                                                                </td>
                                                        </tr>
                                                 </table>
                                         </td>
                                 </tr>
                                 <tr>
                                         <td class="content" style="text-align:left;">
                                                <h3>Hi,</h3>
                                                        <br/>You have a new order from '.$customerData["firstname"].' '.$customerData["lastname"].'</p>  
                                                        <table class="invoice" width="100%" style="border-spacing: 0;border:1px solid #1d1d1d;">
                                                                <tr>
                                                                        <td style="border-right:1px solid #1d1d1d;padding:5px;">Order id : '.$orderData["orderID"].'</td>
                                                                        <td style="border-right:1px solid #1d1d1d;padding:5px;">Invoice No : '.$orderData["invoiceNo"].'</td>
                                                                        <td style="padding:5px;">'.$this->currency->format($orderData["total"], $this->siteCurrency).'</td>
                                                                </tr>
                                                        </table>	
                                                        <p>
                                                        <br/>
                                                        <br/>Regards,
                                                        <br/>Mowneh Team
                                                </p>
                                         </td>
                                 </tr>
                                 <tr>
                                         <td>
                                                <table width="100%" style="border-spacing: 0;">
                                                        <tr>
                                                                <td class="footer" style="border-top:1px solid #f2f2f2;padding: 15px;text-align:left;"> 
                                                                        <p style="font-size: 12px;color: #a7a7a7;">
                                                                                '.$address.'<br/>Copyright © 2020 <a style="color: #a7a7a7;" href="http://www.mowneh.com">Mowneh.</a> All Rights Reserved
                                                                        </p>
                                                                </td>
                                                                <td style="border-top:1px solid #f2f2f2;padding: 15px;text-align:right;">
                                                                        <a href="'.base_url().'"><img src="'.$logo.'" width="80" alt="logo"></a>
                                                                        <br/>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td colspan="2" style="padding: 5px; background-color: #F06C00;">
                                                                </td>
                                                        </tr>
                                                </table>
                                         </td>
                                 </tr>
                         </table>
                 </div>
         </center>
        </body>
        </html>';

            return $message;

    }
    
}
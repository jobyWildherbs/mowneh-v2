<?php
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
defined('BASEPATH') OR exit('No direct script access allowed');
class General extends REST_Controller{
    public function __construct()
    {
        parent::__construct();  
        $this->load->model('GeneralModel','generalModel');
        $this->load->model('CategoryModel','catModel');
        $this->load->model('ProductModel', 'productModel');
        $this->load->model('MobileBannerModel', 'bannerModel');
        $this->load->model('WidgetModel', 'widgetModel');
        $this->load->model('MobilePromotionModel', 'promotionModel');
        $this->load->model('AccountModel', 'accountModel'); 
        $this->load->model('NotificationModel', 'notiModel');
        $this->load->library('form_validation');  
        
    }
    
    public function listHomeContent_get(){
        $valErr =   0;
        $userID         =   $this->userID;
        $valErrMsg  =   array();
        $data        =   '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
            
        if($valErr==0){
            //Home banner start
            $sliderListdata =   array();
            $sliderList = $this->bannerModel->selectBanner($languageID);
            if($sliderList){
                list($width, $height) = $this->getImageDimension('home_banner_slider_dimen');
                foreach ($sliderList as $key => $sliderData) {

                    $sliderListdata[$key]['image'] = $this->setSiteImage("banner/" . $sliderData['image'], $width, $height);
                }
                $resultData['homeBanner']   =   $sliderListdata;
            }
            //Home banner end
            
            //Promotion start
            $promotionListData  =   array();
            $promotionList = $this->promotionModel->selectPromotion($languageID);
            
//            $i  =   1;
            if($promotionList){
                list($width, $height) = $this->getImageDimension('promotion_banner_dimen');
                foreach ($promotionList as $key => $promotionData) {
                    
                    $promotionListData[$key]['image'] = $this->setSiteImage("promotion/" . $promotionData['image'], $width, $height);
                    $promotionListData[$key]['categoryID'] = $promotionData['categoryID'];
//                    $i++;
                }
                $resultData['promotion']   =   $promotionListData;
            }
            
            //static
//            $promotionListData[0]['image']  =   $this->setSiteImage("promotion/" . 'baby-offers.jpg', $width, $height);
//            $promotionListData[0]['categoryID'] =   1;
//            $promotionListData[1]['image']  =   $this->setSiteImage("promotion/" . 'easy-meals.jpg', $width, $height);
//            $promotionListData[1]['categoryID'] =   2;
//            $promotionListData[2]['image']  =   $this->setSiteImage("promotion/" . 'qatari-food.jpg', $width, $height);
//            $promotionListData[2]['categoryID'] =   3;
//            $promotionListData[3]['image']  =   $this->setSiteImage("promotion/" . 'vegan.jpg', $width, $height);
//            $promotionListData[3]['categoryID'] =   4;
            //Promotion end
            $resultData['promotion']   =   $promotionListData;
            //Parent category start
            $parentCategory   =   $this->catModel->selectCategoryByParent(0,$languageID);
            if($parentCategory){
              
                foreach ($parentCategory as $key => $catData) {
                    list($width, $height) = $this->getImageDimension('list_category_dimen');
                    $parentCategory[$key]['image'] = $this->setSiteImage("category/" . $catData['image'], $width, $height);
                    list($width, $height) = $this->getImageDimension('banner_category_dimen');
                    $parentCategory[$key]['banner'] = $this->setSiteImage("category/banner/" . $catData['banner'], $width, $height);
                }
                $resultData['parentCategory']   =   $parentCategory;
            }
            //Parent category end
            
            //Widget start
            $widget = $this->widgetModel->widgetHomeShow($languageID, $userID);
            
            if($widget){ 
                foreach ($widget as $key => $widgetData) {
                    if(is_null($widgetData['widgetName']))
                        $widget[$key]['widgetName']   =   '';
                    
                    $dimen = $widgetData['imageDimension'];
                    $explodeDimen = explode("x", $dimen);
                    if (is_array($explodeDimen) && count($explodeDimen) == 2) {
                        $promWidth = $explodeDimen[0];
                        $promHeight = $explodeDimen[1];
                    } else {
                        $promWidth = "";
                        $promHeight = "";
                    }
                    if ($widgetData['typeKey'] != 'category') {
                         //echo "<pre>"; print_r($widgetData['widgetProduct']); exit;
                        foreach ($widgetData['widgetProduct'] as $innerKey => $productData) {
                            unset($widget[$key]['widgetProduct'][$innerKey]['description']);
                            unset($widget[$key]['widgetProduct'][$innerKey]['pageKey']);
                            $widget[$key]['widgetProduct'][$innerKey]['offPersentage'] = '';
                            $widget[$key]['widgetProduct'][$innerKey]['discountPrice'] = '';
                            $widget[$key]['widgetProduct'][$innerKey]['weight'] = strval($widget[$key]['widgetProduct'][$innerKey]['weight']*1)." ".$this->getSettingValueForAPI('weight_unit',$languageID);
                            
                            $discountPrice = "";
                            $productOffer    = $this->generalModel->getTableValue('*', 'ec_premotion_to_product', 'productID = '.$productData['productID'].' AND priority=(select max(priority) from ec_premotion_to_product)', FALSE);
                            if($productOffer){
                                $discountPrice = $this->currency->format($productOffer['offePrice'], $this->siteCurrency);
                                $widget[$key]['widgetProduct'][$innerKey]['offPersentage']  = $productOffer['offer'];
                            }else{
                                $productDiscount = $this->productModel->productDiscount($productData['productID']);
                                if (!empty($productDiscount)) {
                                    if ($productDiscount['type'] == 'fixed') {
                                        $discountPrice = $productData['price'] - $productDiscount['price'];
                                        $offPersentage = ($productDiscount['price'] / $productData['price']) * 100;
                                        $widget[$key]['widgetProduct'][$innerKey]['offPersentage'] = $offPersentage;
                                    } else {
                                        $offPersentage = $productDiscount['price'];
                                        $widget[$key]['widgetProduct'][$innerKey]['offPersentage'] = $offPersentage;
                                        $discountPrice = $productData['price'] - ($productData['price'] * $productDiscount['price'] / 100);
                                    }
                                }
                            }
                            
                            if ($discountPrice != '')
                                $widget[$key]['widgetProduct'][$innerKey]['discountPrice'] = $this->currency->format($discountPrice, $this->siteCurrency);

                            $now = time(); // or your date as well
                            $dateAdded = strtotime($productData['dateAdded']);
                            $datediff = $now - $dateAdded;
                            if (round($datediff / (60 * 60 * 24)) < 7) {
                                $widget[$key]['widgetProduct'][$innerKey]['new'] = 'new';
                            }
                            $widget[$key]['widgetProduct'][$innerKey]['price'] = $this->currency->format($productData['price'], $this->siteCurrency);
                            //echo "<pre>";print_r($promHeight);exit;
                            $widget[$key]['widgetProduct'][$innerKey]['image'] = $this->setSiteImage("product/" . $productData['image'], $promWidth, $promHeight);
                            //$widget[$key]['widgetProduct'][$innerKey]['productDiscount'] = $productDiscount;
                        }
                    } elseif ($widgetData['typeKey'] == 'category') {
                        foreach ($widgetData['widgetCategory'] as $innerKey => $productData) {
                            $widget[$key]['widgetCategory'][$innerKey]['image'] = $this->setSiteImage("category/" . $productData['image'], $promWidth, $promHeight);
                        }

                        // echo"<pre>"; print_r($widget[$key]['widgetProduct'][$innerKey]['image']);exit;
                    }
                }
                $resultData['widget']   =   $widget;
            } else {
                $resultData['widget']   =   array();
            }
            //Widget end
            
            if($resultData){
                $status      =   parent::HTTP_OK;
                $message     =   lang('Success'); 
                $data        =   $resultData;
            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   lang('Sorry_No_Data_Found');
            }
        }else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
            $message    =   $msgvalue;
            $status      =   parent::HTTP_BAD_REQUEST;
        }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    
    public function listHomeWidget_get(){
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        $categoryID     =   $this->security->xss_clean($this->input->get('categoryID'));
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
       
            
        
        if($valErr==0){
            $userID         =   $this->userID;
            $widget = $this->widgetModel->widgetHomeShow($languageID, $userID);
            
            if($widget){
                
                foreach ($widget as $key => $widgetData) {
                    $dimen = $widgetData['imageDimension'];
                    $explodeDimen = explode("x", $dimen);
                    if (is_array($explodeDimen) && count($explodeDimen) == 2) {
                        $promWidth = $explodeDimen[0];
                        $promHeight = $explodeDimen[1];
                    } else {
                        $promWidth = "";
                        $promHeight = "";
                    }
                    if ($widgetData['typeKey'] != 'category') {
                        // echo "<pre>"; print_r($widgetData['widgetProduct']); exit;
                        foreach ($widgetData['widgetProduct'] as $innerKey => $productData) {
                            $productDiscount = $this->productModel->productDiscount($productData['productID']);
                            $discountPrice = "";
                            if (!empty($productDiscount)) {
                                if ($productDiscount['type'] == 'fixed') {
                                    $discountPrice = $productData['price'] - $productDiscount['price'];
                                    $offPersentage = ($productDiscount['price'] / $productData['price']) * 100;
                                    $widget[$key]['widgetProduct'][$innerKey]['offPersentage'] = $offPersentage;
                                } else {
                                    $offPersentage = $productDiscount['price'];
                                    $widget[$key]['widgetProduct'][$innerKey]['offPersentage'] = $offPersentage;
                                    $discountPrice = $productData['price'] - ($productData['price'] * $productDiscount['price'] / 100);
                                }
                            }
                            if ($discountPrice != '')
                                $widget[$key]['widgetProduct'][$innerKey]['discountPrice'] = $this->currency->format($discountPrice, $this->siteCurrency);

                            $now = time(); // or your date as well
                            $dateAdded = strtotime($productData['dateAdded']);
                            $datediff = $now - $dateAdded;
                            if (round($datediff / (60 * 60 * 24)) < 7) {
                                $widget[$key]['widgetProduct'][$innerKey]['new'] = 'new';
                            }
                            $widget[$key]['widgetProduct'][$innerKey]['price'] = $this->currency->format($productData['price'], $this->siteCurrency);
                            //echo "<pre>";print_r($promHeight);exit;
                            $widget[$key]['widgetProduct'][$innerKey]['image'] = $this->setSiteImage("product/" . $productData['image'], $promWidth, $promHeight);
                            $widget[$key]['widgetProduct'][$innerKey]['productDiscount'] = $productDiscount;
                        }
                    } elseif ($widgetData['typeKey'] == 'category') {
                        foreach ($widgetData['widgetCategory'] as $innerKey => $productData) {
                            $widget[$key]['widgetCategory'][$innerKey]['image'] = $this->setSiteImage("category/" . $productData['image'], $promWidth, $promHeight);
                        }

                        // echo"<pre>"; print_r($widget[$key]['widgetProduct'][$innerKey]['image']);exit;
                    }
                }
              
                
                $status      =   parent::HTTP_OK;
                $message     =   lang('Success');
                $data        =   $widget;
            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   lang('Sorry_No_Data_Found');
                $data        =   '';
            }
        }else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
            $message    =   $msgvalue;
            $status      =   parent::HTTP_BAD_REQUEST;
        }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    
    public function listCategory_get(){
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        $categoryID     =   $this->security->xss_clean($this->input->get('categoryID'));
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        
        if($categoryID){
            if (!is_numeric($categoryID)){
                $valErr =   1;
                $valErrMsg['categoryID']    =  'Only numbers allowed';   
            }
        }
            
        if(!$categoryID)
            $categoryID =   0;
        if($valErr==0){
            $category   =   $this->catModel->selectCategoryByParent($categoryID,$languageID);
            
            if($category){
              
                foreach ($category as $key => $catData) {
                    list($width, $height) = $this->getImageDimension('list_category_dimen');
                $category[$key]['image'] = $this->setSiteImage("category/" . $catData['image'], $width, $height);
                list($width, $height) = $this->getImageDimension('banner_category_dimen');
                $category[$key]['banner'] = $this->setSiteImage("category/banner/" . $catData['banner'], $width, $height);
            }
                $status      =   parent::HTTP_OK;
                $message     =   lang('Success'); 
                $data        =   $category;
            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   lang('Sorry_No_Data_Found');
                $data        =   '';
            }
        }else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
            $message    =   $msgvalue;
            $status      =   parent::HTTP_BAD_REQUEST;
        }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function listfilter_get(){
        
        
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        
        if($valErr==0){
               $filterList = $this->productModel->selectFilterList($languageID);
                $filterArr = array(); 
                if($filterList){
                    //echo "<pre>"; print_r($filterList); exit;
                    foreach ($filterList as $key => $filVal) {
                        $filterArr[$filVal['groupName']][$key]['filterName'] = $filVal['filterName'];
                        $filterArr[$filVal['groupName']][$key]['filterID'] = $filVal['filterID'];
                        
                    }
                    $i=0;
                     foreach ($filterArr as $key => $filArrData) {
                        $filterArrayVal[$i]['filterGroup'] =   $key; 
                        $j  =   0;
                        foreach ($filArrData as $keyCount => $filArrVal) {
                            $filterArrayVal[$i]['filter'][$j]['filterName'] =   $filArrVal['filterName'];   
                            $filterArrayVal[$i]['filter'][$j]['filterID'] =   $filArrVal['filterID'];
                            $j++;
                        }
                        $i++;
                    }

                    
                    $status      =   parent::HTTP_OK;
                    $message     =   lang('Success');  
                    $data        =   $filterArrayVal; 
               
                }else{
                    $status      =   parent::HTTP_BAD_REQUEST;
                    $message     =   lang('Sorry_No_Data_Found'); 
                    $data        =   '';
                }
        }else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
            $message    =   $msgvalue;
            $status      =   parent::HTTP_BAD_REQUEST;
        }       
                
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, REST_Controller::HTTP_OK);
    }
    
     public function listBrands_get(){
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        
        if($valErr==0){        
               $brands = $this->productModel->selectManufacturerList($languageID); 
                if($brands){
                    $status      =   parent::HTTP_OK;
                    $message     =   lang('Success');  
                    $data        =   $brands; 
               
                }else{
                    $status      =   parent::HTTP_BAD_REQUEST;
                    $message     =   lang('Sorry_No_Data_Found');  
                    $data        =   '';
                }
            }else{
                $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
                $message    =   $msgvalue;
                $status      =   parent::HTTP_BAD_REQUEST;
            }     
        $this->setHeaderToken();       
        
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, REST_Controller::HTTP_OK);
    }
    
    public function listBrandsAndFilter_get(){
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        
        if($valErr==0){        
                $brands = $this->productModel->selectManufacturerList($languageID); 
               
                if($brands){
                    foreach($brands as $key=>$brandArr){
                        $brands[$key]['brandStatus']  =   FALSE;
                    }
                }
                 //echo "<pre>"; print_r($brands); exit;
                $filterList = $this->productModel->selectFilterList($languageID);
                $filterArr = array(); 
                if($filterList){
                    //echo "<pre>"; print_r($filterList); exit;
                    foreach ($filterList as $key => $filVal) {
                        $filterArr[$filVal['groupName']][$key]['filterName'] = $filVal['filterName'];
                        $filterArr[$filVal['groupName']][$key]['filterID'] = $filVal['filterID'];
                        
                    }
                    $i=0;
                     foreach ($filterArr as $key => $filArrData) {
                        $filterArrayVal[$i]['filterGroup'] =   $key; 
                        $j  =   0;
                        foreach ($filArrData as $keyCount => $filArrVal) {
                            $filterArrayVal[$i]['filter'][$j]['filterName'] =   $filArrVal['filterName'];   
                            $filterArrayVal[$i]['filter'][$j]['filterID'] =     $filArrVal['filterID'];
                            $filterArrayVal[$i]['filter'][$j]['filterStatus'] =     FALSE;
                            $j++;
                        }
                        $i++;
                    }
                    $dataArray['brandList'] =   $brands;
                    $dataArray['filterList'] =   $filterArrayVal; 
                }
                 $status      =   parent::HTTP_OK;
                    $message     =   lang('Success');  
                    $data        =   $dataArray; 
        }
        else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
            $message    =   $msgvalue;
            $status      =   parent::HTTP_BAD_REQUEST;
        }    
        $this->setHeaderToken();       
        
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, REST_Controller::HTTP_OK);
    }
    
    
    public function addtowishlist_get() {
        
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $productID     =   $this->security->xss_clean($this->input->get('productID')) ? (int) $_GET['productID'] : '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        if($productID){
            if (!is_numeric($productID)){
                $valErr =   1;
                $valErrMsg['productID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['productID']    =  'productID missing';   
        }
        
        if($valErr==0){ 
            $acion  =   '';
            $userID =   $this->userID;
            $value['productID']  = $productID;
            $value['customerID'] = $userID;
            $listItem = $this->generalModel->getTableValue('*', 'ec_customer_wishlist', array('customerID' => $userID, 'productID' => $productID), FALSE);
            if ($listItem) {
                $this->generalModel->deleteTableValues('ec_customer_wishlist', array('customerID' => $userID, 'productID' => $productID), FALSE);
  
                $message = lang('Removed_From_Your_Wishlist');
                $acion  =   1;
            } else {
                $addWishList = $this->generalModel->insertValue('ec_customer_wishlist', $value);
     
                $message = lang('Added_To_Your_Wishlist');
                $acion  =   1;
            }
            
            if($acion){
                $status      =   parent::HTTP_OK;
                $message     =   $message; 
                $data        =   ''; 
            }
            else{
               $status      =   parent::HTTP_BAD_REQUEST;
               $message     =   "Failed"; 
               $data        =   '';
               
            }
            $userID =   $this->userID;
            $wishListData   =   array();
            $wishList = $this->accountModel->selectWishList($languageID, $userID);
            list($width, $height) = $this->getImageDimension('list_product_dimen');
            //echo "<pre>"; print_r($wishList); exit;
            if($wishList){
                foreach ($wishList as $key => $row) {
                    $productDiscount = $this->productModel->productDiscount($row['productID']);
                    
                    $wishListData[$key]['productID'] = $row['productID'];
                    $wishListData[$key]['quantity'] = $row['quantity'];
                    $wishListData[$key]['image'] = $this->setSiteImage("product/" . $row['image'], $width, $height);
                    $wishListData[$key]['price'] = $this->currency->format($row['price'], $this->siteCurrency);
                    $wishListData[$key]['name'] = $row['name'];
                    $wishListData[$key]['dateAdded'] = $row['dateAdded'];
                    $wishListData[$key]['wishList'] = 'Yes';
                     $discountPrice = "";
                    $wishListData[$key]['offPersentage'] = '';
                    $wishListData[$key]['discountPrice'] = '';
                    if (!empty($productDiscount)) {
                        if ($productDiscount['type'] == 'fixed') {
                            $discountPrice = $row['price'] - $productDiscount['price'];
                            $offPersentage = ($productDiscount['price'] / $row['price']) * 100;
                            $wishListData[$key]['offPersentage'] = $offPersentage;
                        } else {
                            $offPersentage = $productDiscount['price'];
                            $wishListData[$key]['offPersentage'] = $offPersentage;
                            $discountPrice = $row['price'] - ($row['price'] * $productDiscount['price'] / 100);
                        }
                    }
                    if ($discountPrice != '')
                        $wishListData[$key]['discountPrice'] = $this->currency->format($discountPrice, $this->siteCurrency);
                    else 
                        $wishListData[$key]['discountPrice'] =   '';
                        
                }
            }
                $data   =   $wishListData;
        }else{
                $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
                $message    =   $msgvalue;
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $data        =   '';
               
            }
        $this->setHeaderToken(); 
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
        
    }
    
    public function authenticationCheck_get() {
        $this->setHeaderToken(); 
        $status                 = parent::HTTP_OK;
        $result['status']	= $status;
        $result['message']      = '';
        $result['data']		= '';
        $this->response($result, $status);
    }
    
    public function firebaseTokenSave_post() {
         
        $config = [
                        [
                                'field' => 'firebaseToken',
                                'label' => 'Firebase Token',
                                'rules' => 'required',
                                'errors' => [
                                        'required'      => 'Firebase Token missing',

                                ],
                        ]
                    ];
        $data = $this->input->post();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() === TRUE) {
            $acion  =   '';
            $firebaseToken  = $this->input->post('firebaseToken');
            $userID         =  $this->input->post('userID') ?? 0;

            $value['firebaseToken']     = $firebaseToken;
            
             if($userID!=0){
              
//                    $fireID =   $this->generalModel->getFieldValue('fireID', 'ec_firebase_token', 'firebaseToken="'. $firebaseToken.'"');
                  $fireID =   $this->generalModel->getFieldValue('fireID', 'ec_firebase_token', 'userID="'. $userID.'"');
                    if($fireID){

                       $value['userID']            = $userID;
                       $value['dateModified'] = date('Y-m-d H:i');
                        $this->generalModel->updateTableValues('ec_firebase_token','firebaseToken="'. $firebaseToken.'"',$value);
                    }else{
                        $value['userID']            = $userID;
                        $value['dateAdded'] = date('Y-m-d H:i');
                        $this->generalModel->insertValue('ec_firebase_token', $value);
                    }
                    $message = 'Token Added';
                $acion  =   1;
            }else{
                $message    =   "No user login";
            }
               // echo $this->db->last_query();exit;
                
            if($acion){
                $status      =   parent::HTTP_OK;
                $message     =   $message; 
                $data        =   ''; 
            }
            else{
               $status      =   parent::HTTP_BAD_REQUEST;
               $message     =   $this->form_validation->error_array(); 
               $data        =   '';
               
            } 
        }else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($this->form_validation->error_array() as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                }
               $status      =   parent::HTTP_BAD_REQUEST;
               $message     =   $msgvalue; 
               $data        =   '';
               
            }
            $result['status']       = $status;
            $result['message']      = $message;
            $result['data']         = $data;
             $this->response($result, $status);
 
    }
    
    public function listNotification_get() {
        $valErr =   0; 
        $valErrMsg  =   array();
        $data        =   '';
        //$firebaseToken     =   $this->security->xss_clean($this->input->get('firebaseToken')) ? (int) $_GET['firebaseToken'] : '';
        $userID             =   $this->security->xss_clean($this->input->get('userID')) ? (int) $_GET['userID'] : 0;
//        if($firebaseToken==''){
//           
//            $valErr =   1;
//            $valErrMsg['firebaseToken']    =  'Token missing';   
//        }
//        if($userID=="" || $userID!=0){
//            $valErr =   1;
//            $valErrMsg['userID']    =  'userID missing';   
//        }
        
        if($valErr==0){ 
            $acion  =   ''; 
            if($userID!=0){
                //$value['firebaseToken']     = $firebaseToken;
                $notificationList =   $this->notiModel->notificationList($userID);
                list($width, $height) = $this->getImageDimension('notification_dimen');
                if($notificationList){
                    foreach($notificationList as $key=>$notificationData){
                        $notificationList[$key]['image']    =   $this->setSiteImage("notification/" . $notificationData['image'], $width, $height);
                    }
                    $status      =   parent::HTTP_OK;
                    $message     =   "Success"; 
                    $data        =   $notificationList; 
                }
                else{
                   $status      =   parent::HTTP_BAD_REQUEST;
                   $message     =   "No Notification Found"; 
                   $data        =   '';

                } 
            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                   $message     =   "No Notification Found"; 
                   $data        =   '';
            }
            
        }else{
                $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
               $status      =   parent::HTTP_BAD_REQUEST;
               $message     =   $msgvalue; 
               $data        =   '';
               
            }
            $result['status']       = $status;
            $result['message']      = $message;
            $result['data']         = $data;
             $this->response($result, $status);
 
    }
    
    public function notificationSatatusChange_post() {
         
        $config = [
                        [
                                'field' => 'userID',
                                'label' => 'userID',
                                'rules' => 'required',
                                'errors' => [
                                        'required'      => 'userID missing',

                                ],
                        ],
                        [
                                'field' => 'notificationID',
                                'label' => 'notificationID',
                                'rules' => 'required',
                                'errors' => [
                                        'required'      => 'notificationID missing',

                                ],
                        ]
                    ];
        $data = $this->input->post();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() === TRUE) {
            $acion  =   '';
            $notificationID  = $this->input->post('notificationID');
            $userID          =  $this->input->post('userID');

                if($notificationID && $userID){
                   $value['seenStatus']     = 'seen';
                   $return  =   $this->generalModel->updateTableValues('ec_notification_to_user','notificationID='. $notificationID.' AND userID='.$userID,$value);
                   
                }
  
                $status      =   parent::HTTP_OK;
                $message     =   lang('Success'); 
                $data        =   ''; 
            
        }else{
               $msgvalue    =   '';
                $i  =   1;
                foreach($this->form_validation->error_array() as $key=>$msgData){
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                }
               $status      =   parent::HTTP_BAD_REQUEST;
               $message     =   $msgvalue; 
               $data        =   '';
               
            }
            $result['status']       = $status;
            $result['message']      = $message;
            $result['data']         = $data;
             $this->response($result, $status);
 
    }
    
    public function notificationCount_get() {
        $valErr =   0; 
        $valErrMsg  =   array();
        $data        =   '';
        //$firebaseToken     =   $this->security->xss_clean($this->input->get('firebaseToken')) ? (int) $_GET['firebaseToken'] : '';
        $userID             =   $this->security->xss_clean($this->input->get('userID')) ? (int) $_GET['userID'] : 0;
//        if($firebaseToken==''){
//           
//            $valErr =   1;
//            $valErrMsg['firebaseToken']    =  'Token missing';   
//        }
//        if($userID){
//            if (!is_numeric($userID)){
//                $valErr =   1;
//                $valErrMsg['userID']    =  'Only numbers allowed';   
//            }
//        } else {
//            $valErr =   1;
//            $valErrMsg['userID']    =  'userID missing';   
//        }
        
        if($valErr==0){ 
            $acion  =   '';
            if($userID!=0){
                $notificationcount =   $this->notiModel->notificationCount($userID);
                if($notificationcount>30){
                    $count['count'] = 30;
                }else{
                    $count['count'] =   $notificationcount;
                }

                if($notificationcount){
                    $status      =   parent::HTTP_OK;
                    $message     =   lang('Success');
                    $data        =   $count; 
                }
                else{
                   $status      =   parent::HTTP_BAD_REQUEST;
                   $message     =   lang('No_Notification_Found');
                   $data        =   '';

                } 
            }else{
                    $count['count'] =   0;
                    $status      =   parent::HTTP_OK;
                    $message     =   lang('Success');
                    $data        =   $count; 
            }
            
        }else{
                $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
               $status      =   parent::HTTP_BAD_REQUEST;
               $message     =   $msgvalue; 
               $data        =   '';
               
            }
            $result['status']       = $status;
            $result['message']      = $message;
            $result['data']         = $data;
             $this->response($result, $status);
 
    }
    
    public function supportContact_post() {
        $config = [
                        [
                                'field' => 'title',
                                'label' => 'title',
                                'rules' => 'required',
                                'errors' => [
                                        'required'      => 'Title missing',

                                ],
                        ],
                        [
                                'field' => 'message',
                                'label' => 'message',
                                'rules' => 'required',
                                'errors' => [
                                        'required'      => 'Message missing',

                                ],
                        ]
                    ];
        $data = $this->input->post();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() === TRUE) {
            $acion  =   '';
            
            $userID          = $this->userID; 
            $title           = $this->input->post('title');
            $messageData     = $this->input->post('message');
               
                $config['protocol'] = 'sendmail';
                $config['mailtype'] = 'html';
                $from_email =   $this->getSettingValue('email');
                $to_email =   $this->getSettingValue('support_email');
                
                $emailMessage   = $this->supportEmailContent($userID,$title,$messageData);
                
                //Load email library 
                $this->load->library('email');
                $this->email->initialize($config);
                $this->email->from($from_email, 'Mooneh');
                $this->email->to($to_email);
                $this->email->subject($title);
                $this->email->message($emailMessage);
                if($this->email->send()){
                    $status      =   parent::HTTP_OK;
                    $message     =   lang('contactSuccess');
                    $data        =   ''; 
                }else{
                    $status      =   parent::HTTP_BAD_REQUEST;
                    $message     =   lang('Failed'); 
                    $data        =  '';
                }
        }else{
               $msgvalue    =   '';
                $i  =   1;
                foreach($this->form_validation->error_array() as $key=>$msgData){
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                }
               $status      =   parent::HTTP_BAD_REQUEST;
               $message     =   $msgvalue; 
               $data        =   '';
               
            }
            $result['status']       = $status;
            $result['message']      = $message;
            $result['data']         = $data;
            $this->response($result, $status);
    }
    
    public function supportEmailContent($customerID   =   "",$title =   "",$messageData =   ""){
        $logo  = base_url('uploads/siteInfo/') . $this->getSettingValue('logo');
        $resetLink  =   base_url('/Login/resetPassword/'.$customerID); 
        $customerData   = $this->generalModel->getTableValue('firstname,lastname,email', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
        $customerEmail  =   $customerData['email'];
        $email      =   $this->getSettingValue('email');
        $phone      =   $this->getSettingValue('phone');
        $message    =   '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                            <html xmlns="http://www.w3.org/1999/xhtml">
                            <head>
                                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                                    <title>'.$title.'</title>

                                    <style type="text/css">
                                    @font-face {font-family: "Mary Ann"; src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot"); src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot?#iefix") format("embedded-opentype"); }
                                            body {
                                                    Margin: 0;
                                                    padding: 0;
                                                    font-family: Mary Ann;
                                                    color: #1d1d1d;
                                            }
                                            table {
                                                    border-spacing: 0;
                                            }
                                            td {
                                                    padding: 0;
                                            }
                                            img {
                                                    border: 0;
                                            }
                                            .wrapper
                                            {
                                                    width: 100%;
                                                    table-layout: fixed;
                                                    background-color: #f6f6f6;
                                            }
                                            .webkit
                                            {
                                                    max-width: 600px;
                                                    background-color: #fff;
                                            }
                                            .content
                                            {
                                                    padding: 30px 20px;
                                            }
                                            .reset a
                                            {
                                                    color: #ffffff;
                                                    font-weight: 400;
                                                    text-transform: uppercase;
                                                    background-color: #F06C00;
                                                    border: 1px solid #F06C00;
                                                    text-decoration: none;
                                                    padding: 5px 8px;
                                                    text-align: center;
                                                    border-radius: 5px;
                                            }
                                            .reset a:hover
                                            {
                                                    color: #F06C00;
                                                    background-color: #fff;
                                                    border: 1px solid #F06C00;
                                            }
                                            .content p
                                            {
                                                    font-size: 15px;
                                            } 
                                            .footer
                                            {
                                                    border-top: 1px solid #f2f2f2;
                                                    padding: 30px 0px;
                                            }
                                            @media screen and (max-width: 600px) { 
                                            }
                                            @media screen and (max-width: 400px) { 
                                            }
                                    </style>
                            </head>
                            <body>
                             <center class="wrapper">
                                     <div class="webkit">
                                             <table class="outer" align="center">
                                                     <tr>
                                                             <td>
                                                                     <table width="100%" style="border-spacing: 0;">
                                                                        <tr>
                                                                                    <td style="background-color:#F06C00;padding: 10px;text-align: center;">
                                                                                            <a href="'.base_url().'"><img style="filter:brightness(100) contrast(100%) sepia(99) grayscale(1)" src="'.$logo.'" width="120" alt="logo" /></a>
                                                                                    </td>
                                                                            </tr>
                                                                     </table>
                                                             </td>
                                                     </tr>
                                                     <tr>
                                                             <td class="content" style="text-align:left;">
                                                                    <h3>Hello ,</h3>
                                                                            <table align="center" class="reset" style="padding: 20px 0px;">
                                                                                    <tr>
                                                                                        <td>Title</td>
                                                                                        <td>:</td>
                                                                                        <td>'.$title.'</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Name</td>
                                                                                        <td>:</td>
                                                                                        <td>'.$customerData['firstname'].' '.$customerData['lastname'].'</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Email</td>
                                                                                        <td>:</td>
                                                                                        <td>'.$customerEmail.'</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Message</td>
                                                                                        <td>:</td>
                                                                                        <td>'.$messageData.'</td>
                                                                                    </tr>
                                                                            </table> 
                                                                            <p>
                                                                            <br/>
                                                                            <br/>Regards,
                                                                            <br/>'.$customerData['firstname'].' '.$customerData['lastname'].'

                                                                    </p>
                                                             </td>
                                                     </tr>
                                                     <tr>
                                                             <td>
                                                                    <table width="100%" style="border-spacing: 0;">
                                                                            <tr>
                                                                                    <td class="footer" style="border-top:1px solid #f2f2f2;padding: 15px;text-align:left;"> 
                                                                                            <p style="font-size: 12px;color: #a7a7a7;">
                                                                                                   Copyright © 2020 <a style="color: #a7a7a7;" href="'.base_url().'">Mowneh.</a> All Rights Reserved
                                                                                            </p>

                                                                                    </td>
                                                                                    <td style="border-top:1px solid #f2f2f2;padding: 15px;text-align:right;">
                                                                                            <a href="'.base_url().'"><img src="'.base_url('image/cache/siteInfo/2c0523d0dad5f48f434a75ab2136b33c-140x45.png').'" width="80" alt="logo"></a>
                                                                                            <br/>


                                                                                    </td>
                                                                            </tr>
                                                                            <tr>
                                                                                    <td colspan="2" style="padding: 5px; background-color: #F06C00;">
                                                                                    </td>
                                                                            </tr>
                                                                    </table>

                                                             </td>
                                                     </tr>
                                             </table>
                                     </div>
                             </center>
                            </body>
                            </html>';
        return $message;
    }
   
    public function getCountryCode_get(){
                $status      =   parent::HTTP_OK;
                $message     =   lang('Success'); 
                $data       =   array();
                $data[0]['countryCode']         =   '91';
                $data[0]['countryCodedisply']   =   '+91';
                $data[0]['country']             =   'India';
                $data[0]['numberCount']         =   '10';
                
                $data[1]['countryCode']         =   '974';
                $data[1]['countryCodedisply']   =   '+974';
                $data[1]['country']             =   'Qatar';
                $data[1]['numberCount']         =   '8';
                
            
        
            $result['status']       = $status;
            $result['message']      = $message;
            $result['data']         = $data;
            $this->response($result, $status);
    }
}
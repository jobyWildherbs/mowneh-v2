<?php

/**
 * Description of ContentPage
 *
 * @author wildherbs-user
 */
class Information extends Site_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('GeneralModel', 'gm');
        $this->load->model('InformationModel', 'im');
    }

    public function index($urlKey = '') {
        
        if(!$urlKey){
            redirect(base_url());
        }
        $content = $this->im->selectInfo($urlKey, $this->siteLanguageID);
         //echo "<pre>"; print_r($content);exit;
        if(!is_countable($content))
            redirect(base_url());
        $this->data['content']['title'] = $content['title'];
        $this->data['content']['content'] = $content['description'];
        
            $this->data['metaTitle']    =   $content['metaTitle'];
            $this->data['metaDescription']    =   $content['metaDescription'];
            $this->data['metaKeyword']    =   $content['metaKeyword'];
            
        $this->render('frontEnd/contentpage/content');
    }

    public function _remap($method) {
        $param_offset = 2;

        if (!method_exists($this, $method)) {
            $param_offset = 1;
            $method = 'index';
        }
// Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
// Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

}

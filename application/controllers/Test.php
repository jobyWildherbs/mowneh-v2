<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends site_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('GeneralModel', 'gm');
        $this->load->library('order');
    }

    public function index() {
//        $category = $this->gm->getCategoryTree();
//        echo "<pre>";
//        print_r($category);
//        $array = $this->getChildrenFor($category, 0);
//        print_r($array);
        
        $this->order->setOrderData($this->siteLanguageID);
    }

    function getChildrenFor($ary, $id) {
        $results = array();

        foreach ($ary as $el) {
            if ($el['parentID'] == $id) {
                $copy = $el;
                unset($copy['children']); // remove child elements
                $results[] = $copy;
            }
            if (array_key_exists('children', $el) && is_countable($el['children']) && ($children = $this->getChildrenFor($el['children'], $id)) !== FALSE) {
                $results = array_merge($results, $children);
            }
        }

        return count($results) > 0 ? $results : FALSE;
    }
    
    function getTimeZone(){
        $tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
       
    }

}

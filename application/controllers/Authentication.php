<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $siteAuthentication = $this->session->userdata('SITE_AUTH_PASSWORD');
        if($siteAuthentication=='Yes'){
            redirect('Home', 'refresh');
        }
    }
    
    public function index() {
        if($this->input->post()){
            $this->form_validation->set_rules('password', 'Password', 'required');
            if($this->form_validation->run()===TRUE)
            { 
                if($this->input->post('password')== SITE_AUTH_PASSWORD){
                    $siteAuth['SITE_AUTH_PASSWORD'] =   'Yes';
                    $this->session->set_userdata($siteAuth);
                    redirect('Home', 'refresh');
                }
            }
        }
        $this->load->view('Authentication');
    }
}
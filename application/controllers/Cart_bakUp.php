<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends Site_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('CustomerLogin');
        $this->load->model('CartModel', 'cartModel');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('ProductModel', 'pm');
        $this->load->model('AccountModel', 'accountModel');
        $this->load->model('WarehouseModel', 'wm');
        $this->load->library('Order');
        $this->lang->load('site', $this->siteLanguage);
        $this->lang->load('error', $this->siteLanguage);
        $this->customerID = $this->customer->getID();
    }

    public function index() {
        $cartItems = $this->carts->getProducts($this->siteLanguageID);
        list($width, $height) = $this->getImageDimension('cart_image_dimen');
        foreach ($cartItems as $key => $cart) {
            $cartItems[$key]['price'] = $this->currency->format($cart['price'], $this->siteCurrency);
            $cartItems[$key]['image'] = $this->setSiteImage('product/' . $cart['image'], $width, $height);
        }
        $this->data['cartItems'] = $cartItems;
        //echo "<pre>"; print_r($cartItems);exit;
        //if (!empty($cartItems)) {
        //$this->data['cartItems']['subTotal'] = $this->currency->format($this->carts->getSubTotal(), $this->siteCurrency);
        //}
        if (!empty($cartItems)) {
            $allTotals = $this->carts->getTotalItemsList($this->siteLanguageID);
            foreach ($allTotals as $key => $tolVal) {
                $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency);
            }
        }
        //echo "<pre>";print_r($allTotals);
        $this->data['subTotal'] = $allTotals;

        $this->render('frontEnd/cart/Cart');
    }

    public function add() {
        $json = array();
        $productID = ($this->input->post('product_id')) ? $this->input->post('product_id') : 0;
        $quantity = ($this->input->post('quantity')) ? $this->input->post('quantity') : 1;
        if ($productID > 0) {
            $productInfo = $this->generalModel->getTableValue('quantity,minimum,status,dateAvailable', 'ec_product', array("productID" => $productID, 'status' => 'Active'));
            if ($productInfo['status'] != 'Active') {
                $json['error']['status'] = $this->lang->line('product_status_error');
            }
            if ($productInfo['dateAvailable'] != '0000-00-00' && $productInfo['dateAvailable'] > date('Y-m-d')) {
                $json['error']['dateAvailable'] = sprintf($this->lang->line('product_date_available_error'), date('jS, F Y', strtotime($productInfo['dateAvailable'])));
            }
            if ($quantity < $productInfo['minimum']) {
                $quantity = $productInfo['minimum'];
            }
            if ($productInfo['quantity'] < ($this->carts->getProductQuantity($productID) + $quantity)) {
                $json['error']['quantity'] = $this->lang->line('quantity_not_available');
            }

            if (!$json) {
                $this->carts->add($productID, $quantity);
                if ($this->carts->hasProducts()) {
                    $json['success'] = $this->lang->line('cart_added');
                    $json['totalProducts'] = $this->carts->countProducts();
                    $json['subtoal'] = $this->currency->format($this->carts->getSubTotal(), $this->siteCurrency);
                    $json['shipping'] = $this->currency->format(0, $this->siteCurrency);
                    $json['totalAmout'] = $this->currency->format($this->carts->getTotal(), $this->siteCurrency);
                } else {
                    $json['error']['product'] = $this->lang->line('product_add_error');
                }
            }
        } else {
            $json['error']['product'] = $this->lang->line('product_add_error');
        }

        echo json_encode($json);
    }

    public function removeFromCart() {
        $json = array();
        $totalProducts = $this->carts->countProducts();
        $cartID = ($this->input->post('key')) ? $this->input->post('key') : 0;
        if ($cartID > 0) {
            $productID = ($this->input->post('productID')) ? $this->input->post('productID') : 0;
            $this->carts->remove($productID);
            $newProductCount = $this->carts->countProducts();
            if ($totalProducts > $newProductCount) {
                $json['success'] = $this->lang->line('cart_removed');
                $json['totalProducts'] = $this->carts->countProducts();
                $json['totalAmout'] = $this->currency->format($this->carts->getTotal(), $this->siteCurrency);
            } else {
                $json['error']['product'] = $this->lang->line('product_add_error');
            }
        } else {
            $json['error']['product'] = $this->lang->line('product_remove_error');
        }
        echo json_encode($json);
    }

    public function updateCart() {
        $json = array();
        $productID = ($this->input->post('key')) ? $this->input->post('key') : 0;
        $quantity = ($this->input->post('quantity')) ? $this->input->post('quantity') : 1;
        if ($productID > 0) {
            $customerID = $this->customer->getId();
            $apiID = ($this->session->userdata('api_id')) ? (int) $this->session->userdata('api_id') : 0;
            $sessionID = $this->session->session_id;
            $params = array(
                'customerID' => $customerID,
                'apiID' => $apiID,
                'sessionID' => $sessionID,
                'productID' => $productID
            );
            if ($productID) {
                $productInfo = $this->generalModel->getTableValue('quantity,minimum,status,dateAvailable', 'ec_product', array("productID" => $productID, 'status' => 'Active'));
                if ($productInfo['status'] != 'Active') {
                    $json['error']['status'] = $this->lang->line('product_status_error');
                }
                if ($productInfo['dateAvailable'] != '0000-00-00' && $productInfo['dateAvailable'] > date('Y-m-d')) {
                    $json['error']['dateAvailable'] = sprintf($this->lang->line('product_date_available_error'), date('jS, F Y', strtotime($productInfo['dateAvailable'])));
                }
                if ($quantity < $productInfo['minimum']) {
                    $quantity = $productInfo['minimum'];
                }
                if ($productInfo['quantity'] < $quantity) {
                    $json['error']['quantity'] = $this->lang->line('quantity_not_available');
                }

                if (!$json) {
                    $totalProducts = $this->carts->countProducts();
                    $this->carts->update($productID, $quantity);
                    $newProductCount = $this->carts->countProducts();
                    if ($newProductCount != $totalProducts) {
                        $json['success'] = $this->lang->line('cart_updated');
                        $json['totalProducts'] = $this->carts->countProducts();
                        $json['subtoal'] = $this->currency->format($this->carts->getSubTotal(), $this->siteCurrency);
                        $json['shipping'] = $this->currency->format(0, $this->siteCurrency);
                        $json['totalAmout'] = $this->currency->format($this->carts->getTotal(), $this->siteCurrency);
                    } else {
                        $json['error']['product'] = $this->lang->line('product_add_error');
                    }
                }
            }
        }
        echo json_encode($json);
    }

    public function checkOut() {
        if ($this->carts->hasProducts() == 0) {
            redirect('Cart', 'refresh');
        }
        if ($this->customerID == "" || $this->customerID == 0) {
            $this->session->set_userdata('moonehCallBackURL', 'Cart/checkOut');
            redirect('Login', 'refresh');
        }

        if ($this->input->post()) { // Address Save
            $customerID = $this->customer->getID();
            $countryID = 173;
            // form validation 
            //echo"hgfhg";exit;
            $this->form_validation->set_rules('firstname', 'First Name', 'required');
            $this->form_validation->set_rules('lastname', 'Last Name', 'required|xss_clean');
            $this->form_validation->set_rules('address1', 'Address1', 'required|xss_clean');
            $this->form_validation->set_rules('address2', 'Address2', 'required|xss_clean');
            $this->form_validation->set_rules('city', 'city', 'required|xss_clean');
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|min_length[8]|max_length[8]', array(
                'min_length' => 'Please provide a valid phone number',
                'max_length' => 'Please provide a valid phone number'
            ));

            if ($this->form_validation->run() === TRUE) {

                $resData['nonXss'] = array(
                    'customerID' => $customerID,
                    'firstname' => $this->input->post('firstname'),
                    'lastname' => $this->input->post('lastname'),
                    'phone' => $this->input->post('phone'),
                    'address1' => $this->input->post('address1'),
                    'address2' => $this->input->post('address2'),
                    'city' => $this->input->post('city'),
                    'postcode' => $this->input->post('postcode'),
                    'countryID' => $countryID
                );
                $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

                $result = $this->gm->insertValue('ec_address', $resData['xssData']);
                $this->session->set_userdata('moonehDeliveryAddressID', $result);
            }
        }


        $cartItems = $this->carts->getProducts($this->siteLanguageID);
        list($width, $height) = $this->getImageDimension('cart_image_dimen');
        foreach ($cartItems as $key => $cart) {
            $cartItems[$key]['price'] = $this->currency->format($cart['price'], $this->siteCurrency);
            $cartItems[$key]['image'] = $this->setSiteImage('product/' . $cart['image'], $width, $height);
        }
        $this->data['cartItems'] = $cartItems;
//        if (!empty($cartItems)) {
//            $this->data['cartItems']['subTotal'] = $this->currency->format($this->carts->getSubTotal(), $this->siteCurrency);
//        }
        if (!empty($cartItems)) {
            $allTotals = $this->carts->getTotalItemsList();
            foreach ($allTotals as $key => $tolVal) {
                $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency);
            }
        }
        $this->data['subTotal'] = $allTotals;
        $this->data['content'] = $this->accountModel->selectAddress($this->customerID);

        $moonehDeliveryAddressID = $this->session->userdata('moonehDeliveryAddressID');
        if ($moonehDeliveryAddressID) {
            $this->data['moonehDeliveryAddress'] = $this->generalModel->getTableValue('*', 'ec_address', 'addressID="' . $moonehDeliveryAddressID . '"', FALSE);
        }
        $this->data['stateList'] = $this->accountModel->selectStateList($this->siteLanguageID, $this->customerID);
        $this->data['addressErr'] = $this->session->flashdata('addressErr');
        $this->data['countryList'] = $this->generalModel->getTableValue('countryID,name', 'ec_country', 'countryID="173" AND status="Active"', TRUE);
        $this->data['warehouseCity'] = $this->wm->getWarehouseCityInfo();
        $this->render('frontEnd/cart/CheckOut');
    }

    public function selectAddress() {
        $addressID = $_POST['id'];
        $address = $this->generalModel->getTableValue('*', 'ec_address', 'addressID=' . "$addressID", FALSE);
        echo json_encode($address);
    }

    public function sessionAddressKeep() {
        $addressID = $this->uri->segment(3);
        $this->session->set_userdata('moonehDeliveryAddressID', $addressID);
        redirect('Cart/checkOut', 'refresh');
    }

    public function payment() {
        // echo "<pre>"; print_r($this->session->userdata());
//        echo $this->session->userdata('moonehDeliveryAddressID'); exit;
        $moonehDeliveryAddressID = $this->session->userdata('moonehDeliveryAddressID');
        if ($moonehDeliveryAddressID == '') {
            $this->session->set_flashdata('addressErr', 'Need to select address');
            redirect('Cart/checkOut', 'refresh');
        }
        $paymentMethod = 3;
                    $confirm = $this->order->setOrderData($this->siteLanguageID, $this->siteCurrency, $paymentMethod);
                    if ($confirm) {
                        redirect('Cart/success', 'refresh');
                    } else {
                        redirect('Cart/failed', 'refresh');
                    }

//        if ($this->input->post()) {
//
//            $this->form_validation->set_rules('paymentMethod', 'Payment method', 'required');
//
//            if ($this->form_validation->run() === TRUE) {
//                if ($this->input->post('paymentMethod')) {
//                    $paymentMethod = $this->input->post('paymentMethod');
//                    $confirm = $this->order->setOrderData($this->siteLanguageID, $this->siteCurrency, $paymentMethod);
//                    if ($confirm) {
//                        redirect('Cart/success', 'refresh');
//                    } else {
//                        redirect('Cart/failed', 'refresh');
//                    }
//                }
//            }
//        }
        $this->render('frontEnd/cart/Payment');
    }

    public function success() {
        if (@$this->session->userdata['orderID'] != "") {
            $orderProduct = $this->generalModel->getTableValue('productID,quantity', 'ec_order_product', array("orderID" => $this->session->userdata['orderID']), TRUE);
            if ($orderProduct) {
                foreach ($orderProduct as $key => $proVal) {
                    $this->cartModel->decreaseProductQuentity($proVal['productID'], $proVal['quantity']);
                }
            }
        }
        $total = $this->carts->getTotal();
        $customerID = $this->customerID;
        $from_email = "mooneh@gmail.com";
        $message = "Your order has been successfully placed. Your total amout:" . $total;
        $customerEmil = $this->generalModel->getTableValue('email', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
        $to_email = $customerEmil['email'];

        //Load email library 
        $this->load->library('email');
        $this->email->from($from_email, 'Mooneh');
        $this->email->to($to_email);
        $this->email->subject('Order Confirmation');
        if ($this->email->message($message)) {
            $this->carts->clear();
            unset($this->session->userdata['orderID']);
            unset($this->session->userdata['moonehDeliveryAddressID']);
        }
        $this->render('frontEnd/cart/success');
    }

    public function failed() {
        $this->render('frontEnd/cart/Failed');
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends Site_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('ProductModel', 'productModel');
        $this->load->model('GeneralModel', 'gm');
        $this->lang->load('site', $this->siteLanguage);
    }

    public function index() {
        $customerID = $this->customer->getID();
        $this->data['action'] = base_url('search');
        $this->form_validation->set_rules('search', 'Search Keyword', 'required|xss_clean');
        if ($this->form_validation->run() === TRUE) {
            $keyWord = $this->input->post('search');
            $category = "";
            if($this->input->post('category')){
                $category = $this->input->post('category');
            }
            $params = array(
                'category' => $category
            );
            $productList = $this->gm->getSearchResult($keyWord, $this->siteLanguageID, $customerID,$params);
            list($width, $height) = $this->getImageDimension('list_product_dimen');
            foreach ($productList as $key => $productData) {
                $productOffer    = $this->gm->getTableValue('*', 'ec_premotion_to_product', 'productID = '.$productData['productID'].' AND priority=(select max(priority) from ec_premotion_to_product)  AND dateStart<=NOW() AND dateEnd>=NOW()', FALSE);
                        //print_r($this->db->last_query()); exit;
                        //echo "<pre>"; print_r($productOffer); echo "<pre>"; echo "1";
                        if($productOffer){ 
                             $productDiscount= $productOffer;
                             $discountPrice = $productOffer['offePrice'];

                        } 
                        else{
                $productDiscount = $this->productModel->productDiscount($productData['productID']);
                $discountPrice = "";
                if (!empty($productDiscount)) {
                    if ($productDiscount['type'] == 'fixed') {
                        $discountPrice = $productData['price'] - $productDiscount['price'];
                        $offPersentage = ($productDiscount['price'] / $productData['price']) * 100;
                        $productList[$key]['offPersentage'] = $offPersentage;
                    } else {
                        $offPersentage = $productDiscount['price'];
                        $productList[$key]['offPersentage'] = $offPersentage;
                        $discountPrice = $productData['price'] - ($productData['price'] * $productDiscount['price'] / 100);
                    }
                }
            }
                if ($discountPrice != '')
                    $productList[$key]['discountPrice'] = $this->currency->format($discountPrice, $this->siteCurrency,'',TRUE,PHP_ROUND_HALF_DOWN);

                $productList[$key]['price'] = $this->currency->format($productData['price'], $this->siteCurrency);
                $productList[$key]['image'] = $this->setSiteImage("product/" . $productData['image'], $width, $height);
                //exit;
                $productList[$key]['productDiscount'] = $productDiscount;


                $now = time(); // or your date as well
                $dateAdded = strtotime($productData['dateAdded']);
                $datediff = $now - $dateAdded;
                if (round($datediff / (60 * 60 * 24)) < 7) {
                    $productList[$key]['new'] = 'new';
                }
            }
            $this->data['productList'] = $productList;
        }
        $this->render('frontEnd/product/search');
    }
}

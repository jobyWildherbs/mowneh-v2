<?php
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('CustomerLogin');
        $this->load->library('Apisupport');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->library('form_validation');
        //$this->lang->load('site', $this->siteLanguage);
    }
    
    public function userRegister_post(){
       
        if ($this->input->post()) {
            $config = [
                        [
                                'field' => 'firstname',
                                'label' => 'First Name',
                                'rules' => 'required',
                                'errors' => [
                                        'required' => 'Please enter First Name',
                                       
                                ],
                        ],
                                [
                                        'field' => 'email',
                                        'label' => 'Email',
                                        'rules' => 'required|valid_email|xss_clean|callback_checkEmailExist',
                                        'errors' => [
                                                'required'      => 'Please enter email',
                                                'valid_email'   => 'Please enter valid email',
                                                'checkEmailExist' => 'Account already exists with this email.',

                                        ],
                                ],
                                [
                                        'field' => 'telephone',
                                        'label' => 'Phone',
                                        'rules' => 'required|min_length[8]|max_length[8]|callback_checkPhoneExist',
                                        'errors' => [
                                                'required'      => 'Please enter Phone number',
                                                'min_length'    => 'Only 8 digit allowed',
                                                'max_length'    => 'Only 8 digit allowed',
                                                'checkPhoneExist' => 'This number is already associated with another account',

                                        ],
                                ],
                                [
                                        'field' => 'password',
                                        'label' => 'Password',
                                        'rules' => 'required',
                                        'errors' => [
                                                'required' => 'Please enter password',
                                                ],
                                ]
                    ];

                $data = $this->input->post();
                $this->form_validation->set_data($data);
                $this->form_validation->set_rules($config);
            if ($this->form_validation->run() === TRUE) {
                $resData['nonXss'] = array(
                    'firstname'     => $this->input->post('firstname'),
                    'lastname'      => $this->input->post('lastname'),
                    'email'         => $this->input->post('email'),
                    'telephone'     => $this->input->post('telephone'),
                    'password'      => $this->input->post('password'),
                    'status'        => 'Notverified'
                );
                $resData['xssData']                 = $this->security->xss_clean($resData['nonXss']);
                $passwordCreate                     = $this->customerlogin->createCredentials($resData['xssData']['email'], $resData['xssData']['password']);
                $resData['xssData']['password']     = $passwordCreate['password'];
                $resData['xssData']['salt']         = $passwordCreate['salt'];
                $resData['xssData']['dateAdded']    = date('Y-m-d H:i');
                $resData['xssData']['ip']           = $this->input->ip_address();
                
                $userID = $this->generalModel->insertValue('ec_customer', $resData['xssData']);
                
                if($userID){
                    
                    //setting token to mobile App
                    $this->apisupport->updateToken($userID);
                    //Token end
                    
                    $customer          = $this->generalModel->getTableValue('customerID,firstname,lastname,email,telephone,status','ec_customer',array('customerID'=> $userID));
                    $customerData        = array(
                        'moonehcustomerID'          => $customer['customerID'],
                        'moonehFirstname'           => $customer['firstname'],
                        'moonehLastname'            => $customer['lastname'],
                        'moonehEmail'               => $customer['email'],
                        'telephone'                 => $customer['telephone'],
                        'moonehStatus'              => $customer['status'],
                        'moonehLoggedIn'            => TRUE
                    );
                    
                   $token = $this->apisupport->getToken($userID);
                
                    
                    $message = "Registration completed successfully.Please check your mail to activate your account..";
                    $status         = parent::HTTP_OK;
                    $data        =   $customerData;
                    
                    //echo $from_email; exit;
                     $otp 		= randomNumber();
                    $resOtpData['otp']            =   $otp;
                    $resOtpData['customerID']     =   $userID;
                    $resOtpData['otpType']        =   'userAuthentication';
                    $resOtpData['otpTime']        =   date("Y-m-d H:s:i");
                    $resOtpData['addedDate']      =   date("Y-m-d H:s:i");
                    $otpID   =   $this->generalModel->insertValue('ec_otp',$resOtpData);
                    if($otpID){
                        $otpData          = $this->generalModel->getTableValue('otp','ec_otp',array('customerID'=> $userID,'status'=>'Active'));
                        $otp    = $otpData['otp'];
                        
                        $mailMessage =  "Dear ".$this->input->post('firstname')." ".$this->input->post('lastname')."</br>";
//                        $mailMessage    .= "Please click below link to activate your account </br>";
//                        $mailMessage    .= base_url()."Register/activateAccount/".$userID."</br>";
                        $mailMessage    .= "Your mowneh registration OTP is  ".$otp."</br>";
                        $mailMessage    .= "Thankyou"; 
                        $to_email = $this->input->post('email');
                        $from_email =   $this->getSettingValue('email');
                         //Load email library 
                        $config['protocol'] = 'sendmail';
                        $config['mailtype'] = 'html';
                        $this->load->library('email');
                        $this->email->initialize($config);
                        $this->email->from($from_email, 'Mooneh');
                        $this->email->to($to_email);
                        $this->email->subject('Mooneh verify your email');
                        $this->email->message($mailMessage);
                        $this->email->send();
                    }
                    
                }
            } 
            else{
              
                $msgvalue    =   '';
                $i  =   1;
                foreach($this->form_validation->error_array() as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
               //$message     =   'validation error';
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $message     =   $msgvalue; 
               $data        =   '';
              // $token       =   '';
               
            }
        }
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        //$result['token']        = $token;
       // $status                 = 'REST_Controller::'.$status;
        //$result = $this->getResposnseResult($status, $message, $data);
        $this->response($result, $status);
    }

    public function activeOtp_get() {
        $data           = array();
        $valErr         =   0;
        $valErrMsg      =   array();
        $otp          =   $this->security->xss_clean($this->input->get('otp'));
        $userID         =   $this->security->xss_clean($this->input->get('userID'));
        $token      =   '';
        if($otp){
            if (!is_numeric($otp)){
                $valErr =   1;
                $valErrMsg['otp']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['otp']    =  'Otp missing';   
        }
        if($userID){
            if (!is_numeric($userID)){
                $valErr =   1;
                $valErrMsg['userID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['userID']    =  'languageID missing';   
        }
        if($valErr==0){
            $otpDet = $this->generalModel->getTableValue('otpID,otp', 'ec_otp', 'otp =' . "$otp" . ' AND customerID=' . "$userID" . ' AND otpType="userAuthentication" AND status="Active"');
            if ($otpDet) {
                $postOtpDet = $this->generalModel->getTableValue('otpID,otp', 'ec_otp', 'customerID=' . "$userID" . ' AND otpType="userAuthentication" AND otp=' . "$otp" . ' AND status="Active"');
                if ($otpDet['otpID'] == $postOtpDet['otpID']) {
                    $value['status'] = 'Active';
                    $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $userID, $value);
                    $otpVal['status'] = 'used';
                    $this->generalModel->updateTableValues('ec_otp', 'otpID=' . $postOtpDet['otpID'], $otpVal);
                    $customer = $this->generalModel->getTableValue('customerID,firstname,lastname,email,status', 'ec_customer', 'customerID =' . "$userID" . ' AND status="Active"');

                    $customerData = array(
                        'moonehcustomerID' => $customer['customerID'],
                        'moonehFirstname' => $customer['firstname'],
                        'moonehLastname' => $customer['lastname'],
                        'moonehEmail' => $customer['email'],
                        'moonehStatus' => $customer['status'],
                        'moonehLoggedIn' => TRUE
                    );
                    $token = $this->apisupport->getToken($userID);
                    $message = "Account Activited";
                    $status         = parent::HTTP_OK;
                    $data        =   $customerData;
                }
            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   'Invalid OTP'; 
                $data        =   '';
                
            }
        }else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
            $message    =   $msgvalue;
            $status      =   parent::HTTP_BAD_REQUEST;
        }
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $result['token']        = $token;
        $this->response($result, $status);
    }
    
    public function activateAccount($userID){
        $value['status'] = 'Active';
        $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $userID, $value);
        $customer = $this->generalModel->getTableValue('customerID,firstname,lastname,email,status', 'ec_customer', 'customerID =' . "$userID" . ' AND status="Active"');

                $customerData = array(
                    'moonehcustomerID' => $customer['customerID'],
                    'moonehFirstname' => $customer['firstname'],
                    'moonehLastname' => $customer['lastname'],
                    'moonehEmail' => $customer['email'],
                    'moonehStatus' => $customer['status'],
                    'moonehLoggedIn' => TRUE
                );
                $this->session->set_userdata($customerData);
                redirect('Home', 'refresh');
    }
    
    public function emailPhonenumberCheck_post(){
        if ($this->input->post()) {
            $field     = $this->input->post('field'); //email or telephone
            $value     = $this->input->post('value');
            $return =   $this->generalModel->getFieldValue($field, 'ec_customer',$field.'="'.$value.'" AND status!="Deleted"');
            if($return){
                $data       =   TRUE;
                $message    =   'Already exist';
            }else{
                 $data          =   FALSE;
                 $message       =   '';
            }
            $status         = parent::HTTP_OK;
        }
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    function checkEmailExist(){
        $email = $this->input->post('email');
        $otpDet = $this->generalModel->getTableValue('customerID', 'ec_customer', 'email ="'.$email.'" AND status!="Deleted"',FALSE);
        if($otpDet){
            return FALSE;   
        }   
        else{
             return TRUE;  
        }
    }
    
     function checkPhoneExist(){
        $telephone = $this->input->post('telephone');
        $otpDet = $this->generalModel->getTableValue('customerID', 'ec_customer', 'telephone ="'.$telephone.'" AND status!="Deleted"',FALSE);
        if($otpDet){
            return FALSE;   
        }   
        else{
             return TRUE;  
        }
    }
    
    public function resentOtp_get() {
        $data           = '';
        $valErr         =   0;
        $valErrMsg      =   array();
        $userID         =   $this->security->xss_clean($this->input->get('userID'));
        $token      =   '';
        $otp        =   '';
        if($userID){
            if (!is_numeric($userID)){
                $valErr =   1;
                $valErrMsg['userID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['userID']    =  'userID missing';   
        }
        if($valErr==0){
            $otpDet = $this->generalModel->getTableValue('otpID,otp', 'ec_otp', 'customerID=' . "$userID" . ' AND otpType="userAuthentication" AND status="Active"');
            $customer          = $this->generalModel->getTableValue('customerID,firstname,lastname,email,status','ec_customer',array('customerID'=> $userID,'status'=>'Notverified'));
            //echo "<pre>"; print_r($otpDet); exit;
            if($customer){
            if ($otpDet) {
                $otp 		= $otpDet['otp'];
            }else{
                    //echo $from_email; exit;
                    $otp 		= randomNumber();
                    $resOtpData['otp']            =   $otp;
                    $resOtpData['customerID']     =   $userID;
                    $resOtpData['otpType']        =   'userAuthentication';
                    $resOtpData['otpTime']        =   date("Y-m-d H:s:i");
                    $resOtpData['addedDate']      =   date("Y-m-d H:s:i");
                    $otpID   =   $this->generalModel->insertValue('ec_otp',$resOtpData); 
            }
            
             if($otp){
                        $from_email =   $this->getSettingValue('email');
                        $mailMessage =  "Dear ".$customer['firstname']." ".$customer['lastname']."</br>";
//                        $mailMessage    .= "Please click below link to activate your account </br>";
//                        $mailMessage    .= base_url()."Register/activateAccount/".$userID."</br>";
                        $mailMessage    .= "Your mowneh registration OTP is  ".$otp."</br>";
                        $mailMessage    .= "Thankyou"; 
                        $to_email = $customer['email'];
                         //Load email library 
                        $config['protocol'] = 'sendmail';
                        $config['mailtype'] = 'html';
                        $this->load->library('email');
                        $this->email->initialize($config);

                        $this->email->from($from_email, 'Mooneh');
                        $this->email->to($to_email);
                        $this->email->subject('Mooneh verify your email');
                        $this->email->message($mailMessage);
                        $this->email->send();
                        $message = "OTP Send Successfully";
                        $status         = parent::HTTP_OK;
                        $data        =   '';
                    }else{
                        $status      =   parent::HTTP_BAD_REQUEST;
                        $message    =   "";
                        $data       =   "";
                    }
            }else{
                        $status      =   parent::HTTP_BAD_REQUEST;
                        $message    =   "Invalid User";
                        $data       =   "";
                    }
        }else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
            $message    =   $msgvalue;
            $status      =   parent::HTTP_BAD_REQUEST;
        }
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }

}

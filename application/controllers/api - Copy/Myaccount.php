<?php
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';
defined('BASEPATH') OR exit('No direct script access allowed');
class Myaccount extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('Customer');
        $this->load->library('CustomerLogin');
        $this->load->library('form_validation');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('AccountModel', 'accountModel');
        $this->load->model('OrderModel', 'orderModel');
        $this->load->model('CustomerModel', 'customerModel');
        $this->load->model('WarehouseModel', 'warehouseModel');
        $this->load->model('ProductModel', 'productModel');
        $this->customerID = $this->customer->getID();
    }
    
    public function customerDetail_get(){
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        if($valErr==0){
            $userID         = $this->userID;
            $userDetails = $this->generalModel->getTableValue('customerID AS userID,firstname,lastname,email,telephone,token', 'ec_customer', 'customerID=' . "$userID", FALSE); //data for edit
 
            if($userDetails){
                $status      =   parent::HTTP_OK;
                $message     =   'Success'; 
                $data        =   $userDetails; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   'Sorry No Data Found'; 
                $data        =   '';
            }
            
        }
        else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
                $message    =   $msgvalue;
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $data        =   '';
               
            }
        $this->setHeaderToken(); 
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function addressList_get() {
        $data        =   array();
        $userID         = $this->userID;
        $addressList = $this->accountModel->selectMobAddress($userID);
        $address= $this->accountModel->selectAddressDefault($userID);
        
        $country    =   $this->generalModel->getTableValue('name', 'ec_country', 'countryID="173"', FALSE);
        if($addressList){
            foreach($addressList as $key=>$adderssArr){ 
                if($adderssArr['postcode']){
                      $postcode =   ", ".$adderssArr['postcode'];
                }else{
                    $postcode   =   "";
                }
              $addressList[$key]['fullAddress']   = $adderssArr['firstname']." ".$adderssArr['lastname']." ,".$adderssArr['address1']." ,".$adderssArr['city'].''.$postcode.",".$country['name'];
               $defaultAddress   =   "No";
              if($adderssArr['addressID']==$address['addressID']){
                  $defaultAddress   =   "Yes";
              }
               $addressList[$key]['defaultAddress']   =  $defaultAddress;
            }
                $status      =   parent::HTTP_OK;
                $message     =   'Success'; 
                $data        =   $addressList; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   'Sorry No Data Found'; 
                $data        =   array();
            }
        $this->setHeaderToken(); 
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function updateUser_put(){
         $config = [
                        [
                                'field' => 'firstname',
                                'label' => 'First Name',
                                'rules' => 'required',
                                'errors' => [
                                        'required'      => 'First Name missing',
                                ],
                        ],
                        [
                                'field' => 'lastname',
                                'label' => 'Last Name',
                                'rules' => 'required',
                                'errors' => [
                                        'required'      => 'Last Name missing',
                                ],
                        ],
                        [
                                'field' => 'telephone',
                                'label' => 'Phone Number',
                                'rules' => 'required|min_length[8]|max_length[8]|numeric',
                                'errors' => [
                                        'required'      => 'Phone Number missing',
                                        'min_length'    => 'Minimum 8 digit',
                                        'max_length'    => 'Maximum  8 digit',
                                        'numeric'       => 'Only numbers allowed',
                                        
                                ],
                        ],
                       
                    ];
        $data = $this->put();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() === TRUE) 
        {
            $userID         = $this->security->xss_clean($this->userID);
            $resData['nonXss'] = array(
                'firstname' => $this->put('firstname'),
                'lastname' => $this->put('lastname'),
                'telephone' => $this->put('telephone'),
                    //'status' => $this->input->post('status')
            );
           
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
            $resData['xssData']['dateAdded'] = date('Y-m-d H:i');
            
            $updateResult = $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $userID, $resData['xssData']);
           
            if($updateResult)
                $userDetails = $this->generalModel->getTableValue('customerID AS userID,firstname,lastname,email,telephone,token', 'ec_customer', 'customerID=' . "$userID", FALSE); //data for edit
          
            if($updateResult){
                $status      =   parent::HTTP_OK;
                $message     =   'Success'; 
                $data        =   $userDetails; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   'Sorry No Data Found'; 
                $data        =   '';
            }
            
        }
        else{
                $msgvalue    =   '';
                $i  =   1;
                foreach($this->form_validation->error_array() as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
                $message    =   $msgvalue;
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION; 
               $data        =   '';
               
            }
        $this->setHeaderToken(); 
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $status                 =   'REST_Controller::'.$status;
        $this->response($result, $status);
    }
    
    public function userOrder_get(){
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        if($valErr==0)
        {
            $language   =   $this->generalModel->getTableValue('directory', 'ec_language', 'languageID=' . $languageID, FALSE); 
            $this->lang->load('app', $language['directory']);
            $userID         = $this->userID;
            
            $params =   array();
            $params['customerID']   =   $userID;
            $params['languageID']   =   $languageID;
            $params['myAcc']        =   'Yes';
            $orderList = $this->orderModel->selectOrderList($params);
            //echo "<pre>"; print_r($orderList); exit;
            foreach ($orderList as $key => $row) {
                if (isset($row['total']))
                    $orderList[$key]['total'] = $this->currency->format($row['total'], $this->siteCurrency);
                
                if ($row['paymentMethod'] == 3){ 
                    $orderList[$key]['paymentMethod']   =   lang('Online_Payment');
                }
                elseif ($row['paymentMethod'] == 2) {
                    $orderList[$key]['paymentMethod']   = lang('Cash_On_Delivery'); 
                    
                } elseif ($row['paymentMethod'] == 1) {
                    $orderList[$key]['paymentMethod']   =   lang('Free_Checkout');      
                }
            } 
            
          
            if($orderList){
                $status      =   parent::HTTP_OK;
                $message     =   'Success'; 
                $data        =   $orderList; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   'No Order Yet !'; 
                $data        =   '';
            }
            
        }
        else{
                $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
                $message    =   $msgvalue;
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $data        =   '';
               
            }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
     public function userOrderDetails_get(){
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $orderID     =   $this->security->xss_clean($this->input->get('orderID')) ? (int) $_GET['orderID'] : '';
        if($orderID){
            if (!is_numeric($orderID)){
                $valErr =   1;
                $valErrMsg['orderID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['orderID']    =  'orderID missing';   
        }
        if ($valErr==0) 
        {
            $userID          = $this->userID;
            
            $params     =   array();
            $orderData  =   array();
            $orderDetails = $this->orderModel->selectDetails($userID, $orderID);
            //echo "<pre>"; print_r($orderDetails); exit;
            if($orderDetails){
                $subTotal   =   $this->orderModel->orderTotal($orderDetails[0]['orderID']);
                if($subTotal){
                     foreach ($subTotal as $key => $subVal) {
                         $subTotal[$key]['value']   =   $this->currency->format($subVal['value'], $this->siteCurrency);;
                     }
                }
                
                $orderData['orderID']       =    $orderDetails[0]['orderID'];
                $orderData['customerID']    =    $orderDetails[0]['customerID'];
                $orderData['invoiceNo']     =    $orderDetails[0]['invoiceNo'];
                $orderData['invoiceNo']     =    $orderDetails[0]['invoiceNo']; 
                if ($orderDetails[0]['paymentMethod'] == 3){ 
                        $orderData['paymentMethod']   =   lang('Online_Payment');
                    }
                    elseif ($orderDetails[0]['paymentMethod'] == 2) {
                        $orderData['paymentMethod']   = lang('Cash_On_Delivery'); 
                    } elseif ($orderDetails[0]['paymentMethod'] == 1) {
                        $orderData['paymentMethod']   =   lang('Free_Checkout');      
                    }
                $orderData['totalSum']      =    $this->currency->format($orderDetails[0]['totalSum'], $this->siteCurrency);
                $orderData['firstname']     =    $orderDetails[0]['paymentFirstname']; 
                $orderData['lastname']      =    $orderDetails[0]['paymentLastname'];
                $orderData['address']       =    $orderDetails[0]['paymentAddress1'].','.$orderDetails[0]['paymentAddress2'].','.$orderDetails[0]['paymentCity'].','.$orderDetails[0]['paymentPostcode'].','.$orderDetails[0]['paymentCountry'];
                $dateAdded  =   date_create($orderDetails[0]['dateAdded']);
                $orderData['dateAdded']     =    date_format($dateAdded,"d-m-Y"); 
                $orderData['subTotal']      =   $subTotal; 
                list($width, $height) = $this->getSettingValue('order_listing_dimen');
                foreach ($orderDetails as $key => $row) {
                    $orderData['product'][$key]['productID']    =   $row['productID'];  
                    $orderData['product'][$key]['name']         =   $row['name'];  
                    $orderData['product'][$key]['model']        =   $row['model'];
                    $orderData['product'][$key]['quantity']     =   $row['quantity']; 
                    $orderData['product'][$key]['sku']          =   $row['sku']; 
                    $orderData['product'][$key]['price']        =   $this->currency->format($row['price'], $this->siteCurrency); 
                    $orderData['product'][$key]['productTotal'] =   $this->currency->format($row['sumProd'], $this->siteCurrency);
                    $orderData['product'][$key]['image']        =   $this->setSiteImage('product/' . $row['image'], $width, $height);
                    
                }
            }
            if($orderData){
                $status      =   parent::HTTP_OK;
                $message     =   'Success'; 
                $data        =   $orderData; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   'No Order Yet !'; 
                $data        =   '';
            }
            
        }
        else{
                $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
                $message    =   $msgvalue;
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $data        =   '';
               
            }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function userWishList_get(){
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        
        if($valErr==0){ 
            $userID =   $this->userID;
            $wishListData   =   array();
            $wishList = $this->accountModel->selectWishList($languageID, $userID);
            list($width, $height) = $this->getImageDimension('list_product_dimen');
            //echo "<pre>"; print_r($wishList); exit;
            if($wishList){
                foreach ($wishList as $key => $row) {
                    $productDiscount = $this->productModel->productDiscount($row['productID']);
                    
                    $wishListData[$key]['productID'] = $row['productID'];
                    $wishListData[$key]['quantity'] = $row['quantity'];
                    $wishListData[$key]['image'] = $this->setSiteImage("product/" . $row['image'], $width, $height);
                    $wishListData[$key]['price'] = $this->currency->format($row['price'], $this->siteCurrency);
                    $wishListData[$key]['name'] = $row['name'];
                    $wishListData[$key]['dateAdded'] = $row['dateAdded'];
                    $wishListData[$key]['wishList'] = 'Yes';
                     $discountPrice = "";
                    $wishListData[$key]['offPersentage'] = '';
                    $wishListData[$key]['discountPrice'] = '';
                    if (!empty($productDiscount)) {
                        if ($productDiscount['type'] == 'fixed') {
                            $discountPrice = $row['price'] - $productDiscount['price'];
                            $offPersentage = ($productDiscount['price'] / $row['price']) * 100;
                            $wishListData[$key]['offPersentage'] = $offPersentage;
                        } else {
                            $offPersentage = $productDiscount['price'];
                            $wishListData[$key]['offPersentage'] = $offPersentage;
                            $discountPrice = $row['price'] - ($row['price'] * $productDiscount['price'] / 100);
                        }
                    }
                    if ($discountPrice != '')
                        $wishListData[$key]['discountPrice'] = $this->currency->format($discountPrice, $this->siteCurrency);
                    else 
                        $wishListData[$key]['discountPrice'] =   '';
                        
                }
            } 
          
            if($wishListData){
                $status      =   parent::HTTP_OK;
                $message     =   'Success'; 
                $data        =   $wishListData; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   'Sorry!! No data found'; 
                $data        =   '';
            }
            
        }
        else{
                $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
                $message    =   $msgvalue;
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION; 
               $data        =   '';
               $token      =   '';
               
            }
        $this->setHeaderToken(); 
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function deleteWishList_get() {
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $productID     =   $this->security->xss_clean($this->input->get('productID')) ? (int) $_GET['productID'] : '';
        
        if($productID){
            if (!is_numeric($productID)){
                $valErr =   1;
                $valErrMsg['productID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['productID']    =  'productID missing';   
        }
        
        if($valErr==0){
            $userID                 = $this->userID;
            $this->generalModel->deleteTableValues('ec_customer_wishlist', array('customerID' => $userID, 'productID' => $productID), FALSE);
            
            $status      =   parent::HTTP_OK;
            $message     =   'Success'; 
            $data        =   ''; 
        }else{
                $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
                $message    =   $msgvalue;
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $data        =   '';
               
            }
        $this->setHeaderToken();       
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function countryList_get(){
        $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        if($valErr==0){
            
            $countryList      = $this->warehouseModel->getTableValue('countryID,name', 'ec_country', 'countryID="173" AND status="Active"', TRUE);
          
            if($countryList){
                $status      =   parent::HTTP_OK;
                $message     =   'Success'; 
                $data        =   $countryList; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   'Sorry!! No data found'; 
                $data        =   '';
            }
            
        }
        else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
                $message    =   $msgvalue;
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $data        =   '';
               
            }
        $this->setHeaderToken();     
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function stateList_get(){
       $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        
        if($valErr==0){
            $stateList        = $this->accountModel->selectStateList($languageID, '');
          
            if($stateList){
                $status      =   parent::HTTP_OK;
                $message     =   'Success'; 
                $data        =   $stateList; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   'Sorry!! No data found'; 
                $data        =   '';
            }
            
        }
        else{
             $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
                $message    =   $msgvalue;
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $data        =   '';
               
            }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function warehouseCityList_get(){
          $valErr =   0;
        $valErrMsg  =   array();
        $data        =   '';
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
        
        if($valErr==0){ 
            $warehouseCity    = $this->warehouseModel->getWarehouseCityInfo();
          
            if($warehouseCity){
                $status      =   parent::HTTP_OK;
                $message     =   'Success'; 
                $data        =   $warehouseCity; 

            }else{
                $status      =   parent::HTTP_BAD_REQUEST;
                $message     =   'Sorry!! No data found'; 
                $data        =   '';
            }
            
        }
        else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
                $message    =   $msgvalue;
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $data        =   '';
            }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }


    public function addressAdd_post() {
        
        $config =  [
                        [
                            'field' => 'firstname',
                            'label' => 'firstname',
                            'rules' => 'required',
                            'errors' => [
                                    'required'      => 'First Name Missing',

                            ],
                        ],
                        [
                            'field' => 'lastname',
                            'label' => 'lastname',
                            'rules' => 'required',
                            'errors' => [
                                    'required'      => 'Last Name Missing',

                            ],
                        ],
                        [
                            'field' => 'address1',
                            'label' => 'address1',
                            'rules' => 'required',
                            'errors' => [
                                    'required'      => 'Address Line 1 Missing',

                            ],
                        ],
                        [
                            'field' => 'address2',
                            'label' => 'address2',
                            'rules' => 'required',
                            'errors' => [
                                    'required'      => 'Address Line 2 Missing',

                            ],
                        ],
                        [
                            'field' => 'city',
                            'label' => 'city',
                            'rules' => 'required',
                            'errors' => [
                                    'required'      => 'City Missing',

                            ],
                        ],
                        [
                            'field' => 'countryID',
                            'label' => 'countryID',
                            'rules' => 'required',
                            'errors' => [
                                    'required'      => 'Country Missing',

                            ],
                        ],
                        [
                            'field' => 'phone',
                            'label' => 'phone',
                            'rules' => 'required|min_length[8]|max_length[8]',
                            'errors' => [
                                    'required'      => 'Phone Number Missing',
                                    'min_length' => 'Please provide a valid phone number',
                                    'max_length' => 'Please provide a valid phone number'

                            ],
                        ]
                    ];
        $data = $this->input->post();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() === TRUE) 
        {
            
            if(is_numeric($this->input->post('city'))){
                $cityData = $this->generalModel->getTableValue('name', 'ec_city_detail', array("cityID" => $this->input->post('city'), 'languageID' => '1'), FALSE);
                $city   =   $cityData['name'];
            }else{
                $city   =   $this->input->post('city');
            }
            $userID             = $this->userID;
                $resData['nonXss'] = array(
                    'customerID'    => $userID,
                    'firstname'     => $this->input->post('firstname'),
                    'lastname'      => $this->input->post('lastname'),
                    'phone'         => $this->input->post('phone'),
                    'address1'      => $this->input->post('address1'),
                    'address2'      => $this->input->post('address2'),
                    'city'          => $city,
                    'postcode'      => $this->input->post('postcode'),
                    'countryID'     => $this->input->post('countryID')
                );
                $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
                $return = $this->generalModel->insertValue('ec_address', $resData['xssData']);
                    $checkDefault   =   $this->accountModel->checkDefault($userID);
                    if(empty($checkDefault)){
                        $existingAddress    =   $this->generalModel->getTableValue('addressID', 'ec_address', 'customerID=' . "$userID", FALSE);
                        if($existingAddress){
                            $resData = array(
                                'addressID' => $return
                            );
                            $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $userID, $resData);
                        }
                    }
     
                 
                $addressList = $this->accountModel->selectAddress($userID);
                $address= $this->accountModel->selectAddressDefault($userID);

                if($addressList){
                    foreach($addressList as $key=>$adderssArr){ 
                        $addressList[$key]['fullAddress']   = $adderssArr['firstname']." ".$adderssArr['lastname']." ,".$adderssArr['address1']." ,".$adderssArr['city']." ,".$adderssArr['postcode'];
                            $defaultAddress   =   "No";
                            if($adderssArr['addressID']==$address['addressID']){
                               $defaultAddress   =   "Yes";
                            }
                           $addressList[$key]['defaultAddress']   =  $defaultAddress;
                        }     
                }


                if ($return) {
                    $status      =   parent::HTTP_OK;
                    $message     =   'Address Added Successfully'; 
                    $data        =   $addressList; 
           
                } else {
                    $status      =   parent::HTTP_BAD_REQUEST;
                    $message     =   'Something Went Wrong'; 
                    $data        =   $addressList;
                }
            
        
        }
        else{
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $message     =   $this->form_validation->error_array(); 
               $data        =   array();
               
            }
        $this->setHeaderToken(); 
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function addressEdit_put() {
        $addressID     =   $this->security->xss_clean($this->input->get('addressID')) ? (int) $_GET['addressID'] : '';
        
        $config =  [
                        [
                            'field' => 'addressID',
                            'label' => 'addressID',
                            'rules' => 'required|numeric',
                            'errors' => [
                                    'required'      => 'addressID Missing',
                                    'numeric'       => 'Only numbers allowed',

                            ],
                        ],
                        [
                            'field' => 'firstname',
                            'label' => 'firstname',
                            'rules' => 'required',
                            'errors' => [
                                    'required'      => 'First Name Missing',

                            ],
                        ],
                        [
                            'field' => 'lastname',
                            'label' => 'lastname',
                            'rules' => 'required',
                            'errors' => [
                                    'required'      => 'Last Name Missing',

                            ],
                        ],
                        [
                            'field' => 'address1',
                            'label' => 'address1',
                            'rules' => 'required',
                            'errors' => [
                                    'required'      => 'Address Line 1 Missing',

                            ],
                        ],
                        [
                            'field' => 'address2',
                            'label' => 'address2',
                            'rules' => 'required',
                            'errors' => [
                                    'required'      => 'Address Line 2 Missing',

                            ],
                        ],
                        [
                            'field' => 'city',
                            'label' => 'city',
                            'rules' => 'required',
                            'errors' => [
                                    'required'      => 'City Missing',

                            ],
                        ],
                        [
                            'field' => 'countryID',
                            'label' => 'countryID',
                            'rules' => 'required',
                            'errors' => [
                                    'required'      => 'Country Missing',

                            ],
                        ],
                        [
                            'field' => 'phone',
                            'label' => 'phone',
                            'rules' => 'required|min_length[8]|max_length[8]',
                            'errors' => [
                                    'required'      => 'Phone Number Missing',
                                    'min_length' => 'Please provide a valid phone number',
                                    'max_length' => 'Please provide a valid phone number'

                            ],
                        ]
                    ];
        $data = $this->put();
        $data['addressID']  =   $addressID;;
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() === TRUE) 
        {
            $userID             = $this->userID;
        
             if(is_numeric($this->put('city'))){
                $cityData = $this->generalModel->getTableValue('name', 'ec_city_detail', array("cityID" => $this->put('city'), 'languageID' => '1'), FALSE);
                $city   =   $cityData['name'];
            }else{
                $city   =   $this->put('city');
            }
                $resData['nonXss'] = array(
                    'customerID' => $userID,
                    'firstname' => $this->put('firstname'),
                    'lastname' => $this->put('lastname'),
                    'phone' => $this->put('phone'),
                    'address1' => $this->put('address1'),
                    'address2' => $this->put('address2'),
                    'city' => $city,
                    'postcode' => $this->put('postcode'),
                    'countryID' => $this->put('countryID')
                );
                $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

                $return = $this->generalModel->updateTableValues('ec_address', 'addressID=' . $addressID, $resData['xssData']);
                $addressList = $this->accountModel->selectAddress($userID);
                $address= $this->accountModel->selectAddressDefault($userID);

                if($addressList){
                    foreach($addressList as $key=>$adderssArr){ 
                        $addressList[$key]['fullAddress']   = $adderssArr['firstname']." ".$adderssArr['lastname']." ,".$adderssArr['address1']." ,".$adderssArr['city']." ,".$adderssArr['postcode'];
                        $defaultAddress   =   "No";
                        if($adderssArr['addressID']==$address['addressID']){
                           $defaultAddress   =   "Yes";
                        }
                       $addressList[$key]['defaultAddress']   =  $defaultAddress;
                    }     
                }

                if ($return) {
                    $status      =   parent::HTTP_OK;
                    $message     =   'Address Updated Successfully'; 
                    $data        =   $addressList; 
           
                } else {
                    $status      =   parent::HTTP_BAD_REQUEST;
                    $message     =   'Something Went Wrong'; 
                    $data        =   $addressList;
                }
                
        }
        else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($this->form_validation->error_array() as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $message     =   $msgvalue; 
               $data        =   array();
               
            }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function changePassword_post() {
        $config = [
                        [
                                'field' => 'password',
                                'label' => 'password',
                                'rules' => 'required',
                                'errors' => [
                                        'required'      => 'New Password missing',

                                ],
                        ],
                        [
                                'field' => 'confirmPassword',
                                'label' => 'confirmPassword',
                                'rules' => 'required|matches[password]',
                                'errors' => [
                                        'required'      => 'Confirm Password missing',
                                        'matches'      => 'Confirm Password mismatch',

                                ],
                        ]
                    ];
        
        $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'required|matches[password]');
        $data = $this->input->post();
        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() === TRUE) 
        {
                $userID             = $this->userID;
                $resData['nonXss'] = array(
                    'password' => $this->input->post('password')
                );
                $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

                $passwordCreate = $this->customerlogin->resetCredentials($resData['xssData']['password']);
                //echo "<pre>"; print_r($passwordCreate); exit;
                $resData['xssData']['password'] = $passwordCreate['password'];
                $resData['xssData']['salt']     = $passwordCreate['salt'];
                //echo "<pre>";print_r($resData['xssData']['password']);exit;

                $return = $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $userID, $resData['xssData']);

                if ($return) {
                    $status      =   parent::HTTP_OK;
                    $message     =   'Password changed Successfully'; 
                    $data        =   ''; 
           
                } else {
                    $status      =   parent::HTTP_BAD_REQUEST;
                    $message     =   'Failed To Update'; 
                    $data        =   '';
                }
            
        }else{
               $status      =   parent::HTTP_NON_AUTHORITATIVE_INFORMATION;
               $message     =   $this->form_validation->error_array(); 
               $data        =   '';
               
            }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
        
    }
    
    public function setAsDefault_get() {
        $valErr =   0;
        $userID         =   $this->userID;
        $valErrMsg  =   array();
        $data   =   array();
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        $addressID     =   $this->security->xss_clean($this->input->get('addressID'));
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
         if($addressID){
            if (!is_numeric($addressID)){
                $valErr =   1;
                $valErrMsg['addressID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['addressID']    =  'addressID missing';   
        }
        if($valErr==0){
            $userID = $this->userID;
            $resData = array(
                'addressID' => $addressID
            );
            $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $userID, $resData);
            $addressList = $this->accountModel->selectAddress($userID);
            $address= $this->accountModel->selectAddressDefault($userID);
        
        if($addressList){
            foreach($addressList as $key=>$adderssArr){ 
                $addressList[$key]['fullAddress']   = $adderssArr['firstname']." ".$adderssArr['lastname']." ,".$adderssArr['address1']." ,".$adderssArr['city']." ,".$adderssArr['postcode'];
                    $defaultAddress   =   "No";
                    if($adderssArr['addressID']==$address['addressID']){
                       $defaultAddress   =   "Yes";
                    }
                   $addressList[$key]['defaultAddress']   =  $defaultAddress;
                }     
            }
            $status      =   parent::HTTP_OK;
            $message     =   'Default Address Set Successfully'; 
            $data        =   $addressList;
        }else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($this->form_validation->error_array())){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
            $message    =   $msgvalue;
            $status      =   parent::HTTP_BAD_REQUEST;
        }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
    
    public function deleteAddress_get() {
        $valErr =   0;
        $valErrMsg  =   array();
        $data   =   array();
        $languageID     =   $this->security->xss_clean($this->input->get('languageID')) ? (int) $_GET['languageID'] : $this->defalutLanguage;
        $addressID     =   $this->security->xss_clean($this->input->get('addressID'));
        if($languageID){
            if (!is_numeric($languageID)){
                $valErr =   1;
                $valErrMsg['languageID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['languageID']    =  'languageID missing';   
        }
         if($addressID){
            if (!is_numeric($addressID)){
                $valErr =   1;
                $valErrMsg['addressID']    =  'Only numbers allowed';   
            }
        } else {
            $valErr =   1;
            $valErrMsg['addressID']    =  'addressID missing';   
        }
        if($valErr==0){
            $userID = $this->userID;
            $this->generalModel->deleteTableValues('ec_address', array('customerID' => $userID, 'addressID' => $addressID), FALSE);
            $isDeleteDefault    =   $this->generalModel->getTableValue('addressID', 'ec_customer', 'customerID=' . "$userID". ' AND addressID='.$addressID, FALSE);
            if($isDeleteDefault){
                 $resData = array(
                        'addressID' => 0
                    );
                    $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $userID, $resData);
            }
           
            $checkDefault   =   $this->accountModel->checkDefault($userID);
            if(empty($checkDefault)){
                $existingAddress    =   $this->generalModel->getTableValue('addressID', 'ec_address', 'customerID=' . "$userID", FALSE);
                if($existingAddress){
                    $resData = array(
                        'addressID' => $existingAddress['addressID']
                    );
                    $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $userID, $resData);
                }
            }
            $userDetails = $this->generalModel->getTableValue('customerID AS userID,firstname,lastname,email,telephone,token', 'ec_customer', 'customerID=' . "$userID", FALSE); //data for edit
            $addressList = $this->accountModel->selectAddress($userID);
            $address= $this->accountModel->selectAddressDefault($userID);
        
        if($addressList){
            foreach($addressList as $key=>$adderssArr){ 
                $addressList[$key]['fullAddress']   = $adderssArr['firstname']." ".$adderssArr['lastname']." ,".$adderssArr['address1']." ,".$adderssArr['city']." ,".$adderssArr['postcode'];
                    $defaultAddress   =   "No";
                    if($adderssArr['addressID']==$address['addressID']){
                       $defaultAddress   =   "Yes";
                    }
                   $addressList[$key]['defaultAddress']   =  $defaultAddress;
                }     
            }
            $status      =   parent::HTTP_OK;
            $message     =   'Address Deleted Successfully'; 
            $data        =   $addressList;
        }else{
            $msgvalue    =   '';
                $i  =   1;
                foreach($valErrMsg as $key=>$msgData){
                   
                    $msgvalue   .= $msgData;
                    if($i!=count($valErrMsg)){
                        $msgvalue   .= ",";
                    }
                    $i++;
                } 
            $message    =   $msgvalue;
            $status      =   parent::HTTP_BAD_REQUEST;
        }
        $this->setHeaderToken();
        $result['status']	= $status;
        $result['message']      = $message;
        $result['data']		= $data;
        $this->response($result, $status);
    }
}
<?php

/**
 * Description of HeaderCart
 *
 * @author wildherbs-user
 */
class HeaderCart extends Site_Controller {
    
    public function __construct(){
        
        parent::__construct();
        $this->load->model('GeneralModel','generalModel');
        
    }
    public function index(){
        
        $products       = $this->carts->getProducts($this->siteLanguageID);
        list($width,$height) = $this->getImageDimension('cart_image_dimen');
        foreach($products as $key => $row){
            
            $products[$key]['image'] = $this->setSiteImage('product/'.$row['image'],$width, $height);
            $products[$key]['price'] = $this->currency->format($row['price'],$this->siteCurrency);
        }
        $incart['incart']       = $products;
        $incart['subtotal']     = $this->currency->format($this->carts->getSubTotal(),$this->siteCurrency);
        //$incart['shipping']     = $this->currency->format(0,$this->siteCurrency);
        $incart['shipping']     = $this->currency->format($this->getSettingValue('shipping_charge',$this->siteLanguageID),$this->siteCurrency);
        $incart['total']        = $this->currency->format($this->carts->getTotal(),$this->siteCurrency);
        
        $this->twig->display('frontEnd/site_master_cart',$incart);
    }
    
}

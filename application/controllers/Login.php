<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends Auth_Controller{
    public function __construct()
    {
        parent::__construct();  
        $this->load->library('CustomerLogin');
        $this->load->library('Apisupport');
        $this->load->model('GeneralModel','generalModel');
        $this->load->library('form_validation');  
        $this->lang->load('site',$this->siteLanguage);  
    }
    public function index(){
        $data['title'] =   'Login page';
        if(isset($_COOKIE["loginId"])){
            $this->data['loginId'] =   $_COOKIE["loginId"];
            $this->data['loginPass'] =   $_COOKIE["loginPass"];
        }
        if($this->input->post()){
            if($this->input->post('countryCode')=='974'){
                    $this->form_validation->set_rules('telephone', 'Phone Number', 'required|min_length[8]|max_length[8]', array(
                        'required' => 'Please Provide Phone Number',
                        'min_length' => 'Please provide a valid phone number',
                        'max_length' => 'Please provide a valid phone number'
                    ));
             }else{
                    $this->form_validation->set_rules('telephone', 'Phone Number', 'required|min_length[10]|max_length[10]', array(
                        'required' => 'Please Provide Phone Number',
                        'min_length' => 'Please provide a valid phone number',
                        'max_length' => 'Please provide a valid phone number'
                    ));
             }
            //$this->form_validation->set_rules('email', 'Email', 'required',array('required' => 'Please Provide A Valid Email'));
            //$this->form_validation->set_rules('password', 'Password', 'required',array('required' => 'Please Provide A Valid Password'));
           
            if($this->form_validation->run()===TRUE)
            {  
                $resData['nonXss']    = array(
                            'telephone'      => $this->input->post('telephone')
                            );
                 $resData['xssData']   = $this->security->xss_clean($resData['nonXss']);

                 $resultStatus      = $this->customerlogin->checkPhCredentials( $resData['xssData']['telephone'],$this->siteLanguage);   
                 //$user   =   $this->generalModel->getTableValue('customerID,firstname,lastname,email,telephone,status', 'ec_customer','(email ="'. $emailPhone.'" OR telephone="'. $emailPhone.'") AND status="Active"',FALSE);
              
                 //echo "<pre>"; print_r($resultStatus); exit;
                 if ($resultStatus['status']==1){   

                    $userID =   $resultStatus['customerID'];
                    $value['status'] = 'expired';
                    $this->generalModel->updateTableValues('ec_otp', 'customerID=' . $userID .' AND otpType="userLogin"', $value);
                    $otp    =   substr(number_format(time() * rand(),0,'',''),0,6); 
                    

                    $resOtpData['otp']            =   $otp;
                    $resOtpData['customerID']     =   $userID;
                    $resOtpData['otpType']        =   'userLogin';
                    $resOtpData['otpTime']        =   date("Y-m-d H:i:s");
                    $resOtpData['addedDate']      =   date("Y-m-d H:i:s");
                    //echo "<pre>"; print_r($resOtpData); exit;
                    $otpID   =   $this->generalModel->insertValue('ec_otp',$resOtpData);
                    if($otpID){
                        $customerData          = $this->generalModel->getTableValue('customerID,firstname,lastname,email,countryCode,telephone,status','ec_customer',array('customerID'=> $userID,'status'=>'Active'));

                        $phoneNo    =   $customerData['countryCode'].$customerData['telephone'];
                        $smsMessage =   "Use ".$otp." as your verification code on mowneh.com . The OTP expires in 5 minutes. Please do not share with anyone.";
                        $this->sendSMS($smsMessage,$phoneNo);
                        $this->data['message'] =   $resultStatus['message'];
                        $this->session->set_userdata("lodinCustomerId",$userID);
                        $this->session->set_userdata("otp",$otp);
                        $this->render('frontEnd/login/LoginOtp');
                        redirect('Login/loginOtp', 'refresh');
                        //exit;
                    }
                    
                    
                   
                    // echo "<pre>"; print_r($this->session->userdata()); exit;
                    if($this->session->userdata('moonehCallBackURL')!=''){
                        
                        $callBackURL    =   $this->session->userdata('moonehCallBackURL');
                        $this->session->set_userdata('moonehCallBackURL', '');
                        redirect($callBackURL, 'refresh');
                        
                    }else{
                        redirect('Home', 'refresh');
                    }
                    
                }else{ 
                    $this->session->set_flashdata('message',$resultStatus['message']); 
                    $this->data['message'] =   $resultStatus['message'];
                    $this->load->helper('form');
                    //redirect('siteadmin', 'refresh');
                }
            }
        } 
       
        $this->render('frontEnd/login/Login');
    }
   
    public function loginOtp(){
        $message    =   "";
        $errMessage =   "";
        $lodinCustomerId  =   $this->session->userdata('lodinCustomerId');
        $this->form_validation->set_rules('otp', 'OTP', 'required');
        if(@$this->input->post('expire')){
            $value['status'] = 'expired';
            $this->generalModel->updateTableValues('ec_otp', 'customerID=' . $lodinCustomerId .' AND otpType="userLogin" AND otp='. $this->session->userdata('otp'), $value);
            echo "1";
            exit;
        }
        
        if($this->form_validation->run()===TRUE)
        {
            
           //$user   =   $this->generalModel->getTableValue('otpID,status', 'ec_otp','otp ="'. $this->input->post('otp').'" AND otpType="userLogin" AND TIMESTAMPDIFF(SECOND,ec_otp.addedDate,NOW()) > 300 AND customerID ="'. $lodinCustomerId.'"',FALSE);
            $dateTime   =   date("Y-m-d H:i:s");
            $user   =   $this->generalModel->getTableValue('otpID,status', 'ec_otp','otp ="'. $this->input->post('otp').'" AND otpType="userLogin" AND addedDate >= date_sub("'.$dateTime.'",interval 5 minute) AND customerID ="'. $lodinCustomerId.'"',FALSE);
            //echo $this->db->last_query();exit;
            if(@$user){ 
                if($user['status']=='Active'){ 
                    $customer          = $this->generalModel->getTableValue('customerID,firstname,lastname,email,countryCode,telephone,status','ec_customer',array('customerID'=> $lodinCustomerId,'status'=>'Active'));
                    $value['status'] = 'used';
                    $this->generalModel->updateTableValues('ec_otp', 'customerID=' . $lodinCustomerId .' AND otpType="userLogin" AND otp='. $this->input->post('otp'), $value);
                    $this->apisupport->updateToken($customer['customerID']);
                    $customerData        = array(
                        'moonehcustomerID'          => $customer['customerID'],
                        'moonehFirstname'           => $customer['firstname'],
                        'moonehLastname'            => $customer['lastname'],
                        'moonehEmail'               => $customer['email'],
                        'moonehStatus'              => $customer['status'],
                        'moonehLoggedIn'            => TRUE
                    ); 
                   
                    $this->session->set_userdata($customerData);
                    redirect('Home', 'refresh');
                    exit;
                }if($user['status']=='used'){ 
                     $this->data['errMessage']  =   lang('OTP_already_used');
                     $this->render('frontEnd/login/LoginOtp');
                     //exit;
                }
                if($user['status']=='expired'){ 
                     $this->data['errMessage']  =   lang('OTP_expired');
                     $this->render('frontEnd/login/LoginOtp');
                    
                }
                
            }else{
                $this->data['errMessage']  =   lang('OTP_Invalid');
                     $this->render('frontEnd/login/LoginOtp');
            }
            $this->data['message']  =   "";
        }else{
           $this->data['message']  = lang('OTP_has_been_sent_to_your_registered_phone_number'); 
        }
        $this->render('frontEnd/login/LoginOtp');
    }
    
    public function resendOtp(){
        $userID =   $this->session->userdata('lodinCustomerId');
                    $value['status'] = 'expired';
                    $this->generalModel->updateTableValues('ec_otp', 'customerID=' . $userID .' AND otpType="userLogin"', $value);
                    $otp    =   substr(number_format(time() * rand(),0,'',''),0,6); 
                    

                    $resOtpData['otp']            =   $otp;
                    $resOtpData['customerID']     =   $userID;
                    $resOtpData['otpType']        =   'userLogin';
                    $resOtpData['otpTime']        =   date("Y-m-d H:i:s");
                    $resOtpData['addedDate']      =   date("Y-m-d H:i:s");
                    $otpID   =   $this->generalModel->insertValue('ec_otp',$resOtpData);
                    $lodinCustomerId  =   $this->session->userdata('lodinCustomerId');
                    $customerData          = $this->generalModel->getTableValue('customerID,firstname,lastname,email,countryCode,telephone,status','ec_customer',array('customerID'=> $lodinCustomerId,'status'=>'Active'));

                    if($otpID){
                        $phoneNo    =   $customerData['countryCode'].$customerData['telephone'];
                        $smsMessage =   "Use ".$otp." as your verification code on mowneh.com . The OTP expires in 5 minutes. Please do not share with anyone.";
                        $this->sendSMS($smsMessage,$phoneNo);
                        $this->session->set_userdata("lodinCustomerId",$userID);
                        $this->session->set_userdata("otp",$otp);
                        echo "1";
                        //exit;
                    }else{
                        echo "0";
                    }
    }
    
    public function loginOld(){
        $data['title'] =   'Login page';
        if(isset($_COOKIE["loginId"])){
            $this->data['loginId'] =   $_COOKIE["loginId"];
            $this->data['loginPass'] =   $_COOKIE["loginPass"];
        }
        if($this->input->post()){
            $this->form_validation->set_rules('email', 'Email', 'required',array('required' => 'Please Provide A Valid Email'));
            $this->form_validation->set_rules('password', 'Password', 'required',array('required' => 'Please Provide A Valid Password'));
           
            if($this->form_validation->run()===TRUE)
            {  
                $resData['nonXss']    = array(
                            'email'         => $this->input->post('email'),
                            'password'      => $this->input->post('password')
                            );
                 $resData['xssData']   = $this->security->xss_clean($resData['nonXss']);

                 $resultStatus      = $this->customerlogin->checkCredentials($resData['xssData']['email'], $resData['xssData']['password'],$this->siteLanguage);   
                 //echo "<pre>"; print_r($resultStatus); exit;
                 if ($resultStatus['status']==1){   
                    
                    // login atttempt table clear
                    $where          = array('login'=>$resData['xssData']['email']);
                    $this->generalModel->deleteTableValues('ec_login_attempts',$where);
                    // login ip address update
                   
                    $ipAddress      = $this->input->ip_address();
                    $values         = array('ip' => $ipAddress,'lastLogin' => date('Y-m-d h:i:s'));
                    $where          = array('email'      => $resData['xssData']['email']);
                    $this->generalModel->updateTableValues('ec_customer',$where,$values);
                    // admin Detials 
                    $customer          = $this->generalModel->getTableValue('customerID,firstname,lastname,email,status','ec_customer',array('email'=> $resData['xssData']['email'],'status!='=>'Deleted'));
                    $customerData        = array(
                        'moonehcustomerID'          => $customer['customerID'],
                        'moonehFirstname'           => $customer['firstname'],
                        'moonehLastname'            => $customer['lastname'],
                        'moonehEmail'               => $customer['email'],
                        'moonehStatus'              => $customer['status'],
                        'moonehLoggedIn'            => TRUE
                    ); 
                   
                    $this->session->set_userdata($customerData);
                    
                    $this->session->set_flashdata('message',$resultStatus['message']); 
                    
                    $this->data['message'] =   $resultStatus['message'];
                    
                    // remember me
                    if(!empty($this->input->post("remember"))) {
                      setcookie ("loginId", $this->input->post('email'), time()+ (10 * 365 * 24 * 60 * 60));  
                      setcookie ("loginPass", $this->input->post('password'),  time()+ (10 * 365 * 24 * 60 * 60));
                    } else {
                      setcookie ("loginId",""); 
                      setcookie ("loginPass","");
                    }   
                    // echo "<pre>"; print_r($this->session->userdata()); exit;
                    if($this->session->userdata('moonehCallBackURL')!=''){
                        
                        $callBackURL    =   $this->session->userdata('moonehCallBackURL');
                        $this->session->set_userdata('moonehCallBackURL', '');
                        redirect($callBackURL, 'refresh');
                        
                    }else{
                    redirect('Home', 'refresh');
                    }
                    
                }else{ 
                    $this->session->set_flashdata('message',$resultStatus['message']); 
                    $this->data['message'] =   $resultStatus['message'];
                    $this->load->helper('form');
                    //redirect('siteadmin', 'refresh');
                }
            }
        } 
       
        $this->render('frontEnd/login/Login');
    }
    
    public function forgotPasswor(){
        $message    =   "";
        $errMessage =   "";
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        if($this->form_validation->run()===TRUE)
        {
            $email  =   $this->input->post('email');
            $user   =   $this->generalModel->getTableValue('customerID,firstname,lastname,email,status', 'ec_customer', array("email" => $email, 'status!=' => 'Deleted'));
            if ($user['status'] == 'Active') {
                
                $config['protocol'] = 'sendmail';
                $config['mailtype'] = 'html';
                $from_email =   $this->getSettingValue('email');
                
                $emailMessage   = $this->resetEmailMessage($user['customerID']);
               
//                    $firstLine = "Dear " . $user['firstname'] . " " . $user['lastname'];
//                    $secondLine = "Please click below link to reset your password";
//                    $emailMessage = '<table border="0"><tr><td>'.$firstLine.'</td></tr>'
//                            . '<tr><td>'.$secondLine.'</td></tr>'
//                            . '<tr><td><a href="'.$resetLink.'">Click Here</a></td></tr>'
//                            . '<tr><td>URL : '.$resetLink.'</td></tr>'
//                            . '<tr><td>Thank You</td></tr></table>';
                    //$to_email = $this->input->post('email');
                    //Load email library 
                    $this->load->library('email');
                    $this->email->initialize($config);
                    
                    $this->email->from($from_email, 'Mooneh');
//                    $this->email->to($to_email);
                    $this->email->to($user['email']);
                    $this->email->subject('mowneh reset password');
                    $this->email->message($emailMessage);
                    if($this->email->send()){
                        $message = lang('ForgotPasswordSuccess');
                    }else{
                         $errMessage = lang('Something_went_wrong_please_try_again');
                    }
                    
            }elseif ($user['status'] == 'Inactive') {
                $errMessage = lang('Your_account_is_inactive_now');
            }elseif ($user['status'] == 'Notverified') {
                $errMessage = lang('Account_Not_Verified');
            }else{
                $errMessage = lang('User_Not_Found');
            }
            $this->data['errMessage'] =   $errMessage;
            $this->data['message'] =   $message;
        }
        $this->render('frontEnd/login/ForgotPassword');
    }
    
    public function resetPassword($userID){
        if($userID){
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('cPassword', 'Confirm Password', 'required|matches[password]');
            if ($this->form_validation->run() === TRUE) {
                $user   =   $this->generalModel->getTableValue('customerID,email,status', 'ec_customer', array("customerID" => $userID, 'status' => 'Active'));
                $resData['nonXss'] = array(
                'password' => $this->input->post('password')
            );
            $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);

            $passwordCreate = $this->customerlogin->resetCredentials($resData['xssData']['password']);
            //echo "<pre>"; print_r($passwordCreate);exit;
            //echo "<pre>"; print_r($passwordCreate); exit;
            $resData['xssData']['password'] = $passwordCreate['password'];
            $resData['xssData']['salt'] = $passwordCreate['salt'];
            //echo "<pre>";print_r($resData['xssData']['password']);exit;
           
            $result = $this->generalModel->updateTableValues('ec_customer', 'customerID=' . $userID, $resData['xssData']);
         

            if ($result) {
                $this->data['message']  = lang('Password_changed_Successfully')."<a href=".base_url('/Login').">".lang('Login_Here')."</a>";
                
            } else {
                $this->data['errMessage']   = lang('Failed_To_Update');
            }
            }
        }
        $this->render('frontEnd/login/ResetPassword');
    }
    
    public function resetEmailMessage($customerID   =   ""){
        $logo  = base_url('uploads/siteInfo/') . $this->getSettingValue('logo');
        $resetLink  =   base_url('/Login/resetPassword/'.$customerID); 
        $customerData   = $this->generalModel->getTableValue('firstname,lastname,email', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
        $email      =   $this->getSettingValue('email');
        $phone      =   $this->getSettingValue('phone');
        $address    =   $this->getSettingValue('address', $this->siteLanguageID, true);
        
        $message    =   '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                            <html xmlns="http://www.w3.org/1999/xhtml">
                            <head>
                                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                                    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                                    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                                    <title>Password Reset</title>

                                    <style type="text/css">
                                    @font-face {font-family: "Mary Ann"; src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot"); src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot?#iefix") format("embedded-opentype"); }
                                            body {
                                                    Margin: 0;
                                                    padding: 0;
                                                    font-family: Mary Ann;
                                                    color: #1d1d1d;
                                            }
                                            table {
                                                    border-spacing: 0;
                                            }
                                            td {
                                                    padding: 0;
                                            }
                                            img {
                                                    border: 0;
                                            }
                                            .wrapper
                                            {
                                                    width: 100%;
                                                    table-layout: fixed;
                                                    background-color: #f6f6f6;
                                            }
                                            .webkit
                                            {
                                                    max-width: 600px;
                                                    background-color: #fff;
                                            }
                                            .content
                                            {
                                                    padding: 30px 20px;
                                            }
                                            .reset a
                                            {
                                                    color: #ffffff;
                                                    font-weight: 400;
                                                    text-transform: uppercase;
                                                    background-color: #F06C00;
                                                    border: 1px solid #F06C00;
                                                    text-decoration: none;
                                                    padding: 5px 8px;
                                                    text-align: center;
                                                    border-radius: 5px;
                                            }
                                            .reset a:hover
                                            {
                                                    color: #F06C00;
                                                    background-color: #fff;
                                                    border: 1px solid #F06C00;
                                            }
                                            .content p
                                            {
                                                    font-size: 15px;
                                            } 
                                            .footer
                                            {
                                                    border-top: 1px solid #f2f2f2;
                                                    padding: 30px 0px;
                                            }
                                            @media screen and (max-width: 600px) { 
                                            }
                                            @media screen and (max-width: 400px) { 
                                            }
                                    </style>
                            </head>
                            <body>
                             <center class="wrapper">
                                     <div class="webkit">
                                             <table class="outer" align="center">
                                                     <tr>
                                                             <td>
                                                                     <table width="100%" style="border-spacing: 0;">
                                                                        <tr>
                                                                                    <td style="background-color:#F06C00;padding: 10px;text-align: center;">
                                                                                            <a href="'.base_url().'"><img style="filter:brightness(100) contrast(100%) sepia(99) grayscale(1)" src="'.$logo.'" width="120" alt="logo" /></a>
                                                                                    </td>
                                                                            </tr>
                                                                     </table>
                                                             </td>
                                                     </tr>
                                                     <tr>
                                                             <td class="content" style="text-align:left;">
                                                                    <h3>Hello '.$customerData['firstname'].' '.$customerData['lastname'].',</h3>
                                                                    <p> Greetings from Mowneh!
                                                                            <br/>We have received your request to reset your password.</p>
                                                                            <table align="center" class="reset" style="padding: 20px 0px;">
                                                                                    <tr>
                                                                                            <td>
                                                                                            <a href="'.$resetLink.'">Click here to reset</a>
                                                                                            </td>
                                                                                    </tr>
                                                                            </table> 
                                                                            <p>
                                                                            <br/>This link will expire after 24 hours. If you did not request to reset your password, please email us on <a style="color: #1d1d1d;" href="'.$email.'">'.$email.'</a>
                                                                            <br/>
                                                                            <br/>Regards,
                                                                            <br/>Mowneh Team

                                                                    </p>
                                                             </td>
                                                     </tr>
                                                     <tr>
                                                             <td>
                                                                    <table width="100%" style="border-spacing: 0;">
                                                                            <tr>
                                                                                    <td class="footer" style="border-top:1px solid #f2f2f2;padding: 15px;text-align:left;"> 
                                                                                            <p style="font-size: 12px;color: #a7a7a7;">
                                                                                                    '.$address.'<br/>Copyright © 2020 <a style="color: #a7a7a7;" href="'.base_url().'">Mowneh.</a> All Rights Reserved
                                                                                            </p>

                                                                                    </td>
                                                                                    <td style="border-top:1px solid #f2f2f2;padding: 15px;text-align:right;">
                                                                                            <a href="'.base_url().'"><img src="'.base_url('image/cache/siteInfo/2c0523d0dad5f48f434a75ab2136b33c-140x45.png').'" width="80" alt="logo"></a>
                                                                                            <br/>


                                                                                    </td>
                                                                            </tr>
                                                                            <tr>
                                                                                    <td colspan="2" style="padding: 5px; background-color: #F06C00;">
                                                                                    </td>
                                                                            </tr>
                                                                    </table>

                                                             </td>
                                                     </tr>
                                             </table>
                                     </div>
                             </center>
                            </body>
                            </html>';
       
        return $message;
    }
}

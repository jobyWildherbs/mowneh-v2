<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cart extends Site_Controller {

    public function __construct() {

        parent::__construct();
        //echo "<pre>"; print_r($_REQUEST); exit;
        $this->load->library('CustomerLogin');
        $this->load->model('CartModel', 'cartModel');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('ProductModel', 'pm');
        $this->load->model('AccountModel', 'accountModel');
        $this->load->model('WarehouseModel', 'wm');
         $this->load->model('OrderModel', 'om');
         $this->load->model('OptionModel', 'optionModel');
        $this->load->model('CouponModel', 'coupon'); 
        $this->load->library('Order');
        $this->lang->load('site', $this->siteLanguage);
        $this->lang->load('error', $this->siteLanguage);
        $this->customerID = $this->customer->getID();
        $this->load->library('form_validation');

    }



    public function index() {
        $customerID = $this->session->userdata('moonehcustomerID');
        if ($this->input->post('couponSubmit')) {
            $this->form_validation->set_rules('couponCode', 'Coupon Code', 'required');
            if ($this->form_validation->run() === TRUE) {
           
                 if ($this->customerID == "" || $this->customerID == 0) {
                    $this->session->set_userdata('moonehCallBackURL', 'Cart');
                    redirect('Login', 'refresh');
                }
                $couponCode =$this->input->post('couponCode');
                $coupon =   $this->coupon->selectCoupon($couponCode);
                
                
                
                if(!empty($coupon)){
                if(@coupon['applayForUser']){
                $applyUser = explode(',',$coupon['applayForUser']);
                }else{
                $applyUser = array("Not found");
                }
                $applyUsers = array();
                foreach($applyUser as $key => $apply){
                	$apply = trim($apply);
    			if (!empty($apply))
    			$applyUsers[] = $apply;
        	}
                //exit;
                    if($coupon['applayFor']=='cart'){
                        $couponData        = array('couponData'=>array(
                            'couponID'          => $coupon['couponID'],
                            'code'              => $coupon['code'],
                            'type'              => $coupon['type'],
                            'applayFor'         => $coupon['applayFor'],
                            'discount'          => $coupon['discount'],
                            'status'            => $coupon['status']
                        ));                  
                        $this->session->set_userdata($couponData);                   
                        $this->data['succMsgCoupon']  = lang('Coupon_Applied'); 
                    }
                    elseif($coupon['applayFor']=='delivery'){
                        $couponData        = array('couponData'=>array(
                            'couponID'          => $coupon['couponID'],
                            'code'              => $coupon['code'],
                            'type'              => $coupon['type'],
                            'applayFor'         => $coupon['applayFor'],
                            'discount'          => $coupon['discount'],
                            'status'            => $coupon['status']
                        ));                  
                        $this->session->set_userdata($couponData);                   
                        $this->data['succMsgCoupon']  = lang('Coupon_Applied'); 
                    }
                    elseif($coupon['applayFor']=='user'){ 
                    if(in_array($this->session->userdata('moonehEmail'),$applyUsers)){
                        $couponData        = array('couponData'=>array(
                            'couponID'          => $coupon['couponID'],
                            'code'              => $coupon['code'],
                            'type'              => $coupon['type'],
                            'applayFor'         => $coupon['applayFor'],
                            'discount'          => $coupon['discount'],
                            'status'            => $coupon['status']
                        ));                  
                        $this->session->set_userdata($couponData);                   
                        $this->data['succMsgCoupon']  = lang('Coupon_Applied'); 
                        }
                        else{
                        $this->session->unset_userdata('couponData');
                        $this->data['errMsgCoupon']  = lang('Invalid_Coupon');
                        }
                    }
                    elseif($coupon['applayFor']=='firstPurchase'){
                       
                        $checkOrder = $this->coupon->checkOrderExist($customerID);
                        
                        if(empty($checkOrder)){
                            $couponData        = array('couponData'=>array(
                                'couponID'          => $coupon['couponID'],
                                'code'              => $coupon['code'],
                                'type'              => $coupon['type'],
                                'applayFor'         => $coupon['applayFor'],
                                'discount'          => $coupon['discount'],
                                'status'            => $coupon['status']
                            ));                  
                            $this->session->set_userdata($couponData);                   
                            $this->data['succMsgCoupon']  = lang('Coupon_Applied'); 
                           
                        }else{
                        $this->session->unset_userdata('couponData');
                             $this->data['errMsgCoupon']  = lang('Invalid_Coupon');
                        }
                    }
                  
                }else{
                $this->session->unset_userdata('couponData');
                    $this->data['errMsgCoupon']  = lang('Invalid_Coupon');                    
                }
                //
           
            }
        }
        $cartItems = $this->carts->getProducts($this->siteLanguageID);

        list($width, $height) = $this->getImageDimension('cart_image_dimen');
        foreach ($cartItems as $key => $cart) { 
            $cartItems[$key]['price'] = $this->currency->format($cart['price'], $this->siteCurrency);
            $cartItems[$key]['image'] = $this->setSiteImage('product/' . $cart['image'], $width, $height);
            $opt = array();
            $optionProductId = array();
            if($cart['option']){
                foreach($cart['option'] as $option){
                    $optionvals = explode(':', $option);$optionArra=array();$optionProduct='';$i=0;
                    foreach ($optionvals as $value) {
                        $vals = explode('-', $value);
                        if($vals[0] == 'productOptionID'){  
                            $optionProduct=$vals[1];
                            $option = $this->optionModel->selectOptionDetail($vals[1],$this->siteLanguageID);
                            $optionArra=$option;
                        }
                        if($vals[0] == 'optionValueId'){  

                        }
                        $i++;
                    }

                    $optionProductId[] = $optionProduct;

                }
                  $optionArray = array();
                  if($option){
                       foreach($optionArra as $option){
                        $optionArray[] = $option['oName']."-".$option['name'];
                    }
                  }
                         $this->db->select('productOptionID,quantity,productID');
        $this->db->from('ec_product_option');
        $this->db->where('productID', $cart['productID']);
        $this->db->where('productOptionID', $optionProductId[0]);
        $this->db->where('status', 'Active');
                $resultSet  = $this->db->get();
                $productOptions = $resultSet->result_array();
        
        $exist  =   0;$productOptionId=array(); $pQuiantity=''; $pProductId='';
        if($productOptions){
            foreach($productOptions as $key1=>$productOptionsData){
               /*$productOptionsValueReturn    =   array();
                $this->db->select('optionValueID');
                $this->db->from('ec_product_option_value');
                $this->db->where('productOptionID', $productOptionsData['productOptionID']);
                $this->db->where('productID', $cart['productID']);
                $resultSetOption  = $this->db->get();
                $productOptionsValue = $resultSetOption->result_array();
                foreach($productOptionsValue as $key2=>$productOptionsValueData){
                   
                   $productOptionsValueReturn[]    = $productOptionsValueData['optionValueID'];
                }
                $checkArray =   array_diff_assoc($productOptionsValueReturn, $optionProductId);
           // print_r($checkArray);
            if(empty($checkArray)){  
                $exist  = 1;*/
                $productOptionId[]=$productOptionsData['productOptionID'];
                $pQuiantity=$productOptionsData['quantity'];
                $pProductId=$productOptionsData['productID'];

            
                //print_r($productOptionsValueReturn);
            }
        }
        
            $cartItems[$key]['option'] = $optionArray;
            $cartItems[$key]['optionProductId'] = $optionProductId;
           }
      
        }
        
        $this->data['cartItems'] = $cartItems;


        //if (!empty($cartItems)) {
        //$this->data['cartItems']['subTotal'] = $this->currency->format($this->carts->getSubTotal(), $this->siteCurrency);
        //}
        $allTotals = array();
        if (!empty($cartItems)) {
            $allTotals = $this->carts->getTotalItemsList($this->siteLanguageID);
            foreach ($allTotals as $key => $tolVal) {
                $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency,'',TRUE,PHP_ROUND_HALF_DOWN);
            }
        }
       // echo "<pre>";print_r($allTotals);
        $this->data['subTotal'] = $allTotals;

        $this->render('frontEnd/cart/Cart');
    }



    public function add() {
        $json = array();
        $productID = ($this->input->post('product_id')) ? $this->input->post('product_id') : 0;
        $quantity = ($this->input->post('quantity')) ? $this->input->post('quantity') : 1;
        //print_r($this->input->post('options'));
        if(@$this->input->post('options')){
            $optionval = explode(',', $this->input->post('options'));
            $price=0; 
            $optionArray = array();
            foreach($optionval as $option){
                $optionvals = explode(':', $option);$i=0;
                $optionArray[] = json_encode($option);
                foreach ($optionvals as $value) {
                    $vals = explode('-', $value);
                    if($vals[0] == 'price'){
                        $price += $vals[1];
                        
                    }
                }
                
            }
           
        }else{
            $optionRequiredCheck = $this->pm->getProductOptions($productID);
            if(@$optionRequiredCheck){
                $required =0;
                foreach($optionRequiredCheck as $option){
                    if($option['required'] == 'Yes'){
                        $required =1;
                    }
                     
                }

                if($required ==1){
                    $customerID = $this->session->userdata('moonehcustomerID');
                    $product = $this->pm->selectProductDetail($productID,$this->siteLanguageID);
                    //redirect('Product/detail/'.$product['pageKey'], 'refresh');
                    $json['optionRequired'] = 1;
                    $json['baseUrl'] = base_url();
                    $json['pageKey'] = $product['pageKey'];
                    
                    
                }
            }
            
        }
        
        
        if ($productID > 0) {
            $productInfo = $this->generalModel->getTableValue('quantity,minimum,status,dateAvailable', 'ec_product', array("productID" => $productID, 'status' => 'Active'));
            if ($productInfo['status'] != 'Active') {
                $json['error']['status'] = $this->lang->line('product_status_error');
            }

            if ($productInfo['dateAvailable'] != '0000-00-00' && $productInfo['dateAvailable'] > date('Y-m-d')) {
                $json['error']['dateAvailable'] = sprintf($this->lang->line('product_date_available_error'), date('jS, F Y', strtotime($productInfo['dateAvailable'])));
            }

            if ($quantity < $productInfo['minimum']) {
                $quantity = $productInfo['minimum'];
            }

            if ($productInfo['quantity'] < ($this->carts->getProductQuantity($productID) + $quantity)) {
                $json['error']['quantity'] = $this->lang->line('quantity_not_available');
            }
            if(@$this->input->post('options')){
                $optionval = explode(',', $this->input->post('options'));  
                $productOptionID=0;
                $optionArray = array();
                foreach($optionval as $option){
                    $optionvals = explode(':', $option);$i=0;
                    $optionArray[] = json_encode($option);
                    foreach ($optionvals as $value) {
                        $vals = explode('-', $value);
                        if($vals[0] == 'productOptionID'){
                            $productOptionID = $vals[1];
                            
                        }
                    }
                    
                }
                $productOptionInfo = $this->generalModel->getTableValue('quantity', 'ec_product_option', array("productID" => $productID, 'status' => 'Active',"productOptionID" => $productOptionID));
            
                if ($productOptionInfo['quantity'] < ($this->carts->getProductOptionQuantity($productID,$productOptionID) + $quantity)) {
                    $json['error']['quantity'] = $this->lang->line('quantity_not_available');
                }
            }
            if (!$json) {
                $this->carts->add($productID, $quantity,$this->input->post('options'));

                if ($this->carts->hasProducts()) {
                if(@$this->input->post('options')){
                    //$this->pm->updateProductOptionQuantity($productID,$quantity, $this->input->post('options'));
                    }
                    $json['success'] = $this->lang->line('cart_added');
                    $json['totalProducts'] = $this->carts->countProducts();
                    $json['subtoal'] = $this->currency->format($this->carts->getSubTotal(), $this->siteCurrency);
//                    $json['shipping'] = $this->currency->format(0, $this->siteCurrency);  
                    $json['shipping'] = $this->currency->format($this->getSettingValue('shipping_charge',$this->siteLanguageID), $this->siteCurrency);
                    $json['totalAmout'] = $this->currency->format($this->carts->getTotal(), $this->siteCurrency);
                } else {
                    $json['error']['product'] = $this->lang->line('product_add_error');
                }

            }

        } else {

            $json['error']['product'] = $this->lang->line('product_add_error');

        }

        echo json_encode($json);

    }



    public function removeFromCart() {

        $json = array();
        
	$this->session->unset_userdata('couponData');
        $totalProducts = $this->carts->countProducts();

        $cartID = ($this->input->post('key')) ? $this->input->post('key') : 0;
        //print_r($cartID);
        //exit;
        if ($cartID > 0) {
            
            $productID = ($this->input->post('productID')) ? $this->input->post('productID') : 0;
            //$this->pm->updateProductOptionRemoveQuantity($productID);
            //exit;
            $this->carts->remove($cartID,$productID);

            $newProductCount = $this->carts->countProducts();

            if ($totalProducts > $newProductCount) {

                $json['success'] = $this->lang->line('cart_removed');

                $json['totalProducts'] = $this->carts->countProducts();

                $json['totalAmout'] = $this->currency->format($this->carts->getTotal(), $this->siteCurrency);

            } else {

                $json['error']['product'] = $this->lang->line('product_add_error');

            }

        } else {

            $json['error']['product'] = $this->lang->line('product_remove_error');

        }

        echo json_encode($json);

    }



    public function updateCart() {

        $json = array();
	$this->session->unset_userdata('couponData');
        $productID = ($this->input->post('key')) ? $this->input->post('key') : 0;

        $quantity = ($this->input->post('quantity')) ? $this->input->post('quantity') : 1;

        if ($productID > 0) {

            $customerID = $this->customer->getId();

            $apiID = ($this->session->userdata('api_id')) ? (int) $this->session->userdata('api_id') : 0;

            $sessionID = $this->session->session_id;

            $params = array(

                'customerID' => $customerID,

                'apiID' => $apiID,

                'sessionID' => $sessionID,

                'productID' => $productID

            );
            $cartInfo = $this->generalModel->getTableValue('*', 'ec_cart', $params,false);
            $option="";
            if(@$cartInfo['option']){
                $option = $cartInfo['option'];
            }
            
            //exit;
            if ($productID) {

                $productInfo = $this->generalModel->getTableValue('quantity,minimum,status,dateAvailable', 'ec_product', array("productID" => $productID, 'status' => 'Active'));

                if ($productInfo['status'] != 'Active') {

                    $json['error']['status'] = $this->lang->line('product_status_error');

                }

                if ($productInfo['dateAvailable'] != '0000-00-00' && $productInfo['dateAvailable'] > date('Y-m-d')) {

                    $json['error']['dateAvailable'] = sprintf($this->lang->line('product_date_available_error'), date('jS, F Y', strtotime($productInfo['dateAvailable'])));

                }

                if ($quantity < $productInfo['minimum']) {

                    $quantity = $productInfo['minimum'];

                }

                if ($productInfo['quantity'] < $quantity) {

                    $json['error']['quantity'] = $this->lang->line('quantity_not_available');

                }

                if (!$json) {

                    $totalProducts = $this->carts->countProducts();

                    $this->carts->update($productID, $quantity,$option);

                    $newProductCount = $this->carts->countProducts();

                    if ($newProductCount != $totalProducts) {

                        $json['success'] = $this->lang->line('cart_updated');

                        $json['totalProducts'] = $this->carts->countProducts();

                        $json['subtoal'] = $this->currency->format($this->carts->getSubTotal(), $this->siteCurrency);

                        $json['shipping'] = $this->currency->format(0, $this->siteCurrency);

                        $json['totalAmout'] = $this->currency->format($this->carts->getTotal(), $this->siteCurrency);

                    } else {

                        $json['error']['product'] = $this->lang->line('product_add_error');

                    }

                }

            }

        }

        echo json_encode($json);

    }



    public function checkOut() {
    //print_r($this->session->userdata);
	    if(@$this->session->userdata['nocouponData']){
	    	$this->data['errMsgCoupon']  = lang('Invalid_Coupon');
	    }else if(@$this->session->userdata['couponData']){
	    	$this->data['succMsgCoupon']  = lang('Coupon_Applied');
	    }
        $nextSloteTime  =   "";
        $nextDeleveryDateTime   =   "";
        $nextDate   =   "";
        $expectDate =   "";
        if ($this->carts->hasProducts() == 0) {
            redirect('Cart', 'refresh');
        }

        if ($this->customerID == "" || $this->customerID == 0) {
            $this->session->set_userdata('moonehCallBackURL', 'Cart/checkOut');
            redirect('Login', 'refresh');
        }
      
        $customerID = $this->customer->getID();
        if ($this->input->post('confirmFlag') == 1) { // Address Save
            $customerID = $this->customer->getID();
            $countryID = 173;
            // form validation 
            $this->form_validation->set_rules('firstname', 'First Name', 'required');
            $this->form_validation->set_rules('lastname', 'Last Name', 'required|xss_clean');
            $this->form_validation->set_rules('address1', 'Building No.', 'required|xss_clean');
            $this->form_validation->set_rules('address2', 'Street No.', 'required|xss_clean');
            $this->form_validation->set_rules('address3', 'Zone No.', 'required|xss_clean');
            $this->form_validation->set_rules('city', 'City', 'required|xss_clean');
            $this->form_validation->set_rules('phone', 'Phone Number', 'required|min_length[8]|max_length[8]', array(
                'min_length' => 'Please provide a valid phone number',
                'max_length' => 'Please provide a valid phone number'
            ));
            if ($this->form_validation->run() === TRUE) {
            $city    = explode("~",$this->input->post('city'));
                $resData['nonXss'] = array(
                    'customerID'    => $customerID,
                    'firstname'     => $this->input->post('firstname'),
                    'lastname'      => $this->input->post('lastname'),
                    'phone'         => $this->input->post('phone'),
                    'address1'      => $this->input->post('address1'),
                    'address2'      => $this->input->post('address2'),
                    'zone'          => $this->input->post('address3'),
                    'city'          => $city[0],
                    'cityID'        =>  $city[1],
                    'postcode'      => $this->input->post('postcode'),
                    'countryID'     => $countryID,
                    'additional_direction'     => $this->input->post('additional_direction'),
                );
                $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
                $result = $this->gm->insertValue('ec_address', $resData['xssData']);
                $this->session->set_userdata('moonehDeliveryAddressID', $result);
                //exit;
                $arrays = array();
                echo json_encode($arrays);
                exit;
            }else{
            		$arrays = array(
		    'error'   => true,
		    'firstname_error' => form_error('firstname'),
		    'lastname_error' => form_error('lastname'),
		    'phone_error' => form_error('phone'),
		    'address1_error' => form_error('address1'),
		    'address2_error' => form_error('address2'),
		    'address3_error' => form_error('address3'),
		    'city_error' => form_error('city')
		   );
		   echo json_encode($arrays);
		   exit;
            }
        }
        $deleveryType =   'normal';
        if($this->input->post('delivery')){
            $this->session->set_userdata('deleveryType', $this->input->post('delivery'));
        }else{
            if($this->session->userdata('deleveryType')=='')
                $this->session->set_userdata('deleveryType', $deleveryType);
                  
        }
        $cartItems = $this->carts->getProducts($this->siteLanguageID);
        list($width, $height) = $this->getImageDimension('cart_image_dimen');
        foreach ($cartItems as $key => $cart) {
            $cartItems[$key]['price'] = $this->currency->format($cart['price'], $this->siteCurrency);
            $cartItems[$key]['image'] = $this->setSiteImage('product/' . $cart['image'], $width, $height);
            $opt = array();
             $optionProductId = array();
             $optionArray = array();
             $optionArra=array();
            if($cart['option']){
                foreach($cart['option'] as $option){
                    $optionvals = explode(':', $option);
                    $optionArra=array();
                    $optionProduct='';
                    $i=0;
                    foreach ($optionvals as $value) {
                        $vals = explode('-', $value);
                        if($vals[0] == 'productOptionID'){  
                            $optionProduct=$vals[1];
                            $option = $this->optionModel->selectOptionDetail($vals[1],$this->siteLanguageID);
                            $optionArra=$option;
                        }
                        if($vals[0] == 'optionValueId'){  

                        }
                        $i++;
                    }

                    $optionProductId[] = $optionProduct;

                }

                foreach($optionArra as $option){
                   $optionArray[] = $option['oName']."-".$option['name'];
                }
            }
            $cartItems[$key]['option'] = $optionArray;
        }
        $this->data['cartItems'] = $cartItems;
//        if (!empty($cartItems)) {

//            $this->data['cartItems']['subTotal'] = $this->currency->format($this->carts->getSubTotal(), $this->siteCurrency);

//        }

       $timeSlotedata           =   array();
       $timeSlote           =   "";
       $expectedDelevery    =   "";
        $moonehDeliveryAddressID = $this->session->userdata('moonehDeliveryAddressID');

        if ($moonehDeliveryAddressID) {
            //$this->session->unset_userdata('moonehDeliveryAddressID');
                $this->data['moonehDeliveryAddress'] = $this->generalModel->getTableValue('*', 'ec_address', 'addressID="' . $moonehDeliveryAddressID . '" AND customerID="' . $customerID . '"', FALSE); 
                $deliveryLocation = $this->generalModel->getTableValue('*', 'ec_warehouse_delivery_location', 'cityID="' . $this->data['moonehDeliveryAddress']['cityID'] . '"', FALSE);
              if (!empty($cartItems)) {
                  
                  if($this->session->userdata('deleveryType')!='')
                    $deleveryType =   $this->session->userdata('deleveryType');
                  else
                      $deleveryType =   'normal';
                  
                  if($deleveryType=='normal'){
                      //echo "<pre>"; print_r($deliveryLocation); exit;
                        $expectedHour   =   $deliveryLocation['normalDeliveryTime'];
                                   
                  }elseif($deleveryType=='express'){
                      $expectedHour   =   $deliveryLocation['expressDeliveryTime'];
                  }
//                        date_default_timezone_set('Asia/Kolkata'); 
                        //set an date and time to work with
                        $start =  date('m/d/Y h:i:s a', time());
                        //display the converted time
                        $expectedDeleveryDateTime   =    date('Y-m-d H:i',strtotime('+'.$expectedHour.' hour +0 minutes',strtotime($start)));  
                        $expectDate =   $expectedDeleveryDateTime;
                        $week   =   date('l', strtotime($expectedDeleveryDateTime));
                        //echo $week;
                        $expectedDelevery   =   "Expected delivery  on ".date('l d-m-Y', strtotime($expectedDeleveryDateTime));
                        $expectedTime   =   date('H:i', strtotime($expectedDeleveryDateTime));
                        
                    $timeSlotedata  = $this->generalModel->getTableValue('*', 'ec_delevery_time_slot', 'fromTime >=TIME("' . $expectedTime . '") AND status="Active"', TRUE);
                   
                    if(empty($timeSlotedata)){
                        $start =  date('m/d/Y', time() + 86400);
                     
                        //display the converted time
                        $expectedDeleveryDateTime   =    date('Y-m-d H:i',strtotime('+'.$expectedHour.' hour +0 minutes',strtotime($start)));  
                        //echo $expectedDeleveryDateTime; exit;
                        $week   =   date('l', strtotime($expectedDeleveryDateTime));
                        //echo $week;
                        $expectedDelevery   =   "Expected delivery  on ".date('l d-m-Y', strtotime($expectedDeleveryDateTime));
                        $expectedTime   =   date('H:i', strtotime($expectedDeleveryDateTime));
                        
                        $timeSlotedata  = $this->generalModel->getTableValue('*', 'ec_delevery_time_slot', 'fromTime >=TIME("' . $expectedTime . '")  AND status="Active"', TRUE);
                        
                        
                    }
                    
                    if($timeSlotedata){
                        
                       
                        $currentSlot    =   "";
                        $i  =   1;
                            foreach($timeSlotedata as $currentSlotData){
                                $currentSlot    .=   $currentSlotData['slotID'];
                                if(count($timeSlotedata)!=$i){
                                    $currentSlot    .= ",";
                                }
                                $i++;
                            }
                            if($currentSlot){
                                //echo strtotime($start);exit;
                                $nextSloteTime  = $this->generalModel->getTableValue('*', 'ec_delevery_time_slot', 'slotID NOT IN ('.$currentSlot.') AND status="Active"', TRUE);
                                if($nextSloteTime){
                                    //echo $this->db->last_query();exit;
                                    $nextDeleveryDateTime   =   date('Y-m-d', strtotime($expectedDeleveryDateTime . ' +1 day'));
                                    $nextDate   =   $nextDeleveryDateTime; 
                                    $nextDeleveryDateTime   =  date('l d-m-Y', strtotime($nextDeleveryDateTime));
                                }
                               
                                  
                            }
                           
                    }
                  
                  
                // $this->session->set_userdata('deleveryType', $this->input->post('delivery'));
                  
                $allTotals = $this->carts->getTotalItemsList($this->siteLanguageID,$deliveryLocation['cityID'],$deleveryType);
                foreach ($allTotals as $key => $tolVal) {
                    if($tolVal['code'] == 'coupon_discount' || $tolVal['code'] == 'discount' || $tolVal['code'] == 'total'){
                        $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency,'',TRUE,PHP_ROUND_HALF_DOWN);

                    }else{
                         $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency);
                    }
                }
                if($this->input->post('timeSlote')){
                    //echo $this->input->post('timeSlote'); exit;
                    if($this->input->post('dateDtata')=='deliveryDate'){
                        $this->session->set_userdata('timeSlote', $this->input->post('timeSlote'));
                        $this->session->set_userdata('expectedDeleveryDateTime', $expectedDeleveryDateTime);
                    }elseif($this->input->post('dateDtata')=='nextDate'){
                         $this->session->set_userdata('timeSlote', $this->input->post('timeSlote'));
                        $this->session->set_userdata('expectedDeleveryDateTime', $nextDate);
                    }
                }
                if($this->session->userdata('expectedDeleveryDateTime')==''){
                    if($expectedDeleveryDateTime)
                        $this->session->set_userdata('expectedDeleveryDateTime',$expectedDeleveryDateTime);
                   
                }
                
                if($this->session->userdata('timeSlote')==''){
                    if(!empty($timeSlotedata))
                        $this->session->set_userdata('timeSlote', $timeSlotedata[0]['slotID']);
                   
                }
                
                if($this->session->userdata('timeSlote'))
                    $timeSlote =   $this->session->userdata('timeSlote');
                
            }
            //echo "<pre>"; print_r($deliveryLocation); exit;
        }else{
            if (!empty($cartItems)) {
                $allTotals = $this->carts->getTotalItemsList();
                
                foreach ($allTotals as $key => $tolVal) {
                    if($tolVal['code'] == 'coupon_discount' || $tolVal['code'] == 'discount' || $tolVal['code'] == 'total'){
                        $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency,'',TRUE,PHP_ROUND_HALF_DOWN);

                    }else{
                         $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency);
                    }
		        
                    	
                }
            }
        }
       
        $sVAl = explode(' ',$allTotals[0]['value']);
        $sVAl[0]   =   (float) str_replace(',', '',  $sVAl[0]);
        if($this->input->post('couponSubmit')){
            $this->form_validation->set_rules('couponCode', 'Coupon Code', 'required');
            if ($this->form_validation->run() === TRUE) {
                if ($this->input->post('couponCode')) {
                     if ($this->customerID == "" || $this->customerID == 0) {
                        $this->session->set_userdata('moonehCallBackURL', 'Cart');
                        redirect('Login', 'refresh');
                    }
                    $couponCode =$this->input->post('couponCode');
                    $coupon =   $this->coupon->selectCoupon($couponCode);


                    if(!empty($coupon) && $sVAl[0] >= $coupon['minimum_amount_purchase']){
                        if(@$coupon['applayForUser']){
                            $applyUser = explode(',',$coupon['applayForUser']);
                        }else{
                            $applyUser = array("Not found");
                        }

                        $applyUsers = array();
                        foreach($applyUser as $key => $apply){
                            $apply = trim($apply);
                            if (!empty($apply))
                                $applyUsers[] = $apply;
                        }

                    //exit;
                        if($coupon['applayFor']=='cart'){
                            $couponData        = array('couponData'=>array(
                                'couponID'          => $coupon['couponID'],
                                'code'              => $coupon['code'],
                                'type'              => $coupon['type'],
                                'applayFor'         => $coupon['applayFor'],
                                'discount'          => $coupon['discount'],
                                'status'            => $coupon['status']
                            ));                  
                            $this->session->set_userdata($couponData); 
                            $this->session->unset_userdata('nocouponData');                  
                            $this->data['succMsgCoupon']  = lang('Coupon_Applied'); 
                        }
                        elseif($coupon['applayFor']=='delivery'){
                            $couponData        = array('couponData'=>array(
                                'couponID'          => $coupon['couponID'],
                                'code'              => $coupon['code'],
                                'type'              => $coupon['type'],
                                'applayFor'         => $coupon['applayFor'],
                                'discount'          => $coupon['discount'],
                                'status'            => $coupon['status']
                            ));                  
                            $this->session->set_userdata($couponData);   
                            $this->session->unset_userdata('nocouponData');                 
                            $this->data['succMsgCoupon']  = lang('Coupon_Applied'); 
                        }
                        elseif($coupon['applayFor']=='user'){ 
                        if(in_array($this->session->userdata('moonehEmail'),$applyUsers)){
                            $couponData        = array('couponData'=>array(
                                'couponID'          => $coupon['couponID'],
                                'code'              => $coupon['code'],
                                'type'              => $coupon['type'],
                                'applayFor'         => $coupon['applayFor'],
                                'discount'          => $coupon['discount'],
                                'status'            => $coupon['status']
                            ));                  
                            $this->session->set_userdata($couponData);    
                            $this->session->unset_userdata('nocouponData');                
                            $this->data['succMsgCoupon']  = lang('Coupon_Applied'); 
                            }
                            else{
                            $nocouponData        = array('nocouponData'=>array(
                                    'couponID'          => "No"
                                ));                  
                                $this->session->set_userdata($nocouponData); 
                            $this->session->unset_userdata('couponData');
                            $this->data['errMsgCoupon']  = lang('Invalid_Coupon');
                            }
                        }
                        elseif($coupon['applayFor']=='firstPurchace'){

                            $checkOrder = $this->coupon->checkOrderExist($customerID);

                            if(empty($checkOrder)){
                                $couponData        = array('couponData'=>array(
                                    'couponID'          => $coupon['couponID'],
                                    'code'              => $coupon['code'],
                                    'type'              => $coupon['type'],
                                    'applayFor'         => $coupon['applayFor'],
                                    'discount'          => $coupon['discount'],
                                    'status'            => $coupon['status']
                                ));                  
                                $this->session->set_userdata($couponData);   
                                $this->session->unset_userdata('nocouponData');                 
                                $this->data['succMsgCoupon']  = lang('Coupon_Applied'); 

                            }else{
                                    $nocouponData        = array('nocouponData'=>array(
                                    'couponID'          => "No"
                                ));                  
                                $this->session->set_userdata($nocouponData);   
                                $this->session->unset_userdata('couponData');   
                                 $this->data['errMsgCoupon']  = lang('Invalid_Coupon');
                            }
                        }

                    }else{
                    $nocouponData        =  array('nocouponData'=>array(
                                                'couponID'          => "No"
                                            ));                  
                                $this->session->set_userdata($nocouponData); 
                            $this->session->unset_userdata('couponData');
                            $this->data['errMsgCoupon']  = lang('Invalid_Coupon');

                    }
                    //
                }   
                redirect('Cart/checkOut', 'refresh');
            }else{
                $this->session->unset_userdata('nocouponData');
            }
        }
           
        //echo "<pre>";  print_r($allTotals);
        $this->data['subTotal'] = $allTotals;
        $this->data['content'] = $this->accountModel->selectAddress($this->customerID);

        
        $this->data['expectedDelevery'] = $expectedDelevery; 
        $this->data['timeSloteData']    = $timeSlotedata;
        $this->data['nextDelevery']     = $nextDeleveryDateTime; 
        $this->data['nextSloteTime']    = $nextSloteTime;
        $this->data['timeSlote']        = $timeSlote; 
        $this->data['nextDate']        = $nextDate; 
        $this->data['expectDate']        = $expectDate;
        $this->data['stateList']        = $this->accountModel->selectStateList($this->siteLanguageID, $this->customerID);
        $this->data['addressErr']       = $this->session->flashdata('addressErr');
        $this->data['countryList']      = $this->generalModel->getTableValue('countryID,name', 'ec_country', 'countryID="173" AND status="Active"', TRUE);
//        $this->data['warehouseCity']    = $this->wm->getWarehouseCityInfo();
        $this->data['warehouseCity']    = $this->wm->getAllActiveCities($this->siteLanguageID);
        $this->data['deleveryType']     = $deleveryType;
        //redirect('Cart/', 'refresh');
        $this->render('frontEnd/cart/CheckOut');
    }



    public function selectAddress() {

        $addressID = $_POST['id'];

        $address = $this->generalModel->getTableValue('*', 'ec_address', 'addressID=' . "$addressID", FALSE);

        echo json_encode($address);

    }



    public function sessionAddressKeep() {

        $addressID = $this->uri->segment(3);

        $this->session->set_userdata('moonehDeliveryAddressID', $addressID);

        redirect('Cart/checkOut', 'refresh');

    }



    public function payment() {
        $moonehDeliveryAddressID = $this->session->userdata('moonehDeliveryAddressID');
        if ($moonehDeliveryAddressID == '') {
            $this->session->set_flashdata('addressErr', lang('Please_Provide_Address'));
            redirect('Cart/checkOut', 'refresh');
        }

        $cartItems = $this->carts->getProducts($this->siteLanguageID);
        if (empty($cartItems)) {
            $this->session->set_flashdata('addressErr', 'Cart Empty');
            redirect('Cart/checkOut', 'refresh');
        }

        $paymentMethod = 3;
        $confirm = $this->order->setOrderData($this->siteLanguageID, $this->siteCurrency, $paymentMethod);
        if ($confirm) {
            $transactionUUID = $this->get_unique_key(25, 'ec_order_transaction', 'transaction_uuid');
            $referenceNumber = $this->get_unique_key(25, 'ec_order_transaction', 'reference_number');
            $amount = $this->generalModel->getFieldValue('value', 'ec_order_total', array('orderID' => $confirm, 'code' => 'total'));
            $transactionArray = array(
                'orderID' => $confirm,
                'transaction_uuid' => $transactionUUID,
                'reference_number' => $referenceNumber,
                'amount' => $amount,
                'currency' => "QAR",
                'paymentStatus' => "started"
            );

            $this->generalModel->insertValue('ec_order_transaction', $transactionArray);
            redirect('Cart/confirmOrder/' . $confirm . '/' . $transactionUUID . '/' . $referenceNumber, 'refresh');

        } else {
            $this->session->set_flashdata('addressErr', 'Something went wrong, Please try again');
            redirect('Cart/checkOut', 'refresh');
        }
    }

    public function success($orderID=0) {
        //echo "<pre>"; print_r($this->session->userdata);  exit;
        $email  =   $this->getSettingValue('email');
//        if (@$orderID != "") {
//            $orderProduct = $this->generalModel->getTableValue('productID,quantity', 'ec_order_product', array("orderID" => $orderID), TRUE);
//            if ($orderProduct) {
//                foreach ($orderProduct as $key => $proVal) {
//                    $this->cartModel->decreaseProductQuentity($proVal['productID'], $proVal['quantity']);
//                }
//            }
//        }
        //$total = $this->carts->getTotal();
        $total =    $this->currency->format($this->generalModel->getFieldValue('value', 'ec_order_total', 'orderID=' . $orderID . ' AND code="total"'), $this->siteCurrency);
        $customerID = $this->customerID;
        $this->carts->clear();
        $this->data['orderID']    =   $orderID;

        unset($this->session->userdata['orderID']);
        unset($this->session->userdata['moonehDeliveryAddressID']);
        unset($this->session->userdata['couponData']);
        unset($this->session->userdata['timeSlote']);
        unset($this->session->userdata['deleveryType']);

//        $from_email = $email;
//        $message = "Your order has been successfully placed. Your total amout:" . $total;
//        $customerEmil = $this->generalModel->getTableValue('email', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
//        $to_email = $customerEmil['email'];
        //Load email library 
//        $this->load->library('email');
//        $this->email->from($from_email, 'Mooneh');
//        $this->email->to($to_email);
//        $this->email->subject('Order Confirmation');
//        $this->email->message($message);
//          unset($this->session->userdata['couponData']);
        $this->render('frontEnd/cart/Success');
    }



    public function failed() {
        $this->data['dataError'] = $this->session->flashdata('data-error');
        $this->render('frontEnd/cart/Failed');
    }



    public function confirmOrder($orderID = "", $transactionUUID = "", $referenceNumber = "") {
       
        if ($orderID != "" && $transactionUUID != "" && $referenceNumber != "") {
            $address    =   $this->generalModel->getTableValue('*', 'ec_address', 'addressID='.$this->session->userdata('moonehDeliveryAddressID'), FALSE);
            if ($this->generalModel->getTableValue('*', 'ec_order_transaction', array('orderID' => $orderID, 'transaction_uuid' => $transactionUUID, 'reference_number' => $referenceNumber))) {
                $orderData = $this->generalModel->getTableValue('*', 'ec_order', array('orderID' => $orderID));
                $cartItems = $this->carts->getProducts($this->siteLanguageID);
                list($width, $height) = $this->getImageDimension('cart_image_dimen');
                foreach ($cartItems as $key => $cart) {
                    $cartItems[$key]['price'] = $this->currency->format($cart['price'], $this->siteCurrency);
                    $cartItems[$key]['image'] = $this->setSiteImage('product/' . $cart['image'], $width, $height);
                    $opt = array();
                     $optionProductId = array();
                     $optionArra=array();
                    if($cart['option']){
                        foreach($cart['option'] as $option){
                            $optionvals = explode(':', $option);
                            $optionArra=array();
                            $optionProduct='';$i=0;
                            foreach ($optionvals as $value) {
                                $vals = explode('-', $value);
                                if($vals[0] == 'productOptionID'){  
                                    $optionProduct=$vals[1];
                                    $option = $this->optionModel->selectOptionDetail($vals[1],$this->siteLanguageID);
                                    $optionArra=$option;
                                }
                                if($vals[0] == 'optionValueId'){  

                                }
                                $i++;
                            }

                            $optionProductId[] = $optionProduct;

                        }
                    }
                   
                    $optionArray = array();
                     foreach($optionArra as $option){
                        $optionArray[] = $option['oName']."-".$option['name'];
                    }
                    $cartItems[$key]['option'] = $optionArray;

                }
                
                $this->data['cartItems'] = $cartItems;
                $amount = 0;
                if (!empty($cartItems)) {
                    $allTotals = $this->carts->getTotalItemsList('',$address['cityID'],$this->session->userdata('deleveryType'));
                    //echo "<pre>";print_r($allTotals);
                    foreach ($allTotals as $key => $tolVal) {
                        if ($tolVal['code'] == 'total') {
                            $amount = number_format($tolVal['value'],2);
                        }
                        $allTotals[$key]['value'] = $this->currency->format($tolVal['value'], $this->siteCurrency);
                    }
                } else {
                    $this->session->set_flashdata('addressErr', 'Cart Empty');
                    redirect('Cart/checkOut', 'refresh');
                }
                
                //echo $amount;exit;
                $orderDelevery    =   $this->generalModel->getTableValue('*', 'ec_order_to_delivery', 'orderID='.$orderID, FALSE);
                //echo "<pre>"; print_r($this->session->userdata()); exit;
                $delData['nonXss'] = array(
                        'orderID'               => $orderID,
                        'timeSlotID'            => $this->session->userdata('timeSlote'),
                        'orderDeliveryType'     => $this->session->userdata('deleveryType'),
                        'deliveryDate'          => $this->session->userdata('expectedDeleveryDateTime'),
                    );
                    $delData['xssData'] = $this->security->xss_clean($delData['nonXss']);
                if($orderDelevery){
                     $this->generalModel->updateTableValues('ec_order_to_delivery', array('orderID' => $orderID, 'orderDeliveryID' => $orderDelevery['orderDeliveryID']), $delData['xssData']);
                }else{
                    $this->gm->insertValue('ec_order_to_delivery', $delData['xssData']);
                }
                                         
                $this->data['subTotal'] = $allTotals;
                $this->data['content'] = $this->accountModel->selectAddress($this->customerID);
                $moonehDeliveryAddressID = $this->session->userdata('moonehDeliveryAddressID');
                if ($moonehDeliveryAddressID) {
                    $this->data['moonehDeliveryAddress'] = $this->generalModel->getTableValue('*', 'ec_address', 'addressID="' . $moonehDeliveryAddressID . '"', FALSE);
                }
                $this->data['orderID'] =  $orderID;
                $this->data['stateList'] = $this->accountModel->selectStateList($this->siteLanguageID, $this->customerID);
                $this->data['countryList'] = $this->generalModel->getTableValue('countryID,name', 'ec_country', 'countryID="173" AND status="Active"', TRUE);
                $this->data['warehouseCity'] = $this->wm->getWarehouseCityInfo();
                $array = array(
                    'access_key' => "199d034c2dfc3846ae819928166a81a9",
                    'profile_id' => "9BA016D8-646E-4518-8803-07D0BC6462FF",
                    'transaction_uuid' => $transactionUUID,
                    'signed_field_names' => "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,ccAuthService_run,ccCaptureService_run,bill_to_address_city,bill_to_address_country,bill_to_address_line1,bill_to_address_postal_code,bill_to_forename,bill_to_phone,bill_to_surname,bill_to_email,merchant_defined_data1,customer_ip_address",
                    'unsigned_field_names' => "",
                    'signed_date_time' => gmdate("Y-m-d\TH:i:s\Z"),
                    'locale' => "en",
                    'transaction_type' => "sale",
                    'reference_number' => $referenceNumber,
                    'amount' => $amount,
                    'currency' => "QAR",
                    //'device_fingerprint_id' => $orderData['invoicePrefix'] . "-" . $orderData['invoiceNo'],
                    'ccAuthService_run' => 'true',
                    'ccCaptureService_run' => 'true',
                    'bill_to_address_city' => $this->data['moonehDeliveryAddress']['city'],
                    'bill_to_address_country' => 'QA',
                    'bill_to_address_line1' => $this->data['moonehDeliveryAddress']['address1'],
                    'bill_to_address_postal_code' => $this->data['moonehDeliveryAddress']['postcode'],
                    'bill_to_forename' => $this->data['moonehDeliveryAddress']['firstname'],
                    'bill_to_phone' => $this->data['moonehDeliveryAddress']['phone'],
                    'bill_to_email' => $this->session->userdata('moonehEmail'),
                    'bill_to_surname' => $this->data['moonehDeliveryAddress']['lastname'],
                    'merchant_defined_data1' => 'WC',
                    'customer_ip_address' => $this->getIPAddress()
                );

                $array['signature'] = $this->sign($array);
                //echo"<pre>";print_r($array);exit;
                $this->data['payForm'] = $array;
                $this->data['payFormAction'] = "https://secureacceptance.cybersource.com/pay";
                $this->render('frontEnd/cart/CartPayment');
            } else {
                $this->session->set_flashdata('addressErr', 'order data missing, Please try again');
                redirect('Cart/checkOut', 'refresh');
            }
        } else {
            $this->session->set_flashdata('addressErr', 'order data missing, Please try again');
            redirect('Cart/checkOut', 'refresh');
        }
    }

   
    function getIPAddress() {  
        //whether ip is from the share internet  
        if(!empty($_SERVER['HTTP_CLIENT_IP'])) {  
            $ip = $_SERVER['HTTP_CLIENT_IP'];  
        }  
        //whether ip is from the proxy  
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];  
        }  
        //whether ip is from the remote address  
        else{  
           $ip = $_SERVER['REMOTE_ADDR'];  
        }  
        return $ip;  
    }  

    function sign($params) {
        $secretKey = "e9e6ee6272934d5da33c865dbdac36a05bf7acbebc7948bcb9c51d1c4654b8271c1412afc28d480ebb893a19e7541244f243f10d488c437581b22d3fb14c87934a0379f9090b440caf7d21143622658f8d111a5120a34501b136766176c84538e259f8da72654e9b971434e583a7cf12f161b4c50d3a448cb4c47aea4120bea5";
        return $this->signData($this->buildDataToSign($params), $secretKey);
    }

    function signData($data, $secretKey) {
        return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
    }

    function buildDataToSign($params) {
        $signedFieldNames = explode(",", $params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
            $dataToSign[] = $field . "=" . $params[$field];
        }
        return $this->commaSeparate($dataToSign);
    }


    function commaSeparate($dataToSign) {
        return implode(",", $dataToSign);
    }



    public function transactionStatus() {
        $params = array();
        $accept = array(100, 110);
        $review = array(200, 201, 230, 480, 520);
        $reject = array(102, 200, 202, 203, 204, 205, 207, 208, 210, 211, 221, 222, 230, 231, 232, 233, 234, 236, 240, 475, 476, 481);
        $error = array(104, 150, 151, 152, 250);
        foreach ($_REQUEST as $name => $value) {
            $params[$name] = $value;
        } 

        $merchantReferenceNumber = $params['req_reference_number'];
        $orderData          = $this->generalModel->getLoginAfterCart($merchantReferenceNumber);
        if($params['req_merchant_defined_data1']=='WC'){
            $customerData        = array(
                'moonehcustomerID'          => $orderData['customerID'],
                'moonehFirstname'           => $orderData['firstname'],
                'moonehLastname'            => $orderData['lastname'],
                'moonehEmail'               => $orderData['email'],
                'moonehStatus'              => $orderData['status'],
                'moonehLoggedIn'            => TRUE
            );
            $this->session->set_userdata($customerData);
        }
        
        if (strcmp($params["signature"], $this->sign($params)) == 0) {
            if (isset($params['reason_code']) && in_array($params['reason_code'], $accept) && $params['decision'] == "ACCEPT") {
                $transactionUUID = $params['req_transaction_uuid'];
                $transactionRequestID = $params['transaction_id'];
                $message = $params['message'];
                $reasonCode = $params['reason_code'];
                $cardType = $params['card_type_name'];
                $cardExpDate = $params['req_card_expiry_date'];
                $paymentMethod = $params['req_payment_method'];
                $cardNumber = $params['req_card_number'];
                $decision = $params['decision'];               
                $orderTransDetail = $this->generalModel->getTableValue('amount,currency', 'ec_order_transaction', array('transaction_uuid' => $transactionUUID, 'reference_number' => $merchantReferenceNumber), FALSE);
//                echo round($orderTransDetail['amount']); exit;
//                echo"<pre>";print_r($orderTransDetail);exit;
                if(number_format($orderTransDetail['amount'],2)==number_format($params['req_amount'],2) && $orderTransDetail['currency']==$params['req_currency']){                   
                    $updateArray = array(
                        'transactionRequestID' => $transactionRequestID,
                        'message' => $message,
                        'reasonCode' => $reasonCode,
                        'decision' => $decision,
                        'paymentMethod' => $paymentMethod,
                        'cardType' => $cardType,
                        'cardExpDate' => $cardExpDate,
                        'cardNumber' => $cardNumber,
                        'paymentStatus' => 'success',
                        'updateTime' => date('Y-m-d H:i:s')
                    );
                    $this->generalModel->updateTableValues('ec_order_transaction', array('transaction_uuid' => $transactionUUID, 'reference_number' => $merchantReferenceNumber), $updateArray);
                    $email      =   $this->getSettingValue('email');
                    $orderID    =   $orderData['orderID'];
                     if (@$orderID != "") {
                            $orderProduct = $this->generalModel->getTableValue('productID,quantity', 'ec_order_product', array("orderID" => $orderID), TRUE);
//                            echo "<pre>"; print_r($orderProduct); exit;
                            
                            if ($orderProduct) {
                                foreach ($orderProduct as $key => $proVal) {
                                    $this->cartModel->decreaseProductQuentity($proVal['productID'], $proVal['quantity']);
                                    $product    =   $this->generalModel->getTableValue('productID,type', 'ec_product', array("productID" => $proVal['productID']), FALSE);
                                    if($product['type']=='Bundle'){
                                        $bundleProduct    =   $this->generalModel->getTableValue('productID,bundleProductID,quantity', 'ec_product_bundle', array("productID" => $proVal['productID']), TRUE);
                                        if($bundleProduct){
                                            foreach($bundleProduct as $bundleProductData){
                                                $this->cartModel->decreaseProductQuentity($bundleProductData['bundleProductID'], $bundleProductData['quantity']);
                                                
                                            }
                                        }
                                    }
                                    $orderOptionProduct = $this->cartModel->getProductOption($orderID);
                                    //print_r($orderOptionProduct);
                                    if($orderOptionProduct){ 
                                        foreach($orderOptionProduct as $option){
                                            $this->cartModel->decreaseProductOptionQuentity($proVal['productID'], $proVal['quantity'],$option['pID']);
                                        }

                                    }
                                }
                            }
                           
                        }
                        //$total = $this->carts->getTotal();
                        $total =    $this->currency->format($this->generalModel->getFieldValue('value', 'ec_order_total', 'orderID=' . $orderID . ' AND code="total"'), $this->siteCurrency);
                        $customerID = $orderData['customerID'];
    //                    $this->carts->clear();
                        $this->data['orderID']    =   $orderID;
                        unset($this->session->userdata['orderID']);
                        unset($this->session->userdata['moonehDeliveryAddressID']);  
                        //Send SMS Start
                        $customerData   = $this->generalModel->getTableValue('firstname,lastname,email,countryCode,telephone', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
                        $orderDataSMS      = $this->generalModel->getTableValue('orderID,invoiceNo', 'ec_order', array("orderID" => $orderID));
                        //echo "<pre>"; print_r($orderData); exit;
                        //$phoneNo    =  "974".$customerData['telephone']; 
                        $phoneNo    =  $customerData['countryCode'].$customerData['telephone']; 
                        //$smsMessage =   "Your order successfully completed.Your order id : ".$orderDataSMS['orderID']." ,invoice no : ".$orderDataSMS['orderID'].", amount : ".$total;
                        $smsMessage =   "Your order has been placed on Mowneh. OrderID : ".$orderDataSMS['orderID'].", Order amount: ".$total.". Ordered on ".date('d-m-Y').". Thank You!";
                        $this->sendSMS($smsMessage,$phoneNo);
                        
                        $smsMessage =   "New order has been placed on Mowneh. OrderID : ".$orderDataSMS['orderID'].", Order amount: ".$total.". Ordered on ".date('d-m-Y').". Thank You!";
                        $this->sendSMS($smsMessage,'97466714422'); // SMS TO ADMIN
                        
                        //Send SMS ENd
                        
                       $customerEmil = $this->generalModel->getTableValue('email', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
                        $config['protocol'] = 'sendmail';
                        $config['mailtype'] = 'html';
                        $from_email = $email;
                        $to_email = $customerEmil['email'];
                        //Load email library 
                        //$message = "Your order has been successfully placed. Your total amout:" . $total;
                        $message    =   $this->orderConfirmMessage($customerID,$orderID);
                        //echo $message; exit;
                        $this->load->library('email');
                        $this->email->initialize($config);
                        $this->email->from($from_email, 'Mooneh');
                        $this->email->to($to_email);
                        $this->email->subject('Order Confirmation');
                        $this->email->message($message);
                        if($this->email->send()){
                            $from_email = $email;
                            $to_email = $from_email;
                            $this->load->library('email');
                            $this->email->initialize($config);
                            $this->email->from($from_email, 'Mooneh');
                            $this->email->to($to_email);
                            $this->email->subject('Order Confirmation');
                            
                            $message    =   $this->orderConfirmMessageAdmin($customerID,$orderID);
                            $this->email->message($message);
                            $this->email->send();
                        }
                    //echo "<pre>";print_r($updateArray);exit;
                    if($params['req_merchant_defined_data1']=='WC'){
                        unset($this->session->userdata['couponData']);
                        $this->carts->clear();
                        redirect('Cart/success/'.$orderData['orderID'], 'refresh');
                    }else{
                        $this->load->library('CartsApi');
                        $this->cartsapi->clear($customerID);
                        redirect('api/ApiSuccess/appSuccess', 'refresh');
                    }  
                }else{
                        $transactionRequestID = $params['transaction_id'];
                        $message = $params['message'];
                        $reasonCode = $params['reason_code'];
                        $cardType = $params['card_type_name'];
                        $cardExpDate = $params['req_card_expiry_date'];
                        $paymentMethod = $params['req_payment_method'];
                        $cardNumber = $params['req_card_number'];
                        $decision = $params['decision'];
                        $updateArray = array(
                            'transactionRequestID' => $transactionRequestID,
                            'message' => $message,
                            'reasonCode' => $reasonCode,
                            'decision' => $decision,
                            'paymentMethod' => $paymentMethod,
                            'cardType' => $cardType,
                            'cardExpDate' => $cardExpDate,
                            'cardNumber' => $cardNumber,
                            'paymentStatus' => 'fraud',
                            'updateTime' => date('Y-m-d H:i:s')
                        );

                        $this->generalModel->updateTableValues('ec_order_transaction', array('transaction_uuid' => $transactionUUID, 'reference_number' => $merchantReferenceNumber), $updateArray);
                        if($params['req_merchant_defined_data1']=='WC'){
                            $this->session->set_flashdata('data-error', $message);
                            redirect('Cart/failed', 'refresh');
                        }else{
                            redirect('api/ApiSuccess/appFailed', 'refresh');
                        } 
                }
              
            }else{
                $transactionUUID = $params['req_transaction_uuid'] ?? "";
                $transactionRequestID = $params['transaction_id'] ?? "";
                $message = $params['message'] ?? "";
                $reasonCode = $params['reason_code'] ?? "";
                $cardType = $params['card_type_name'] ?? "";
                $cardExpDate = $params['req_card_expiry_date'] ?? "";
                $paymentMethod = $params['req_payment_method'] ?? "";
                $cardNumber = $params['req_card_number'] ?? "";
                $decision = $params['decision'] ?? "";
                $updateArray = array(
                    'transactionRequestID' => $transactionRequestID,
                    'message' => $message,
                    'reasonCode' => $reasonCode,
                    'decision' => $decision,
                    'paymentMethod' => $paymentMethod,
                    'cardType' => $cardType,
                    'cardExpDate' => $cardExpDate,
                    'cardNumber' => $cardNumber,
                    'paymentStatus' => 'cancelled',
                    'updateTime' => date('Y-m-d H:i:s')
                );
                $this->generalModel->updateTableValues('ec_order_transaction', array('transaction_uuid' => $transactionUUID, 'reference_number' => $merchantReferenceNumber), $updateArray);
                if($params['req_merchant_defined_data1']=='WC'){
                    $this->session->set_flashdata('data-error', $message);
                    redirect('Cart/failed', 'refresh');
                }else{
                    redirect('api/ApiSuccess/appFailed', 'refresh');
                }  
            }
        }else {
            $transactionUUID = $params['req_transaction_uuid'];
            $transactionRequestID = $params['transaction_id'];
            $message = $params['message'];
            $reasonCode = $params['reason_code'];
            $cardType = $params['card_type_name'];
            $cardExpDate = $params['req_card_expiry_date'];
            $paymentMethod = $params['req_payment_method'];
            $cardNumber = $params['req_card_number'];
            $decision = $params['decision'];
            $updateArray = array(
                'transactionRequestID' => $transactionRequestID,
                'message' => $message,
                'reasonCode' => $reasonCode,
                'decision' => $decision,
                'paymentMethod' => $paymentMethod,
                'cardType' => $cardType,
                'cardExpDate' => $cardExpDate,
                'cardNumber' => $cardNumber,
                'paymentStatus' => 'failed',
                'updateTime' => date('Y-m-d H:i:s')
            );
            $this->generalModel->updateTableValues('ec_order_transaction', array('transaction_uuid' => $transactionUUID, 'reference_number' => $merchantReferenceNumber), $updateArray);
            //echo "<pre>";print_r($updateArray);exit;
            if($params['req_merchant_defined_data1']=='WC'){
                $this->session->set_flashdata('data-error', $message);
                redirect('Cart/failed', 'refresh');
            }else{
                redirect('api/ApiSuccess/appFailed', 'refresh');
            }
        }
    }

    function randString($length) {
        $char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $char = str_shuffle($char);
        for ($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
            $rand .= $char{mt_rand(0, $l)};
        }
        return $rand;
    }

    function get_unique_key($length = 16, $table, $field = "transaction_uuid") {
        $i = 0;
        do {
            $unique_id = $this->randString($length);
            $this->db->from($table);
            $this->db->where($field, $unique_id);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $i = 0;
            } else {
                $i = 1;
            }
        } while ($i = 0);
        return $unique_id;
    }
 
    function appSuccess(){

    }
    
    function appFailed(){    
    }

    public function codAction($orderID) {
        if($orderID){
            $paymentMethod   =   2;
            $updateArray = array(
                       'transactionRequestID' => "",
                        'message' => "Cash on delivery",
                        'reasonCode' => "",
                        'decision' => "",
                        'paymentMethod' => $paymentMethod,
                        'cardType' => "",
                        'cardExpDate' => "",
                        'cardNumber' => "",
                        'paymentStatus' => 'cod',
                        'updateTime' => date('Y-m-d H:i:s')
                    );
                    $this->generalModel->updateTableValues('ec_order_transaction', array('orderID' => $orderID), $updateArray);
                    $orderPaymet['paymentMethod']   =   $paymentMethod;
                    $this->generalModel->updateTableValues('ec_order', array('orderID' => $orderID), $orderPaymet);
                    $email      =   $this->getSettingValue('email');
                        if (@$orderID != "") {
                            $orderProduct = $this->generalModel->getTableValue('productID,quantity,orderProductID', 'ec_order_product', array("orderID" => $orderID), TRUE);
//                            echo "<pre>"; print_r($orderProduct); exit;
                            
                            if ($orderProduct) {
                                foreach ($orderProduct as $key => $proVal) {
                                    $this->cartModel->decreaseProductQuentity($proVal['productID'], $proVal['quantity']);
                                    $product    =   $this->generalModel->getTableValue('productID,type', 'ec_product', array("productID" => $proVal['productID']), FALSE);
                                    if($product['type']=='Bundle'){
                                        $bundleProduct    =   $this->generalModel->getTableValue('productID,bundleProductID,quantity', 'ec_product_bundle', array("productID" => $proVal['productID']), TRUE);
                                        if($bundleProduct){
                                            foreach($bundleProduct as $bundleProductData){
                                                $this->cartModel->decreaseProductQuentity($bundleProductData['bundleProductID'], $bundleProductData['quantity']);
                                                
                                            }
                                        }
                                    }
                                    $orderOptionProduct = $this->cartModel->getProductOptionByProductId($orderID,$proVal['orderProductID']);
                                    //print_r($orderOptionProduct);
                                    if($orderOptionProduct){ 
                                        foreach($orderOptionProduct as $option){
                                            $this->cartModel->decreaseProductOptionQuentity($proVal['productID'], $proVal['quantity'],$option['pID']);
                                        }

                                    }
                                 
                                }
                            }
                        }
                        //$total = $this->carts->getTotal();
                        $total =    $this->currency->format($this->generalModel->getFieldValue('value', 'ec_order_total', 'orderID=' . $orderID . ' AND code="total"'), $this->siteCurrency);
                        $customerID = $this->customerID;
    //                    $this->carts->clear();
                        $this->data['orderID']    =   $orderID;
                        unset($this->session->userdata['orderID']);
                        unset($this->session->userdata['moonehDeliveryAddressID']);      
                        
                       //Send SMS Start
                        $customerData   = $this->generalModel->getTableValue('firstname,lastname,email,countryCode,telephone', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
                        
                        $orderDataSMS      = $this->generalModel->getTableValue('orderID,invoiceNo,total', 'ec_order', array("orderID" => $orderID));
                        //$ordrTotalSms      = $this->generalModel->getTableValue('value', 'ec_order_total', array("orderID" => $orderID, "code" => 'total'));
                        $phoneNo    =  $customerData['countryCode'].$customerData['telephone']; 
                        //$smsMessage =   "Your order successfully completed.Your order id : ".$orderDataSMS['orderID']." ,invoice no : ".$orderDataSMS['orderID'].", amount : ".$total;
                        $smsMessage =   "Your order has been placed on Mowneh. OrderID : ".$orderDataSMS['orderID'].", Order amount: ".$total.". Ordered on ".date('d-m-Y').". Thank You!";
                        $this->sendSMS($smsMessage,$phoneNo);
                        //Send SMS ENd
                        $from_email = $email;
                        //$message = "Your order has been successfully placed. Your total amout:" . $total;
                        $message    =   $this->orderConfirmMessage($customerID,$orderID);                   
                        $customerEmil = $this->generalModel->getTableValue('email', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
                        $config['protocol'] = 'sendmail';
                        $config['mailtype'] = 'html';
                        $to_email = $customerEmil['email'];
                        //Load email library 
                        $this->load->library('email');
                        $this->email->initialize($config);
                        $this->email->from($from_email, 'Mooneh');
                        $this->email->to($to_email);
                        $this->email->subject('Order Confirmation');
                        $this->email->message($message);
                        if($this->email->send()){
                             $from_email = $email;
                             $to_email = $from_email;
                            $this->load->library('email');
                            $this->email->initialize($config);
                            $this->email->from($from_email, 'Mooneh');
                            $this->email->to($to_email);
                            $this->email->subject('Order Confirmation');
                            $message    =   $this->orderConfirmMessageAdmin($customerID,$orderID);
                            $this->email->message($message);
                            $this->email->send();
                        }
                    //echo "<pre>";print_r($updateArray);exit;
                        unset($this->session->userdata['couponData']);
                        $this->carts->clear();
                        redirect('Cart/success/'.$orderID, 'refresh');
        }else{
                $message    =   "Something Went Wrong Please Try Again...";
                $this->session->set_flashdata('data-error', $message);
                redirect('Cart/failed', 'refresh');
        }
    }

    public function orderConfirmMessage($customerID="",$orderID=""){
        $logo  = base_url('uploads/siteInfo/') . $this->getSettingValue('logo');
        $customerData   = $this->generalModel->getTableValue('firstname,lastname,email', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
        $orderData      = $this->generalModel->getTableValue('orderID,invoiceNo,total', 'ec_order', array("orderID" => $orderID));
        $email      =   $this->getSettingValue('email');
        $phone      =   $this->getSettingValue('phone');
        $address    =   $this->getSettingValue('address', $this->siteLanguageID, true);  
        $params = array(
            'orderID' => $orderID
        );
        $products = $this->om->getOrderProducts($params);   
       $productsDetail      = $this->generalModel->getTableValue('*', 'ec_order_total', array("orderID" => $orderID),true);
            $message    =   '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>Order Confirmation</title>
                <style type="text/css">
                @font-face {font-family: "Mary Ann"; src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot"); src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot?#iefix") format("embedded-opentype"); }
                        body {
                                Margin: 0;
                                padding: 0;
                                font-family: Mary Ann;
                                color: #1d1d1d;
                        }
                        table {
                                border-spacing: 0;
                        }
                        td {
                                padding: 0;
                        }
                        img {
                                border: 0;
                        }
                        .wrapper
                        {
                                width: 100%;
                                table-layout: fixed;
                                background-color: #f6f6f6;
                        }
                        .webkit
                        {
                                max-width: 600px;
                                background-color: #fff;
                        }
                        .content
                        {
                            padding: 30px 20px;
                        }
                        .reset a
                        {
                                color: #ffffff;
                                font-weight: 400;
                                text-transform: uppercase;
                                background-color: #F06C00;
                                border: 1px solid #F06C00;
                                text-decoration: none;
                                padding: 5px 8px;
                                text-align: center;
                                border-radius: 5px;
                        }
                        .reset a:hover
                        {
                                color: #F06C00;
                                background-color: #fff;
                                border: 1px solid #F06C00;
                        }
                        .content p
                        {
                                font-size: 15px;
                        } 
                        .footer
                        {
                                border-top: 1px solid #f2f2f2;
                                padding: 30px 0px;
                        } 
                        @media screen and (max-width: 600px) { 
                        }
                        @media screen and (max-width: 400px) { 
                        }
                        

                </style>
        </head>
        <body>
         <center class="wrapper">
                 <div class="webkit">
                        <table class="outer" align="center">
                                 <tr>
                                         <td>
                                                 <table width="100%" style="border-spacing: 0;">
                                                    <tr>
                                                                <td style="background-color:#F06C00;padding: 10px;text-align: center;">
                                                                    <span style="filter: brightness(100) contrast(100%) sepia(99) grayscale(1);">
                                                                        <a href="'.base_url().'" ><img  src="'.$logo.'" width="120" alt="logo" /></a>
                                                                    </span>
                                                                </td>
                                                        </tr>
                                                 </table>
                                         </td>
                                 </tr>
                                 <tr>
                                         <td class="content" style="text-align:left;">
                                                <h3>Hello '.$customerData["firstname"].' '.$customerData["lastname"].',</h3>
                                                <p> Thank you for shopping with Mowneh!
                                                       <br/>Your order '.$orderData["orderID"].' is confirmed and attached herewith is your invoice.</p> 
                                                        <table class="invoice" width="100%" style="border-spacing: 0;border:1px solid #1d1d1d;">
                                                                <tr>
                                                                    <td style="border-right:1px solid #1d1d1d;padding:5px;">Order id : '.$orderData["orderID"].'</td>
                                                                    <td style="border-right:1px solid #1d1d1d;padding:5px;">Invoice No : '.$orderData["invoiceNo"].'</td>
                                                                    <td style="padding:5px;">'.$this->currency->format($orderData["total"], $this->siteCurrency).'</td>
                                                                </tr></table><br/><table class="invoice" width="100%" style="border-spacing: 0;border:1px solid #1d1d1d;">
                                                                <tr><th>Product</th><th>Total</th></tr>';
                                                                foreach( $products as $product){ 
                                                                 $message    .=   ' <tr>
                                                                    <td style="border-right:1px solid #1d1d1d;padding:5px;">'.$product["name"].'</td>
                                                                    <td style="border-right:1px solid #1d1d1d;padding:5px;">'.$this->currency->format($product["total"], $this->siteCurrency).'</td>
                                                                    
                                                                </tr>';
                                                                }
                                                                foreach( $productsDetail as $detail){ 
                                                                 $message    .=   ' <tr>
                                                                    <td style="border-right:1px solid #1d1d1d;padding:5px;">'.$detail["title"].'</td>
                                                                    <td style="border-right:1px solid #1d1d1d;padding:5px;">'.$this->currency->format($detail["value"], $this->siteCurrency).'</td>
                                                                    
                                                                </tr>';
                                                                }
                                                       $message    .=   ' </table>	
                                                        <p>
                                                        <br/>Please let us know if you have any issues with your purchase, by replying to this email or calling our hotline '.$phone.'.
                                                        <br/>
                                                        <br/>Regards,
                                                        <br/>Mowneh Team
                                                </p>
                                         </td>
                                 </tr>
                                 <tr>
                                         <td>
                                                <table width="100%" style="border-spacing: 0;">
                                                        <tr>
                                                                <td class="footer" style="border-top:1px solid #f2f2f2;padding: 15px;text-align:left;"> 
                                                                        <p style="font-size: 12px;color: #a7a7a7;">
                                                                                '.$address.'<br/>Copyright © 2020 <a style="color: #a7a7a7;" href="http://www.mowneh.com">Mowneh.</a> All Rights Reserved
                                                                        </p>

                                                                </td>
                                                                <td style="border-top:1px solid #f2f2f2;padding: 15px;text-align:right;">
                                                                       <a href="'.base_url().'"><img src="'.$logo.'" width="80" alt="logo"></a>
                                                                        <br/>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td colspan="2" style="padding: 5px; background-color: #F06C00;">
                                                                </td>
                                                        </tr>
                                                </table>
                                         </td>
                                 </tr>
                         </table>
                 </div>
         </center>
        </body>
        </html>';
        
            return $message;

    }

    
    public function orderConfirmMessageAdmin($customerID="",$orderID=""){
        $logo  = base_url('uploads/siteInfo/') . $this->getSettingValue('logo');
        $customerData   = $this->generalModel->getTableValue('firstname,lastname,email', 'ec_customer', array("customerID" => $customerID, 'status' => 'Active'));
        $orderData      = $this->generalModel->getTableValue('orderID,invoiceNo,total', 'ec_order', array("orderID" => $orderID));
        $email      =   $this->getSettingValue('email');
        $phone      =   $this->getSettingValue('phone');
        $address    =   $this->getSettingValue('address', $this->siteLanguageID, true);
            $message    =   '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <title>Order Confirmation</title>
                <style type="text/css">
                @font-face {font-family: "Mary Ann"; src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot"); src: url("//db.onlinewebfonts.com/t/389170fdc8bb30fec551161a4794f520.eot?#iefix") format("embedded-opentype"); }
                        body {
                                Margin: 0;
                                padding: 0;
                                font-family: Mary Ann;
                                color: #1d1d1d;
                        }
                        table {
                                border-spacing: 0;
                        }
                        td {
                                padding: 0;
                        }
                        img {
                                border: 0;
                        }
                        .wrapper
                        {
                                width: 100%;
                                table-layout: fixed;
                                background-color: #f6f6f6;
                        }
                        .webkit
                        {
                                max-width: 600px;
                                background-color: #fff;
                        }
                        .content
                        {
                               padding: 30px 20px;
                        }
                        .reset a
                        {
                                color: #ffffff;
                                font-weight: 400;
                                text-transform: uppercase;
                                background-color: #F06C00;
                                border: 1px solid #F06C00;
                                text-decoration: none;
                                padding: 5px 8px;
                                text-align: center;
                                border-radius: 5px;
                        }
                        .reset a:hover
                        {
                                color: #F06C00;
                                background-color: #fff;
                                border: 1px solid #F06C00;
                        }
                        .content p
                        {
                                font-size: 15px;
                        } 
                        .footer
                        {
                                border-top: 1px solid #f2f2f2;
                                padding: 30px 0px;
                        } 
                        @media screen and (max-width: 600px) { 
                        }
                        @media screen and (max-width: 400px) { 
                        }
                </style>
        </head>
        <body>
         <center class="wrapper">
                 <div class="webkit">
                         <table class="outer" align="center">
                                 <tr>
                                         <td>
                                                <table width="100%" style="border-spacing: 0;">
                                                    <tr>
                                                                <td style="background-color:#F06C00;padding: 10px;text-align: center;">
                                                                    <span style="filter:brightness(100) contrast(100%) sepia(99) grayscale(1)">
                                                                        <a href="'.base_url().'" ><img  src="'.$logo.'" width="120" alt="logo" /></a>
                                                                    </span>
                                                                </td>
                                                        </tr>
                                                 </table>
                                         </td>
                                 </tr>
                                 <tr>
                                         <td class="content" style="text-align:left;">
                                                <h3>Hi,</h3>
                                                        <br/>You have a new order from '.$customerData["firstname"].' '.$customerData["lastname"].'</p>  
                                                        <table class="invoice" width="100%" style="border-spacing: 0;border:1px solid #1d1d1d;">
                                                                <tr>
                                                                        <td style="border-right:1px solid #1d1d1d;padding:5px;">Order id : '.$orderData["orderID"].'</td>
                                                                        <td style="border-right:1px solid #1d1d1d;padding:5px;">Invoice No : '.$orderData["invoiceNo"].'</td>
                                                                        <td style="padding:5px;">'.$this->currency->format($orderData["total"], $this->siteCurrency).'</td>
                                                                </tr>
                                                        </table>	
                                                        <p>
                                                        <br/>
                                                        <br/>Regards,
                                                        <br/>Mowneh Team
                                                </p>
                                         </td>
                                 </tr>
                                 <tr>
                                         <td>
                                                <table width="100%" style="border-spacing: 0;">
                                                        <tr>
                                                                <td class="footer" style="border-top:1px solid #f2f2f2;padding: 15px;text-align:left;"> 
                                                                        <p style="font-size: 12px;color: #a7a7a7;">
                                                                                '.$address.'<br/>Copyright © 2020 <a style="color: #a7a7a7;" href="http://www.mowneh.com">Mowneh.</a> All Rights Reserved
                                                                        </p>
                                                                </td>
                                                                <td style="border-top:1px solid #f2f2f2;padding: 15px;text-align:right;">
                                                                        <a href="'.base_url().'"><img src="'.$logo.'" width="80" alt="logo"></a>
                                                                        <br/>
                                                                </td>
                                                        </tr>
                                                        <tr>
                                                                <td colspan="2" style="padding: 5px; background-color: #F06C00;">
                                                                </td>
                                                        </tr>
                                                </table>
                                         </td>
                                 </tr>
                         </table>
                 </div>
         </center>
        </body>
        </html>';

            return $message;

    }



}


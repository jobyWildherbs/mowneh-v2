<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends Site_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->library('CustomerLogin');
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('BlogModel', 'blogModel');
        $this->load->library('form_validation');
        $this->lang->load('site', $this->siteLanguage);
        $this->customerID = $this->customer->getID();
    }

    public function index() {
        $par = array('languageID'=>$this->siteLanguageID,'limit'=>10);
        $blogList = $this->blogModel->selectBlog($par);
        list($width, $height) = $this->getImageDimension('blog_image_dimen');
        foreach ($blogList as $key => $blogData) {

            $blogList[$key]['image'] = $this->setSiteImage("blog/" . $blogData['image'], $width, $height);
        }
        $this->data['blogList'] = $blogList;
        $this->render('frontEnd/blog/blog');
    }
     public function blogDetail() {
        $blogID = $this->uri->segment(3);
        
        $par = array('languageID'=>$this->siteLanguageID,'limit'=>5);
        $blogList = $this->blogModel->selectBlog($par);
        $this->data['blogIDs']=$blogID;
       // print_r($this->data['blogIDs']);
        list($width, $height) = $this->getImageDimension('blog_image_dimen');
        foreach ($blogList as $key => $blogData) {

            $blogList[$key]['image'] = $this->setSiteImage("blog/" . $blogData['image'], $width, $height);
        }
        $this->data['blogList'] = $blogList;
        $blogDeta = $this->blogModel->selectBlogDet($blogID,$this->siteLanguageID);
        list($width, $height) = $this->getImageDimension('blog_image_dimen');
        foreach ($blogDeta as $key => $blogDataDetail) {

            $blogDeta[$key]['image'] = $this->setSiteImage("blog/" . $blogDataDetail['image'], $width, $height);
        }
        $this->data['blogDeta'] = $blogDeta;
        $this->render('frontEnd/blog/blog_detail');
    }

   

}

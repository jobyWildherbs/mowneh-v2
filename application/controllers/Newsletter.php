<?php
	if(!defined('BASEPATH'))
		exit('No direct script access allowed');

	class Newsletter extends Site_Controller
	{

		public function __construct()
		{
			parent::__construct();
                        //$this->load->model('GeneralModel', 'generalModel');
		}

        function index(){
		    $this->render('frontEnd/newsletter/add');
        }
	public function add_new() {
        if ($this->input->post()) {

            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean|is_unique[ec_newsletter.email]', array(
                'is_unique' => 'Account already exists with this email. Please Use Another Email',
                'valid_email' => 'Please provide a valid email address',
                'xss_clean' => 'Please provide a valid email address'
            ));
            if ($this->form_validation->run() === TRUE) {
                $resData['nonXss'] = array(
                    'email' => $this->input->post('email'),
                    'dateAdded' => date('Y-m-d H:i')
                );

                $resData['xssData'] = $this->security->xss_clean($resData['nonXss']);
                $insert = $this->generalModel->insertValue('ec_newsletter', $resData['xssData']);
                echo(json_encode($insert));
            }
        }
    }


	}
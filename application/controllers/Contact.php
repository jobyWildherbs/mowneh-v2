<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends Site_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('GeneralModel', 'generalModel');
        $this->load->model('SettingsModel', 'sm');
        
    }

    public function index() {
        $this->data['addressValue'] = $this->sm->getSettingValues('address', $this->siteLanguageID, true);
         $this->data['emailValue'] = $this->getSettingValue('email');
        
         $this->data['phoneValue'] = $this->getSettingValue('phone');
         $this->data['latitudeVal'] = $this->getSettingValue('latitude');
         $this->data['longitudeVal'] = $this->getSettingValue('longitude');
        $this->render('frontEnd/contact/contact_us');
    }

}
